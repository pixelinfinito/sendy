$(document).ready(function() {
    
	$('#SMSSettingsForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            sc_acc_id: {
                validators: {
                    notEmpty: {
                        message: 'AccountID cannot be empty'
                    },
					 
                }
            },
			sc_auth_token: {
                validators: {
                    notEmpty: {
                        message: 'Auth Token cannot be empty'
                    },
					 
                }
            },
			sc_origin_number: {
                validators: {
                    notEmpty: {
                        message: 'Origin number cannot be empty'
                    },
					 
                }
            },
			sc_resend_time: {
                validators: {
                    notEmpty: {
                        message: 'Password rensend interval cannot be empty'
                    },
					 
                }
            },
			campaign_minval: {
                validators: {
                    notEmpty: {
                        message: 'Minval token cannot be empty'
                    },
					 
                }
            },
			campaign_maxval: {
                validators: {
                    notEmpty: {
                        message: 'Max token cannot be empty'
                    },
					 
                }
            },
			credit_threshold_recall: {
                validators: {
                    notEmpty: {
                        message: 'Credit threshold interval cannot be empty'
                    },
					 
                }
            },
			
			cmp_alerts: {
                validators: {
                   stringLength: {
                        min: 3,
                        max: 11,
                        message: 'Maximum 11 characters long'
                    },
					 
                }
            },
			blacklist_alerts: {
                validators: {
                   stringLength: {
                        min: 3,
                        max: 11,
                        message: 'Maximum 11 characters long'
                    },
					 
                }
            },
			
			cn_alerts: {
                validators: {
                   stringLength: {
                        min: 3,
                        max: 11,
                        message: 'Maximum 11 characters long'
                    },
					 
                }
            },
			
			sid_alerts: {
                validators: {
                   stringLength: {
                        min: 3,
                        max: 11,
                        message: 'Maximum 11 characters long'
                    },
					 
                }
            },
			
			pay_alerts: {
                validators: {
                   stringLength: {
                        min: 3,
                        max: 11,
                        message: 'Maximum 11 characters long'
                    },
					 
                }
            },
			
			sid_approval_alerts: {
                validators: {
                   stringLength: {
                        min: 3,
                        max: 11,
                        message: 'Maximum 11 characters long'
                    },
					 
                }
            },
			sc_sender_id: {
                validators: {
                   stringLength: {
                        min: 3,
                        max: 11,
                        message: 'Maximum 11 characters long'
                    },
					 
                }
            },
			
			service_recall: {
                validators: {
                    notEmpty: {
                        message: 'Service recall interval cannot be empty'
                    },
					 
                }
            }
			
        }
		
    });
	
    
	$('#enable_alerts').click(function() {
	if($("#enable_alerts").is(':checked')){
		$('#hid_enable_alerts').val('1');
		
	} else {
		$('#hid_enable_alerts').val('0');
		
	}
	});
});


function SmsSettingsController($scope, $http) {
	
		$scope.SiteSmsBtn=function() {
		
		var validate=$('#SmsSettingsForm').bootstrapValidator('validate');
		var f1=$('#sc_acc_id').val();
		var f2=$('#sc_auth_token').val();
		var f3=$('#sc_origin_number').val();
		var f4=$('#sc_sender_id').val();
		var f5=$('#sc_resend_time').val();
		
		var f6=$('#campaign_minval').val();
		var f7=$('#campaign_maxval').val();
		var f8=$('#service_recall').val();
		
		
		var f9=$('#credit_threshold_recall').val();
		var f10=$('#cmp_alerts').val();
		var f11=$('#cn_alerts').val();
		var f12=$('#sid_alerts').val();
		var f13=$('#pay_alerts').val();
		var f14=$('#sid_approval_alerts').val();
		var f15=$('#blacklist_alerts').val();
		var enable_alerts=$('#hid_enable_alerts').val();
		
		if(f4.length>11 || f10.length>11 || f11.length>11 || f12.length>11 || f13.length>11 || f14.length>11 || f15.length>11)
		{
			alert('Exceeding check character length of Sender/Masking ID, please cross check.');
			return false;
		}
		
		if(f1!='' && f2!='' && f3!='' && f5!='' && f6!='' && f7!='' && f8!='' && f9!=''){
		
			$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
			var parsePath=$(location).attr('href');
			
			var re = new RegExp(/^.*\//);
			var rCPath= re.exec(window.location.href);
			
			$http.post(rCPath+'process_sms_settings', {'sc_acc_id':f1, 'sc_auth_token': f2,'sc_origin_number':f3,
			'sc_sender_id':f4,'sc_resend_time':f5,'campaign_minval':f6,'campaign_maxval':f7,'service_recall':f8,
			'credit_threshold_recall':f9,'cmp_alerts':f10,'cn_alerts':f11,'sid_alerts':f12,'pay_alerts':f13,
			'sid_approval_alerts':f14,'enable_alerts':enable_alerts,'blacklist_alerts':f15}
			).success(function(data) {
				//alert(data);
			
			if (data!='failed')
			{
				
				$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Settings saved successfully.');
				
			
			}
			else
			{
				
				$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to save settings information.');
				
			}
			})
		}
		else{
			return false;
		}
}
}


