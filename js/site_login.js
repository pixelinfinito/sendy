 $(function () {
		  
		$('#loginForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            
            login_username: {
                message: 'Email is not valid',
                validators: {
                    notEmpty: {
                        message: 'Email Id cannot be empty'
                    },
                    
                }
            },
            login_password: {
                validators: {
                    notEmpty: {
                        message: 'Password cannot be empty'
                    },
                    different: {
                        field: 'login_username',
                        message: 'The password cannot be the same as email'
                    }
                },
				
            },
			
        }
		
    });
	
	$('#forgotForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            
            login_email: {
                message: 'Email ID is not valid',
                validators: {
                    notEmpty: {
                        message: 'Email ID cannot be empty'
                    },
                    
                }
            },
           
        }
		
    });
	
 });
  
  
  
 function SiteAuthController($scope, $http) {
    $scope.SignIn = function() {
     var validate=$('#loginForm').bootstrapValidator('validate');
		var f1=$('#login_username').val();
		var f2=$('#login_password').val();
		
		if(f1!='' && f2!=''){
		var parsePath=$(location).attr('href');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		$http.post(parsePath+'/authenticate', {'uname': $scope.login_username, 'pswd': $scope.login_password}
		).success(function(data) {
		 
		if (data!='failed')
		{
			//window.location="dashboard";
			window.location="sms/campaign";
			
		}
		else
		{
			$('#vPrompt').slideDown();
			$('#vPrompt').html("<span class='red'><i class='fa fa-info-circle'></i> Invalid email/password ..!</span>");
			setTimeout('eraseVprompt()',3000); 
		}
    })
    }
    else{
		$('#vPrompt').slideDown();	
		setTimeout('eraseVprompt()',3000); 
    	}
   	 }
}

 
 function ForgotController($scope, $http) {
    $scope.SubmitIn = function() {
     var validate=$('#forgotForm').bootstrapValidator('validate');
		var f1=$('#login_email').val();
		if(f1!=''){
		var parsePath=$(location).attr('href');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		$('#vPrompt').slideDown();
		$('#vPrompt').html("<i class='fa fa-spinner fa-spin'></i> Processing request..");
		$http.post(rCPath+'process', {'email_id': $scope.login_email}
		).success(function(msg) {
		
		if (msg!='failed')
		{
			$('#vPrompt').slideDown();
			$('#vPrompt').html(msg);
			
		}
		else
		{
			$('#vPrompt').slideDown();
			$('#vPrompt').html("<span class='text-danger'><i class='fa fa-exclamation'></i> Sorry your email id is invalid.</span>");
			setTimeout('eraseVprompt()',3000); 
		}
    })
    }
    else{
		$('#vPrompt').slideDown();	
		setTimeout('eraseVprompt()',3000); 
    	}
   	 }
}
    
function isValidEmailAddress(emailAddress) {
var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
return pattern.test(emailAddress);
}

function eraseVprompt(){
 $('#vPrompt').slideUp();
}