$(document).ready(function() {
    $('#assignCNForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            member_cn: {
                validators: {
                    notEmpty: {
                        message: 'Number cannot be empty'
                    },
					 
                }
            },
			
			
        }
		
    });
	
    
});
/*
var app = angular.module('CallerNumberApp', ['ui.bootstrap']);
app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

*/

function approveCIctrl($scope, $http){
	$scope.approveCIfn = function() {
		var cn_id=$scope.data.tid;
		
		var parsePath=$(location).attr('href');	
		var re = new RegExp(/^.*\//);
	    var rCPath= re.exec(window.location.href);
	
		$("#complexConfirm"+cn_id).confirm({
		title:"Approval confirmation",
		text:"Are you sure you want to approve?",
		confirm: function(button) {
		        window.location.reload();
				$http.post(rCPath+'approve_caller_id', {'cn_id': $scope.data.tid}
				).success(function(data) {
					alert('success');
					
				})
				
		},
		cancel: function(button) {
		  //alert("You aborted the operation.");
		},
		confirmButton: "Yes I am",
		cancelButton: "No"
		});

	}
}
function assignCnfCallerNumber() {
		
		var f1=$('#member_cn').val();
		var f2=$('#cnx_tid').val();
		
		if(f1!='' && f2!='' ){
		$('#cnProgress').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		$.ajax({
			type: "POST",
			url: rCPath+"assign_caller_number",
			data: {hidtId:f2,cn_number:f1}
			})
			.done(function(msg) {
                
				if(msg=='success'){
					$('#cnProgress').html('<i class="fa fa-check-circle darkGreen"></i> Caller number assigned successfully.');
				    window.location.reload();
						
				}else{
					$('#cnProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				}
			});

		
		}
		else{
			
			alert('* Missing mandatory fields.');
			return false;
		}
	
}

//// suspend caller number
function suspend_caller_number(tid,member_cn){
	    var f1=member_cn;
		var f2=tid;
		
		if(f1!='' && f2!='' ){
		var parsePath=$(location).attr('href');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		$('#dlx_cn').html(member_cn);
		
		$('#suspendCNModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {
		var suspend_remarks=$('#suspend_remarks').val();
		
		$('#cnSuspendStatus').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
        
		$.ajax({
			type: "POST",
			url: rCPath+"suspend_caller_number",
			data: {hidtId:f2,cn_number:f1,remarks:suspend_remarks}
			})
			.done(function(msg) {
               // alert(msg);
				if(msg=='success'){
					$('#cnSuspendStatus').html('<i class="fa fa-check-circle darkGreen"></i> Caller number suspended successfully.');
				    window.location.reload();
						
				}else{
					$('#cnSuspendStatus').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				}
			});
		});
		
		}
		else{
			
			alert('* Missing mandatory fields.');
			return false;
		}
	
}


//// suspend caller id
function suspendCallerId(tid,member_ci){
	 
	    var f1=member_ci;
		var f2=tid;
		
		if(f1!='' && f2!='' ){
		var parsePath=$(location).attr('href');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		$('#dlx_ci').html(member_ci);
		
		$('#suspendCiModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {
		var suspend_remarks=$('#suspend_remarks').val();
		
		$('#ciSuspendStatus').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
        
		$.ajax({
			type: "POST",
			url: rCPath+"suspend_caller_id",
			data: {hidtId:f2,cn_id:f1,remarks:suspend_remarks}
			})
			.done(function(msg) {
               // alert(msg);
				if(msg=='success'){
					$('#ciSuspendStatus').html('<i class="fa fa-check-circle darkGreen"></i> CallerID suspended successfully.');
				    window.location.reload();
						
				}else{
					$('#ciSuspendStatus').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				}
			});
		});
		
		}
		else{
			
			alert('* Missing mandatory fields.');
			return false;
		}
	
}
/*

app.controller('CNRequestCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	//alert(rCPath);
    $http.get(rCPath+'requests').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

var cidApp = angular.module('CallerIDApp', ['ui.bootstrap']);
cidApp.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

cidApp.controller('CIDRequestCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	//alert(rCPath);
    $http.get(rCPath+'load_ci_requests').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

/////////////// assign caller number controller ///////////////////
/*app.controller('assignCNctrl', function ($scope, $http, $timeout) {
	$scope.loadCNForm=function(){
		tid=$scope.data.tid;
		var parsePath=$(location).attr('href');	
		var re = new RegExp(/^.*\//);
	    var rCPath= re.exec(window.location.href);
	    
        $('#assignCNModal'+tid).modal('show');
		
	}
});
*/

function assign_caller_number(tid,name,country){
	$('#cnx_name').html(name);
	$('#cnx_tid').val(tid);
	$('#cnx_country').html(country);
	$('#assignCNModal').modal('show');
	
}

//// approving caller id
function approveCallerId(tid,ci_name,uid){
	    var parsePath=$(location).attr('href');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
	   
        $('#apx_ci').html(ci_name);
		
	    var f1=tid;
		
		//// checking caller id 
		$('#ci_availability').html('<i class="fa fa-spinner fa-spin"></i> Checking availability ..');
		$.ajax({
			type: "POST",
			url: rCPath+"check_caller_id",
			data: {ci_tag:ci_name,uid:uid}
			})
			.done(function(msg) {
                
				$('#ci_availability').html(msg);
		});
		
		
		
		if(f1!=''){
		
		$('#approveCiModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#approve', function() {
		$('#ciApproveStatus').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		
		$.ajax({
			type: "POST",
			url: rCPath+"approve_caller_id",
			data: {cn_id:f1}
			})
			.done(function(msg) {
                
				if(msg=='success'){
					$('#ciApproveStatus').html('<i class="fa fa-check-circle darkGreen"></i> CallerID approved successfully.');
				    window.location.reload();
						
				}else{
					$('#ciApproveStatus').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				}
			});
		});
		
		}
		else{
			
			alert('* Missing mandatory fields.');
			return false;
		}
	
}


/*

/////////////// Delete controller ///////////////////
app.controller('delCNController', function ($scope, $http, $timeout) {
	$scope.delCN=function(){
		cn_id=$scope.data.cn_id;
		var parsePath=$(location).attr('href');	
		var re = new RegExp(/^.*\//);
	    var rCPath= re.exec(window.location.href);
	
		$("#complexConfirm"+cn_id).confirm({
		title:"Delete confirmation",
		text:"Are you sure you want to delete?",
		confirm: function(button) {
		        window.location.reload();
				$http.post(rCPath+'delete_caller_number', {'cn_id': $scope.data.cn_id}
				).success(function(data) {
					alert('success');
					
				})
				
		},
		cancel: function(button) {
		  //alert("You aborted the operation.");
		},
		confirmButton: "Yes I am",
		cancelButton: "No"
		});

		
	}
});
*/