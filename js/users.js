<!--///////////////// Form validate /////////////////////-->
$(document).ready(function() {
    
	$('#userForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    },
					 
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    }
                }
            },
            user_name: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    },
                   
                }
            },
            email_id: {
                validators: {
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
			 user_groups: {
                validators: {
                    notEmpty: {
                        message: 'The user group cannot be empty'
                    },
				}
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The password must be more than 6 and less than 30 characters long'
                    },
                    different: {
                        field: 'user_name',
                        message: 'The password cannot be the same as username'
                    }
                },
				
            },
			
        }
		
    });
	
	$('#userUpForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    },
					 
                }
            },
            
            email_id: {
                validators: {
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
			 user_groups: {
                validators: {
                    notEmpty: {
                        message: 'The user group cannot be empty'
                    },
				}
            },
           
        }
		
    });

    
});



function inactive_member(uid,id){
	
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	$('#cnfStatusModal').modal({ backdrop: 'static', keyboard: false })
	.one('click', '#notify_btn', function() {

	$("#active_label"+id).html('<i class="fa fa-spinner fa-spin"></i> Inactivating..');
	var notify_status=$('#notify_status').val();
	
	$.ajax({
	type: "POST",
	url: rCPath+"inactive_member",
	data: {uid: uid,id: id,notify_status:notify_status}
	})
	.done(function(msg) {
		
		if(msg!='failed'){
			$('#active_placeholder'+id).html(msg);
			
		}else{
		    $('#active_placeholder'+id).html('Error');
			
		}
	});
	});	
}

function active_member(uid,id){
	
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	$('#cnfStatusModal').modal({ backdrop: 'static', keyboard: false })
	.one('click', '#notify_btn', function() {

	$("#inactive_label"+id).html('<i class="fa fa-spinner fa-spin"></i> Activating..');
	var notify_status=$('#notify_status').val();
	
	$.ajax({
	type: "POST",
	url: rCPath+"active_member",
	data: {uid: uid,id: id,notify_status:notify_status}
	})
	.done(function(msg) {
		
		if(msg!='failed'){
			$('#inactive_placeholder'+id).html(msg);
			
		}else{
		    $('#inactive_placeholder'+id).html('Error');
			
		}
	});
	});
}

function UserController($scope, $http) {
	                   
	   $scope.AddUser = function() {
		var validate=$('#userForm').bootstrapValidator('validate');
		var f1=$('#user_name').val();
		var f2=$('#password').val();
		var f3=$('#email_id').val();
		var f4=$('#first_name').val();
		var f5=$('#user_groups').val();
		
		if(f1!='' && f2!='' && f3!='' && f4!='' && f5 !=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		$http.post('add_process', {'user_name': $scope.user_name,'password': $scope.password,'first_name': $scope.first_name,'last_name': $scope.last_name,'email_id': $scope.email_id,'telephone': $scope.telephone,'telephone2': $scope.telephone2,'user_groups': $scope.user_groups,'user_status': $scope.user_status}
		).success(function(msg) {
			
		if (msg!='failed')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> User added successfully.');
			setTimeout('reloadPage()',1000);
			
		}
		else
		{
			
			$('#rsDiv').html('<span class="btn btn-warning btn-outline active"><i class="fa fa-times-circle red"></i> Something went wrong.</span>');
			
		}
		})
		}
		else
		{
			return false;
		}
	}
}

function UserUpController($scope, $http) {
	                   
	   $scope.UpdateUser = function() {
		var validate=$('#userUpForm').bootstrapValidator('validate');
		var f1=$('#email_id').val();
		var f2=$('#first_name').val();
		var f3=$('#user_groups').val();
		
		if(f1!='' && f2!='' && f3!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
			
		$http.post('update_process', {'user_id': $scope.user_id,'first_name': $scope.first_name,'last_name': $scope.last_name,'email_id': $scope.email_id,'telephone': $scope.telephone,'telephone2': $scope.telephone2,'user_groups': $scope.user_groups,'user_status': $scope.user_status}
		).success(function(data) {
			
		if (data.msg !='')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> User updated successfully.');
			setTimeout('reloadPage()',1000);
		}
		else
		{
			$('#rsDiv').html('<span class="btn btn-warning btn-outline active"><i class="fa fa-times-circle red"></i> Failed to update user.</span>');
			setTimeout('reloadPage()',1000);
		}
		})
		}
		else
		{
			return false;
		}
	}
		
}

 

var app = angular.module('UserApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('UsersCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
    $http.get(parsePath+'/all').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

//////////// Load user groups /////////////
function dyUserGroupCntrl($scope, $http){
    var user_group = $scope.data.user_group;
	var user_id    = $scope.data.user_id;
	var parsePath=$(location).attr('href');
    $http.post(parsePath+'/load_user_group', {'user_group': user_group}
			).success(function(data) {
				//alert(data);
			    $('.rLoadedGroup'+user_id).html(data);
				$('#dyLoadUserGroup'+user_id).css('display','none');
     })
 
}

/// delete user
function delUser(uid,uname){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#dlx_username').html(uname);
		$('#delUserModal').modal({ backdrop: 'static', keyboard: false })
		.one('click', '#delete', function() {
		
		$("#callDelStatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"users/delete_user",
		data: {user_id:uid}
		})
		.done(function(msg) {
            
			if(msg!='failed'){
				$("#callDelStatus").html('<i class="fa fa-check-circle"></i>'+uname+' deleted successfully');
				setTimeout('reloadPage()',1000);
			}else{
			    $("#callDelStatus").html('<i class="fa fa-times"></i> Something went wrong');
			}
		});

		});
}
function reloadPage(){
	window.location.reload();
}
/////////////// Delete controller ///////////////////
app.controller('delUsrController', function ($scope, $http, $timeout) {
	$scope.delUsr=function(){
		user_id=$scope.data.user_id;
		jConfirm('Are you sure you want to delete ?', 'Confirmation Alert', function(r) {
			if(r==true){
			  	$http.post('delete_process', {'user_id': $scope.data.user_id}
				).success(function(data) {
					jAlert('Delete status : ' + data.msg, 'Results');
					window.location.reload();
					
				})
			}
		    
		});
	}
});
///////////////// end //////////////////////////////