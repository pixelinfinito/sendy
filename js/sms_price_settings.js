$(document).ready(function() {
    
	$('#CRSettingsForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            sp_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    },
					 
                }
            },
			sp_currency_code: {
                validators: {
                    notEmpty: {
                        message: 'Currency code cannot be empty'
                    },
					 
                }
            },
			sp_price: {
                validators: {
                    notEmpty: {
                        message: 'Price cannot be empty'
                    },
					 
                }
            },
			
           
        }
		
    });
	
    
});


function SMSPriceCrtl($scope, $http) {
		$scope.AddSPSettings = function() {
		var validate=$('#SMSSettingsForm').bootstrapValidator('validate');
		var f1=$('#sp_country').val();
		var f2=$('#sp_currency_code').val();
		var f3=$('#sp_price').val();
		var f4=$('#sp_desc').val();
		var f5=$('#cr_hidId').val();
		
		
		if(f1!='' && f2!='' && f3!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		$http.post(rCPath+'process_sms_prices', {'hidId':f5, 'sp_country': f1,
		'sp_currency_code':f2,'sp_price':f3,'sp_desc':f4}
		).success(function(data) {
			//alert(data);
		
		if (data!='failed')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Settings saved successfully.');
			$('#cr_name').val('');
			$('#cr_symbol').val('');
			$('#cr_desc').val('');
		
			window.location.reload();
			
			
		
		}
		else
		{
			
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to save settings information.');
			
		}
		})
		}
		else{
			return false;
		}
	}
}

var app = angular.module('SMSPricesApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('LSMSCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_sms_prices').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});



/// delete sms price ajx
function delSPricing(p_id){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	$('#dlx_pid').html(p_id);

	//// process instant 
	$('#delSPriceModal').modal({ backdrop: 'static', keyboard: false })
	.one('click', '#delete', function() {


	$("#callDelStatus").html('<i class="fa fa-spinner fa-spin"></i> Deleting..');
	$.ajax({
	type: "POST",
	url: rCPath+"delete_sms_prices",
	data: {p_id:p_id}
	})
	.done(function(msg) {
		
		$("#callDelStatus").html('<i class="fa fa-check"></i> Deleted successfully');
		window.location.reload();
		
	});

	});	
	
	
}