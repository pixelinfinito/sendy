$(document).ready(function() {
    
	$('#PaymentSettingsForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            pp_email: {
                validators: {
                    notEmpty: {
                        message: 'Paypal email adress cannot be empty'
                    },
					 
                }
            },
			pp_return_url: {
                validators: {
                    notEmpty: {
                        message: 'Paypal return URL cannot be empty'
                    },
					 
                }
            },
			pp_cancel_url: {
                validators: {
                    notEmpty: {
                        message: 'Paypal cancel URL cannot be empty'
                    },
					 
                }
            },
			
			pp_payment_url: {
                validators: {
                    notEmpty: {
                        message: 'Paypal payment URL cannot be empty'
                    },
					 
                }
            },
			pp_wallet_email: {
                validators: {
                    notEmpty: {
                        message: 'Paypal wallet email cannot be empty'
                    },
					 
                }
            },
			pp_wallet_return_url: {
                validators: {
                    notEmpty: {
                        message: 'Paypal wallet return URL cannot be empty'
                    },
					 
                }
            },
			pp_wallet_cancel_url: {
                validators: {
                    notEmpty: {
                        message: 'Paypal wallet cancel URL cannot be empty'
                    },
					 
                }
            },

			co_seller_id: {
                validators: {
                    notEmpty: {
                        message: '2Co seller id is required'
                    },
					 
                }
            },
			
			
			co_base_url: {
                validators: {
                    notEmpty: {
                        message: '2Co base url is required'
                    },
					 
                }
            },
			
			
			co_sandbox_url: {
                validators: {
                    notEmpty: {
                        message: '2Co sandbox url is required'
                    },
					 
                }
            },
			
			co_private_key: {
                validators: {
                    notEmpty: {
                        message: '2Co private key is required'
                    },
					 
                }
            },
			
			co_publish_key: {
                validators: {
                    notEmpty: {
                        message: '2Co publishable key is required'
                    },
					 
                }
            },
			
			co_username: {
                validators: {
                    notEmpty: {
                        message: '2Co account username is required'
                    },
					 
                }
            },
			
			co_password: {
                validators: {
                    notEmpty: {
                        message: '2Co account password is required'
                    },
					 
                }
            },
			
        }
		
    });
	
    
});


function PaymentSettingsController($scope, $http) {
		$scope.AddPaymentSettings = function() {
		var validate=$('#PaymentSettingsForm').bootstrapValidator('validate');
		var f1=$('#pp_email').val();
		var f2=$('#pp_return_url').val();
		var f3=$('#pp_cancel_url').val();
		var f4=$('#pp_notify_url').val();
		var f5=$('#pp_payment_url').val();
		
		var f6=$('#pp_wallet_email').val();
		var f7=$('#pp_wallet_return_url').val();
		var f8=$('#pp_wallet_cancel_url').val();
		var f9=$('#pp_wallet_notify_url').val();
		
		var f10=$('#co_seller_id').val();
		var f11=$('#co_base_url').val();
		var f12=$('#co_sandbox_url').val();
		var f13=$('#co_private_key').val();
		
		var f14=$('#co_publish_key').val();
		var f15=$('#co_username').val();
		var f16=$('#co_password').val();
		var f17=$('#is_sandbox').val();
		
		
		if(f1!='' && f2!='' && f3!='' && f5!='' && f6!='' && f7!='' && f8!='' && f10!='' && f11!='' && f12!='' && f13!='' && f14!='' && f15!='' && f16!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		alert(rCPath);
		$http.post(rCPath+'process_payment_settings', {'pp_email':f1, 'pp_return_url': f2,'pp_cancel_url':f3,
		'pp_notify_url':f4,'pp_payment_url':f5,'pp_wallet_email':f6,'pp_wallet_return_url':f7,
		'pp_wallet_cancel_url':f8,'pp_wallet_notify_url':f9,'co_seller_id':f10,'co_base_url':f11,'co_sandbox_url':f12,'co_private_key':f13,'co_publish_key':f14,'co_username':f15,'co_password':f16,'is_sandbox':f17}
		).success(function(data) {
			//alert(data);
			
			if (data!='failed')
			{
				
				$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Settings saved successfully.');
				/*$('#thumb_reference').val(f2);
				
				$('#pTab1').removeClass("active");
				$('#pTab2').addClass("active");
				$('#pTab2').html('<a href="#tab_2" data-toggle="tab" >Thumbnail</a>');
				$('#tab_2').addClass("active");
				$('#tab_1').removeClass("active");
				*/
				
			
			}
			else
			{
				
				$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to save settings information.');
				
			}
			});
		}
		else{
			return false;
		}
	}
}

