
$(document).ready(function() {
	$('#transForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            amount: {
                validators: {
                    notEmpty: {
                        message: 'Amount field cannot be empty'
                    },
					 
                }
            },
            recipient_email: {
                message: 'Email is not valid',
                validators: {
                    notEmpty: {
                        message: 'Email field cannot be empty'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'Email must be more than 3 and less than 100 characters long'
                    },
                   /* regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    },*/
                   
                }
            },
			
        }
		
    });
	
	$('#transfer_btn').click(function(){
		var validate=$('#transForm').bootstrapValidator('validate');
		var amount=Number($('#amount').val());
		var accAmount= Number($('#accAmount').val());
		var currencyTag= $('#currencyTag').val();
		var recipient_email= $('#recipient_email').val();
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
	
		if(amount>accAmount){
			alert('Sorry your credit is low, you may transfer lessthan '+currencyTag+' '+accAmount);
			return false;
		}else{
			if(amount!='' && recipient_email!='')
			{
				$('#amt_tag').html(currencyTag+' '+amount);
				$('#cnfTranferAlert').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#cnf_confirm', function() {
					
					 
					$("#rsTransfer").html('<i class="fa fa-spinner fa-spin"></i> Started fund transfer ..');
					$.ajax({
					type: "POST",
					url: rCPath+"transfer_process",
					data: {amount:amount,recipient_email:recipient_email}
					})
					.done(function(msg) {
					   
						if(msg!='failed'){
							$("#rsTransfer").html('<i class="fa fa-check-circle"></i> Funds transfered successfully.');
							setTimeout('reloadPage()',1000);
						}else{
							$("#rsTransfer").html('<span class="text-danger"><i class="fa fa-times"></i>Transaction Failed /please cross check receiver\'s details.</span>');
						}
					});

		
				});
			}else{
				alert('Missing mandatory fields.');
			}
		}
			
	});	
});



/// delete enquiry
function delEnquiry(enq_id,name){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#dlx_recipient').html(name);
		
		$('#deleteEnqModal').modal({ backdrop: 'static', keyboard: false })
		.one('click', '#delete', function() {
		
		$("#callEnqStatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"delete_enquiry",
		data: {enquiry_id:enq_id}
		})
		.done(function(msg) {
           
			if(msg!='failed'){
				$("#callEnqStatus").html('<i class="fa fa-check-circle"></i>'+enq_id+' deleted successfully');
				setTimeout('reloadPage()',1000);
			}else{
			    $("#callEnqStatus").html('<i class="fa fa-times"></i> Something went wrong');
			}
		});

	});
}
    
function reloadPage(){
	window.location.reload();
}	


