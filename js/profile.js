

$(document).ready(function() {
    
	$('#preferencesForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
			payment_threshold: {
                validators: {
                    notEmpty: {
                        message: 'Payment threshold cannot be empty'
                    },
				}
            },
          
			
        }
		
    });

	
	$('#profilePasswordForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
			old_password: {
                validators: {
                    notEmpty: {
                        message: 'The current password is cannot be empty'
                    },
				}
            },
           new_password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
					stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The password must be more than 6 and less than 30 characters long'
                    },
                    identical: {
                        field: 'retype_password',
                        message: 'The password and its confirm are not the same'
                    },
                    different: {
                        field: 'old_password',
                        message: 'The password cannot be the same as username'
                    }
                }
            },
            retype_password: {
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and cannot be empty'
                    },
                    identical: {
                        field: 'new_password',
                        message: 'The password and its confirm are not the same'
                    },
                    different: {
                        field: 'old_password',
                        message: 'The password cannot be the same as username'
                    }
                }
            },
			
        }
		
    });

	$('#profileBasicInfoForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
			first_name: {
                validators: {
                    notEmpty: {
                        message: 'First name cannot be empty'
                    },
				}
            },
           last_name: {
                validators: {
                    notEmpty: {
                        message: 'Last name cannot be empty'
                    }
					
                }
            },
			
			gender: {
                validators: {
                    notEmpty: {
                        message: 'Gender cannot be empty'
                    }
					
                }
            },
			email_id: {
                validators: {
                    notEmpty: {
                        message: 'Email Id cannot be empty'
                    }
					
                }
            },
			address1: {
                validators: {
                    notEmpty: {
                        message: 'Address cannot be empty'
                    }
					
                }
            },
			
			city: {
                validators: {
                    notEmpty: {
                        message: 'City cannot be empty'
                    }
					
                }
            },
			
			phone: {
                validators: {
                    notEmpty: {
                        message: 'Mobile number cannot be empty'
                    }
					
                }
            },
			ad_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    }
					
                }
            },
            
            
        }
		
    });
	
	 $('#check_newsletters').click(function() {
		
	   if($("#check_newsletters").is(':checked')){
			$('#news_letters').val('1');
		} else {
			$('#news_letters').val('0');
		}
     });

	   
	
    
});


function profilePasswordCtrl($scope, $http) {
					
	$scope.passwordBtnIn = function() {
		var validate=$('#profilePasswordForm').bootstrapValidator('validate');
		var f1=$('#old_password').val();
		var f2=$('#new_password').val();
		var f3=$('#retype_password').val();

		if(f1!='' && f2!='' && f3!=''){
			return true;
		}
		else{
			return false;
		}
	}
	
}

function preferencesCtrl($scope, $http) {
					
	$scope.preferencesBtnIn = function() {
		var validate=$('#preferencesForm').bootstrapValidator('validate');
		var f1=$('#payment_threshold').val();
		

		if(f1!=''){
			return true;
		}
		else{
			return false;
		}
	}
	
}


function profileBasicInfoCtrl($scope, $http) {
					
	$scope.basicInfoBtnIn = function() {
		var validate=$('#profileBasicInfoForm').bootstrapValidator('validate');
		var f1=$('#first_name').val();
		var f2=$('#last_name').val();
		var f3=$('#gender').val();
		var f4=$('#email_id').val();
		var f5=$('#address1').val();
		var f6=$('#city').val();
		var f7=$('#phone').val();
		var f8=$('#ad_country').val();
		
		
		if(f1!='' && f2!='' && f3!='' && f4!='' && f5!='' && f6!='' && f7!='' && f8!=''){
			return true;
		}
		else{
			return false;
		}
	}
	
}
