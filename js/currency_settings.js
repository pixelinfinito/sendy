$(document).ready(function() {
    
	$('#CRSettingsForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            c_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    },
					 
                }
            },
			cr_name: {
                validators: {
                    notEmpty: {
                        message: 'Currency name cannot be empty'
                    },
					 
                }
            },
			
           
        }
		
    });
	
    
});


function CRSettingsController($scope, $http) {
		$scope.AddCRSettings = function() {
		var validate=$('#CRSettingsForm').bootstrapValidator('validate');
		var f1=$('#c_country').val();
		var f2=$('#cr_name').val();
		var f3=$('#cr_symbol').val();
		var f4=$('#cr_desc').val();
		var f5=$('#cr_hidId').val();
		
		
		if(f1!='' && f2!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		$http.post(rCPath+'process_currency_settings', {'hidId':f5, 'c_country': f1,
		'cr_name':f2,'cr_symbol':f3,'cr_desc':f4}
		).success(function(data) {
			//alert(data);
		
		if (data!='failed')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Settings saved successfully.');
			$('#cr_name').val('');
			$('#cr_symbol').val('');
			$('#cr_desc').val('');
		
			window.location.reload();
			
		
		}
		else
		{
			
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to save settings information.');
			
		}
		})
		}
		else{
			return false;
		}
	}
}

var app = angular.module('ACSettingsApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('CRSettingsCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_currency').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});



/// delete currency code 
function delCurrencyCode(c_id,c_name){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	$('#dlx_ccid').html(c_name);
    
	//// process instant 
	$('#delCCodeModal').modal({ backdrop: 'static', keyboard: false })
	.one('click', '#delete', function() {

	$("#callDelStatus").html('<i class="fa fa-spinner fa-spin"></i> Deleting..');
	$.ajax({
	type: "POST",
	url: rCPath+"delete_currency_process",
	data: {c_id:c_id}
	})
	.done(function(msg) {
		//alert(msg);
		$("#callDelStatus").html('<i class="fa fa-check"></i> Deleted successfully');
		window.location.reload();
		
	});

	});	
	
	
}