<!--///////////////// Form validate /////////////////////-->
$(document).ready(function() {
    
	$('#card_form').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            c_holder_name: {
                validators: {
                    notEmpty: {
                        message: 'Card holder name cannot be empty'
                    },
					 
                }
            },
            c_number: {
                validators: {
                    notEmpty: {
                        message: 'Card number cannot be empty'
                    }
                }
            },
			
           c_month: {
                validators: {
                    notEmpty: {
                        message: 'Card month cannot be empty'
                    }
                }
            },
			
			c_year: {
                validators: {
                    notEmpty: {
                        message: 'Card year cannot be empty'
                    }
                }
            },
			c_cvv: {
                validators: {
                    notEmpty: {
                        message: 'CVV cannot be empty'
                    }
                }
            },
			
			c_address1: {
                validators: {
                    notEmpty: {
                        message: 'Billing address cannot be empty'
                    }
                }
            },
			c_city: {
                validators: {
                    notEmpty: {
                        message: 'City cannot be empty'
                    }
                }
            },
			c_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    }
                }
            },
			
			
			
        }
		
    });
	

    
});

function McardController($scope, $http){
	                   
	   $scope.cardIn = function() {
		var validate=$('#card_form').bootstrapValidator('validate');
		var f1=$('#c_holder_name').val();
		var f2=$('#c_number').val();
		var f3=$('#c_month').val();
		var f4=$('#c_year').val();
		
		var f5=$('#c_cvv').val();
		var f6=$('#c_address1').val();
		var f7=$('#c_address2').val();
		var f8=$('#c_city').val();
		var f9=$('#c_country').val();
		var f10=$('#c_postal_code').val();
		
		
		if(f1!='' && f2!='' && f3!='' && f4!='' && f5!='' && f6!='' && f8!='' && f9!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Updating card information please wait..');
			
		$http.post('process_card', {'c_holder_name': f1,'c_number': f2,'c_month': f3,'c_year': f4,'c_cvv': f5
		,'c_address1': f6,'c_address2': f7,'c_city': f8,'c_country': f9,'c_postal_code': f10}
		).success(function(data) {
			
			switch(data){
			case 'success':
				$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Card information updated successfully.');
			break;
			
			case 'failed':
			    $('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to update card information.');
			break;
			
			case 'duplicated':
				$('#rsDiv').html('<i class="fa  fa-exclamation-triangle red"></i> The information already existed, you may update other card.');
			break;
			}
		
		
		})
		}
		else
		{
			return false;
		}
	}
		
}

 
