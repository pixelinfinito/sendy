<!--///////////////// Form validate /////////////////////-->
$(document).ready(function() {
  
 
	$('#promitionForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            notify_type: {
                validators: {
                    notEmpty: {
                        message: 'Promotion type cannot be empty'
                    },
					 
                }
            },
            notify_to: {
                validators: {
                    notEmpty: {
                        message: 'To recipients cannot be empty'
                    }
                }
            },
			
           notify_headline: {
                validators: {
                    notEmpty: {
                        message: 'Subject cannot be empty'
                    }
                }
            },
			
			
        }
		
    });
	
	$('#promitionSMSForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            notify_type: {
                validators: {
                    notEmpty: {
                        message: 'Promotion type cannot be empty'
                    },
					 
                }
            },
            notify_to: {
                validators: {
                    notEmpty: {
                        message: 'To recipients cannot be empty'
                    }
                }
            },
			
           notify_message: {
                validators: {
                    notEmpty: {
                        message: 'Message cannot be empty'
                    },
					 stringLength: {
                        min: 3,
                        max: 255,
                        message: 'Maximum 255 characters long'
                    },
                }
            },
			
			
        }
		
    });
	
	  
	 $("#link_delete_all").click(function() {
	    deleteSelectedPromotion();
	}); 

	 //// check all to fetch
	 $('#check_all_notify').click(function () {    
		
		 $('input[name="check_notify"]').prop('checked', this.checked);    
		 $.each($("input[name='check_notify']:checked"), function(){  
		   //alert($(this).val());
		    
		 });
	 });
	 
    
});

function PromitionController($scope, $http) {
					
	$scope.sendPromotionBtn = function() {
		var validate=$('#promitionForm').bootstrapValidator('validate');
		var f1=$('#notify_type').val();
		var f2=$('#notify_to').val();
		var f3=$('#notify_headline').val();

		if(f1!='' && f2!='' && f3!=''){
			return true;
		}
		else{
			return false;
		}
	}
	
}

function PromitionSMSController($scope, $http) {
					
	$scope.sendSMSPromotionBtn = function() {
		var validate=$('#promitionSMSForm').bootstrapValidator('validate');
		var f1=$('#notify_type').val();
		var f2=$('#notify_to').val();
		var f3=$('#notify_message').val();

		if(f1!='' && f2!='' && f3!=''){
			return true;
		}
		else{
			return false;
		}
	}
	
}

function loadMemberCredit(uid,currency){
	
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	if(uid!='all'){
		$("#member_credit").html('<i class="fa fa-spinner fa-spin"></i> <small>loading available funds..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"member_wallet_credit",
		data: {uid: uid}
		})
		.done(function(msg) {
			
			var sp_payment=msg.split('__');
			$('#wallet_tamt').val(sp_payment[0]);
			$('#wallet_bamt').val(sp_payment[1]);
			if(msg!='failed'){
				$("#member_credit").html('<i class="fa fa-money"></i> Available credit <b>'+currency+' '+sp_payment[1]+'</b>');
				
			}else{
			   $("#member_credit").html('<i class="fa fa-meh-o"></i> Available credit <b>'+currency+' 00.00</b>');
				
			}
		});
	}
}
 
/// load promotion body
function loadNotifyBody(ntf_id){
	
	$('#NotifyBodyModal').modal('show');
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
	$('#dyNotifyBody').html('<i class="fa fa-spinner fa-spin"></i> Loading please wait ..');
	$.ajax({
		type: "POST",
		url: rCPath+"load_promotion_message",
		data: {promotion_id:ntf_id}
		})
		.done(function(msg) {
		
			$('#dyNotifyBody').html(msg);
		
	});
		
} 

/// delete promotion
function delPromotion(pid,subject,to){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#dlx_recipient').html(to);
		$('#dlx_subject').val(subject);
		
		$('#delePromotionModal').modal({ backdrop: 'static', keyboard: false })
		.one('click', '#delete', function() {
		
		$("#callPromotionStatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"delete_promotion",
		data: {promotion_id:pid}
		})
		.done(function(msg) {
           
			if(msg!='failed'){
				$("#callPromotionStatus").html('<i class="fa fa-check-circle"></i>'+pid+' deleted successfully');
				setTimeout('reloadPage()',1000);
			}else{
			    $("#callPromotionStatus").html('<i class="fa fa-times"></i> Something went wrong');
			}
		});

	});
}

///// proceed to delete selected promotions
function deleteSelectedPromotion(){
	
	
	/* declare an checkbox array */
	var chkArray = [];
	
	var cFtVals="";
	/* look for all checkboxs that have a class 'chk' attached to it and check if it was checked */
	$(".check_notify:checked").each(function() {
		
		chkArray.push($(this).val());
		
	});
	
	/* we join the array separated by the comma */
	var depthSelected;
	depthSelected=chkArray.join(',') + ",";
	
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(depthSelected.length > 1){
		
		$('#del_notifycount').html(chkArray.length);
		$('#deleAllNotifyModal').modal({ backdrop: 'static', keyboard: false }).one('click', '#delete_all', function() {
		
	   
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
		   $("#del_status").html('<i class="fa fa-circle-o-notch fa-spin"></i> Deleting ..');
			$.ajax({
			type: "POST",
			url: rCPath+"delete_selected_promotions",
			data: {cIds:depthSelected}
			})
			.done(function(msg) {
				
				if(msg!='failed'){
					
					$('input[name="check_all_notify"]').prop('checked',false);  
					$('input[name="check_notify"]').prop('checked',false);  
					reloadPage();

				}else{
					
					$('input[name="check_all_notify"]').prop('checked',false);  
					$('input[name="check_notify"]').prop('checked',false);  
					reloadPage();
				}
			   
			});
	  });	
		
	}else{
		alert("Please select at least one of the checkbox");	
		
	}
	
}

function reloadPage(){
	window.location.reload();
}