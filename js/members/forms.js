<!--///////////////// Form validate /////////////////////-->
$(document).ready(function() {
    
	$('#blkMemberForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            member_id: {
                validators: {
                    notEmpty: {
                        message: 'Member field cannot be empty'
                    },
					 
                }
            },
            block_status: {
                validators: {
                    notEmpty: {
                        message: 'Block status cannot be empty'
                    }
                }
            },
			
           remarks: {
                validators: {
                    notEmpty: {
                        message: 'Remarks field cannot be empty'
                    }
                }
            },
			
			
        }
		
    });
	

    
});

function blkController($scope, $http) {
	                   
	   $scope.blkMemberIn = function() {
		var validate=$('#blkMemberForm').bootstrapValidator('validate');
		var f1=$('#member_id').val();
		var f2=$('#block_status').val();
		var f3=$('#remarks').val();
		var f4;
	
		if($('[type="checkbox"]').is(":checked")){
            f4="1";
        }else{
            f4="0";
        }
		
		if(f1!='' && f2!='' && f3!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
			
		$http.post('blackwhitelist_process', {'user_id': f1,'block_status': f2,'remarks': f3,'notify_mail': f4}
		).success(function(data) {
			
			//alert(data);
			switch(data){
				
			case 'success':
				if(f2==1){
					$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Member blacklisted successfully.');
				}else{
					$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Member whitelisted successfully.');
				}
			break;
			
			case 'failed':
			    if(f2==1){
					$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to blacklist the member.');
				}else{
					$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to whitelist the member.');
				}
			break;
			
			case 'notify_failed':
				if(f2==1){
					$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Member notification mail failed.');
				}else{
					$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				}
			break;
			}
		
		
		})
		}
		else
		{
			return false;
		}
	}
		
}

 
