
$(document).ready(function() {
    
	$('#depositMfForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            wallet_token: {
                validators: {
                    notEmpty: {
                        message: 'Wallet token cannot be empty'
                    },
					 
                }
            },
            member_id: {
                validators: {
                    notEmpty: {
                        message: 'Member field cannot be empty'
                    }
                }
            },
			deposit_amt: {
                validators: {
                    notEmpty: {
                        message: 'Amount field cannot be empty'
                    }
                }
            },
			
				
            },
			
        });
		
		$('#rollbackMfForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            wallet_token: {
                validators: {
                    notEmpty: {
                        message: 'Wallet token cannot be empty'
                    },
					 
                }
            },
            member_id: {
                validators: {
                    notEmpty: {
                        message: 'Member field cannot be empty'
                    }
                }
            },
			deposit_amt: {
                validators: {
                    notEmpty: {
                        message: 'Amount field cannot be empty'
                    }
                }
            },
			 transaction_id: {
                validators: {
                    notEmpty: {
                        message: 'Transaction Id cannot be empty'
                    }
                }
            },
			
				
            },
			
        });
		
		
});
	


//// deposit funds
function depositFundsController($scope, $http) {
	                  
	   $scope.depositIn = function() {
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		var validate=$('#depositMfForm').bootstrapValidator('validate');
		var wallet_token=$('#wallet_token').val();
		var member_id=$('#member_id').val();
		var deposit_amt=$('#deposit_amt').val();
		var payment_remarks=$('#payment_remarks').val();
		var wallet_tamt=$('#wallet_tamt').val();
		var wallet_bamt=$('#wallet_bamt').val();
		var currency_name=$('#currency_name').val();
		var txnid=$('#transaction_id').val();
		if(isNaN(deposit_amt)){
			alert('Deposit/refund amount field, should be a numeric value.');
			return false;
		}
		if(wallet_token!='' && member_id!='' && deposit_amt){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Transfering please wait..');
		var parsePath=$(location).attr('href');
		
		$http.post(rCPath+'process_deposit', {'wallet_token': wallet_token,'member_id': member_id,
		'deposit_amt': deposit_amt,'payment_remarks': payment_remarks,'transaction_id':txnid,'wallet_tamt':wallet_tamt
		,'wallet_bamt':wallet_bamt,'currency':currency_name}
		).success(function(data) {
			
			switch(data){
			case 'success':
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Funds transferred successfully.');
			window.location.reload();
			break;
			
			case 'failed':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Sorry your transaction failed.');
			window.location.reload();
			break;
			
			
		}
		
		});
		}
		else
		{
			return false;
		}
	}
}




//// rollback funds
function rollbackFundsController($scope, $http) {
	                  
	   $scope.rollbackIn = function() {
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		var validate=$('#rollbackMfForm').bootstrapValidator('validate');
		var wallet_token=$('#wallet_token').val();
		var member_id=$('#member_id').val();
		var payment_remarks=$('#payment_remarks').val();
		var txnid=$('#transaction_id').val();
		var deposit_amt=$('#deposit_amt').val();
		var wallet_tamt=$('#wallet_tamt').val();
		var wallet_bamt=$('#wallet_bamt').val();
		
		
		var notify_mail;
	
		if($('[type="checkbox"]').is(":checked")){
            notify_mail="1";
        }else{
            notify_mail="0";
        }
		
		
		if(wallet_token!='' && member_id!='' && txnid){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Transfering please wait..');
		var parsePath=$(location).attr('href');
		
		$http.post(rCPath+'process_rollback', {'wallet_token': wallet_token,'member_id': member_id,
		'payment_remarks': payment_remarks,'transaction_id':txnid,'notify_mail':notify_mail,'wallet_tamt':wallet_tamt
		,'wallet_bamt':wallet_bamt,'deposit_amt': deposit_amt
		}
		).success(function(data) {
			
			switch(data){
			case 'success':
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Funds rollback successful.');
			window.location.reload();
			break;
			
			case 'failed':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Sorry your transaction failed.');
			window.location.reload();
			break;
			
			case 'notfound':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Sorry transaction not found, please check the inputs.');
			//window.location.reload();
			break;
			
			case 'notsys':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Not authorized to rollback this transaction.');
			//window.location.reload();
			break;
			
			
		}
		
		});
		}
		else
		{
			return false;
		}
	}
}




//// send free credits
function freeCreditController($scope, $http) {
	                  
	   $scope.depositIn = function() {
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		var validate=$('#depositMfForm').bootstrapValidator('validate');
		var wallet_token=$('#wallet_token').val();
		var member_id=$('#member_id').val();
		var deposit_amt=$('#deposit_amt').val();
		var payment_remarks=$('#payment_remarks').val();
		var wallet_tamt=$('#wallet_tamt').val();
		var wallet_bamt=$('#wallet_bamt').val();
		var currency_name=$('#currency_name').val();
		var txnid=$('#transaction_id').val();
		if(isNaN(deposit_amt)){
			alert('Free credit amount field, should be a numeric value.');
			return false;
		}
		if(wallet_token!='' && member_id!='' && deposit_amt){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Transfering please wait..');
		var parsePath=$(location).attr('href');
		
		$http.post(rCPath+'process_free_credits', {'wallet_token': wallet_token,'member_id': member_id,
		'deposit_amt': deposit_amt,'payment_remarks': payment_remarks,'transaction_id':txnid,'wallet_tamt':wallet_tamt
		,'wallet_bamt':wallet_bamt,'currency':currency_name}
		).success(function(data) {
			
			switch(data){
			case 'success':
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Funds transferred successfully.');
			window.location.reload();
			break;
			
			case 'statusfailed':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Credits transferred success, but failed to update the status.');
			//window.location.reload();
			break;
			
			case 'failed':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Sorry your transaction failed.');
			window.location.reload();
			break;
			
			
		}
		
		});
		}
		else
		{
			return false;
		}
	}
}

function loadMemberCredit(uid,currency){
	
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	$("#member_credit").html('<i class="fa fa-spinner fa-spin"></i> <small>loading available funds..</small>');
	$.ajax({
	type: "POST",
	url: rCPath+"member_wallet_credit",
	data: {uid: uid}
	})
	.done(function(msg) {
		
        var sp_payment=msg.split('__');
		$('#wallet_tamt').val(sp_payment[0]);
		$('#wallet_bamt').val(sp_payment[1]);
		if(msg!='failed'){
			$("#member_credit").html('<i class="fa fa-dollar"></i> Available credit <b>'+currency+' '+sp_payment[1]+'</b>');
			
		}else{
		   $("#member_credit").html('<i class="fa fa-meh-o"></i> Available credit <b>'+currency+' 00.00</b>');
			
		}
	});
}