function triggerPaymentInfoModal(txtAmount,txnOrderNo,txnRemarks,txnId){
	
	$('#paymentMoreInfoModal').modal('show');
	$('#txn_orderno').html('Order No: '+txnOrderNo);
	$('#txn_remarks').html(txnRemarks);
	$('#txt_amount').html(txtAmount);
	$('#txt_id').html('<i class="fa fa-tags"></i> Transaction ID: <strong>'+txnId+'</strong>');
}