
function inactive_member(uid,id){
	
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	$('#cnfStatusModal').modal({ backdrop: 'static', keyboard: false })
	.one('click', '#notify_btn', function() {

	$("#active_label"+id).html('<i class="fa fa-spinner fa-spin"></i> Inactivating..');
	var notify_status=$('#notify_status').val();
	
	$.ajax({
	type: "POST",
	url: rCPath+"inactive_member",
	data: {uid: uid,id: id,notify_status:notify_status}
	})
	.done(function(msg) {
		
		if(msg!='failed'){
			$('#active_placeholder'+id).html(msg);
			
		}else{
		    $('#active_placeholder'+id).html('Error');
			
		}
	});
	});	
}

function active_member(uid,id){
	
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	$('#cnfStatusModal').modal({ backdrop: 'static', keyboard: false })
	.one('click', '#notify_btn', function() {

	$("#inactive_label"+id).html('<i class="fa fa-spinner fa-spin"></i> Activating..');
	var notify_status=$('#notify_status').val();
	
	$.ajax({
	type: "POST",
	url: rCPath+"active_member",
	data: {uid: uid,id: id,notify_status:notify_status}
	})
	.done(function(msg) {
		
		if(msg!='failed'){
			$('#inactive_placeholder'+id).html(msg);
			
		}else{
		    $('#inactive_placeholder'+id).html('Error');
			
		}
	});
	});
}

/// delete member
function delMember(id,uid,uname){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#dlx_member').html(uname);
		$('#deleMemberModal').modal({ backdrop: 'static', keyboard: false })
		.one('click', '#delete', function() {
		
		$("#callMemberdelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"delete_member",
		data: {uid:uid,id:id}
		})
		.done(function(msg) {
           
			if(msg!='failed'){
				$("#callMemberdelstatus").html('<i class="fa fa-check-circle"></i>'+uname+' deleted successfully');
				setTimeout('reloadPage()',1000);
			}else{
			    $("#callMemberdelstatus").html('<i class="fa fa-times"></i> Something went wrong');
			}
		});

		});
}
function reloadPage(){
	window.location.reload();
}
