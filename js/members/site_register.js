<!--///////////////// Form validate /////////////////////-->
$(document).ready(function() {
    
	$('#siteRegisterForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ac_first_name: {
                validators: {
                    notEmpty: {
                        message: 'First name is required, it cannot be empty'
                    },
					 
                }
            },
            ac_last_name: {
                validators: {
                    notEmpty: {
                        message: 'Last name is required, it cannot be empty'
                    }
                }
            },
			ac_country: {
                validators: {
                    notEmpty: {
                        message: 'Country is required, it cannot be empty'
                    }
                }
            },
			ac_phone: {
                validators: {
                    notEmpty: {
                        message: 'Phone number cannot be empty'
                    }
                }
            },
			
            ac_email: {
                message: 'Email is not valid',
                validators: {
                    notEmpty: {
                        message: 'Email is required, it cannot be empty'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'Email must be more than 3 and less than 100 characters long'
                    },
                   /* regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    },*/
                   
                }
            },
           
			 ac_class: {
                validators: {
                    notEmpty: {
                        message: 'Account type cannot be empty'
                    },
				}
            },
            ac_password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The password must be more than 6 and less than 30 characters long'
                    },
                    different: {
                        field: 'ac_email',
                        message: 'The password cannot be the same as email'
                    }
                },
				
            },
			
        }
		
    });
	
	$('#updateMemberForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ac_first_name: {
                validators: {
                    notEmpty: {
                        message: 'First name is required, it cannot be empty'
                    },
					 
                }
            },
            ac_last_name: {
                validators: {
                    notEmpty: {
                        message: 'Last name is required, it cannot be empty'
                    }
                }
            },
			ac_country: {
                validators: {
                    notEmpty: {
                        message: 'Country is required, it cannot be empty'
                    }
                }
            },
			ac_phone: {
                validators: {
                    notEmpty: {
                        message: 'Phone number cannot be empty'
                    }
                }
            },
			
			 ac_class: {
                validators: {
                    notEmpty: {
                        message: 'Account type cannot be empty'
                    },
				}
            },
           
        }
		
    });
	

    
});


function SiteRegisterController($scope, $http) {
	                  
	   $scope.RegisterUser = function() {
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		var validate=$('#siteRegisterForm').bootstrapValidator('validate');
		var ac_first_name=$('#ac_first_name').val();
		var ac_last_name=$('#ac_last_name').val();
		var ac_phone=$('#ac_phone').val();
		var ac_country=$('#ac_country').val();
		var ac_class =$('#ac_class').val();
		var created_by =$('#created_by').val();
		var ac_active_status =$('#ac_active_status').val();
		
		var gender_sel = $("input[type='radio'][name='ac_gender']:checked");
		
	    var ac_gender=gender_sel.val();
	  
		var ac_about=$('#ac_about').val();
		var ac_email=$('#ac_email').val();
		var ac_password=$('#ac_password').val();
		
		
		if(ac_first_name!='' && ac_last_name!='' && ac_class!='' && ac_phone!='' && ac_country!='' && ac_email!='' && ac_password!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		$http.post(rCPath+'register_process', {'ac_first_name': ac_first_name,'ac_last_name': ac_last_name,
		'ac_phone': ac_phone,'ac_country': ac_country,'ac_class': ac_class,
		'ac_gender': ac_gender,'ac_about': ac_about,'ac_email': ac_email,'ac_password': ac_password,
		'created_by': created_by,'ac_active_status': ac_active_status}
		).success(function(data) {
			
			switch(data){
			case 'success':
			
			if(ac_active_status!=1){
				$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i>Member registration is success & an activation link sent to given email.');
			}else{
				$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i>Member registered successfully & activated.');
			}
			window.location.reload();
			break;
			
			case 'failed':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Registration failed.');
			window.location.reload();
			break;
			
			case 'duplicated':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Email is already existed, please try again.');
			break;
		}
		
		});
		}
		else
		{
			return false;
		}
	}
}

function updateMemberController($scope, $http) {
	                  
	   $scope.UpdateMember = function() {
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		var validate=$('#updateMemberForm').bootstrapValidator('validate');
		var ac_first_name=$('#ac_first_name').val();
		var ac_last_name=$('#ac_last_name').val();
		var ac_phone=$('#ac_phone').val();
		var ac_country=$('#ac_country').val();
		var ac_class =$('#ac_class').val();
		var gender_sel = $("input[type='radio'][name='ac_gender']:checked");
		var ac_gender=gender_sel.val();
	    var ac_about=$('#ac_about').val();
		var member_id=$('#member_id').val();
		
		if(ac_first_name!='' && ac_last_name!='' && ac_class!='' && ac_phone!='' && ac_country!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		$http.post(rCPath+'update_process', {'ac_first_name': ac_first_name,'ac_last_name': ac_last_name,
		'ac_phone': ac_phone,'ac_country': ac_country,'ac_class': ac_class,
		'ac_gender': ac_gender,'ac_about': ac_about,'user_id': member_id}
		).success(function(data) {
			
			switch(data){
			case 'success':
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i>Member information updated successfully.');
			window.location.reload();
			break;
			
			case 'failed':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Member updations failed.');
			window.location.reload();
			break;
			
		}
		
		});
		}
		else
		{
			return false;
		}
	}
}

