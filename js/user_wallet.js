

//// credit request 
function fCreditReqCtrl() {
	          
        var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		//alert(rCPath);
		var member_name=$('#member_name').val();
		var request_comments=$('#request_comments').val();
		if(member_name!=''){
		 $("#rsDiv").html('<i class="fa fa-spinner fa-spin"></i> Sending request please wait..');
				$.ajax({
				type: "POST",
				url: rCPath+"/dashboard/free_credit_request",
				data: {member_name:member_name,request_comments:request_comments}
				})
				.done(function( msg ) {
					//alert(msg);
					switch(msg){
					case 'success':
					$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Your request sent successfully.');
					window.location.reload();
					break;
					
					case 'failed':
					$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Sorry your request failed.');
					window.location.reload();
					break;
					
					case 'duplicated':
					$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Sorry you already requested.');
					//window.location.reload();
					break;
					}
			});
            
		}
		
		else{
			alert('Something went wrong.');
		}
}

/*
var app = angular.module('WalletApp', ['ui.bootstrap']);
app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('WalletLogCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_history').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

*/
function fCreditRequestForm(){
	$('#creditRequestModal').modal('show');
}

function callBtUpgradeModal(){
$('#modalConfirmUpgrade').modal({
        show: 'true'
    }); 
}


