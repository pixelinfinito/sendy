<!--///////////////// Form validate /////////////////////-->
$(document).ready(function() {
  
 
	$('#articleForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            article_type: {
                validators: {
                    notEmpty: {
                        message: 'Article type cannot be empty'
                    },
					 
                }
            },
            article_headline: {
                validators: {
                    notEmpty: {
                        message: 'Article title cannot be empty'
                    }
                }
            },
			
           article_body: {
                validators: {
                    notEmpty: {
                        message: 'Article content be empty'
                    }
                }
            },
			
			
        }
		
    });
	
	
    
});

function ArticleController($scope, $http) {
					
	$scope.articleBtn = function() {
		var validate=$('#articleForm').bootstrapValidator('validate');
		var f1=$('#article_type').val();
		var f2=$('#article_headline').val();
		var f3=$('#article_body').val();

		if(f1!='' && f2!='' && f3!=''){
			return true;
		}
		else{
			return false;
		}
	}
	
}

/// load notification body modal
function loadArticleBody(article_id){
	
	$('#NotifyBodyModal').modal('show');
	///// load msg , change read status
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
	$('#dyNotifyBody').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
	$.ajax({
		type: "POST",
		url: rCPath+"load_notification",
		data: {notify_id:ntf_id}
		})
		.done(function(msg) {
		 $('#dyNotifyBody').html(msg);
		
	});
		
} 

/// delete article
function delArticle(ntf_id,subject,to){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#dlx_recipient').html(to);
		$('#dlx_subject').val(subject);
		
		$('#deleNotifyModal').modal({ backdrop: 'static', keyboard: false })
		.one('click', '#delete', function() {
		
		$("#callNotifyStatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"delete_notification",
		data: {notify_id:ntf_id}
		})
		.done(function(msg) {
           
			if(msg!='failed'){
				$("#callNotifyStatus").html('<i class="fa fa-check-circle"></i>'+ntf_id+' deleted successfully');
				setTimeout('reloadPage()',1000);
			}else{
			    $("#callNotifyStatus").html('<i class="fa fa-times"></i> Something went wrong');
			}
		});

	});
}


function reloadPage(){
	window.location.reload();
}
