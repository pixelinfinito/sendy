<!--///////////////// Form validate /////////////////////-->
$(document).ready(function() {
    
	$('#userGroupForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            group_name: {
                validators: {
                    notEmpty: {
                        message: 'The group name is required and cannot be empty'
                    },
					 
                }
            },
           
        }
		
    });
	
	$('#UpUserGroupForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            group_name: {
                validators: {
                    notEmpty: {
                        message: 'The group name is required and cannot be empty'
                    },
					 
                }
            },
           
        }
		
    });

    
});
<!--///////////////////// End ///////////////////////////-->


function UGController($scope, $http) {
		$scope.AddIn = function() {
		var validate=$('#userGroupForm').bootstrapValidator('validate');
		var f1=$('#group_name').val();
		
		if(f1!=''){
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
				
		$http.post('add_process', {'ug_name': $scope.group_name, 'ug_desc': $scope.group_desc}
		).success(function(data) {
		if (data!='failed')
		{
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Usergroup added successfully.');
			setTimeout('reloadPage()',1000);
			
		}
		else
		{
			$('#rsDiv').html('<span class="btn btn-warning btn-outline active"><i class="fa fa-times-circle red"></i> Failed to add usergroup.</span>');
			setTimeout('reloadPage()',1000);
		}
	})
	 }
		else
		{
			return false;
		}
	}
}

function UpUserGropupController($scope, $http) {
		$scope.UpdateUserGroup = function() {
		var validate=$('#UpUserGroupForm').bootstrapValidator('validate');
		var f1=$('#group_name').val();
		
		if(f1!=''){
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
				
		$http.post('update_process', {'group_id': $scope.group_id,'ug_name': $scope.group_name, 'ug_desc': $scope.group_desc}
		).success(function(data) {
			
		if (data!='failed')
		{
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Usergroup updated successfully.');
			setTimeout('reloadPage()',1000);
			
		}
		else
		{
			$('#rsDiv').html('<span class="btn btn-warning btn-outline active"><i class="fa fa-times-circle red"></i> Failed to update usergroup.</span>');
			setTimeout('reloadPage()',1000);
		}
	})
	 }
		else
		{
			return false;
		}
	}
}
/// delete group
function delGroup(gid,gname){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#dlx_grpname').html(gname);
		$('#delUserGroupModal').modal({ backdrop: 'static', keyboard: false })
		.one('click', '#delete', function() {
		
		$("#callDelStatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"user_groups/delete_group",
		data: {group_id:gid}
		})
		.done(function(msg) {
            
			if(msg!='failed'){
				$("#callDelStatus").html('<i class="fa fa-check-circle"></i>'+gname+' deleted successfully');
				setTimeout('reloadPage()',1000);
			}else{
			    $("#callDelStatus").html('<i class="fa fa-times"></i> Something went wrong');
			}
		});

		});
}

function reloadPage(){
	window.location.reload();
}

var app = angular.module('myApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('customersCrtl', function ($scope, $http, $timeout) {
	
    $http.get('all').success(function(data){
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

/////////////// Delete controller ///////////////////
app.controller('delUgController', function ($scope, $http, $timeout) {
	$scope.delUgp=function(){
		group_id=$scope.data.group_id;
		jConfirm('Are you sure you want to delete ?', 'Confirmation Alert', function(r) {
			if(r==true){
			  	$http.post('delete_process', {'group_id': $scope.data.group_id}
				).success(function(data) {
					jAlert('Delete status : ' + data.msg, 'Results');
					window.location.reload();
					
				})
			}
		    
		});
	}
});
///////////////// end //////////////////////////////
