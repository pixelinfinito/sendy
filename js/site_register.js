<!--///////////////// Form validate /////////////////////-->
$(document).ready(function() {
    
	$('#siteRegisterForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ac_first_name: {
                validators: {
                    notEmpty: {
                        message: 'First name is required, it cannot be empty'
                    },
					 
                }
            },
            ac_last_name: {
                validators: {
                    notEmpty: {
                        message: 'Last name is required, it cannot be empty'
                    }
                }
            },
			ac_country: {
                validators: {
                    notEmpty: {
                        message: 'Country is required, it cannot be empty'
                    }
                }
            },
			reg_terms: {
                validators: {
                    notEmpty: {
                        message: 'Please check the option upon your read status'
                    }
                }
            },
			
			
            ac_email: {
                message: 'Email is not valid',
                validators: {
                    notEmpty: {
                        message: 'Email is required, it cannot be empty'
                    },
                    stringLength: {
                        min: 3,
                        max: 100,
                        message: 'Email must be more than 3 and less than 100 characters long'
                    },
                   /* regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    },*/
                   
                }
            },
            email_id: {
                validators: {
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
			 ac_class: {
                validators: {
                    notEmpty: {
                        message: 'Account type cannot be empty'
                    },
				}
            },
            ac_password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The password must be more than 6 and less than 30 characters long'
                    },
                    different: {
                        field: 'ac_email',
                        message: 'The password cannot be the same as email'
                    }
                },
				
            },
			
        }
		
    });
	

    
});
<!--///////////////////// End ///////////////////////////-->


function SiteRegisterController($scope, $http) {
	                  
	   $scope.RegisterUser = function() {
		
		var validate=$('#siteRegisterForm').bootstrapValidator('validate');
		var ac_first_name=$('#ac_first_name').val();
		var ac_last_name=$('#ac_last_name').val();
		var ac_phone=$('#ac_phone').val();
		var ac_country=$('#ac_country').val();
		var ac_class =$('#ac_class').val();
		
		var gender_sel = $("input[type='radio'][name='ac_gender']:checked");
		
	    var ac_gender=gender_sel.val();
	  
		var ac_about=$('#ac_about').val();
		var ac_email=$('#ac_email').val();
		var ac_password=$('#ac_password').val();
		
		
		if(ac_first_name!='' && ac_last_name!='' && ac_class!='' && ac_country!='' && ac_email!='' && ac_password!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		$http.post(parsePath+'/register_process', {'ac_first_name': ac_first_name,'ac_last_name': ac_last_name,
		'ac_phone': ac_phone,'ac_country': ac_country,'ac_class': ac_class,
		'ac_gender': ac_gender,'ac_about': ac_about,'ac_email': ac_email,'ac_password': ac_password}
		).success(function(data) {
		   
		   switch(data){
			case 'success':
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i>Your registration is success, please check your email and activate your account.');
			window.location.reload();
			break;
			
			case 'failed':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Registration failed.');
			window.location.reload();
			break;
			
			case 'duplicated':
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Email is already existed, please try again.');
			break;
		   }
		});
		}
		else
		{
			return false;
		}
	}
}

function UserUpController($scope, $http) {
	                   
	   $scope.UpdateUser = function() {
		var validate=$('#userUpForm').bootstrapValidator('validate');
		var f1=$('#email_id').val();
		var f2=$('#first_name').val();
		var f3=$('#user_groups').val();
		
		if(f1!='' && f2!='' && f3!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
			
		$http.post('update_process', {'user_id': $scope.user_id,'first_name': $scope.first_name,'last_name': $scope.last_name,'email_id': $scope.email_id,'telephone': $scope.telephone,'telephone2': $scope.telephone2,'user_groups': $scope.user_groups,'user_status': $scope.user_status}
		).success(function(data) {
			
		if (data.msg !='')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> User updated successfully.');
			
		}
		else
		{
			$('#rsDiv').html('<span class="btn btn-warning btn-outline active"><i class="fa fa-times-circle red"></i> Failed to update user.</span>');
			
		}
		})
		}
		else
		{
			return false;
		}
	}
		
}

 

var app = angular.module('UserApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('UsersCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
    $http.get(parsePath+'/all').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

//////////// Load user groups /////////////
function dyUserGroupCntrl($scope, $http){
    var user_group = $scope.data.user_group;
	var user_id    = $scope.data.user_id;
	var parsePath=$(location).attr('href');
    $http.post(parsePath+'/load_user_group', {'user_group': user_group}
			).success(function(data) {
				//alert(data);
			    $('.rLoadedGroup'+user_id).html(data);
				$('#dyLoadUserGroup'+user_id).css('display','none');
     })
 
}

/////////////// Delete controller ///////////////////
app.controller('delUsrController', function ($scope, $http, $timeout) {
	$scope.delUsr=function(){
		user_id=$scope.data.user_id;
		jConfirm('Are you sure you want to delete ?', 'Confirmation Alert', function(r) {
			if(r==true){
			  	$http.post('delete_process', {'user_id': $scope.data.user_id}
				).success(function(data) {
					jAlert('Delete status : ' + data.msg, 'Results');
					window.location.reload();
					
				})
			}
		    
		});
	}
});
///////////////// end //////////////////////////////