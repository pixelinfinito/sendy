$(document).ready(function() {
    
	$('#AppSettingsForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            app_timezone: {
                validators: {
                    notEmpty: {
                        message: 'Timezone settings cannot be empty'
                    },
					 
                }
            },
			app_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    },
					 
                }
            },
			app_currency: {
                validators: {
                    notEmpty: {
                        message: 'Currency cannot be empty'
                    },
					 
                }
            },
			app_from_mail: {
                validators: {
                    notEmpty: {
                        message: 'Default From Mail cannot be empty'
                    },
					 
                }
            },
			app_default_name: {
                validators: {
                    notEmpty: {
                        message: 'DefaultApp Name cannot be empty'
                    },
					 
                }
            },
			app_mobile_number: {
                validators: {
                    notEmpty: {
                        message: 'Default Mobile Number cannot be empty'
                    },
					 
                }
            },
			smtp_host: {
                validators: {
                    notEmpty: {
                        message: 'SMTP Hostname cannot be empty'
                    },
					 
                }
            },
			smtp_port: {
                validators: {
                    notEmpty: {
                        message: 'SMTP Port cannot be empty'
                    },
					 
                }
            },
           
        }
		
    });
	
    
});


function AppSettingsController($scope, $http) {
		$scope.AddAppSettings = function() {
		var validate=$('#AppSettingsForm').bootstrapValidator('validate');
		var f1=$('#app_timezone').val();
		var f2=$('#app_country').val();
		var f3=$('#app_currency').val();
		
		var app_from_mail=$('#app_from_mail').val();
		var app_mobile_number=$('#app_mobile_number').val();
		var app_default_name =$('#app_default_name').val();
		var smtp_host=$('#smtp_host').val();
		var smtp_username=$('#smtp_username').val();
		var smtp_password=$('#smtp_password').val();
		var smtp_port=$('#smtp_port').val();
		var app_support_number = $('#app_support_number').val();
		var app_support_email = $('#app_support_email').val();
		
		var app_upgrade_option = $('#app_upgrade_option').val();
		var app_helptext = $('#app_helptext').val();
		var app_branded_by = $('#app_branded_by').val();
		var app_copyrights_text = $('#app_copyrights_text').val();
		var fb_link = $('#fb_link').val();
		var twitter_link = $('#twitter_link').val();
		var youtube_link = $('#youtube_link').val();
		
		if(f1!='' && f2!='' && f3!='' && smtp_host!='' && smtp_port!='' && app_default_name!='' && app_mobile_number!='' && app_from_mail!='' ){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		$http.post(rCPath+'process_app_settings', {'app_timezone':f1, 'app_country': f2,'app_currency':f3,
		'smtp_host':smtp_host,'smtp_username':smtp_username,'smtp_password':smtp_password,
		'smtp_port':smtp_port,'app_from_mail':app_from_mail,'app_default_name':app_default_name,
		'app_mobile_number':app_mobile_number,'app_support_number':app_support_number,'app_support_email':app_support_email
		,'app_upgrade_option':app_upgrade_option,'app_helptext':app_helptext,'app_branded_by':app_branded_by
		,'app_copyrights_text':app_copyrights_text,'fb_link':fb_link,'twitter_link':twitter_link,'youtube_link':youtube_link}
		).success(function(data) {
			//alert(data);
		
		if (data!='failed')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Settings saved successfully.');
			
		}
		else
		{
			
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to save settings information.');
			
		}
		})
		}
		else{
			return false;
		}
	}
}

