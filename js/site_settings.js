$(document).ready(function() {
    
	$('#SiteSettingsForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            site_ccpayment: {
                validators: {
                    notEmpty: {
                        message: 'Card payment field cannot be empty'
                    },
					 
                }
            },
			site_paypal: {
                validators: {
                    notEmpty: {
                        message: 'Paypal payment field cannot be empty'
                    },
					 
                }
            },
			site_bidexpire: {
                validators: {
                    notEmpty: {
                        message: 'Bid Expire field cannot be empty'
                    },
					 
                }
            },
			site_image_uploads: {
                validators: {
                    notEmpty: {
                        message: 'Image upload field cannot be empty'
                    },
					 
                }
            },
			site_wallet: {
                validators: {
                    notEmpty: {
                        message: 'Wallet payment cannot be empty'
                    },
					 
                }
            },
			site_userkey: {
                validators: {
                    notEmpty: {
                        message: 'User keygen cannot be empty'
                    },
					 
                }
            },
			
			
			vcode_min_val: {
                validators: {
                    notEmpty: {
                        message: 'Min value cannot be empty'
                    },
					 
                }
            },
			vcode_max_val: {
                validators: {
                    notEmpty: {
                        message: 'Max value cannot be empty'
                    },
					 
                }
            },
			
			vcode_string: {
                validators: {
                    notEmpty: {
                        message: 'String value cannot be empty'
                    },
					 
                }
            },
			
			wallet_min_val: {
                validators: {
                    notEmpty: {
                        message: 'Wallet min value cannot be empty'
                    },
					 
                }
            },
			wallet_max_val: {
                validators: {
                    notEmpty: {
                        message: 'Wallet max value cannot be empty'
                    },
					 
                }
            },
			
			wallet_string: {
                validators: {
                    notEmpty: {
                        message: 'Wallet string value cannot be empty'
                    },
					 
                }
            },
			
           
        }
		
    });
	
    
});


function SiteSettingsController($scope, $http) {
		$scope.AddSiteSettings = function() {
		var validate=$('#SiteSettingsForm').bootstrapValidator('validate');
		var f1=$('#site_ccpayment').val();
		var f2=$('#site_paypal').val();
		var f3=$('#site_bidexpire').val();
		
		var image_uploads=$('#site_image_uploads').val();
		var sms_panel=$('#site_smspanel').val();
		var email_panel=$('#site_emailpanel').val();
		var payad_expire_days=$('#site_payad_expires').val();
		
		var vcode_min_val=$('#vcode_min_val').val();
		var vcode_max_val=$('#vcode_max_val').val();
		var vcode_string=$('#vcode_string').val();
		
		var wallet_min_val=$('#wallet_min_val').val();
		var wallet_max_val=$('#wallet_max_val').val();
		var wallet_string=$('#wallet_string').val();
		
		
		var iAdLimit=$('#individual_ad_limit').val();
		var bAdLimit=$('#business_ad_limit').val();
		var sbImageUploads=$('#site_business_image_uploads').val();
		var site_wallet=$('#site_wallet').val();
		var site_userkey =$('#site_userkey').val();
		var bid_refresh_time =$('#bid_refresh_time').val();
		
		
		
		if(f1!='' && f2!='' && f3!='' && image_uploads!='' && site_wallet!='' && wallet_string!='' 
		&& wallet_min_val!='' && wallet_max_val!='' && vcode_min_val!='' && vcode_max_val!='' && vcode_string!='' && site_userkey!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		$http.post(rCPath+'process_site_settings', {'allow_card':f1, 'allow_paypal': f2,'bid_expire_days':f3,
		'image_uploads':image_uploads,'sms_panel':sms_panel,'email_panel':email_panel,
		'payad_expire_days':payad_expire_days,'vcode_min_val':vcode_min_val,'vcode_max_val':vcode_max_val,
		'vcode_string':vcode_string,'iAdLimit':iAdLimit,'bAdLimit':bAdLimit,
		'sbImageUploads':sbImageUploads,'site_wallet':site_wallet,'wallet_min_val':wallet_min_val,
		'wallet_max_val':wallet_max_val,'wallet_string':wallet_string,'user_keygen':site_userkey,'bid_refresh_time':bid_refresh_time}
		).success(function(data) {
			//alert(data);
		
		if (data!='failed')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Settings saved successfully.');
			
		
		}
		else
		{
			
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to save settings information.');
			
		}
		})
		}
		else{
			return false;
		}
	}
}

