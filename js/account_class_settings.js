$(document).ready(function() {
    
	$('#ACSettingsForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ac_itemcode: {
                validators: {
                    notEmpty: {
                        message: 'Item code cannot be empty'
                    },
					 
                }
            },
			ac_name: {
                validators: {
                    notEmpty: {
                        message: 'Service name cannot be empty'
                    },
					 
                }
            },
			ac_cost: {
                validators: {
                    notEmpty: {
                        message: 'Service cost cannot be empty'
                    },
					 
                }
            },
			
			
           
        }
		
    });
	
    
});


function ACSettingsController($scope, $http) {
		$scope.AddACSettings = function() {
		var validate=$('#ACSettingsForm').bootstrapValidator('validate');
		var f1=$('#ac_itemcode').val();
		var f2=$('#ac_name').val();
		var f3=$('#ac_cost').val();
		var f4=$('#ac_status').val();
		var f5=$('#ac_desc').val();
		
		
		if(f1!='' && f2!='' && f3!=''){
		
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		
		$http.post(rCPath+'process_acclass_settings', {'ac_itemcode':f1, 'ac_name': f2,'ac_cost':f3,
		'ac_status':f4,'ac_desc':f5}
		).success(function(data) {
			//alert(data);
		
		if (data!='failed')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Settings saved successfully.');
			$('#ac_name').val('');
			$('#ac_cost').val('');
			$('#ac_desc').val('');
		
			window.location.reload();
			
			
		
		}
		else
		{
			
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to save settings information.');
			
		}
		})
		}
		else{
			return false;
		}
	}
}

var app = angular.module('ACSettingsApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('ACSettingsCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_account_classes').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

/////////////// Delete controller ///////////////////
app.controller('delACController', function ($scope, $http, $timeout) {
	$scope.delAC=function(){
		acc_id=$scope.data.acc_id;
		var parsePath=$(location).attr('href');	
		var re = new RegExp(/^.*\//);
	    var rCPath= re.exec(window.location.href);
	
		$("#complexConfirm"+acc_id).confirm({
		title:"Delete confirmation",
		text:"Are you sure you want to delete?",
		confirm: function(button) {
		        window.location.reload();
				$http.post(rCPath+'delete_acclass_process', {'acc_id': $scope.data.acc_id}
				).success(function(data) {
					alert('success');
					
				})
				
		},
		cancel: function(button) {
		  //alert("You aborted the operation.");
		},
		confirmButton: "Yes I am",
		cancelButton: "No"
		});

		
	}
});



/// delete account class ajx
function delAccountClass(acc_id,item_no){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	$('#dlx_accid').html(item_no);

	//// process instant 
	$('#delACClassModal').modal({ backdrop: 'static', keyboard: false })
	.one('click', '#delete', function() {


	$("#callDelStatus").html('<i class="fa fa-spinner fa-spin"></i> Deleting..');
	$.ajax({
	type: "POST",
	url: rCPath+"delete_acclass_process",
	data: {acc_id:acc_id}
	})
	.done(function(msg) {
		
		$("#callDelStatus").html('<i class="fa fa-check"></i> Deleted successfully');
		window.location.reload();
		
	});

	});	
	
	
}