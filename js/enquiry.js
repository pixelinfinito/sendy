
 $(function(){
 $('#submit_enq').click(function(){
 
   
    var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	var enq_country=$('#geo_country').val();
	var enq_name=$('#enq_name').val();
	var enq_contact=$('#enq_contact').val();
	var enq_email=$('#enq_email').val();
	var enq_type=$('#enq_type').val();
	var enq_comments=$('#enq_comments').val();
	
	if(enq_country!=''&&enq_name!=''&&enq_contact!=''){
		$('#submit_enq').val('Sending..');
		$.ajax({
		type: "POST",
		url: rCPath+"enquiry/send_enquiry",
		data: {enq_country: enq_country,enq_name:enq_name,enq_contact:enq_contact,enq_email:enq_email,
		enq_type:enq_type,enq_comments:enq_comments}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				$('#submit_enq').val('Submit');
			    $('#enq_response').html("<span class='text-success'><i class='icon-check'></i> Thank you for inquiry our team will get back to you in 24 hours.</span>");
				
				$('#enq_name').val('');
				$('#enq_contact').val('');
				$('#enq_email').val('');
				$('#enq_comments').val('');
				
			}else{
				$('#enq_response').html(' Something went wrong.');
			}

		});
	}else{
		alert('Missing mandatory fields.');
	}
	
	});
	
});



/// delete enquiry
function delEnquiry(enq_id,name){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#dlx_recipient').html(name);
		
		$('#deleteEnqModal').modal({ backdrop: 'static', keyboard: false })
		.one('click', '#delete', function() {
		
		$("#callEnqStatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"delete_enquiry",
		data: {enquiry_id:enq_id}
		})
		.done(function(msg) {
           
			if(msg!='failed'){
				$("#callEnqStatus").html('<i class="fa fa-check-circle"></i>'+enq_id+' deleted successfully');
				setTimeout('reloadPage()',1000);
			}else{
			    $("#callEnqStatus").html('<i class="fa fa-times"></i> Something went wrong');
			}
		});

	});
}
    
function reloadPage(){
	window.location.reload();
}	


