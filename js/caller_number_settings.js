$(document).ready(function() {
    
	$('#CallerNumberForm').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            cn_type: {
                validators: {
                    notEmpty: {
                        message: 'Caller number type cannot be empty'
                    },
					 
                }
            },
			cn_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    },
					 
                }
            },
			cn_currency_code: {
                validators: {
                    notEmpty: {
                        message: 'Currency code cannot be empty'
                    },
					 
                }
            },
			
			cn_price: {
                validators: {
                    notEmpty: {
                        message: 'Price cannot be empty'
                    },
					 
                }
            },
           cn_period: {
                validators: {
                    notEmpty: {
                        message: 'CN period cannot be empty'
                    },
					 
                }
            },
        }
		
    });
	
    
});


function CallerNumberCrtl($scope, $http) {
		$scope.AddCNSettings = function() {
		var validate=$('#CallerNumberForm').bootstrapValidator('validate');
		var f1=$('#cn_country').val();
		var f2=$('#cn_type').val();
		var f3=$('#cn_price').val();
		var f4=$('#cn_currency_code').val();
		var f5=$('#cn_desc').val();
		var f6=$('#cn_hidId').val();
		var f7=$('#cn_period').val();
		
		
		if(f1!='' && f2!='' && f3!='' && f4!=''){
		//alert(f1+'-'+f2+'-'+f3+'-'+f4+'-'+f5+'-'+f6+'-'+f7)
		$('#rsDiv').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		var parsePath=$(location).attr('href');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		//alert(rCPath);
		$http.post(rCPath+'process_caller_number', {'hidId':f6, 'cn_country': f1,
		'cn_currency_code':f4,'cn_price':f3,'cn_desc':f5,'cn_type':f2,'cn_period':f7}
		).success(function(data) {
			//alert(data);
		
		if (data!='failed')
		{
			
			$('#rsDiv').html('<i class="fa fa-check-circle darkGreen"></i> Settings saved successfully.');
			window.location.reload();
			
			
		
		}
		else
		{
			
			$('#rsDiv').html('<i class="fa fa-times-circle red"></i> Failed to save settings information.');
			
		}
		})
		}
		else{
			return false;
		}
	}
}

var app = angular.module('CallerNumberApp', ['ui.bootstrap']);

app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('cNListCtrl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_caller_numbers').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

/////////////// Delete controller ///////////////////
app.controller('delCNController', function ($scope, $http, $timeout) {
	$scope.delCN=function(){
		cn_id=$scope.data.cn_id;
		var parsePath=$(location).attr('href');	
		var re = new RegExp(/^.*\//);
	    var rCPath= re.exec(window.location.href);
	
		$("#complexConfirm"+cn_id).confirm({
		title:"Delete confirmation",
		text:"Are you sure you want to delete?",
		confirm: function(button) {
		        window.location.reload();
				$http.post(rCPath+'delete_caller_number', {'cn_id': $scope.data.cn_id}
				).success(function(data) {
					alert('success');
					
				})
				
		},
		cancel: function(button) {
		  //alert("You aborted the operation.");
		},
		confirmButton: "Yes I am",
		cancelButton: "No"
		});

		
	}
});



/// delete caller price ajx
function delCNPricing(cn_id){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	$('#dlx_cnid').html(cn_id);

	//// process instant 
	$('#delCNPriceModal').modal({ backdrop: 'static', keyboard: false })
	.one('click', '#delete', function() {


	$("#callDelStatus").html('<i class="fa fa-spinner fa-spin"></i> Deleting..');
	$.ajax({
	type: "POST",
	url: rCPath+"delete_caller_number",
	data: {cn_id:cn_id}
	})
	.done(function(msg) {
		
		$("#callDelStatus").html('<i class="fa fa-check"></i> Deleted successfully');
		window.location.reload();
		
	});

	});	
	
	
}