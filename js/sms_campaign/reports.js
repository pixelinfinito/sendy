/// load campaigns 

app.controller('MyCampaignsCtrl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_my_campaigns').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});


/// load schedules

app.controller('SchedulesCtrl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_my_schedules').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

function triggerEditContact(c_id,c_group,country_code,call_code) {
	  
		$('#modalEditContact').modal('show');	
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		$('#ecnx_title').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$('#ecnx_mobile').val('loading..');
		$('#ecnx_name').val('loading..');
		$('#ecnx_email').val('loading..');
        
		$('#ecnx_contact_group').val(c_group);
		$('#ecnx_country').val(country_code);
		$('#ecnx_caller_code').val(call_code+'__'+country_code);
		$.ajax({
		type: "POST",
		url: rCPath+"load_modal_contact",
		data: {cId: c_id}
		})
		.done(function(data) {
			//alert(data)
			if (data!='failed')
			{
			  
			   /// format mobile number
			   var call_prefix=call_code.length;
			   var final_prefix=call_prefix+1;
			   
			   var fly_info=data.split('__');	 
			   $('#ecnx_cid').val(fly_info[0]);
			   $('#ecnx_mobile').val(fly_info[1].substring(final_prefix));
			   $('#ecnx_name').val(fly_info[2]);
			   $('#ecnx_email').val(fly_info[3]);
			   //$('#mxc_ccode').val(fly_info[4]);
			   $('#ecnx_title').html(fly_info[2]);
			   //$('#md_group').html(fly_info[5]);
			   
				
			}else{
				$('#ecnx_error').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}

function etrCountry(c_code){
	var callercode=$('#ecnx_caller_code').val();
	var sp_callercode=callercode.split('__');
	var c_code=sp_callercode[1];
    $('#ecnx_country').val(c_code);
}

function editSMSContactFrm($scope, $http){
	    $scope.updateSMSContact= function() {
	    
		
	    var cid=$('#ecnx_cid').val();
		var f1=$('#ecnx_name').val();
		var f2=$('#ecnx_mobile').val();
		var f3=$('#ecnx_email').val();
		var f4=$('#ecnx_contact_group').val();
		var f5=$('#ecnx_country').val();
		//var f6=$('#ecnx_caller_code').val();
		
		var callercode=$('#ecnx_caller_code').val();
		var sp_callercode=callercode.split('__');
		var f6=sp_callercode[0];
	
		
		if(f1!='' && f2!='' && f4!='' && f5!='' && f6!=''){
			var re = new RegExp(/^.*\//);
			var rCPath= re.exec(window.location.href);
			$('#dyUSMSProgress').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
			$.ajax({
			type: "POST",
			url: rCPath+"edit_contact",
			data: {full_name: f1, mobile: f2,email:f3,group:f4,country:f5,cid:cid,calling_code:f6}
			})
			.done(function(msg) {
				if (msg!='failed')
				{
					$('#dyUSMSProgress').html('<i class="fa fa-check-circle darkGreen"></i> Contact updated successfully.');
					reloadPage();
				}else{
					$('#dyUSMSProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				}

			});
		}else{
			$('#dyUSMSProgress').html('<i class="fa fa-exclamation-triangle red"></i> Missing mandatory fields.');
			setTimeout('autoHideMandatoryblock()',1000);
			return false;
		}
				
}
}

function autoHideMandatoryblock(){
	$('#dyUSMSProgress').slideUp();
	$('#ecnx_cgprocess').slideUp();
	$('#dyEditTTProgress').slideUp();
	
}

/// delete campaigns ajx
function delCampaigns(c_code){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	    
	 	$.ajax({
		type: "POST",
		url: rCPath+"load_campaign_headline",
		data: {c_code: c_code}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var campaign_code=fly_info[0];
				$('#dlx_campaign_code').html(campaign_code);
				$('#dlx_campaign_message').html(fly_info[1]);

				//// process instant 
				$('#delCampaignModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callCampaignDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_campaign",
				data: {campaign_code:campaign_code}
				})
				.done(function( msg ) {

					if(msg!='failed'){
						$.notify({
							icon: 'pe-7s-check',
							message: "Campaign code "+fly_info[0]+" deleted successfully."

						},{
							type: 'success',
							timer: 4000
						});
					/*$.notify("Campaign code ["+fly_info[0]+"] deleted successfully ", "success",{arrowSize: 14,style: 'bootstrap',width: '500px'});*/
					setTimeout('reloadPage()',2000);
					}else{
						/*$.notify('Something went wrong.', "danger");*/
						$.notify({
						icon: 'pe-7s-close',
						message: "Something went wrong."

						},{
							type: 'danger',
							timer: 4000
						});

					}
				});

				});	
				//// end 
			   
				
			}else{
				/*alert('Something went wrong.');*/
					$.notify({
					icon: 'pe-7s-close',
					message: "Something went wrong."

					},{
					type: 'danger',
					timer: 4000
					});
				//$('#dy_load_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}

/// delete schedules ajx
function delSchedules(c_code){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	 //alert(c_code);
	 	$.ajax({
		type: "POST",
		url: rCPath+"load_campaign_headline",
		data: {c_code: c_code}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var campaign_code=fly_info[0];
				$('#dlx_schedule_code').html(campaign_code);
				$('#dlx_schedule_message').html(fly_info[1]);

				//// process instant 
				$('#delScheduleModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callScheduleDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_campaign",
				data: {campaign_code:campaign_code}
				})
				.done(function( msg ) {

					if(msg!='failed'){
						$.notify({
						icon: 'pe-7s-check',
						message: "Campaign code "+fly_info[0]+" deleted successfully."

						},{
						type: 'success',
						timer: 4000
						});

					/*$.notify("Campaign code ["+fly_info[0]+"] deleted successfully ", "success",{arrowSize: 14,style: 'bootstrap',width: '500px'});*/
					setTimeout('reloadPage()',2000);
					}else{
						/*$.notify('Something went wrong.', "danger");*/
						$.notify({
						icon: 'pe-7s-close',
						message: "Something went wrong."

						},{
							type: 'danger',
							timer: 4000
						});

					}
				});

				});	
				//// end 
			   
				
			}else{
				/*alert('Something went wrong.');*/
				$.notify({
				icon: 'pe-7s-close',
				message: "Something went wrong."

				},{
				type: 'danger',
				timer: 4000
				});
				//$('#dy_load_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}

/// delete group ajx
function delCgGroup(cg_id){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	 
	 	$.ajax({
		type: "POST",
		url: rCPath+"load_cgroup_headline",
		data: {cg_id: cg_id}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var group_id=fly_info[0];
				$('#dlx_groupname').html(fly_info[1]);

				//// process instant 
				$('#delCgGroupModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callGroupDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_sms_group",
				data: {group_id:group_id}
				})
				.done(function( msg ) {

					if(msg!='failed'){
						$.notify({
						icon: 'pe-7s-check',
						message: "Group "+fly_info[1]+" deleted successfully."

						},{
						type: 'success',
						timer: 4000
						});
					/*
					$.notify("Group ["+fly_info[1]+"] deleted successfully ", "success",{arrowSize: 14,style: 'bootstrap',width: '500px'});
					*/
					setTimeout('reloadPage()',2000);
					}else{
					    /*$.notify('Something went wrong.', "danger");*/
						$.notify({
						icon: 'pe-7s-close',
						message: "Something went wrong."

						},{
							type: 'danger',
							timer: 4000
						});

					}
				});

				});	
				//// end 
			   
				
			}else{
				alert('Something went wrong.');
				//$('#dy_load_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
	
}

/// delete template title ajx
function delTemplateTitle(t_code){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	 
	 	$.ajax({
		type: "POST",
		url: rCPath+"load_ttitle_headline",
		data: {t_code: t_code}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var tt_code=fly_info[0];
				$('#dlx_tt_code').html(tt_code);
				$('#dlx_tt_title').val(fly_info[1]);

				//// process instant 
				$('#delTtitleModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callTTitleDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_ttitle",
				data: {tt_code:tt_code}
				})
				.done(function(msg) {

					if(msg!='failed'){
						$.notify({
							icon: 'pe-7s-check',
							message: "Template code "+fly_info[0]+" deleted successfully."

						},{
							type: 'success',
							timer: 4000
						});
					/*$.notify("Template code ["+fly_info[0]+"] deleted successfully ", "success",{arrowSize: 14,style: 'bootstrap',width: '500px'});*/
					setTimeout('reloadPage()',2000);
					}else{
						/*$.notify('Something went wrong.', "danger");*/
						$.notify({
						icon: 'pe-7s-close',
						message: "Something went wrong."

						},{
							type: 'danger',
							timer: 4000
						});

					}
				});

				});	
				//// end 
			   
				
			}else{
				/*alert('Something went wrong.');*/
					$.notify({
					icon: 'pe-7s-close',
					message: "Something went wrong."

					},{
					type: 'danger',
					timer: 4000
					});
				//$('#dy_load_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
	
}

/// delete group ajx
function delNotify(notify_id){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	 
	 	$.ajax({
		type: "POST",
		url: rCPath+"load_notification_headline",
		data: {notify_id: notify_id}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				
				$('#dlx_notify').val(data);

				//// process instant 
				$('#delMNotifyModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_notification",
				data: {notify_id:notify_id}
				})
				.done(function(msg) {

					if(msg!='failed'){
						$.notify({
						icon: 'pe-7s-check',
						message: "Notification ID "+notify_id+" deleted successfully."

						},{
						type: 'success',
						timer: 4000
						});
					setTimeout('reloadPage()',2000);
					}else{
					    $.notify({
						icon: 'pe-7s-close',
						message: "Something went wrong."

						},{
							type: 'danger',
							timer: 4000
						});

					}
				});

				});	
				//// end 
			   
				
			}else{
				alert('Something went wrong.');
				
			}

		});
		
	
}

/// delete history
function delSmsHistory(cmp_id,tno){
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
	 		
		if (tno!='')
		{
			$('#dlx_shtno').html(tno);

			//// process instant 
			$('#delSHistoryModal').modal({ backdrop: 'static', keyboard: false })
			.one('click', '#delete', function() {

			
			$("#callsHisDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
			$.ajax({
			type: "POST",
			url: rCPath+"delete_history_contact",
			data: {c_id:cmp_id}
			})
			.done(function( msg ) {

				if(msg!='failed'){
					$.notify({
					icon: 'pe-7s-check',
					message: "History contact "+tno+" deleted successfully."

					},{
					type: 'success',
					timer: 4000
					});
				/*
				$.notify("Group ["+fly_info[1]+"] deleted successfully ", "success",{arrowSize: 14,style: 'bootstrap',width: '500px'});
				*/
				setTimeout('reloadPage()',2000);
				}else{
					/*$.notify('Something went wrong.', "danger");*/
					$.notify({
					icon: 'pe-7s-close',
					message: "Something went wrong."

					},{
						type: 'danger',
						timer: 4000
					});

				}
			});

			});	
			//// end 
		   
			
		}else{
			alert('Something went wrong.');
			//$('#dy_load_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
		}

	
}

//// edit sms group

function triggerEditCgGroup(cgrp_id) {
	  
		$('#modalEditSMSGroup').modal('show');	
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		$('#ecnx_cgprocess').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$('#ecnx_cg_groupname').val('loading..');
		
		$.ajax({
		type: "POST",
		url: rCPath+"load_cgroup_headline",
		data: {cg_id: cgrp_id}
		})
		.done(function(data) {
			//alert(data)
			if (data!='failed')
			{
			  $('#ecnx_cgprocess').slideUp();
			  //alert(data);	
			   var fly_info=data.split('__');	 
			   $('#ecnx_groupid').val(fly_info[0]);
			   $('#ecnx_cg_groupname').val(fly_info[1]);
			   $('#ecnx_cggroup').html(fly_info[1]);
			   
			   
				
			}else{
				$('#ecnx_cgprocess').slideDown();
				$('#ecnx_cgprocess').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}

function editCgGroupFrm($scope, $http){
	    $scope.updateSMSGroup= function() {
	    var cgroup_id=$('#ecnx_groupid').val();
		var f1=$('#ecnx_cg_groupname').val();
		
		
		if(f1!=''){
			var re = new RegExp(/^.*\//);
			var rCPath= re.exec(window.location.href);
			$('#dyUSMSGroupProgress').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
			$.ajax({
			type: "POST",
			url: rCPath+"edit_smsgroup",
			data: {cg_groupname: f1,cgrp_id:cgroup_id}
			})
			.done(function(msg) {
				if (msg!='failed')
				{
					$('#dyUSMSGroupProgress').html('<i class="fa fa-check-circle darkGreen"></i> Group updated successfully.');
					reloadPage();
				}else{
					$('#dyUSMSGroupProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				}

			});
		}else{
			$('#ecnx_cgprocess').html('<i class="fa fa-exclamation-triangle red"></i> Missing mandatory fields.');
			setTimeout('autoHideMandatoryblock()',1000);
			return false;
		}
				
}
}

/// update template title
function triggerEditTTitle(t_code){
	$('#modalEditTempleteTitle').modal('show');
	var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		$('#ecnx_tt_process').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$('#ecnx_tt_code').val('loading..');
		$('#ecnx_tt_title').val('loading..');
		
		
		$.ajax({
		type: "POST",
		url: rCPath+"load_ttitle_headline",
		data: {t_code: t_code}
		})
		.done(function(data) {
			//alert(data)
			if (data!='failed')
			{
			  $('#ecnx_tt_process').slideUp();
			  //alert(data);	
			   var fly_info=data.split('__');	
		   
			   $('#ecnx_tt_code').val(fly_info[0]);
			   $('#ecnx_tt_title').val(fly_info[1]);
			   $('#ecnx_tt_hcode').html(fly_info[0]);
			  
			}else{
				$('#ecnx_tt_process').slideDown();
				$('#ecnx_tt_process').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}

function editTemplateTitleFrm($scope, $http){
	    $scope.updateTemplateTitle= function() {
	    var t_code=$('#ecnx_tt_code').val();
		var f1=$('#ecnx_tt_title').val();
		
		
		if(f1!='' && t_code!=''){
			var re = new RegExp(/^.*\//);
			var rCPath= re.exec(window.location.href);
			$('#dyEditTTProgress').slideDown();
			$('#dyEditTTProgress').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
			$.ajax({
			type: "POST",
			url: rCPath+"edit_ttitle",
			data: {t_code: t_code,t_title:f1}
			})
			.done(function(msg) {
				if (msg!='failed')
				{
					$('#dyEditTTProgress').html('<i class="fa fa-check-circle darkGreen"></i> Group updated successfully.');
					reloadPage();
				}else{
					$('#dyEditTTProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				}

			});
		}else{
			$('#dyEditTTProgress').html('<i class="fa fa-exclamation-triangle red"></i> Missing mandatory fields.');
			setTimeout('autoHideMandatoryblock()',1000);
			return false;
		}
				
}
}


