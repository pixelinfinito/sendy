
/// calling from header
function checkcamp_logs()
{
	
	///// trigger undelivered sms
	   var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		//alert(rCPath);
		var token='trigger';
		
		$("#trigger_caller").html('<i class="fa fa-fighter-jet fa-spin"></i> <small>triggering..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"trigger_sms_campaign",
		data: {token:token}
		})
		.done(function(msg) {
		 //alert(msg);
		 setTimeout('grabDeliverlogs()',5000);
		$("#trigger_caller").html('<i class="fa fa-paper-plane-o "></i> <small>Sending..</small>');
		// window.location.reload();
	});
	
}

function grabDeliverlogs(){
	///// propagate final status
	   var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		//alert(rCPath);
		var token='finalcall';
		
		$("#final_caller").html('<i class="fa fa-fighter-jet fa-spin"></i> <small>Grabbing..</small>');
		$.ajax({
		type: "POST",
		url: rCPath+"grab_smsfinal_status",
		data: {token:token}
		})
		.done(function(msg) {
		//alert(msg);
		 
		$("#final_caller").html('<i class="fa fa-paper-plane-o "></i> <small>Got it..</small>');
		// window.location.reload();
	});
}

function startSmsService(){
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		//alert(rCPath);
		var token='start';
		
		$("#service_caller").html('<i class="fa fa-fighter-jet fa-spin"></i> Starting..');
		$.ajax({
		type: "POST",
		url: rCPath+"call_sms_service",
		data: {token:token}
		})
		.done(function(msg) {
		 //alert(msg);
		$("#service_caller").html('<i class="fa fa-paper-plane-o "></i> Running..');
		 window.location.reload();
	});
            
}

function stopSmsService(){
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		//alert(rCPath);
		var token='stop';
		
		$("#stop_caller").html('<i class="fa fa-fighter-jet fa-spin"></i> Stopping..');
		$.ajax({
		type: "POST",
		url: rCPath+"call_sms_service",
		data: {token:token}
		})
		.done(function(msg) {
		 //alert(msg);
		
		$("#stop_caller").html('<i class="fa fa-paper-plane-o "></i> Stopped..');
		 window.location.reload();
	});
            
}