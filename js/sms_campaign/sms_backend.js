
/// delete campaigns ajx
function delCampaigns(c_code,uid){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	   // alert(rCPath);
	 	$.ajax({
		type: "POST",
		url: rCPath+"load_campaign_headline",
		data: {c_code: c_code,uId: uid}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var campaign_code=fly_info[0];
				$('#dlx_campaign_code').html(campaign_code);
				$('#dlx_campaign_message').html(fly_info[1]);

				//// process instant 
				$('#delCampaignModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callCampaignDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_campaign",
				data: {campaign_code:campaign_code,uId: uid}
				})
				.done(function(msg) {

					if(msg!='failed'){
						setTimeout('reloadPage()',1000);
					}else{
						alert('Something went wrong.');
					
					}
				});

				});	
				
				
			}else{
				alert('Something went wrong @ loading title.');
				
			}

		});
		
}

/// delete schedules ajx
function delSchedules(c_code,uid){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	 //alert(c_code);
	 	$.ajax({
		type: "POST",
		url: rCPath+"load_campaign_headline",
		data: {c_code: c_code,uId: uid}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var campaign_code=fly_info[0];
				$('#dlx_schedule_code').html(campaign_code);
				$('#dlx_schedule_message').html(fly_info[1]);

				//// process instant 
				$('#delScheduleModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callScheduleDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_campaign",
				data: {campaign_code:campaign_code,uId: uid}
				})
				.done(function( msg ) {

					if(msg!='failed'){
						setTimeout('reloadPage()',1000);
					}else{
						alert('Something went wrong.');

					}
				});

				});	
				
				
			}else{
				alert('Something went wrong @ loading title.');
				
			}

		});
		
}




/// delete history @ sms ctrl
function delBackendSmsHistory(cmp_id,tno,uid){
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
	 		
		if (tno!='')
		{
			$('#dlx_shtno').html(tno);

			//// process instant 
			$('#delSHistoryModal').modal({ backdrop: 'static', keyboard: false })
			.one('click', '#delete', function() {

			
			$("#callsHisDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
			$.ajax({
			type: "POST",
			url: rCPath+"delete_history_contact",
			data: {c_id:cmp_id,uid:uid}
			})
			.done(function( msg ) {
                
				if(msg!='failed'){
					$("#callContactDelstatus").html('<i class="fa fa-check-circle"></i> Contact '+tno+' deleted successfully.');
					setTimeout('reloadPage()',1000);
						
				}else{
					$("#callContactDelstatus").html('<i class="fa fa-times"></i> Contact '+tno+' deletion failed.');
				    setTimeout('reloadPage()',1000);
				}
			});

			});	
			//// end 
		   
			
		}else{
			alert('Something went wrong.');
			
		}

	
}

/// remarks @ sms ctrl
function backendSmsRemarks(cmp_id,uid){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#modalSMSRemarks').modal('show');
		$('#rsRemarks').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$.ajax({
		type: "POST",
		url: rCPath+"load_sms_remarks",
		data: {cId: cmp_id,uId: uid}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
			    $('#rsRemarks').html(data);
				
			}else{
				$('#rsRemarks').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}



function reloadPage(){
	
	window.location.reload();
}
