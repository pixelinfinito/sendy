
/// delete campaigns ajx
function delCampaigns(c_code,uid){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	    
	 	$.ajax({
		type: "POST",
		url: rCPath+"load_campaign_headline",
		data: {c_code: c_code,uid:uid}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var campaign_code=fly_info[0];
				$('#dlx_campaign_code').html(campaign_code);
				$('#dlx_campaign_message').html(fly_info[1]);

				//// process instant 
				$('#delCampaignModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callCampaignDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_campaign",
				data: {campaign_code:campaign_code,uid:uid}
				})
				.done(function(msg) {

					if(msg!='failed'){
						$('#callCampaignDelstatus').html('<i class="fa fa-check-circle"></i> Campaign code '+fly_info[0]+' deleted successfully.');
						setTimeout('reloadPage()',1000);
					}else{
						$('#callCampaignDelstatus').html('<i class="fa fa-times"></i> Something went wrong, campaign code '+fly_info[0]+' deletion failed.');
						setTimeout('reloadPage()',1000);
					}
				});

				});	
				//// end 
			   
				
			}else{
				alert('Something went wrong.');
					
			}

		});
		
}





/// delete caller number ajx
function delCallerNumber(tid,uid){
	
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	 
	 	$.ajax({
		type: "POST",
		url: rCPath+"caller_number_headline",
		data: {tId: tid,uId: uid}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var cn_tid=fly_info[0];
				$('#dlx_cn_number').html(fly_info[1]);

				//// process instant 
				$('#delCallerNumberModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callCNDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_caller_number",
				data: {cn_tid:cn_tid,uId: uid}
				})
				.done(function(msg) {

					if(msg!='failed'){
						$("#callCNDelstatus").html('<i class="fa fa-check-circle"></i> Caller number '+fly_info[1]+' deleted successfully.');
						setTimeout('reloadPage()',1000);
					}else{
					   $("#callCNDelstatus").html('<i class="fa fa-times"></i> Caller number '+fly_info[1]+' deletion failed.');
						setTimeout('reloadPage()',1000);

					}
				});

				});	
				//// end 
			   
				
			}else{
						
				alert('Something went wrong.');
				
			}

		});
		
}


/// delete sms contact ajx
function delSMSContact(c_id,uid){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	   // $('#callContactDelstatus').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$.ajax({
		type: "POST",
		url: rCPath+"load_modal_contact",
		data: {cId: c_id,uId: uid}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var contact_id=fly_info[0];
				$('#dlx_contact_name').html(fly_info[2]);

				//// process instant 
				$('#delSmsContactModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callContactDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_sms_contact",
				data: {contact_id:contact_id,uId: uid}
				})
				.done(function( msg ) {

					if(msg!='failed'){
						$("#callContactDelstatus").html('<i class="fa fa-check-circle"></i>'+fly_info[2]+' deleted successfully.');
						setTimeout('reloadPage()',1000);
					}else{
					   $("#callContactDelstatus").html('<i class="fa fa-times"></i>'+fly_info[2]+' deletion failed.');
						setTimeout('reloadPage()',1000);
					}
				});

				});	
				//// end 
			   
				
			}else{
				alert('Something went wrong.');
				
			}

		});
		
	
}



/// delete history
function delSmsHistory(cmp_id,tno,uid){
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
	 		
		if (tno!='')
		{
			$('#dlx_shtno').html(tno);

			//// process instant 
			$('#delSHistoryModal').modal({ backdrop: 'static', keyboard: false })
			.one('click', '#delete', function() {

			
			$("#callsHisDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
			$.ajax({
			type: "POST",
			url: rCPath+"delete_history_contact",
			data: {c_id:cmp_id,uid:uid}
			})
			.done(function( msg ) {
                
				if(msg!='failed'){
					$("#callContactDelstatus").html('<i class="fa fa-check-circle"></i> Contact '+tno+' deleted successfully.');
					setTimeout('reloadPage()',1000);
						
				}else{
					$("#callContactDelstatus").html('<i class="fa fa-times"></i> Contact '+tno+' deletion failed.');
				    setTimeout('reloadPage()',1000);
				}
			});

			});	
			//// end 
		   
			
		}else{
			alert('Something went wrong.');
			
		}

	
}




function reloadPage(){
	window.location.reload();
}



