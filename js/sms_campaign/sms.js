///////////////// Form validate /////////////////////
$(document).ready(function() {
	
   	
	$('#addSMSContactForm').bootstrapValidator({
		
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            contact_name: {
                validators: {
                    notEmpty: {
                        message: 'Name cannot be empty'
                    },
					 
                }
            },
			contact_number: {
                validators: {
                    notEmpty: {
                        message: 'Mobile number cannot be empty'
                    },
					 
                }
            },
			contact_group: {
                validators: {
                    notEmpty: {
                        message: 'Group cannot be empty'
                    },
					 
                }
            },
			contact_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    },
					 
                }
				
            },
			caller_code: {
                validators: {
                    notEmpty: {
                        message: 'Calling code cannot be empty'
                    },
					 
                }
				
            },
			
           
        }
		
    });
	
	$('#editSMSContactForm').bootstrapValidator({
		
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            contact_name: {
                validators: {
                    notEmpty: {
                        message: 'Name cannot be empty'
                    },
					 
                }
            },
			contact_number: {
                validators: {
                    notEmpty: {
                        message: 'Mobile number cannot be empty'
                    },
					 
                }
            },
			contact_group: {
                validators: {
                    notEmpty: {
                        message: 'Group cannot be empty'
                    },
					 
                }
            },
			contact_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    },
					 
                }
				
            },
			
           
        }
		
    });
	
	$('#addSMSGroupForm').bootstrapValidator({
		
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            group_name: {
                validators: {
                    notEmpty: {
                        message: 'Name cannot be empty'
                    },
					 
                }
            },
			
           
        }
		
    });
	
$('#buyCNumberForm').bootstrapValidator({
		
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            cn_type: {
                validators: {
                    notEmpty: {
                        message: 'Caller number type cannot be empty'
                    },
					 
                }
            },
			
			cn_country: {
                validators: {
                    notEmpty: {
                        message: 'Country cannot be empty'
                    },
					 
                }
            },
			
			cn_lease_period: {
                validators: {
                    notEmpty: {
                        message: 'Lease period cannot be empty'
                    },
					 
                }
            },
			
			
           
        }
		
    });
	
$('#campaignSMSForm').bootstrapValidator({
		
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            campaign_to_list: {
                validators: {
                    notEmpty: {
                        message: 'To numbers cannot be empty'
                    },
					 
                }
            },
			campaign_from: {
                validators: {
                    notEmpty: {
                        message: 'From number cannot be empty'
                    },
					 
                }
            },
			campaign_text: {
                validators: {
                    notEmpty: {
                        message: 'Message cannot be empty'
                    },
					 
                }
            },
			
           
        }
		
    });
	
$('#ccSingleCtrlForm').bootstrapValidator({
		
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            to_group: {
                validators: {
                    notEmpty: {
                        message: 'To group cannot be empty'
                    },
					 
                }
            },
			
        }
		
    });
	
$('#mvSingleCtrlForm').bootstrapValidator({
		
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            to_group: {
                validators: {
                    notEmpty: {
                        message: 'To group cannot be empty'
                    },
					 
                }
            },
			
        }
		
    });
	
	
	   
	//// counting message characters 
	   
	var text_max = 160;
    $('#campaign_text_alerter').html(text_max + ' characters remaining');
    $('#campaign_text').keyup(function() {
        var text_length = $('#campaign_text').val().length;
        var text_remaining = text_max - text_length;
		if(text_remaining <= '0'){
			alert('Sorry exceeded character limit');
		}else{
			$('#campaign_text_alerter').html(text_remaining + ' characters remaining');
		}
	});
	
	$("#assign_btn").click(function() {
	    getAssignedContacts();
	});
	
	$("#link_copy_all").click(function() {
	    getCopiedContacts();
	});
	$("#link_move_all").click(function() {
	    getMovedContacts();
	});
	
	$("#link_delete_all").click(function() {
	    deleteSelectedContacts();
	});
	
	
	
	//// check all to fetch
	 $('#achk_contact_all').click(function () {    
		 //$('input:checkbox').prop('checked', this.checked);    
		 $('input[name="achk_contact"]').prop('checked', this.checked);    
		 $.each($("input[name='achk_contact']:checked"), function(){  
		   //alert($(this).val());
		    
		 });
	 });
	
	//// check all to copy 
	 $('#check_all_contacts').click(function () {    
		 $('input[name="check_contact"]').prop('checked', this.checked);    
		 $.each($("input[name='check_contact']:checked"), function(){  
		    //alert($(this).val());
		 });
	 });
		
	
		
});

/// assigning group contacts 
function getAssignedContacts(){
	
	/* declare an checkbox array */
	var campaign_mode=$('#hidCampaignMode').val();
	var chkArray = [];
	var inchkArray=[];
	var cFtVals="";
	/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$(".chk:checked").each(function() {
		//chkArray.push($(this).val());
		var crossVal=$(this).val();
		var inrCrossVal=crossVal.split('_');
		inchkArray.push(inrCrossVal[0]);
		chkArray.push(inrCrossVal[1]);
	});
	
	/* we join the array separated by the comma */
	var selected;
	var depthSelected;
	
	selected = chkArray.join(',') + ",";
	depthSelected=inchkArray.join(',') + ",";
	
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(selected.length > 1){
		$('#campaign_to_list').attr('readonly', true);
		$('#campaign_to_list').val(selected);
		$('#hidAssignGrpToken').val(depthSelected);	
		$('#clearToContacts').slideDown(1000);
		//alert(selected);
		//alert(depthSelected);
		$('#modalFetchGrouptoFrom').modal('hide');
		/* displaying projected price */
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
		   $("#proacc_price").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
			$.ajax({
			type: "POST",
			url: rCPath+"get_projected_price",
			data: {gid:depthSelected,mode:campaign_mode}
			})
			.done(function(msg) {
			 //alert(msg);
			 $('#hidProPrice').val(msg);
			 $('#proacc_price').html(msg);
			
		});
		
	}else{
		$('#campaign_to_list').val('');
		$('#hidAssignGrpToken').val('');	
		$('#campaign_to_list').attr('readonly', false);
		$('#clearToContacts').slideUp(1000);
		alert("Please at least one of the checkbox");	
		
		/* displaying projected price */
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
		   $("#proacc_price").html('<h4><i class="fa fa-spinner fa-spin"></i></h4>');
			$.ajax({
			type: "POST",
			url: rCPath+"get_projected_price",
			data: {gid:'',mode:campaign_mode}
			})
			.done(function(msg) {
			 $('#hidProPrice').val(msg);
			 $('#proacc_price').html(msg);
			
		});
			
	}
	
}

///// proceed to bulk copy contacts
function getCopiedContacts(){
	
	/* declare an checkbox array */
	var chkArray = [];
	var inchkArray=[];
	var cFtVals="";
	/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$(".check_contact:checked").each(function() {
		var crossVal=$(this).val();
		var inrCrossVal=crossVal.split('_');
		inchkArray.push(inrCrossVal[0]);
		chkArray.push(inrCrossVal[1]);
		
	});
	
	/* we join the array separated by the comma */
	var selected;
	var depthSelected;
	
	selected = chkArray.join(',') + ",";
	depthSelected=inchkArray.join(',') + ",";
	
	
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(selected.length > 1){
		
		$('#tt_contacts').html(chkArray.length);
		$('#cpContactsAlert').modal({ backdrop: 'static', keyboard: false }).one('click', '#cp_confirm', function() {
		var cpgroup=$('#cpto_group').val();
	   
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
		   $("#cp_status").html('<i class="fa fa-circle-o-notch fa-spin"></i> Copying in progress..');
			$.ajax({
			type: "POST",
			url: rCPath+"process_cpselected_contacts",
			data: {cIds:depthSelected,groupId:cpgroup}
			})
			.done(function(msg) {
				var duplicated=(inchkArray.length)-(msg);
				if(msg!='failed'){
					$.notify({
					icon: 'pe-7s-check',
					message: " ("+msg+") Contacts copied successfully, "+duplicated+" already existed in destination group"

					},{
					type: 'success',
					timer: 4000
					});
					
					$('input[name="check_all_contacts"]').prop('checked',false);  
					$('input[name="check_contact"]').prop('checked',false);  
					reloadPage();

				}else{
					$.notify({
					icon: 'pe-7s-close',
					message: "Something went wrong."

					},{
					type: 'danger',
					timer: 4000
					});
					$('input[name="check_all_contacts"]').prop('checked',false);  
					$('input[name="check_contact"]').prop('checked',false);  
					reloadPage();
				}
			   
			});
	  });	
		
	}else{
		alert("Please at least one of the checkbox");	
		
	}
	
}


///// proceed to bulk moving contacts
function getMovedContacts(){
	
	/* declare an checkbox array */
	var chkArray = [];
	var inchkArray=[];
	var cFtVals="";
	/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$(".check_contact:checked").each(function() {
		var crossVal=$(this).val();
		var inrCrossVal=crossVal.split('_');
		inchkArray.push(inrCrossVal[0]);
		chkArray.push(inrCrossVal[1]);
		
	});
	
	/* we join the array separated by the comma */
	var selected;
	var depthSelected;
	
	selected = chkArray.join(',') + ",";
	depthSelected=inchkArray.join(',') + ",";
	
	
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(selected.length > 1){
		
		$('#mv_contacts').html(chkArray.length);
		$('#mvContactsAlert').modal({ backdrop: 'static', keyboard: false }).one('click', '#mv_confirm', function() {
		var mvgroup=$('#mvto_group').val();
	   
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
		   $("#mv_status").html('<i class="fa fa-circle-o-notch fa-spin"></i> Copying in progress..');
			$.ajax({
			type: "POST",
			url: rCPath+"process_mvselected_contacts",
			data: {cIds:depthSelected,groupId:mvgroup}
			})
			.done(function(msg) {
				//alert(msg);
				var duplicated=(inchkArray.length)-(msg);
				if(msg!='failed'){
					$.notify({
					icon: 'pe-7s-check',
					message: " ("+msg+") Contacts moved successfully, "+duplicated+" already existed in destination group"

					},{
					type: 'success',
					timer: 4000
					});
					
					$('input[name="check_all_contacts"]').prop('checked',false);  
					$('input[name="check_contact"]').prop('checked',false);  
					reloadPage();

				}else{
					$.notify({
					icon: 'pe-7s-close',
					message: "Something went wrong."

					},{
					type: 'danger',
					timer: 4000
					});
					$('input[name="check_all_contacts"]').prop('checked',false);  
					$('input[name="check_contact"]').prop('checked',false);  
					reloadPage();
				}
			   
			});
	  });	
		
	}else{
		alert("Please at least one of the checkbox");	
		
	}
	
}


///// proceed to delete selected contacts
function deleteSelectedContacts(){
	
	
	/* declare an checkbox array */
	var chkArray = [];
	var inchkArray=[];
	var cFtVals="";
	/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$(".check_contact:checked").each(function() {
		var crossVal=$(this).val();
		var inrCrossVal=crossVal.split('_');
		inchkArray.push(inrCrossVal[0]);
		chkArray.push(inrCrossVal[1]);
		
	});
	
	/* we join the array separated by the comma */
	var selected;
	var depthSelected;
	
	selected = chkArray.join(',') + ",";
	depthSelected=inchkArray.join(',') + ",";
	
	
	/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	if(selected.length > 1){
		
		$('#del_contacts').html(chkArray.length);
		$('#delContactsAlert').modal({ backdrop: 'static', keyboard: false }).one('click', '#del_confirm', function() {
		
	   
		var re = new RegExp(/^.*\//);
		var rCPath= re.exec(window.location.href);
		   $("#del_status").html('<i class="fa fa-circle-o-notch fa-spin"></i> Deleting ..');
			$.ajax({
			type: "POST",
			url: rCPath+"process_delselected_contacts",
			data: {cIds:depthSelected}
			})
			.done(function(msg) {
				//alert(msg);
				var duplicated=inchkArray.length;
				if(msg!='failed'){
					$.notify({
					icon: 'pe-7s-check',
					message: "Selected contacts deleted successfully."

					},{
					type: 'success',
					timer: 4000
					});
					
					$('input[name="check_all_contacts"]').prop('checked',false);  
					$('input[name="check_contact"]').prop('checked',false);  
					reloadPage();

				}else{
					$.notify({
					icon: 'pe-7s-close',
					message: "Something went wrong."

					},{
					type: 'danger',
					timer: 4000
					});
					$('input[name="check_all_contacts"]').prop('checked',false);  
					$('input[name="check_contact"]').prop('checked',false);  
					reloadPage();
				}
			   
			});
	  });	
		
	}else{
		alert("Please select at least one of the checkbox");	
		
	}
	
}



var cnApp = angular.module('CNApp', ['ui.bootstrap']);
var app = angular.module('SMSApp', ['ui.bootstrap']);
app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

var pricesApp = angular.module('SMSAppPrices', ['ui.bootstrap']);
pricesApp.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

var contactsApp = angular.module('SMSAppContacts', ['ui.bootstrap']);
contactsApp.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

var groupsApp = angular.module('SMSAppGroups', ['ui.bootstrap']);
groupsApp.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});


app.controller('LSMSCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_sms_prices').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});




app.controller('ContactsCtrl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_contacts').success(function(data){
		//alert(data);
        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

/// load all groups 


app.controller('SMSGroupsCtrl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_groups').success(function(data){
		//alert(data);

        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

//var app = angular.module('app', ['groupsApp', 'contactsApp', 'pricesApp']);

/////// load sms contact form
function triggerSMSContact(){
	
	$('#modalAddContact').modal('show');
}
function triggerImportModal(){
	$('#modalImportContacts').modal('show');
}

function trCountry(c_code){
	var callercode=$('#caller_code').val();
	var sp_callercode=callercode.split('__');
	var c_code=sp_callercode[1];
		
   $('#contact_country').val(c_code);
}


	
/// load caller numbers 	

app.controller('CallerNumbersCrtl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_caller_numbers').success(function(data){
		//alert(data);

        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

app.controller('SmsHistoryCtrl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_campaign_history').success(function(data){
		//alert(data);

        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});


/// load template titles
app.controller('SMSTitleCtrl', function ($scope, $http, $timeout) {
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	
    $http.get(rCPath+'load_template_titles').success(function(data){
		//alert(data);

        $scope.list = data;
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 10; //max no of items to display in a page
        $scope.filteredItems = $scope.list.length; //Initially for no filter  
        $scope.totalItems = $scope.list.length;
    });
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };
});

////// add contact process 
	
function addSMSFormCtrl($scope, $http) {
		$scope.addSMSContactBtn = function() {
		var validate=$('#addSMSContactForm').bootstrapValidator('validate');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		
		var f1=$('#contact_name').val();
		var f2=$('#contact_number').val();
		var f3=$('#contact_email').val();
		var f4=$('#contact_group').val();
		var f5=$('#contact_country').val();
		var callercode=$('#caller_code').val();
		var sp_callercode=callercode.split('__');
		var f6=sp_callercode[0];
		
		if(f1!='' && f2!='' && f4!='' && f5!='' && f6!=''  ){
		
		$('#dyASMSProgress').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		
		
		$http.post(rCPath+'add_sms_contact', {'full_name': f1, 'mobile': f2,'email':f3,'group':f4,'country':f5,'caller_code':f6}
		).success(function(data) {
		 
				switch (data) {
				case 'success':
					$('#dyASMSProgress').html('<i class="fa fa-check-circle darkGreen"></i> Contact added successfully.');
					setTimeout('autoHideContactModal()',1000);
					break;
				case 'failed':
					$('#dyASMSProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
					setTimeout('autoHideContactModal()',1000);
					break;
				case 'duplicated':
					$('#dyASMSProgress').html('<i class="fa fa-exclamation-triangle red"></i> Contact number duplicated.');
					setTimeout('autoHideContactModal()',1000);
					break;

				}
						
				
		});
		}
		else{
			return false;
		}
	}
}

function autoHideContactModal(){
	$('#modalAddContact').modal('hide');
	window.location.reload();
}


/////// load sms group form
function triggerSMSGroup(){
	
	$('#modalAddSMSGroup').modal('show');
}
function triggerSMSTitle(){
	$('#modalAddSMSTitle').modal('show');
}
function addSMSGroupFrmCtrl($scope, $http) {
		$scope.addSMSGroupBtn = function() {
		var validate=$('#addSMSGroupForm').bootstrapValidator('validate');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		
		var f1=$('#group_name').val();
		var f2=$('#group_type').val();
		
		
		if(f1!=''){
		
		$('#dyAGrpProgress').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		
		
		$http.post(rCPath+'add_sms_group', {'group_name': f1, 'group_type': f2}
		).success(function(data) {
		 
		if (data!='failed')
		{
			
			$('#dyAGrpProgress').html('<i class="fa fa-check-circle darkGreen"></i> Group added successfully.');
			setTimeout('autoHideGroupModal()',1000);
			
		}
		else
		{
			
			$('#dyAGrpProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			setTimeout('autoHideGroupModal()',1000);
			
		}
		});
		}
		else{
			return false;
		}
	}
}

function autoHideGroupModal(){
	
	 $('#modalAddSMSGroup').modal('hide');
	 window.location.reload();
	
}
function autoHideTTemplateModal(){
	
	 $('#modalAddSMSTitle').modal('hide');
	 window.location.reload();
	
}

//// adding template title
function addSMSTitleFrmCtrl($scope, $http) {
		$scope.addSMSTitleBtn = function() {
		var validate=$('#addSMSTitleForm').bootstrapValidator('validate');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		
		var f1=$('#template_code').val();
		var f2=$('#template_title').val();
		
		
		if(f2!=''){
		
		$('#dyATTProgress').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		
		
		$http.post(rCPath+'add_template_title', {'template_code': f1, 'template_title': f2}
		).success(function(data) {
		 
		if (data!='failed')
		{
			
			$('#dyATTProgress').html('<i class="fa fa-check-circle darkGreen"></i> Title added successfully.');
			setTimeout('autoHideTTemplateModal()',1000);
			
		}
		else
		{
			
			$('#dyATTProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			setTimeout('autoHideTTemplateModal()',1000);
			
		}
		});
		}
		else{
			return false;
		}
	}
}


/////////////// Delete group controller ///////////////////

app.controller('delSMSGroupCtrl', function($scope) {
    var group_id = $scope.data.cgroup_id;
    $scope.delSMSGroup = function() {

        var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
	    
	   $('#delGroupModalConfirm'+group_id).modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function() {
			
			$("#callGroupDelstatus"+group_id).html('<i class="fa fa-spinner fa-spin"></i> <small>Please wait..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_sms_group",
				data: {group_id:group_id}
				})
				.done(function( msg ) {
				
				 if(msg!='success'){
					$.notify({
					icon: 'pe-7s-check',
					message: "Group ID "+group_id+" deleted successfully."

					},{
					type: 'success',
					timer: 4000
					});

					/*$.notify("Group ID ["+group_id+"] deleted successfully ", "success",{arrowSize: 14,style: 'bootstrap',width: '500px'});*/
					setTimeout('reloadPage()',2000);
				 }else{
					 /*$.notify('Something went wrong.', "danger");*/
					$.notify({
					icon: 'pe-7s-close',
					message: "Something went wrong."

					},{
						type: 'danger',
						timer: 4000
					});
					
				 }
			});
            
        });	
  }
});




/// delete caller number ajx
function delCallerNumber(tid){
	
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	 	$.ajax({
		type: "POST",
		url: rCPath+"caller_number_headline",
		data: {tId: tid}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var cn_tid=fly_info[0];
				$('#dlx_cn_number').html(fly_info[1]);

				//// process instant 
				$('#delCallerNumberModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callCNDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_caller_number",
				data: {cn_tid:cn_tid}
				})
				.done(function( msg ) {

					if(msg!='failed'){
					$.notify({
						icon: 'pe-7s-check',
						message: "Caller number "+fly_info[1]+" deleted successfully."

					},{
						type: 'success',
						timer: 4000
					});
					
					/*$.notify("Caller number ["+fly_info[1]+"] deleted successfully ", "success",{arrowSize: 14,style: 'bootstrap',width: '500px'});*/
					setTimeout('reloadPage()',2000);
					}else{
					   
					   /*$.notify('Something went wrong.', "danger");*/
					   $.notify({
						icon: 'pe-7s-close',
						message: "Something went wrong."

						},{
							type: 'danger',
							timer: 4000
						});

					}
				});

				});	
				//// end 
			   
				
			}else{
						$.notify({
						icon: 'pe-7s-close',
						message: "Something went wrong."

						},{
							type: 'danger',
							timer: 4000
						});
				//alert('Something went wrong.');
				//$('#dy_load_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}

//// Load contacts at group
function lgcCntrl($scope, $http){
	
    var group_id = $scope.data.cgroup_id;
	var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		
		$http.post(rCPath+'load_group_contacts', {'group_id': group_id}
				).success(function(data) {
					//alert(data);
					$('#dylgcCntrl'+group_id).css('font-size','32px');
					$('#dylgcCntrl'+group_id).css('display','none');
					$('.rlgcLoaded'+group_id).html(data);
					
		 })
	
 
}


//// Load contacts name
function lgcNameCntrl($scope, $http){
	
    var group_id = $scope.data.contact_group;
	var c_id = $scope.data.c_id;
	var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		
		$http.post(rCPath+'load_group_name', {'group_id': group_id}
				).success(function(data) {
					//alert(data);
					$('#dylgcNameCntrl'+group_id+c_id).css('font-size','32px');
					$('#dylgcNameCntrl'+group_id+c_id).css('display','none');
					$('.rlgcNameLoaded'+group_id+c_id).html(data);
					
		 })
	
 
}

//// Load group at history
function historyGroupCntrl($scope, $http){
	
    var group_id = $scope.data.sms_group;
	var camp_id = $scope.data.campaign_id;
	var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		
		$http.post(rCPath+'campaign_history_grpname', {'group_id': group_id}
				).success(function(data) {
					//alert(data);
					$('#dySmsHgNameCntrl'+group_id+camp_id).css('font-size','32px');
					$('#dySmsHgNameCntrl'+group_id+camp_id).css('display','none');
					$('.rlgSmsHgNameLoaded'+group_id+camp_id).html(data);
					
		 })
	
 
}

function reloadPage(){
	window.location.reload();
}

/////// load buy number modal
function triggerBuyNumberModal(){
	
	$('#modalBuyNumber').modal('show');
}
function autoCNHideModal(){
	$('#modalBuyNumber').modal('hide');
}

//// display caller number prices

function displayCNPrice(val){
	
	var spVals=val.split('-');
	$('#cn_projected_price').html(spVals[3]+' '+spVals[1]);
	$('#cn_period_content').html('Per '+spVals[2]+' Month(s)');
	$('#hidCNdyPrice').val(spVals[1]);
	
	$('#total_cn_price').val('');
	$('#cn_lease_period').val('');
	
	
	
}
function totalCNPrice(val){
	var price=$('#hidCNdyPrice').val();
	if(price==''){alert('Please select country');}
	var total= val * price;
	$('#total_cn_price').val(total);
	
}

function buyNumberFrmCtrl($scope, $http) {
		$scope.buyCNumberBtn = function() {
		var validate=$('#buyCNumberForm').bootstrapValidator('validate');
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		
		var f1=$('#cn_type').val();
		var f2=$('#cn_country').val();
		var f3=$('#cn_lease_period').val();
		var f4=$('#hidCNdyPrice').val();
		var f5=$('#total_cn_price').val();
		var f6=$('#hidWalletBalance').val();
		var f7= $('#hidWalletReference').val();
		
		
		if(parseInt(f6) < parseInt(f5))
		{
			alert('You have insufficient funds, please topup your wallet.'); 
			return false;
		} 
		else 
		{
		
			if(f1!='' && f2!='' && f3!='' ){
			
			$('#dyCNProgress').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
			
			
			$http.post(rCPath+'buy_caller_number', {'cn_type': f1,'cn_country': f2,'cn_lease_period': f3
			,'hid_cnprice': f4,'total_cn_price': f5,'hid_wallet_credit': f6,'hid_wallet_reference': f7}
			).success(function(data) {
			 alert(data);
			if (data!='failed')
			{
				
				$('#dyCNProgress').html('<i class="fa fa-check-circle darkGreen"></i> Purchase request submitted successfully.');
				setTimeout('autoCNHideModal()',1000);
				
			}
			else
			{
				
				$('#dyCNProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				setTimeout('autoCNHideModal()',1000);
				
			}
			})
			}
			else{
				return false;
			}
		}
	}
}

/// assign group to modal
 function fetchGrouptoFromModal(){
	$('#modalFetchGrouptoFrom').modal('show');
	$('#hidCampaignMode').val('group');
}

/// assign contact to modal
function fetchContacttoFromModal(){
	$('#modalFetchContacttoFrom').modal('show');
	$('#hidCampaignMode').val('contact');
}


/// load notification body modal
function loadNotifyBody(ntf_id){
	
	$('#NotifyBodyModal').modal('show');
	
	///// load msg , change read status
	var parsePath=$(location).attr('href');
	var re = new RegExp(/^.*\//);
	var rCPath= re.exec(window.location.href);
	$('#dyNotifyBody').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
	$.ajax({
		type: "POST",
		url: rCPath+"load_notification",
		data: {notify_id:ntf_id}
		})
		.done(function(msg) {
		 $('#dyNotifyBody').html(msg);
		
	});
		
}

function closeAssignGroup(){
	$('#modalFetchGrouptoFrom').modal('hide');
}

////// add campaign process 
	
function campaignSMSFormCtrl($scope, $http) {
		$scope.addCampaignSMS = function() {
		
        var campaign_mode=$('#hidCampaignMode').val();
		var validate=$('#campaignSMSForm').bootstrapValidator('validate');
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		
		var f1=$('#hidAssignGrpToken').val();
		var f2=$('#campaign_from').val();
		var f3=$('#campaign_text').val();
		var f4=$('#hidCampCode').val();
		var projectedPrice =$('#hidProPrice').val();
		var accountCredit =$('#hidACredit').val();
		var scheduleTime=$('#schedule_input').val();
		var campTimezone=$('#camp_timezone').val();
		var mCNlist=$('#campaign_to_list').val();
		
		///// checking sender id mark
	    var chkSid;
		if($("#use_sid").is(':checked')){
		
			chkSid=1;
			//alert(chkSid);
				
		}else{
			chkSid=0;
			//alert(chkSid);

		}
		
		if(f2!='' && f3!='' ){
			
			if(projectedPrice < accountCredit){
				
				// prompting non listed contact alert
				$('#cnfContactAlert').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#cnf_confirm', function() {
						$('#dyCampaignProgress').html('<span class="text-danger"><i class="fa fa-spinner fa-spin"></i> Submitting your campaign, please wait..</span>');
						//alert(rCPath);
						$http.post(rCPath+'process_sms_campaign', {'to_tokens': f1, 'from_token': f2,'message':f3,
						'chksid':chkSid,'camp_code':f4,'mode':campaign_mode,'schedule_time':scheduleTime,'mcn_list':mCNlist,'camp_timezone':campTimezone}
						).success(function(data) {
						 //alert(data);
						if (data!='failed')
						{
							
							$('#dyCampaignProgress').html('<i class="fa fa-check-circle darkGreen"></i> Campaign submitted successfully.');
							//reloadPage();
							startSmsService();
							
						}
						else
						{
							
							$('#dyCampaignProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
							
						}
						});
				
				});	// end confirm
			
				
			}else{
				$('#modalCreditAlert').modal('show');
				return false;
			}
		}
		else{
			return false;
		}
	}
}


/////// load sender id modal
function triggerModalsenderId(tid){
	$('#modalSenderIdReq').modal('show');
	var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		$('#cnmdx_process').slideDown();
		$('#cnmdx_process').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$('#sender_number').val('loading..');
		
		$.ajax({
		type: "POST",
		url: rCPath+"caller_number_headline",
		data: {tId: tid}
		})
		.done(function(data) {
			//alert(data)
			if (data!='failed')
			{
			  $('#cnmdx_process').slideUp();
			  //alert(data);	
			   var fly_info=data.split('__');	 
			   $('#cn_tid').val(fly_info[0]);
			   $('#sender_number').val(fly_info[1]);
			   $('#cn_status').val(fly_info[2]);
			   
				
			}else{
				$('#cnmdx_process').slideDown();
				$('#cnmdx_process').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}

function SenderIdReqCtrl($scope, $http){
	/*$scope.senderIdReqlnk = function() {
		var tid= $scope.data.tid;	
		$('#modalSenderIdReq'+tid).modal('show');
	}*/
	
	//// process btn
	$scope.senderIdReqBtn = function() {
		
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		var tid= $('#cn_tid').val();	
		var f1=$('#sender_number').val();
		var f2=$('#sender_id_name').val();
		var status= $('#cn_status').val();
		if(f2.length>12){alert('Sorry you exceeded maximum character limit.'); return false;}
		if(status!='1'){alert('Sorry we cant process your request, since your mobile number is not yet active.');return false;}
		
		if(f1!='' && f2!=''){
		    
			$('#dySIdReqProgress').html('<span class="text-muted"><i class="fa fa-spinner fa-spin"></i> Request sending please wait..</span>');
			
			//alert(rCPath);
			$http.post(rCPath+'process_sender_id', {'sender_number': f1, 'sender_name': f2,'tid':tid}
			).success(function(data) {
			 //alert(data);
			if (data!='failed')
			{
				
				$('#dySIdReqProgress').html('<i class="fa fa-check-circle darkGreen"></i> Your request sent successfully, we need 24-36 hours time to update your request.');
				reloadPage();
				
			}
			else
			{
				
				$('#dySIdReqProgress').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				
			}
			})
		}
		else{
			alert('Please fill the mandatory fields');
			//$('#dySIdReqProgress'+tid).html('<i class="fa fa-exclamation red"></i> Please fill the mandatory fields.');
			//setTimeout('autoHideSenderIdError(tid)',2000);
			return false;
		}
		
	}
}


function failRemarkslnk(cmp_id){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#modalSMSRemarks').modal('show');
		$('#rsRemarks').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$.ajax({
		type: "POST",
		url: rCPath+"load_sms_remarks",
		data: {cId: cmp_id}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
			    $('#rsRemarks').html(data);
				
			}else{
				$('#rsRemarks').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}

function failRemarksBackend(cmp_id,uid){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	    $('#modalSMSRemarks').modal('show');
		$('#rsRemarks').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$.ajax({
		type: "POST",
		url: rCPath+"load_sms_remarks",
		data: {cId: cmp_id,uId: uid}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
			    $('#rsRemarks').html(data);
				
			}else{
				$('#rsRemarks').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
}



function triggerCCSingle(c_id){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	$('#ccSingleModal').modal('show');
	$('#md_group').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$.ajax({
		type: "POST",
		url: rCPath+"load_modal_contact",
		data: {cId: c_id}
		})
		.done(function(data) {
			//alert(data)
			if (data!='failed')
			{
			  
			  //alert(data);	
			   var fly_info=data.split('__');	 
			   $('#mxc_id').val(fly_info[0]);
			   $('#mxc_mobile').val(fly_info[1]);
			   $('#mxc_name').val(fly_info[2]);
			   $('#mxc_email').val(fly_info[3]);
			   $('#mxc_ccode').val(fly_info[4]);
			   $('#md_title').html(fly_info[2]);
			   $('#md_group').html(fly_info[5]);
			   
				
			}else{
				$('#dy_load_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
	
}
///// process copy single contact 
function copySingleCtrlFrm($scope, $http) {
		$scope.copyCCSingleBtn = function() {
		var validate=$('#ccSingleCtrlForm').bootstrapValidator('validate');
		var id=$('#mxc_id').val();
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		var f1=isNaN($scope.cpto_group);
		var mxc_mobile=$('#mxc_mobile').val();
		var mxc_name=$('#mxc_name').val();
		var mxc_email=$('#mxc_email').val();
		var mxc_ccode=$('#mxc_ccode').val();
		
		if(!f1){
		    
			$('#ccStatus').html('<span class="text-muted"><i class="fa fa-spinner fa-spin"></i> Coping please wait..</span>');
			//alert(rCPath);
			$http.post(rCPath+'process_cc_single', {'contact_id': id, 'to_group': $scope.cpto_group,'c_mobile':mxc_mobile,
			'c_name':mxc_name,'c_email':mxc_email,'country_code':mxc_ccode}
			).success(function(data) {
			 //alert(data);
			if (data!='failed')
			{
				$('#ccStatus').html('<i class="fa fa-check-circle darkGreen"></i> Contact copied successfully.');
				reloadPage();
			}
			else
			{
				
				$('#ccStatus').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				
			}
			})
		}
		else{
			alert('Missing mandatory fields.');
			return false;
		}
		
		}
}

/////// move single contact 
function triggerMvSingle(c_id) {
	   
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		$('#mvSingleModal').modal('show');
		$('#mv_md_group').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$.ajax({
		type: "POST",
		url: rCPath+"load_modal_contact",
		data: {cId: c_id}
		})
		.done(function(data) {
			//alert(data)
			if (data!='failed')
			{
			  
			  //alert(data);	
			   var fly_info=data.split('__');	 
			   $('#mv_mxc_id').val(fly_info[0]);
			   $('#mv_mxc_mobile').val(fly_info[1]);
			   $('#mv_mxc_name').val(fly_info[2]);
			   $('#mv_mxc_email').val(fly_info[3]);
			   $('#mv_mxc_ccode').val(fly_info[4]);
			   $('#mv_md_title').html(fly_info[2]);
			   $('#mv_md_group').html(fly_info[5]);
			   
				
			}else{
				$('#load_mvmodal_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
		
		
}

///// process move single contact 
function mvSingleCtrlFrm($scope, $http) {
		$scope.mvSingleCBtn = function() {
		var validate=$('#mvSingleCtrlForm').bootstrapValidator('validate');
		
		
		var id=$('#mv_mxc_id').val();
		var re = new RegExp(/^.*\//);
        var rCPath= re.exec(window.location.href);
		var parsePath=$(location).attr('href');
		var f1=isNaN($scope.to_group);
		var mxc_mobile=$('#mv_mxc_mobile').val();
		var mxc_name=$('#mv_mxc_name').val();
		var mxc_email=$('#mv_mxc_email').val();
		var mxc_ccode=$('#mv_mxc_ccode').val();
		//alert('CID:'+id);
		if(!f1){
		    
			$('#mvStatus').html('<span class="text-muted"><i class="fa fa-spinner fa-spin"></i> Moving please wait..</span>');
			
			//alert(mxc_ccode);
			$http.post(rCPath+'process_mv_single', {'contact_id': id, 'to_group': $scope.to_group,'c_mobile':mxc_mobile,
			'c_name':mxc_name,'c_email':mxc_email,'country_code':mxc_ccode}
			).success(function(data) {
			// alert(data);
			if (data!='failed')
			{
				
				$('#mvStatus').html('<i class="fa fa-check-circle darkGreen"></i> Contact moved successfully.');
				reloadPage();
				
			}
			else
			{
				
				$('#mvStatus').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
				
			}
			})
		}
		else{
			alert('Please fill the mandatory fields');
			//$('#dySIdReqProgress'+tid).html('<i class="fa fa-exclamation red"></i> Please fill the mandatory fields.');
			//setTimeout('autoHideSenderIdError(tid)',2000);
			return false;
		}
		
		}
}



/// delete sms contact ajx
function delSMSContact(c_id){
	var re = new RegExp(/^.*\//);
    var rCPath= re.exec(window.location.href);
	
	   // $('#callContactDelstatus').html('<i class="fa fa-spinner fa-spin"></i> Please wait..');
		$.ajax({
		type: "POST",
		url: rCPath+"load_modal_contact",
		data: {cId: c_id}
		})
		.done(function(data) {
			
			if (data!='failed')
			{
				var fly_info=data.split('__');	 
				var contact_id=fly_info[0];
				$('#dlx_contact_name').html(fly_info[2]);

				//// process instant 
				$('#delSmsContactModal').modal({ backdrop: 'static', keyboard: false })
				.one('click', '#delete', function() {

				
				$("#callContactDelstatus").html('<i class="fa fa-spinner fa-spin"></i> <small>Deleting..</small>');
				$.ajax({
				type: "POST",
				url: rCPath+"delete_sms_contact",
				data: {contact_id:contact_id}
				})
				.done(function( msg ) {

					if(msg!='failed'){

					$.notify({
						icon: 'pe-7s-check',
						message: "Contact "+fly_info[2]+" deleted successfully."

						},{
						type: 'success',
						timer: 4000
						});

					/*$.notify("Contact ["+fly_info[2]+"] deleted successfully ", "success",{arrowSize: 14,style: 'bootstrap',width: '500px'});*/
					setTimeout('reloadPage()',2000);
					}else{
					 /*$.notify('Something went wrong.', "danger");*/
						$.notify({
						icon: 'pe-7s-close',
						message: "Something went wrong."

						},{
						type: 'danger',
						timer: 4000
						});

					}
				});

				});	
				//// end 
			   
				
			}else{
				alert('Something went wrong.');
				//$('#dy_load_contact').html('<i class="fa fa-times-circle red"></i> Something went wrong.');
			}

		});
	
}


function assignContact(){
	
	    /* declare an checkbox array */
		var chkArray = [];
		var inchkArray=[];
		var cFtVals="";
		var campaign_mode=$('#hidCampaignMode').val();
		/*$scope.albumNameArray = [];
		angular.forEach($scope.filtered, function(data){
		if (!!data.selected) $scope.albumNameArray.push(data.c_id);
	              //alert(data.c_id);
		});*/
		
		
	 
		/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
		$(".achk_contact:checked").each(function() {
		
			var crossVal=$(this).val();
			var inrCrossVal=crossVal.split('_');
			chkArray.push(inrCrossVal[1]);
			inchkArray.push(inrCrossVal[0]);
		});
		
		/* we join the array separated by the comma */
		var selected;
		var depthSelected;
		
		selected = chkArray.join(',') + ",";
		depthSelected=inchkArray.join(',') + ",";
		
		if(chkArray.length > 10){
			alert("Sorry you exceeded maximum contact selection");
			return false;
		}
		
		/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
		if(selected.length > 1){
			$('#campaign_to_list').attr('readonly', true);
			$('#campaign_to_list').val(selected);
			$('#hidAssignGrpToken').val(depthSelected);	
			$('#clearToContacts').slideDown(1000);
			
			$('#modalFetchContacttoFrom').modal('hide');
			/* displaying projected price */
			var re = new RegExp(/^.*\//);
			var rCPath= re.exec(window.location.href);
			   $("#proacc_price").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
				$.ajax({
				type: "POST",
				url: rCPath+"get_projected_price",
				data: {gid:depthSelected,mode:campaign_mode}
				})
				.done(function(msg) {
				 $('#hidProPrice').val(msg);
				 $('#proacc_price').html(msg);
				
			});
			
		}else{
			$('#campaign_to_list').val('');
			$('#hidAssignGrpToken').val('');	
			$('#campaign_to_list').attr('readonly', false);
			$('#clearToContacts').slideUp(1000);
			alert("Please at least one of the checkbox");	
			
			/* displaying projected price */
			var re = new RegExp(/^.*\//);
			var rCPath= re.exec(window.location.href);
			   $("#proacc_price").html('<small><i class="fa fa-circle-o-notch fa-spin"></i></small>');
				$.ajax({
				type: "POST",
				url: rCPath+"get_projected_price",
				data: {gid:'',mode:campaign_mode}
				})
				.done(function(msg) {
				
				 $('#hidProPrice').val(msg);
				 $('#proacc_price').html(msg);
				
			});
				
		}
	
}

function clearToContacts(){
	$('#campaign_to_list').val('');
	$('#hidAssignGrpToken').val('');
	$('#campaign_to_list').attr('readonly', false);
	$('#proacc_price').html('00.000');
	$('#clearToContacts').slideUp();
	$('input[name="achk_contact_all"]').prop('checked',false);  
	$('input[name="achk_contact"]').prop('checked',false);  
	
}

/// fetching title as message
function triggerAddTitle(){
	$('#modalFetchTitleFrom').modal('show');
	
}

/// caller number remarks
function cn_remarks(remarks){
	$('#cn_remarks').val(remarks);
	$('#cnRemarksModal').modal('show');
	
}

/// how to apply senderid
function triggerApplySenderId(){
	$('#modalApplySenderId').modal('show');
	
}

/// To numbers in campaign
function triggerNumberInfo(){
	$('#modalNumberInfo').modal('show');
	
}
/// Search history by dates
function triggerSearchHistoryDates(){
	$('#searchHistByDateAlert').modal('show');
	
}

/// checking callerid availability
function check_callerid_available(key){
		
		if(key.length < 12){
			var re = new RegExp(/^.*\//);
			var rCPath= re.exec(window.location.href);
			$("#dySIdReqProgress").html('<i class="fa fa-spinner fa-spin"></i> Checking availability..');
			$.ajax({
			type: "POST",
			url: rCPath+"check_caller_id",
			data: {key:key}
			})
			.done(function(msg) {
			
					$('#dySIdReqProgress').html(msg);
				
			});
		}else{
			       $('#dySIdReqProgress').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Sorry exceeded character limit.</span>');
		}
}

function assignTitle(){
	
	    /* declare an checkbox array */
		var chkArray = [];
		var inchkArray=[];
		var cFtVals="";
		
		/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
		$(".achk_title:checked").each(function() {
		
			var crossVal=$(this).val();
			var inrCrossVal=crossVal.split('___');
			chkArray.push(inrCrossVal[1]);
			inchkArray.push(inrCrossVal[0]);
		});
		
		/* we join the array separated by the comma */
		var selected;
		var depthSelected;
		
		selected = chkArray.join(',') + ",";
		depthSelected=inchkArray.join(',') + ",";
		
		if(chkArray.length > 1){
			alert("Sorry you exceeded maximum title selection");
			return false;
		}
		
		/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
		if(selected.length > 1){
			$('#campaign_text').val(selected);
			$('#modalFetchTitleFrom').modal('hide');
			/* displaying projected price */
			
			
		}else{
			$('#campaign_text').val('');	
			alert("Please at least one of the checkbox");	
			
				
		}
	
}

function toggleSchedule(){
	 $( "#schedule_message" ).toggle("slow");
}



