<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Internal_Settings{

    private $ci;
    public function __construct()
    {
		$this->ci=& get_instance();
		$this->ci->load->database();
        
    }
	
	public function local_settings()
	{
			return  $this -> ci->db->get("oc_app_settings")->result();
			
	}
	public function guest_enquiries()
	{
		    
			$this -> ci->db-> select('*');
			$this -> ci->db->from('oc_guest_enquiry');
			$this -> ci->db->join('oc_countries','oc_countries.country_code=oc_guest_enquiry.enq_country');
			$this -> ci->db->group_by('oc_guest_enquiry.enq_id');
			$query=$this -> ci->db->get();
				
			if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
			}else{
				return 0;
			}
			  
			
	}
	
	
}