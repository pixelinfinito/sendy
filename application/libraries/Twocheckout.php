<?php

abstract class Twocheckout
{
    public static $sid='901316091';
    public static $privateKey='F0BC2305-A262-4F98-ADBB-8428D1CBACA2';
    public static $username='sybase123';
    public static $password='Systems@321#';
    public static $sandbox=true;
    public static $verifySSL = true;
    public static $baseUrl = 'https://www.2checkout.com';
    public static $error;
    public static $format = 'array';
    const VERSION = '0.3.1';

    public static function sellerId($value = null) {
        self::$sid = $value;
    }

    public static function privateKey($value = null) {
        self::$privateKey = $value;
    }

    public static function username($value = null) {
        self::$username = $value;
    }

    public static function password($value = null) {
        self::$password = $value;
    }

    public static function sandbox($value = null) {
        if ($value == 1 || $value == true) {
            self::$sandbox = true;
            self::$baseUrl = 'https://sandbox.2checkout.com';
        } else {
            self::$sandbox = false;
            self::$baseUrl = 'https://www.2checkout.com';
        }
    }

    public static function verifySSL($value = null) {
        if ($value == 0 || $value == false) {
            self::$verifySSL = false;
        } else {
            self::$verifySSL = true;
        }
    }

    public static function format($value = null) {
        self::$format = $value;
    }
}

require_once(APPPATH .'libraries/Twocheckout/Api/TwocheckoutAccount.php');
require_once(APPPATH . 'libraries/Twocheckout/Api/TwocheckoutPayment.php');
require_once(APPPATH . 'libraries/Twocheckout/Api/TwocheckoutApi.php');
require_once(APPPATH . 'libraries/Twocheckout/Api/TwocheckoutSale.php');
require_once(APPPATH . 'libraries/Twocheckout/Api/TwocheckoutProduct.php');
require_once(APPPATH . 'libraries/Twocheckout/Api/TwocheckoutCoupon.php');
require_once(APPPATH . 'libraries/Twocheckout/Api/TwocheckoutOption.php');
require_once(APPPATH . 'libraries/Twocheckout/Api/TwocheckoutUtil.php');
require_once(APPPATH . 'libraries/Twocheckout/TwocheckoutReturn.php');
require_once(APPPATH . 'libraries/Twocheckout/TwocheckoutNotification.php');
require_once(APPPATH . 'libraries/Twocheckout/TwocheckoutCharge.php');
require_once(APPPATH . 'libraries/Twocheckout/TwocheckoutMessage.php');