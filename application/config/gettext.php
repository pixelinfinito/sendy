<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Gettext catalog codeset
$config['gettext_catalog_codeset'] = 'UTF-8';

// Gettext domain
$config['gettext_text_domain'] = 'messages';

// Path to gettext locale directory relative to FCPATH.APPPATH
$config['gettext_locale_dir'] = 'language/locale';

// Gettext locale
//$config['gettext_locale'] = Array( "en_GB.UTF-8", "en_GB@euro", "en_GB", "english", "eng", "en");
//$config['gettext_locale'] = Array( "ru_RU.UTF-8", "ru_RU", "ru");
$config['gettext_locale'] = Array( "pt_PT.UTF-8", "pt_PT", "pt");


/* End of file gettext.php */
/* Location: ./application/config/gettext.php */
