<script type="text/javascript">
	// set minutes
	var mins = 2;
	 
	// calculate the seconds (don't change this! unless time progresses at a different speed for you...)
	var secs = mins * 60;
	function countdown() {
	setTimeout('Decrement()',1000);
	}
	function Decrement() {
	if (document.getElementById) {
	minutes = document.getElementById("minutes");
	seconds = document.getElementById("seconds");
	// if less than a minute remaining
	if (seconds < 59) {
	seconds.value = secs;
	} else {
	minutes.value = getminutes();
	seconds.value = getseconds();
	}
	secs--;
	setTimeout('Decrement()',1000);
	}
	}
	function getminutes() {
	// minutes is seconds divided by 60, rounded down
	mins = Math.floor(secs / 60);
		if(mins==0&&secs==0)
		{
			document.getElementById("dispRCode").style.display="block"; 
			document.getElementById("timer").style.display="none"; 
			return false;
		}
		else{
			return mins;
		}
	}
	function getseconds() {
	// take mins remaining (as seconds) away from total seconds remaining
	return secs-Math.round(mins *60);
	}
</script>
<?php

//session_start();

$username=$_GET['username'];
$user_id=$_GET['user_id'];

$mobile_parse=$_GET['mobile'];
$mobile_1=substr($mobile_parse,0,3);

if($mobile_1=='673'){
	$mobile='+'.$mobile_parse;
}
else{
$mobile='+673'.$mobile_parse;
}

	$ci=& get_instance();
	$ci->load->database();
	$ci->db->select('*');
	$ci->db->where('user_id',$user_id);
	$ci->db->where('status','1');
	$ci->db->limit(1);
	$row = $ci->db->get('oc_mobile_validation')->row();



	if($row->user_id=='')
	{

		$ci=& get_instance();
		$ci->load->database();
		$ci->db->select('*');
		$ci->db->where('user_id',$user_id);
		$ci->db->where('status','0');
		$ci->db->limit(1);
		$row2 = $ci->db->get('oc_mobile_validation')->row();


		$parseRowId=$row2->id ;
		if($row2->user_id ==''){
		$authStatus='NotRecordedYet';
		}
		else{
		$authStatus='NotVerified';
		}

	}
	else
	{

		$authCode=$row->mobile_validate_code;
		$authStatus='Verified';
		$parseRowId=$row->id;
		$actStatus=$row->mobile_validate;    				

	}
  
?>

		<link rel="stylesheet" href="../css/style.css">
        <style>
		 body{background:#FFF;overflow:none;margin:0 0 0 0;height:auto;min-width:auto}p{font-size:14px;color:#333;font-family:'ColaborateRegular';color:#0ab271;}.mv_adjust{margin-bottom:10px;}
		</style>
   
        <div class="center">
            <span id="message">
            <?php 
			if($authStatus=='NotVerified'){
            /*if(isset($_SESSION['password'])){*/
            echo "<p><strong>Hi " . $username;
            //echo "<p>The Two Factor Password that was sent to your phone was: " . $_SESSION['password'] . "</p>";
            echo " </strong>&nbsp;, Please compare this to the password that was sent to your mobile device.</p>";
			
            ?>
			 <form id="passAuthKey" name="passAuthKey" action="validateMobileProcess.php" method="post">
			   <table cellpadding="0" cellspacing="0" border="0" class="form">
			   <tr>
			   <td>Enter Authentication Password : </td>
			   <td>
			   <input type="text" name="parseAuthkey" id="parseAuthkey" value="" class="form-control"> 
			   <input type="hidden" name="parse_user_id" id="parse_user_id" value="<?php echo $user_id;?>">
			   <input type="hidden" name="parse_row_id" id="parse_row_id" value="<?php echo $parseRowId;?>">
			   </td>
			   </tr>
			   <tr>
			    <td>
				 <div class="mv_adjust">
				  Resend password waiting time : 
				 </div>
				</td>
			    <td >
				
				<div class="mv_adjust">
			    <span id="timer" style="font-size:13px;color:#333">
                 <input id="minutes" type="text" style="color:#333;width:50px"> Minutes <input id="seconds" type="text" style="color:#333;width:50px"> Seconds.
				</span>
				<span style="font-size:13px;display:none" id="dispRCode">
				 <a href="reSendCode.php?username=<?php echo $username;?>&user_id=<?php echo $user_id;?>&mobile=<?php echo substr($mobile,1);?>">Resend password </a> 
				</span>
				<script>
				countdown();
				</script>
			   </div>
				</td>
			   </tr>
			   <tr>
			    <td> 
				 <input type="submit" name="submitAuthKey" id="submitAuthKey" value="Validate">
				 
				</td>
				<td> &nbsp;</td>
			   </tr>
			   </table>
			 </form>
            <?php	
             }
            			 
			// }
			else if($authStatus=='Verified'){echo "<span style='color:#0ab271;font-family:ColaborateRegular'>" . _('You mobile already verified.') . "</span>";}
			else {
			?>
            </span>
        </div>
        <form id="generateTwoFA" action="twoFactorAuthProcessor.php" method="POST" class="center">
		<table cellpadding="0" cellspacing="0" border="0" class="form">
		<tr>
		<td>
        <div id="userName" class="ui-resizable-disabled ui-state-disabled">
               <label for="userName">Your Name</label>
		 </td>
		 <td>
               <input type="text" name="userName" id="userName" required="required" value="<?php echo $username;?>">
          </div>
		 </td>
		 </tr>
		 <tr>
		 <td>
          <div id="tel_number" class="ui-resizable-disabled ui-state-disabled">
               <label for="tel_number">Your phone number</label>
		 </td>
		 <td>
               <input type="tel" name="tel_number" id="tel_number" required="required" class="tel_number" value="<?php echo $mobile;?>">
		       <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
		</div>
		</td>
		</tr>
		<tr>
		<td colspan="2">
		<div id="method" class="ui-resizable-disabled ui-state-disabled">
		<div class="mv_adjust">
		<label for="method"><strong> Deliver Code Via: </strong><br></label>
		</div>
		
		<div class="mv_adjust">
			<input type="radio" name="method" value="sms">SMS Message
			<input type="radio" name="method" value="voice">Voice Call
		</div>
		</td>
		</tr>
		 <tr>
		 <td colspan="2">
         <div id="form-submit" class="field f_100 clearfix submit">
               <input type="submit" value="Submit">
          </div>
		  </td>
		 </tr>
		 </table>
        </form>
       <?php
	    }
	   ?>
	 