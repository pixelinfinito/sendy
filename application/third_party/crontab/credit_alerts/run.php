<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on"){
	$prototype='https://';
}else{
	$prototype='http://';
}
$parseURL=$prototype.$_SERVER['HTTP_HOST'];

$homepage = file_get_contents('https://onetextglobal.com/cli/credit');
echo $homepage;
?>