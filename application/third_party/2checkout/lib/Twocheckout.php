<?php


abstract class Twocheckout 
{

    
    public static $sid='901316091';
    public static $privateKey='F0BC2305-A262-4F98-ADBB-8428D1CBACA2';
    public static $username='sybase123';
    public static $password='Systems@321#';
    public static $sandbox=true;
    public static $verifySSL = true;
    public static $baseUrl='https://www.2checkout.com';
    public static $error;
    public static $format = 'array';
	
	
    const VERSION = '0.3.1';
  
	
    public static function sellerId($value = null) {
        self::$sid = $value;
    }

    public static function privateKey($value = null) {
        self::$privateKey = $value;
    }

    public static function username($value = null) {
        self::$username = $value;
    }

    public static function password($value = null) {
        self::$password = $value;
    }

    public static function sandbox($value = null) {
		$ci=& get_instance();
		$ci->load->database();
		$ci->db->select('*');
		$row = $ci->db->get('oc_payment_settings')->row();
       
        if ($value == 1 || $value == true) {
            self::$sandbox = true;
            self::$baseUrl = $row->tco_sandbox_url;
        } else {
            self::$sandbox = false;
            self::$baseUrl = $row->tco_base_url;
        }
    }

    public static function verifySSL($value = null) {
        if ($value == 0 || $value == false) {
            self::$verifySSL = false;
        } else {
            self::$verifySSL = true;
        }
    }

    public static function format($value = null) {
        self::$format = $value;
    }
}

require('Twocheckout/Api/TwocheckoutAccount.php');
require('Twocheckout/Api/TwocheckoutPayment.php');
require('Twocheckout/Api/TwocheckoutApi.php');
require('Twocheckout/Api/TwocheckoutSale.php');
require('Twocheckout/Api/TwocheckoutProduct.php');
require('Twocheckout/Api/TwocheckoutCoupon.php');
require('Twocheckout/Api/TwocheckoutOption.php');
require('Twocheckout/Api/TwocheckoutUtil.php');
require('Twocheckout/Api/TwocheckoutError.php');
require('Twocheckout/TwocheckoutReturn.php');
require('Twocheckout/TwocheckoutNotification.php');
require('Twocheckout/TwocheckoutCharge.php');
require('Twocheckout/TwocheckoutMessage.php');
