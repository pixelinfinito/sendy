<?php 
if(!isset($this->session->userdata['site_login'])){
   $this->load->view('themeFront/header.php');
}else{
	
	$this->load->view('themeFront/header_logged.php');
}
?>
<?php $this->load->view('themeFront/banner_intro.php');?>

<div class="main-container">

    <div class="container">
        <?php
		 $catRes=$this->categories_model->list_categories_limit12();
		 //$allCat=$this->categories_model->list_categories();
		 //$allAds=$this->ads_model->load_allsite_ads();
		?>
		
		<!--/////////// Search block /////////////-->
		<?php $this->load->view('includes/search_inc.php');?>
		
        <div class="col-lg-12 content-box ">
                <div class="row row-featured row-featured-category">
                    <div class="col-lg-12  box-title no-border">
                       <div class="inner"><h2><span><?php echo _("Browse by")?></span>
                           <?php echo _("Category")?>     <a href="category.html" class="sell-your-item"> <?php echo _("View more")?> <i class="  icon-th-list"></i> </a></h2>
                       </div>
                    </div>
                     
					 <?php 
				    if(count($catRes)>0){
						for($i=0;$i<count($catRes);$i++){
							$category_Id=$catRes[$i]->category_id;
							$enc_l1 = base64_encode($category_Id);
							$encrypted_cid = str_replace(array('+','/','='),array('-','_',''),$enc_l1);	
				
							$listingCount=$this->categories_model->list_ads_bycId($category_Id);
							if($listingCount!=0){
								$adCount=count($listingCount);
							}else{
								$adCount=0;
							}
							
				    ?>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category">
                        <a href="<?php echo base_url()?>site/category/level1/<?php echo $encrypted_cid?>">
						<img src="<?php echo base_url().$catRes[$i]->thumb_path?>" class="img-responsive" alt="img"> 
						<h6> <?php echo $catRes[$i]->category_name." (".$adCount.")";?>  </h6> 
						</a>
                    </div>
                    <?php 
						}
					}
					?>
					
                </div>

            </div>

        <div style="clear: both"></div>
         
        <div class="col-lg-12 content-box">
            <div class="row row-featured">
                <div class="col-lg-12  box-title ">
                    <div class="inner"><h2><span><?php echo _("Exclusive")?> </span>
                        <?php echo _("Ads")?>     <a href="<?php echo base_url()?>site/category/exclusive" class="sell-your-item"> <?php echo _("View more")?> <i class="  icon-th-list"></i> </a></h2>

                     
                    </div>
                </div>

                <div style="clear: both"></div>

                <div class=" relative  content featured-list-row clearfix">

                    <nav class="slider-nav has-white-bg nav-narrow-svg">
                        <a class="prev">
                            <span class="nav-icon-wrap"></span>

                        </a>
                        <a class="next">
                            <span class="nav-icon-wrap"></span>
                        </a>
                    </nav>

                    <div class="no-margin featured-list-slider ">
                       
                        <?php $this->load->view('ads/exclusive_ads');?>
                    </div>

                </div>
            </div>
         </div>

         <?php $this->load->view('includes/top_cities_inc');?>

        <div class="row">
        <div class="col-sm-9 page-content col-thin-right">
          <div class="inner-box category-content ">
           <div class="box-title"><h2><span><?php echo _("Top")?> </span><?php echo _("Classifieds By")?></h2></div><br>
            <div class="row">
		 	 <!--/.page-side-bar-->
        <div class="page-content col-thin-left">
          <div class="category-list">
            <div class="tab-box " > 
              <?php 
			   @$urgentAdsCount=$this->ads_model->get_urgent_ads_count();
			   @$featuredAdsCount=$this->ads_model->get_featured_ads_count();
			   @$latestAdsCount=$this->ads_model->get_latest_ads_count();
			   
			  ?>
			 
			 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs add-tabs" id="ajaxTabs" role="tablist">
                <li class="active"><a href="#allAds" data-url="#" role="tab" data-toggle="tab"><?php echo _("Urgent")?> <span class="badge"><?php echo count($urgentAdsCount);?></span></a></li>
                <li><a href="#businessAds" data-url="#" role="tab" data-toggle="tab"><?php echo _("Featured")?> <span class="badge"><?php echo count($featuredAdsCount);?></span></a></li>
				<li><a href="#personalAds" data-url="#" role="tab" data-toggle="tab"><?php echo _("Latest")?> <span class="badge"><?php echo count($latestAdsCount);?></span></a></li>
                
              </ul>
              <div class="tab-filter">
                <select class="selectpicker" data-style="btn-select" data-width="auto">
                  <option><?php echo _("Short by")?></option>
                  <option><?php echo _("Price: Low to High")?></option>
                  <option><?php echo _("Price: High to Low")?></option>
                </select>
              </div>
            </div>
            <!--/.tab-box-->
            
            <div class="listing-filter">
              <div class="pull-left col-xs-6">
                <div class="breadcrumb-list"> <a href="#" class="current"> <span><?php echo _("All ads")?></span></a> <?php echo _("in New York")?> <a href="#selectRegion" id="dropdownMenu1"  data-toggle="modal"> <span class="caret"></span> </a> </div>
              </div>
              <div class="pull-right col-xs-6 text-right listing-view-action"> <span class="list-view active"><i class="  icon-th"></i></span> <span class="compact-view"><i class=" icon-th-list  "></i></span> <span class="grid-view "><i class=" icon-th-large "></i></span> </div>
              <div style="clear:both"></div>
            </div>
            <!--/.listing-filter-->
            
            <div class="adds-wrapper">
              <div class="tab-content">
                <div class="tab-pane active" id="allAds">
				<?php echo _("Loading...")?>
				</div>
                <div class="tab-pane" id="businessAds"></div>
                <div class="tab-pane" id="personalAds"></div>
              </div>
            </div>
            <!--/.adds-wrapper-->
            
            
          </div>
          </div>
		 
          <!--/.pagination-bar -->
		
            </div>
          </div>

		  
          <div class="inner-box has-aff relative">
              <a class="dummy-aff-img" href="category.html"><img src="<?php echo base_url()?>themeBeta/images/aff2.jpg" class="img-responsive" alt=" aff"> </a>
          </div>
        </div>
        <div class="col-sm-3 page-sidebar col-thin-left">
          <aside>
            <div class="inner-box no-padding">
              <div class="inner-box-content"> <a href="#"><img class="img-responsive" src="<?php echo base_url()?>themeBeta/images/site/app.jpg" alt="tv"></a> </div>
            </div>
            <div class="inner-box">
              <h2 class="title-2"><?php echo _("Popular Categories")?> </h2>
              <div class="inner-box-content">
                <ul class="cat-list arrow">
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Apparel (1,386")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Art (1,163")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Business Opportunities (4,974")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Community and Events (1,258")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Electronics and Appliances (1,578")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Jobs and Employment (3,609")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Motorcycles (968")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Pets (1,188")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Services (7,583")?>) </a></li>
                  <li> <a href="sub-category-sub-location.html"> <?php echo _("Vehicles (1,129")?>) </a></li>
                </ul>
              </div>
            </div>
            
            <div class="inner-box no-padding"> <img class="img-responsive" src="<?php echo base_url()?>themeBeta/images/add2.jpg" alt=""> </div>
          </aside>
        </div>
      </div>
	  
    </div>
	
  </div>
  <!-- /.main-container -->
  
  <div class="page-info" style="background: url(<?php echo base_url()?>themeBeta/images/bg.jpg); background-size:cover">
    <div class="container text-center section-promo"> 
    	<div class="row">
        	<div class="col-sm-3 col-xs-6 col-xxs-12">
                <div class="iconbox-wrap">
                          <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                            <i class="icon  icon-group"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                              <h5><span>2200</span> </h5>
                              <div  class="iconbox-wrap-text"><?php echo _("Trusted Seller")?></div>
                            </div>
                          </div>
  							<!-- /..iconbox -->
                     </div><!--/.iconbox-wrap-->
            </div>
            
            <div class="col-sm-3 col-xs-6 col-xxs-12">
            	<div class="iconbox-wrap">
                          <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                            <i class="icon  icon-th-large-1"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                              <h5><span>100</span> </h5>
                              <div  class="iconbox-wrap-text"><?php echo _("Categories")?></div>
                            </div>
                          </div>
  							<!-- /..iconbox -->
                     </div><!--/.iconbox-wrap-->
            </div>
            
            <div class="col-sm-3 col-xs-6  col-xxs-12">
            	<div class="iconbox-wrap">
                          <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                            <i class="icon  icon-map"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                              <h5><span>700</span> </h5>
                              <div  class="iconbox-wrap-text"><?php echo _("Location")?></div>
                            </div>
                          </div>
  							<!-- /..iconbox -->
                     </div><!--/.iconbox-wrap-->
            </div>
            
            <div class="col-sm-3 col-xs-6 col-xxs-12">
            	<div class="iconbox-wrap">
                          <div class="iconbox">
                            <div class="iconbox-wrap-icon">
                            <i class="icon icon-user"></i>
                            </div>
                            <div class="iconbox-wrap-content">
                              <h5><span>50,000</span> </h5>
                              <div  class="iconbox-wrap-text"> <?php echo _("Visitors")?></div>
                            </div>
                          </div>
  							<!-- /..iconbox -->
                     </div><!--/.iconbox-wrap-->
            </div>
            
        </div>
    
    </div>
  </div>
  <!-- /.page-info -->
  
  <div class="page-bottom-info">
      <div class="page-bottom-info-inner">
      
      	<div class="page-bottom-info-content text-center">
        	<h1><?php echo _("If you have any questions, comments or concerns, please call the Classified Advertising department at (000) 555-5555")?></h1>
            <a class="btn  btn-lg btn-primary-dark" href="tel:+000000000">
            <i class="icon-mobile"></i> <span class="hide-xs color50"><?php echo _("Call Now")?>:</span> (000) 555-5555   </a>
        </div>
      
      </div>
  </div>
  
 <script src="<?php echo base_url()?>themeBeta/assets/js/load_frontpage_ads.js"></script>  
<?php 

$this->load->view('themeFront/footer.php');

?>



