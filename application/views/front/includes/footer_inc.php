<?php 
	$intlib=$this->internal_settings->local_settings();
    $segment= $this->uri->segment(1);
	$remoteCountry=$this->config->item('remote_country_code');
	$remoteCity=$this->config->item('remote_geo_city');
	if($remoteCity!=''){
		$geoCity=$remoteCity;
		$geolatitude=$this->config->item('remote_geo_latitude');
		$geolongitude=$this->config->item('remote_geo_longitude');
	}else{
		$geoCity='';
		$geolatitude='';
		$geolongitude='';
	}
	if($this->config->item('remote_country_code')!=''){
			$countryCode=$this->config->item('remote_country_code');
	  }else{
		  $countryCode=$this->session->userdata['site_login']['ac_country'];
	  }
  
	$appSettings= $this->app_settings_model->get_primary_settings();
	$appCountry=$appSettings[0]->app_country;
	$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
	$getCountry= $this->countries_model->list_country_timezone_byCode($countryCode);
	
	
	$remoteAddress = $_SERVER['REMOTE_ADDR'];
	$adRef="OCSMSCAMP";
	if($segment=='home'){
	$upAdVisit=$this->visitors_model->trigger_geo_visit($adRef,$remoteAddress,$geoCity,$remoteCountry,$geolatitude,$geolongitude);
	}
	
	?>

		<footer id="footer-panel">
		<div class="fh5co-spacer fh5co-spacer-sm"></div>
			<!--<div class="fh5co-overlay"></div>-->
			<div class="fh5co-footer-content">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-4 col-md-push-3">
							<h3 class="fh5co-lead"><?php echo _("Navigation")?> </h3>
							<ul>
								<li><a href="<?php echo base_url()?>features"><?php echo _("OTG Features")?></a></li>
								<li><a href="<?php echo base_url()?>pricing"><?php echo _("SMS Pricing")?></a></li>
								<li><a href="<?php echo base_url()?>virtual_numbers"><?php echo _("Virtual Numbers")?></a></li>
								<li><a href="<?php echo base_url()?>industries"><?php echo _("Industries")?></a></li>
								<li><a href="<?php echo base_url()?>enterprise_sms_gateway"><?php echo _("Enterprise SMS Gateway")?></a></li>
								<li><a href="<?php echo base_url()?>home"><?php echo _("FAQs")?></a></li>
								<li><a href="<?php echo base_url()?>site/demo"><?php echo _("Try Demo")?></a></li>
								
							</ul>
						</div>
						<div class="col-md-3 col-sm-4 col-md-push-3">
							<h3 class="fh5co-lead"><?php echo _("Company")?> </h3>
							<ul>
								<li><a href="<?php echo base_url()?>company/about_us"><?php echo _("About Us")?></a></li>
								<li><a href="#"><?php echo _("Press & Media")?></a></li>
								<li><a href="<?php echo base_url()?>company/careers"><?php echo _("Careers")?></a></li>
								<li><a href="<?php echo base_url()?>enquiry"><?php echo _("Contact Us")?></a></li>
								
							</ul>
						</div>
						<div class="col-md-3 col-sm-4 col-md-push-3">
							<h3 class="fh5co-lead"><?php echo _("Legal Docs")?></h3>
							<ul>
								<li><a href="<?php echo base_url()?>legal/privacy_policy"><?php echo _("Privacy Policy")?></a></li>
								<li><a href="<?php echo base_url()?>legal/terms_conditions"><?php echo _("Terms & Conditions")?></a></li>
								<li><a href="<?php echo base_url()?>legal/anti_spam_policy"><?php echo _("AntiSpam Policy")?></a></li>
								
							</ul>
						</div>

						<div class="col-md-3 col-sm-12 col-md-pull-9">
							<div class="fh5co-footer-logo">
							<a href="<?php echo base_url()?>home">
							<img src="<?php echo base_url()?>theme4.0/landing/images/logo_black.png" alt="OTG"/>
							</a>
							</div>
							<p class="fh5co-copyright">
							<?php echo "<small><i class='icon-globe'></i> ".@$getCountry[0]->zone_name."&nbsp;[".$getCountry[0]->country_code."]</small>";?>
							
							</p>
							
							<p class="fh5co-social-icons">
								<a href="<?php echo $intlib[0]->twitter_social_link;?>"><i class="icon-twitter"></i></a>
								<a href="<?php echo $intlib[0]->fb_social_link;?>"><i class="icon-facebook"></i></a>
								<a href="<?php echo $intlib[0]->youtube_social_link;?>"><i class="icon-youtube"></i></a>
							</p>
						</div>
						
					</div>
					<div class="row">
					<div class="col-md-8 col-sm-12">
						<p class="fh5co-copyright"><small>&copy; <?php echo $intlib[0]->brand_copyrights;?> <br></small></p>
						</div>
						
						<div class="col-md-4 col-sm-12 ico_contrast">
						 <img src="<?php echo base_url()?>theme4.0/landing/images/payment/visa.png" alt="visa">&nbsp;&nbsp;
						 <img src="<?php echo base_url()?>theme4.0/landing/images/payment/master.png" alt="visa">&nbsp;&nbsp;
						 <img src="<?php echo base_url()?>theme4.0/landing/images/payment/paypal.png" alt="visa">&nbsp;&nbsp;
						 <img src="<?php echo base_url()?>theme4.0/landing/images/payment/ae.png" alt="visa">
						</div>
					</div>
				</div>
			</div>
		</footer>
	
    
	<!-- jQuery -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/jquery.waypoints.min.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/jquery.magnific-popup.min.js"></script>
	<!-- Owl Carousel -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/owl.carousel.min.js"></script>
	<!-- toCount -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/jquery.countTo.js"></script>
	<!-- Main JS -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/main.js"></script>

	<script>
	 $(function(){
		 $('#check_price').click(function(){
			 var ccode= $('#geo_country').val();
			 var qty  = $('#check_count').val();
			 
			 var re = new RegExp(/^.*\//);
			 var rCPath= re.exec(window.location.href);
			 
			 $("#check_title").html('Checking..');
				$.ajax({
				type: "POST",
				url: rCPath+"home/check_price",
				data: {ccode:ccode,qty:qty}
				})
				.done(function(msg) {
					$("#check_title").html('Check');
					$('#price_set').slideDown();  
					$('#price_set').html(msg);
					
				});

		 });
	 });
	</script>
</body>
</html>
