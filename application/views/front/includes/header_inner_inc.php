<!DOCTYPE html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>OneTextGlobal | Best SMS Campaigning Services</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="OneTextGlobal is a bulk SMS marketing service provider founded in 2016. Our tools are:  Online SMS Campaign, Email to SMS . FREE TRIAL!"/>
	<meta name="keywords" content="Bulk SMS, Global SMS, Global Text, SMS Marketing, SMS Campaign, Cheap SMS Marketing, Best SMS Market" />
	<meta name="author" content="saidapro.com" />

  	<!-- 
	//////////////////////////////////////////////////////

	Title:          OneTextGlobal
	Website: 		http://saidapro.com
	Email: 			contact@saidapro.com
	Author: 		Mr.Saida.D
	Industry:       SMS Marketing

	//////////////////////////////////////////////////////
	 -->

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="<?php echo base_url()?>theme4.0/landing/images/logo.ico">

	<!-- Google Webfonts -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/landing/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/landing/css/icomoon.css">
	<!-- Simple Line Icons-->
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/landing/css/simple-line-icons.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/landing/css/magnific-popup.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/landing/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/landing/css/owl.theme.default.min.css">
	<!-- Salvattore -->
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/landing/css/salvattore.css">
	<!-- Theme Style -->
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/landing/css/style.css">
	<!-- Modernizr JS -->
	<script src="<?php echo base_url()?>theme4.0/landing/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
  
	</head>
	<body>
    
	<div id="fh5co-offcanvass">
		<ul>
			<li class="active"><a href="<?php echo base_url()?>home"><?php echo _("Home")?></a></li>
			<li><a href="<?php echo base_url()?>features"><?php echo _("Features")?></a></li>
			<li><a href="<?php echo base_url()?>virtual_numbers"><?php echo _("Virtual Numbers")?></a></li>
			<li><a href="<?php echo base_url()?>enterprise_sms_gateway"><?php echo _("Enterprise SMS Gateway")?></a></li>
			<li><a href="<?php echo base_url()?>industries"><?php echo _("Industries")?></a></li>
			<li><a href="<?php echo base_url()?>pricing"><?php echo _("Pricing")?></a></li>
			<li><a href="<?php echo base_url()?>home" data-nav-section="faqs"><?php echo _("FAQs")?></a></li>
			<li><a href="<?php echo base_url()?>enquiry"><?php echo _("Contact Us")?></a></li>
		</ul>
		<h3 class="fh5co-lead"><?php echo _("OTG Account")?></h3>
		<ul>
		    <li class="active"><a href="<?php echo base_url()?>site/demo" ><?php echo _("Try Demo")?></a></li>
			<li><a href="<?php echo base_url()?>site/login"><?php echo _("Login")?></a></li>
			
		</ul>
	</div>
	
	<div class="inner-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#fh5co-navbar" aria-expanded="false" aria-controls="navbar"><span><?php echo _("Menu")?></span> <i></i></a>
					<a href="<?php echo base_url()?>home" class="navbar-brand"><img src="<?php echo base_url()?>theme4.0/landing/images/logo.png" alt="OneTextGlobal" width="150" height="65"></a>
				</div>
			</div>
		</div>
	</div>
