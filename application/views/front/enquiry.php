<?php 
  $this->load->view('front/includes/header_inner_inc');
  $inner_bg="background-image: url(theme4.0/landing/images/bg_12.jpg);
  background-repeat:no-repeat;background-position: center;height:250px;";
  $appSettings=$this->app_settings_model->get_app_settings();
 ?>	
<script src="<?php echo base_url()?>theme4.0/client/js/jquery-2.1.1.js"></script> 
<script type="application/javascript" src="<?php echo base_url();?>js/enquiry.js"></script>

	<div id="fh5co-page">
		<div id="fh5co-wrap">
			
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="<?php echo $inner_bg?>">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<div class="text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 text-center">
												<h1 class="text-primary to-animate text-white">
												  <?php echo _("We Happy to Assist You")?>
												</h1>
												<p class="fh5co-sub to-animate">
													<?php echo _("Let us know how to get in touch for general inquiries")?>
												</p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				   
				 <div id="fh5co-features-2"  style="padding-top:0">
				 <div class="fh5co-spacer fh5co-spacer-sm"></div>
					<div class="fh5co-features-2-content">
					    <div class="container">
					    	  <div class="row">
							  <div class="col-md-12">
							  <div class="col-md-7">
							  
							  <div class="row">
							  <div class="col-md-3">
							  <h2><?php echo _("Send Us")?></h2>
							  </div>
							  <div class="col-md-6 pull-left">
							  <h2><small class="text-muted"><?php echo _("Inquiry Details Please")?></small></h2>
							  </div>
							  </div>
							  
							  <form role="form" class="form-horizontal" id="enqForm" name="enqForm">
							  <div class="row">
							  <div class="col-md-10">
							   <?php 
							   $rsCountry = $this->countries_model->list_countries();
							   $remoteCountry=$this->config->item('remote_country_code');
							   ?>
							   <select class="form-control"  id="geo_country" name="geo_country" required>
							   <?php 
								 
								  if(count($rsCountry)>0){
									  for($rC=0;$rC<count($rsCountry);$rC++){
									  ?>
									  <option value="<?php echo $rsCountry[$rC]->country_code;?>" <?php if($rsCountry[$rC]->country_code==$remoteCountry){echo 'selected';}?>>
									  <?php echo $rsCountry[$rC]->country_name;?>
									  </option>
									  <?php
									  }
								  }
								 ?>
							   </select>
							   </div>
							   </div><br>
							   
							  <div class="row">
							   <div class="col-md-10">
							    <input type="text" name="enq_name" id="enq_name" class="form-control" placeholder="*<?php echo _('Full Name') ?>" required>
							   </div>
							   </div><br>
							   
							   <div class="row">
							   <div class="col-md-10">
							    <input type="text" name="enq_contact" id="enq_contact" class="form-control" placeholder="*<?php echo _('Contact No') ?>" required>
							   </div>
							   </div><br>
							   
							   <div class="row">
							  <div class="col-md-10">
							    <input type="text" name="enq_email" id="enq_email" class="form-control" placeholder="<?php echo _('Email Id') ?>">
							   </div>
							   </div><br>
							   
							   <div class="row">
							  <div class="col-md-10">
							    <select class="form-control" id="enq_type" name="enq_type">
								 <option value="general"><?php echo _("General Inquiry")?></option>
								 <option value="sales"><?php echo _("Sales Inquiry")?></option>
								 <option value="technical"><?php echo _("Technical Inquiry")?></option>
								 <option value="other"><?php echo _("Other Inquiry")?></option>
								</select>
							   </div>
							   </div><br>
							   
							   <div class="row">
							   <div class="col-md-10">
							    <textarea class="form-control" id="enq_comments" name="enq_comments" placeholder="<?php echo _('Comments') ?>"></textarea>
							   </div>
							   </div><br>
							   
							   <div class="row">
							   <div class="col-md-6">
							   <input type="button" value="Submit" id="submit_enq" name="submit_enq" class="btn btn-primary">
							   </div>
							   <div class="col-md-6">
							    <small class="text-danger">* <?php echo _("Mandatory Fields")?></small>
							   </div>
							   </div><br>
							   
							   <div id="enq_response">

							   </div>
							   </form>
							   
							  </div>
							  <div class="col-md-5">
							  <h2 class="text-success"><?php echo _("Get in Touch with Support")?></h2>
							  <?php echo _("OneTextGlobal is always here for you.
							  Choose a support plan for phone or emergency support. 
							  OneTextGlobal does not tolerate violations of our Acceptable Use Policy. 
							  If you encounter an issue please report SMS or voice abuse to us, 
							  and we will be in touch shortly.")?><br>
                              <?php echo _("For more details email us @")?><a href="#"><?php echo $appSettings[0]->default_support_email;?></a>
							  </div>
							  </div>
							</div>
					    	</div>
					    </div>
					</div>	
				
				
				<div class="bg-blue">
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
				  <div class="container">
				  	<div class="row">
						<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center" style="margin-bottom:0">
						<h2 class="fh5co-lead text-white"><?php echo _("Call").$appSettings[0]->default_support_numbers?></h2>
						<p class="fh5co-sub text-white">
						<?php echo _("No longer to wait until we get back to your query. Just call us at above number for instant assistance
						we happy to help you.")?>
						</p>
                       </div>
			       </div>
			    </div>
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
				</div>
			</div>
			
	</div>
</div>


<?php 
  $this->load->view('front/includes/footer_inc');
 ?>
		
