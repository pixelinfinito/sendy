<?php 
  $this->load->view('front/includes/header_inner_inc');
  $inner_bg="background-image: url(../theme4.0/landing/images/bg_13.jpg);
  background-repeat:no-repeat;background-position: center;height:250px;";
  $appSettings=$this->app_settings_model->get_app_settings();
  
 ?>	
<script src="<?php echo base_url()?>theme4.0/client/js/jquery-2.1.1.js"></script> 
<script type="application/javascript" src="<?php echo base_url();?>js/enquiry.js"></script>

	<div id="fh5co-page">
		<div id="fh5co-wrap">
			
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="<?php echo $inner_bg?>">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<div class="text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 text-center">
												<h1 class="text-primary to-animate text-white">
												  <?php echo _("Privacy Policy")?>
												</h1>
												<p class="fh5co-sub to-animate">
													<?php echo _("Privacy statements").$appSettings[0]->app_default_name?>
												</p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				   
				 <div id="fh5co-features-2"  style="padding-top:0">
				 <div class="fh5co-spacer fh5co-spacer-sm"></div>
					<div class="fh5co-features-2-content">
					    <div class="container">
					    	  <div class="row">
							  <div class="col-md-12">
							    <?php 
								 $type=$this->uri->segment(2);
								 $article=$this->cms_model->list_articles_bytype($type);
								 if($article!=0){
								 echo $article[0]->article_body;
								 }else{
									 
									 echo "<h2>" . _('204 No Content') . "</h2>";
								 }
								?>
							  
							  </div>
							</div>
					    	</div>
					    </div>
					</div>	
				
			</div>
			
	</div>
</div>


<?php 
  $this->load->view('front/includes/footer_inc');
 ?>
		
