<?php 
  $this->load->view('front/includes/header_inner_inc');
  $inner_bg="background-image: url(theme4.0/landing/images/bg_8.jpg);
  background-repeat:no-repeat;background-position: center;height:250px;";
 ?>	
	
	<div id="fh5co-page">
		<div id="fh5co-wrap">
			<div id="fh5co-main">
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="<?php echo $inner_bg?>">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<div class="text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 text-center">
												<h1 class="text-primary to-animate text-white">
												 <?php echo _("Best In Marketing Solution")?>
												</h1>
												<p class="fh5co-sub to-animate">
													<?php echo _("See how messaging can help grow your business")?>
												</p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				   
				<div id="fh5co-features-3" data-section="industries" style="padding-top:0;">
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single features3-animate-1"><?php echo _("Choose Your Industry")?></h2>
								<p class="fh5co-sub animate-single features3-animate-2">
								<?php echo _("We supports all industries, no matter the industry, organisations across the globe are beginning to capitalise on the benefits SMS has to offer. Use SMS to help your business increase its consumer reach, improve your interactions with customers and grow their engagement with your brand.")?>
								</p>
							</div>

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-shopping-cart"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Retail")?></h4>
									<p>
									<?php echo _("Build loyalty programs, improve sales promotions and build your database with a multi-channel SMS approach.")?>
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-briefcase"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Marketing & Advertising")?></h4>
									<p><?php echo _("Discover how integrating an SMS marketing strategy into your communications workflow can help your business grow.")?></p>
								</div>
							</div>
							
							<div class="clearfix visible-sm-block"></div>

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-train"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Travel & Tourism")?></h4>
									<p><?php echo _("Keep customers up to date with timely travel information, reservation confirmations and discount offers via SMS.")?></p>
								</div>
							</div>	

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-dollar"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Banking & Finance")?></h4>
									<p><?php echo _("Utilise SMS for billing reminders, server monitoring and fraud prevention to keep your customers abrest of critical information.")?></p>
								</div>
							</div>

							<div class="clearfix visible-sm-block"></div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-book"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Education")?></h4>
									<p><?php echo _("Integrating SMS into your communications workflow is the perfect way to keep your students, parents & staff up-to-date.")?></p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-building"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Real Estate")?></h4>
									<p>
									<?php echo _("Schedule inspection reminders, manage rentals properties and broadcast auction alerts to prospective buyers via SMS.")?></p>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-tree"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Healthcare")?></h4>
									<p><?php echo _("Show your patients how much you care with reminders, updates and notifications sent directly to their mobile device.")?></p>
								</div>
							</div>

							<div class="clearfix visible-sm-block"></div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-bed"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Hospitality")?></h4>
									<p><?php echo _("Manage customer bookings, conformations and cancellations with simple SMS reminders and notifications.")?></p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-heart"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Beauty & Fitness")?></h4>
									<p>
									<?php echo _("Promote sales and special offers as well as schedule appointments using the most efficient communication tool available.")?>
									</p>
								</div>
							</div>
							
							<div class="fh5co-spacer fh5co-spacer-sm"></div>
							<div class="col-md-4 col-md-offset-4 text-center to-animate">
								<a href="<?php echo base_url()?>site/register" class="btn btn-info"><i class="icon-laptop"></i> <?php echo _("Get Started!")?></a>
							</div>

						</div>
					</div>
				</div>  
			    </div>
			</div>
			
	</div>
</div>

<div class="bg-pink">
	<div class="container">
		<div class="row animate-box">
		<div class="fh5co-spacer fh5co-spacer-sm"></div>
		   <div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
				<h1 class="text-white"><?php echo _("For Inquiries")?></h1>
				<p class="text-white">
				<?php 
				$appSettings=$this->app_settings_model->get_app_settings();
				?>
				 <?php echo _("Let us know how to get in touch for general inquiries, and we'll get back to you within twenty-four hours (or) reach us at")?> <?php echo $appSettings[0]->default_support_numbers?>.
				 <div class="">
					<a href="<?php echo base_url()?>enquiry" class="btn btn-primary btn-md"><i class="icon-envelope"></i> <?php echo _("Contact Us")?></a>
				 </div>
				</p>
			</div>
			 
		</div>
	</div>
</div>

<?php 
  $this->load->view('front/includes/footer_inc');
 ?>
		
