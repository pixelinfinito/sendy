<?php 
  $this->load->view('front/includes/header_inner_inc');
  $inner_bg="background-image: url(theme4.0/landing/images/bg_9.jpg);
  background-repeat:no-repeat;background-position: center;height:250px;";
 ?>	
	
	<div id="fh5co-page">
		<div id="fh5co-wrap">
			<div id="fh5co-main">
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="<?php echo $inner_bg?>">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<div class="text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 text-center">
												<h1 class="text-primary to-animate text-white">
												<?php echo _("Trusted by the world's largest brands")?>
												</h1>
												<p class="fh5co-sub to-animate">
													<?php echo _("Our SMS gateway centers around security, performance and reliability")?>
												</p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				   
				<div id="fh5co-products" data-section="sms_gateway" style="padding-top:0;">
				
				  <div class="container">
				  <div class="fh5co-spacer fh5co-spacer-sm"></div>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single product-animate-1"><?php echo _("Enterprise SMS Gateway")?></h2>
								<p class="fh5co-sub animate-single product-animate-2">
								<?php echo _("Our SMS gateway centers around security, performance and reliability. 
								For the past decade, OneTextGlobal has partnered with organisations all over the globe to develop innovative mobile messaging solutions that drive revenue growth and deliver outstanding results for our Enterprise clients.")?>
								</p>
							</div>
						

							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="images/product_1.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_1.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Global Connectivity")?></h3>
									<p class="fh5co-figure-text"><?php echo _("Benefit from OneTextGlobal vendor relationships with global mobile carriers around the world, backed by our geographically distributed infrastructure and industry leading web messaging platform.")?></p>
								</a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_2.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_2.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Flexible & Scalable")?></h3>
									<p class="fh5co-figure-text"><?php echo _("OneTextGlobal's intelligent SMS routing system provides a powerful messaging architecture that is scalable to thousands of messages per second. Using bulk SMS you can instantly meet the evolving needs of your messaging requirements.")?></p>
								</a>
							</div>
							<div class="clearfix visible-sm-block"></div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_3.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_3.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Quality to the Core")?></h3>
									<p class="fh5co-figure-text"><?php echo _("Benefit from real-time reporting, allowing you to track your message from inception to the handset. Should you encounter any difficulties, our 24/7 hotline and dedicated account managers are on-hand to troubleshoot.")?></p>
								</a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_4.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_4.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("24/7 Monitoring")?></h3>
									<p class="fh5co-figure-text"><?php echo _("Rely on OneTextGlobal's round the clock monitoring of connectivity to carriers, with message throughput and delivery performance tracked by a proprietary routing system that responds dynamically to any delivery issues.")?></p>
								</a>
							</div>
							<div class="clearfix visible-sm-block"></div>
							
						</div>
						
						<div class="row">
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="images/product_5.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_5.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Geo Match")?></h3>
									<p class="fh5co-figure-text">
									<?php echo _("OneTextGlobal automatically select local phone numbers when sending messages globally to create a local experience for your users.
									User no need to bother about international/local difference, we take care of it.")?>
									
									</p>
								</a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_6.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_6.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Peace of Mind")?></h3>
									<p class="fh5co-figure-text"><?php echo _("Trust OneTextGlobal to share expertise & advice from the earliest stages of planning through to the implementation & delivery of your project. Once your solutions are up and running, technical assistance is always on-hand.")?></p>
								</a>
							</div>
							<div class="clearfix visible-sm-block"></div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_7.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_7.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Enterprise Product")?></h3>
									<p class="fh5co-figure-text">
									<?php echo _("OneTextGlobal's Enterprise product suite offers large organisations the ability to seamlessly roll-out new mobile messaging solutions to their global customer base, and flexibly scale these upwards as demand grows.")?>
									</p>
								</a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_8.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_8.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Great Interaction")?> </h3>
									<p class="fh5co-figure-text">
									 <?php echo _("Our Enterprise customers will have great interaction to a number of advanced web interface design features, customised to suit your business environment.")?>
									</p>
								</a>
							</div>
							<div class="clearfix visible-sm-block"></div>
							<div class="fh5co-spacer fh5co-spacer-sm"></div>
							<div class="col-md-4 col-md-offset-4 text-center to-animate">
								<a href="<?php echo base_url()?>site/register" class="btn btn-info"><i class="icon-th-list"></i> <?php echo _("Get Started!")?></a>
							</div>
						</div>
						
			       </div>
			    </div>
			</div>
			
	</div>
</div>

<div class="bg-green">
	<div class="container">
		<div class="row animate-box">
		<div class="fh5co-spacer fh5co-spacer-sm"></div>
		   <div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
				<h1 class="text-white"><?php echo _("For Inquiries")?></h1>
				<p class="text-white">
				<?php 
				$appSettings=$this->app_settings_model->get_app_settings();
				?>
				 <?php echo _("Let us know how to get in touch for general inquiries, and we'll get back to you within twenty-four hours (or) reach us at")?> <?php echo $appSettings[0]->default_support_numbers?>.
				 <div class="">
					<a href="<?php echo base_url()?>enquiry" class="btn btn-primary btn-md"><i class="icon-envelope"></i> <?php echo _("Contact Us")?></a>
				 </div>
				</p>
			</div>
			 
		</div>
	</div>
</div>

<?php 
  $this->load->view('front/includes/footer_inc');
 ?>
		
