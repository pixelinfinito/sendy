<?php 
  $this->load->view('front/includes/header_inner_inc');
  $inner_bg="background-image: url(theme4.0/landing/images/bg_11.jpg);
  background-repeat:no-repeat;background-position: center;height:250px;";
 ?>	
	
	<div id="fh5co-page">
		<div id="fh5co-wrap">
			<div id="fh5co-main">
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="<?php echo $inner_bg?>">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<div class="text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 text-center">
												<h1 class="text-primary to-animate text-white">
												   <?php echo _("We Offering Best Pricing")?>
												</h1>
												<p class="fh5co-sub to-animate">
													<?php echo _("We are sure, you feels it and believe it")?>
												</p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				   
				 <div id="fh5co-pricing" data-section="pricing" style="border-bottom:1px solid #ccc;padding:0">
				 <div class="fh5co-spacer fh5co-spacer-sm"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single pricing-animate-1"><?php echo _("Simple SMS Pricing for Business")?></h2>
								<p class="fh5co-sub animate-single pricing-animate-2">
								 <?php echo _("No hidden fees, no surprises. Pay only for outbound text messages,Inbound SMS is FREE. Messages can traverse any of 1,800 mobile carriers, each with varying levels of dependability. 
								 OTG(OneTextGlobal) leaves nothing to chance, rigorously evaluating carriers to rule out SIM farms and carrier filtering.")?>
								 
								</p>
							</div>
							
							<div class="col-md-3 to-animate">
							 <a href="#" class="fh5co-figure">
							  <h3 class="fh5co-figure-lead">
							   <i class="icon-globe"></i> <?php echo _("Select Country")?>
							  </h3>
							  </a>
							</div>
							<div class="col-md-4 to-animate">
							   <?php 
							   $rsCountry = $this->countries_model->list_countries();
							   $remoteCountry=$this->config->item('remote_country_code');
							   ?>
							   <select class="form-control"  id="geo_country" name="geo_country">
							   <?php 
								 
								  if(count($rsCountry)>0){
									  for($rC=0;$rC<count($rsCountry);$rC++){
									  ?>
									  <option value="<?php echo $rsCountry[$rC]->country_code;?>" <?php if($rsCountry[$rC]->country_code==$remoteCountry){echo 'selected';}?>><?php echo $rsCountry[$rC]->country_name;?></option>
									  <?php
									  }
								  }
								 ?>
							   </select>
							   
							</div>
							<div class="col-md-2 to-animate">
							 <input type="text" class="form-control" id="check_count" name="check_count" placeholder="<?php echo _('Qty e.g. 100') ?>">
							</div>
							<div class="col-md-3 to-animate">
							  <button id="check_price" name="check_price" class="btn btn-primary">
							   <i class="icon-check"></i> <span id="check_title"><?php echo _("Check")?></span>
							  </button>
							</div>
							
							 <span class="text-center" id="price_set" style="display:none">
							  &nbsp;
							 </span>
							<div class="fh5co-spacer fh5co-spacer-sm"></div>
						</div>
					</div>
				</div>
					
				<div id="fh5co-features-3" style="padding-top:0">
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
				  <div class="container">
				  	<div class="row">
						<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single features3-animate-1">
								<?php echo _("Secure Payment Options")?></h2>
								<p class="fh5co-sub animate-single features3-animate-2">
								<?php echo _("OneTextGlobal accepts Visa, MasterCard, American Express and PayPal. 
								You can also pay via regular bank transfer by requesting an invoice on our payment page - SMS credit will be added after your bank transfer has cleared.")?>
								<br><br><img src="<?php echo base_url()?>theme4.0/landing/images/cards-sprite.png" alt="Payments">
								</p>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-flash"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Credit Is Added Instantly")?></h4>
									<p>
									<?php echo _("SMS credit will be added to your account instantly after making a payment.")?>
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-shopping-cart"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Credit Never Expires")?></h4>
									<p>
									<?php echo _("No SMS credit you buy from us will ever expire. Credit remains active forever.")?>
									</p>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-repeat"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Money Back Guarantee")?></h4>
									<p>
									<?php echo _("If you are not happy with our service you can request a refund for unused credit.")?>
									</p>
								</div>
							</div>	
                       </div>
					   
					   <div class="row">
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-star"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Credit Balance Alerts")?></h4>
									<p>
									<?php echo _("Enable auto-recharge for credit purchases or get low-balance alerts via email.")?>
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-print"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Printable Payment Receipts")?></h4>
									<p>
									<?php echo _("A printable PDF payment receipt will be emailed to you with every purchase.")?>
									</p>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-list"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Instant Invoices")?></h4>
									<p>
									<?php echo _("Insert a custom PO number on every invoice if needed by your business.")?>
									</p>
								</div>
							</div>	
                       </div>
					   
						<div class="row">
						   <div class="col-md-4 col-md-offset-4 text-center to-animate">
							<a href="<?php echo base_url()?>site/register" class="btn btn-primary btn-md"><i class="icon-laptop"></i> <?php echo _("Get Started!")?></a>
							</div>
						</div>
						
			       </div>
			    </div>
				
			    </div>
			</div>
			
	</div>
</div>

<div class="bg-blue">
	<div class="container">
		<div class="row animate-box">
		<div class="fh5co-spacer fh5co-spacer-sm"></div>
		   <div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
				<h1 class="text-white"><?php echo _("For Inquiries")?></h1>
				<p class="text-white">
				<?php 
				$appSettings=$this->app_settings_model->get_app_settings();
				?>
				 <?php echo _("Let us know how to get in touch for general inquiries, and we'll get back to you within twenty-four hours (or) reach us at")?> <?php echo $appSettings[0]->default_support_numbers?>.
				 <div class="">
					<a href="<?php echo base_url()?>enquiry" class="btn btn-danger btn-md"><i class="icon-envelope"></i> <?php echo _("Contact Us")?></a>
				 </div>
				</p>
			</div>
			 
		</div>
	</div>
</div>

<?php 
  $this->load->view('front/includes/footer_inc');
 ?>
		
