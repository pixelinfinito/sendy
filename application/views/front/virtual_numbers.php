<?php 
  $this->load->view('front/includes/header_inner_inc');
  $inner_bg="background-image: url(theme4.0/landing/images/bg_10.jpg);
  background-repeat:no-repeat;background-position: center;height:250px;";
 ?>	
	
	<div id="fh5co-page">
		<div id="fh5co-wrap">
			<div id="fh5co-main">
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="<?php echo $inner_bg?>">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<div class="text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 text-center">
												<h1 class="text-primary to-animate text-white">
												<?php echo _("Own Branding in Campaigns")?>
												</h1>
												<p class="fh5co-sub to-animate">
													<?php echo _("Get your own branding messages tied with virtual numbers")?>
												</p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				   
				 <div id="fh5co-features-2" class="border-gray-bottom" data-section="virtual_numbers" style="padding-top:0">
				 <div class="fh5co-spacer fh5co-spacer-sm"></div>
					<div class="fh5co-features-2-content">
					    <div class="container">
					    	<div class="row">
					    		<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center" style="margin-bottom:0">
									<h2 class="fh5co-lead to-animate"><?php echo _("Virtual Numbers")?></h2>
									<p class="fh5co-sub to-animate">
									<?php echo _("A Dedicated Number is a Virtual Phone Number that can be used for SMS campaign and belongs to only one account (unlike the OneTextGlobal shared pool numbers). Think of a virtual number as your own special unique code that customers and employees can get to know you by, that no one else can send from.")?>
									</p>
									<a href="<?php echo base_url()?>site/login" class="btn btn-primary btn-md"><i class="icon-tag"></i> <?php echo _("Buy a number")?></a>
					    		</div>
					    	</div>
					    </div>
					</div>	
					</div>
					
				<div id="fh5co-features-3" style="padding-top:0">
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
				  <div class="container">
				  	<div class="row">
						<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single features3-animate-1"><?php echo _("Why you need a virtual number?")?></h2>
								<p class="fh5co-sub animate-single features3-animate-2">
								<?php echo _("Provisioning a virtual number offers you a direct and personalised way of contacting your customers, with the added flexibility of using advanced features including keywords and message triggers to automate your SMS campaigns.")?>
								</p>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-tags"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Own Branding")?></h4>
									<p>
									<?php echo _("Setting up a dedicated number allows you to use a consistent Sender ID for all messages sent from your OTG account. 
									Advertise the number on your website to grow your mobile database with user-initiated engagement.")?>
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-send"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Campaigns & Promotions")?></h4>
									<p>
									<?php echo _("Use your dedicated number as a consistent Sender ID when broadcasting 2-way marketing campaigns to your user base. 
									Opt-out replies are automatically captured and removed from the phonebook of your OTG account.")?>
									</p>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-calendar"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Low Price & Short Terms Periods")?></h4>
									<p>
									<?php echo _("We are offering virtual numbers/ caller numbers for low prices, you can buy a number as minimum starts from 1 month upto 12 months.
									When you purchase a number you are eligible to request own SenderId.")?>
									</p>
								</div>
							</div>	
                       </div>
					   
						<div class="row">
						   <div class="col-md-4 col-md-offset-4 text-center to-animate">
							<a href="<?php echo base_url()?>site/register" class="btn btn-info btn-md"><i class="icon-laptop"></i> <?php echo _("Get Started!")?></a>
							</div>
						</div>
						
			       </div>
			    </div>
				
			    </div>
			</div>
			
	</div>
</div>

<div class="bg-pink">
	<div class="container">
		<div class="row animate-box">
		<div class="fh5co-spacer fh5co-spacer-sm"></div>
		   <div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
				<h1 class="text-white"><?php echo _("For Inquiries")?></h1>
				<p class="text-white">
				<?php 
				$appSettings=$this->app_settings_model->get_app_settings();
				?>
				 <?php echo _("Let us know how to get in touch for general inquiries, and we'll get back to you within twenty-four hours (or) reach us at")?> <?php echo $appSettings[0]->default_support_numbers?>.
				 <div class="">
					<a href="<?php echo base_url()?>enquiry" class="btn btn-primary btn-md"><i class="icon-envelope"></i> <?php echo _("Contact Us")?></a>
				 </div>
				</p>
			</div>
			 
		</div>
	</div>
</div>

<?php 
  $this->load->view('front/includes/footer_inc');
 ?>
		
