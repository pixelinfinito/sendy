<?php 
  $this->load->view('front/includes/header_inner_inc');
  $inner_bg="background-image: url(../theme4.0/landing/images/bg_4.jpg);
  background-repeat:no-repeat;background-position: center;height:250px;";
  $appSettings=$this->app_settings_model->get_app_settings();
 ?>	
	
	<div id="fh5co-page">
		<div id="fh5co-wrap">
			<div id="fh5co-main">
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="<?php echo $inner_bg?>">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<div class="text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 text-center">
												<h1 class="text-primary to-animate text-white">
												 <?php echo _("Careers")?>
												</h1>
												<p class="fh5co-sub to-animate">
													<?php echo _("Dedicating persons always welcome, join with us")?>
												</p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				   
				<div id="fh5co-features-3" data-section="industries" style="padding-top:0;">
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
					<div class="container">
						<div class="row">
						    <div class="col-md-2">
							  &nbsp;
							 </div>
							<div class="col-md-8">
							<?php 
								 $type=$this->uri->segment(2);
								 $article=$this->cms_model->list_articles_bytype($type);
								 if($article!=0){
								 echo $article[0]->article_body;
								 }else{
									 
									 echo "<h2>" . _('204 No Content') . "</h2>";
								 }
								?>
							</div>
							
							<div class="col-md-2">
							  &nbsp;
							 </div>

						</div>
					</div>
				</div>  
			    </div>
			</div>
			
	</div>
</div>

<div class="bg-green">
	<div class="container">
		<div class="row animate-box">
		<div class="fh5co-spacer fh5co-spacer-sm"></div>
		   <div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
				<h1 class="text-white"><?php echo _("For Inquiries")?></h1>
				<p class="text-white">
				<?php 
				$appSettings=$this->app_settings_model->get_app_settings();
				?>
				 <?php echo _("Let us know how to get in touch for general inquiries, and we'll get back to you within twenty-four hours (or) reach us at")?> <?php echo $appSettings[0]->default_support_numbers?>.
				 <div class="">
					<a href="<?php echo base_url()?>enquiry" class="btn btn-primary btn-md"><i class="icon-envelope"></i> <?php echo _("Contact Us")?></a>
				 </div>
				</p>
			</div>
			 
		</div>
	</div>
</div>

<?php 
  $this->load->view('front/includes/footer_inc');
 ?>
		
