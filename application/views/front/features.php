<?php 
  $this->load->view('front/includes/header_inner_inc');
  $inner_bg="background-image: url(theme4.0/landing/images/bg_3.jpg);
  background-repeat:no-repeat;background-position: center;height:250px;";
 ?>	
	
	<div id="fh5co-page">
		<div id="fh5co-wrap">
			<div id="fh5co-main">
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="<?php echo $inner_bg?>">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
									<div class="text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 text-center">
												<h1 class="text-primary to-animate text-white"><?php echo _("Featured Business SMS Platform")?></h1>
												<p class="fh5co-sub to-animate">
													<?php echo _("We help you to improve your business with affrodable features")?>
												</p>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				 <div class="bg-gray brand-dark-pattern border-gray-bottom">
				    <div class="row">
					  <div class="col-md-12 text-center">
						  <img src="theme4.0/landing/images/dashboard_otg.png">
						</div>
					</div>
				 </div>
				   
				<div id="fh5co-features-3" data-section="industries" style="padding-top:0;">
				  <div class="container">
				  <div class="fh5co-spacer fh5co-spacer-sm"></div>
						<div class="row">
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-check"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Success Rate Charges")?></h4>
									<p>
									<?php echo _("Many other companies are charging per unit (sms), no matter whether it was delivered successfull or not.
									But we different to other our charges only for delivered messages, no other hidden charges its transparent.")?>
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-tags"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Short codes/Sender ID")?></h4>
									<p>
									<?php echo _("Alphanumeric Sender ID allows you to set your own business brand as the Sender ID when sending one-way messages. This feature is only available when sending messages to supported countries.")?>
									</p>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-calendar"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Schedule Campaign")?></h4>
									<p><?php echo _("Sometimes peoples can forget things to do,implies it might be impact to your business or any of your personals.
									Here is the option schedule campaign, which helps you to schedule SMS based on different timezone.")?>
									</p>
								</div>
							</div>	
                       </div>
					   <div class="row">
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-book"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Contacts Book")?></h4>
									<p><?php echo _("Contacts are nothing but recipient (numbers), you can save or import (from .CSV) contacts based
									on country & groups. It is easy and quick to start campaign when you saved all your contacts.")?>
									</p>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-phone"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Caller Numbers")?></h4>
									<p>
									<?php echo _("We offering a dedicated caller numbers instead of using a shared pool number. 
									Caller numbers are available for limited countries , we offering it for low prices based on different subscription period.")?>
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-users"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Contact Groups")?></h4>
									<p>
									<?php echo _("When you have more contacts it may be difficult to assign contacts when you campaign, to avoid this situation we provided this group 
feature, using groups you can add/move/copy contacts as you want.")?>
									</p>
								</div>
							</div>
							
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-list"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Campaign List")?></h4>
									<p>
                                     <?php echo _("Here you see all your campaigns with status and summary, even you schedule a campaign you can those campaings here.
                                     Each campaign was assigned a unique code which was system generated code, here we also provided brief (SMS)history feature.")?>
									</p>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-money"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Wallet Money/Payments")?></h4>
									<p>
									<?php echo _("We introduced one more great feature WalletMoney, its like your personal wallet when you add funds using our payment gateway your
									funds goes to your wallet money, once you have enough funds then you ready to spend for your campaigns.")?>
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-graph"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Reports")?></h4>
									<p>
									<?php echo _("Access performance metrics like delivery, reply rates and costs through the your admin panel and export data when needed.	
                                   You can check the reports followed by campaigns , date, sms (success/failed).")?>
									</p>
								</div>
							</div>
							
						</div>
						<div class="row">
						   <div class="col-md-4 col-md-offset-4 text-center to-animate">
								<a href="<?php echo base_url()?>site/demo" class="btn btn-danger btn-md" title="<?php echo _('Try a Free Demo') ?>">
								<i class="icon-send"></i> <?php echo _("Try It Free")?>
								</a>
							</div>
						</div>
						
			       </div>
			    </div>
			</div>
			
	</div>
</div>

<div class="bg-blue">
	<div class="container">
		<div class="row animate-box">
		<div class="fh5co-spacer fh5co-spacer-sm"></div>
		   <div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
				<h1 class="text-white"><?php echo _("For Inquiries")?></h1>
				<p class="text-white">
				<?php 
				$appSettings=$this->app_settings_model->get_app_settings();
				?>
				 <?php echo _("Let us know how to get in touch for general inquiries, and we'll get back to you within twenty-four hours (or) reach us at")?> <?php echo $appSettings[0]->default_support_numbers?>.
				 <div class="">
					<a href="<?php echo base_url()?>enquiry" class="btn btn-primary btn-md"><i class="icon-envelope"></i> <?php echo _("Contact Us")?></a>
				 </div>
				</p>
			</div>
			 
		</div>
	</div>
</div>

<?php 
  $this->load->view('front/includes/footer_inc');
 ?>
		
