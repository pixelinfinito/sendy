<?php 
  $this->load->view('front/includes/header_inc');
  $appSettings=$this->app_settings_model->get_app_settings();
 ?>
	<div id="fh5co-page">
		<div id="fh5co-wrap">
			<header id="fh5co-hero" data-section="home" role="banner" style="background: url(<?php echo base_url()?>theme4.0/landing/images/bg_8.jpg) top left; background-size: cover;" >
				<div class="fh5co-overlay"></div>
				<div class="fh5co-intro">
					<div class="container">
						<div class="row">
							<div class="col-md-2 fh5co-text">&nbsp;</div>
							<div class="col-md-8 fh5co-text text-center">
								<h2 class="to-animate intro-animate-1"><?php echo _("SMS Service for Business")?></h2>
								<p class="to-animate intro-animate-2">
								<?php echo _("OneTextGlobal is a business text-messaging service for sending notifications, alerts, reminders, confirmations and SMS marketing campaigns.")?>
								</p>
								<p class="to-animate intro-animate-3">
								<a href="<?php echo base_url()?>site/login" class="btn btn-info btn-md" title="<?php echo _('Learn More') ?>">
								<i class="icon-list"></i> <?php echo _("Learn More")?>
								</a>
								</p>
							</div>
							<div class="col-md-2 fh5co-text">&nbsp;</div>
							
						</div>
					</div>						
				</div>
			</header>
			<!-- END .header -->
			
			<div id="fh5co-main">
				<div id="fh5co-clients">
					<div class="container">
						<div class="row text-center">
							<div class="col-md-3 col-sm-6 col-xs-6 to-animate">
								<figure class="fh5co-client"><img src="<?php echo base_url()?>theme4.0/landing/images/client_1.png" alt="OneTextGlobal"></figure>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 to-animate">
								<figure class="fh5co-client"><img src="<?php echo base_url()?>theme4.0/landing/images/client_2.png" alt="OneTextGlobal"></figure>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 to-animate">
								<figure class="fh5co-client"><img src="<?php echo base_url()?>theme4.0/landing/images/client_3.png" alt="OneTextGlobal"></figure>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 to-animate">
								<figure class="fh5co-client"><img src="<?php echo base_url()?>theme4.0/landing/images/client_4.png" alt="OneTextGlobal"></figure>
							</div>
						</div>
					</div>
				</div>
				<div id="fh5co-features" data-section="features">


					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead to-animate"><?php echo _("Excellent Features")?></h2>
								<p class="fh5co-sub to-animate">
								<?php echo _("SMS has fast become one of the most reliable and influential communications mediums in the market today. With over 5.5 billion mobile users globally, sending over 8.3 trillion messages a year, getting your message across is now easier than ever before.")?>
								</p>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="#" class="fh5co-feature to-animate">
									<span class="fh5co-feature-icon"><i class="icon-send"></i></span>
									<h3 class="fh5co-feature-lead"><?php echo _("Success Rate Charges")?></h3>
									<p class="fh5co-feature-text"><?php echo _("We charge you only for successfully delivered sms,its very transparent")?> </p>
								</a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="#" class="fh5co-feature to-animate">
									<span class="fh5co-feature-icon"><i class="icon-screen-smartphone"></i></span>
									<h3 class="fh5co-feature-lead"><?php echo _("Short codes/Masking")?></h3>
									<p class="fh5co-feature-text"><?php echo _("You can setup own SenderID by campaign based, its for free of cost")?>  </p>
								</a>
							</div>
							<div class="clearfix visible-sm-block"></div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="#" class="fh5co-feature to-animate">
									<span class="fh5co-feature-icon"><i class="icon-calendar"></i></span>
									<h3 class="fh5co-feature-lead"><?php echo _("Schedule Campaign")?></h3>
									<p class="fh5co-feature-text"><?php echo _("A great option to schedule your campaign based on timezones")?></p>
								</a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="#" class="fh5co-feature to-animate">
									<span class="fh5co-feature-icon"><i class="icon-laptop"></i></span>
									<h3 class="fh5co-feature-lead"><?php echo _("Responsive Layout")?></h3>
									<p class="fh5co-feature-text"><?php echo _("Our system support fully responsive, you can access it from any device")?> </p>
								</a>
							</div>

							<div class="clearfix visible-sm-block"></div>
							<div class="fh5co-spacer fh5co-spacer-sm"></div>
							<div class="col-md-4 col-md-offset-4 text-center to-animate">
								<a href="<?php echo base_url()?>features" class="btn btn-primary"><i class="icon-th-list"></i> <?php echo _("View All Features")?></a>
							</div>
			        	</div>
			       </div>
			       
			    </div>
				
			    <div id="fh5co-features-2" data-section="virtual_numbers">
					<div class="fh5co-features-2-content">
					    <div class="container">
					    	<div class="row">
					    		<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
									<h2 class="fh5co-lead to-animate"><?php echo _("Virtual Numbers")?></h2>
									<p class="fh5co-sub to-animate">
									<?php echo _("A Dedicated Number is a Virtual Phone Number that can be used for SMS campaign and belongs to only one account (unlike the OneTextGlobal shared pool numbers). Think of a virtual number as your own special unique code that customers and employees can get to know you by, that no one else can send from.")?>
									</p>
									<div class="fh5co-spacer fh5co-spacer-sm"></div>
									<a href="<?php echo base_url()?>site/login" class="btn btn-primary btn-md"><i class="icon-tag"></i> <?php echo _("Buy a number")?></a>
					    		</div>
					    		
					    	</div>
					    </div>
					</div>	

			    </div>
			    
				<div id="fh5co-counter" class="fh5co-bg-section" style="background-image: url(<?php echo base_url()?>theme4.0/landing/images/bg_2.jpg); background-attachment: fixed;">
					<div class="fh5co-overlay"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-12">
							 <?php 
							  $loadVisits=$this->countries_model->list_visitors_count();
							  $allMembers=$this->account_model->list_all_members();
							  $allCampaigns =$this->sms_model->total_campaign_count();
							  $total_successful_smscount=$this->sms_model->total_successful_smscount();
							  $total_unsuccessful_smscount=$this->sms_model->total_unsuccessful_smscount();
                              $allSMSCount=$total_successful_smscount+$total_unsuccessful_smscount;
							 ?>
								<div class="fh5co-hero-wrap">
									<div class="fh5co-hero-intro text-center to-animate counter-animate">
										<div class="col-md-3 text-center">
											<span class="fh5co-counter js-counter" data-from="0" data-to="<?php echo count($allMembers);?>" data-speed="5000" data-refresh-interval="50"></span>
											<span class="fh5co-counter-label"><?php echo _("Customers")?></span>
											
										</div>
										<div class="col-md-3 text-center">
											<span class="fh5co-counter js-counter" data-from="0" data-to="<?php echo $allCampaigns;?>" data-speed="5000" data-refresh-interval="50"></span>
											<span class="fh5co-counter-label"><?php echo _("Campaigns")?></span>
										</div>
										<div class="col-md-3 text-center">
											<span class="fh5co-counter js-counter" data-from="0" data-to="<?php echo $allSMSCount;?>" data-speed="5000" data-refresh-interval="50"></span>
											<span class="fh5co-counter-label"><?php echo _("Sms Sent")?></span>
										</div>
										<div class="col-md-3 text-center">
											<span class="fh5co-counter js-counter" data-from="100" data-to="<?php echo $loadVisits;?>" data-speed="5000" data-refresh-interval="50"></span>
											<span class="fh5co-counter-label"><?php echo _("Visitors")?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div id="fh5co-products" data-section="sms_gateway">
				    <div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single product-animate-1"><?php echo _("Enterprise SMS Gateway")?></h2>
								<p class="fh5co-sub animate-single product-animate-2">
								<?php echo _("Our SMS gateway centers around security, performance and reliability. 
								For the past decade, OneTextGlobal has partnered with organisations all over the globe to develop innovative mobile messaging solutions that drive revenue growth and deliver outstanding results for our Enterprise clients.")?>
								</p>
							</div>
						

							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="images/product_1.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_1.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Global Connectivity")?></h3>
									<p class="fh5co-figure-text"><?php echo _("Benefit from OneTextGlobal vendor relationships with global mobile carriers around the world, backed by our geographically distributed infrastructure and industry leading web messaging platform.")?></p>
								</a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_2.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_2.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Flexible & Scalable")?></h3>
									<p class="fh5co-figure-text"><?php echo _("OneTextGlobal's intelligent SMS routing system provides a powerful messaging architecture that is scalable to thousands of messages per second. Using bulk SMS you can instantly meet the evolving needs of your messaging requirements.")?></p>
								</a>
							</div>
							<div class="clearfix visible-sm-block"></div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_3.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_3.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("Quality to the Core")?></h3>
									<p class="fh5co-figure-text"><?php echo _("Benefit from real-time reporting, allowing you to track your message from inception to the handset. Should you encounter any difficulties, our 24/7 hotline and dedicated account managers are on-hand to troubleshoot.")?></p>
								</a>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12">
								<a href="<?php echo base_url()?>theme4.0/landing/images/product_4.jpg" class="fh5co-figure to-animate image-popup">
									<figure>
										<img src="<?php echo base_url()?>theme4.0/landing/images/product_4.jpg" alt="Free HTML5 Responsive Template" class="img-responsive">
									</figure>
									<h3 class="fh5co-figure-lead"><?php echo _("24/7 Monitoring")?></h3>
									<p class="fh5co-figure-text"><?php echo _("Rely on OneTextGlobal's round the clock monitoring of connectivity to carriers, with message throughput and delivery performance tracked by a proprietary routing system that responds dynamically to any delivery issues.")?></p>
								</a>
							</div>
							<div class="clearfix visible-sm-block"></div>
							<div class="fh5co-spacer fh5co-spacer-sm"></div>
							<div class="col-md-4 col-md-offset-4 text-center to-animate">
								<a href="<?php echo base_url()?>enterprise_sms_gateway" class="btn btn-primary"><i class="icon-th-list"></i> <?php echo _("Read More")?></a>
							</div>
						</div>
				    </div>
				   
				</div>

				<div id="fh5co-features-3" data-section="industries">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single features3-animate-1"><?php echo _("Choose Your Industry")?></h2>
								<p class="fh5co-sub animate-single features3-animate-2">
								<?php echo _("We supports all industries, no matter the industry, organisations across the globe are beginning to capitalise on the benefits SMS has to offer. Use SMS to help your business increase its consumer reach, improve your interactions with customers and grow their engagement with your brand.")?>
								</p>
							</div>

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-shopping-cart"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Retail")?></h4>
									<p>
									<?php echo _("Build loyalty programs, improve sales promotions and build your database with a multi-channel SMS approach.")?>
									</p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-briefcase"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Marketing & Advertising")?></h4>
									<p><?php echo _("Discover how integrating an SMS marketing strategy into your communications workflow can help your business grow.")?></p>
								</div>
							</div>
							
							<div class="clearfix visible-sm-block"></div>

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-train"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Travel & Tourism")?></h4>
									<p><?php echo _("Keep customers up to date with timely travel information, reservation confirmations and discount offers via SMS.")?></p>
								</div>
							</div>	

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-dollar"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Banking & Finance")?></h4>
									<p><?php echo _("Utilise SMS for billing reminders, server monitoring and fraud prevention to keep your customers abrest of critical information.")?></p>
								</div>
							</div>

							<div class="clearfix visible-sm-block"></div>

							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-book"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Education")?></h4>
									<p><?php echo _("Integrating SMS into your communications workflow is the perfect way to keep your students, parents & staff up-to-date.")?></p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 text-center fh5co-text-wrap">
								<div class="fh5co-text to-animate">
				    				<span class="fh5co-icon"><i class="icon-building"></i></span>
									<h4 class="fh5co-uppercase-sm"><?php echo _("Real Estate")?></h4>
									<p>
									<?php echo _("Schedule inspection reminders, manage rentals properties and broadcast auction alerts to prospective buyers via SMS.")?></p>
								</div>
							</div>
							<div class="fh5co-spacer fh5co-spacer-sm"></div>
							<div class="col-md-4 col-md-offset-4 text-center to-animate">
								<a href="<?php echo base_url()?>industries" class="btn btn-primary"><i class="icon-th-list"></i> <?php echo _("Read More")?></a>
							</div>

						</div>
					</div>
				</div>
				
				<div class="fh5co-bg-section cta" id="fh5co-cta" style="background-image: url(<?php echo base_url()?>theme4.0/landing/images/hero_bg.jpg); background-attachment: fixed;">
					<div class="fh5co-overlay"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="fh5co-hero-wrap">
									<div class="fh5co-hero-intro text-center">
										<div class="row">
											<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
												<h2 class="fh5co-lead to-animate"><?php echo _("Try Demo Account")?></h2>
												<p class="fh5co-sub to-animate"><?php echo _("No contract,No credit card required.Your account includes all our features and FREE credit for testing, feel the experience")?></p>

												<div class="to-animate">
												<a href="<?php echo base_url()?>site/demo" class="btn btn-primary"><i class="icon-laptop"></i> <?php echo _("Get Started!")?></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div id="fh5co-pricing" data-section="pricing">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single pricing-animate-1"><?php echo _("Simple SMS Pricing for Business")?></h2>
								<p class="fh5co-sub animate-single pricing-animate-2">
								 <?php echo _("No hidden fees, no surprises. Pay only for outbound text messages,Inbound SMS is FREE. Messages can traverse any of 1,800 mobile carriers, each with varying levels of dependability. 
								 OTG(OneTextGlobal) leaves nothing to chance, rigorously evaluating carriers to rule out SIM farms and carrier filtering.")?>
								 
								</p>
							</div>
							
							<div class="col-md-3 to-animate">
							 <a href="#" class="fh5co-figure">
							  <h3 class="fh5co-figure-lead">
							   <i class="icon-globe"></i> <?php echo _("Select Country")?>
							  </h3>
							  </a>
							</div>
							<div class="col-md-4 to-animate">
							   <?php 
							   $rsCountry = $this->countries_model->list_countries();
							   $remoteCountry=$this->config->item('remote_country_code');
							   ?>
							   <select class="form-control"  id="geo_country" name="geo_country">
							   <?php 
								 
								  if(count($rsCountry)>0){
									  for($rC=0;$rC<count($rsCountry);$rC++){
									  ?>
									  <option value="<?php echo $rsCountry[$rC]->country_code;?>" <?php if($rsCountry[$rC]->country_code==$remoteCountry){echo 'selected';}?>><?php echo $rsCountry[$rC]->country_name;?></option>
									  <?php
									  }
								  }
								 ?>
							   </select>
							   
							</div>
							<div class="col-md-2 to-animate">
							 <input type="text" class="form-control" id="check_count" name="check_count" placeholder="<?php echo _('Qty e.g. 100') ?>">
							</div>
							<div class="col-md-3 to-animate">
							  <button id="check_price" name="check_price" class="btn btn-primary">
							   <i class="icon-check"></i> <span id="check_title"><?php echo _("Check")?></span>
							  </button>
							</div>
							
							 <span class="text-center" id="price_set" style="display:none">
							  &nbsp;
							 </span>
							
						</div>
					</div>
				</div>

				<div id="fh5co-faqs"  data-section="faqs">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h2 class="fh5co-lead animate-single faqs-animate-1"><?php echo _("Frequently Ask Questions")?></h2>
								<p class="fh5co-sub animate-single faqs-animate-2">
								<?php echo _("Common queries we optimize by customers view")?> </p>
							</div>
						</div>
					</div>
					

					<div class="container">
						<div class="faq-accordion active to-animate">
							<span class="faq-accordion-icon-toggle active"><i class="icon-arrow-down"></i></span>
							<h3><?php echo _("When I send a message, does the person I'm sending it to know who sent it?")?></h3>
							<div class="faq-body" style="display: block;">
								<p>
								<?php echo _("The person will know that the text message came from you if")?>:
								<ul>
								<li><?php echo _("You send a text message from your personal mobile number and the recipient has already added you to his phone's contact list.")?></li>
								<li><?php echo _("You send a text message from your dedicated number and the recipient has already added you to his phone's contact list.")?></li>
								<li><?php echo _("You send a text message from a Sender ID - your company's name.")?></li>
								<li><?php echo _("If none of the above is the case, we recommend adding your name into the SMS body.")?></li>
								</ul>
								</p>
							</div>
						</div>
						<div class="faq-accordion to-animate">
							<span class="faq-accordion-icon-toggle"><i class="icon-arrow-down"></i></span>
							<h3><?php echo _("Can I receive SMS from my clients?")?></h3>
							<div class="faq-body">
								<?php echo _("You can receive replies and incoming SMS from your clients.")?>
								<ul>
								<li><?php echo _("If you use a share reply number, you can only receive replies to the messages you have sent earlier. All replies will be displayed in SMS Chat.")?></li>
								<li><?php echo _("If you have purchased a dedicated number, your clients can contact you whenever they need. All incoming messages will be displayed in SMS Chat. You can send and receive SMS using a dedicated number.")?></li>
								<li><?php echo _("You can buy a dedicated number")?> <a href="<?php echo base_url()?>site/login"><?php echo _("here.")?></a></li>
								</ul>
							</div>
						</div>
						<div class="faq-accordion to-animate">
							<span class="faq-accordion-icon-toggle"><i class="icon-arrow-down"></i></span>
							<h3><?php echo _("How do I change the name or the number that my SMS comes from?")?></h3>
							<div class="faq-body">
								<p><?php echo _("These options are available from CallerNumber/SenderID services")?> </p>
								<ul>
								<li><?php echo _("First logon to your account")?></li>
								<li><?php echo _("Goto CallerNumber Tab request for \"SenderID\" if you already purchased a caller number")?></li>
								<li><?php echo _("If you din't purchase a caller number, you must purchase it first")?></li>
								<li><?php echo _("Every caller number (a didicated virtual number) is linked up with didicated SenderId")?></li>
								<li><?php echo _("Follow the options accordingly")?></li>
								</ul>
							</div>
						</div>
						<div class="faq-accordion to-animate">
							<span class="faq-accordion-icon-toggle"><i class="icon-arrow-down"></i></span>
							<h3><?php echo _("Is there any provision to schedule my campaign ?")?></h3>
							<div class="faq-body">
								<p><?php echo _("Yes OneTextGlobal(OTG) provides this feature along with different timezones")?></p>
							</div>
						</div>
						<div class="faq-accordion to-animate">
							<span class="faq-accordion-icon-toggle"><i class="icon-arrow-down"></i></span>
							<h3><?php echo _("How to add funds to my \"wallet money\"?")?></h3>
							<div class="faq-body">
								<p><?php echo _("OTG provided a options called \"Add Funds\" using this option you may topup funds to wallet,
								where we allow different cards (debit/credit) like VISA/MASTER, JCB, American Express etc.")?> </p>
							</div>
						</div>
						<div class="faq-accordion to-animate">
							<span class="faq-accordion-icon-toggle"><i class="icon-arrow-down"></i></span>
							<h3><?php echo _("I have technical problem, who do I email?")?></h3>
							<div class="faq-body">
								<p><?php echo _("We happy to help to always, customer satisfaction is highly important to us. 
								Please mail us at")?> <b><?php echo $appSettings[0]->default_support_email;?></b> </p>
							</div>
						</div>
					</div>
				</div>

				<div id="fh5co-subscribe">
					<div class="container">
						<div class="row animate-box">
						   <div class="col-md-8 col-md-offset-2 fh5co-section-heading text-center">
								<h3 class="fh5co-lead animate-single pricing-animate-1 text-white"><?php echo _("For Inquiries")?></h3>
								<p class="fh5co-sub animate-single pricing-animate-2 text-white">
								
								 <?php echo _("Let us know how to get in touch for general inquiries, and we'll get back to you within twenty-four hours (or) reach us at")?> <?php echo $appSettings[0]->default_support_numbers?>.
								 <div>
									<a href="<?php echo base_url()?>enquiry" class="btn btn-danger btn-md"><i class="icon-envelope"></i> <?php echo _("Contact Us")?></a>
								 </div>
								</p>
							</div>
							 
						</div>
					</div>
				</div>

			</div>
		</div>
   </div>
 <?php 
  $this->load->view('front/includes/footer_inc');
 ?>