<div class="modal fade" id="paymentMoreInfoModal" tabindex="-1" role="dialog" 
     aria-labelledby="pMoreinfoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Payment Information")?> (<span id="txt_amount"></span>)
                </h4>
            </div>
			<div class="modal-body">
			    <h2 id="txn_orderno"></h2><hr>
				<b><?php echo _("Remarks")?></b>
			    <textarea id="txn_remarks" class="form-control" rows="6"></textarea> 
			 </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  <div class="pull-left" id="txt_id">&nbsp; </div>
				</div>
				<div class="col-md-6">
				  &nbsp;
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>