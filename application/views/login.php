<html>
<head>
<?php 
$intlib=$this->internal_settings->local_settings();
$copyrights    = $intlib[0]->brand_copyrights;
$appname       = $intlib[0]->app_default_name;
?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $appname;?> | <?php echo _('Administration Area')?> </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="<?php echo base_url()?>theme4.0/admin/images/ico/favicon.png">
    <link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/admin.css">
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/animation.css">
    <link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/blue.css">
	 <script src= "<?php echo base_url();?>js/angular.min.js"></script>
	 <script src="<?php echo base_url();?>js/users.js"></script>
  </head>

<body class="login-page">
   <?php
    if(!isset($this->session->userdata['login_in'])){
    ?>  
      
   
    <div ng-app="">
    <div class="login-box">
    <div class="box">
	<div class="login-logo animated fadeInDown" data-animation="fadeInDown">
        <img src="<?php echo base_url()?>theme4.0/admin/images/logo_black.png"/> 
    </div><div class="text-center text-muted"><?php echo _('Global Administration Panel')?></div><hr>
    <div class="login-box-body  animated fadeInUp" data-animation="fadeInUp">
        
        <form role="form" ng-controller="AuthController">
            <?php echo validation_errors(); ?>
            <?php echo $this->session->flashdata('error'); ?>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" id="username" placeholder="<?php echo _('Username') ?>" ng-model="username" name="username" required="required" />
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" id="password" placeholder="<?php echo _('Password') ?>" ng-model="password" name="password" required="required" />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <button type="submit" class="btn bg-blue btn-block btn-flat" ng-click='SignIn()'><?php echo _('Login'); ?></button>
            </div>
            <div class="row">
                <div class="col-xs-12">
                </div>
            </div>
        </form>

        <a href="<?php echo base_url()?>password/forgot"><i class="fa fa-key"></i> <?php echo _('I forgot my password'); ?></a><br>
       <br>
	    <div id="vPrompt" style="display:none">
			<span class="text-danger"><?php echo "<i class='fa fa-exclamation-triangle'></i> " . _('Username & Password cannot be empty..!') ?> </span>
		</div>	
    </div><!-- /.login-box-body -->
	</div>
	<p class="text-muted text-center"><i class="fa fa-copyright"></i> <?php echo $copyrights;?> </p>
   </div>
</div> 
    <?php
    }else{
    ?>
    <div class="row">
		<div id="content" class="col-sm-12 full">
			<div class="row">
			<div class="login-box" style="text-align:center">
			<h1 ><?php echo _('Alive'); ?></h1>
			<div class="error-desc">

			<?php echo _('You already logged in,session is still alive.'); ?>
		   <br><br>
			<a href="<?php echo base_url()?>dashboard" class="label label-primary"> 
			<i class="fa fa-circle"></i> <?php echo _('Goto Dashboard'); ?></a>
			</div>
			</div>
			</div>
		</div>
</div>
    <?php
    
    }
    ?>	
    
	
    <!-- Mainly scripts -->
    <script src="<?php echo base_url()?>js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    
   
    <script type="text/javascript">
    function AuthController($scope, $http) {
    $scope.SignIn = function() {
    
		var f1=$('#username').val();
		var f2=$('#password').val();
		
		if(f1!='' && f2!=''){
		
		$http.post('login/authenticate', {'uname': $scope.username, 'pswd': $scope.password}
		).success(function(data) {
		//alert(data);
		if (data!='')
		{
			window.location="dashboard";
			
		}
		else
		{
			$('#vPrompt').slideDown();
			$('#vPrompt').html("<span class='text-warning'><i class='fa fa-info-circle'></i> <?php echo _('Invalid username/password ..!'); ?></span>");
			setTimeout('eraseVprompt()',3000); 
		}
    })
    }
    else{
		$('#vPrompt').slideDown();	
		setTimeout('eraseVprompt()',3000); 
    	}
   	 }
    }
    
    
    function eraseVprompt(){
   	 $('#vPrompt').slideUp();
    }
    </script> 
    
</body>
</html>
