<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
<script type="application/javascript" src="<?php echo base_url()?>js/enquiry.js"></script>
<section class="content-header">
  <h1><?php echo _("Guest Enquiries")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Guest Enquiries")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_enquiries" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>enquiry/all" class="btn btn-sm btn-info"  title="<?php echo _('Reload') ?>">
		 <i class="fa fa-rotate-left"></i><?php echo _("Reload")?></a>
   </div>
</div> 
</div>
 <div class="box-body">
    <table class="table table-bordered table-striped" id="dataTables-enquiries">
		<thead>
		<tr>
		<th class="col-sm-1"><?php echo _("Id")?></th>
		<th class="col-sm-1"><?php echo _("Name")?></th>
		<th class="col-sm-1"><?php echo _("Type")?></th>
		<th class="col-sm-1"><?php echo _("Email Id")?></th>
		<th class="col-sm-1"><?php echo _("Contact No")?></th>
		<th class="col-sm-2"><?php echo _("Comments")?></th>
		<th class="col-sm-2"><?php echo _("Geo Information")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead>
		<tbody>
		<?php 
		$guest_info=$this->internal_settings->guest_enquiries();
		$sp = 1 
		?>
		<?php if ($guest_info!=0): foreach ($guest_info as $enquiry) : ?>
		<tr>
		<td>
		<?php echo $enquiry->enq_id?>
		</td>
		<td>
		  <?php echo ucfirst($enquiry->enq_name);?>
		  <?php echo "<br><small class='text-muted'> <b>" .  _("From") . " :</b> ".$enquiry->country_name."</small>"?>
		</td>
		<td>
		  <?php echo ucfirst($enquiry->enq_type);?>
		</td>
		<td>
		   <?php echo $enquiry->enq_email?>
		</td>
		<td>
		  <?php echo $enquiry->enq_contact?>
		</td>
		<td>
		  <?php echo $enquiry->enq_comments?>
		  <?php  echo "<br><small class='text-muted'> <b>" . _('Enquiry On') . "</b> ".date('d-M-Y h:i:s',strtotime($enquiry->enq_on))."</small>";?>
		</td>
		<td>
		  <?php 
		  $exp_raddress=explode(',',$enquiry->remote_address);
		  echo @$exp_raddress[0]."<hr>";
		  echo "<small class='text-muted'>".@$exp_raddress[1]."</small><br>";
		  echo "<small class='text-muted'>".@$exp_raddress[2]."</small><br>";
		  echo "<small class='text-muted'>".ucfirst(@$exp_raddress[3])."</small><br>";
		  echo "<small class='text-muted'>".ucfirst(@$exp_raddress[4])."</small><br>";
		  
		  ?>
		</td>
		<td>
		  <a href="javascript:void(0)" class="btn btn-xs btn-danger" title="<?php echo _('Delete Enquiry') ?>" onclick="delEnquiry('<?php echo $enquiry->enq_id?>','<?php echo $enquiry->enq_name?>')">
		  <i class="glyphicon glyphicon-trash"></i>
		 </a> 
		</td>
		</tr>
		<?php
		$sp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody>
</table> 

 </div>
</div>	
</section>
<?php $this->load->view('admin/enquiry/modals/delete_enquiry');?> 		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-enquiries').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>
		
