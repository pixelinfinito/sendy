<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
 
<script type="application/javascript" src="<?php echo base_url()?>js/users.js"></script>
<section class="content-header">
  <h1>
	<?php echo _("Users")?>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Users")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>site/export/export_groups" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>users/add" class="btn btn-sm btn-primary"  title="<?php echo _('New User') ?>">
		 <i class="fa fa-plus-circle"></i> <?php echo _("New User")?>
		</a>
   </div>
</div> 
</div>
<div class="box-body">
  <table class="table table-bordered table-striped" id="dataTables-userList">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?> </th>
		<th class="col-sm-1"><?php echo _("Thumbnail")?></th>
		<th class="col-sm-1"><?php echo _("Name")?> </th>
		<th class="col-sm-1"><?php echo _("Email ID")?> </th>
		<th class="col-sm-1"><?php echo _("Mobile No")?></th>
		<th class="col-sm-1"><?php echo _("User Group")?></th>
		<th class="col-sm-1"><?php echo _("Created On")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$user_info=$result=$this->user_model->list_users();
		$gp = 1 
		?>
		<?php if ($user_info!=0): foreach ($user_info as $user) : ?>

		<tr>
		<td>
		<?php echo $user->user_id?>
		</td>
		<td>
		<?php 
			if($user->profile_thumbnail!=''){
			?>
			<img src="<?php echo base_url().$user->profile_thumbnail?>" alt="Thumbnail" width="50" height="50" class="img-circle">

			<?php 
			}
			else
			{
			
			?>
			<img src="<?php echo base_url().'theme4.0/admin/images/avatar5.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">
			<?php
			
			}
		?>	
		</td>
		<td><?php echo $user->first_name.' '.$user->last_name?>
		</td>
		<td><?php echo $user->user_email ?></td>
		<td>
		   <?php echo $user->user_mobile ?> 
			 
		</td>
		<td>
		   <?php echo $user->group_name ?> 
			 
		</td>
		<td>
		<?php 
		 echo date('d-M-Y H:i:s',strtotime($user->date_of_join)); 
		?>
		</td>
		<td>
		<?php 
		
		  switch($user->status){
			  case 1:
			  echo '<span class="label label-success"> ' . _('Active') . '</span>';
			  break;
			  
			  case 0:
			  echo '<span class="label label-warning"> ' . _('InActive') . '</span>';
			  break;
		  }
		?>
		</td>
		
		<td>
		<a href="users/edit?id=<?php echo $user->user_id?>" title="<?php echo _('Edit') ?>" class='btn btn-xs btn-default'>
            <i class="fa fa-pencil"></i> 
        </a>
		 <?php 
		 if($user->username!='admin'){
			 ?>
			 <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delUser('<?php echo $user->user_id?>','<?php echo $user->username?>')"  class='btn btn-xs btn-danger'>
				<i class='glyphicon glyphicon-trash'></i> 
			 </a>
		  <?php 
		 }else{
			 ?>
			 <a href="javascript:alert('Sorry you cant delete master user')" title="<?php echo _('Delete') ?>"  class='btn btn-xs btn-default'>
				<i class='glyphicon glyphicon-trash'></i> 
			 </a>
			 <?php
			 
		 }
		  ?>
		</td>
		</tr>
		<?php
		$gp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody>
		</table>
	</div>
 </div>	
	<?php 
	 $this->load->view('modals/delete_user');
	?>
		
</section>
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-userList').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>
		
