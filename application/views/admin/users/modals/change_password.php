<div class="modal fade" id="modalProfilePassword" tabindex="-1" role="dialog" 
     aria-labelledby="profilePasswordLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="profilePasswordLabel">
                    <?php echo _("Change Password")?>
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
               <?php 
			     $user_info   = $this->user_model->list_current_user($this->input->get('id'));
			   ?>
                <form role="form" action="<?php echo base_url()?>users/change_password" method="post" enctype="multipart/form-data">
				
				
				<div class="row">
				<div class="form-group">
				<label class="col-md-3 "><?php echo _("Old Password")?> <small class="red">*</small></label>
				 <div class="col-md-8 text-left">	
				  <input type="password" name="old_password" id="old_password"  required="required" class="form-control"/>
				</div>
				</div>
				</div>
				
				<div class="row">
				<div class="form-group">
				<label class="col-md-3 "><?php echo _("New Password")?> <small class="red">*</small></label>
				 <div class="col-md-8 text-left">	
				  <input type="password" name="new_password" id="new_password"  required="required" class="form-control"/>
				</div>
				</div>
				</div>
				
				<div class="row">
				<div class="col-md-3">
				&nbsp;
				</div>
				<div class="col-md-7">
				 <button type="submit" class="btn btn-info"><i class="fa fa-key"></i> <?php echo _("Change")?></button>
				 </div>
				</div>
				
				</form>
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <small class="text-danger">* <?php echo _("Mandatory Fields")?></small>
            </div>
        </div>
    </div>
</div>