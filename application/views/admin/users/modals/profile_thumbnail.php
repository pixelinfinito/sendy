<div class="modal fade" id="modalProfileThumb" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Change Profile Picture")?>
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
               <?php 
			   $user_info   = $this->user_model->list_current_user($this->input->get('id'));
			   ?>
                <form role="form" action="<?php echo base_url()?>users/thumbnail" method="post" enctype="multipart/form-data">
				
				<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_info[0]->user_id?>" />
				<div class="row">
				<div class="form-group">
				<label class="col-md-2 control-label"><?php echo _("Choose Picture")?> <small class="red">*</small></label>
				 <div class="col-md-7">	
				  <input type="file" name="files" id="photoimg"  required="required" class="form-control"/>
				  <small>[<?php echo _("This will override your existing profile picture")?>] </small>
				</div>
				<div class="col-md-3">
				 <button type="submit" class="btn btn-info"><i class="fa fa-upload"></i> <?php echo _("Upload")?></button>
				</div>
				</div>
				</div>
				
                </form>
                
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <small class="text-danger">* <?php echo _("Mandatory Fields")?></small>
            </div>
        </div>
    </div>
</div>