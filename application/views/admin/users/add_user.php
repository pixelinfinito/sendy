<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
	
<script type="application/javascript" src="<?php echo base_url();?>js/users.js"></script>
<?php
$user_groups= $this->user_groups_model->list_groups();
$total_users= $this->user_model->list_users();
$inactive_users=$this->user_model->list_inactive_users();
?>

 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo _("Add User")?>
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>users"><?php echo _("Users")?></a></li> <li class="active"><?php echo _("Add User")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div ng-app="">
        <div ng-controller="UserController">
          
            <div class="row">
            <div class="col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			     <form role="form" ng-controller="UserController" name="userForm" id="userForm"  >
				<div class="form-group">
				<label><?php echo _("Username")?> </label> <small class="red">*</small>
				<input type="text" placeholder="" class="form-control" ng-model="user_name" id="user_name" required="required" name="user_name">
				 </div>
									   
				<div class="form-group">
				<label><?php echo _("Password")?> </label> <small class="red">*</small>
				<input type="text" placeholder="<?php echo _('Eg: xHbd2453#') ?>" class="form-control" ng-model="password" id="password" required="required" name="password">
				</div>
				
				 <div class="form-group">
				 
				<label><?php echo _("First Name")?> </label> <small class="red">*</small>
				<input type="text" placeholder="" class="form-control" ng-model="first_name" id="first_name" required="required" name="first_name">
				</div>
				
				 <div class="form-group">
				<label><?php echo _("Last Name")?> </label> <small class="red" style="font-size:14px">*</small>
				<input type="text" placeholder="" class="form-control" ng-model="last_name" id="last_name" name="last_name">
				</div>
				
				 <div class="form-group">
				<label><?php echo _("Email")?> </label> <small class="red">*</small>
				<input type="email" placeholder="<?php echo _('example@example.com') ?>" class="form-control" ng-model="email_id" id="email_id" required="required" name="email_id">
				
				</div>
				
				 <div class="form-group">
				<label><?php echo _("Mobile No")?></label>
				<input type="text" placeholder="<?php echo _('E.g. 673xxxxxx') ?>" class="form-control" ng-model="telephone" id="telephone" >
				</div>
				
				<div class="form-group">
				<label><?php echo _("Phone No")?></label>
				<input type="text" placeholder="<?php echo _('E.g. 673xxxxx') ?>" class="form-control" ng-model="telephone2" id="telephone2" >
				</div>
				
			   <div class="form-group">
				<label><?php echo _("User Group")?> </label> <small class="red">*</small>
				  <select class="form-control" id="user_groups" ng-model="user_groups" name="user_groups" required>
				   <option value="">--<?php echo _("Choose")?>--</option>
				   <?php
					foreach($user_groups as $groups){
						?>
						<option value="<?php echo $groups->group_id?>">
						<?php echo $groups->group_name?>
						</option>
						<?php
					}
				   ?>
				  </select>
				</div>
				
			   <div class="form-group">
			  <label><?php echo _("Status")?></label> <small class="red">*</small>
				  <select class="form-control" id="user_status" ng-model="user_status" name="user_status" required>
				  <option value="">--<?php echo _("Choose")?>--</option>
				   <option value="1"><?php echo _("Active")?></option>
				   <option value="0"><?php echo _("Inactive")?></option>
				  </select>
				  <small><?php echo _("Inactive users are restricted to access the system.")?></small><br>
				  <small class="red">* <?php echo _("Mandatory field(s).")?></small>
				</div>
				
			   <div class="form-group">
					<!--<button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="button" ng-click='AddUser();' data-toggle="modal" data-target="#myModal" ><strong><?php echo _("Add")?></strong>
					</button>-->
					 <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='AddUser();'>
					 <i class="fa fa-plus"></i> <?php echo _("Add")?>
					</button>
				   
				</div>
				<div id="rsDiv"></div>
				<br>
			  </form>
			  </div>
            </div>
            </div>
			<div class="col-md-4">
                       
			  <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua">
                  <div class="widget-user">
                    <h3><i class="fa fa-user"></i> <?php echo _("Total Users")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?>
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_users!=0){
						  if(count($total_users)>10){
							  echo count($total_users);
						  }else{
							  echo "0".count($total_users);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_users!=0){
						  if(count($inactive_users)>10){
							  echo count($inactive_users);
						  }else{
							  echo "0".count($inactive_users);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Telephone is either mobile (or) land line number , along with country code.")?></li>
				</ul>
                        
              </div>
				
			</div>
			</div>
			
            </div>
           
        </section><!-- /.content -->
		
		
<?php $this->load->view('theme/footer.php');?>
		
