<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<script type="application/javascript" src="<?php echo base_url()?>theme4.0/admin/tinymce/tinymce.js"></script>
<script type="application/javascript" src="<?php echo base_url()?>js/notifications/send.js"></script>

<script>
  tinymce.init({
    selector: '#notify_message',
	theme: 'modern',
    height: 250,
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
    ],
    content_css: '<?php echo base_url()?>theme4.0/admin/tinymce/skins/lightgray/content.min.css',
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
  });
</script>

<?php
 
 $notify_from= $this->session->userdata['login_in']['username'];
 $notify_from_email1= $this->session->userdata['login_in']['user_email'];
 
 $intlib=$this->internal_settings->local_settings();
 $notify_from_email2= $intlib[0]->default_from_mail;
 $notify_appname= $intlib[0]->app_default_name;
 
 
 $total_members=$this->account_model->list_all_members();
 $inactive_members=$this->account_model->list_inactive_members();
 $total_payment_transactions=$this->payment_model->total_member_payments();
 
 $appSettings= $this->app_settings_model->get_primary_settings();
 $currency_name=$appSettings[0]->app_currency;
 
?>

<section class="content-header">
  <h1><?php echo _("Notifications")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>notifications/all"><?php echo _("Notifications")?></a></li> <li class="active"><?php echo _("Send")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			
				<div ng-app="">
                 <form role="form" ng-controller="NotifyController" name="notifyForm" id="notifyForm" method="post" action="<?php echo base_url()?>notifications/process_general_notification" enctype="multipart/form-data">
				   <div class="form-group">
                    
					<div class="row">
					<div class="col-md-6">
					<label><?php echo _("Notification Type")?></label> <span class="red">*</span>
				    <select class="form-control" id="notify_type" name="notify_type" required="required">
					 <option value="general"><?php echo _("General")?></option>
					 <option value="finance"><?php echo _("Financial")?></option>
					 <option value="technical"><?php echo _("Technical")?></option>
					 <option value="promotional"><?php echo _("Promotional")?></option>
					 <option value="other"><?php echo _("Other")?></option>
					</select>
					
					</div>
					<div class="col-md-6">
					<label><?php echo _("Notification From")?></label>
						<input type="text" class="form-control" name="notify_from" id="notify_from" readonly value="<?php echo $notify_from.' ['.$notify_appname.']';?>"> 
					</div>
					</div>
					</div>
					
					<div id="notify_maildiv" style="display:none">
					<div class="form-group">
                    <label><?php echo _("Notification")?> <em>(<?php echo _("From Email")?>)</em></label>
					<select class="form-control" id="notify_from_mail" name="notify_from_mail">
					 <option value="<?php echo $notify_from_email1;?>"><?php echo $notify_from_email1;?></option>
					 <option value="<?php echo $notify_from_email2;?>"><?php echo $notify_from_email2;?></option>
					 <option value="<?php echo "no-reply@domain.com";?>"><?php echo "no-reply@domain.com";?></option>
					</select>
					</div>
					</div>
					
                    <div class="form-group">
                    <label><?php echo _("To Whom")?></label> <span class="red">*</span>
						 <select class="form-control"  id="notify_to" required="required" name="notify_to" onchange="javascript:loadMemberCredit(this.value,'<?php echo $currency_name;?>')">
						 <option value="">--<?php echo _("Select")?>--</option>
						 <option value="all" class="text-danger" style="font-weight:bold">--<?php echo _("To All Members")?>--</option>
						 <?php 
						  foreach($total_members as $member){
							  ?>
							  <option value="<?php echo $member->user_id;?>">
							   <?php echo $member->ac_first_name.' '.$member->ac_last_name.' ('.$member->country_name.' )'?>
							  </option>
							  <?php
							  
						  }
						 ?>
						</select>
						
						<div id="member_credit" class="text-muted"></div>
                    </div>
					<div class="form-group">
                    <label><?php echo _("Headline")?></label> <span class="red">*</span>
					<input type="text" class="form-control" name="notify_headline" id="notify_headline" placeholder="<?php echo _('E.g. This is technical notification') ?>" required="required">
					</div>
					
                    <div class="form-group">
                    <label><?php echo _("Message")?></label>
                    <textarea placeholder="<?php echo _('Notification message') ?>" class="form-control"  id="notify_message" name="notify_message" rows="7"></textarea>
					</div>
					
					<div class="form-group">
					<label><?php echo _("Attach File")?></label>
					<input type="file" name="file" id="file1"  class="form-control">
					<small class="text-muted"><em>(<?php echo _("Formats .pdf, .doc , .docx")?>)</em></small>
					</div> 
				
					
					<div class="form-group">
                     <input type="checkbox" id="notify_mailcheck" name="notify_mailcheck">
					 <input type="hidden" id="notify_sendby_mail" name="notify_sendby_mail"><?php echo _("Let members know by email aswell")?></div>
					
                    <div class="form-group">
                         <button class="btn btn-success pull-left m-t-n-xs" type="submit" ng-click='sendNotifyBtn()'>
						 <i class="fa fa-envelope-o"></i><?php echo _("Send")?></button>
                    </div>
					<div id="rsDiv"></div>
					<br>
					</form>
					</div>
				</div>
			</div>
            </div>
		    <div class="col-md-4">
			   
			<div class="box box-widget widget-user-2">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header bg-primary">
			  <div class="widget-user">
				 <h3><i class="fa fa-bell-o"></i> <?php echo _("Total Notifies ")?>:
				<?php 
				 $notify_count=$this->notification_model->list_notifications();
				 if($notify_count!=0){
					 echo "<b>".count($notify_count)."</b>";
				 }else{
					 echo "<b>0</b>";
				 }
				?>
				</h3>
			  </div><!-- /.widget-user-image -->
			</div>
			 <div class="box-footer no-padding">
			   <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Delivered")?>
					<span class="pull-right badge bg-blue">
						<?php 
						$notify_delivered=$this->notification_model->list_delivered_notifications();
						 if($notify_delivered!=0){
						  if(count($notify_delivered)>10){
							  echo count($notify_delivered);
						  }else{
							  echo "0".count($notify_delivered);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
					 <li><a href="#"><?php echo _("Failed")?>
					<span class="pull-right badge bg-red">
						<?php 
						$notify_failed=$this->notification_model->list_failed_notifications();
						 if($notify_failed!=0){
						  if(count($notify_failed)>10){
							  echo count($notify_failed);
						  }else{
							  echo "0".count($notify_failed);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
				</ul>
			 </div>
			</div>
			
			 
			  <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-maroon">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i><?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?>
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Cross check <b>type of notification </b> & send to particular member (or) all members.")?></li>
				 <li><?php echo _("Cross check <b>Notification <em>(From Email</em></b> if you sending notification by email.")?></li>
				</ul>
		    </div>
   </div>

</section>

<?php $this->load->view('theme/footer.php');?>

