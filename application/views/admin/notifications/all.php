<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
 
<script type="application/javascript" src="<?php echo base_url()?>js/notifications/send.js"></script>
<section class="content-header">
  <h1><?php echo _("Notifications")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Notifications")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_all_notifications" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>
	
	 <a href="javascript:void(0)" id="link_delete_all" class="btn btn-sm btn-default explink"  title="<?php echo _('Delete Selected Notifications') ?>">
			<span id="delete_all_notify" class="text-danger">
			<i class="glyphicon glyphicon-trash"></i>
			</span>
	 </a>

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>notifications/send" class="btn btn-sm btn-primary"  title="<?php echo _('Send Notification') ?>">
		 <i class="fa fa-bell-o"></i><?php echo _("Send Notification")?></a>
   </div>
</div> 
</div>
 <div class="box-body">
    <table class="table table-bordered table-striped" id="dataTables-Notify">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-xs-1">
          <input type="checkbox" name="check_all_notify" id="check_all_notify">
		</th>
		<th class="col-sm-1"><?php echo _("Id")?></th>
		<th class="col-sm-1"><?php echo _("Notification Type")?></th>
		<th class="col-sm-1"><?php echo _("Title")?></th>
		<th class="col-sm-1"><?php echo _("Notification To")?></th>
		<th class="col-sm-1"><?php echo _("Is Read")?></th>
		<th class="col-sm-1"><?php echo _("Attachment")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$notify_info=$result=$this->notification_model->list_notifications();
		$sp = 1 
		?>
		<?php if ($notify_info!=0): foreach ($notify_info as $notify) : ?>

		<tr>
		<td>
	 	 <input type="checkbox" class="check_notify" name="check_notify" value="<?php echo $notify->notify_id;?>">
		</td>
		
		<td>
		<?php echo $notify->notify_id?>
		</td>
		<td><?php echo ucfirst($notify->notify_type);?>
		</td>
		<td>
		 
		  <?php 
		   if($notify->read_status!=0){
			   ?>
			   <a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->notify_id;?>)">
			   <u><?php echo $notify->notify_title?></u>
			   </a>
			   <?php
		   }else{
			   ?>
			   <a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->notify_id;?>)">
			   <u><b><?php echo $notify->notify_title?></b></u>
			   </a>
			   <?php
		   }
		  ?>
		
		
		</td>
		<td>
		  <?php 
		  echo ucfirst($notify->notify_to)."<br>";
		  echo "<small class='text-muted'> <b>" . _('Sent On') . "</b> ".date('d-M-Y h:i:s',strtotime($notify->notify_timestamp))."</small>";
		  ?>  
		</td>
		<td>
		  <?php 
		   if($notify->read_status!=0){
			   echo "<span class='text-success'><i class='fa fa-check'></i> " . _("Yes") . "</span>";
		   }else{
			   echo "<span class='text-danger'><i class='fa fa-hourglass-half'></i>" . _("No") . "</span>";
		   }
		  ?>  
		</td>
		<td>
		  <?php 
		  if($notify->attachment_path!=''){
		  ?>
		  <a href="<?php echo base_url().$notify->attachment_path ?>">
		   <h5><i class="fa fa-paperclip"></i></h5>
		  </a>  
		  <?php 
		  }else{
			  
			  echo "-";
		  }
		  ?>
		</td>
		<td>
		<a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->notify_id;?>)" class="btn btn-xs btn-default">
		  <i class="fa fa-envelope-o"></i>
		 </a> 
		 
		 <a href="javascript:void(0)" class="btn btn-xs btn-danger" title="<?php echo _('Delete Message') ?>" onclick="delNotify('<?php echo $notify->notify_id?>','<?php echo $notify->notify_title?>','<?php echo $notify->notify_to?>')">
		  <i class="glyphicon glyphicon-trash"></i>
		 </a> 
		</td>
	
		</tr>
		<?php
		$sp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
</table> <!-- / Table -->

<?php $this->load->view('site/modals/notification_body');?> 
<?php $this->load->view('admin/notifications/modals/delete_notification');?> 
<?php $this->load->view('admin/notifications/modals/delete_all_notification');?> 
 </div>
</div>	
</section>
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-Notify').DataTable({
			"order": [[ 1, "desc" ]],
			"aoColumnDefs" : [{
            'bSortable' : false,
            'aTargets' : [ 0,5 ]}],
			"columns": [
			{ "width": "1%" },
			null,
			null,
			null,
			null,
			null,
			null,
			null
			]
		});
		
		
		
    });
</script>
		
