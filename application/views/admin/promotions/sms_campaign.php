<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<script type="application/javascript" src="<?php echo base_url()?>js/promotions/promotions.js"></script>

<?php
 
 $notify_from= $this->session->userdata['login_in']['username'];
 $notify_from_email1= $this->session->userdata['login_in']['user_email'];
 
 $intlib=$this->internal_settings->local_settings();
 $notify_from_email2= $intlib[0]->default_from_mail;
 $notify_appname= $intlib[0]->app_default_name;
 
 
 $total_members=$this->account_model->list_all_members();
 $inactive_members=$this->account_model->list_inactive_members();
 $total_payment_transactions=$this->payment_model->total_member_payments();
 
 $appSettings= $this->app_settings_model->get_primary_settings();
 $sms_settings= $this->app_settings_model->get_sms_settings();
 
 $currency_name=$appSettings[0]->app_currency;
 
?>

<section class="content-header">
  <h1>
	<?php echo _("Text Message")?> <small>(<?php echo _("SMS Campaign")?>)</small>
	
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>promotions/all"><?php echo _("Promotions")?></a></li> <li class="active"><?php echo _("SMS Campaign")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			     <div class="row">
				 <div class="col-md-6 text-muted">
				 <?php echo _("Send some promotional messages your members e.g. review pricing information, discounts, free sms upon requests etc.,")?>
				 </div>
				 <div class="col-md-6 text-right">
				  <?php 
				   $promotion_code='PRS-'.rand(1000,10000);
				   echo "<span class='text-primary'>" . _('Promotion ID') . " : <b>".$promotion_code."</b></span>";
				   
				  ?>
				  
				 </div>
				 </div>
				 <hr>
				<div ng-app="">
                 <form role="form" ng-controller="PromitionSMSController" name="promitionSMSForm" id="promitionSMSForm" method="post" action="<?php echo base_url()?>promotions/process_sms_campaign" enctype="multipart/form-data">
				   <div class="form-group">
                    
					<div class="row">
					<div class="col-md-6">
					<label><?php echo _("Promotion Type")?></label> <span class="red">*</span>
				    <select class="form-control" id="notify_type" name="notify_type" required="required">
					 <option value="1"><?php echo _("SMS")?></option>
					</select>
					
					<input type="hidden" value="<?php echo $promotion_code?>" name="promotion_code" id="promotion_code">
					</div>
					<div class="col-md-6">
					<label><?php echo _("Sender From")?></label>
					<select class="form-control" name="notify_from" id="notify_from">
					 <option value="<?php echo $sms_settings[0]->twilio_origin_number?>">
					  <?php echo @$sms_settings[0]->twilio_origin_number?>
					 </option>
					 <option value="<?php echo $sms_settings[0]->twilio_sender_id?>">
					  <?php echo @$sms_settings[0]->twilio_sender_id?>
					 </option>
					</select>
					</div>
					</div>
					</div>
					
					
                    <div class="form-group">
                    <label><?php echo _("To Whom")?></label> <span class="red">*</span>
						 <select class="form-control"  id="notify_to" required="required" name="notify_to" onchange="javascript:loadMemberCredit(this.value,'<?php echo $currency_name;?>')">
						 <option value="">-- <?php echo _("Select")?> --</option>
						 <option value="all" class="text-danger" style="font-weight:bold">-- <?php echo _("To All Members")?> --</option>
						 <?php 
						  foreach($total_members as $member){
							  ?>
							  <option value="<?php echo $member->user_id;?>">
							   <?php echo $member->ac_first_name.' '.$member->ac_last_name.' ('.$member->country_name.' )'?>
							  </option>
							  <?php
							  
						  }
						 ?>
						</select>
						
						<div id="member_credit" class="text-muted"></div>
                    </div>
					
                    <div class="form-group">
                    <label><?php echo _("Message")?> </label> <span class="red">*</span>
                    <textarea placeholder="<?php echo _('Notification message') ?>" class="form-control"  id="notify_message" name="notify_message" rows="7"></textarea>
					<small><?php echo _("Maximum 255 characters allowed")?></small>
					</div>
					
					
                    <div class="form-group">
                         <button class="btn btn-info pull-left m-t-n-xs" type="submit" ng-click='sendSMSPromotionBtn()'>
						 <i class="fa fa-send-o"></i> <?php echo _("Send")?>
                        </button>
                    </div>
					<div id="rsDiv"></div>
					<br>
					</form>
					</div>
				</div>
			</div>
            </div>
		    <div class="col-md-4">
			   
			<div class="box box-widget widget-user-2">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header bg-maroon">
			  <div class="widget-user">
				 <h3><i class="fa fa-envelope-o"></i> <?php echo _("Total Promotions ")?>:
				<?php 
				 $promotion_count=$this->promotions_model->list_promotion_count();
				 if($promotion_count!=0){
					 echo "<b>".count($promotion_count)."</b>";
				 }else{
					 echo "<b>0</b>";
				 }
				?>
				</h3>
			  </div><!-- /.widget-user-image -->
			</div>
			 <div class="box-footer no-padding">
			   <ul class="nav nav-stacked">
			       
                    <li><a href="#"><?php echo _("Delivered")?>
					<span class="pull-right badge bg-blue">
						<?php 
						 $promotion_delivered=$this->promotions_model->list_delivered_promotions();
						 if($promotion_delivered!=0){
						  if(count($promotion_delivered)>10){
							  echo count($promotion_delivered);
						  }else{
							  echo "0".count($promotion_delivered);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
					
					 <li><a href="#"><?php echo _("In Waiting")?>
					<span class="pull-right badge bg-primary">
						<?php 
						$promotion_waiting=$this->promotions_model->list_waiting_promotions();
						 if($promotion_waiting!=0){
						  if(count($promotion_waiting)>10){
							  echo count($promotion_waiting);
						  }else{
							  echo "0".count($promotion_waiting);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
					
					 <li><a href="#"><?php echo _("Failed")?>
					<span class="pull-right badge bg-red">
						<?php 
						$promotion_failed=$this->promotions_model->list_failed_promotions();
						 if($promotion_failed!=0){
						  if(count($promotion_failed)>10){
							  echo count($promotion_failed);
						  }else{
							  echo "0".count($promotion_failed);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
				</ul>
			 </div>
			</div>
			
			 
			  <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-blue">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i> <?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?>
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Cross check <b>From name, type of promotion </b> & send to particular member (or) all members.")?></li>
				 <li><?php echo _("Make sure weather you selected correct <b>(From email address)</b>")?></li>
				</ul>
		    </div>
   </div>

</section>

<?php $this->load->view('theme/footer.php');?>

