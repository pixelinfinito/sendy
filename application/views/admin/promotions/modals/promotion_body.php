<div class="modal fade" id="NotifyBodyModal" tabindex="-1" role="dialog" 
     aria-labelledby="NotifyBodyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="NotifyBodyLabel">
                   <?php echo _("Promotion Message")?>
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
			 <div id="dyNotifyBody">
			  &nbsp;
			 </div>
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                &nbsp;
            </div>
        </div>
    </div>
</div>