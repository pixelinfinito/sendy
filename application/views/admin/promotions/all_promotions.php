<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
 
<script type="application/javascript" src="<?php echo base_url()?>js/promotions/promotions.js"></script>
<section class="content-header">
  <h1>
	<?php echo _("Promotions")?>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Promotions")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_promotions" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>
	
	 <a href="javascript:void(0)" id="link_delete_all" class="btn btn-sm btn-default explink"  title="<?php echo _('Delete Selected Promotions') ?>">
			<span id="delete_all_notify" class="text-danger">
			<i class="glyphicon glyphicon-trash"></i>
			</span>
	 </a>
	 

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>promotions/email_campaign" class="btn btn-sm btn-primary"  title="<?php echo _('Email Campaign') ?>">
		 <i class="fa fa-envelope-o"></i> <?php echo _("Compose Email")?>
		</a>
		
		 <a href="<?php echo base_url()?>promotions/sms_campaign" class="btn btn-sm btn-info"  title="<?php echo _('Email Campaign') ?>">
		 <i class="fa fa-mobile"></i> <?php echo _("Compose SMS")?>
		</a>
   </div>
</div> 
</div>
 <div class="box-body">
    <table class="table table-bordered table-striped" id="dataTables-Notify">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-xs-1">
          <input type="checkbox" name="check_all_notify" id="check_all_notify">
		</th>
		<th class="col-sm-1"><?php echo _("Id")?></th>
		<th class="col-sm-1"><?php echo _("Promotion Code")?></th>
		<th class="col-sm-1"><?php echo _("Subject")?></th>
		<th class="col-sm-1"><?php echo _("Recipient")?> </th>
		<th class="col-sm-1"><?php echo _("Attachment")?></th>
		<th class="col-sm-1"><?php echo _("Message Type")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$promotion_info=$this->promotions_model->list_promotions();
		$sp = 1 
		?>
		<?php if ($promotion_info!=0): foreach ($promotion_info as $notify) : ?>

		<tr>
		<td>
	 	 <input type="checkbox" class="check_notify" name="check_notify" value="<?php echo $notify->promotion_id;?>">
		</td>
		<td>
		<?php echo $notify->promotion_id?>
		</td>
		<td><?php echo ucfirst($notify->promotion_code);?>
		</td>
		<td>
		<a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->promotion_id;?>)" title="<?php echo _('View Message') ?>">
		 <u><?php echo ucfirst($notify->promotion_title);?></u>
		 </a>
		  
		</td>
		<td>
		  <?php 
		  echo ucfirst($notify->to_name)."<br>";
		  echo "<small class='text-muted'> <b>" . _('Sent On') . "</b> ".date('d-M-Y h:i:s',strtotime($notify->notify_timestamp))."</small>";
		  ?>  
		</td>
		
		<td>
		  <?php 
		  if($notify->attachment_path!=''){
		  ?>
		  <a href="<?php echo base_url().$notify->attachment_path ?>">
		   <h5><i class="fa fa-paperclip"></i></h5>
		  </a>  
		  <?php 
		  }else{
			  
			  echo "-";
		  }
		  ?>
		</td>
		<td>
		 <?php 
		  switch($notify->promotion_type){
			  case '1':
			  echo "<i class='fa fa-mobile'></i> SMS";
			  break;
			  
			  case '2':
			  echo "<i class='fa fa-envelope-o'></i> Email";
			  break;
			  
		  }
		 ?>
		</td>
		<td>
		 <?php 
		  switch($notify->status){
			  
			  case '1':
			  echo '<span class="label label-success"><i class="fa fa-check"></i> ' . _('Delivered') .'</span>';
			  break;
			  
			  case '2':
			  echo '<span class="label label-warning"><i class="fa fa-hourglass-half"></i> ' . _('Waiting') .'</span>';
			  break;
			  
			  case '0':
			  echo '<span class="label label-danger"><i class="fa fa-exclamation-triangle"></i> ' . _('Failed') .'</span>';
			  break;
		  }
		 ?>
		</td>
		<td>
		  
		  <?php 
		  switch($notify->promotion_type){
			  case '1':
			  ?>
			  <a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->promotion_id;?>)" class="btn btn-xs btn-info" title="<?php echo _('View Message') ?>">
				<i class='fa fa-comments-o'></i>
			 </a>
			  <?php
			  break;
			  
			  case '2':
			  ?>
			  <a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->promotion_id;?>)" class="btn btn-xs btn-primary" title="<?php echo _('View Message') ?>">
			  <i class="fa fa-envelope-o"></i>
			  </a>
			  <?php
			  
			  break;
			  
		  }
		  ?>
		  
		 </a> 
		 
		 <a href="javascript:void(0)" class="btn btn-xs btn-danger" title="<?php echo _('Delete Message') ?>" onclick="delPromotion('<?php echo $notify->promotion_id?>','<?php echo $notify->promotion_title?>','<?php echo $notify->to_name?>')">
		  <i class="glyphicon glyphicon-trash"></i>
		 </a> 
		</td>
	
		</tr>
		<?php
		$sp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
</table> <!-- / Table -->

<?php $this->load->view('admin/promotions/modals/promotion_body');?> 
<?php $this->load->view('admin/promotions/modals/delete_promotion');?> 
<?php $this->load->view('admin/promotions/modals/delete_all_promotions');?> 
 </div>
</div>	
</section>
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		
		$('#dataTables-Notify').DataTable({
			"order": [[ 1, "desc" ]],
			"aoColumnDefs" : [{
            'bSortable' : false,
            'aTargets' : [ 0,5 ]}],
			"columns": [
			{ "width": "1%" },
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null
			]
		});
		
		
    });
</script>
		
