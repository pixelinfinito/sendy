<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
 
<?php 
$total_members=$this->account_model->list_all_members();
$total_campaigns=$this->sms_model->get_all_campaigns();
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
?> 
<script type="application/javascript" src="<?php echo base_url()?>js/sms_campaign/sms_backend.js"></script>
<section class="content-header">
  <h1>
	<?php echo _("SMS History by Campaign")?>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("SMS History by Campaign")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_history_bycampaign/<?php echo str_replace('=','',$this->input->get('c_code')).'/'.str_replace('=','',$this->input->get('id'))?>" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>sms/history" class="btn btn-sm btn-primary"  title="<?php echo _('Scheduled Campaigns') ?>">
		 <i class="fa fa-envelope-o"></i>&nbsp; <?php echo _("SMS History")?>
		</a>
   </div>
</div> 
</div>
<div class="box-body">
 <form role="form" method="get">
		<div class="row"> 
		<div class="col-md-2">
		  <label><?php echo _("Select Campaign Code")?> </label><span class="red">*</span>
		</div>
		<div class="col-md-3">			
		<select class="form-control"  id="c_code" required="required" name="c_code">
		 <option value="">--<?php echo _("Select")?>--</option>
		 <?php 
		  foreach($total_campaigns as $campaign){
			  ?>
			  <option value="<?php echo base64_encode($campaign->campaign_code)?>">
			   <?php echo "#".$campaign->campaign_code.' (by - '.$campaign->ac_first_name.' '.$campaign->ac_last_name.' )'?>
			  </option>
			  <?php
			  
		  }
		 ?>
		</select>
		</div>
		
		<div class="col-md-2">
		  <label><?php echo _("Select Member")?> </label>
		</div>
		<div class="col-md-3">			
		<select class="form-control"  id="id" name="id">
		 <option value="">--<?php echo _("Select")?>--</option>
		 <?php 
		  foreach($total_members as $member){
			  ?>
			  <option value="<?php echo base64_encode($member->user_id)?>">
			   <?php echo $member->ac_first_name.' '.$member->ac_last_name.' ('.$member->country_name.' )'?>
			  </option>
			  <?php
			  
		  }
		 ?>
		</select>
		</div>
		
		<div class="col-md-2">
		  <button class="btn btn-warning pull-left" type="submit">
		   <i class="fa fa-check"></i> <?php echo _("Check")?>
		  </button>
		</div>
		</div>
		<br>
		
		<div class="row"> 
		<div class="col-md-2">
		 &nbsp;
		</div>
		<div class="col-md-6">
		<i class="fa fa-info-circle"></i> <?php echo _("Check SMS history followed by member or campaign.")?>
		</div>
		<div class="col-md-3">
		 &nbsp;
		</div>
		</div>
		<hr>
 </form>
	 <?php 
	   $user_id=base64_decode($this->input->get('id'));
	   $campaign_code=base64_decode($this->input->get('c_code'));
	   if($campaign_code!=''){
     ?>	 
   
    <table class="table table-bordered table-striped" id="dataTables-SmsHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Campaign Code")?></th>
		<th class="col-sm-1"><?php echo _("From")?></th>
		<th class="col-sm-1"><?php echo _("To")?></th>
		<th class="col-sm-2"><?php echo _("Message Body")?></th>
		<th class="col-sm-1"><?php echo _("Unit Cost")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$history_info=$this->sms_model->get_campaign_history_byargs($user_id,$campaign_code);
		$h = 1 
		?>
		<?php if ($history_info!=0): foreach ($history_info as $history) : ?>

		<tr>
		<td>
		 <?php echo $history->campaign_id?>
		</td>
		<td>
		 <?php echo "#".$history->campaign_code?>
		</td>
		<td>
		  <?php echo $history->from_number?>
		</td>
		<td>
		  <?php echo $history->to_number.'<br>'.'<small class="text-muted">'.$history->group_name.'</small>' ?>
		 </td>
		<td>
		  <?php echo $history->message ?>
		  <?php 
		   if($history->is_scheduled==1){
			   echo '<hr><small><i class="fa fa-calendar"></i> <b> ' . _('Scheduled') . ' : </b><br>'.date('d-M-Y H:i:s',strtotime($history->scheduled_datetime)).'</small>';
		   }
		  ?>
			 
		</td>
		<td>
		  <?php echo "<small class='text-muted'>".$currencyInfo[0]->currency_name."</small> ".$history->unit_cost ?>  
		 
		</td>
	
		<td>
		   
		   <?php 
		    switch($history->deliver_status){
				case '3':
				echo '<span class="label label-primary"><i class="fa  fa-hourglass-end"></i> ' . _('Queued') . '</span>';
				break;
				
				case '2':
				echo '<span class="label label-warning"><i class="fa  fa-hourglass-half"></i> ' . _('Pending') . '</span>';
				break;
				
				case '1':
				echo '<span class="label label-success"><i class="fa  fa-check"></i> ' . _('Delivered') . '</span><br>';
				echo '<small>'.$history->do_sent.'</small>';
				break;
				
				case '0':
				echo '<span class="label label-danger">	<i class="fa  fa-times"></i> ' . _('Failed') . '</span><br>';
				?>
				<a href="javascript:void(0)" onclick="backendSmsRemarks('<?php echo $history->campaign_id?>','<?php echo $history->user_id?>')"><small><?php echo _("Remarks")?></small></a>
				<?php
				break;
			}
		   ?>
		   
		</td>
		<td>
		<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delBackendSmsHistory('<?php echo $history->campaign_id?>','<?php echo $history->to_number?>','<?php echo $history->user_id?>')" class="text-danger">
			<i class='glyphicon glyphicon-trash'></i> 
		</a>
		</td>

		</tr>
		<?php
		$h++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="7" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody>
 </table> 
	<?php 
	   }
	 ?>
	   
	   
		
	</div>
 </div>	
	
<?php 
$this->load->view('site/modals/campaign/sms/delete_sms_history');
$this->load->view('site/campaign/sms_remarks');
?>
</section>
		
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-SmsHistory').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>
		
