
<?php
$user_id = base64_decode($this->input->get('id'));
$userInfo= $this->account_model->list_current_user($user_id);
$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);
 
$paymentSettings=$this->app_settings_model->get_payment_settings();
$settingsInfo= $this->app_settings_model->get_site_settings();

$paymentInfo= $this->payment_model->get_member_payments($user_id);
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
?>
 
<script type="text/javascript">
 function printInvoice(pid,uid){
    var URL = "<?php echo base_url()?>export/payment_invoice/"+pid+"/"+uid;
    var W = window.open(URL);   
    W.window.print(); 
 }
</script>
 
	<table class="table table-bordered table-striped" id="dataTables-paymentHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Ref.No")?></th>
		<th class="col-sm-1"><?php echo _("Amount")?></th>
		<th class="col-sm-1"><?php echo _("Transaction ID")?></th>
		<th class="col-sm-1"><?php echo _("Response Code")?></th>
		<th class="col-sm-1"><?php echo _("Payment Datetime")?></th>
		<th class="col-sm-1"><?php echo _("Payment Status")?></th>
		<th class="col-sm-1"><?php echo _("Invoice")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$gp = 1 
		?>
		<?php if ($paymentInfo!=0): foreach ($paymentInfo as $payment) : ?>

		<tr>
		<td>
		<?php echo $payment->acc_payment_id?>
		</td>
		
		<td>
		<?php echo $payment->acc_item_number?>
		</td>
		<td>
		<?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.$payment->c2o_total_amount?> 
		</td>
		
		<td>
		  <?php echo $payment->c2o_transaction_id ?>
		</td>
		<td>
		  <?php echo $payment->c2o_response_code ?>
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($payment->payment_datetime)); 
		?>
		</td>
		
		<td>
		     <?php 
			  switch($payment->payment_status){
				  case '1':
				  ?>
				  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i><?php echo _("Accepted")?></div>
				  <?php
				  break;
				  
				  case '0':
				  ?>
				  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i><?php echo _("Failed")?></div>
				  <?php
				  break;
			  }
			 ?>
             
         </td>
		 <td>
		     <?php 
			  switch($payment->payment_status){
				  case '1':
				  ?>
				  <a href="<?php echo base_url().'export/payment_invoice/'.str_replace('=','',base64_encode($payment->acc_payment_id)).'p/'.base64_encode($payment->user_id)?>" title="<?php echo _('Download Invoice') ?>" class="btn btn-xs btn-info">
				  <i class="fa fa-download"></i>
				  </a>
				  
				  <a href="javascript:void(0)" onclick="javascript:printInvoice('<?php echo str_replace('=','',base64_encode($payment->acc_payment_id))."p"?>','<?php echo base64_encode($payment->user_id)?>')" title="<?php echo _('Print Invoice') ?>" class="btn btn-xs btn-default">
				  <i class="fa fa-print"></i>
				  </a>
				  
				  <?php
				  break;
				  
				  case '0':
				  echo '-';
				  break;
			  }
			 ?>
             
         </td>
		</tr>
	<?php
	$gp++;
	endforeach;
	?>
	<?php else : ?>
	<td colspan="6" class="text-center">
	<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
	</td>
	<?php endif; ?>
	</tbody><!-- / Table body -->
	</table> <!-- / Table -->
