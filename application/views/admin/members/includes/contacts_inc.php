
		<table class="table table-bordered table-striped" id="dataTables-contacts">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Name")?></th>
		<th class="col-sm-1"><?php echo _("Country")?></th>
		<th class="col-sm-1"><?php echo _("MobileNo")?></th>
		<th class="col-sm-1"><?php echo _("Campaign Group")?></th>
		<th class="col-sm-1"><?php echo _("Action")?></th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$userId=base64_decode($this->input->get('id'));
		$all_contacts_info=$this->sms_model->get_contacts_byuser($userId);
		$key = 1 

		?>
		<?php if ($all_contacts_info!=0): foreach ($all_contacts_info as $contact) : ?>

		<tr>
		<td><?php echo $contact->c_id ?></td>
		<td><?php echo $contact->contact_name ?></td>
		<td><?php echo $contact->country_name ?></td>
		<td><?php echo $contact->contact_mobile?></td>
		<td><?php echo $contact->group_name?></td>
		<td>


		<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delSMSContact('<?php echo $contact->c_id?>','<?php echo $contact->user_id?>')" class="text-danger">
		<i class='glyphicon glyphicon-trash'></i> 
		</a>
		</td>

		</tr>
		<?php
		$key++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
			
<?php 
	$this->load->view('site/modals/campaign/sms/delete_contact');
?>
	
			
