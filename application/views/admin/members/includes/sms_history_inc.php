

<?php 
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
$user_id=base64_decode($this->input->get('id'));
$history_info=$this->sms_model->get_campaign_history($user_id);

?>


  <table class="table table-bordered table-striped" id="dataTables-SmsHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("C.Code")?></th>
		<th class="col-sm-1"><?php echo _("From")?></th>
		<th class="col-sm-1"><?php echo _("To")?></th>
		<th class="col-sm-2"><?php echo _("Message Body")?></th>
		<th class="col-sm-1"><?php echo _("Unit Cost")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$h = 1 
		?>
		<?php if ($history_info!=0): foreach ($history_info as $history) : ?>

		<tr>
		<td>
		 <?php echo $history->campaign_id?>
		</td>
		<td>
		 <?php echo $history->campaign_code?>
		</td>
		<td>
		  <?php echo $history->from_number?>
		</td>
		<td>
		  <?php echo $history->to_number.'<br>'.'<small class="text-muted">'.$history->group_name.'</small>' ?>
		 </td>
		<td>
		  <?php echo $history->message ?>
		  <?php 
		   if($history->is_scheduled==1){
			   echo '<hr><small><i class="fa fa-calendar"></i> <b> <?php echo _("Scheduled ")?>: </b><br>'.date('d-M-Y H:i:s',strtotime($history->scheduled_datetime)).'</small>';
		   }
		  ?>
			 
		</td>
		<td>
		  <?php echo "<small class='text-muted'>".$currencyInfo[0]->currency_name."</small> ".$history->unit_cost ?>  
		 
		</td>
	
		<td>
		   
		   <?php 
		    switch($history->deliver_status){
				case '3':
				echo '<span class="label label-primary"><i class="fa  fa-hourglass-end"></i> ' . _('Queued') . '</span>';
				break;
				
				case '2':
				echo '<span class="label label-warning"><i class="fa  fa-hourglass-half"></i> ' . _('Pending') . '</span>';
				break;
				
				case '1':
				echo '<span class="label label-success"><i class="fa  fa-check"></i> ' . _('Delivered') . '</span><br>';
				echo '<small>'.$history->do_sent.'</small>';
				break;
				
				case '0':
				echo '<span class="label label-danger">	<i class="fa  fa-times"></i> ' . _('Failed') . '</span><br>';
				?>
				<a href="javascript:void(0)" onclick="failRemarksBackend('<?php echo $history->campaign_id?>','<?php echo $history->user_id?>')"><small><?php echo _("Remarks")?></small></a>
				<?php
				break;
			}
		   ?>
		   
		</td>
		<td>
		 <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delSmsHistory('<?php echo $history->campaign_id?>','<?php echo $history->to_number?>','<?php echo $history->user_id?>')" class="text-danger">
			<i class='glyphicon glyphicon-trash'></i> 
			</a>
		</td>

		</tr>
		<?php
		$h++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="7" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
</table> <!-- / Table -->


<?php 
$this->load->view('site/modals/campaign/sms/delete_sms_history');
$this->load->view('site/campaign/sms_remarks');
?>
