
<?php
$user_id = base64_decode($this->input->get('id'));
$wallet_history = $this->wallet_model->ad_payments_bywallet($user_id);

$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
?>

<script type="text/javascript">
 function printInvoice(pid,uid){
    var URL = "<?php echo base_url()?>export/invoice/"+pid+"/"+uid;
    var W = window.open(URL);   
    W.window.print(); 
 }
</script>

  <table class="table table-bordered table-striped" id="dataTables-walletHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Ref.No")?></th>
		<th class="col-sm-1"><?php echo _("Payment")?></th>
		<th class="col-sm-1"><?php echo _("Date Of Payment")?></th>
		<th class="col-sm-1"><?php echo _("Payment/Invoice Ref.No")?></th>
		<th class="col-sm-2"><?php echo _("Remarks")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Invoice")?></th>
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$gp = 1 
		?>
		<?php if ($wallet_history!=0): foreach ($wallet_history as $wallet) : ?>

		<tr>
		<td>
		<?php echo $wallet->payment_id?>
		</td>
		<td>
		<?php echo $wallet->ad_reference?>
		</td>
		<td>
		<?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.$wallet->ad_payment?> 
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($wallet->date_of_payment)); 
		?>
		</td>
		<td>
		   <?php echo $wallet->payment_reference ?>
		</td>
		<td>
		<?php 
		  echo $wallet->payment_remarks
		?>
		</td>
		<td>
		     <?php 
			  switch($wallet->payment_status){
				  case '1':
				  ?>
				  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i><?php echo _("PAID")?></div>
				  <?php
				  break;
				  
				  case '0':
				  ?>
				  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i><?php echo _("Failed")?></div>
				  <?php
				  break;
			  }
			 ?>
             
         </td>
		 <td>
		     <?php 
			  switch($wallet->payment_status){
				  case '1':
				  ?>
				  <a href="<?php echo base_url().'export/invoice/'.str_replace('=','',base64_encode($wallet->payment_id)).'w/'.base64_encode($wallet->user_id)?>" title="<?php echo _('Download Invoice') ?>" class="btn btn-xs btn-info">
				  <i class="fa fa-download"></i>
				  </a>
				  
				  <a href="javascript:void(0)" onclick="javascript:printInvoice('<?php echo str_replace('=','',base64_encode($wallet->payment_id))."w"?>','<?php echo base64_encode($wallet->user_id)?>')" title="<?php echo _('Print Invoice') ?>" class="btn btn-xs btn-default">
				  <i class="fa fa-print"></i>
				  </a>
				  <?php
				  break;
				  
				  case '0':
				  echo '-';
				  break;
			  }
			 ?>
             
         </td>
		</tr>
	<?php
	$gp++;
	endforeach;
	?>
	<?php else : ?>
	<td colspan="6" class="text-center">
	<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
	</td>
	<?php endif; ?>
	</tbody><!-- / Table body -->
	</table> <!-- / Table -->
				
	


