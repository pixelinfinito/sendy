 
  <table class="table table-bordered table-striped" id="dataTables-callerNumbers">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("Country")?></th>
		<th class="col-sm-1"><?php echo _("Number")?></th>
		<th class="col-sm-1"><?php echo _("SenderID")?></th>
		<th class="col-sm-1"><?php echo _("Lease Period")?></th>
		<th class="col-sm-1"><?php echo _("Price")?></th>
		<th class="col-sm-1"><?php echo _("Purchase On")?></th>
		<th class="col-sm-1"><?php echo _("Expires In")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		
		<tbody>
		<?php 
		$appSettings= $this->app_settings_model->get_primary_settings();
		$user_id=base64_decode($this->input->get('id'));
		$caller_numbers=$this->sms_model->get_caller_numbers($user_id);
		$cnp = 1 
		?>
		<?php if ($caller_numbers!=0): foreach ($caller_numbers as $cn) : ?>

		<tr>
		<td>
		  <strong></strong> <?php echo $cn->country?>
		</td>
		<td>
		<?php 
		
		 if($cn->twilio_origin_number!=''){
			 ?>
			 <a href="#" class="text-success"><?php echo $cn->twilio_origin_number?></a><br>
			
			 <?php
		 }else{
		?>
			<a href="#" class="text-warning"><i class="fa fa-hourglass-1"></i><?php echo _("In Process")?></a><br>
			
		 <?php 
		 }
		 ?>
		</td>
		<td>
		<?php 
		   if($cn->twilio_sender_id!=''){
			 
                if($cn->sender_id_status!=2){
					?>
					 <div class="ad_publish_btn expwi50">
					 <a href="javascript:void(0)"><i class="fa fa-check"></i> <?php echo $cn->twilio_sender_id?></a> 
					 </div>
					<?php
					
				}else{
					?>
					 <div class="ad_unpublish_btn expwi50">
					 <a href="javascript:void(0)"><i class="fa fa-hourglass-1"></i><?php echo _("In Process")?></a>
					 </div>
					<?php
				}			 
		   } else{
			   ?>
			    
					<a href="javascript:void(0)" onclick="triggerModalsenderId('<?php echo $cn->tid?>')">
					<i class="fa fa-hand-stop-o"></i><?php echo _("No Request")?></a>
				
			   <?php
			   
		   }
		?>
		</td>
   
        <td>
		<?php echo $cn->subscription_period.' <small> ' . _('Month(s)') . '</small>'?>
		</td>
		
		 <td>
		<?php echo $appSettings[0]->app_currency.' '.$cn->price?>
		</td>
		
		 <td>
		<?php echo date('d-M-Y H:i:s',strtotime($cn->purchased_on))?>
		</td>
		
		<td>
		<?php
		date_default_timezone_set($appSettings[0]->app_timezone);
		$expires_in = strtotime($cn->expired_on) - strtotime($cn->purchased_on);
		$today=date('Y-m-d H:i:s');
		if(strtotime($cn->expired_on) > strtotime($today))
		{
			$number_expires= floor($expires_in/3600/24);
			echo "
			<span class='text-success'>
			<i class='fa fa-clock-o'></i>   
			<strong>". $number_expires." </strong> <small>" . _('Day(s)') . "</small>
			</span>";
		}
		else
		{
			echo "<span class='text-danger'>
					<i class='fa fa-exclamation-triangle'></i> " . _('Expired') . "
				  </span><br>";
				  ?>
				  <a href="javascript:alert('<?php echo _('Sorry this feature is currently disabled.')?>')"><u><small><?php echo _("Extend ?")?></small></u></a>
				  <?php
		}
		?>
	   </td>
		
		<td>
			
		   <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delCallerNumber('<?php echo $cn->tid?>','<?php echo $cn->user_id?>')" class="text-danger">
				<i class='glyphicon glyphicon-trash'></i> 
			</a>
		
		</td>

		</tr>
		<?php
		$cnp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="9" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
		
	
<?php 
  
  $this->load->view('site/modals/campaign/sms/delete_caller_number');
  //$this->load->view('site/campaign/sender_id');

?>


