
<table class="table table-bordered table-striped" id="dataTables-sCampaigns">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1 "><?php echo _("ID")?></th>
		<th class="col-sm-1 "><?php echo _("Campaign Code")?></th>
		<th class="col-sm-1 "><?php echo _("Message")?></th>
		<th class="col-sm-1 "><?php echo _("Sent On")?></th>
		<th class="col-sm-1 "><?php echo _("Summary")?></th>
		<th class="col-sm-1 "><?php echo _("Type")?></th>
		<th class="col-sm-1 "><?php echo _("Status")?></th>
		<th class="col-sm-1 "><?php echo _("Action")?></th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$user_id=base64_decode($this->input->get('id'));
		$campaigns_info=$this->sms_model->get_campaign_overview($user_id);
		$cmp = 1 
		?>
		<?php if ($campaigns_info!=0): foreach ($campaigns_info as $campaigns) : ?>

		<tr>
		<td>
		<?php echo $campaigns->campaign_id?>
		</td>
		<td><?php echo "#".$campaigns->campaign_code?>
		<br>
		<small class="text-muted"><b><?php echo _("Submit On")?></b><br>
		 <?php echo date('d-M-Y H:i:s',strtotime($campaigns->do_submit)); ?>
		</small>
		
		</td>
		<td><?php echo $campaigns->message ?></td>
		
		<td>
		 <?php echo date('d-M-Y H:i:s',strtotime($campaigns->do_sent)); ?>
		</td>
		<td width="25%">
		    <?php 
			  $success_count=$this->sms_model->get_success_count($campaigns->campaign_code,$user_id);
			  $participated_count=$this->sms_model->get_campaign_contacts_count($campaigns->campaign_code,$user_id);
			?>
			  
			  <div class="row text-left">
			  <div class="col-md-4">
			    <div class="ad_free_btn"><?php echo _("Total")?>  <?php echo "(".$participated_count.")";?></div>
			  </div>
			  <div class="col-md-2">
			   &nbsp;
			  </div>
			  <div class="col-md-6">
			     <?php 
				  if($success_count!=0){
					  $failed=$participated_count-$success_count;
				  }else{
					  $failed="0";
				  }
				  
				  ?>
				  <?php echo "<span class='text-success'><i class='fa fa-check-circle'></i> ".$success_count." </span>&nbsp;";?>
				  <?php echo "<span class='text-danger'><i class='fa fa-exclamation-circle'></i> ".$failed." </span>";?>
			  </span>
			  </div>
			  </div>
			
			 
		</td>
		<td>
		 <?php 
		   switch($campaigns->is_scheduled){
			   case '0':
			   echo "<span class='text-primary'><i class='fa fa-leaf'></i> </span>";
			   break;
			   
			   case '1':
			   echo "<span class='text-warning'><i class='fa  fa-clock-o'></i> </span>";
			   break;
		   }
		  ?>
		</td>
		<td>
		<?php 
		   switch($campaigns->status){
			   case '1':
			   echo "<span class='label label-success'><i class='fa fa-check'></i> " . _("Completed") . "</span>";
			   break;
			   
			   case '0':
			   echo "<span class='label label-warning'><i class='fa fa-hourglass-half'></i> " . _("Waiting") . "</span>";
			   break;
		   }
		  ?>
		</td>
		<td>
			<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delCampaigns('<?php echo $campaigns->campaign_code?>','<?php echo $campaigns->user_id?>')" class="text-danger">
			<i class='glyphicon glyphicon-trash'></i> 
			</a>
		</td>

		</tr>
		<?php
		$cmp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="7" class="text-center">
		   <h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
					
		<?php 
		$this->load->view('site/modals/campaign/sms/delete_campaign');
		
		?>
		
