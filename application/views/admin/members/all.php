<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<?php 
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
?>
<script type="application/javascript" src="<?php echo base_url()?>js/members/all.js"></script>
<section class="content-header">
  <h1><?php echo _("Members")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Members")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_members" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>members/add" class="btn btn-sm btn-primary"  title="<?php echo _('New Member') ?>">
		 <i class="fa fa-plus-circle"></i><?php echo _("New Member")?></a>
   </div>
</div> 
</div>
<div class="box-body">
  <table class="table table-bordered table-striped" id="dataTables-memberList">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("Id")?></th>
		<th class="col-sm-1"><?php echo _("Thumbnail")?></th>
		<th class="col-sm-2"><?php echo _("Name")?></th>
		<th class="col-sm-1"><?php echo _("Country")?></th>
		<th class="col-sm-1"><?php echo _("Available Funds")?></th>
		<th class="col-sm-1"><?php echo _("Total Campaigns")?></th>
		<th class="col-sm-1"><?php echo _("Activation Status")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$member_info=$this->account_model->list_all_members();
		$gp = 1 
		?>
		<?php if ($member_info!=0): foreach ($member_info as $member) : ?>

		<tr>
		<td>
		<?php 
		echo $member->id;
		switch($member->is_blocked){
			  case 1:
			  echo '<br><span class="text-danger"><i class="fa fa-ban"></i><?php echo _("blacklisted")?></span>';
			  break;
			 
		  }
		?>
		</td>
		
		<td>
		<?php 
		if($member->ac_thumbnail!=''){
		?>
		<img src="<?php echo base_url().$member->ac_thumbnail?>" alt="Thumbnail" width="50" height="50" class="img-circle">
		
		<?php 
		}else{
			if($member->ac_gender!=1){
				?>
				<img src="<?php echo base_url().'theme4.0/admin/images/avatar2.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">
		
				<?php
			}else{
				?>
				<img src="<?php echo base_url().'theme4.0/admin/images/avatar5.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">
		
				<?php
			}
		}
		?>
		</td>
		
		<td>
		<?php echo $member->ac_first_name.' '.$member->ac_last_name?>
		<br><small class="text-muted"><?php echo _("Joined On")?> <?php echo $member->ac_join_date?></small>
		</td>
		
		<td>
		   <?php echo $member->country_name ?> 
			 
		</td>
		<td>
		 <?php 
		    $walletInfo= $this->account_model->get_account_wallet($member->user_id);
			if(@$walletInfo[0]->balance_amount!=''){
			  echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.@$walletInfo[0]->balance_amount;
			}else{
				echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name." 00.00";
			}
			
			$free_credits_req=$this->wallet_model->check_freecredit_request_byid($member->user_id);
			if($free_credits_req!=0){
				if($free_credits_req[0]->status=='1'){
					?>
					<br>
					<span class="text-success" title="<?php echo _('Already used free credits') ?>"><i class="fa  fa-money"></i></span>
					<?php
				}else{
					?>
					<br>
					<span class="text-danger" title="<?php echo _('Free credits request in pending') ?>"><i class="fa  fa-money"></i></span>
					<?php
				}
			}
		 ?>
		</td>
		<td>
		 <?php 
		  $campaigns=$this->sms_model->get_campaign_history($member->user_id);
		  if($campaigns!=0){
			  echo count($campaigns);
		  }else{
			  echo "0";
		  }
		  ?>
		</td>
		
		<td>
		<?php 
		
		  switch($member->is_validated){
			  case 1:
			  echo '<span class="text-success"><i class="fa fa-check-circle"></i></span>';
			  break;
			  
			  case 0:
			  echo '<span class="text-danger"><i class="fa fa-hourglass-half"></i></span>';
			  break;
		  }
		?>
		</td>
		
		<td>
		<?php 
		 
		  switch($member->status){
			  case 1:
			  ?>
			  <div id="active_placeholder<?php echo $member->id?>">
			  <span class="label label-success" id="active_label<?php echo $member->id?>"><?php echo _("Active")?></span><br>
			  <a href="javascript:void(0)" onclick="javascript:inactive_member('<?php echo $member->user_id?>','<?php echo $member->id?>')" class="text-danger" title="<?php echo _('Change to Inactive') ?>">
			  <small><u><?php echo _("Inactive")?></u></small>
			  </a>
			  </div>
			  <?php
			  break;
			  
			  case 0:
			  ?>
			  <div id="inactive_placeholder<?php echo $member->id?>">
			  <span class="label label-warning" id="inactive_label<?php echo $member->id?>"><?php echo _("Inactive")?></span><br>
			  <a href="javascript:void(0)" onclick="javascript:active_member('<?php echo $member->user_id?>','<?php echo $member->id?>')" class="text-success" title="<?php echo _('Change to Active') ?>">
			  <small><u><?php echo _("Activate")?></u></small>
			  </a>
			  </div>
			  <?php
			  break;
		  }
		  
		?>
		</td>
		
		<td>
		<a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member->user_id)?>" title="<?php echo _('More Details') ?>" class='btn btn-xs btn-info'>
            <i class="fa fa-laptop"></i> 
        </a>
		
		<?php 
		 if($member->created_by!=0){
		?>
		<a href="<?php echo base_url()?>members/edit?id=<?php echo base64_encode($member->user_id)?>" title="<?php echo _('Edit') ?>" class='btn btn-xs btn-default'>
            <i class="fa fa-pencil"></i> 
        </a>
		<?php 
		 }
		?>
	     
		<?php 
		 if($member->status!=0 && $member->user_id!='1000578dd5df29bdd'){
		?>		
		 <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delMember('<?php echo $member->id?>','<?php echo $member->user_id?>','<?php echo $member->ac_first_name.' '.$member->ac_last_name?>')"  class='btn btn-xs btn-danger'>
			<i class='glyphicon glyphicon-trash'></i> 
		 </a>
		 <?php 
		 }
		 ?>
		 
		</td>
		</tr>
		<?php
		$gp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody>
		</table>
	</div>
 </div>	
	<?php 
	 $this->load->view('admin/members/modals/confirm_status');
	 $this->load->view('admin/members/modals/delete_member');
	?>
		
</section>
		
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-memberList').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>
		
