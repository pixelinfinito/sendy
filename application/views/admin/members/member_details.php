<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<script type="application/javascript" src="<?php echo base_url()?>js/sms_campaign/sms.js"></script>
<script type="application/javascript" src="<?php echo base_url()?>js/sms_campaign/member_details.js"></script>

<section class="content-header">
  <h1><?php echo _("Member Details")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>members"><i class="fa fa-suitcase"></i><?php echo _("Members")?></a></li>
	<li class="active"><?php echo _("Details")?></li>
  </ol>
</section>

<section class="content">
<?php 
 $member_id=base64_decode($this->input->get('id'));
 
 $member=$this->account_model->get_member_details($member_id);
 $member_thumb=$this->account_model->get_profile_thumbnail($member_id);
 
 $walletInfo= $this->account_model->get_account_wallet($member_id);
 $appSettings= $this->app_settings_model->get_primary_settings();
 $appCountry=$appSettings[0]->app_country;
 $currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
 
 $campaigns=$this->sms_model->get_campaign_overview($member_id);
 $caller_numbers=$this->sms_model->get_caller_numbers($member_id);
 $contacts_info=$this->sms_model->get_contacts_byuser($member_id);

 if($member_id!='' && $member!=0){
?> 
 <div class="row">
  <div class="col-md-3">
  <a href="#" class="btn btn-primary btn-block margin-bottom">
	<?php 
	if(@$walletInfo[0]->balance_amount!=''){
	  echo "Available funds <b>".@$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.@$walletInfo[0]->balance_amount."</b>";
	}else{
		echo "Available funds <b>".@$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name." 00.00</b>";
	}
	?>
  </a>
  
  
  <div class="box box-solid">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo "#".$member[0]->user_id?></h3>
	  <div class="box-tools">
		<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	  </div>
	</div>
	<div class="box-body">
	<div class="text-center">
	<?php 
		if($member_thumb[0]->ac_thumbnail!=''){
		?>
		<img src="<?php echo base_url().$member_thumb[0]->ac_thumbnail?>" alt="Thumbnail"  class="img-circle" width="100" height="100">
		
		<?php 
		}else{
			if($member[0]->ac_gender!=1){
				?>
				<img src="<?php echo base_url().'themeAdmin/dist/img/avatar2.png'?>" alt="Thumbnail"  class="img-circle" width="100" height="100">
		
				<?php
			}else{
				?>
				<img src="<?php echo base_url().'themeAdmin/dist/img/avatar5.png'?>" alt="Thumbnail" class="img-circle" width="100" height="100">
		
				<?php
			}
		}
		?>
		
		<br><?php echo "<small><b class='text-primary'>" . _('Member Since') . " </b>".date('d-M-Y H:i:s',strtotime($member[0]->ac_join_date))."</small>" ?>
		<br><?php echo "<small><b class='text-warning'>" . _('Last Login') . " </b>".date('d-M-Y H:i:s',strtotime($member[0]->last_login))."</small>" ?>	<hr>
		<div class="text-left">
		<?php echo "<i class='fa fa-envelope-o'></i>&nbsp;&nbsp;".$member[0]->ac_email?><br>
		<?php echo "<i class='fa fa-phone-square'></i>&nbsp;&nbsp;".$member[0]->ac_phone?><br>		
		<?php echo "<i class='fa fa-globe'></i>&nbsp;&nbsp;".$member[0]->country_name?><br>	
        <?php 
	   switch($member[0]->is_blocked){
			  case 1:
			  echo '<span class="text-danger"><i class="fa fa-ban"></i> <b>' . _('Blacklisted Member') . '</b></span>';
			  break;
			 
		  }
		?>		
		</div>
	</div>
     
	</div><!-- /.box-body -->
  </div><!-- /.box -->
  
  <div class="box box-solid">
	<div class="box-header with-border">
	  <h3 class="box-title"><?php echo _("Actions")?></h3>
	  <div class="box-tools">
		<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	  </div>
	</div>
	<div class="box-body no-padding">
	  <?php 
	   $tkn=base64_decode($this->input->get('tkn'));
	  ?>
	  <ul class="nav nav-pills nav-stacked">
		<li class="<?php if($tkn=='cmp'){echo 'active';}?>">
		<a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member_id)?>&tkn=<?php echo base64_encode('cmp')?>">
		<i class="fa fa-send-o"></i> <?php echo _("Campaigns")?>
		<span class="badge bg-aqua pull-right">
		<?php 
		  if($campaigns!=0){
			  if(count($campaigns)<10){
				echo "0".count($campaigns);
			  }else{
				echo count($campaigns);
			  }
		  }else{
			  echo "0";
		  }
		  ?>
		</span>
		</a>
		</li>
		
		<li class="<?php if($tkn=='cn'){echo 'active';}?>">
		<a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member_id)?>&tkn=<?php echo base64_encode('cn')?>">
		<i class="fa fa-phone"></i> <?php echo _("Caller Numbers")?>
		<span class="badge bg-green pull-right">
		<?php 
		  if($caller_numbers!=0){
			  if(count($caller_numbers)<10){
				echo "0".count($caller_numbers);
			  }else{
				echo count($caller_numbers);
			  }
		  }else{
			  echo "0";
		  }
		  ?>
		</span>
		</a>
		</li>
		
		<li class="<?php if($tkn=='cts'){echo 'active';}?>">
		<a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member_id)?>&tkn=<?php echo base64_encode('cts')?>">
		<i class="fa fa-user"></i> <?php echo _("Contacts")?>
		<span class="badge bg-yellow pull-right">
		<?php 
		  if($contacts_info!=0){
			  if(count($contacts_info)<10){
				echo "0".count($contacts_info);
			  }else{
				echo count($contacts_info);
			  }
		  }else{
			  echo "0";
		  }
		  ?>
		</span>
		</a>
		</li>
		
		<li class="<?php if($tkn=='phist'){echo 'active';}?>">
		<a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member_id)?>&tkn=<?php echo base64_encode('phist')?>">
		<i class="fa fa-dollar"></i><?php echo _("Payment Transactions")?></a>
		</li>
		
		<li class="<?php if($tkn=='whist'){echo 'active';}?>">
		<a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member_id)?>&tkn=<?php echo base64_encode('whist')?>">
		<i class="fa fa-google-wallet"></i><?php echo _("Wallet Transactions")?></a>
		</li>
		
		<li><a href="#"><i class="fa fa-exchange"></i><?php echo _("Fund Transfers")?></a></li>
		<li class="<?php if($tkn=='smhist'){echo 'active';}?>">
		<a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member_id)?>&tkn=<?php echo base64_encode('smhist')?>">
		<i class="fa fa-history"></i><?php echo _("SMS History")?></a>
		</li>
	  </ul>
	</div><!-- /.box-body -->
  </div><!-- /. box -->
  
 <div class="bg-maroon">
  <?php 
  $free_credits_req=$this->wallet_model->check_freecredit_request_byid($member_id);
	if($free_credits_req!=0){
		if($free_credits_req[0]->status=='1'){
			?>
			<div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-teal">
                  <div class="widget-user">
                    <h4 title="<?php echo _('Already used free credits') ?>"><i class="fa fa-money"></i> <?php echo _("Used Free Credits")?> <?php echo $currencyInfo[0]->currency_name.' '.round($free_credits_req[0]->approved_funds,2)?></h4>
                  </div><!-- /.widget-user-image -->
			</div>
			</div>
			<?php
		}else{
			?>
			<div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-maroon">
                  <div class="widget-user">
                    <h4 title="<?php echo _('Free credits request in pending') ?>"> <i class="fa fa-money"></i><?php echo _("Free Credits Pending")?></h4>
                  </div><!-- /.widget-user-image -->
			</div>
			</div>
			<?php
		}
	}
  ?>
 </div> 
 <div class="padding-lft20">
  <span class="text-success"><i class="fa fa-check-circle"></i> = <?php echo _("Success")?></span><br>
  <span class="text-danger"><i class="fa fa-exclamation-circle"></i> = <?php echo _("Failed")?></span><br>
  <span class="text-warning"><i class="fa  fa-clock-o"></i> = <?php echo _("Scheduled")?></span><br>
  <span class="text-primary"><i class="fa fa-leaf"></i> = <?php echo _("Regular")?></span><br>
  <span class="text-danger"><i class="fa fa-ban"></i> = <?php echo _("Blacklisted")?></span><br>
  <span class="text-danger"><i class="fa fa-money"></i> = <?php echo _("Free Credits (Pending")?>)</span><br>
  <span class="text-success"><i class="fa fa-money"></i> = <?php echo _("Free Credits (Used")?>)</span><br>
</div>  
<br><br>
  
</div><!-- /.col -->
<div class="col-md-9">			
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
    <?php 
	 switch(base64_decode($this->input->get('tkn'))){
		
		case 'cmp':
		$export_url=base_url().'export/export_campaigns/'.$member_id;
		break;
		
		case 'cn':
		$export_url=base_url().'export/export_caller_numbers/'.$member_id;
		break;
		
		case 'cts':
		$export_url=base_url().'export/export_contacts/'.$member_id;
		break;
		
		case 'phist':
		$export_url=base_url().'export/export_payment_history/'.$member_id;
		break;
		
		case 'whist':
		$export_url=base_url().'export/export_wallet_history/'.$member_id;
		break;
		
		case 'smhist':
		$export_url=base_url().'export/export_history/'.$member_id;
		break;
		
		default:
		$export_url=base_url().'export/export_campaigns/'.$member_id;
	 }
	?>
    <a href="<?php echo $export_url?>" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>members/add" class="btn btn-sm btn-info"  title="<?php echo _('New Member') ?>">
		 <i class="fa fa-plus-circle"></i><?php echo _("New Member")?></a>
   </div>
</div> 
</div>
<div class="box-body">
   <?php 
    switch(base64_decode($this->input->get('tkn'))){
		
		case 'cmp':
		$this->load->view('admin/members/includes/campaigns_inc'); 
		break;
		
		case 'cn':
		$this->load->view('admin/members/includes/caller_numbers_inc'); 
		break;
		
		case 'cts':
		$this->load->view('admin/members/includes/contacts_inc'); 
		break;
		
		case 'phist':
		$this->load->view('admin/members/includes/payments_history_inc'); 
		break;
		
		case 'whist':
		$this->load->view('admin/members/includes/wallet_history_inc'); 
		break;
		
		case 'smhist':
		$this->load->view('admin/members/includes/sms_history_inc'); 
		break;
		
		default:
		$this->load->view('admin/members/includes/campaigns_inc'); 
	}
   ?>
</div>
 </div>	
 </div>
	<?php 
	 //$this->load->view('modals/delete_user');
	?>
	</div>	
	<?php 
	}else{
		 echo '<div class="box col-sm-12 text-center">
		<h2 class="text-danger"><i class="fa  fa-exclamation-circle"></i><?php echo _("Unauthorized request")?></h2>
		Sorry we don\'t allow unauthorized actions<br><br>
		</div>';
	}
 ?>
</section>
		
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-sCampaigns').DataTable({
			"order": [[ 0, "desc" ]]
		});
		$('#dataTables-callerNumbers').DataTable({
			"order": [[ 0, "desc" ]]
		});
		$('#dataTables-contacts').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		$('#dataTables-paymentHistory').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		$('#dataTables-walletHistory').DataTable( {
			"order": [[ 0, "desc" ]]
		});
		
		$('#dataTables-SmsHistory').DataTable( {
			"order": [[ 0, "desc" ]]
		});
		
		
		
    });
</script>
		
