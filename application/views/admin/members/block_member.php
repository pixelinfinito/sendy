<?php 
$this->load->view('theme/header.php');
$this->load->view('theme/sidebar.php');

$account_classes= $this->account_model->list_account_classes();
$countries= $this->countries_model->list_countries();
$total_members=$this->account_model->list_all_members();
$inactive_members=$this->account_model->list_inactive_members();
$list_latest_members=$this->account_model->list_latest_members();
?>

<script type="application/javascript" src="<?php echo base_url()?>js/members/forms.js"></script>
<section class="content-header">
  <h1><?php echo _("Blacklist/Whitelist Member")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>members"><?php echo _("Members")?></a></li> <li class="active"><?php echo _("Blacklist/Whitelist Member")?></li>
  </ol>
</section>

 <section class="content">
 	   <div class="row">
	   <div class="col-md-8">
	   <div class="box padding_20">
         <div class="box-body">
	      <div ng-app="">
                 <form role="form" ng-controller="blkController" name="blkMemberForm" id="blkMemberForm">
                    <div class="form-group">
                    <label><?php echo _("Member")?></label> <span class="red">*</span>
				
					<select class="form-control"  id="member_id" required="required" name="member_id">
					 <option value="">--<?php echo _("Select")?>--</option>
					 <?php 
					  foreach($total_members as $member){
						  ?>
						  <option value="<?php echo $member->user_id.'__'.$member->ac_email?>">
						   <?php echo $member->ac_first_name.' '.$member->ac_last_name.' ('.$member->country_name.' )'?>
						  </option>
						  <?php
						  
					  }
					 ?>
					</select>
                    </div>
					
					<div class="form-group">
					 <label><?php echo _("Change Status")?></label>  <span class="red">*</span>
					 <select class="form-control"  id="block_status" required="required" name="block_status">
					 <option value="1"><?php echo _("Blacklist")?></option>
					 <option value="0"><?php echo _("Whitelist")?></option>
					</select>
					</div>
					
                    <div class="form-group">
                    <label><?php echo _("Remarks")?></label> <span class="red">*</span>
                    <textarea placeholder="<?php echo _('Type remarks for blacklist') ?>" class="form-control" required="required" id="remarks" name="remarks" rows="5"></textarea>
					<small class="red">* <?php echo _("Mandatory field(s).")?></small>
                    </div>
					
					<div class="form-group">
					 <input type="checkbox" name="notify_mail" id="notify_mail"><?php echo _("Let member know by email")?></div>
					
                    <div class="form-group">
                         <button class="btn btn-danger pull-left m-t-n-xs" type="button" ng-click='blkMemberIn()' >
						 <i class="fa fa-ban"></i><?php echo _("Submit")?></button>
                    </div>
					<div id="rsDiv"></div>
					<br>
					</form>
			</div>
	    </div>
	   </div>
	   </div>
	   
	    <div class="col-md-4">
		 
		   <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i><?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?> 
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
		</div>
		</div>
		
</section>

<?php $this->load->view('theme/footer.php');?>



