<div class="modal fade" id="deleMemberModal" tabindex="-1" role="dialog" 
     aria-labelledby="delMemberLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title"><?php echo _("Confirm Alert")?></h4>
            </div>
			<div class="modal-body">
			   <div class="row">
			   <div class="col-md-6">
			   <h1><?php echo _("Are you sure ?")?></h1>
			    <?php echo _("You want to delete")?> <b><span id="dlx_member"></span></b> <br>
			    <div id="callMemberdelstatus"></div>
				</div>
				
				<div class="col-md-6">
				<b><?php echo _("Note ")?>:</b>
				<?php echo _("If you delete this member, automatically dependency records 
				(e.g. wallet history,payment history,sms history,campaign history etc.,)also will get delete.")?>
				</div>
				</div>
			 </div>
           
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  <div class="pull-left"> &nbsp; </div>
				</div>
				<div class="col-md-6">
				<button type="button" data-dismiss="modal" class="btn btn-danger" id="delete"><?php echo _("Yes")?></button>
				<button type="button" data-dismiss="modal" class="btn"><?php echo _("No")?></button>
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>