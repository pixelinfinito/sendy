<div class="modal fade" id="cnfStatusModal" tabindex="-1" role="dialog" 
     aria-labelledby="cnfStatusLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title"><?php echo _("Confirm Alert")?></h4>
            </div>
			<div class="modal-body">
			   <h1><?php echo _("Are you sure ?")?></h1>
			    <?php echo _("You want to perform this action")?> <br><br>
			   
			   <b><?php echo _("Would you like send notification to member")?></b>
			   <select class="form-control" id="notify_status" name="notify_status">
			    <option value="">--<?php echo _("By")?>--</option>
				<option value="sms"><?php echo _("SMS")?></option>
				<option value="email"><?php echo _("Email")?></option>
			   </select>
			   <br>
			   
			 </div>
           
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  <div class="pull-left"> <small><i class="fa fa-info-circle"></i><?php echo _("When you choose notify option, 
				  system will send status notification to respective member")?></small> </div>
				</div>
				<div class="col-md-6">
				<button type="button" data-dismiss="modal" class="btn btn-danger" id="notify_btn"><?php echo _("Yes")?></button>
				<button type="button" data-dismiss="modal" class="btn"><?php echo _("No")?></button>
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>