<?php 
$this->load->view('theme/header.php');
$this->load->view('theme/sidebar.php');

$member_id=base64_decode($this->input->get('id'));
$account_classes= $this->account_model->list_account_classes();
$countries= $this->countries_model->list_countries();
$total_members=$this->account_model->list_all_members();
$inactive_members=$this->account_model->list_inactive_members();
$list_latest_members=$this->account_model->list_latest_members();


$member_info=$this->account_model->get_member_details($member_id);


?>

<script type="application/javascript" src="<?php echo base_url()?>js/members/site_register.js"></script>
<section class="content-header">
  <h1><?php echo _("Edit Member")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>members"><?php echo _("Members")?></a></li> <li class="active"><?php echo _("Edit Member")?></li>
  </ol>
</section>

 <section class="content">
  <?php 
  if($member_info!=0){
	  
  ?>
 	   <div class="row">
	   <div class="col-md-8">
	   <div class="box padding_20">
         <div class="box-body">
	      <div ng-app="">
            <form ng-controller="updateMemberController" role="form" name="updateMemberForm" id="updateMemberForm"> 
                  <fieldset>
                    <div class="form-group">
                      <label><?php echo _("Member Class")?> <span class="red">*</span></label>
                        <select class="form-control"  name="ac_class" id="ac_class">
						 <option value="">--<?php echo _("Select option")?>--</option>
						 <?php
						  for($ac=0;$ac<count($account_classes);$ac++){
							  ?>
							  <option value="<?php echo $account_classes[$ac]->acc_id;?>" <?php if($member_info[0]->ac_class==$account_classes[$ac]->acc_id){echo _("selected");}?>>
							  <?php echo $account_classes[$ac]->acc_type;?>
							  </option>
							  <?php
						  }
						 ?>
						</select>
						<small><a href="#"><?php echo _("What is Member class ?")?></a></small>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group">
                      <label><?php echo _("First Name")?> <span class="red">*</span></label>
                        <input  name="ac_first_name" id="ac_first_name" placeholder="<?php echo _('First Name') ?>" class="form-control input-md" required type="text" value="<?php echo $member_info[0]->ac_first_name?>">
						<input name="member_id" id="member_id"  required type="hidden" value="<?php echo $member_info[0]->user_id?>">
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group">
                      <label><?php echo _("Last Name")?> <span class="red">*</span></label>
                        <input  name="ac_last_name" id="ac_last_name" placeholder="<?php echo _('Last Name') ?>" class="form-control input-md" required type="text" value="<?php echo $member_info[0]->ac_last_name?>">
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group">
                      <label><?php echo _("Phone Number")?>  <span class="red">*</span></label>
                        <input  name="ac_phone" id="ac_phone" placeholder="<?php echo _('E.g. +673xxxxxx') ?>" class="form-control input-md" type="text" required="required" value="<?php echo $member_info[0]->ac_phone?>">
                    </div>
                    
					 <div class="form-group">
                      <label><?php echo _("Country")?> <span class="red">*</span></label>
                      <?php $remoteCountry=$this->config->item('remote_country_code');?>
                        <select class="form-control"  name="ac_country" id="ac_country">
						 <option value="">--<?php echo _("Select Country")?>--</option>
						 <?php
						  for($c=0;$c<count($countries);$c++){
							  ?>
							  <option value="<?php echo $countries[$c]->country_code;?>" <?php if($countries[$c]->country_code==$member_info[0]->ac_country){echo 'selected';}?>>
							  <?php echo $countries[$c]->country_name;?>
							  </option>
							  <?php
						  }
						 ?>
						</select>
					</div>
					
                    <!-- Multiple Radios -->
                    <div class="form-group">
                      <label><?php echo _("Gender")?> <span>&nbsp;</span> </label>
                      
                            <input name="ac_gender" id="ac_gender1" value="1" checked="checked" type="radio" <?php if($member_info[0]->ac_gender==1){echo _("checked");}?>>
                            <?php echo _("Male")?>&nbsp;&nbsp;
                            <input name="ac_gender" id="ac_gender2" value="2" type="radio" <?php if($member_info[0]->ac_gender==2){echo _("checked");}?>><?php echo _("Female")?></div>
                    
                    <!-- Textarea -->
                    <div class="form-group">
                      <label><?php echo _("About Member")?></label>
                        <textarea class="form-control" id="ac_about" name="ac_about"><?php echo $member_info[0]->ac_about?></textarea>
                    </div>
					
                    <div class="form-group">
					  <button class="btn btn-info" type="button" ng-click='UpdateMember()'>
					  <i class="fa fa-pencil"></i><?php echo _("Update")?></button>
					</div>
					
                  </fieldset>
				      <div id="rsDiv"></div>
                </form>
          </div>
        
	   </div>
	   </div>
	   </div>
	   
	    <div class="col-md-4">
		 
		   <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i><?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?> 
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
			  <!-- Member LIST -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo _("Latest Members")?></h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
				    <?php 
					 if($list_latest_members!=0){
					 foreach($list_latest_members as $member){
					?>
                    <li class="item">
                      <div class="product-img">
					    <?php 
						
						 if($member->ac_thumbnail!=''){
						?>
							<img src="<?php echo base_url().$member->ac_thumbnail?>" alt="Thumbnail">
						<?php 
						 }else{
							if($member->ac_gender!=1){
							?>
							<img src="<?php echo base_url().'theme4.0/admin/images/avatar2.png'?>" alt="Thumbnail">

							<?php
							}else{
							?>
							<img src="<?php echo base_url().'theme4.0/admin/images/avatar5.png'?>" alt="Thumbnail">

							<?php
							}
						 }
						 ?>
                      </div>
                      <div class="product-info">
                        <a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member->user_id)?>" class="More Details">
						 <?php echo $member->ac_first_name.' '.$member->ac_last_name?>
						 <?php 
						  if($member->is_validated==1){
						   ?>
						   <span class="label label-success pull-right"><?php echo _("activated")?></span>
							<?php						   
						  }else{
							  ?>
						  <span class="label label-warning pull-right"><?php echo _("pending")?></span>
							  <?php
						  }
						 ?>
						</a>
                        <span class="product-description">
                          <?php 
						   echo _("Joined on").date('d-M-Y H:i:s',strtotime($member->ac_join_date));
						  ?>
                        </span>
                      </div>
                    </li>
					<?php 
					 }
					 }else{
					 ?>
					  <li class="item"><i class="fa fa-info-circle"></i><?php echo _("No members found")?></li>
					 <?php
					 }
					 ?>
					 
                   
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="<?php echo base_url()?>members" class="uppercase"><?php echo _("View All")?></a>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Telephone is either mobile number(recommended) or, land line number along with country code(e.g. +673xxxxxx).")?></li>
				</ul>
		</div>
		</div>
		<?php 
  }else{
	    echo '<div class="box col-sm-12 text-center">
		<h2 class="text-danger"><i class="fa  fa-exclamation-circle"></i> ' . _('Unauthorized request') . '</h2>
		' . _('Sorry we don\'t allow unauthorized actions') . '<br><br>
		</div>';
  }
		?>
</section>

<?php $this->load->view('theme/footer.php');?>



