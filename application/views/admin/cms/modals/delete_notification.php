<div class="modal fade" id="deleNotifyModal" tabindex="-1" role="dialog" 
     aria-labelledby="delNotifyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title"><?php echo _("Confirm Alert")?></h4>
            </div>
			<div class="modal-body">
			   <div class="row">
			   <div class="col-md-7">
			   <h1><?php echo _("Are you sure ?")?></h1>
			     <?php echo _("You want to delete,")?> <b><?php echo _("notification headline")?></b><br><br>
				
				<textarea id="dlx_subject" name="dlx_subject" class="form-control"></textarea>
			    <div id="callNotifyStatus"></div>
				</div>
				
				<div class="col-md-5 text-center">
				<br><br>
				 <i class="fa fa-suitcase"></i> <?php echo _("Recipient is")?> <br> 
				 <b class="text-primary"><span id="dlx_recipient"></span></b> 
				</div>
				</div>
			 </div>
           
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  <div class="pull-left"> &nbsp; </div>
				</div>
				<div class="col-md-6">
				<button type="button" data-dismiss="modal" class="btn btn-danger" id="delete"><?php echo _("Yes")?></button>
				<button type="button" data-dismiss="modal" class="btn"><?php echo _("No")?></button>
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>