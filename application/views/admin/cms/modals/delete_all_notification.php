<div class="modal fade" id="deleAllNotifyModal" tabindex="-1" role="dialog" 
     aria-labelledby="delAllNotifyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title"><?php echo _("Confirm Alert")?></h4>
            </div>
			<div class="modal-body">
			  <div class="row">
			   <div class="col-md-6">
			     <h1 class="text-danger"><?php echo _("Are you sure ?")?></h1>
				 <div><?php echo _("You want to delete selected notifications")?></div> <br><br>
			   </form>
			   
			   </div>
			   <div class="col-md-6">
			     <b><?php echo _("Total Selected")?></b>
				 <h1 id="del_notifycount"></h1>
			   </div>
			   </div>
			 </div>
           
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  <div id="del_status" class="pull-left"></div>
				</div>
				<div class="col-md-6">
				<button type="button" data-dismiss="modal" class="btn btn-danger" id="delete_all"><?php echo _("Yes")?></button>
				<button type="button" data-dismiss="modal" class="btn"><?php echo _("No")?></button>
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>