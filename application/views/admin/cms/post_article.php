<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<script type="application/javascript" src="<?php echo base_url()?>theme4.0/admin/tinymce/tinymce.js"></script>
<script type="application/javascript" src="<?php echo base_url()?>js/cms/articles.js"></script>

<script>
  tinymce.init({
    selector: '#article_body',
	theme: 'modern',
    height: 250,
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
    ],
    content_css: '<?php echo base_url()?>theme4.0/admin/tinymce/skins/lightgray/content.min.css',
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
  });
</script>

<?php
 
 $notify_from= $this->session->userdata['login_in']['username'];
 $notify_from_email1= $this->session->userdata['login_in']['user_email'];
 
 $intlib=$this->internal_settings->local_settings();
 $notify_from_email2= $intlib[0]->default_from_mail;
 $notify_appname= $intlib[0]->app_default_name;
 
 
 $total_members=$this->account_model->list_all_members();
 $inactive_members=$this->account_model->list_inactive_members();
 $total_payment_transactions=$this->payment_model->total_member_payments();
 
 $appSettings= $this->app_settings_model->get_primary_settings();
 $currency_name=$appSettings[0]->app_currency;
 
 $article_info=$this->cms_model->list_articles_bytype($type);
 
 if($article_info!=0){
	    $article_type		    = $article_info[0]->article_type;
		$article_code    	    = $article_info[0]->article_code;
		$article_postedby  		= $article_info[0]->posted_by;
		$article_headline       = $article_info[0]->article_title;
		$article_body           = $article_info[0]->article_body;
 }else{
	    $article_type		    = '';
		$article_code    	    = '';
		$article_postedby  		= '';
		$article_headline       = '';
		$article_body           = '';
 }
 
 if($article_code!='' && $article_code!='0'){
	 $random_code=$article_code;
 }else{
	 $random_code=rand(1000,10000);
 }
 
?>

<section class="content-header">
 <?php 
  if($article_info!=0){
	  echo "<h1>Update Article (".$token.")</h1>";
  }else{
	  echo "<h1>Post Article (".$token.")</h1>";
  }
 ?>
 
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>cms/articles"><?php echo _("Articles")?></a></li> <li class="active"><?php echo _("Post Article")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			  
				<div ng-app="">
                 <form role="form" ng-controller="ArticleController" name="articleForm" id="articleForm" method="post" action="<?php echo base_url()?>cms/process_article" enctype="multipart/form-data">
				   <div class="row">
				   <div class="col-md-6">
				   <?php 
				    echo "Article code : <b>".$random_code."</b>";
				   ?>
				   </div>
				   <div class="col-md-6 text-right">
				   <a href="<?php echo base_url()?>cms/articles"><i class="fa fa-list"></i> <u><?php echo _("Article list")?></u></a>
				   </div>
				   </div>
				   <hr>
				   <div class="form-group">
                    
					<div class="row">
					<div class="col-md-6">
					<label><?php echo _("Article Type")?></label> <span class="red">*</span>
				    <select class="form-control" id="article_type" name="article_type" required="required">
					 <option value="about_us" <?php if($type=='about_us'){echo 'selected';}?>><?php echo _("About Us")?></option>
					 <option value="careers" <?php if($type=='careers'){echo 'selected';}?>><?php echo _("Careers")?></option>
					 <option value="terms_conditions" <?php if($type=='terms_conditions'){echo 'selected';}?>><?php echo _("Terms & Conditions")?></option>
					 <option value="privacy_policy" <?php if($type=='privacy_policy'){echo 'selected';}?>><?php echo _("Privacy Policy")?></option>
					 <option value="anti_spam_policy" <?php if($type=='anti_spam_policy'){echo 'selected';}?>><?php echo _("Anti Spam Policy")?></option>
					 <option value="other" <?php if($type=='other'){echo 'selected';}?>><?php echo _("Other")?></option>
					</select>
					
					<input type="hidden" value="<?php echo $random_code?>" id="article_code" name="article_code"/>
					</div>
					<div class="col-md-6">
					<label><?php echo _("Publish By")?></label>
						<input type="text" class="form-control" name="article_postedby" id="article_postedby"  value="<?php if($article_postedby==''){echo $notify_from.' ['.$notify_appname.']';} else {echo $article_postedby;}?>"> 
					</div>
					</div>
					</div>
					
					
					
					<div class="form-group">
                    <label><?php echo _("Article Title")?></label> <span class="red">*</span>
					<input type="text" class="form-control" name="article_headline" id="article_headline" placeholder="<?php echo _('E.g. Terms & conditions ') ?>" required="required" value="<?php echo @$article_headline?>">
					</div>
					
                    <div class="form-group">
                    <label><?php echo _("Article Content")?></label>  <span class="red">*</span>
                    <textarea placeholder="<?php echo _('Notification message') ?>" class="form-control"  id="article_body" name="article_body" required="required" rows="7" value="<?php echo @$article_body?>"><?php echo @$article_body?></textarea>
					</div>
					
					
                    <div class="form-group">
                         <button class="btn btn-info pull-left m-t-n-xs" type="submit" ng-click='articleBtn()'>
						  <?php 
							if($article_info!=0){
								echo '<i class="fa fa-pencil"></i> Update'; 
							}
							else{
	  
								echo '<i class="fa fa-share"></i> Post'; 
							}
							?>
                        </button>
                    </div>
					<div id="rsDiv"></div>
					<br>
					</form>
					</div>
				</div>
			</div>
            </div>
		    <div class="col-md-4">
			
			<div class="box box-widget widget-user-2">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header bg-primary">
			  <div class="widget-user">
				 <h3><i class="fa fa-leaf"></i> <?php echo _("Total Articles ")?>:
				<?php 
				 $article_count=$this->cms_model->list_articles();
				 if($article_count!=0){
					 echo "<b>".count($article_count)."</b>";
				 }else{
					 echo "<b>0</b>";
				 }
				?>
				</h3>
			  </div><!-- /.widget-user-image -->
			</div>
			 <div class="box-footer no-padding">
			   <ul class="nav nav-stacked">
			          <?php 
					   if($article_count!=0){
					    foreach($article_count as $article){
					   ?>
					    <li>
						<a href="<?php echo base_url().'cms/'.$article->article_type?>"><i class="fa fa-caret-right"></i> <?php echo $article->article_title?></a>
						</li>
						
					   <?php
						} 
				      }else{
						 ?>
						<li>
						<a href="#"><?php echo'No articles found';?></a>
						</li>
						<?php						 
					  }
				   ?>
                    
					 
				</ul>
			 </div>
			</div>
			
			 
			  <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-maroon">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i><?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?>
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Cross check article title and content before you publish.")?></li>
				 <li><?php echo _("Make sure you have selected relavent <b>Article Type</b>, otherwise it will overwrite the existing content of other article.")?></li>
				</ul>
		    </div>
   </div>

</section>

<?php $this->load->view('theme/footer.php');?>

