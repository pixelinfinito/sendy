<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
 
<script type="application/javascript" src="<?php echo base_url()?>js/notifications/send.js"></script>
<section class="content-header">
  <h1><?php echo _("Article List")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Articles")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>
	
	
   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>cms/articles" class="btn btn-sm btn-primary"  title="<?php echo _('Reload') ?>">
		 <i class="fa fa-rotate-left"></i><?php echo _("Reload")?></a>
   </div>
</div> 
</div>
 <div class="box-body">
    <table class="table table-bordered table-striped" id="dataTables-Articles">
		<thead ><!-- Table head -->
		<tr>
		
		<th class="col-sm-1"><?php echo _("Id")?></th>
		<th class="col-sm-1"><?php echo _("Article Code")?></th>
		<th class="col-sm-1"><?php echo _("Article Type")?></th>
		<th class="col-sm-1"><?php echo _("Title")?></th>
		<th class="col-sm-1"><?php echo _("Posted By")?></th>
		<th class="col-sm-1"><?php echo _("Posted On")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$cms_info=$result=$this->cms_model->list_articles();
		$sp = 1 
		?>
		<?php if ($cms_info!=0): foreach ($cms_info as $article) : ?>

		<tr>
		
		<td>
		<?php echo $article->article_id?>
		</td>
		<td>
		<?php echo $article->article_code;?>
		</td>
		
		<td>
		<?php echo $article->article_type;?>
		</td>
		<td>
		<a href="<?php echo base_url().'cms/'.$article->article_type?>"   title="<?php echo _('Full View') ?>">
		 <?php echo "<u>".ucfirst($article->article_title)."</u>";?>
		 </a>
		</td>
		
		<td>
		 <?php echo ucfirst($article->posted_by);?>
		</td>
		
		<td>
		<?php echo date('d-M-Y h:i:s',strtotime($article->posted_on));?>
		</td>
		
		
		<td>
		 <a href="<?php echo base_url().'cms/'.$article->article_type?>"  class="btn btn-xs btn-info" title="<?php echo _('Full View') ?>">
		  <i class="fa fa-pencil"></i>
		 </a> 
		 
		 <a href="<?php echo base_url().$article->article_type?>"  class="btn btn-xs btn-default" target="_blank" title="<?php echo _('View Page') ?>">
		  <i class="fa fa-link"></i>
		 </a> 
		 
		 <a href="javascript:void(0)" class="btn btn-xs btn-danger" title="<?php echo _('Delete Message') ?>" onclick="delNotify('<?php echo $article->article_id?>','<?php echo $article->article_title?>')">
		  <i class="glyphicon glyphicon-trash"></i>
		 </a> 
		</td>
	
		</tr>
		<?php
		$sp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
</table> <!-- / Table -->

<?php $this->load->view('admin/notifications/modals/delete_notification');?> 
 </div>
</div>	
</section>
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-Articles').DataTable({
			"order": [[ 0, "desc" ]],
			"aoColumnDefs" : [{
            'bSortable' : false,
            'aTargets' : [ 0,5 ]}],
			"columns": [
			{ "width": "1%" },
			null,
			null,
			null,
			null,
			null,
			null
			]
		});
		
		
		
    });
</script>
		
