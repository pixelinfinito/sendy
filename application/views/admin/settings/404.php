<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<section class="content-header">
  <h1>
	<?php echo _("Settings")?>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>settings"><?php echo _("Settings")?></a></li> <li class="active"><?php echo _("Unknown")?></li>
  </ol>
</section>
		
 <section class="content">
 <div class="row">
 <div class="col-md-12">
	<?php 

		echo '<div class="box text-center"><br>
		<h2 class="text-danger"><i class="fa  fa-exclamation-circle"></i> ' . _('Unauthorized request') . '</h2>
		' . _('Sorry we dont allow unauthorized actions, please use sidebar nagivation') . '<br><br>
		</div>';
	
  ?>	
  </div>
  </div>
 </section> 


<?php $this->load->view('theme/footer.php');?>
