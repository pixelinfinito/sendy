<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_payments_bymember.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$user_id=base64_decode($this->uri->segment(3));
		if($user_id!=''){
		$paymentInfo= $this->payment_model->get_member_payments($user_id);
		$total_payment_transactions=$this->payment_model->total_member_payments();
		$appSettings= $this->app_settings_model->get_primary_settings();
		$currency_name=$appSettings[0]->app_currency;
        $member=$this->account_model->get_member_details($user_id);
		$payment_arr=array();
		
		?>
		<table border='1'>
		<tr>
		 <td colspan="4"><h2><?php echo _("Member ")?>: <?php echo $member[0]->ac_first_name.' '.$member[0]->ac_last_name?></h2></td>
		 <td colspan="3"><b><?php echo _("Country ")?>: <?php echo $member[0]->country_name?></b> </td>
		</tr>
		
		<tr>
		<td><b><?php echo _("S.No")?></b></th>
		<td><b><?php echo _("Ref.No")?></b></th>
		<td><b><?php echo _("Amount")?></b></th>
		<td><b><?php echo _("Transaction ID")?></b></th>
		<td><b><?php echo _("Response Code")?></b></th>
		<td><b><?php echo _("Payment Datetime")?></b></th>
		<td><b><?php echo _("Payment Status")?></b></th>
		</tr>
		
		<?php
		$i=0;
		foreach($paymentInfo as $payment){
	    ?>
		
		<tr>
		<td>
		<?php echo ++$i?>
		</td>
		
		<td>
		<?php echo $payment->acc_item_number?>
		</td>
		<td>
		<?php 
		echo $currency_name.' '.$payment->c2o_total_amount;
		if($payment->payment_status==1){
		array_push($payment_arr,$payment->c2o_total_amount);
		}
		?> 
		</td>
		
		<td>
		  <?php echo $payment->c2o_transaction_id ?>
		</td>
		<td>
		  <?php echo $payment->c2o_response_code ?>
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($payment->payment_datetime)); 
		?>
		</td>
		<td>
		 <?php 
		  switch($payment->payment_status){
			  case '1':
			  ?>
			  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i><?php echo _("Accepted")?></div>
			  <?php
			  break;
			  
			  case '0':
			  ?>
			  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i><?php echo _("Failed")?></div>
			  <?php
			  break;
		  }
		 ?>
         </td>
		</tr>
		<?php } 
		?>
		<tr>
		<td colspan="2">&nbsp;</td>
		<td><?php echo "<h3>Total: ".$currency_name.' '.array_sum($payment_arr)."</h3>";?></td>
		<td colspan="4"><small>* <?php echo _("Total is calculated for successful transactions.")?> </small></td>
		</tr>
</table>

<?php 
}else{
	
	echo _("Missing inputs.");
}
?>

