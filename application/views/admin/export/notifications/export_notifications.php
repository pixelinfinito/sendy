<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_all_notifications.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$notify_info=$result=$this->notification_model->list_notifications();
		
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?></b></td>
		<td><b><?php echo _("Notification Type")?></b></td>
		<td><b><?php echo _("Headline")?></b></td>
		<td><b><?php echo _("Notification To")?></b></td>
		<td><b><?php echo _("Is Read")?></b></td>
		<td><b><?php echo _("Message")?></b></td>
		</tr>
		<?php
		$i=0;
		foreach($notify_info as $notify){
	    ?>
		<tr>
		<td>
		<?php echo ++$i?>
		</td>
		<td><?php echo ucfirst($notify->notify_type);?>
		</td>
		<td>
		 
		  <?php 
		   if($notify->read_status!=0){
			   ?>
			   <u><?php echo $notify->notify_title?></u>
			   <?php
		   }else{
			   ?>
			   <u><b><?php echo $notify->notify_title?></b></u>
			   <?php
		   }
		  ?>
		
		
		</td>
		<td>
		  <?php 
		  echo ucfirst($notify->notify_to)."<br>";
		  echo "<small class='text-muted'> <b>" . _('Sent On') . "</b> ".date('d-M-Y h:i:s',strtotime($notify->notify_timestamp))."</small>";
		  ?>  
		</td>
		<td>
		  <?php 
		   if($notify->read_status!=0){
			   echo "<span class='text-success'><i class='fa fa-check'></i>" . _("Yes") . "</span>";
		   }else{
			   echo "<span class='text-danger'><i class='fa fa-hourglass-half'></i>" . _("No") . "</span>";
		   }
		  ?>  
		</td>
		<td>
		  <?php 
		  echo $notify->notify_body;
		  ?>
		</td>
		
		</tr>
		<?php } ?>
</table>

