<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_wallet_history.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		//$user_id=$this->uri->segment(3);
		$wallet_history= $this->account_model->all_wallet_transactions();
		$appSettings= $this->app_settings_model->get_primary_settings();
		$currency_name=$appSettings[0]->app_currency;
        
		$payment_arr=array();
		
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?></b></td>
		<td><b><?php echo _("Ref.No")?></b></td>
		<td><b><?php echo _("Amount")?></b></td>
		<td><b><?php echo _("Payment By")?></b></td>
		<td><b><?php echo _("Date Of Payment")?></b> </td>
		<td><b><?php echo _("Payment/Invoice Ref.No")?></b></td>
		<td><b><?php echo _("Remarks")?></b> </td>
		<td><b><?php echo _("Status")?></b></td>
		</tr>
		<?php
		$i=0;
		foreach($wallet_history as $wallet){
	    ?>
		<tr>
		<td>
		<?php echo ++$i?>
		</td>
		
		<td>
		<?php echo $wallet->ad_reference?>
		</td>
		<td>
		<?php 
		echo $currency_name.' '.$wallet->ad_payment;
		if($wallet->payment_status==1){
			array_push($payment_arr,$wallet->ad_payment);
		}
		?> 
		</td>
		<td>
		<?php echo $wallet->ac_first_name.' '.$wallet->ac_last_name?>
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($wallet->date_of_payment)); 
		?>
		</td>
		<td>
		   <?php echo $wallet->payment_reference ?>
		</td>
		<td>
		<?php 
		  echo $wallet->payment_remarks
		?>
		</td>
		<td>
		     <?php 
			  switch($wallet->payment_status){
				  case '1':
				  ?>
				  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i><?php echo _("PAID")?></div>
				  <?php
				  break;
				  
				  case '0':
				  ?>
				  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i><?php echo _("Failed")?></div>
				  <?php
				  break;
			  }
			 ?>
             
         </td>
		 
		</tr>
		<?php } ?>
		
	<tr>
	 <td colspan="2">&nbsp;</td>
	 <td>
	  <?php echo "<h3>Total : ".$currency_name.' '.array_sum($payment_arr)."</h3>"?>
	 </td>
	 <td colspan="5">
	 <small>* <?php echo _("Total payments calculated for successful transactions.")?></small>
	 </td>
	 
    </tr>	
</table>

