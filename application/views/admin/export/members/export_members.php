<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_members.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$member_info=$this->account_model->list_all_members();
		$appSettings= $this->app_settings_model->get_primary_settings();
		$appCountry=$appSettings[0]->app_country;
		$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
		
		?>
		<table border='1'>
		<tr>
		<td><strong><?php echo _("Name")?></strong></td>
		<td><strong><?php echo _("Country")?></strong></td>
		<td><strong><?php echo _("Contact Number")?></strong></td>
		<td><strong><?php echo _("Email Id")?></strong></td>
		<td><strong><?php echo _("Available Funds")?></strong></th>
		<td><strong><?php echo _("Total Campaigns")?></strong></th>
		<td><strong><?php echo _("Status")?></strong></th>
		</tr>
		<?php
		foreach($member_info as $member){
	    ?>
		<tr>
		<td>
		<?php echo $member->ac_first_name.' '.$member->ac_last_name?>
		<br><small class="text-muted"><b><?php echo _("Joined On")?></b> <?php echo date('d-M-Y H:i:s',strtotime($member->ac_join_date))?></small>
		<br>
		
		<?php 
		 if($member->ac_gender==1){
			 echo "<small><b>" . _('Gender') . " - </b>" . _('Male') . "</small>";
		 }else{
			 echo "<small><b>" . _('Gender') . " - </b>" . _('Female') . "</small>";
		 }
		?>
		</td>
		<td>
		<?php echo $member->country_name ?> 
		</td>
		
		<td>
		<?php echo $member->ac_phone ?> 
		</td>
		
		<td>
		<?php echo $member->ac_email ?> 
		</td>
		
		<td>
		  <?php 
		    $walletInfo= $this->account_model->get_account_wallet($member->user_id);
			if(@$walletInfo[0]->balance_amount!=''){
			  echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.@$walletInfo[0]->balance_amount;
			}else{
				echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name." 00.00";
			}
			?>
		</td>
		
		<td>
		 <?php 
		  $campaigns=$this->sms_model->get_campaign_history($member->user_id);
		  if($campaigns!=0){
			  echo count($campaigns);
		  }else{
			  echo "0";
		  }
		  ?>
		</td>
		
		<td>
		 <?php 
		  if($member->status!=0){
			  echo _("Active");
		  }else{
			  echo _("Inactive");
		  }
		 ?>
		</td>
		
		</tr>
		<?php } ?>
</table>

