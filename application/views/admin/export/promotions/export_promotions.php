<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_promotions.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$promotion_info=$this->promotions_model->list_promotions();
		
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?></b></td>
		<td><b><?php echo _("Promotion Code")?></b></td>
		<td><b><?php echo _("Subject")?></b></td>
		<td><b><?php echo _("Recipient")?></b></td>
		<td><b><?php echo _("Message Type")?></b></td>
		<td><b><?php echo _("Message")?></b></td>
		<td><b><?php echo _("Status")?></b></td>
		</tr>
		<?php
		$i=0;
		foreach($promotion_info as $notify){
	    ?>
		<tr>
		<td>
		<?php echo ++$i?>
		</td>
		
		<td><?php echo ucfirst($notify->promotion_code);?>
		</td>
		<td>
		<a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->promotion_id;?>)" title="<?php echo _('View Message') ?>">
		 <u><?php echo ucfirst($notify->promotion_title);?></u>
		 </a>
		  
		</td>
		<td>
		  <?php 
		  echo ucfirst($notify->to_name)."<br>";
		  echo "<small class='text-muted'> <b>" . _('Sent On') . "</b> ".date('d-M-Y h:i:s',strtotime($notify->notify_timestamp))."</small>";
		  ?>  
		</td>
		
		
		<td>
		 <?php 
		  switch($notify->promotion_type){
			  case '1':
			  echo "<i class='fa fa-mobile'></i> SMS";
			  break;
			  
			  case '2':
			  echo "<i class='fa fa-envelope-o'></i> Email";
			  break;
			  
		  }
		 ?>
		</td>
		<td>
		  <?php 
		  echo $notify->promotion_body;
		  ?>
		</td>
		<td>
		 <?php 
		  switch($notify->status){
			  
			  case '1':
			  echo '<span class="label label-success"><i class="fa fa-check"></i> ' . _('Delivered') .'</span>';
			  break;
			  
			  case '2':
			  echo '<span class="label label-warning"><i class="fa fa-hourglass-half"></i> ' . _('Waiting') .'</span>';
			  break;
			  
			  case '0':
			  echo '<span class="label label-danger"><i class="fa fa-exclamation-triangle"></i> ' . _('Failed') .'</span>';
			  break;
		  }
		 ?>
		</td>
		
		</tr>
		<?php } ?>
</table>

