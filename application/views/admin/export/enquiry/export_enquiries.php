<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_enquiries.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$guest_info=$this->internal_settings->guest_enquiries();
		
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?></b></td>
		<td><b><?php echo _("Name")?></b></td>
		<td><b><?php echo _("Type")?></b></td>
		<td><b><?php echo _("Email Id")?></b></td>
		<td><b><?php echo _("Contact No")?></b></td>
		<td><b><?php echo _("Comments")?></b></td>
		<td><b><?php echo _("Geo Information")?></b></td>
		</tr>
		<?php
		$i=0;
		foreach($guest_info as $enquiry){
	    ?>
		<tr>
		<td><?php echo ++$i;?></td>
		<td>
		  <?php echo ucfirst($enquiry->enq_name);?>
		  <?php echo "<br><small class='text-muted'> <b>" .  _("From") . " :</b> ".$enquiry->country_name."</small>"?>
		</td>
		<td>
		  <?php echo ucfirst($enquiry->enq_type);?>
		</td>
		<td>
		   <?php echo $enquiry->enq_email?>
		</td>
		<td>
		  <?php echo $enquiry->enq_contact?>
		</td>
		<td>
		  <?php echo $enquiry->enq_comments?>
		  <?php  echo "<br><small class='text-muted'> <b>" . _("Enquiry On") . "</b> ".date('d-M-Y h:i:s',strtotime($enquiry->enq_on))."</small>";?>
		</td>
		<td>
		  <?php 
		  $exp_raddress=explode(',',$enquiry->remote_address);
		  echo @$exp_raddress[0]."<hr>";
		  echo "<small class='text-muted'>".@$exp_raddress[1]."</small><br>";
		  echo "<small class='text-muted'>".@$exp_raddress[2]."</small><br>";
		  echo "<small class='text-muted'>".ucfirst(@$exp_raddress[3])."</small><br>";
		  echo "<small class='text-muted'>".ucfirst(@$exp_raddress[4])."</small><br>";
		  
		  ?>
		</td>
		
		</tr>
		<?php } ?>
</table>

