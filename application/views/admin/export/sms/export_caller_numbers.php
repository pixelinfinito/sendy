<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_caller_numbers.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$user_id=$this->uri->segment(3);
		$caller_numbers=$this->sms_model->get_caller_numbers($user_id);
		$appSettings= $this->app_settings_model->get_app_settings();
		?>
		<table border='1'>
		<tr>
		<td><strong><?php echo _("Country")?></strong></td>
		<td><strong><?php echo _("Caller Number")?></strong></td>
		<td><strong><?php echo _("Sender ID")?></strong></td>
		<td><strong><?php echo _("Lease Period")?></strong></td>
		<td><strong><?php echo _("Price")?></strong></td>
		<td><strong><?php echo _("Purchase On")?></strong></td>
		<td><strong><?php echo _("Expires On")?></strong></td>
		<td><strong><?php echo _("Status")?></strong></td>
		
		</tr>
		<?php
		foreach($caller_numbers as $data){
	    ?>
		<tr>
		<td><?php echo $data->country; ?></td>
		<td>
		<?php 
		 if($data->twilio_origin_number!=''){
			 echo $data->twilio_origin_number;
			
		 }else{
		    echo _("In Process");
		 }
		 ?>
		</td>
		
		<td>
		<?php 
		
		   if($data->twilio_sender_id!=''){
			 
                if($data->sender_id_status!=2){
					echo $data->twilio_sender_id;
					
				}else{
					echo _("In Process");
				}			 
		   } else{
				echo _("You may Request");
		   }
		?>
		</td>
		 <td>
		<?php echo $data->subscription_period.' Month(s)'?>
		</td>
		
		 <td>
		<?php echo $data->cn_currency.' '.$data->price?>
		</td>
		
		 <td>
		<?php echo date('d-M-Y H:i:s',strtotime($data->purchased_on))?>
		</td>
		
		<td>
		<?php
		date_default_timezone_set($appSettings[0]->app_timezone);
		$expires_in = strtotime($data->expired_on) - strtotime($data->purchased_on);
		$today=date('Y-m-d H:i:s');
		if(strtotime($data->expired_on) > strtotime($today))
		{
			$number_expires= floor($expires_in/3600/24);
			echo  $number_expires." Day(s)";
		}
		else
		{
			echo _("Expired");
		}
		?>
	
		</td>
		<td>
		 <?php 
		
		   switch($data->status){
			   case '2':
			   echo _("In Process");
			   break;
			   
			   case '1':
			   echo _("Active");
			   break;
			   
			   case '0':
			   echo _("Suspended");
			   break;
		   }
		  ?>
		</td>
		
		</tr>
		<?php } ?>
</table>

