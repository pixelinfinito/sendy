<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_schedules.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$user_id=$this->uri->segment(3);
		$schedules_info=$this->sms_model->get_campaign_schedules($user_id);
		
		?>
		<table border='1'>
		<tr>
		<td><strong><?php echo _("Campaign Code")?></strong></td>
		<td><strong><?php echo _("Message")?></strong></td>
		<td><strong><?php echo _("Submit On")?></strong></td>
		<td><strong><?php echo _("Scheduled On")?></strong></td>
		<td><strong><?php echo _("Campaign Summary")?></strong></td>
		<td><strong><?php echo _("Status")?></strong></td>
		
		
		</tr>
		<?php
		foreach($schedules_info as $data){
	    ?>
		<tr>
		<td><?php echo $data->campaign_code; ?></td>
		<td><?php echo $data->message; ?></td>
		<td><?php echo date('d-M-Y H:i:s',strtotime($data->do_submit)); ?></td>
		<td><?php echo date('d-M-Y H:i:s',strtotime($data->scheduled_datetime)); ?></td>
		
		<td>
		<?php 
		      $success_count=$this->sms_model->get_success_count($data->campaign_code,$user_id);
			  $participated_count=$this->sms_model->get_campaign_contacts_count($data->campaign_code,$user_id);
			  if($success_count!=0){
				  $failed=$participated_count-$success_count;
			  }else{
				  $failed="0";
			  }
			  echo _("total") . " (".$participated_count.") => " ._("success") . " (".$success_count.")/ " ._("failed") . " (".$failed.")";
		?>
		</td>
		<td>
		
		 <?php 
		   switch($data->status){
			   case '1':
			   echo _("completed");
			   break;
			   
			   case '0':
			   echo _("waiting");
			   break;
		   }
		  ?>
		
		</td>
		
		</tr>
		<?php } ?>
</table>

