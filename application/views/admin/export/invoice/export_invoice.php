

<?php
		
		$intlib=$this->internal_settings->local_settings();
		
		$user_id=base64_decode($this->uri->segment(4));
		$payment_id=base64_decode($this->uri->segment(3)).'w';
		$wallet_transaction=$this->wallet_model->wallet_invoice_byid($user_id,$payment_id);
		$appSettings= $this->app_settings_model->get_app_settings();
		$currency=$appSettings[0]->app_currency;
		
		$member_name=$wallet_transaction[0]->ac_first_name.' '.$wallet_transaction[0]->ac_last_name;
		$member_city=$wallet_transaction[0]->ac_city;
		$member_add1=$wallet_transaction[0]->ac_address1;
		$member_add2=$wallet_transaction[0]->ac_address2;
		$member_email=$wallet_transaction[0]->ac_email;
		$member_phone=$wallet_transaction[0]->ac_phone;
		
		$order_reference=$wallet_transaction[0]->ad_reference;
		$payment_reference=$wallet_transaction[0]->payment_reference;
		$date_of_payment=$wallet_transaction[0]->date_of_payment;
		
		if($wallet_transaction[0]->campaign_code!=''){
			$payment_headline="To campaign service code : ".$wallet_transaction[0]->campaign_code;
		}else{
			$payment_headline="To caller number/campaign";
		}
		$payment_desc=$wallet_transaction[0]->payment_remarks;
		$total_amount=$wallet_transaction[0]->ad_payment;
		
		$grab_success_count=$this->sms_model->get_success_count($wallet_transaction[0]->campaign_code,$user_id);
		$grab_participated_count=$this->sms_model->get_campaign_contacts_count($wallet_transaction[0]->campaign_code,$user_id);
		
		if($grab_success_count!=0){
			$success_count=$grab_success_count;
		}else{
			$success_count='-';
		}
		if($grab_participated_count!=0){
			$participated_count=$grab_participated_count;
		}else{
			$participated_count='-';
		}
		
		/*header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=invoice_".$payment_reference.".pdf");
		header("Pragma: no-cache");
		header("Expires: 0");*/
   
   $logo_path="<img src='".base_url()."theme4.0/client/images/logo.png'>";    
   	
   $html='<table  width="100%">
         <tr>
		  <td colspan="2" class="bg_blue">
		  <table width="100%" >
		  <tr>
		  <td> <img src="'.base_url().'theme4.0/client/images/logo.png" alt="Logo" width="150" height="70"/> </td>
		  <td class="pull_right"> <h3>' . _('Invoice') . '</h3> <h5>'.$payment_reference.'</h5></td>
		  </tr>
		  </table>
		  </td>
		 </tr>
		 <tr>
		  <td colspan="2">
		   &nbsp;
		  </td>
		 </tr>
		 <tr>
		 <td width="50%">
		  <table width="100%" class="border_all">
		   <tr>
		    <td class="border_bottom">
			<strong>' . _('Customer Information') . '</strong>
			</td>
			</tr>
			
			<tr>
			<td>'.$member_name.'</td>
			</tr>
			
			<tr>
			<td>'.$member_city.'</td>
			</tr>
			
			<tr>
			<td>'.$member_add1.'</td>
			</tr>
			
			<tr>
			<td>'.$member_add2.'</td>
			</tr>
			
			<tr>
			<td>'.$member_email.'</td>
			</tr>
			
			<tr>
			<td>'.$member_phone.'</td>
			</tr>
		  
		  </table>
		 </td>
		 
		  <td width="50%">
		  <table width="100%">
		  
			<tr>
			<td><b>' . _('Invoice No') . '</b></td> <td>:</td> <td><b>'.$payment_reference.'</b></td>
			</tr>
			
			<tr>
			<td>' . _('Order Reference') . '</td> <td>:</td> <td>'.$order_reference.'</td>
			</tr>
			
			<tr>
			<td>' . _('Date Of Payment') . '</td> <td>:</td> <td>'.date('d-M-Y H:i:s',strtotime($date_of_payment)).'</td>
		   </tr>
		   
		   <tr>
			<td>' . _('Reference') . '</td> <td>:</td> <td>'.$payment_headline.'</td>
		   </tr>
		   
		  </table>
		 </td>
		 </tr>
		 
		 <tr>
		 <td colspan="2">
		  &nbsp;
		 </td>
		 </tr>
		  <tr>
		 <td colspan="2" class="border_bottom">
		   <table  width="100%">
		    <tr>
			 <td class="cap_letters"><b>' . _('Bill Description') . '</b></td>
			 <td class="cap_letters"><b>' . _('Quantity') . '</b></td>
			 <td class="cap_letters"><b>' . _('Quantity Charged') . '</b> </td>
			 <td class="cap_letters"><b>' . _('Total') . '</b></td>
			</tr>
			 <tr>
			 <td>'.$payment_desc.'</td>
			 <td>'.$participated_count.'</td>
			 <td>'.$success_count.'</td>
			 <td>'.$currency.' '.$total_amount.'</td>
			</tr>
		   </table>
		 </td>
		 </tr>
		 
		 <tr>
		 <td colspan="2"  class="border_bottom">
		 <table width="100%">
		  <tr>
		  <td colspan="3">
		   &nbsp;
		  </td>
		  
		  <td style="text-align:right">
		   <h4> ' . _('Grand Total') . ' : '.$currency.' '.$total_amount.'</h4>
		  </td>
		  </tr>
		  </table>
		  </td>
		 </tr>
	</table>
<table>
<tr>
 <td colspan="4">
		******* ' . _('Thank you') . ' '.$intlib[0]->app_default_name.' *******
 </td>
 </tr>
</table>';

$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list
// LOAD a stylesheet
$stylesheet = file_get_contents(APPPATH.'third_party/mpdf/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->WriteHTML($html,2);
$mpdf->Output('Invoice_'.$payment_reference.'.pdf','I');
exit;

?>

