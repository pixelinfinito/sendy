<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_ci_requests.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		//$user_id=$this->uri->segment(3);
		$ci_info=$this->caller_number_model->get_ci_requests();
		$appSettings= $this->app_settings_model->get_app_settings();
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?></b></td>
		<td><b><?php echo _("Request By")?></b></td>
		<td><b><?php echo _("Request Country")?></b></td>
		<td><b><?php echo _("Number")?></b></td>
		<td><b><?php echo _("Caller ID")?></b></td>
		<td><b><?php echo _("Payment Status")?></b></td>
		<td><b><?php echo _("Approval Status")?></b></td>
		<td><b><?php echo _("Caller Number Status")?></b></td>
		
		</tr>
		
		<?php
		$i=0;
		foreach($ci_info as $cn){
	    ?>
		<tr>
		<td>
		<?php 
		echo ++$i;
		?>
		</td>
		
		<td>
		<?php echo $cn->ac_first_name.' '.$cn->ac_last_name?>
		</td>
		
		<td>
		<?php echo $cn->country_name?>
		<br><small class="text-success"><b><?php echo _("Purchased On")?>: </b><br><?php echo date('d-M-Y H:i:s',strtotime($cn->purchased_on));?></small>
		</td>
		
		<td>
		   <?php 
		   if($cn->twilio_origin_number!=''){
		     echo $cn->twilio_origin_number.'<br>';
			 echo '<small><b class="text-danger">' . _('Expired On') . ':</b><br> '.date('d-M-Y H:i:s',strtotime($cn->expired_on))."</small>";
		   } else{
		     ?>
			 <a href="javascript:void(0)" onclick="javascript:assign_caller_number('<?php echo $cn->tid?>','<?php echo $cn->ac_first_name.' '.$cn->ac_last_name?>','<?php echo $cn->country_name?>')"><i class="fa fa-plus-circle"></i><?php echo _("Assign Number")?></a>
			 <?php
		   }
		   ?> 
			 
		</td>
		<td>
		   <?php 
		   if($cn->twilio_sender_id!=''){
		     echo $cn->twilio_sender_id.'<br>';
			
		   } else{
		     ?>
			<i class="fa fa-hourglass-o"></i> <a href="javascript:void(0)" ng-click="approveCIfn()" class="text-warning" > <u><?php echo _("Waiting")?></u> </a>
			 <?php
		   }
		   ?> 
			 
		</td>
		
		<td>
		<?php 
		  if($cn->payment_status=='1'){
		   echo '<span class="label label-success"><i class="fa fa-check"></i> ' . _('Received') . '</span>';
		  }else{
		   echo '<span class="label label-warning"><i class="fa fa-times"></i> ' . _('Not yet') . '</span>';
		  }
		  
		?>
		</td>
		
		<td>
		 <?php 
		    switch($cn->sender_id_status){
			   case 2:
			   if($cn->cn_status=='3' || $cn->cn_status=='0'){
			   ?>
			    <a href="javascript:void(0)" onclick="approveCallerId('<?php echo $cn->tid?>','<?php echo $cn->twilio_sender_id?>','<?php echo $cn->user_id?>')"  class="btn btn-xs btn-info" title="<?php echo _('Caller Number Suspended, No use if your approve') ?>">
				<i class="fa fa-hourglass-o"></i><?php echo _("Waiting")?> ..</a>
				<a href="javascript:void(0)" onclick="suspendCallerId('<?php echo $cn->tid?>','<?php echo $cn->twilio_sender_id?>')"  class="btn btn-xs btn-danger" title="<?php echo _('Suspend CallerID') ?>">
				<i class="fa fa-times"></i> 
			    </a>
			   <?php
			   }
			   else
			   {
				   ?>
				<a href="javascript:void(0)" onclick="approveCallerId('<?php echo $cn->tid?>','<?php echo $cn->twilio_sender_id?>','<?php echo $cn->user_id?>')"  class="btn btn-xs btn-warning" title="<?php echo _('Waiting for approval') ?>">
				<i class="fa fa-hourglass-o"></i><?php echo _("Waiting")?> ..</a>
				<a href="javascript:void(0)" onclick="suspendCallerId('<?php echo $cn->tid?>','<?php echo $cn->twilio_sender_id?>')"  class="btn btn-xs btn-danger" title="<?php echo _('Suspend CallerID') ?>">
				<i class="fa fa-times"></i> 
			    </a>
				   <?php
			   }
			   break;
			   
			   case 1:
			   ?>
			   <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-check"></i><?php echo _("Approved")?></a>
			   <a href="javascript:void(0)" onclick="suspendCallerId('<?php echo $cn->tid?>','<?php echo $cn->twilio_sender_id?>')"  class="btn btn-xs btn-danger" title="<?php echo _('Suspend CallerID') ?>">
				<i class="fa fa-times"></i> 
			    </a>
			   <?php
			   break;
			   
			   case 3:
			   ?>
			   
			    <a href="javascript:void(0)"  class="btn btn-xs btn-danger" title="<?php echo _('Caller ID Suspended, you may approve it again') ?>">
				<i class="fa fa-times"></i><?php echo _("Suspended")?></a>
				
			   <a href="javascript:void(0)" onclick="approveCallerId('<?php echo $cn->tid?>','<?php echo $cn->twilio_sender_id?>','<?php echo $cn->user_id?>')"  class="btn btn-xs btn-info" title="<?php echo _('ReApprove CallerID') ?>">
				<i class="fa fa-check-circle"></i>
			    </a>
			   <?php
			   break;
			   
			   case 0:
			   ?>
			   <a href="javascript:void(0)" class="text-danger"><i class="fa fa-times"></i><?php echo _("Rejected")?></a>
			   <?php
			   break;
			   
			}
		 ?>
		</td>
		
		
		<td>
		<?php 
		
		  switch($cn->cn_status){
			  case 1:
			  echo '<span class="label label-success"><i class="fa fa-caret-up"></i>&nbsp; ' . _('Active') . '</span>';
			  break;
			  
			  case 2:
			  echo '<span class="label label-warning"><i class="fa fa-hourglass-2"></i>&nbsp; ' . _('Waiting') . '</span>';
			  break;
			  
			  case 0:
			  echo '<span class="label label-danger"><i class="fa fa-times"></i> ' . _('Rejected') . '</span>';
			  break;
			  
			  case 3:
			  echo '<span class="label label-danger"><i class="fa fa-times"></i> ' . _('Suspended') . '</span>';
			  break;
			  
			 
		  }
		  
		?>
		</td>
		
		
		
		</tr>
		<?php } ?>
</table>

