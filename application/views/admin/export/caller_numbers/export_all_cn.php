<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_cn_requests.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		//$user_id=$this->uri->segment(3);
		$cn_info=$this->caller_number_model->get_cn_requests();
		$appSettings= $this->app_settings_model->get_app_settings();
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?></b></td>
		<td><b><?php echo _("Request By")?></b></td>
		<td><b><?php echo _("Request Country")?></b></td>
		<td><b><?php echo _("Number")?></b></td>
		<td><b><?php echo _("Price")?></b></td>
		<td><b><?php echo _("Period")?></b></td>
		<td><b><?php echo _("Payment Status")?></b></td>
		<td><b><?php echo _("Caller Number Status")?></b></td>
		</tr>
		
		<?php
		$i=0;
		foreach($cn_info as $cn){
	    ?>
		<tr>
		<td>
		<?php 
		echo ++$i;
		?>
		</td>
		
		<td>
		<?php echo $cn->ac_first_name.' '.$cn->ac_last_name?>
		</td>
		
		<td>
		<?php echo $cn->country_name?>
		<br><small class="text-success"><b><?php echo _("Purchased On")?>: </b><br><?php echo date('d-M-Y H:i:s',strtotime($cn->purchased_on));?></small>
		</td>
		
		<td>
		   <?php 
		   if($cn->twilio_origin_number!=''){
		     echo $cn->twilio_origin_number.'<br>';
			 echo '<small><b class="text-danger">' . _('Expired On') . ':</b><br> '.date('d-M-Y H:i:s',strtotime($cn->expired_on))."</small>";
		   } else{
		     ?>
			 <a href="javascript:void(0)" onclick="javascript:assign_caller_number('<?php echo $cn->tid?>','<?php echo $cn->ac_first_name.' '.$cn->ac_last_name?>','<?php echo $cn->country_name?>')"><i class="fa fa-plus-circle"></i><?php echo _("Assign Number")?></a>
			 <?php
		   }
		   ?> 
			 
		</td>
		<td>
		 <?php 
		   echo $cn->cn_currency.' '.$cn->price;
		 ?>
		</td>
		
		<td>
		 <?php 
		   echo $cn->subscription_period.'<small> ' . _('Month(s)') . '</small>';
		  ?>
		</td>
		
		<td>
		<?php 
		  if($cn->payment_status=='1'){
		   echo '<span class="label label-success"><i class="fa fa-check"></i> ' . _("Received") . '</span>';
		  }else{
		   echo '<span class="label label-warning"><i class="fa fa-times"></i> ' . _("Not yet") . '</span>';
		  }
		  
		?>
		</td>
		
		<td>
		<?php 
		
		  switch($cn->cn_status){
			  case 1:
			  echo '<span class="label label-primary">' . _('Active') . '</span>';
			  break;
			  
			  case 2:
			  echo '<span class="label label-warning">' . _('Waiting') . '</span>';
			  break;
			  
			  case 0:
			  echo '<span class="label label-danger"><i class="fa fa-times"></i> ' . _('Rejected') . '</span>';
			  break;
			  
			  case 3:
			  echo '<span class="label label-danger"><i class="fa fa-times"></i> ' . _('Suspended') . '</span>';
			  break;
			  
			 
		  }
		  
		?>
		</td>
		
		
		</tr>
		<?php } ?>
</table>

