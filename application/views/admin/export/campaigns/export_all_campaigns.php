<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_all_campaigns.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		
		
		$campaigns_info=$this->sms_model->get_all_campaigns();
		
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?></b></td>
		<td><b><?php echo _("Campaign Code")?></b> </td>
		<td><b><?php echo _("Campaign By")?></b></td>
		<td><b><?php echo _("Message")?></b> </td>
		<td><b><?php echo _("Sent On")?></b></td>
		<td><b><?php echo _("Campaign Summary")?></b> </td>
		<td><b><?php echo _("Type")?></b> </td>
		<td><b><?php echo _("Status")?></b> </td>
		</tr>
		<?php
		$i=0;
		foreach($campaigns_info as $campaigns){
	    ?>
		<tr>
		<td>
		<?php echo ++$i?>
		</td>
		<td>
		<a href="<?php echo base_url().'sms/by_campaign?c_code='.base64_encode($campaigns->campaign_code).'&id='.base64_encode($campaigns->user_id)?>" title="<?php echo _('Campaign Details') ?>">
		<u><?php echo "#".$campaigns->campaign_code?></u>
		</a>
		
		<br>
		<small class="text-muted"><b><?php echo _("Submit On")?></b><br>
		 <?php echo date('d-M-Y H:i:s',strtotime($campaigns->do_submit)); ?>
		</small>
		</td>
		
		<td>
		<?php echo $campaigns->ac_first_name.' '.$campaigns->ac_last_name?><br>
		<?php echo "<small class='text-muted'>Country: ".$campaigns->ac_country."</small>"?>
		</td>
		
		<td><?php echo $campaigns->message ?></td>
		
		<td>
		 <?php 
		  if($campaigns->do_sent!='0000-00-00 00:00:00'){
			 echo date('d-M-Y H:i:s',strtotime($campaigns->do_sent));
		  }else{
			  echo "<span class='label label-default'><i class='fa fa-hourglass-half'></i>" . _("Waiting") . "..</span>";
		  }
		 
		 ?>
		</td>
		<td width="25%">
		    <?php 
			  $success_count=$this->sms_model->get_success_count($campaigns->campaign_code,$campaigns->user_id);
			  $participated_count=$this->sms_model->get_campaign_contacts_count($campaigns->campaign_code,$campaigns->user_id);
			  $failed_count=$this->sms_model->get_unsuccess_count($campaigns->campaign_code,$campaigns->user_id);
			?>
			  
			  <div class="row text-left">
			  <div class="col-md-4">
			    
				<a href="<?php echo base_url().'sms/by_campaign?c_code='.base64_encode($campaigns->campaign_code).'&id='.base64_encode($campaigns->user_id)?>" title="<?php echo _('Campaign Details') ?>" class="btn btn-sm btn-info">
				<i class="fa fa-align-left"></i> <?php echo _("Total")?>  <?php echo "<b>(".$participated_count.")</b>";?>
				</a>
				
			  </div>
			  <div class="col-md-2">
			   &nbsp;
			  </div>
			  <div class="col-md-6">
			     <?php 
				  if($success_count!=0){
					  $failed=$participated_count-$success_count;
				  }else{
					  if($failed_count!=0){
						  $failed=$failed_count;
					  }else{
						  $failed="0";
					  }
				  }
  
				  ?>
				  <?php echo "<span class='text-success'><i class='fa fa-check-circle'></i> success (".$success_count.") </span><br>";?>
				  <?php echo "<span class='text-danger'><i class='fa fa-exclamation-circle'></i> failed (".$failed.")</span>";?>
			  </span>
			  </div>
			  </div>
			
			 
		</td>
		<td>
		 <?php 
		   switch($campaigns->is_scheduled){
			   case '0':
			   echo "<span class='text-primary'><i class='fa fa-leaf'></i>" . _("regular") ."</span>";
			   break;
			   
			   case '1':
			   echo "<span class='text-warning'><i class='fa  fa-clock-o'></i>" . _("scheduled") ."</span>";
			   break;
		   }
		  ?>
		</td>
		<td>
		<?php 
		   switch($campaigns->cmp_status){
			   case '1':
			   echo "<span class='label label-success'><i class='fa fa-check'></i>" . _("Completed") ."</span>";
			   break;
			   
			   case '0':
			   echo "<span class='label label-warning'><i class='fa fa-hourglass-half'></i>" . _("Waiting") ."</span>";
			   break;
			   
		   }
		  ?>
		</td>
		</tr>
		<?php } ?>
</table>

