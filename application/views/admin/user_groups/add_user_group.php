<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<script type="application/javascript" src="<?php echo base_url();?>js/user_groups.js"></script>
<?php
 $user_group=$this->user_groups_model->get_single_group($this->input->get('id'));
 $total_groups=$this->user_groups_model->list_groups();
 $inactive_groups=$this->user_groups_model->list_inactive_groups();
?>

<section class="content-header">
  <h1>
	<?php echo _("Add User Group")?>
	
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>user_groups"><?php echo _("User Groups")?></a></li> <li class="active"><?php echo _("Add User Group")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-8">
			<div class="box padding_20">
            <div class="box-body">
				<div ng-app="">
                 <form role="form" ng-controller="UGController" name="userGroupForm" id="userGroupForm" >
                    <div class="form-group">
                    <label><?php echo _("Group Name")?></label> <small class="red" style="font-size:14px">*</small>
                    <input type="text" placeholder="<?php echo _('Type group name') ?>" class="form-control" ng-model="group_name" id="group_name" required="required" name="group_name">
                    </div>
                    <div class="form-group">
                    <label><?php echo _("Description")?></label>
                    <textarea placeholder="<?php echo _('Type group short description ') ?>" class="form-control" ng-model="group_desc" id="group_desc"></textarea>
					<small class="red">* <?php echo _("Mandatory field(s).")?></small>
                    </div>
					
                    <div class="form-group">
                         <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='AddIn()' >
						 <i class="fa fa-plus"></i> <?php echo _("Add")?>
                        </button>
                    </div>
					
					<div id="rsDiv"></div>
					<br>
					</form>
					</div>
				</div>
			</div>
            </div>
		    <div class="col-md-4">
		    <div class="box box-widget widget-user-2">
				<!-- Add the bg color to the header using any of the bg-* classes -->
				<div class="widget-user-header bg-aqua">
				  <div class="widget-user">
					<h3><i class="fa fa-users"></i> <?php echo _("Total Groups")?></h3>
				  </div><!-- /.widget-user-image -->
				  
				</div>
				<div class="box-footer no-padding">
				  <ul class="nav nav-stacked">
					<li><a href="#"><?php echo _("Active")?>
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_groups!=0){
						  if(count($total_groups)>10){
							  echo count($total_groups);
						  }else{
							  echo "0".count($total_groups);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
					<li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
						 if($inactive_groups!=0){
						  if(count($inactive_groups)>10){
							  echo count($inactive_groups);
						  }else{
							  echo "0".count($inactive_groups);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
				  </ul>
				</div>
			  </div>
			<ul>
			<strong><?php echo _("Suggestions")?></strong>
			<li><?php echo _("Group name is a key entity , don't leave it blank .")?> </li>
			<li><?php echo _("Description is a short details about the group , suggesting you to fill the field as well.")?> </li>
		  </ul>    
		 </div>
   </div>

</section>

<?php $this->load->view('theme/footer.php');?>

