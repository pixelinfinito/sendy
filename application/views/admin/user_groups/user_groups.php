<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
 
<script type="application/javascript" src="<?php echo base_url();?>js/user_groups.js"></script>
 
<section class="content-header">
  <h1>
	<?php echo _("User Groups")?>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("User Groups")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
	    <a href="<?php echo base_url()?>site/export/export_groups" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
			<i class="fa fa-file-excel-o"></i>
		</a>
		<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			<i class="fa fa-print"></i>
		</a>
   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>user_groups/add" class="btn btn-sm btn-primary"  title="<?php echo _('New Group') ?>">
			<i class="fa fa-plus-circle"></i> <?php echo _("New Group")?>
		</a>
   </div>
</div> 
</div>
<div class="box-body">
  <table class="table table-bordered table-striped" id="dataTables-userList">
		<thead>
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?> </th>
		<th class="col-sm-1"><?php echo _("Group Name")?> </th>
		<th class="col-sm-1"><?php echo _("Description")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		</tr>
		</thead>
		<tbody>
		<?php 
		$groups_info=$result=$this->user_groups_model->list_groups();
		$gp = 1 
		?>
		<?php if ($groups_info!=0): foreach ($groups_info as $group) : ?>

		<tr>
		<td>
		<?php echo $group->group_id?>
		</td>
		<td><?php echo $group->group_name?>
		</td>
		<td><?php echo $group->group_desc ?></td>
		<td>
		<?php 
		
		  switch($group->status){
			  case 1:
			  echo '<span class="label label-success"> ' . _('Active') . '</span>';
			  break;
			  
			  case 0:
			  echo '<span class="label label-warning"> ' . _('InActive') . '</span>';
			  break;
		  }
		?>
		</td>
		
		<td>
		<a href="user_groups/edit?id=<?php echo $group->group_id?>" title="<?php echo _('Edit') ?>" class='btn btn-xs btn-default'>
            <i class="fa fa-pencil"></i> 
        </a>
		 <?php 
		 if($group->group_name!='Administrator'){
			 ?>
			 <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delGroup('<?php echo $group->group_id?>','<?php echo $group->group_name?>')"  class='btn btn-xs btn-danger'>
				<i class='glyphicon glyphicon-trash'></i> 
			 </a>
		  <?php 
		 }else{
			 ?>
			 <a href="javascript:alert('Sorry you cant delete master group')" title="<?php echo _('Delete') ?>"  class='btn btn-xs btn-default'>
				<i class='glyphicon glyphicon-trash'></i> 
			 </a>
			 <?php
			 
		 }
		  ?>
		</td>
		</tr>
		<?php
		$gp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody>
		</table>
	</div>
</div>	
		<?php 
		 $this->load->view('modals/delete_user_group');
		?>
			
		</section>
		
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-userList').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
		
    });
</script>
		

