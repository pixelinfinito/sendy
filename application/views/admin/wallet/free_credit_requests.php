<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<script type="application/javascript" src="<?php echo base_url();?>js/members/wallet.js"></script>
<?php
 
 $total_members=$this->account_model->list_all_members();
 $inactive_members=$this->account_model->list_inactive_members();
 $total_payment_transactions=$this->payment_model->total_member_payments();
 
 $appSettings= $this->app_settings_model->get_primary_settings();
 $currency_name=$appSettings[0]->app_currency;
 
 $settingsInfo= $this->app_settings_model->get_site_settings();
 $wallet_min_val=@$settingsInfo[0]->wallet_min_val;
 $wallet_max_val=@$settingsInfo[0]->wallet_max_val;
 $wallet_string=@$settingsInfo[0]->wallet_string;
 
 $walletCode=$wallet_string.rand($wallet_min_val,$wallet_max_val);
 if($walletCode!=''){
	 $wItemCode="SYS-".$walletCode;
 }else{
	 $wItemCode="SYS-".'OCWL'.rand(500,5000);
 }
 $transCode="SYSTNS-".rand(50000,10000);
 
 
 $total_payment_transactions=$this->account_model->grandtotal_wallet_transactions();

?>

<section class="content-header">
  <h1>
	<?php echo _("Free Credit Requests")?>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>wallet"><?php echo _("Wallet Funds")?></a></li> <li class="active"><?php echo _("Deposit/Refunds Funds")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-4">
			<div class="box padding_20">
            <div class="box-body">
			
				<div ng-app="">
                 <form role="form" ng-controller="freeCreditController" name="depositMfForm" id="depositMfForm">
				   <div class="form-group">
                    
					<div class="row">
					<div class="col-md-6">
					<label><?php echo _("Wallet Token")?></label> <span class="red">*</span>
				    <input type="text" class="form-control" name="wallet_token" id="wallet_token" value="<?php echo $wItemCode;?>" readonly required="required"> 
					<small>(<?php echo _("Sytem automatic generated code")?>)</small>
					</div>
					<div class="col-md-6">
					<label><?php echo _("Transaction ID")?></label> <span class="red">*</span>
				    <input type="text" class="form-control" name="transaction_id" id="transaction_id" value="<?php echo $transCode;?>" readonly required="required"> 
					<small>(<?php echo _("Sytem automatic generated code")?>)</small>
					</div>
					</div>
					
					<input type="hidden" class="form-control" name="wallet_tamt" id="wallet_tamt"> 
					<input type="hidden" class="form-control" name="wallet_bamt" id="wallet_bamt"> 
					<input type="hidden" class="form-control" name="currency_name" id="currency_name" value="<?php echo $currency_name;?>"> 
					</div>
					
                    <div class="form-group">
                    <label><?php echo _("Member")?></label> <span class="red">*</span>
						 <select class="form-control"  id="member_id" required="required" name="member_id" onchange="javascript:loadMemberCredit(this.value,'<?php echo $currency_name;?>')">
						 <option value="">--<?php echo _("Select")?>--</option>
						 <?php 
						  foreach($total_members as $member){
							  ?>
							  <option value="<?php echo $member->user_id;?>">
							   <?php echo $member->ac_first_name.' '.$member->ac_last_name.' ('.$member->country_name.' )'?>
							  </option>
							  <?php
							  
						  }
						 ?>
						</select>
						
						<div id="member_credit"></div>
                    </div>
					<div class="form-group">
                    <label><?php echo _("Free Credit")?> <em>(<?php echo _("Amount In")?> <?php echo $currency_name?>)</em></label> <span class="red">*</span>
					<div class="row">
					
					<div class="col-md-12">
					<input type="text" class="form-control" name="deposit_amt" id="deposit_amt" placeholder="<?php echo _('E.g. 2') ?>" >
					</div>
					</div>
					</div>
					
                    <div class="form-group">
                    <label><?php echo _("Remarks")?></label>
                    <textarea placeholder="<?php echo _('Payment Remarks') ?>" class="form-control" ng-model="payment_remarks" id="payment_remarks"></textarea>
					<small class="red">* <?php echo _("Mandatory field(s).")?></small>
                    </div>
					
                    <div class="form-group">
                         <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='depositIn()' >
						  <i class="fa fa-money"></i> <?php echo _("Send Free Credits")?>
                        </button>
                    </div>
					<div id="rsDiv"></div>
					<br>
					</form>
					</div>
				</div>
			</div>
            </div>
		    <div class="col-md-8">
			 <div class="box">
			 <div class="box-header">
				<div class="row">
				   <div class="col-md-6">
				   <a href="<?php echo base_url()?>site/export/export_groups" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
					<i class="fa fa-file-excel-o"></i>
					</a>
					<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
					<i class="fa fa-print"></i>
					</a>

				   </div>
				   <div class="col-md-6 text-right">
						<?php 
						if($total_payment_transactions!=0){
						echo "<i class='fa fa-google-wallet'></i> " . _('Total Transactions') . " : <b>".$currency_name.' '.round($total_payment_transactions[0]->ad_payment,3)."</b>";
						}else{
						echo "<i class='fa fa-google-wallet'></i> " . _('Total Transactions') . " : <b>".$currency_name." 00.00"."</b>";
						}
						?>
				   </div>
				</div> 
			  </div>
			 <div class="box-body">
			    <?php 
				$requests_info=$this->wallet_model->check_all_freecredit_requests();
				?>
		       <!--//// load free credit requests ////-->
					<table class="table table-bordered table-striped" id="dataTables-fCreditRequests">
					<thead ><!-- Table head -->
					<tr>
					<th class="col-sm-1"><?php echo _("Id")?></th>
					<th class="col-sm-1"><?php echo _("Request By")?></th>
					<th class="col-sm-1"><?php echo _("Country")?></th>
					<th class="col-sm-1"><?php echo _("Request Comments")?></th>
					<th class="col-sm-1"><?php echo _("Requested On")?> </th>
					<th class="col-sm-1"><?php echo _("Approved Credit")?></th>
					<th class="col-sm-1"><?php echo _("Approval Status")?></th>
					</tr>
					</thead><!-- / Table head -->
					<tbody>
					<?php 
					
					$gp = 1 
					?>
					<?php if ($requests_info!=0): foreach ($requests_info as $request) : ?>

					<tr>
					<td>
					<?php echo $request->request_id?>
					</td>
					<td>
					<?php echo $request->member_name?>
					</td>
					<td>
					<?php echo $request->ac_country?> 
					</td>
					<td>
					<?php echo $request->request_comments?> 
					</td>
					<td>
					<?php 
					echo date('d-M-Y H:i:s',strtotime($request->request_datetime)); 
					?>
					</td>
					<td>
						 <?php 
						  if($request->approved_funds!=0){
							  echo $currency_name.' '.$request->approved_funds;
						  }else{
							  echo 'None';
						  }
						 ?>
						 
					 </td>
					 
					<td>
						 <?php 
						
						  switch($request->arrpove_status){
							  case 1:
							  ?>
							  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i> <?php echo _("Approved")?></div>
							  <?php
							  break;
							  
							  case 0:
							  ?>
							  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i> <?php echo _("Rejected")?></div>
							  <?php
							  break;
							  
							  case 2:
							  ?>
							  <div class="ad_unpublish_btn"> <i class="fa fa-hourglass-half"></i> <?php echo _("Pending")?></div>
							  <?php
							  break;
						  }
						 ?>
						 
					 </td>
					 
					</tr>
				<?php
				$gp++;
				endforeach;
				?>
				<?php else : ?>
				<td colspan="6" class="text-center">
				<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
				</td>
				<?php endif; ?>
				</tbody>
				</table>
			   <!--//// end ///---->
				</div>
		    </div>
			</div>
   </div>

</section>

<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-fCreditRequests').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>

