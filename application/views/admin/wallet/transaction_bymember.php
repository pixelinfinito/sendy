<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

      
<?php
 
$paymentSettings=$this->app_settings_model->get_payment_settings();
$settingsInfo= $this->app_settings_model->get_site_settings();

$total_payment_transactions=$this->account_model->grandtotal_wallet_transactions();
$appSettings= $this->app_settings_model->get_primary_settings();
$currency_name=$appSettings[0]->app_currency;

$total_members=$this->account_model->list_all_members();
?>
<script type="application/javascript" src="<?php echo base_url()?>js/members/payments.js"></script>
<script type="text/javascript">
 function printInvoice(pid,uid){
    var URL = "<?php echo base_url()?>export/invoice/"+pid+"/"+uid;
    var W = window.open(URL);   
    W.window.print(); 
 }
</script>
<section class="content-header">
  <h1>
	<?php echo _("Wallet Transactions")?>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Wallet Transactions")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_wallet_bymember/<?php echo str_replace('=','',$this->input->get('id'))?>" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		<?php 
		if($total_payment_transactions!=0){
		echo "<i class='fa fa-google-wallet'></i> " . _('Total Transactions') . " : <b>".$currency_name.' '.round($total_payment_transactions[0]->ad_payment,3)."</b>";
		}else{
		echo "<i class='fa fa-google-wallet'></i> " . _('Total Transactions') . " : <b>".$currency_name." 00.00"."</b>";
		}
		?>
   </div>
</div> 
</div>
	<div class="box-body">
	  <form role="form" method="get">
	        <div class="row"> 
			<div class="col-md-2">
			  <label><?php echo _("Choose Member")?> </label><span class="red">*</span>
			</div>
            <div class="col-md-6">			
			<select class="form-control"  id="id" required="required" name="id">
			 <option value="">--<?php echo _("Select")?>--</option>
			 <?php 
			  foreach($total_members as $member){
				  ?>
				  <option value="<?php echo base64_encode($member->user_id)?>">
				   <?php echo $member->ac_first_name.' '.$member->ac_last_name.' ('.$member->country_name.' )'?>
				  </option>
				  <?php
				  
			  }
			 ?>
			</select>
			</div>
			
			<div class="col-md-3">
			  <button class="btn btn-info pull-left" type="submit">
			   <i class="fa fa-check"></i> <?php echo _("Check")?>
              </button>
			</div>
			</div>
			<br>
			
			<div class="row"> 
			<div class="col-md-2">
			 &nbsp;
			</div>
			<div class="col-md-6">
			<i class="fa fa-info-circle"></i> <?php echo _("Check wallet transaction details followed by member.")?>
			</div>
			<div class="col-md-3">
			 &nbsp;
			</div>
			</div>
			<hr>
			
	  </form>
	 <?php 
	   $user_id=base64_decode($this->input->get('id'));
	   if($user_id!=''){
     ?>	 
   
	
	<table class="table table-bordered table-striped" id="dataTables-walletHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Ref.No")?></th>
		<th class="col-sm-1"><?php echo _("Amount")?></th>
		<th class="col-sm-1"><?php echo _("Date Of Payment")?> </th>
		<th class="col-sm-1"><?php echo _("Payment/Invoice Ref.No")?></th>
		<th class="col-sm-2"><?php echo _("Remarks")?> </th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>

		</tr>
		<tbody>
		<?php 
		
		$wallet_history= $this->account_model->get_wallet_transaction_byuser($user_id);
		$gp = 1 
		?>
		<?php 
		
		$gp = 1 
		?>
		<?php if ($wallet_history!=0): foreach ($wallet_history as $wallet) : ?>

		<tr>
		<td>
		<?php echo $wallet->payment_id?>
		</td>
		<td>
		<?php echo $wallet->ad_reference?>
		</td>
		<td>
		<?php echo $currency_name.' '.$wallet->ad_payment?> 
		</td>
		
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($wallet->date_of_payment)); 
		?>
		</td>
		<td>
		   <?php echo $wallet->payment_reference ?>
		</td>
		<td>
		<?php 
		  echo $wallet->payment_remarks
		?>
		</td>
		<td>
		     <?php 
			  switch($wallet->payment_status){
				  case '1':
				  ?>
				  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i> <?php echo _("PAID")?></div>
				  <?php
				  break;
				  
				  case '0':
				  ?>
				  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i> <?php echo _("Failed")?></div>
				  <?php
				  break;
			  }
			 ?>
             
         </td>
		 <td>
		     <?php 
			  switch($wallet->payment_status){
				  case '1':
				  ?>
				  <a href="<?php echo base_url().'export/invoice/'.str_replace('=','',base64_encode($wallet->payment_id)).'w/'.str_replace('=','',base64_encode($wallet->user_id))?>" title="<?php echo _('Download Invoice') ?>" class="btn btn-xs btn-info">
				  <i class="fa fa-download"></i>
				  </a>
				  
				  <a href="javascript:void(0)" onclick="javascript:printInvoice('<?php echo str_replace('=','',base64_encode($wallet->payment_id))."w"?>','<?php echo str_replace('=','',base64_encode($wallet->user_id))?>')" title="<?php echo _('Print Invoice') ?>" class="btn btn-xs btn-default">
				  <i class="fa fa-print"></i>
				  </a>
				  <?php
				  break;
				  
				  case '0':
				  echo '-';
				  break;
			  }
			 ?>
             
         </td>
		
		</tr>
	<?php
	$gp++;
	endforeach;
	?>
	<?php else : ?>
	<td colspan="6" class="text-center">
	<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
	</td>
	<?php endif; ?>
	</tbody>
	
	</table>
	
	
	<?php }?>
	</div>
 </div>	
	
</section>

		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-walletHistory').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>
		
