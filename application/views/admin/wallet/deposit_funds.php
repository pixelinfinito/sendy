<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<script type="application/javascript" src="<?php echo base_url();?>js/members/wallet.js"></script>
<?php
 
 $total_members=$this->account_model->list_all_members();
 $inactive_members=$this->account_model->list_inactive_members();
 $total_payment_transactions=$this->payment_model->total_member_payments();
 
 $appSettings= $this->app_settings_model->get_primary_settings();
 $currency_name=$appSettings[0]->app_currency;
 
 $settingsInfo= $this->app_settings_model->get_site_settings();
 
 $wallet_min_val=@$settingsInfo[0]->wallet_min_val;
 $wallet_max_val=@$settingsInfo[0]->wallet_max_val;
 $wallet_string=@$settingsInfo[0]->wallet_string;
 
 $walletCode=$wallet_string.rand($wallet_min_val,$wallet_max_val);
 if($walletCode!=''){
	 $wItemCode="SYS-".$walletCode;
 }else{
	 $wItemCode="SYS-".'OCWL'.rand(500,5000);
 }
 $transCode="SYSTNS-".rand(50000,10000);
?>

<section class="content-header">
  <h1>
	<?php echo _("Deposit/Refunds Funds")?>
	
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>wallet"><?php echo _("Wallet Funds")?></a></li> <li class="active"><?php echo _("Deposit/Refunds Funds")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			
				<div ng-app="">
                 <form role="form" ng-controller="depositFundsController" name="depositMfForm" id="depositMfForm">
				   <div class="form-group">
                    
					<div class="row">
					<div class="col-md-6">
					<label><?php echo _("Wallet Token")?></label> <span class="red">*</span>
				    <input type="text" class="form-control" name="wallet_token" id="wallet_token" value="<?php echo $wItemCode;?>" readonly required="required"> 
					<small>(<?php echo _("Sytem automatic generated code")?>)</small>
					</div>
					<div class="col-md-6">
					<label><?php echo _("Transaction ID")?></label> <span class="red">*</span>
				    <input type="text" class="form-control" name="transaction_id" id="transaction_id" value="<?php echo $transCode;?>" readonly required="required"> 
					<small>(<?php echo _("Sytem automatic generated code")?>)</small>
					</div>
					</div>
					
					<input type="hidden" class="form-control" name="wallet_tamt" id="wallet_tamt"> 
					<input type="hidden" class="form-control" name="wallet_bamt" id="wallet_bamt"> 
					<input type="hidden" class="form-control" name="currency_name" id="currency_name" value="<?php echo $currency_name;?>"> 
					</div>
					
                    <div class="form-group">
                    <label><?php echo _("Member")?></label> <span class="red">*</span>
						 <select class="form-control"  id="member_id" required="required" name="member_id" onchange="javascript:loadMemberCredit(this.value,'<?php echo $currency_name;?>')">
						 <option value="">--<?php echo _("Select")?>--</option>
						 <?php 
						  foreach($total_members as $member){
							  ?>
							  <option value="<?php echo $member->user_id;?>">
							   <?php echo $member->ac_first_name.' '.$member->ac_last_name.' ('.$member->country_name.' )'?>
							  </option>
							  <?php
							  
						  }
						 ?>
						</select>
						
						<div id="member_credit"></div>
                    </div>
					<div class="form-group">
                    <label><?php echo _("Deposit/Refund Amount")?></label> <span class="red">*</span>
					<div class="row">
					<div class="col-md-3">
					  <select class="form-control" name="c_code" id="c_code">
					   <option value="<?php echo $currency_name?>"><?php echo $currency_name?></option>
					  </select>
					</div>
					<div class="col-md-9">
					<input type="text" class="form-control" name="deposit_amt" id="deposit_amt" placeholder="<?php echo _('E.g. 50') ?>" >
					</div>
					</div>
					</div>
					
                    <div class="form-group">
                    <label><?php echo _("Remarks")?></label>
                    <textarea placeholder="<?php echo _('Payment Remarks') ?>" class="form-control" ng-model="payment_remarks" id="payment_remarks"></textarea>
					<small class="red">* <?php echo _("Mandatory field(s).")?></small>
                    </div>
					
                    <div class="form-group">
                         <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='depositIn()'>
						 <i class="fa fa-dollar"></i> <?php echo _("Deposit")?>
                        </button>
                    </div>
					<div id="rsDiv"></div>
					<br>
					</form>
					</div>
				</div>
			</div>
            </div>
		    <div class="col-md-4">
			   
		      <?php 
				if($total_payment_transactions!=0){
				?>
				<div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-teal">
                  <div class="widget-user">
                    <h3><small><?php echo _("Total Payments")?>: </small> <?php echo $currency_name.' '.round($total_payment_transactions[0]->c2o_total_amount,2)?></h3>
                  </div><!-- /.widget-user-image -->
                </div>
				</div>
				<?php
				}else{
				  ?>
				<div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-warning">
                  <div class="widget-user">
                    <h3><small><?php echo _("Total Payments")?>: </small> <?php echo $currency_name.' 00.00 '?></h3>
                  </div><!-- /.widget-user-image -->
                </div>
				</div>
				  <?php
				}
			 ?>
			 
			  <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-maroon">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i> <?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?>
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Fill some remarks for member's understanding.")?></li>
				 <li><?php echo _("Cross check the funds before you refund/deposit back (its highly important).")?></li>
				</ul>
		    </div>
   </div>

</section>

<?php $this->load->view('theme/footer.php');?>

