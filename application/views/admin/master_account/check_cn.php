<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<?php
$allCountries= $this->app_settings_model->get_merge_caller_numbers();
?>

<section class="content-header">
  <h1>
	<?php echo _("Caller Number Lookup")?> <small><?php echo _("with telecom service provider (twilio")?>)</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>caller_numbers/cn_requests"><?php echo _("Caller Numbers")?></a></li> <li class="active"><?php echo _("Caller Number Lookup")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-4">
			<div class="box box-widget widget-user-2">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header bg-teal">
			  <div class="widget-user">
				<img src="<?php echo base_url()?>theme4.0/admin/images/twilio_png.png" width="250"/>
			  </div><!-- /.widget-user-image -->
			</div>
			</div>
			<div class="box padding_20">
            <div class="box-body">
			    <h4><i class="fa fa-search-plus"></i><?php echo _("Lookup by Country")?></h4> <hr>
				 <form role="form" method="get">
				 <div class="form-group">
                 <label><?php echo _("Country")?></label> <span class="red">*</span>
				 <select class="form-control" id="cn_country" name="cn_country" required="required">
				   <option value="">--<?php echo _("Select")?>--</option>
				   <?php 
				    foreach($allCountries as $country){
						?>
						<option value="<?php echo $country->cn_country?>">
						<?php echo $country->country_name;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				  </div>
					
				<div class="form-group">
				<label><?php echo _("Number by Prefix")?></label>
				<input type="text" class="form-control" name="cn_number" id="cn_number" placeholder="<?php echo _('E.g. 673') ?>" >
				</div>
			
				<div class="form-group">
					 <button class="btn btn-primary pull-left m-t-n-xs" type="submit">
					  <i class="fa fa-mobile"></i><?php echo _("Lookup")?></button>
				</div>
				<br>
				</form>
				</div>
			</div>
			<br><br>
			<span class="text-muted"><?php echo _("To check brief utility please logon to twilio service account")?></span>
			<a href="https://www.twilio.com/login" target="_blank"> 
			<i class="fa fa-caret-right"></i><?php echo _("Access twilio account")?></a>
            </div>
		    <div class="col-md-8">
			 <div class="box">
			 <div class="box-header">
				<div class="row">
				   <div class="col-md-6">
				    <h4><?php echo _("Caller Numbers")?></h4>
				   </div>
				   <div class="col-md-6 text-right">
				        <a href="<?php echo base_url()?>master_account/check_cn" class="btn btn-sm btn-primary">
						<i class="fa fa-rotate-left"></i><?php echo _("Reload")?></a>
						
						<a href="<?php echo base_url()?>master_account/service_utility" class="btn btn-sm btn-info">
						<i class="fa fa-search"></i><?php echo _("Service Utility")?></a>
				   </div>
				</div> 
			  </div>
			 <div class="box-body">
			    <?php 
				$cn_country=$this->input->get('cn_country');
				$cn_number=$this->input->get('cn_number');
				
				
				try{
					
				$appSettings=$this->app_settings_model->get_app_settings();
				$smsSettings=$this->app_settings_model->get_sms_settings();
				$account_sid = $smsSettings[0]->twilio_accid;
				//Put your Twilio Auth Key here:
				$auth_token = $smsSettings[0]->twilio_authtoken;
				// Put your Twilio Outgoing SMS enabled number here:
				include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
				$client = new Services_Twilio($account_sid, $auth_token); 
				
				if($cn_country!=''){
					if($cn_number!=''){
						$numbers = $client->account->available_phone_numbers->getList($cn_country, 'Mobile', array(
						"Contains" => $cn_number));
					}else{
						$numbers = $client->account->available_phone_numbers->getList($cn_country, 'Mobile', array());
					}
	            }else{
						$numbers = $client->account->available_phone_numbers->getList('GB','Mobile', array());
				}
				
				
				?>
		          	<table class="table table-bordered table-striped" id="dataTables-cnCheck">
					<thead ><!-- Table head -->
					<tr>
					<th class="col-sm-1"><?php echo _("Id")?></th>
					<th class="col-sm-1"><?php echo _("Caller Number")?></th>
					<th class="col-sm-1"><?php echo _("Friendly Name")?></th>
					<th class="col-sm-1"><?php echo _("Is Assigned ?")?></th>
					<th class="col-sm-1"><?php echo _("No.Members (been assigned")?>)</th>
					
					</tr>
					</thead><!-- / Table head -->
					<tbody>
					<?php 
					
					$gp = 1;
					foreach ($numbers->available_phone_numbers as $number) 
					{
					?>

					<tr>
					<td>
					<?php echo $gp?>
					</td>
					<td>
					<?php echo $number->phone_number?>
					</td>
					<td>
					<?php echo $number->friendly_name?> 
					</td>
					<td>
					<?php 
					 $cn_check=$this->caller_number_model->cn_local_check($number->phone_number);
					 if($cn_check){
					     echo '<span class="label label-danger">' ._("Yes") . '</span>';
					 }else{
						 echo '<span class="label label-success">' ._("No") . '</span>';
					 }
					?> 
					</td>
					
					<td>
					<?php 
					 if($cn_check!=0){
						 echo count($cn_check);
					 }else{
						 echo "0";
					 }
					?> 
					</td>
					
				   <?php
				    $gp++;
					}
					
					?>
				   
				</tbody>
				</table>
			   <?php 
			    }
				catch (Services_Twilio_RestException $e) {
					echo "<div class='row'>
					<div class='text-center col-md-12'>
					<h4 class='text-danger'> <i class='fa  fa-exclamation-triangle'></i>"
						. _('Data not available') . "</h4>
					<span class='text-muted'>" . sprintf(_('No caller number services offering to [%s] this country.'), $cn_country) . " </span>
					<br><br></div>
					</div>";
				}
				
				?>
				</div>
		    </div>
			</div>
   </div>

</section>

<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-cnCheck').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>

