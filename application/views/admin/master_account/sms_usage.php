<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<?php
 
$allCountries= $this->app_settings_model->get_merge_caller_numbers();
?>
<script src="<?php echo base_url()?>assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<link href="<?php echo base_url()?>assets/bootstrap-datepicker/css/bootstrap-datepicker3.min" />

<script>

$(function(){
	    $('#from_date').datepicker({
		});
		$('#to_date').datepicker({
		});
});
</script>

<section class="content-header">
  <h1>
	<?php echo _("Service Utility")?> <small><?php echo _("with telecom service provider (twilio")?>)</small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li><a href="<?php echo base_url()?>caller_numbers/cn_requests"><?php echo _("Caller Numbers")?></a></li> <li class="active"><?php echo _("Service Utility")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-4">
			<div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-teal">
                  <div class="widget-user">
                    <img src="<?php echo base_url()?>theme4.0/admin/images/twilio_png.png" width="250"/>
                  </div><!-- /.widget-user-image -->
                </div>
				</div>
				
			<div class="box padding_20">
            <div class="box-body">
			   <h4><i class="fa fa-calendar"></i><?php echo _("Search by Date")?></h4> <hr>
			    <form role="form" method="get">
				  <div class="form-group">
					  <label><?php echo _("Start Date")?></label> <span class="red">*</span>
					  <input type="text" id="from_date" name="from_date" class="form-control" placeholder="<?php echo _('e.g. 01/28/2015') ?>" required/>
				  </div>
					
				  <div class="form-group">
					  <label><?php echo _("End Date")?></label> <span class="red">*</span>
					  <input type="text" id="to_date" name="to_date" class="form-control" placeholder="<?php echo _('e.g. 01/30/2015') ?>" required/>
				  </div>
				  
				   <div class="form-group">
					  <label><?php echo _("Service Call")?></label>
				  </div>
				   <div class="form-group">
					  <input type="radio" id="sms" name="token" value="sms" checked="checked"/><?php echo _("SMS")?></div>
				  <div class="form-group">
					  <input type="radio" id="sms" name="token" value="sms-inbound"/><?php echo _("SMS Inbound")?></div>
				  <div class="form-group">
					 <input type="radio" id="calls" name="token" value="calls"/><?php echo _("Calls")?></div>
				  <div class="form-group">
					  <input type="radio" id="calls_inbound" name="token" value="calls-inbound"/><?php echo _("Calls Inbound")?></div>
				  <div class="form-group">
					<input type="radio" id="short_codes" name="token" value="sms-inbound-shortcode"/><?php echo _("Shortcode")?></div>
				
				
				  <div class="form-group">
					<button class="btn btn-danger pull-left m-t-n-xs" type="submit">
					 <i class="fa fa-search"></i><?php echo _("Search")?></button>
					
				  </div>
				  <br>
				</form>
				
			</div>
			</div>
			<br><br>
			<span class="text-muted"><?php echo _("To check brief utility please logon to twilio service account")?></span>
			<a href="https://www.twilio.com/login" target="_blank"> 
			<i class="fa fa-caret-right"></i><?php echo _("Access twilio account")?></a>
			</div>
		    <div class="col-md-8">
			 <div class="box">
			 <div class="box-header">
				<div class="row">
				   <div class="col-md-6">
				    <h4><?php echo _("Summary")?></h4>
				   </div>
				   <div class="col-md-6 text-right">
				        <a href="<?php echo base_url()?>master_account/service_utility" class="btn btn-sm btn-info">
						<i class="fa  fa-rotate-left"></i><?php echo _("Reload")?></a>
						
						<a href="<?php echo base_url()?>master_account/cn_check" class="btn btn-sm btn-primary">
						<i class="fa fa-search"></i><?php echo _("Caller Number Lookup")?></a>
				   </div>
				</div> 
			  </div>
			 <div class="box-body">
			    <?php 
					
				$from_date=date('Y-m-d',strtotime($this->input->get('from_date')));
				$to_date=date('Y-m-d',strtotime($this->input->get('to_date')));
				$token=$this->input->get('token');
				$appSettings=$this->app_settings_model->get_app_settings();
				$smsSettings=$this->app_settings_model->get_sms_settings();
				$account_sid = $smsSettings[0]->twilio_accid;
				//Put your Twilio Auth Key here:
				$auth_token = $smsSettings[0]->twilio_authtoken;
				// Put your Twilio Outgoing SMS enabled number here:
				include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
				$client = new Services_Twilio($account_sid, $auth_token); 
				
				try{
				if($from_date=='1970-01-01' && $to_date=='1970-01-01'){
				
				?>
		     		<table class="table table-bordered table-striped" id="dataTables-sUtility">
					<thead>
					<tr>
					<th class="col-sm-1"><?php echo _("Id")?></th>
					<th class="col-sm-1"><?php echo _("Category")?></th>
					<th class="col-sm-1"><?php echo _("Usage Count")?></th>
					<th class="col-sm-1"><?php echo _("Price Unit")?></th>
					<th class="col-sm-1"><?php echo _("Price")?></th>
					<th class="col-sm-1"><?php echo _("Description")?></th>
					</tr>
					</thead><!-- / Table head -->
					<tbody>
					<?php 
					$gp = 1;
						
						foreach ($client->account->usage_records->last_month  as $record) 
						{
						?>
							<tr>
							<td>
							<?php echo $gp?>
							</td>
							<td>
							<?php echo $record->category?>
							</td>
							<td>
							<?php echo $record->count?> 
							</td>
							<td>
							<?php echo $record->price_unit?> 
							</td>
							
							<td>
							<?php echo $record->price?> 
							</td>
							
							<td>
							<?php echo $record->description?> 
							</td>
							</tr>
						   <?php
							$gp++;
						}
					?>
				   
				</tbody>
				</table>
			   <?php 
			    }
				else {
					?>
					<table class="table table-bordered table-striped" id="dataTables-sUtility">
					<thead>
					<tr>
					<th class="col-sm-1"><?php echo _("Id")?></th>
					<th class="col-sm-1"><?php echo _("Category")?></th>
					<th class="col-sm-1"><?php echo _("Usage Count")?></th>
					<th class="col-sm-1"><?php echo _("Price Unit")?></th>
					<th class="col-sm-1"><?php echo _("Price")?></th>
					<th class="col-sm-1"><?php echo _("Description")?></th>
					</tr>
					</thead><!-- / Table head -->
					<tbody>
					<?php 
					$gp = 1;
						
						foreach ($client->account->usage_records->getIterator(0, 50, array("Category" =>$token,"StartDate" =>$from_date,"EndDate" =>$to_date)) as $record) 
						{
						?>
							<tr>
							<td>
							<?php echo $gp?>
							</td>
							<td>
							<?php echo $record->category?>
							</td>
							<td>
							<?php echo $record->count?> 
							</td>
							<td>
							<?php echo $record->price_unit?> 
							</td>
							
							<td>
							<?php echo $record->price?> 
							</td>
							
							<td>
							<?php echo $record->description?> 
							</td>
							</tr>
						   <?php
							$gp++;
						}
					?>
				   
				</tbody>
				</table>
					<?php
				}
				}catch (Services_Twilio_RestException $e) {
					echo "<div class='row'>
					<div class='text-center col-md-12'>
					<h4 class='text-danger'> <i class='fa  fa-exclamation-triangle'></i> 
					" . _('Data not available') . "  </h4>
					<span class='text-muted'>" . _('Sorry for the inconvience, try again.') . " </span>
					<br><br></div>
					</div>";
				}
				
				?>
				</div>
		    </div>
			</div>
   </div>
</section>

<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-sUtility').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>

