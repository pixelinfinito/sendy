<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

<?php
$allCountries= $this->countries_model->list_countries();
$visitors=$this->countries_model->list_visitors();
$visitors_count=$this->countries_model->list_visitors_count();

?>
<script src="<?php echo base_url()?>assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<link href="<?php echo base_url()?>assets/bootstrap-datepicker/css/bootstrap-datepicker3.min" />

<script>

$(function(){
	    $('#search_date').datepicker({
    });
});
</script>
<section class="content-header">
  <h1>
    <?php echo _("Visitors")?> <small><?php echo _("around the world")?></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Visitors")?></li>
  </ol>
</section>

 <section class="content">
		<div class="row">
			<div class="col-md-4">
			<div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-primary text-center" style="padding:5px">
				      
	
				  <?php
					 if($visitors_count!=0){
						 echo "<h3> 
						 <i class='fa  fa-map-marker'></i>
						 Total  ".$visitors_count."
						 </h3>";
					 }else{
						 echo "<span class='text-muted'> " . _('Total Visitors') . " (0)</span>";
					 }
					?>
			      
                </div>
				</div>
				
			<div class="box padding_20">
            <div class="box-body">
			     <h4><i class="fa fa-search"></i> <?php echo _("Search")?> </h4><hr>
				 <form role="form" method="get">
				 <div class="form-group">
                 <label><?php echo _("Country")?></label> <span class="red">*</span>
				 <select class="form-control" id="cn_country" name="cn_country" required="required">
				   <option value="">--<?php echo _("Select")?>--</option>
				   <?php 
				    foreach($allCountries as $country){
						?>
						<option value="<?php echo $country->country_code?>">
						<?php echo $country->country_name;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				  </div>
					
				
				  <div class="form-group">
					  <label><?php echo _("By Date")?></label>
					  <input type="text" id="search_date" name="search_date" class="form-control" placeholder="<?php echo _('e.g. 07/29/2015') ?>"/>
				  </div>
					
			
				<div class="form-group">
					 <button class="btn btn-success pull-left m-t-n-xs" type="submit">
					  <i class="fa fa-globe"></i> <?php echo _("Search")?>
					</button>
				</div>
				<br>
				</form>
					
				</div>
			</div>
            </div>
		    <div class="col-md-8">
			 <div class="box">
			 <div class="box-header">
				<div class="row">
				   <div class="col-md-6">
				     
					<h4><?php echo _("Summary")?></h4>
				   </div>
				   <div class="col-md-6 text-right">
				       <a href="<?php echo base_url()?>visitors" class="btn btn-sm btn-primary">
						<i class="fa fa-rotate-left"></i> <?php echo _("Reload")?>
						</a>
						
						
						<a href="<?php echo base_url()?>master_account/service_utility" class="btn btn-sm btn-info">
						<i class="fa fa-search"></i> <?php echo _("Service Utility")?>
						</a>
				   </div>
				</div> 
			  </div>
			 <div class="box-body">
			    <?php 
				if($this->input->get('cn_country')==''){
					$visitors_info=$this->visitors_model->list_all_visitors();	
				}else{
					
					$country=$this->input->get('cn_country');
					$date=$this->input->get('search_date');
					$visitors_info=$this->visitors_model->list_all_visitors_byparams($country,$date);
				}
				
				if($visitors_info!=0)
				{
				?>
		       <!--//// load free credit requests ////-->
					<table class="table table-bordered table-striped" id="dataTables-visitors">
					<thead ><!-- Table head -->
					<tr>
					<th class="col-sm-1"><?php echo _("Id")?></th>
					<th class="col-sm-1"><?php echo _("IP Address")?></th>
					<th class="col-sm-1"><?php echo _("Country Code")?></th>
					<th class="col-sm-1"><?php echo _("City")?></th>
					<th class="col-sm-1"><?php echo _("Latitude")?></th>
					<th class="col-sm-1"><?php echo _("Longitude")?></th>
					<th class="col-sm-1"><?php echo _("Visited On")?></th>
					
					</tr>
					</thead><!-- / Table head -->
					<tbody>
					<?php 
					
					$gp = 1;
					foreach ($visitors_info as $visitor) 
					{
					?>

					<tr>
					<td>
					<?php echo $visitor->visit_id?>
					</td>
					<td>
					<?php echo $visitor->remote_address?>
					</td>
					<td>
					<?php echo $visitor->remote_country?> 
					</td>
					<td>
					<?php echo $visitor->remote_city?> 
					</td>
					
					<td>
					<?php echo $visitor->geo_latitude?> 
					</td>
					
					<td>
					<?php echo $visitor->geo_longitude?> 
					</td>
					
					<td>
					<?php echo date('d-M-Y H:i:s',strtotime($visitor->visit_on))?> 
					</td>
					
				   <?php
				    $gp++;
					}
					
					?>
				   
				</tbody>
				</table>
			   <?php 
			    }
				else{
					echo "<div class='row'>
					<div class='text-center col-md-12'>
					<h4 class='text-danger'> <i class='fa fa-info-circle'></i> 
					" ._('No Information is available') . " </h4>
					<span class='text-muted'>" ._('Sorry no visitors found yet.') . "</span>
					<br><br></div>
					</div>";
				}
				?>
				</div>
		    </div>
			</div>
   </div>

</section>

<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-visitors').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>

