<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
 
<script type="application/javascript" src="<?php echo base_url()?>js/sms_campaign/sms_backend.js"></script>
<section class="content-header">
  <h1><?php echo _("Scheduled Campaigns")?></h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i><?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Scheduled Campaigns")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_all_scheduled_campaigns" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>campaigns/all" class="btn btn-sm btn-primary"  title="<?php echo _('Scheduled Campaigns') ?>">
		 <i class="fa fa-send-o"></i>&nbsp; <?php echo _("All Campaigns")?>
		</a>
   </div>
</div> 
</div>
<div class="box-body">


 <table class="table table-bordered table-striped" id="dataTables-cSchedules">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Campaign Code")?></th>
		<th class="col-sm-1"><?php echo _("Campaign By")?></th>
		<th class="col-sm-1"><?php echo _("Message")?></th>
		<th class="col-sm-1"><?php echo _("Submit Datetime")?></th>
		<th class="col-sm-1"><?php echo _("Scheduled Datetime")?></th>
		<th class="col-sm-1"><?php echo _("Campaign Summary")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$schedules_info=$this->sms_model->get_all_scheduled_campaigns();
		$key = 1 
		?>
		<?php if ($schedules_info!=0): foreach ($schedules_info as $schedule) : ?>

		<tr>
		<td>
		<?php echo $schedule->campaign_id?>
		</td>
		
		<td>
		<a href="<?php echo base_url().'sms/by_campaign?c_code='.base64_encode($schedule->campaign_code).'&id='.base64_encode($schedule->user_id)?>" title="<?php echo _('Campaign Details') ?>">
		<u><?php echo "#".$schedule->campaign_code?></u>
		</a>
		</td>
		
		<td>
		<?php echo $schedule->ac_first_name.' '.$schedule->ac_last_name?><br>
		<?php echo "<small class='text-muted'>Country: ".$schedule->ac_country."</small>"?>
		</td>
		
		<td>
		<?php echo $schedule->message ?>
		</td>
		
		<td>
		 <?php echo date('d-M-Y H:i:s',strtotime($schedule->do_submit)); ?>
		</td>
		
		<td>
		<?php 
		   echo date('d-M-Y H:i:s',strtotime($schedule->scheduled_datetime)); 
		?>
		</td>
		
		<td width="25%">
		    <?php 
			  $success_count=$this->sms_model->get_success_count($schedule->campaign_code,$schedule->user_id);
			  $participated_count=$this->sms_model->get_campaign_contacts_count($schedule->campaign_code,$schedule->user_id);
			  $failed_count=$this->sms_model->get_unsuccess_count($schedule->campaign_code,$schedule->user_id);
			?>
			  
			  <div class="row text-left">
			  <div class="col-md-4">
			    
				<a href="<?php echo base_url().'sms/by_campaign?c_code='.base64_encode($schedule->campaign_code).'&id='.base64_encode($schedule->user_id)?>" title="<?php echo _('Campaign Details') ?>" class="btn btn-sm btn-info">
				<i class="fa fa-align-left"></i> <?php echo _("Total")?>  <?php echo "<b>(".$participated_count.")</b>";?>
				</a>

			  </div>
			  <div class="col-md-2">
			   &nbsp;
			  </div>
			  <div class="col-md-6">
			     <?php 
				  if($success_count!=0){
					  $failed=$participated_count-$success_count;
				  }else{
					  if($failed_count!=0){
						  $failed=$failed_count;
					  }else{
						  $failed="0";
					  }
				  } 
				  ?>
				  <?php echo "<span class='text-success'><i class='fa fa-check-circle'></i> success (".$success_count.")</span><br>";?>
				  <?php echo "<span class='text-danger'><i class='fa fa-exclamation-circle'></i> failed (".$failed.")</span>";?>
			  </span>
			  </div>
			  </div>
			
			 
		</td>
		
		<td>
		  <?php 
		   switch($schedule->cmp_status){
			   case 1:
			   echo "<span class='label label-success'><i class='fa fa-check'></i>" . _("Completed") . "</span>";
			   break;
			   
			   case 0:
			   echo "<span class='label label-warning'><i class='fa fa-hourglass-half'></i>" . _("Waiting") ."</span>";
			   break;
		   }
		  ?>
		</td>
		
		<td>
		<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delSchedules('<?php echo $schedule->campaign_code?>','<?php echo $schedule->user_id?>')" class="text-danger">
		<i class='glyphicon glyphicon-trash'></i> 
		</a>
		</td>

		</tr>
		<?php
		$key++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="8" class="text-center">
		  <h4 class="text-muted"><i class="fa fa-info-circle"></i><?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody>
		</table>
		
		
	</div>
 </div>	
			
	<?php 
	$this->load->view('site/modals/campaign/sms/delete_schedule');
	?>
		
					
</section>
		
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-cSchedules').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>
		
