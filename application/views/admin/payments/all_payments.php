<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>

      
<?php
 
$paymentSettings=$this->app_settings_model->get_payment_settings();
$settingsInfo= $this->app_settings_model->get_site_settings();

$paymentInfo= $this->payment_model->all_member_payments();
$total_payment_transactions=$this->payment_model->total_member_payments();
$appSettings= $this->app_settings_model->get_primary_settings();
$currency_name=$appSettings[0]->app_currency;

?>
<script type="application/javascript" src="<?php echo base_url()?>js/members/payments.js"></script>
 
<script type="text/javascript">
 function printInvoice(pid,uid){
    var URL = "<?php echo base_url()?>export/payment_invoice/"+pid+"/"+uid;
    var W = window.open(URL);   
    W.window.print(); 
 }
</script>

<section class="content-header">
  <h1><?php echo _("All Payments")?> </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("All Payments")?></li>
  </ol>
</section>

<section class="content">
<div class="box">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_all_payments" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		<?php 
		if($total_payment_transactions!=0){
		echo "<i class='fa fa-shopping-cart'></i> " . _("Total Payments") . ": <b>".$currency_name.' '.$total_payment_transactions[0]->c2o_total_amount."</b>";
		}else{
		echo "<i class='fa fa-shopping-cart'></i> " . _("Total Payments") . ": <b>".$currency_name." 00.00"."</b>";
		}
		?>
   </div>
</div> 
</div>
	<div class="box-body">
	<table class="table table-bordered table-striped" id="dataTables-paymentHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Ref.No")?></th>
		<th class="col-sm-1"><?php echo _("Amount")?></th>
		<th class="col-sm-1"><?php echo _("Payment By")?></th>
		<th class="col-sm-1"><?php echo _("Transaction ID")?></th>
		<th class="col-sm-1"><?php echo _("Response Code")?></th>
		<th class="col-sm-1"><?php echo _("Payment Datetime")?></th>
		<th class="col-sm-1"><?php echo _("Payment Status")?> </th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$gp = 1 
		?>
		<?php if ($paymentInfo!=0): foreach ($paymentInfo as $payment) : ?>

		<tr>
		<td>
		<?php echo $payment->acc_payment_id?>
		</td>
		
		<td>
		<?php echo $payment->acc_item_number?>
		</td>
		<td>
		<?php echo $currency_name.' '.$payment->c2o_total_amount?> 
		</td>
		
		<td>
		<?php echo $payment->ac_first_name.' '.$payment->ac_last_name?>
		</td>
		
		<td>
		  <?php echo $payment->c2o_transaction_id ?>
		</td>
		<td>
		  <?php echo $payment->c2o_response_code ?>
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($payment->payment_datetime)); 
		?>
		</td>
		
		<td>
		     <?php 
			  switch($payment->payment_status){
				  case '1':
				  ?>
				  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i> <?php echo _("Accepted")?></div>
				  <?php
				  break;
				  
				  case '0':
				  ?>
				  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i> <?php echo _("Failed")?></div>
				  <?php
				  break;
			  }
			 ?>
             
         </td>
		 <td>
		     <a href="javascript:void(0)" title="<?php echo _('More Information') ?>" class="btn btn-xs btn-default" onclick="javascript:triggerPaymentInfoModal('<?php echo $currency_name.' '.$payment->c2o_total_amount?>','<?php echo $payment->c2o_order_number?>','<?php echo $payment->c2o_response_msg?>','<?php echo $payment->c2o_transaction_id?>')">
			 <i class="fa fa-clone"></i>
			 </a>
		     <?php 
			  switch($payment->payment_status){
				  case '1':
				  ?>
				  <a href="<?php echo base_url().'export/payment_invoice/'.str_replace('=','',base64_encode($payment->acc_payment_id)).'p/'.str_replace('=','',base64_encode($payment->user_id))?>" title="<?php echo _('Download Invoice') ?>" class="btn btn-xs btn-info">
				  <i class="fa fa-download"></i>
				  </a>
				  
				  <a href="javascript:void(0)" onclick="javascript:printInvoice('<?php echo str_replace('=','',base64_encode($payment->acc_payment_id))."p"?>','<?php echo str_replace('=','',base64_encode($payment->user_id))?>')" title="<?php echo _('Print Invoice') ?>" class="btn btn-xs btn-default">
				  <i class="fa fa-print"></i>
				  </a>
				  <?php
				  break;
				  
				  case '0':
				  echo '-';
				  break;
			  }
			 ?>
             
         </td>
		</tr>
	<?php
	$gp++;
	endforeach;
	?>
	<?php else : ?>
	<td colspan="6" class="text-center">
	<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
	</td>
	<?php endif; ?>
	</tbody><!-- / Table body -->
	</table> <!-- / Table -->
	</div>
 </div>	
	
</section>
<?php 
	 $this->load->view('modals/payment_moreinfo');
	?>
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-paymentHistory').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>
		
