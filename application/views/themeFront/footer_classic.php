<?php 
$intlib=$this->internal_settings->local_settings();
?>
<div class="footer" id="footer">
<div class="container">
  <div class="col-md-6 text-left">
   <img src="<?php echo base_url()?>theme4.0/client/images/logo_black.png">
  </div>	
  
  <div class="text-right col-md-6">
	 <img src="<?php echo base_url()?>theme4.0/client/images/payment/visa.png" alt="visa">&nbsp;&nbsp;
	 <img src="<?php echo base_url()?>theme4.0/client/images/payment/master.png" alt="visa">&nbsp;&nbsp;
	 <img src="<?php echo base_url()?>theme4.0/client/images/payment/paypal.png" alt="visa">&nbsp;&nbsp;
	 <img src="<?php echo base_url()?>theme4.0/client/images/payment/ae.png" alt="visa">
  </div>
</div>

<div class="container">
  <div class="col-md-6 text-left">
  <span class="copyright"> <a href=""><?php echo _("Your remote IP Address ")?>:<?php echo $_SERVER["REMOTE_ADDR"];?></a></span>
  </div>	
  
  <div class="text-right col-md-6">
   <span class="copyright">&copy; <?php echo $intlib[0]->brand_copyrights;?></span>
  </div>
</div>
</div>
<!-- /.footer --> 
</div>

<script src="<?php echo base_url()?>theme4.0/client/js/bootstrap.min.js"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url()?>theme4.0/client/js/hideMaxListItem.js"></script> 
<!-- include carousel slider plugin  --> 
<script src="<?php echo base_url()?>theme4.0/client/js/bootstrap-notify.js"></script>
<!-- include equal height plugin  --> 
<script src="<?php echo base_url()?>theme4.0/client/js/jquery.matchHeight-min.js"></script> 

</body>
</html>
