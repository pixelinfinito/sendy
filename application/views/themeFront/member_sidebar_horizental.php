		 <div class="">
          <aside>
				<div class="card card-user">
				<div class="image">
					<img src="<?php echo base_url()?>theme4.0/landing/images/product_5.jpg" alt="Profile Banner"/>
				</div>
				<div class="alert-info content">
					<div class="author">
					
						 <a href="#" data-toggle="modal" data-target="#modalProfileThumb" title="<?php echo _('Change Picture') ?>">
							<?php 
							$user_id=$this->session->userdata['site_login']['user_id'];
							$result = $this->account_model->get_profile_thumbnail($user_id);

							if($result[0]->ac_thumbnail!=''){
							?>
								<img src="<?php echo base_url().$result[0]->ac_thumbnail;?>" alt="Thumb" class="avatar border-gray"/>
							<?php
							}else{
							
								if($result[0]->ac_gender!=1){
								?>
								<img src="<?php echo base_url().'theme4.0/client/images/avatar2.png'?>" alt="Thumbnail" width="50" height="50" class="avatar border-gray">
						
									<?php
								}else{
									?>
									<img src="<?php echo base_url().'theme4.0/client/images/avatar5.png'?>" alt="Thumbnail" width="50" height="50" class="avatar border-gray">
							
									<?php
								}
								
							}
							?>

						  <h4 class="title">
							 <?php echo _("Welcome,").$this->session->userdata['site_login']['ac_first_name']." ".$this->session->userdata['site_login']['ac_last_name']."<br>";?>
						  
							 <small class="text-primary"><?php echo "#".$this->session->userdata['site_login']['user_id']?></small>
						  </h4>
						</a>
					</div>
					<div class="description text-center">  
					<?php 
					$intlib=$this->internal_settings->local_settings();
					$user_id=$this->session->userdata['site_login']['user_id'];
					$userInfo= $this->account_model->list_current_user($user_id);
					$acc_class=@$userInfo[0]->ac_class;
						
	                	
						switch($acc_class){

						case '1':
						 if($intlib[0]->upgrade_option!=0){
							?>
							<div class="btn btn-sm btn-primary btn-border">
							<a class="active" href="javascript:void(0)"  data-toggle="modal" data-target="#modalConfirmUpgrade">
							<span class="fa fa-star"></span> <?php echo _("Upgrade Account")?></a>
							</div>
							<?php
						 }
						break;

						case '2':
							?>
							<div class="btn btn-sm btn-success btn-border ">
							<a class="active" href="#">
							<span class="fa fa-shirtsinbulk"></span> <?php echo _("Premium Account")?></a>
							</div>
							<?php
						break;
						}
					 
				?>
					 <br><br>
					</div>
				</div>

				</div>
			
          </aside>
     </div>
    <!--/.page-sidebar-->


