<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url()?>theme4.0/client/images/ico/favicon.png">
<title>OneTextGlobal | Best SMS Campaign Services</title>

<script src="<?php echo base_url()?>theme4.0/client/js/jquery-2.1.1.js"></script> 
<script src= "<?php echo base_url();?>theme4.0/client/js/angular.min.1.2.26.js"></script>
<script src="<?php echo base_url()?>theme4.0/client/js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/client/css/bootstrapValidator.css"/>
<script type="text/javascript" src="<?php echo base_url()?>theme4.0/client/js/bootstrapValidator.js"></script>
<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap-submenu.min.css" rel="stylesheet">

<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>theme4.0/client/css/animate.min.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>theme4.0/client/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>theme4.0/client/css/demo.css" rel="stylesheet" />

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url()?>theme4.0/client/css/pe-icon-7-stroke.css" rel="stylesheet" />
	

<script>
    paceOptions = {
      elements: true
    };
</script>
<script src="<?php echo base_url()?>theme4.0/client/js/pace.min.js"></script>

 </head>
<body>

<div id="wrapper">
 
  <div class="header">
    <nav class="navbar   navbar-site navbar-default" data-color="blue" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only"><?php echo _("Toggle navigation")?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a href="<?php echo base_url()?>home" class="navbar-brand logo logo-title"> 
          <!-- Original Logo will be placed here  --> 
          <span class="logo-icon"> 
		   <?php 
		    $segment=$this->uri->segment(2);
			
			if($segment!='login'){
			 ?>
				<img src="<?php echo base_url()?>theme4.0/client/images/logo_black.png">
			<?php			 
			}else if($segment!='register'){
				?>
				<img src="<?php echo base_url()?>theme4.0/client/images/logo_black.png">
				<?php
			}else{
				?>
				<img src="<?php echo base_url()?>theme4.0/client/images/logo.png">
				<?php
			}
		   ?>
		  
		  </a> 
		  </div>
        
          <?php 
		  if($this->config->item('remote_country_code')!=''){
			 $countryCode=$this->config->item('remote_country_code');
			 $getCountry= $this->countries_model->list_country_timezone_byCode($countryCode);
			 ?>
			   <ul class="nav navbar-nav navbar-right">
			 
			  <li>
			  <br>
			  <a href=""><span class="fa  fa-map-marker"></span> 
					<?php echo @$getCountry[0]->zone_name."&nbsp;[".$getCountry[0]->country_code."]";?>
			  </a>
			  </li>
			  </ul>
			 <?php
			
		  }
         ?>
		 
         
        
        <!--/.nav-collapse --> 
      </div>
      <!-- /.container-fluid --> 
    </nav>
  </div>
  <!-- /.header -->
  