  <?php 
	$token=base64_decode($this->input->get('tkn'));
	$class='class="active"';
	
?>

<ul class="nav nav-tabs tabs-left">

<li <?php if($token=='nc'){echo $class;}else if($token==''){echo $class;}?>>
 <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>" >
 <i class="fa fa-envelope"></i> <?php echo _("New Campaign")?></a>
</li>

<li <?php if($token=='cts'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('cts')?>" >
<i class="fa fa-user-plus"></i> <?php echo _("All Contacts")?></a>
</li>

<li <?php if($token=='grp'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('grp')?>" >
<i class="fa fa-users"></i>  <?php echo _("Contact Groups")?></a>
</li>


<li <?php if($token=='mc'){echo $class;}?> >
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('mc')?>" >
<i class="fa fa-paper-plane-o"></i> <?php echo _("All Campaigns")?></a>
</li>

<li <?php if($token=='sch'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('sch')?>" >
<i class="fa  fa-calendar-plus-o"></i> <?php echo _("Schedules")?></a>
</li>

<li <?php if($token=='tt'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('tt')?>" >
<i class="fa fa-tags"></i> <?php echo _("Title Templates")?></a>
</li>

<li <?php if($token=='ntf'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('ntf')?>" >
<i class="fa  fa-bell-o"></i> <?php echo _("Notifications")?></a>
</li>

<li <?php if($token=='cn'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('cn')?>" >
<i class="fa  fa-phone"></i> <?php echo _("Caller Numbers")?></a>
</li>

<li <?php if($token=='sp'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('sp')?>" >
<i class="fa  fa-dollar"></i> <?php echo _("SMS Pricing")?></a>
</li>

<li <?php if($token=='rpt'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('rpt')?>" >
<i class="fa  fa-pie-chart"></i> <?php echo _("Reports")?></a>
</li>

<li <?php if($token=='hist'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('hist')?>" >
<i class="fa  fa-history"></i> <?php echo _("SMS History")?></a>
</li>

<hr>
<li <?php if($token=='hlp'){echo $class;}?>>
<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('hlp')?>"  class="text-danger">
<i class="fa   fa-question-circle"></i> <?php echo _("Help & Support")?></a>
</li>

</ul>