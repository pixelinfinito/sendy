<div class="col-sm-3 page-sidebar">
          <aside>
            <div class="inner-box">
              <div class="user-panel-sidebar">
			  <div class="row">
			  <div class="col-md-12 col-xs-4 col-xxs-12">
              <div class="col-md-6 col-xs-6 col-xxs-6">
			  <h3 class="no-padding text-center-480 useradmin">
			  <img class="circle" src="<?php echo base_url()?>themeBeta/images/user.jpg" alt="user" > 
			  </h3>
			  </div>
			  <div class="col-md-6 col-xs-6 col-xxs-6">
			    <a href=""><?php echo _("Change")?></a>
			  </div>
              </div>
			 </div>
                <div class="collapse-box">
                  <h5>&nbsp;</h5>
                  <div class="panel-collapse collapse in" id="MyClassified">
                    <ul class="acc-list">
                      <li><a class="active" href="#"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
                      
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                <div class="collapse-box">
                  <h5 class="collapse-title"> <?php echo _("My Ads")?><a href="#MyAds" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="MyAds">
                    <ul class="acc-list">
                      <li><a href="<?php echo base_url()?>site/ads/all/"><i class="icon-docs"></i> <?php echo _("All Ads")?><span class="badge">42</span> </a></li>
                      <li><a href="<?php echo base_url()?>site/ads/favourite/"><i class="icon-heart"></i> <?php echo _("Favourite ads")?><span class="badge">42</span> </a></li>
                      <li><a href="<?php echo base_url()?>site/search/saved/"><i class="icon-star-circled"></i> <?php echo _("Saved search")?><span class="badge">42</span> </a></li>
                      <li><a href="<?php echo base_url()?>site/ads/pending/"><i class="icon-hourglass"></i> <?php echo _("Pending approval")?><span class="badge">42</span></a></li>
                 
                    </ul>
                  </div>
                </div>
                <!-- /.collapse-box  -->
                
                 <div class="collapse-box">
                  <h5 class="collapse-title"> <?php echo _("Banner Ads")?><a href="#Banners" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="Banners">
                    <ul class="acc-list">
                      <li><a href="account-myads.html"><i class="icon-docs"></i> <?php echo _("My Banners")?></a></li>
                      <li><a href="account-favourite-ads.html"><i class="icon-heart"></i> <?php echo _("Buy a Slide")?></a></li>
					  <li><a href="account-favourite-ads.html"><i class="fa fa-credit-card"></i> <?php echo _("Information center")?></a></li>
                 
                    </ul>
                  </div>
                </div>
				<div class="collapse-box">
                  <h5 class="collapse-title"><?php echo _("Auctions")?><a href="#Auctions" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="Auctions">
                    <ul class="acc-list">
                      <li><a href="account-myads.html"><i class="icon-docs"></i> <?php echo _("My Bids")?><span class="badge">42</span> </a></li>
                      <li><a href="account-favourite-ads.html"><i class="icon-heart"></i><?php echo _("My Offers")?><span class="badge">42</span> </a></li>
					  <li><a href="account-favourite-ads.html"><i class="icon-heart"></i> <?php echo _("Query & Responses")?></a></li>
                    </ul>
                  </div>
                </div>
				
				<div class="collapse-box">
                  <h5 class="collapse-title"><?php echo _("Payments")?><a href="#Payments" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h5>
                  <div class="panel-collapse collapse in" id="Payments">
                    <ul class="acc-list">
					  <li><a href="<?php echo base_url()?>site/wallet/add/"><i class="icon-heart"></i> <?php echo _("Make Payment")?></a></li>
					  <li><a href="<?php echo base_url()?>site/payments/card/"><i class="fa fa-credit-card"></i> <?php echo _("Add/Update Card")?></a></li>
					  <li><a href="<?php echo base_url()?>site/payments/history/"><i class="icon-docs"></i> <?php echo _("Payment History")?></a></li>
                    </ul>
                  </div>
                </div>
				
                <!-- /.collapse-box  --> 
              </div>
            </div>
            <!-- /.inner-box  --> 
            
          </aside>
        </div>
        <!--/.page-sidebar-->


