<?php
 if(!isset($this->session->userdata['site_login'])){
	  header("location:".base_url()."site/login");
	  exit(0);
      
 }

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="shortcut icon" href="<?php echo base_url()?>theme4.0/client/images/ico/favicon.png">
<title>OneTextGlobal | Best SMS Campaiging Services</title>

<script src="<?php echo base_url()?>theme4.0/client/js/jquery-2.1.1.js"></script>
<script src= "<?php echo base_url();?>theme4.0/client/js/angular.min.1.2.26.js"></script>
<script src="<?php echo base_url();?>theme4.0/client/js/ui-bootstrap-tpls-0.10.0.min.js"></script>

<link href="<?php echo base_url();?>theme4.0/customStyle.css" rel="stylesheet"/>
<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/client/css/bootstrapValidator.css"/>
<script type="text/javascript" src="<?php echo base_url()?>theme4.0/client/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/sms_campaign/interval.js"></script>

<link href="<?php echo base_url()?>theme4.0/client/css/style.css" rel="stylesheet">
<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>theme4.0/client/css/light-bootstrap-dashboard-classic.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap.vertical-tabs.min.css" rel="stylesheet"> 
<link href="<?php echo base_url()?>theme4.0/client/css/pe-icon-7-stroke.css" rel="stylesheet" />
	
<script>
    paceOptions = {
      elements: true
    };
</script>

<script src="<?php echo base_url()?>theme4.0/client/js/pace.min.js"></script>
</head>
<body>

<div id="wrapper">
<?php 
  if($this->config->item('remote_country_code')!=''){
	 $countryCode=$this->config->item('remote_country_code');
  }else{
	  $countryCode=$this->session->userdata['site_login']['ac_country'];
  }
  
  $acUserId=$this->session->userdata['site_login']['user_id'];
  $getCountry= $this->countries_model->list_country_timezone_byCode($countryCode);
  $walletInfo= $this->account_model->get_account_wallet($acUserId);
  
  $smsMServiceInfo= $this->account_model->get_sms_service($acUserId);
  $sms_service_status=@$smsMServiceInfo[0]->trigger_status;
  $smsMSettings=$this->account_model->get_sms_settings();
  
  if($sms_service_status==1){
	 $interval= $smsMSettings[0]->service_recall_interval;
	  ?>
	  <script>
	   $(document).ready(function() {
	
			setInterval('checkcamp_logs()',<?php echo $interval;?>);
	
		});
	  </script>
	  
	  <?php
  }
?>
  <div class="header">
    <nav class="navbar   navbar-site navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only"><?php echo _("Toggle navigation")?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a href="<?php echo base_url()?>home" class=""> 
          <!-- Original Logo will be placed here  --> 
			<img src="<?php echo base_url()?>theme4.0/client/images/logo.png">
		  </a> 
		  </div>
        <div class="navbar-collapse collapse">
          
		   <ul class="nav navbar-nav navbar-right">
		    <li><a href=""><span class="fa  fa-map-marker"></span> 
			<?php echo @$getCountry[0]->zone_name."&nbsp;[".$getCountry[0]->country_code."]";?>
			</a></li>
            <li><a href="<?php echo base_url()?>site/bye"> <i class="glyphicon glyphicon-off"></i>  <?php echo _("Sign out")?></a></li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
			<span><i class="icon-user fa"></i>  <?php echo $this->session->userdata['site_login']['ac_first_name']." ".$this->session->userdata['site_login']['ac_last_name'];?></span> <i class=" icon-down-open-big fa"></i></a>
              <ul class="dropdown-menu user-menu">
                <li class="active"><a href="<?php echo base_url()?>site/dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
				<li><a href="<?php echo base_url()?>site/profile"><i class="fa fa-home"></i> <?php echo _("Profile Home")?></a></li>
                <li>
					<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('mc')?>" >
					<i class="fa fa-paper-plane-o"></i> <?php echo _("All Campaigns")?></a>
				</li>
				<li>
				<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('cts')?>" >
					<i class="fa fa-user-plus"></i> <?php echo _("Contacts")?></a>
				</li>
                <li>
				<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('cn')?>" >
					<i class="fa  fa-phone"></i> <?php echo _("Caller Numbers")?></a>

				</li>
				<li>
				<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('grp')?>" >
					<i class="fa fa-users"></i>  <?php echo _("SMS Groups")?></a>
				</li>
                <li>
				<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('sp')?>" >
					<i class="fa  fa-dollar"></i> <?php echo _("SMS Pricing")?></a>
				</li>
                <li>
				<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('rpt')?>" >
					<i class="fa  fa-pie-chart"></i> <?php echo _("Reports")?></a>
				</li>
                <li>
				<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('hist')?>" >
					<i class="fa  fa-history"></i> <?php echo _("History")?></a>
				</li>
				<li class="active">
				<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>" >
					<i class="fa fa-play"></i> <?php echo _("Start Campaign")?></a>
				</li>
				<li>
				<a href="<?php echo base_url()?>site/payment/history">
				<i class="fa fa-dollar"></i> <?php echo _("Payment History")?></a>
				</li>
              </ul>
			</li>
			<li class="dropdown"> 
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
			<i class="fa fa-google-wallet"></i> <?php echo _("My Wallet")?><?php 
				$appSettings= $this->app_settings_model->get_primary_settings();
				$appCountry=$appSettings[0]->app_country;
				$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
				
				if(@$walletInfo[0]->balance_amount!=''){
				  echo "(".@$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.@$walletInfo[0]->balance_amount.")";
				}else{
					echo "(".@$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name." 0)";
				}
			?>
			<i class=" icon-down-open-big fa"></i> 
			</a>
              <ul class="dropdown-menu user-menu">
                <li><a href="<?php echo base_url()?>site/wallet/add"><i class="fa fa-dollar"></i> <?php echo _("Add Funds")?></a></li>
				<li><a href="<?php echo base_url()?>site/wallet/transfer"><i class="fa fa-exchange"></i> <?php echo _("Transfer Funds")?></a></li>
				<li><a href="<?php echo base_url()?>site/wallet/received"><i class="fa fa-retweet"></i> <?php echo _("Received Funds")?></a></li>
                <li><a href="<?php echo base_url()?>site/wallet/history"><i class="fa fa-history"></i> <?php echo _("Wallet History")?></a></li>
              </ul>
			  
            </li>
            <li class="postadd">
			  <a class="btn btn-border btn-info" href="<?php echo base_url()?>site/help">
			  <i class="pe-7s-chat"></i> <?php echo _("Need Help ?")?></a>
			</li>
          </ul>
		  
        </div>
        <!--/.nav-collapse --> 
      </div>
      <!-- /.container-fluid --> 
    </nav>
  </div>
  <!-- /.header -->
  