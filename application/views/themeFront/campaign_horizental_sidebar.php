
<div class="bluebg">
<div class="container">
		<div class="row">
		<div class="col-sm-4">
		   <h4> <i class="fa  fa-dashboard"></i>&nbsp; <?php echo _("Dashboard")?></h4>
		 </div>
		 <div class="col-sm-8 navbar-collapse collapse">
		   
		     <ul class="nav navbar-nav pull-right">
			   <li class="dropdown "> <a href="#" class="dropdown-toggle " data-toggle="dropdown"> 
			   <i class="fa fa-newspaper-o"></i> <?php echo _("My Ads")?><i class="fa fa-angle-down"></i>
               <ul class="dropdown-menu user-menu">
                
                      <li><a href="<?php echo base_url()?>site/ads/all/"><i class="icon-docs"></i> <?php echo _("All Ads")?><span class="badge">42</span> </a></li>
                      <li><a href="<?php echo base_url()?>site/ads/favourite/"><i class="icon-heart"></i> <?php echo _("Favourite Ads")?><span class="badge">42</span> </a></li>
                      <li><a href="<?php echo base_url()?>site/search/saved/"><i class="icon-star-circled"></i> <?php echo _("Saved search")?><span class="badge">42</span> </a></li>
                      <li><a href="<?php echo base_url()?>site/ads/pending/"><i class="icon-hourglass"></i> <?php echo _("Pending approval")?><span class="badge">42</span></a></li>
                <li>&nbsp;</li>
              </ul>
		      </li>
			  
			  <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
			  <i class="fa  fa-leaf"></i> <?php echo _("Banner Ads")?><i class="fa fa-angle-down"></i>
               <ul class="dropdown-menu user-menu">
                       <li><a href="account-myads.html"><i class="icon-docs"></i> <?php echo _("My Banners")?></a></li>
                      <li><a href="account-favourite-ads.html"><i class="icon-heart"></i> <?php echo _("Buy a Slide")?></a></li>
					  <li><a href="account-favourite-ads.html"><i class="icon-info"></i> <?php echo _("Information Center")?></a></li>
					  <li>&nbsp;</li>
              </ul>
		      </li>
			  
			  <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
			  <i class="fa  fa-institution"></i> <?php echo _("Auctions")?><i class="fa fa-angle-down"></i>
               <ul class="dropdown-menu user-menu">
                      <li><a href="<?php echo base_url()?>site/auctions/my_bids/"><i class="icon-docs"></i> <?php echo _("My Bids")?><span class="badge">42</span> </a></li>
                      <li><a href="<?php echo base_url()?>site/auctions/my_auctions/"><i class="icon-heart"></i><?php echo _("My Auctions")?><span class="badge">42</span> </a></li>
					  <li><a href="<?php echo base_url()?>site/auctions/query_responses/"><i class="icon-heart"></i> <?php echo _("Query & Responses")?></a></li>
					  <li>&nbsp;</li>
              </ul>
		      </li>
			  
			  <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
			 <i class="fa  fa-money"></i> <?php echo _("Payments")?><i class="fa fa-angle-down"></i>
               <ul class="dropdown-menu user-menu">
                      <li><a href="<?php echo base_url()?>site/wallet/add/"><i class="icon-usd"></i> <?php echo _("Make Payment")?></a></li>
					  <li><a href="<?php echo base_url()?>site/wallet/add/"><i class="icon-usd"></i> <?php echo _("Withdraw Payments")?></a></li>
					  <li><a href="<?php echo base_url()?>site/payments/card/"><i class="fa fa-credit-card"></i> <?php echo _("Add/Update Card")?></a></li>
					  <li><a href="<?php echo base_url()?>site/payments/history/"><i class="icon-history"></i> <?php echo _("Payment History")?></a></li>
					  <li>&nbsp;</li>
              </ul>
		      </li>
			  
			   <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
			  <i class="fa  fa-send-o"></i> <?php echo _("SMS Campaign")?><i class="fa fa-angle-down"></i>
               <ul class="dropdown-menu">
                      <li><a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>"><i class="icon-flash"></i> <?php echo _("Start Campaign")?></a></li>
					  <li><a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('mc')?>"><i class="icon-mobile"></i> <?php echo _("My Campaigns")?></a></li>
					  <li><a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('hist')?>"><i class="icon-repeat"></i> <?php echo _("Campaign History")?></a></li>
					  <li>&nbsp;</li>
              </ul>
		      </li>
			  
			</ul>
			
		 </div>
		</div>
		</div>
		
		</div>