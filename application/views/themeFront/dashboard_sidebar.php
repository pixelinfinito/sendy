
<?php 
	$user_id=$this->session->userdata['site_login']['user_id'];
    $campaign_summary=$this->sms_model->get_campaign_overview($user_id);
	$contacts_summary=$this->sms_model->get_contacts_byuser($user_id);
	if($campaign_summary!=0){
		if(count($campaign_summary)<10){
			$allCamps="0".count($campaign_summary);
		}else{
			$allCamps=count($campaign_summary);
		}
	    
	}else{
	      $allCamps=0; 
	}
	
	if($contacts_summary!=0){
		if(count($contacts_summary)<10){
			$allContacts="0".count($contacts_summary);
		}else{
			$allContacts=count($contacts_summary);
		}
	}else{
		$allContacts=0;
	}
?>
<div class="graybg">
<div class="container">
		<div class="row">
		<div class="col-sm-4">
		   <h4> <i class="fa  fa-dashboard"></i>&nbsp; <?php echo _("Dashboard")?></h4>
		 </div>
		 <div class="col-sm-8 navbar-collapse collapse pull-right">
		     <ul class="nav navbar-nav pull-right">
			  <li> 
			   <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>" class="text-success"> 
			    <i class="fa  fa-play"></i> <b><?php echo _("Start Campaign")?></b>
				</a>
              </li>
			  
			  
			   <li> 
			   <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('mc')?>"> 
			   <i class="fa fa-send-o"></i> <?php echo _("All Campaigns")?><span class="badge"><?php echo $allCamps?></span>
			   </a>
              </li>
			  
			  <li> 
			  <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('cts')?>"> 
			  <i class="fa fa-user-plus"></i> <?php echo _("Contacts")?><span class="badge"><?php echo $allContacts?></span> 
			  </a>
			  </li>
			  
			 <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
			 <i class="fa  fa-money"></i> <?php echo _("Payments")?><i class="fa fa-angle-down"></i>
               <ul class="dropdown-menu user-menu">
                      <li><a href="<?php echo base_url()?>site/wallet/add/"><i class="fa fa-google-wallet"></i> <?php echo _("Make Payment")?></a></li>
					  <li><a href="<?php echo base_url()?>site/payment/card/"><i class="fa fa-credit-card"></i> <?php echo _("Add/Update Card")?></a></li>
					  <li><a href="<?php echo base_url()?>site/payment/history/"><i class="fa fa-dollar"></i> <?php echo _("Payment History")?></a></li>
					  <li>&nbsp;</li>
              </ul>
		      </li>
			  
			   
			   <li> 
			   <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>"> 
			    <i class="fa  fa-book"></i> <?php echo _("Tutorials")?></a>
              </li>
			  
			</ul>
			
		 </div>
		</div>
		</div>
		
		</div>