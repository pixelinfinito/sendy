<?php 
$intlib=$this->internal_settings->local_settings();
?>
<footer>
  	<div class="container">
      <div class="col-md-6 text-left">
      <span class="copyright"> 
	  <!--<a href=""><?php echo _("Your remote IP Address ")?>:<?php echo $_SERVER["REMOTE_ADDR"];?></a> -->
	  </span>
	  <img src="<?php echo base_url()?>theme4.0/client/images/logo_black.png" width="95" height="45">
	  </div>	
	  <div class="text-right col-md-6">
       <span class="copyright">&copy; <?php echo $intlib[0]->brand_copyrights;?></span>
      </div>
    </div>
  </footer>
  <!-- /.footer --> 
</div>
<script src="<?php echo base_url()?>theme4.0/client/js/bootstrap-submenu.min.js"></script> 
<script src="<?php echo base_url()?>theme4.0/client/js/bootstrap.min.js"></script> 
<!-- include jquery list shorting plugin plugin  --> 
<script src="<?php echo base_url()?>theme4.0/client/js/hideMaxListItem.js"></script>
<script src="<?php echo base_url()?>theme4.0/client/js/jquery.matchHeight-min.js"></script> 
<script src="<?php echo base_url()?>theme4.0/client/js/bootstrap-notify.js"></script> 
<script src="<?php echo base_url()?>theme4.0/client/js/light-bootstrap-dashboard.js"></script> 
<script src="<?php echo base_url()?>theme4.0/client/js/demo.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		/*demo.initChartist();*/

		$.notify({
			icon: 'pe-7s-like2',
			message: "<?php echo $intlib[0]->app_default_name;?> <b> <?php echo _("the best platform")?></b> for business campaigns"

		},{
			type: 'info',
			timer: 4000
		});

	});
</script>

</body>
</html>
