<div class="col-sm-4 page-sidebar">
          <aside>
		  <div class="card card-user">
					<div class="image">
						<img src="<?php echo base_url()?>theme4.0/landing/images/product_5.jpg" alt="Profile Banner"/>
					</div>
					<div class="alert-success content">
						<div class="author">
							 <a href="#" data-toggle="modal" data-target="#modalProfileThumb" title="<?php echo _('Change Picture') ?>">
								<?php 
								$user_id=$this->session->userdata['site_login']['user_id'];
								$result = $this->account_model->get_profile_thumbnail($user_id);

								if(@$result[0]->ac_thumbnail!=''){
								?>
								<img src="<?php echo base_url().$result[0]->ac_thumbnail;?>" alt="Thumb" class="avatar border-gray"/>
								<?php
								}else{
								  	if($result[0]->ac_gender!=1){
									?>
									<img src="<?php echo base_url().'theme4.0/client/images/avatar2.png'?>" alt="Thumbnail" width="50" height="50" class="avatar border-gray">
							
										<?php
									}else{
										?>
										<img src="<?php echo base_url().'theme4.0/client/images/avatar5.png'?>" alt="Thumbnail" width="50" height="50" class="avatar border-gray">
								
										<?php
									}
										
								}
								?>

							  <h4 class="title">
								 <?php echo _("Welcome,").$this->session->userdata['site_login']['ac_first_name']." ".$this->session->userdata['site_login']['ac_last_name']."<br>";?>
							  
								 <small><?php echo $this->session->userdata['site_login']['user_id']?></small>
							  </h4>
							</a>
						</div>
						<div class="description text-center">  
					<?php 
					 $intlib=$this->internal_settings->local_settings();
					 $user_id=$this->session->userdata['site_login']['user_id'];
					 $userInfo= $this->account_model->list_current_user($user_id);
					 $acc_class=@$userInfo[0]->ac_class;
					 
	                	 switch($acc_class){
							 
							case '1':
							if($intlib[0]->upgrade_option!=0){
							?>
								<div class="btn btn-sm btn-primary btn-border">
								<a class="active" href="javascript:void(0)" data-toggle="modal" data-target="#modalConfirmUpgrade">
								<span class="fa fa-star"></span> <?php echo _("Upgrade Account")?></a>
								</div>
							<?php
							}
							break;

							case '2':
							?>
								<div class="btn btn-sm btn-success btn-border ">
								<a class="active" href="#">
								<span class="fa fa-shirtsinbulk"></span> <?php echo _("Premium Account")?></a>
								</div>
							<?php
							break;
						 }
					
				   ?>
						 <br><br>
						</div>
					</div>
				
				 
            </div>
			
		   <div class="panel sidebar-panel">
              <div class="panel-heading uppercase"><i class="fa fa-info-circle"></i> <?php echo _("Information Center")?></div>
              <div class="panel-content">
			  <div class="panel-body text-left">
			   <div class="row">
			    <div class="col-sm-12 text-center">
				<?php 
				 if($userInfo[0]->mobile_validate!='1'){
				?>
				<?php echo _("Your mobile number is not verified")?><br>
				<a href="#" data-toggle="modal" data-target="#MobileValidateModal" class="text-danger">
				<i class="fa fa-mobile"></i> <?php echo _("Click here")?></a> <?php echo _("to verify")?><?php
				 }else{
					 echo "<h4 class='text-success'><i class='fa fa-check-circle'></i> " . _('Mobile Verified') . "</h4>";
				 }
				?>
				   
				</div>
				</div>
				</div>
				
              </div>
           </div>
			<div class="col-md-12 btn btn-danger">
			<a href="#" ><i class="fa fa-times"></i> <?php echo _("Terminate My Account")?></a>
			</div>
			
          </aside>
        </div>
        <!--/.page-sidebar-->

		

