<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
<script type="application/javascript" src="<?php echo base_url();?>js/payment_settings.js"></script>
<?php 
 
 @$appRes=$this->app_settings_model->get_payment_settings();
 $total_members=$this->account_model->list_all_members();
 $inactive_members=$this->account_model->list_inactive_members();

?>

 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo _("Payment Gateway Settings")?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>settings/site"><?php echo _("Settings")?></a></li> <li class="active"><?php echo _("Payment Settings")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div ng-app="">
            <div class="row">
            <div class="col-lg-8 col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			     
                <form role="form" ng-controller="PaymentSettingsController" name="PaymentSettingsForm" id="PaymentSettingsForm">
				   <h3 class="custom_priceTag"><?php echo _("Paypal")?><span class="fa fa-cc-paypal"></span></h3><br>
				   <label class="text-primary"><?php echo _("For Upgrade")?></label> <small>[<?php echo _("When user wants to upgrade account")?>]</small>
					<div class="custom_alert">
					<div class="row">
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Paypal Email")?></strong></small> <small class="red" >*</small>
						<input type="email" class="form-control" id="pp_email" name="pp_email" value="<?php echo @$appRes[0]->pp_email;?>" placeholder="<?php echo _('E.g. mypp@domain.com') ?>" required="required"/>
						</div>
						</div>
						
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Return URL")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="pp_return_url" name="pp_return_url" value="<?php echo @$appRes[0]->pp_return_url;?>" placeholder="<?php echo _('E.g. http://domain.com/success.html') ?>" required="required"/>
						</div>
						</div>
                     </div>
					 
					 <div class="row">
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Cancel URL")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="pp_cancel_url" name="pp_cancel_url" value="<?php echo @$appRes[0]->pp_cancel_url;?>" placeholder="<?php echo _('E.g. http://domain.com/cancel.html') ?>" required="required"/>
						</div>
						</div>
						
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Notify URL")?></strong></small>
						<input type="text" class="form-control" id="pp_notify_url" name="pp_notify_url" value="<?php echo @$appRes[0]->pp_notify_url;?>" placeholder="<?php echo _('E.g. http://domain.com/notify.html') ?>"/>
						</div>
						</div>
                     </div>
					 </div>
					 <br>
					 
					 <label class="text-primary"><?php echo _("For Wallet Money")?></label> <small>[<?php echo _("When user wants to add funds to wallet")?>]</small>
					 <div class="custom_alert">
					<div class="row">
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Paypal Email")?></strong></small> <small class="red" >*</small>
						<input type="email" class="form-control" id="pp_wallet_email" name="pp_wallet_email" value="<?php echo @$appRes[0]->pp_wallet_email;?>" placeholder="<?php echo _('E.g. mypp@domain.com') ?>" required="required"/>
						</div>
						</div>
						
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Return URL")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="pp_wallet_return_url" name="pp_wallet_return_url" value="<?php echo @$appRes[0]->pp_wallet_return_url;?>" placeholder="<?php echo _('E.g. http://domain.com/success.html') ?>" required="required"/>
						</div>
						</div>
                     </div>
					 
					 <div class="row">
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Cancel URL")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="pp_wallet_cancel_url" name="pp_wallet_cancel_url" value="<?php echo @$appRes[0]->pp_wallet_cancel_url;?>" placeholder="<?php echo _('E.g. http://domain.com/cancel.html') ?>" required="required"/>
						</div>
						</div>
						
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Notify URL")?></strong></small>
						<input type="text" class="form-control" id="pp_wallet_notify_url" name="pp_wallet_notify_url" value="<?php echo @$appRes[0]->pp_wallet_notify_url;?>" placeholder="<?php echo _('E.g. http://domain.com/notify.html') ?>"/>
						</div>
						</div>
                     </div>
					 </div>
					 
					<br>
					<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
						<small><strong><?php echo _("Paypal Payment URL")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="pp_payment_url" name="pp_payment_url" value="<?php echo @$appRes[0]->pp_payment_url;?>" placeholder="<?php echo _('E.g. https://www.sandbox.paypal.com/cgi-bin/webscrm') ?>" required="required"/>
						</div>
						</div>
					</div>
					<h3 class="custom_priceTag"><?php echo _("2Checkout")?><span class="fa fa-cc-visa"></span> <span class="fa fa-cc-mastercard"></span>
					</h3><br>
					
					<div class="custom_alert">
					<div class="row">
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("2Co Seller ID")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="co_seller_id" name="co_seller_id" value="<?php echo @$appRes[0]->tco_seller_id;?>" placeholder="<?php echo _('E.g. 901316091') ?>" required="required"/>
						</div>
						</div>
						
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("2Co Base URL")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="co_base_url" name="co_base_url" value="<?php echo @$appRes[0]->tco_base_url;?>" placeholder="<?php echo _('E.g. https://2checkout.com') ?>" required="required"/>
						</div>
						</div>
                     </div>
					 
					 <div class="row">
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("2Co Sandbox URL")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="co_sandbox_url" name="co_sandbox_url" value="<?php echo @$appRes[0]->tco_sandbox_url;?>" placeholder="<?php echo _('E.g. https://sandbox.2checkout.com') ?>" required="required"/>
						</div>
						</div>
						
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("Is Sandbox Test")?></strong></small>
						<select class="form-control" id="is_sandbox" name="is_sandbox">
						 <option value="1" <?php if($appRes[0]->tco_is_sandbox==1){echo _("selected");}?>><?php echo _("Yes")?></option>
						 <option value="0" <?php if($appRes[0]->tco_is_sandbox==0){echo _("selected");}?>><?php echo _("No")?></option>
						</select>
						
						</div>
						</div>
                     </div>
					 <hr>
					 
					 <div class="row">
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("2Co Private Key")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="co_private_key" name="co_private_key" value="<?php echo @$appRes[0]->tco_private_key;?>" placeholder="<?php echo _('E.g. F0BC2305-A262-4F98-ADBB-8428D1CBACA2') ?>" required="required"/>
						</div>
						</div>
						
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("2Co Publishable Key")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="co_publish_key" name="co_publish_key" value="<?php echo @$appRes[0]->tco_publish_key;?>" placeholder="<?php echo _('E.g. E4055168-408C-4CAC-99C3-53F5FBAF8E6D') ?>" required="required"/>
						</div>
						</div>
                     </div>
					
					 <div class="row">
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("2Co Username")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="co_username" name="co_username" value="<?php echo @$appRes[0]->tco_username;?>" placeholder="<?php echo _('E.g. myusername') ?>" required="required"/>
						</div>
						</div>
						
						<div class="col-lg-6">
						<div class="form-group">
						<small><strong><?php echo _("2Co Password")?></strong></small> <small class="red" >*</small>
						<input type="text" class="form-control" id="co_password" name="co_password" value="<?php echo @$appRes[0]->tco_password;?>" placeholder="<?php echo _('E.g xxxxxx') ?>" required="required"/>
						</div>
						</div>
                     </div>
					 </div>
					 <br>
					
                    <div>
                        <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='AddPaymentSettings()'>
						<i class="fa fa-save"></i> <?php echo _("Save")?></button>
                       
                    </div>
                </form>
				</div>
				<div id="rsDiv"></div>
				<br>
			</div>
			</div>
		
			<div class="col-md-4">
            <div class="widget-user-header bg-info">
			  <div class="widget-user text-center">
				<h2><b><span class="text-info">2</span><?php echo _("CHECKOUT.COM")?></b></h2>
			  </div><!-- /.widget-user-image -->
			    <div class="text-center">
				<a href="https://www.2checkout.com" target="_blank" class="btn btn-sm btn-info"><?php echo _("Logon to 2co account")?></a><br><br>
				</div>
			</div>
			<br>
			
               <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-primary">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i> <?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?><span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?><span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Enable payment methods as you required.")?></li>
				 <li><?php echo _("Validation code should be by given parameters if you don't aware of this please leave it same.")?></li>
				</ul>
                           
              </div>
				
			</div>
			
			
            </div>
           
        </section><!-- /.content -->
		
		
<?php $this->load->view('theme/footer.php');?>
		
