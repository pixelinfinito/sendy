<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
<script type="application/javascript" src="<?php echo base_url();?>js/site_settings.js"></script>
<?php 
 
 @$appRes=$this->app_settings_model->get_site_settings();
$total_members=$this->account_model->list_all_members();
$inactive_members=$this->account_model->list_inactive_members();
 
?>
 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo _("Site Settings")?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>settings/site"><?php echo _("Settings")?></a></li> <li class="active"><?php echo _("Site Settings")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div ng-app="">
            <div class="row">
            <div class="col-lg-8 col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			    <form role="form" ng-controller="SiteSettingsController" name="SiteSettingsForm" id="SiteSettingsForm">
				   <h3 class="custom_priceTag"><?php echo _("Payment Methods")?></h3>
					<div class="custom_alert">
					<div class="row">
					<div class="col-lg-6">
                    <div class="form-group">
					 <b><small><?php echo _("Allow Credit/Debit Card")?><span class="red" >*</span></small></b>
					  <select class="form-control"  id="site_ccpayment" name="site_ccpayment" >
					     <option value="">--<?php echo _("Select option")?>--</option>
						 <option value="1" <?php if(@$appRes[0]->allow_card=='1'){echo _("selected");}?>><?php echo _("Yes (Users can pay using credit/debit card")?>)</option>
						 <option value="0" <?php if(@$appRes[0]->allow_card=='0'){echo _("selected");}?>><?php echo _("No (Restricted credit/debit card")?>)</option>
					  </select>
					</div>
					</div>
					
					<div class="col-lg-6">
					<div class="form-group">
					
                    <b><small><?php echo _("Allow Paypal")?><span class="red" >*</span></small></b>
					  <select class="form-control"  id="site_paypal" name="site_paypal" >
					     <option value="">--<?php echo _("Select option")?>--</option>
						 <option value="1" <?php if(@$appRes[0]->allow_paypal=='1'){echo _("selected");}?>><?php echo _("Yes (Users can pay using paypal")?>)</option>
						 <option value="0" <?php if(@$appRes[0]->allow_paypal=='0'){echo _("selected");}?>><?php echo _("No (Restricted paypal")?>)</option>
					  </select>
					
                    </div>
					</div>
                     </div>
					 </div>
					<br>
					
					<div class="form-group">
                     <label><?php echo _("Allow Wallet Payment")?></label> <small class="red" >*</small>
					  <select class="form-control"  id="site_wallet" name="site_wallet" >
					     <option value="">--<?php echo _("Select option")?>--</option>
						 <option value="1" <?php if(@$appRes[0]->allow_wallet=='1'){echo _("selected");}?>><?php echo _("Yes (Users can use wallet money")?>)</option>
						 <option value="0" <?php if(@$appRes[0]->allow_wallet=='0'){echo _("selected");}?>><?php echo _("No (Restricted wallet money")?>)</option>
					  </select>
                    </div>
					
					<div class="form-group">
                    <label><?php echo _("Default Bid Expire days")?></label> <small class="red" >*</small>
                    <input type="text" placeholder="<?php echo _('E.g. 30') ?>" class="form-control"  value="<?php echo @$appRes[0]->bid_expire_days;?>" id="site_bidexpire" required="required" name="site_bidexpire">
                    </div>
					
					<div class="form-group">
                    <label><?php echo _("Auction countdown time")?></label> <small class="red" >*</small>
                    <input type="text" placeholder="<?php echo _('E.g. 1000') ?>" class="form-control"  value="<?php echo @$appRes[0]->bid_refresh_time;?>" id="bid_refresh_time" required="required" name="bid_refresh_time">
                    <small>[<?php echo _("This is auction expire,refreshing time in seconds 1 second = 1000 ms")?>]</small>
					</div>
					
					<h3 class="custom_priceTag"><?php echo _("Maximum Image Uploads")?></h3>
					<div class="custom_alert">
					<div class="row">
					
					<div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("For Individual")?><span class="red" >*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 5') ?>" class="form-control" value="<?php echo @$appRes[0]->image_uploads;?>" id="site_image_uploads" required="required" name="site_image_uploads">
					
                    </div>
					</div>
					
					<div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("For Business")?><span class="red" >*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 5') ?>" class="form-control" value="<?php echo @$appRes[0]->business_image_uploads;?>" id="site_business_image_uploads" required="required" name="site_business_image_uploads">
					
                    </div>
					</div>
					</div>
					</div>
					
					<h3 class="custom_priceTag"><?php echo _("AD Limit")?></h3>
					<div class="custom_alert">
					<div class="row">
					
					<div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("For Individual")?><span class="red" >*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 5') ?>" class="form-control" value="<?php echo @$appRes[0]->individual_ad_limit;?>" id="individual_ad_limit" required="required" name="individual_ad_limit">
					
                    </div>
					</div>
					
					<div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("For Business")?><span class="red" >*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 5') ?>" class="form-control" value="<?php echo @$appRes[0]->business_ad_limit;?>" id="business_ad_limit" required="required" name="business_ad_limit">
					
                    </div>
					</div>
					
					</div>
					</div>
					
					<h3 class="custom_priceTag"><?php echo _("Business Account")?></h3>
					<div class="custom_alert">
					<div class="row">
					
					<div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("Allow SMS Panel")?></small> </b>
                    <select class="form-control"  id="site_smspanel" name="site_smspanel" >
					     <option value="">--<?php echo _("Select option")?>--</option>
						 <option value="1" <?php if(@$appRes[0]->sms_panel=='1'){echo _("selected");}?>><?php echo _("Yes (Users can access SMS panel")?>)</option>
						 <option value="0" <?php if(@$appRes[0]->sms_panel=='0'){echo _("selected");}?>><?php echo _("No (Restricted")?>)</option>
					  </select>
                    </div>
					</div>
					
					<div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("Allow Email Panel")?></small> </b>
                    <select class="form-control"  id="site_emailpanel" name="site_emailpanel" >
					     <option value="">--<?php echo _("Select option")?>--</option>
						 <option value="1" <?php if(@$appRes[0]->email_panel=='1'){echo _("selected");}?>><?php echo _("Yes (Users can access Email panel")?>)</option>
						 <option value="0" <?php if(@$appRes[0]->email_panel=='0'){echo _("selected");}?>><?php echo _("No (Restricted")?>)</option>
					  </select>
                    </div>
					</div>
					</div>
					</div>
					
					<h3 class="custom_priceTag"><?php echo _("Validation Code")?></h3>
					<div class="custom_alert">
					<div class="row">
					<div class="col-lg-4 col-md-4">
					<div class="form-group">
                    <b><small><?php echo _("Min Value")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 1000') ?>" class="form-control"  value="<?php echo @$appRes[0]->vcode_min_val;?>" id="vcode_min_val" required="required" name="vcode_min_val">
                    </div>
					</div>
					
					<div class="col-lg-4 col-md-4">
					<div class="form-group">
                    <b><small><?php echo _("Max Value")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 50000') ?>" class="form-control"  value="<?php echo @$appRes[0]->vcode_max_val;?>" id="vcode_max_val" required="required" name="vcode_max_val">
                    </div>
					</div>
					
					<div class="col-lg-4 col-md-4">
					<div class="form-group">
                    <b><small><?php echo _("Join String")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. OCL') ?>" class="form-control"  value="<?php echo @$appRes[0]->vcode_string;?>" id="vcode_string" required="required" name="vcode_string">
                    </div>
					</div>
					</div>
					</div>
					
					<h3 class="custom_priceTag"><?php echo _("Wallet Item Code")?></h3>
					<div class="custom_alert">
					<div class="row">
					<div class="col-lg-4 col-md-4">
					<div class="form-group">
                    <b><small><?php echo _("Min Value")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 1000') ?>" class="form-control"  value="<?php echo @$appRes[0]->wallet_min_val;?>" id="wallet_min_val" required="required" name="wallet_min_val">
                    </div>
					</div>
					
					<div class="col-lg-4 col-md-4">
					<div class="form-group">
                    <b><small><?php echo _("Max Value")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 50000') ?>" class="form-control"  value="<?php echo @$appRes[0]->wallet_max_val;?>" id="wallet_max_val" required="required" name="wallet_max_val">
                    </div>
					</div>
					
					<div class="col-lg-4 col-md-4">
					<div class="form-group">
                    <b><small><?php echo _("Join String")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. OCL') ?>" class="form-control"  value="<?php echo @$appRes[0]->wallet_string;?>" id="wallet_string" required="required" name="wallet_string">
                    </div>
					</div>
					</div>
					</div>
					<br>
					
					<div class="form-group">
                    <label><?php echo _("UserId (Keygen Secondary Value")?>) </label> <small class="red" >*</small>
                    <input type="text" placeholder="<?php echo _('E.g. 30') ?>" class="form-control"  value="<?php echo @$appRes[0]->user_keygen;?>" id="site_userkey" required="required" name="site_userkey">
					<small class="red">[<?php echo _("If you dont aware this please leave it like this")?>]</small>
                    </div>
					
					<div class="form-group">
                    <label><?php echo _("PayAd Expire Days")?></label> <small class="red" >*</small>
                    <input type="text" placeholder="<?php echo _('E.g. 30') ?>" class="form-control"  id="site_payad_expires" value="<?php echo @$appRes[0]->payad_expire_days;?>" required="required" name="site_payad_expires">
					<small>[<?php echo _("Set \"0\" for unlimited days")?>] <a href="#"><?php echo _("What is PayAd ?")?></a> </small>
                    </div>
					
                    <div>
                    <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='AddSiteSettings()'>
					<i class="fa fa-save"></i> <?php echo _("Save")?></button>
				   </div>
                </form>
            	</div>
            	<div id="rsDiv"></div>
				<br>
			  </div>
            </div>
            
			<div class="col-md-4">
             <div class="widget-user-header bg-info">
			  <div class="widget-user">
				<img src="<?php echo base_url()?>theme4.0/admin/images/twilio_png.png" width="250"/>
			  </div><!-- /.widget-user-image -->
			    <div class="text-center">
				<a href="https://www.twilio.com/login" target="_blank" class="btn btn-sm btn-info"><?php echo _("Logon to Twilio Account")?></a><br><br>
				</div>
			</div>
			<br>
			
               <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-primary">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i> <?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?><span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?><span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Enable payment methods as you required.")?></li>
				 <li><?php echo _("Validation code should be by given parameters if you don't aware of this please leave it same.")?></li>
				</ul>
               
                        
              </div>
			</div>
		    </div>
           
        </section><!-- /.content -->
		
		
<?php $this->load->view('theme/footer.php');?>
		
