<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
	
<script type="application/javascript" src="<?php echo base_url();?>js/ad_feature_settings.js"></script>
<?php 
 
 @$appRes=$this->app_settings_model->get_payment_settings();
 if($this->input->get('tId')!=''){
 @$aFInfo = $this->app_settings_model->get_ad_features_byId($this->input->get('tId'));
 }
 
?>


 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo _("Ad Feature Settings")?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>settings/site"><?php echo _("Settings")?></a></li> <li class="active"><?php echo _("Ad Feature Settings")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div ng-app="AFSettingsApp">
        
          
            <div class="row">
            <div class="col-lg-4 col-md-4">
			<div class="box">
            <div class="box-body">
			     <div class="nav-tabs-custom">
               
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
           
                <form role="form" ng-controller="AFSettingsController" name="AFSettingsForm" id="AFSettingsForm">
					
					<?php 
					
					if($this->input->get('tId')!='')
					{
						$refCode=$aFInfo[0]->featured_code;
						//$randVal=rand(1000,900000);$refCode="OCAF".$randVal;
						
					}else{
					$randVal=rand(1000,900000);$refCode="OCAF".$randVal;
					}
					?>
						<div class="form-group">
						<label><?php echo _("Featured Code")?></label> <small class="red" >*</small>
						<input type="text" class="form-control" id="af_code" name="af_code" value="<?php echo @$refCode;?>" placeholder="<?php echo _('E.g. OCAF0099') ?>" required="required" readonly/>
						</div>
					
						
						<div class="form-group">
						<label><?php echo _("Feature Name")?></label> <small class="red" >*</small>
						<input type="text" class="form-control" id="af_name" name="af_name" value="<?php echo @$aFInfo[0]->feature_name;?>" placeholder="<?php echo _('E.g. Professional Services') ?>" required="required"/>
						</div>
					
					
						
						<div class="form-group">
						<label><?php echo _("Price Tag")?></label> <small class="red" >*</small>
						<input type="text" class="form-control" id="price_tag" name="price_tag" value="<?php echo @$aFInfo[0]->price_tag;?>" placeholder="<?php echo _('E.g. 50') ?>" required="required"/>
						</div>
					
					
					   
					   <div class="form-group">
						<label><?php echo _("Feature Mapping")?></label> <small class="red" >*</small>
						<div class="custom_alert">
						 
						 <?php
						 $maps=array('f1','f2','f3','f4');
						 
						  for($i=0;$i<4;$i++){
							  ?>
							  <input type="radio" name="af_mapping" id="af_mapping<?php echo $i?>" value="<?php echo $maps[$i];?>">
							  
							  <?php
							  echo $maps[$i]."&nbsp;";
						  }
						 ?>
						 </div>
						</div>
						
						<div class="form-group">
						<label><?php echo _("Status")?></label>
						 <select class="form-control" name="af_status" id="af_status">
						  <option value="1"><?php echo _("Active")?></option>
						  <option value="0"><?php echo _("Inactive")?></option>
						 </select>
						</div>
						
                   
					
				
						<div class="form-group">
						<label><?php echo _("Description")?></label>
						 <textarea class="form-control" id="af_desc" name="af_desc"><?php echo @$aFInfo[0]->feature_desc;?></textarea>
						</div>
						
					
                    <small class="red" >* = <?php echo _("Mandatory Fields")?></small>
					<br><br>
                    <div class="row">
                        <!--<button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="button" ng-click='AddSgIn();' data-toggle="modal" data-target="#SgAddModal"><strong><?php echo _("Add")?></strong>
                        </button>-->
						<div class="col-lg-3">
                        <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="button" ng-click='AddAFSettings();'>
						<?php 
						if($this->input->get('tId')!='')
						{
							?>
							
							<strong><?php echo _("Update")?></strong>
						<?php 
						}else{
							?>
							<strong><?php echo _("Save")?></strong>
							<?php
						}
						?>
                        </button>
						</div>
						<div class="col-lg-3">
						
						<?php 
						if($this->input->get('tId')!='')
						{
							?>
							<a href="<?php echo base_url()?>settings/account_classes" class="btn btn-sm btn-default pull-left m-t-n-xs"><?php echo _("Cancel")?></a>
							<?php
						}
						?>
						</div>
                       
                    </div>
                </form>
                </div>
				
				</div>
				</div>
            </div>
				<div id="rsDiv"></div>
			     
			  </div>
			  	<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Telephone is either mobile (or) land line number , along with country code.")?></li>
				</ul>
                        
            </div>
          
			<div class="col-lg-8 col-md-8">
			
        <div ng-controller="AFSettingsCrtl">
          <div class="box">
                <div class="box-header">
            <div class="row">
            <div class="col-md-2">
            <select ng-model="entryLimit" class="form-control">
            <option>5</option>
            <option>10</option>
            <option>20</option>
            <option>50</option>
            <option>100</option>
            </select>
			</div>
			
			<div class="btn-group pull-left" style="padding-left:5px">
			<a href="/users/export_users_to_excel" class="btn btn-default btn-sm" style="border:1px solid #ddd;height:35px" title="<?php echo _('Export To Excel') ?>">
			 <img src="<?php echo base_url()?>images/icons/xls.png" border="0">
			</a>
			<button class="btn btn-default btn-sm" style="border:1px solid #ddd;height:35px" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			<img src="<?php echo base_url()?>images/icons/print_16.png">
			</button>
			</div>
			
            <div class="col-md-3 pull-right">
            <input type="text" ng-model="search" ng-change="filter()" placeholder="<?php echo _('Filter') ?>" class="form-control" />
			</div>
            <div class="col-md-3 pull-right">
             <h5>Filtered {{ filtered.length }} of {{ totalItems}} total features</h5>
            </div>
            </div>
           </div>
		   
		    <div class="box-body">
            <div class="row">
            <div class="col-md-12" ng-show="filteredItems > 0">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
            <th><?php echo _("ID")?>&nbsp;<a ng-click="sort_by('feature_id');"><i class="fa fa-arrows-v"></i></a></th>
            <th><?php echo _("Feature Code")?>&nbsp;<a ng-click="sort_by('featured_code');"><i class="fa fa-arrows-v"></i></a></th>
            <th><?php echo _("Feature Name")?>&nbsp;</th>
            <th><?php echo _("Description")?></th>
            <th><?php echo _("Price Tag")?></th>
			<th><?php echo _("F1")?></th>
			<th><?php echo _("F2")?></th>
			<th><?php echo _("F3")?></th>
			<th><?php echo _("F4")?></th>
            <th><?php echo _("Status")?></th>
			<th><?php echo _("Actions")?></th>
            
            </thead>
            <tbody>
            <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
            <td>{{data.feature_id}}</td>
            <td width="15%">{{data.featured_code}} </td>
            <td>{{data.feature_name}}</td>
            <td>{{data.feature_desc}}</td>
            <td>{{data.price_tag}}</td>
			
			<td ng-if="data.f1==1"><i class="fa  fa-check-circle"></i></td>
			<td ng-if="data.f1==0"><i class="fa fa-close"></i></td>
			
			<td ng-if="data.f2==1"><i class="fa  fa-check-circle"></i></td>
			<td ng-if="data.f2==0"><i class="fa fa-close"></i></td>
			
			<td ng-if="data.f3==1"><i class="fa  fa-check-circle"></i></td>
			<td ng-if="data.f3==0"><i class="fa fa-close"></i></td>
			
			<td ng-if="data.f4==1"><i class="fa  fa-check-circle"></i></td>
			<td ng-if="data.f4==0"><i class="fa fa-close"></i></td>
			
            <td ng-if="data.status=='1'">
            <span class="label label-primary"><?php echo _("Active")?></span>
            </td>
            <td ng-if="data.status!='1'">
            <span class="label label-warning"><?php echo _("InActive")?></span>
            </td>
            
			<td>
			<div class="col-md-4">
            <a href="<?php echo base_url()?>settings/ad_feature?tId={{data.feature_id}}" title="<?php echo _('Edit') ?>" class='btn btn-xs btn-default'>
            <i class="fa fa-pencil"></i> 
            </a>
			</div>
            <div class="col-md-4">
           
		   <span ng-controller="delAFController" >
            <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" ng-click="delAC(data.feature_id)" class='btn btn-xs btn-danger' >
             <div id="complexConfirm{{data.feature_id}}"><i class='glyphicon glyphicon-trash'></i>  </div>
            </a>
            </span>
			</div>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
            <div class="col-md-12" ng-show="filteredItems == 0">
            <div class="col-md-12">
            <h4><?php echo _("No Ad features found.")?></h4>
            </div>
            </div>
            <div class="col-md-12" ng-show="filteredItems > 0">    
            <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;"></div>
            
            </div>
            </div>
			
            </div>
              </div>
			  </div>
			  
			
              </div>
				
			</div>
			
			
            </div>
           
        </section><!-- /.content -->
		
		
<?php $this->load->view('theme/footer.php');?>
		
