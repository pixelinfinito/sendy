<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
<script type="application/javascript" src="<?php echo base_url();?>js/sms_settings.js"></script>
<?php 
 @$appRes=$this->app_settings_model->get_sms_settings();
 $total_members=$this->account_model->list_all_members();
 $inactive_members=$this->account_model->list_inactive_members();
 
?>
 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo _("Global SMS/Call Settings")?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>settings/site"><?php echo _("Settings")?></a></li> <li class="active"><?php echo _("SMS/Call Settings")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div ng-app="">
            <div class="row">
            <div class="col-lg-8 col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			 
   			 <form role="form" ng-controller="SmsSettingsController" name="SMSSettingsForm" id="SMSSettingsForm">
				   <h3 class="custom_priceTag"><?php echo _("Twilio SMS")?></h3>
				   <div class="custom_alert">
					<div class="row">
					<div class="col-lg-6">
                    <div class="form-group">
					
                    <strong><small><?php echo _("Account SID")?></small></strong> <small class="red" >*</small>
					<input type="text"  class="form-control" value="<?php echo @$appRes[0]->twilio_accid;?>" id="sc_acc_id" required="required" name="sc_acc_id">
					 
                    </div>
					</div>
					<div class="col-lg-6">
					<div class="form-group">
					<strong><small><?php echo _("Authentication Code")?></small></strong> <small class="red" >*</small>
					<input type="text"  class="form-control" value="<?php echo @$appRes[0]->twilio_authtoken;?>" id="sc_auth_token" required="required" name="sc_auth_token"> 
					</div>
					</div>
                    </div>
					 
				    <div class="row">
					<div class="col-lg-6">
                    <div class="form-group">
					
                    <strong><small><?php echo _("Origin Number")?></small></strong> <small class="red" >*</small>
					<input type="text"  class="form-control" value="<?php echo @$appRes[0]->twilio_origin_number;?>" id="sc_origin_number" required="required" name="sc_origin_number">
					 
                    </div>
					</div>
					
					<div class="col-lg-6">
					<div class="form-group">
					<strong><small><?php echo _("Default Sender ID")?></small></strong>
					<input type="text"  class="form-control" placeholder="<?php echo _('E.g OneSms') ?>" value="<?php echo @$appRes[0]->twilio_sender_id;?>" id="sc_sender_id"  name="sc_sender_id"> 
					</div>
					</div>
				   </div>
				   </div>
					
					 
				    <h3 class="custom_priceTag"><?php echo _("Campaign Token")?></h3>
				   <div class="custom_alert">
					<div class="row">
					<div class="col-lg-6">
                    <div class="form-group">
					
                    <strong><small><?php echo _("Min Value")?></small></strong> <small class="red" >*</small>
					<input type="text"  class="form-control" placeholder="<?php echo _('E.g 1111') ?>" value="<?php echo @$appRes[0]->campaign_minval;?>" id="campaign_minval" required="required" name="campaign_minval">
					
                    </div>
					</div>
					<div class="col-lg-6">
					<div class="form-group">
					<strong><small><?php echo _("Max Value")?></small></strong>  <small class="red" >*</small>
					<input type="text"  class="form-control" placeholder="<?php echo _('E.g 9999') ?>" value="<?php echo @$appRes[0]->campaign_maxval;?>" id="campaign_maxval" required="required" name="campaign_maxval"> 
					</div>
					</div>
                    </div>
					</div>
					
					<h3 class="custom_priceTag"><?php echo _("Masking Alerts")?></h3>
				   <div class="custom_alert">
					<div class="row">
					<div class="col-lg-6">
                    <div class="form-group">
					
                    <strong><small><?php echo _("Campaign Alerts")?></small></strong>
					<input type="text"  class="form-control" placeholder="<?php echo _('E.g CmpAlerts') ?>" value="<?php echo @$appRes[0]->cmp_alert_token;?>" id="cmp_alerts"  name="cmp_alerts">
					
                    </div>
					</div>
					<div class="col-lg-6">
					<div class="form-group">
					<strong><small><?php echo _("Caller Number Alerts")?></small></strong>
					<input type="text"  class="form-control" placeholder="<?php echo _('E.g CNAlerts') ?>" value="<?php echo @$appRes[0]->cn_alert_token;?>" id="cn_alerts"  name="cn_alerts"> 
					</div>
					</div>
                    </div>
					
					<div class="row">
					<div class="col-lg-6">
                    <div class="form-group">
					
                    <strong><small><?php echo _("SenderID Alerts")?></small></strong>
					<input type="text"  class="form-control" placeholder="<?php echo _('E.g SIDAlerts') ?>" value="<?php echo @$appRes[0]->sid_alert_token;?>" id="sid_alerts"  name="sid_alerts">
					
                    </div>
					</div>
					<div class="col-lg-6">
					<div class="form-group">
					<strong><small><?php echo _("Payment Alerts")?></small></strong>
					<input type="text"  class="form-control" placeholder="<?php echo _('E.g PayAlerts') ?>" value="<?php echo @$appRes[0]->pay_alert_token;?>" id="pay_alerts"  name="pay_alerts"> 
					</div>
					</div>
                    </div>
					
					<div class="row">
					<div class="col-lg-6">
                    <div class="form-group">
					<strong><small><?php echo _("SenderId Approval Alerts")?></small></strong>
					<input type="text"  class="form-control" placeholder="<?php echo _('E.g OTGAlerts') ?>" value="<?php echo @$appRes[0]->sid_approve_alert_token;?>" id="sid_approval_alerts"  name="sid_approval_alerts">
					
                    </div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
						<strong><small><?php echo _("Member's Block/Unblock Alerts")?></small></strong>
						<input type="text"  class="form-control" placeholder="<?php echo _('E.g OTGAlerts') ?>" value="<?php echo @$appRes[0]->blacklist_alerts;?>" id="blacklist_alerts"  name="blacklist_alerts">
						</div>
					</div>
                    </div>
					
					<div class="row">
					<div class="col-lg-10">
					  <input type="checkbox" id="enable_alerts" name="enable_alerts"  <?php if($appRes[0]->enable_alerts==1){echo _("checked");}?> /> <?php echo _("Use Default Sender ID (for blank fields")?>) <br>
					  <input type="hidden" id="hid_enable_alerts" name="hid_enable_alerts">
					  <small class="text-muted">
					   <?php echo _("Alerts can't send if resective field leave it in blank, e.g. if <b>Campaign Alerts</b> is empty then you won't reveice
					   alert against campaigns. If you click checkbox system will send alerts by using")?> <b><?php echo _("Default SenderID")?> </b>.
					  </small>
					</div>
					<div class="col-lg-2">
					  &nbsp;
					</div>
                    </div>
					<br>
					</div><br>
					 
					  
					<div class="form-group">
					<strong><?php echo _("Credit Threshold (Recall Interval")?>)</strong> <small class="red" >*</small>
					<input type="text"  class="form-control" value="<?php echo $appRes[0]->credit_threshold_interval;?>" id="credit_threshold_recall" required="required" name="credit_threshold_recall"> 
					<small><?php echo _("To send next credit threshold alert in given interval")?> <b class="text-muted"><em>(<?php echo _("Note : Interval in minutes e.g. 3000")?>)</em></b></small>
					</div>
					
					 <div class="form-group">
					<strong><?php echo _("Service Recall Interval")?></strong> <small class="red" >*</small>
					<input type="text"  class="form-control" value="<?php echo @$appRes[0]->service_recall_interval;?>" id="service_recall" required="required" name="service_recall"> 
					</div>
					
					 <div class="form-group">
					<strong><?php echo _("Resend Password Interval")?></strong> <small class="red" >*</small>
					<input type="text"  class="form-control" value="<?php echo @$appRes[0]->password_resend_interval;?>" id="sc_resend_time" required="required" name="sc_resend_time"> 
					</div>
					<br>
					<div>
					 <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='SiteSmsBtn()'>
					 <i class="fa fa-save"></i> <?php echo _("Save")?> </button>
					</div>
					
                </form>
				 <div id="rsDiv"></div>
				 <br>
            </div>
		    </div>

			</div>
			<div class="col-md-4">
                  <div class="widget-user-header bg-info">
			  <div class="widget-user">
				<img src="<?php echo base_url()?>theme4.0/admin/images/twilio_png.png" width="250"/>
			  </div><!-- /.widget-user-image -->
			    <div class="text-center">
				<a href="https://www.twilio.com/login" target="_blank" class="btn btn-sm btn-info"><?php echo _("Logon to Twilio Account")?></a><br><br>
				</div>
			</div>
			<br>
			
               <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-primary">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i> <?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?>	<span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Origin number should valid number.")?></li>
				 <li><?php echo _("Resend password interval in seconds, where member able to get resend password form in given interval.")?></li>
				</ul>
              </div>
				
			</div>
		    </div>
           
        </section><!-- /.content -->
		
		
<?php $this->load->view('theme/footer.php');?>
		
