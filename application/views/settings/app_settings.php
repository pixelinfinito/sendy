<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
<script type="application/javascript" src="<?php echo base_url();?>js/app_settings.js"></script>
<?php 
 
 @$appRes=$this->app_settings_model->get_app_settings();
 @$getTimezones= $this->countries_model->list_country_timezone();
 @$getCountries= $this->countries_model->list_countries();
 $total_members=$this->account_model->list_all_members();
 $inactive_members=$this->account_model->list_inactive_members();
?>


 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo _("App Settings")?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>settings/app"><?php echo _("Settings")?></a></li> <li class="active"><?php echo _("App Settings")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div ng-app="">
            <div class="row">
            <div class="col-lg-8 col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			    <form role="form" ng-controller="AppSettingsController" name="AppSettingsForm" id="AppSettingsForm">
				<h3 class="custom_priceTag"><?php echo _("Timezone")?></h3>
				<div class="custom_alert">
				<div class="row">
				 <div class="col-lg-6">
				    <div class="form-group">
				    <b><small><?php echo _("Timezone")?><span class="red">*</span></small></b>
					<?php 
					
					  if(count($getTimezones)>0){
						  ?>
						  <select class="form-control"  id="app_timezone" name="app_timezone" >
						  <option value="">--<?php echo _("Select option")?>--</option>
						   <?php 
						    for($i=0;$i<count($getTimezones);$i++){
								?>
								<option value="<?php echo $getTimezones[$i]->zone_name;?>" <?php if($getTimezones[$i]->zone_name==@$appRes[0]->app_timezone){echo 'selected';}?>><?php echo $getTimezones[$i]->zone_name;?></option>
								<?php
							}
						   ?>
						  </select>
						  <?php
					  }
					?>
                    </div>
					</div>
					
				   <div class="col-lg-6">
					<div class="form-group">
					<?php $remoteCountry=$this->config->item('remote_country_code');?>
                    <b><small><?php echo _("Country")?><span class="red">*</span></small></b>
					<?php 
					  if(count($getCountries)>0){
						  ?>
						  <select class="form-control"  id="app_country" name="app_country" required>
						  <option value="">--<?php echo _("Select option")?>--</option>
						   <?php 
						    for($i=0;$i<count($getCountries);$i++){
								?>
								<option value="<?php echo $getCountries[$i]->country_code;?>" <?php if($getCountries[$i]->country_code==@$appRes[0]->app_country){echo 'selected';}?>><?php echo $getCountries[$i]->country_name;?></option>
								<?php
							}
						   ?>
						  </select>
						  <?php
					  }
					?>
                    </div>
					</div>
					</div>
				</div>
					<br>
				<div class="row">
				 <div class="col-lg-6">	
					<div class="form-group">
                    <label class="text-primary"><?php echo _("Currency")?></label> <small class="red" style="font-size:14px">*</small>
                    <input type="text" placeholder="<?php echo _('E.g. USD') ?>" class="form-control"  value="<?php echo @$appRes[0]->app_currency;?>" id="app_currency" required="required" name="app_currency">
                    </div>
				 </div>
				 
				 <div class="col-lg-6">
				   <div class="form-group">
                    <label class="text-primary"><?php echo _("Default AppName")?></label> <small class="red" style="font-size:14px">*</small>
                    <input type="text" placeholder="<?php echo _('E.g. OneTextGlobal') ?>" class="form-control"  value="<?php echo @$appRes[0]->app_default_name;?>" id="app_default_name" required="required" name="app_default_name">
                    </div>
				 </div>
				 </div>
					
				<h3 class="custom_priceTag"><?php echo _("Default")?></h3>
				<div class="custom_alert">
				<div class="row">
				 <div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("Default From Mail")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. alerts@domain.com') ?>" class="form-control"  value="<?php echo @$appRes[0]->default_from_mail;?>" id="app_from_mail" required="required" name="app_from_mail">
                    </div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("Default Mobile Number")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. +673xxxxxx') ?>" class="form-control"  value="<?php echo @$appRes[0]->default_mobile_number;?>" id="app_mobile_number"  name="app_mobile_number" required="required">
					<small><?php echo _("This number(s) will receive alerts on member activities")?></small>
                    </div>
				</div>
				</div>
				</div>
				
				<h3 class="custom_priceTag"><?php echo _("Support")?></h3>
				<div class="custom_alert">
				<div class="row">
				 <div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("Support Email")?></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. support@domain.com') ?>" class="form-control"  value="<?php echo @$appRes[0]->default_support_email;?>" id="app_support_email" name="app_support_email">
                    </div>
				</div>
				 <div class="col-lg-6">
					<div class="form-group">
                     <b><small><?php echo _("Support Contact Number")?></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. +673xxxxxx') ?>" class="form-control"  value="<?php echo @$appRes[0]->default_support_numbers;?>" id="app_support_number"  name="app_support_number">
                    </div>
				</div>
				</div>
				</div>
				<br>
					
				<h3 class="custom_priceTag"><?php echo _("SMTP Settings")?></h3>
				<div class="custom_alert">
				<div class="row">
				 <div class="col-lg-6">
				 
					<div class="form-group">
                    <b><small><?php echo _("Hostname")?><span class="red">*</span> </small></b>
                    <input type="text" placeholder="<?php echo _('E.g. smtp.sybaseinfotech.com (or) ip adress') ?>" class="form-control" value="<?php echo @$appRes[0]->smtp_host;?>" id="smtp_host" required="required" name="smtp_host">
                    </div>
				</div>
				 <div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("Email Id")?></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. abc@domain.com') ?>" class="form-control"  id="smtp_username" value="<?php echo @$appRes[0]->smtp_user;?>" name="smtp_username">
                    </div>
				</div>
				 <div class="col-lg-6">
					<div class="form-group">
                   <b> <small><?php echo _("Password")?></small> </b>
                    <input type="password" placeholder="<?php echo _('E.g. xxxxxxxx') ?>" class="form-control"  id="smtp_password" value="<?php echo @$appRes[0]->smtp_password;?>" name="smtp_password">
                    </div>
					<small><?php echo _("This password is your smtp mail(abc@domain) password")?></small><br>
				 </div>
				 <div class="col-lg-6">
					<div class="form-group">
                    <b> <small><?php echo _("Port No")?><span class="red">*</span></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 25') ?>" class="form-control"  id="smtp_port" value="<?php echo @$appRes[0]->smtp_port;?>" required="required" name="smtp_port">
                    </div>
				 </div>
				 </div>
				 </div>
				 <br>
				<div class="form-group">
				<label class="text-primary"><?php echo _("Upgrade Account")?></label>
				  <select class="form-control" id="app_upgrade_option" name="app_upgrade_option">
				   <option value="0" <?php if($appRes[0]->upgrade_option==0){echo _("selected");}?>>
				   <?php if($appRes[0]->upgrade_option==0){echo _("Disabled");}else{echo _("Make it Disable");}?>
				   </option>
				   <option value="1" <?php if($appRes[0]->upgrade_option==1){echo _("selected");}?>>
				   <?php if($appRes[0]->upgrade_option==1){echo _("Enabled");}else{echo _("Make it Enable");}?>
				   </option>
				  </select>
				</div>
				
				<div class="form-group">
				<label class="text-primary"><?php echo _("Help Text")?></label> <small class="red">*</small>
				<textarea placeholder="<?php echo _('E.g. We\'re happy to help you call us at +673xxxxxx') ?>" class="form-control"  id="app_helptext" required="required" name="app_helptext"><?php echo $appRes[0]->help_text;?></textarea>
				</div>
				
				<h3 class="custom_priceTag"><?php echo _("Branding")?></h3>
				<div class="custom_alert">
				<div class="row">
				 <div class="col-lg-6">
					<div class="form-group">
                    <b><small><?php echo _("Powered By")?></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. Abc Ltd') ?>" class="form-control"  value='<?php echo $appRes[0]->branded_by;?>' id="app_branded_by"  name="app_branded_by">
                    </div>
				</div>
				 <div class="col-lg-6">
					<div class="form-group">
                     <b><small><?php echo _("Copyrights Text")?></small></b>
                    <input type="text" placeholder="<?php echo _('E.g. 2017 Copyrights Abc Ltd') ?>" class="form-control"  value="<?php echo $appRes[0]->brand_copyrights;?>" id="app_copyrights_text"  name="app_copyrights_text">
                    </div>
				</div>
				</div>
				</div>
				<br>
				<h3 class="custom_priceTag"><?php echo _("Social Media Links")?></h3>
				<div class="form-group">
				<label><?php echo _("Facebook")?></label>
				<input type="text" placeholder="<?php echo _('E.g. http://www.facebook.com/brandname') ?>" class="form-control"  id="fb_link"  name="fb_link" value='<?php echo $appRes[0]->fb_social_link;?>'>
				</div>
				
				<div class="form-group">
				<label><?php echo _("Twitter")?></label>
				<input type="text" placeholder="<?php echo _('E.g. http://www.twitter.com/brandname') ?>" class="form-control"  id="twitter_link"  name="twitter_link" value='<?php echo $appRes[0]->twitter_social_link;?>'>
				</div>
				
				<div class="form-group">
				<label><?php echo _("Youtube")?></label>
				<input type="text" placeholder="<?php echo _('E.g. http://www.youtube.com/brandname') ?>" class="form-control"  id="youtube_link"  name="youtube_link" value='<?php echo $appRes[0]->youtube_social_link;?>'>
				</div>
				
				
				 <br>
				<div>
					<button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='AddAppSettings()'>
					<i class="fa fa-save"></i> <?php echo _("Save")?></button>
				</div>
				
                </form>
                </div>
		    	<div id="rsDiv"></div>	
				<br>
				</div>
		    </div>
			<div class="col-md-4">
                       
			<div class="widget-user-header bg-info">
			  <div class="widget-user">
				<img src="<?php echo base_url()?>theme4.0/admin/images/twilio_png.png" width="250"/>
			  </div><!-- /.widget-user-image -->
			    <div class="text-center">
				<a href="https://www.twilio.com/login" target="_blank" class="btn btn-sm btn-info"><?php echo _("Logon to Twilio Account")?></a><br><br>
				</div>
			</div>
			<br>
			
               <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-primary">
                  <div class="widget-user">
                    <h3><i class="fa fa-user-plus"></i> <?php echo _("Total Members")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?><span class="pull-right badge bg-blue">
						<?php 
						 if($total_members!=0){
						  if(count($total_members)>10){
							  echo count($total_members);
						  }else{
							  echo "0".count($total_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?><span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_members!=0){
						  if(count($inactive_members)>10){
							  echo count($inactive_members);
						  }else{
							  echo "0".count($inactive_members);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Default from mail should be valid.")?></li>
				 <li><?php echo _("SMTP settings based on your hosting provider.")?></li>
				 <li><?php echo _("You can add multiple contact numbers seperated by comma, e.g +673xxxxxx,+91999xxxxxx")?></li>
				</ul>
                        
              </div>
			</div>
			</div>
           
        </section><!-- /.content -->
		
		
<?php $this->load->view('theme/footer.php');?>
		
