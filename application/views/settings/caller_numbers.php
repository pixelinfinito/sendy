<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
	
<script type="application/javascript" src="<?php echo base_url();?>js/caller_number_settings.js"></script>
<?php 
 @$countryInfo=$this->countries_model->list_countries();
 @$appRes=$this->app_settings_model->get_sms_prices();
 if($this->input->get('cId')!=''){
 @$cRInfo = $this->app_settings_model->get_caller_number_byid($this->input->get('cId'));
 }
 
?>


 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo _("Caller Number Settings")?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>settings/site"><?php echo _("Settings")?></a></li> <li class="active"><?php echo _("Caller Number Settings")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          
            <div class="row">
            <div class="col-lg-4 col-md-4" ng-app="">
			<div class="box padding_20">
            <div class="box-body">
			    <form role="form" ng-controller="CallerNumberCrtl" name="CallerNumberForm" id="CallerNumberForm">
					  <div class="form-group">
							<label><?php echo _("Call Number Type")?></label> <small class="red" >*</small>
							<select id="cn_type" name="cn_type" class="form-control">
							<option value="">--<?php echo _("Select Type")?>--</option>
							
							<option value="1" <?php if(@$cRInfo[0]->cn_type=='1'){echo _("selected");}?>>
							 <?php echo _("Standard Number")?></option>
							
							<option value="2" <?php if(@$cRInfo[0]->cn_type=='2'){echo _("selected");}?>>
							 <?php echo _("Tollfree  Number")?></option>
							
							<option value="3" <?php if(@$cRInfo[0]->cn_type=='3'){echo _("selected");}?>>
							 <?php echo _("Special  Number")?></option>
							
							</select>
						</div>
						
						<div class="form-group">
						    
							<label><?php echo _("CN Period")?></label> <small class="red" >*</small>
							<select id="cn_period" name="cn_period" class="form-control" required>
							<option value="">--<?php echo _("Select Period")?>--</option>
							
							<?php 
							 for($i=1;$i<=12;$i++){
								 ?>
						    <option value="<?php echo $i;?>" <?php if(@$cRInfo[0]->cn_period==$i){echo _("selected");}?>>
							  <?php echo sprintf(_("Per [%d] Month(s)"),$i);?>
							</option>
								 <?php
							 }
							?>
							
							</select>
							<small><?php echo _("Caller number charges per (n) months")?></small>
						</div>
						
					<?php 
					if($this->input->get('cId')!='')
					{
						 ?>
						 <input type="hidden" value="<?php echo $this->input->get('cId');?>" id="cn_hidId" name="cn_hidId" class="form-control">
						<div class="form-group">
						
							<label><?php echo _("Country")?></label> <small class="red" >*</small>
							<select id="cn_country" name="cn_country" class="form-control">
							<option value="">--<?php echo _("Select country")?>--</option>
							<?php 
							for($c=0;$c<count($countryInfo);$c++){
							?>
							<option value="<?php echo $countryInfo[$c]->country_code;?>" 
							<?php if($cRInfo[0]->cn_country==$countryInfo[$c]->country_code){echo _("selected");}?>>
							<?php echo $countryInfo[$c]->country_name;?>
							</option>
							<?php
							}
							?>
							</select>
						</div>
						 <?php
						
					}else{
					    
						?>
						<input type="hidden" value="" id="cn_hidId" name="cn_hidId" class="form-control">
						<div class="form-group">
							<label><?php echo _("Country")?></label> <small class="red" >*</small>
							<select id="cn_country" name="cn_country" class="form-control">
							<option value="">--<?php echo _("Select country")?>--</option>
							<?php 
							for($c=0;$c<count($countryInfo);$c++){
							?>
							<option value="<?php echo $countryInfo[$c]->country_code;?>">
							<?php echo $countryInfo[$c]->country_name;?>
							</option>
							<?php
							}
							?>
							</select>
						</div>
					
						
						<?php
					}
					?>
						
					<div class="form-group">
					<label><?php echo _("Currency Code")?></label> <small class="red" >*</small>
					<input type="text" class="form-control" id="cn_currency_code" name="cn_currency_code" value="<?php if(@$cRInfo[0]->cn_currency!=''){echo @$cRInfo[0]->cn_currency;}else{echo 'USD';}?>" placeholder="" required="required"/>
					</div>
					
					<div class="form-group">
					<label><?php echo _("Price")?></label> <small class="red" >*</small>
					<input type="text" class="form-control" id="cn_price" name="cn_price" value="<?php echo @$cRInfo[0]->cn_price;?>" placeholder="<?php echo _('E.g. 50') ?>" required="required"/>
					</div>
				
				
					<div class="form-group">
					<label><?php echo _("Description")?></label>
					 <textarea class="form-control" id="cn_desc" name="cn_desc"><?php echo @$cRInfo[0]->cn_desc;?></textarea>
					</div>
					
					<br>
                    <div class="row">
                       <div class="col-lg-3">
                        <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='AddCNSettings()'>
						<?php 
						if($this->input->get('cId')!='')
						{
							?>
							
							<i class="fa fa-pencil"></i> <?php echo _("Update")?><?php
						}else{
							?>
							<i class="fa fa-plus-circle"></i> <?php echo _("Add")?><?php
						}
						?>
                        </button>
						</div>
						<div class="col-lg-3">
						
						<?php 
						if($this->input->get('cId')!='')
						{
							?>
							<a href="<?php echo base_url()?>settings/caller_numbers" class="btn btn-default pull-left m-t-n-xs"><?php echo _("Cancel")?></a>
							<?php
						}
						?>
						</div>
                       
                    </div>
                </form>
             </div>
				<div id="rsDiv"></div>
			  </div>
			  	<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Price should be a non string value.")?></li>
				  <li><?php echo _("Currency code should be international standard code.")?></li>
				</ul>
            </div>
          
			<div class="col-lg-8 col-md-8">
			
        <div ng-controller="cNListCtrl">
          <div class="box">
           <div class="box-header">
            <div class="row">
			   <div class="col-md-6">
					<a href="<?php echo base_url()?>site/export/export_groups" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
						<i class="fa fa-file-excel-o"></i>
					</a>
					<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
						<i class="fa fa-print"></i>
					</a>
			   </div>
			   <div class="col-md-6 text-right">
					 <a href="<?php echo base_url()?>settings/caller_numbers" class="btn btn-sm btn-info"  title="<?php echo _('Reload') ?>">
						<i class="fa fa-rotate-left"></i> <?php echo _("Reload")?></a>
			   </div>
			</div> 	
		   </div>
		   
		    <div class="box-body">
			<table class="table table-bordered table-striped" id="dataTables-cnPrices">
			<thead>
			<tr>
			<th class="col-sm-1"><?php echo _("ID")?></th>
            <th class="col-sm-1"><?php echo _("Country")?></th>
            <th class="col-sm-1"><?php echo _("Caller Number Type")?></th>
            <th class="col-sm-1"><?php echo _("Price")?></th>
            <th class="col-sm-1"><?php echo _("CN Period")?></th>
			<th class="col-sm-1"><?php echo _("Description")?></th>
            <th class="col-sm-1"><?php echo _("Actions")?></th>
			
		   </tr>
			</thead>
			<tbody>
			<?php 
			$price_info=$this->app_settings_model->get_merge_caller_numbers();
			$gp = 1 
			?>
			<?php if ($price_info!=0): foreach ($price_info as $price) : ?>

			<tr>
			<td>
			<?php echo $price->cn_id?>
			</td>
			<td><?php echo $price->country_name?>
			</td>
			<td>
			<?php
			echo $price->class_name 
			?>
			</td>
			<td><?php echo $price->cn_currency.' '.$price->cn_price ?></td>
			<td><?php echo $price->cn_period." <small>" . _("Month(s)") . "</small>" ?></td>
			<td><?php echo $price->cn_desc ?></td>
			
			<td>
				<a href="<?php echo base_url()?>settings/caller_numbers?cId=<?php echo $price->cn_id?>" title="<?php echo _('Edit') ?>" class='btn btn-xs btn-default'>
					<i class="fa fa-pencil"></i> 
				</a>
				
				<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delCNPricing('<?php echo $price->cn_id?>')" class='btn btn-xs btn-danger' >
				 <i class='glyphicon glyphicon-trash'></i>
				</a>
				
			</td>
			</tr>
			<?php
			$gp++;
			endforeach;
			?>
			<?php else : ?>
			<td colspan="6" class="text-center">
			<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
			</td>
			<?php endif; ?>
			</tbody>
			</table>
			
			</div>
		</div>
	</div>

</div>
</div>

<?php $this->load->view('settings/modals/delete_caller_price');?>
</section><!-- /.content -->

		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-cnPrices').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
		
    });
</script>
	
