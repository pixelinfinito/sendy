<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
<script type="application/javascript" src="<?php echo base_url();?>js/account_class_settings.js"></script>
<?php 
 @$appRes=$this->app_settings_model->get_payment_settings();
 if($this->input->get('tId')!=''){
	@$acInfo = $this->app_settings_model->get_acclass_settings($this->input->get('tId'));
 }
?>

 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <?php echo _("Account Class Settings")?></h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>settings/site"><?php echo _("Settings")?></a></li> <li class="active"><?php echo _("Account Class Settings")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           <div class="row">
            <div class="col-lg-4 col-md-4" ng-app="">
			<div class="box padding_20">
            <div class="box-body">
			    <form role="form" ng-controller="ACSettingsController" name="ACSettingsForm" id="ACSettingsForm">
					
					<?php 
					if($this->input->get('tId')!='')
					{
						$refCode=$acInfo[0]->item_number;
						
					}else{
					$randVal=rand(1000,900000);$refCode="OCSVR".$randVal;
					}
					?>
						<div class="form-group">
						<label><?php echo _("Service/Item Code")?></label> <small class="red" >*</small>
						<input type="text" class="form-control" id="ac_itemcode" name="ac_itemcode" value="<?php echo @$refCode;?>" placeholder="<?php echo _('E.g. OCSVR0099') ?>" required="required" readonly/>
						</div>
					
						
						<div class="form-group">
						<label><?php echo _("Service Name")?></label> <small class="red" >*</small>
						<input type="text" class="form-control" id="ac_name" name="ac_name" value="<?php echo @$acInfo[0]->acc_type;?>" placeholder="<?php echo _('E.g. Professional Services') ?>" required="required"/>
						</div>
						
						<div class="form-group">
						<label><?php echo _("Service Cost")?></label> <small class="red" >*</small>
						<input type="text" class="form-control" id="ac_cost" name="ac_cost" value="<?php echo @$acInfo[0]->acc_cost;?>" placeholder="<?php echo _('E.g. 50') ?>" required="required"/>
						</div>
						
						<div class="form-group">
						<label><?php echo _("Status")?></label>
						 <select class="form-control" name="ac_status" id="ac_status">
						  <option value="1"><?php echo _("Active")?></option>
						  <option value="0"><?php echo _("Inactive")?></option>
						 </select>
						</div>
						
						<div class="form-group">
						<label><?php echo _("Description")?></label>
						 <textarea class="form-control" id="ac_desc" name="ac_desc"><?php echo @$acInfo[0]->acc_desc;?></textarea>
						</div>
					
					 <br>
                    <div class="row">
                        <div class="col-lg-3">
                        <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='AddACSettings()'>
						<?php 
						if($this->input->get('tId')!='')
						{
							?>
							
							<i class="fa fa-pencil"></i> <?php echo _("Update")?><?php
						}else{
							?>
							<i class="fa fa-plus-circle"></i> <?php echo _("Add")?><?php
						}
						?>
                        </button>
						</div>
						<div class="col-lg-3">
						
						<?php 
						if($this->input->get('tId')!='')
						{
							?>
							<a href="<?php echo base_url()?>settings/account_classes" class="btn  btn-default pull-left m-t-n-xs"><?php echo _("Cancel")?></a>
							<?php
						}
						?>
						</div>
                       
                    </div>
                </form>
                
            </div>
				<div id="rsDiv"></div>
			     
			  </div>
			  	<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Please give some brief descrption about service, which wil be appear to member.")?></li>
				</ul>
                        
            </div>
          
			<div class="col-lg-8 col-md-8">
			
        <div ng-controller="ACSettingsCrtl">
          <div class="box">
            <div class="box-header">
            <div class="row">
			   <div class="col-md-6">
					<a href="<?php echo base_url()?>site/export/export_groups" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
						<i class="fa fa-file-excel-o"></i>
					</a>
					<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
						<i class="fa fa-print"></i>
					</a>
			   </div>
			   <div class="col-md-6 text-right">
					 <a href="<?php echo base_url()?>settings/account_classes" class="btn btn-sm btn-info"  title="<?php echo _('Reload') ?>">
						<i class="fa fa-rotate-left"></i> <?php echo _("Reload")?></a>
			   </div>
			</div> 	
           </div>
		   
		    <div class="box-body">
			
			<table class="table table-bordered table-striped" id="dataTables-acClasses">
			<thead>
			<tr>
			<th class="col-sm-1"><?php echo _("ID")?></th>
            <th class="col-sm-1"><?php echo _("Service/Item Code")?></th>
            <th class="col-sm-1"><?php echo _("Account Class")?></th>
            <th class="col-sm-1"><?php echo _("Description")?></th>
            <th class="col-sm-1"><?php echo _("Service Cost")?></th>
            <th class="col-sm-1"><?php echo _("Status")?></th>
			<th class="col-sm-1"><?php echo _("Actions")?></th>
            
		   </tr>
			</thead>
			<tbody>
			<?php 
			$class_info=$this->app_settings_model->get_account_classes();
			$gp = 1 
			?>
			<?php if ($class_info!=0): foreach ($class_info as $class) : ?>

			<tr>
			<td>
			<?php echo $class->acc_id?>
			</td>
			
			<td>
			<?php echo $class->item_number?>
			</td>
			<td><?php echo $class->acc_type?>
			</td>
			<td>
			<?php
			echo $class->acc_desc 
			?>
			</td>
			<td><?php echo $class->acc_cost ?></td>
			<td>
			  <?php 
			   switch($class->status){
				   case 1:
				   ?>
				   <span class="label label-primary"><?php echo _("Active")?></span>
				   <?php
				   break;
				   
				   case 0:
				   ?>
				   <span class="label label-warning"><?php echo _("InActive")?></span>
				   <?php
				   break;
			   }
			  ?>
			</td>
			
			<td>
			<a href="<?php echo base_url()?>settings/account_classes?tId=<?php echo $class->acc_id?>" title="<?php echo _('Edit') ?>" class='btn btn-xs btn-default'>
				<i class="fa fa-pencil"></i> 
			</a>
			
			<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delAccountClass('<?php echo $class->acc_id?>','<?php echo $class->item_number?>')" class='btn btn-xs btn-danger'>
             <i class='glyphicon glyphicon-trash'></i>
            </a>
				
			</td>
			</tr>
			<?php
			$gp++;
			endforeach;
			?>
			<?php else : ?>
			<td colspan="6" class="text-center">
			<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
			</td>
			<?php endif; ?>
			</tbody>
			</table>
		</div>
	</div>
</div>

</div>
</div>


</section>
<?php $this->load->view('settings/modals/delete_account_class.php');?>		
<?php $this->load->view('theme/footer.php');?>
		
<script>
    $(document).ready(function() {
		$('#dataTables-acClasses').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
		
    });
</script>
