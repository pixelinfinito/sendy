<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
	
<script type="application/javascript" src="<?php echo base_url();?>js/service_groups.js"></script>
<?php 
 $category_id=$this->input->get('id');
 $result=$this->categories_model->get_single_category($category_id);
 
?>

 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo _("Edit Category")?>
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>categories/primary"><?php echo _("Categories")?></a></li> <li class="active"><?php echo _("Edit Category")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div ng-app="">
        
          
            <div class="row">
            <div class="col-lg-8 col-md-8">
			<div class="box">
            <div class="box-body">
			     <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active" id="pTab1"><a href="#tab_1" data-toggle="tab" ><?php echo _("Category Information")?></a></li>
                  <li id="pTab2"><a href="#tab_2" data-toggle="tab"><?php echo _("Thumbnail")?></a></li>
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
           
                <form role="form" ng-controller="UpSgController" name="UpServiceGroupForm" id="UpServiceGroupForm">
                    <div class="form-group">
					<?php 
					
					if(@$result[0]->category_reference!=''){
						$parseCId=$result[0]->category_reference;
					}else{
						$randVal=rand(1000,5000);
						$refCode="ADC-".$randVal;
						$parseCId=$refCode;
					}
					
					?>
					<input type="hidden" class="form-control"  id="category_id"  name="category_id" value="<?php echo $category_id;?>">
                    <label><?php echo _("Reference Code")?></label> <small class="red" style="font-size:14px">*</small>
                    <input type="text" placeholder="<?php echo _('Eg: ADC-001') ?>" class="form-control" ng-model="ref_code" id="ref_code" required="required" name="ref_code" ng-init="ref_code=['<?php echo $parseCId;?>']" readonly>
                    </div>
                    
                    <div class="form-group">
                    <label><?php echo _("Name")?></label> <small class="red" style="font-size:14px">*</small>
                    <input type="text" placeholder="<?php echo _('Category name') ?>" class="form-control" ng-model="service_group_name" id="service_group_name" required="required" name="service_group_name" ng-init="service_group_name=['<?php echo $result[0]->category_name;?>']">
                    </div>
                    <div class="form-group">
                    <label><?php echo _("Description")?></label>
                    <textarea placeholder="<?php echo _('Category description ') ?>" class="form-control" ng-model="service_group_desc" id="service_group_desc" name="service_group_desc" ng-init="service_group_desc=['<?php echo $result[0]->category_desc;?>']"></textarea>
					<small class="red">* <?php echo _("Mandatory field(s).")?></small>
                    </div>
                    <div >
                        <!--<button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="button" ng-click='AddSgIn();' data-toggle="modal" data-target="#SgAddModal"><strong><?php echo _("Add")?></strong>
                        </button>-->
                        <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="button" ng-click='UpServiceGroup();' ><strong><?php echo _("Update")?></strong>
                        </button>
                       
                    </div>
                </form>
                </div>
				<div class="tab-pane" id="tab_2">
				<?php 
				$parseRefId=$result[0]->category_reference;
				$thumbRs = $this->categories_model->load_thumbnail($parseRefId);
				?>
				 <div class="attachment-block clearfix">
                    <img class="attachment-img" src="<?php echo base_url().$thumbRs[0]->thumb_path;?> " alt="attachment image">
                    <div class="attachment-pushed">
                      <h4 class="attachment-heading"><a href="http://www.lipsum.com/"><?php echo _("Category Reference")?>[<?php echo $result[0]->category_reference;?>] </a></h4>
                      <div class="attachment-text">
                       
                         <?php echo _("This is current category thumbnail. You can change thumbnail by using below form.")?>
                      </div><!-- /.attachment-text -->
                    </div><!-- /.attachment-pushed -->
                  </div><!-- /.attachment-block -->

				  
				<form  action="<?php echo base_url();?>categories/primary/edit?id=<?php echo $category_id;?>" method="post" enctype="multipart/form-data">
				    <div class="form-group">
                    <label><?php echo _("Reference Code")?></label> <small class="red" style="font-size:14px">*</small>
                    <input type="text" placeholder="<?php echo _('Empty') ?>" class="form-control" id="thumb_reference" required="required" name="thumb_reference" value="<?php echo $result[0]->category_reference;?>">
                    </div>
					  <div class="form-group">
                    <label><?php echo _("Change thumbnail")?></label> <small class="red" style="font-size:14px">*</small>
                    <input type="file" name="thumb_file" id="thumb_file" class="form-control">
                    </div>
					  <div class="form-group">
                       <input type="submit" value="Upload" class="btn btn-sm btn-primary pull-left m-t-n-xs" id="uploadThumb" name="uploadThumb">
                    </div>
				 </form>
				</div>
				</div>
				</div>
            
				<div id="rsDiv"></div>
			     <?php 
				   if(isset($_POST['uploadThumb'])){
					$upload_dir = "uploads/";
					
						if ($_FILES["thumb_file"]["error"] > 0) {
						 echo _("Error:") . $_FILES["file"]["error"] . "<br>";
						} else {
						 move_uploaded_file($_FILES["thumb_file"]["tmp_name"], $upload_dir .$parseRefId.'-'.$_FILES["thumb_file"]["name"]);
						 $category_code=$_POST['thumb_reference'];
						 $thumb_path=$upload_dir .$parseRefId.'-'.$_FILES["thumb_file"]["name"];
						 //echo _("Uploaded File :") . $_FILES["myfile"]["name"];
						 $result = $this->categories_model->update_thumbnail($category_code,$thumb_path);
						
						 if($result){
							  echo '<i class="fa fa-check-circle darkGreen"></i> Thumbnail updated successfully.';
						 }else{
							  echo '<i class="fa fa-times-circle red"></i> Failed to update thumbnail.';
						 }
						}
					
				   }
				 ?>
			  </div>
            </div>
            </div>
			<div class="col-md-4">
                       
						 <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                  <h3 class="widget-user-username"><?php echo _("Total Groups")?></h3>
                  <h5 class="widget-user-desc"><?php echo _("Until").date('d-M-Y');?></h5>
                </div>
                <div class="widget-user-image">
				 
                  <img class="img-circle" src="<?php echo base_url()?>themeAdmin/dist/img/user1-128x128.jpg" alt="User Avatar">
                </div>
                <div class="box-footer">
                  <div class="row">
                    
                    <div class="col-sm-12 border-right">
                      <div class="description-block">
                        <h5 class="description-header">
						
						
						</h5>
                        <span class="description-text"><?php echo _("ACTIVE")?></span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                   
                  </div><!-- /.row -->
                </div>
              </div><!-- /.widget-user -->
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Telephone is either mobile (or) land line number , along with country code.")?></li>
				</ul>
                        
              </div>
				
			</div>
			
			
            </div>
           
        </section><!-- /.content -->
		
		
<?php $this->load->view('theme/footer.php');?>
		
