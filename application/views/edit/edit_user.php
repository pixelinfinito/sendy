<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
	
	
<script type="application/javascript" src="<?php echo base_url();?>js/users.js"></script>
<?php
$uri_user_id=$this->input->get('id');
$user_groups= $this->user_groups_model->list_groups();
$total_users= $this->user_model->list_users();
$get_user   = $this->user_model->list_current_user($uri_user_id);
$inactive_users=$this->user_model->list_inactive_users();
$current_user_id=$this->session->userdata['login_in']['user_id'];
$token=base64_decode($this->input->get('token'));
?>


 <!-- Content Header (Page header) -->
        <section class="content-header">
		  <h1>
            <?php echo _("Edit User")?>
          </h1>
		 
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
            <li><a href="<?php echo base_url()?>users"><?php echo _("Users")?></a></li> <li class="active"><?php echo _("Edit User")?></li>
          </ol>
		 
        </section>

        <!-- Main content -->
        <section class="content">
		
          <div ng-app="">
        <div ng-controller="UserUpController">
            <?php 
			 if($get_user!=0){
			?>
            <div class="row">
            <div class="col-md-8">
			<div class="box padding_20">
            <div class="box-body">
			
			 <?php 
				switch($token){
				case 'success':
				echo "<div class='row'><div class='col-md-12'>
				<span class='text-success'><i class='fa fa-check'></i>" . _("Password changed successfully.") . " </span>
				</div></div><hr>";
				break;

				case 'failed':
				echo "<div class='row'><div class='col-md-12'>
				<span class='text-danger'><i class='fa fa-times'></i>" . _("Sorry request failed.") . " </span></div></div><hr>";
				break;

				case 'match':
				echo "<div class='row'><div class='col-md-12'>
				<span class='text-warning'><i class='fa fa-meh-o'></i>" . _("Sorry old password, new password can't be same.") . " </span>
				</div></div><hr>";
				break;

				case 'thumb_success':
				echo "<div class='row'><div class='col-md-12'>
				<span class='text-success'><i class='fa fa-check'></i>" . _("Profile picture changed successfully.") . " </span>
				</div></div><hr>";
				break;
				
				
				default:
				echo "";
			   
				}
			  ?>
			  
			  <div class="row">
			  <div class="col-md-8">
			    <?php echo "Update <b class='text-danger'>".$get_user[0]->first_name.' '.$get_user[0]->last_name."</b> 's profile settings";?> 
			  </div>
			  <div class="col-md-4">
			    <?php 
				 if($current_user_id==$uri_user_id)
				 {
				?>
			    <a href="#" data-toggle="modal" data-target="#modalProfilePassword" title="<?php echo _('Change Password') ?>">
				 <i class="fa fa-key"></i> <?php echo _("Change Password")?>
				</a>
				<?php 
				 }
				 ?>
				
			  </div>
			  </div>
			  <hr>
			     <form role="form" name="userUpForm" id="userUpForm">
				 <input type="hidden" placeholder="<?php echo $get_user[0]->user_id;?>" class="form-control" ng-model="user_id" id="user_id"  ng-init="user_id='<?php echo $get_user[0]->user_id;?>'">
            
                
                <div class="form-group">
                <label><?php echo _("First Name")?></label> <small class="red" style="font-size:14px">*</small>
                <input type="text" placeholder="<?php echo $get_user[0]->first_name;?>" class="form-control" ng-model="first_name" id="first_name" required="required" name="first_name" ng-init="first_name='<?php echo $get_user[0]->first_name;?>'">
                </div>
                
                 <div class="form-group">
                <label><?php echo _("Last Name")?></label>
                <input type="text" placeholder="<?php echo $get_user[0]->last_name;?>" class="form-control" ng-model="last_name" id="last_name" name="last_name" ng-init="last_name='<?php echo $get_user[0]->last_name;?>'">
                </div>
                
                 <div class="form-group">
                <label><?php echo _("Email")?></label> <small class="red" style="font-size:14px">*</small>
                <input type="email" placeholder="<?php echo $get_user[0]->user_email;?>" class="form-control" ng-model="email_id" id="email_id" required="required" name="email_id" ng-init="email_id='<?php echo $get_user[0]->user_email;?>'">
                </div>
                
                 <div class="form-group">
                <label><?php echo _("Mobile No")?></label>
                <input type="text" placeholder="<?php echo $get_user[0]->user_mobile;?>" class="form-control" ng-model="telephone" id="telephone" ng-init="telephone='<?php echo $get_user[0]->user_mobile;?>'">
                </div>
                
                <div class="form-group">
                <label><?php echo _("Phone No")?></label>
                <input type="text" placeholder="<?php echo $get_user[0]->user_phone;?>" class="form-control" ng-model="telephone2" id="telephone2" ng-init="telephone2='<?php echo $get_user[0]->user_phone;?>'">
                </div>
               
                <?php 
				 if($get_user[0]->username!='admin'){
				?>
               <div class="form-group">
				<label><?php echo _("User Group")?> </label> <small class="red" style="font-size:14px">*</small>
                  <select class="form-control" id="user_groups" ng-model="user_groups" name="user_groups" required>
                   <option value="">--<?php echo _("Choose")?>--</option>
                   <?php
                    foreach($user_groups as $groups){
                        ?>
                        <option value="<?php echo $groups->group_id?>" ng-init="user_groups='<?php echo $get_user[0]->user_group;?>'">
                        <?php echo $groups->group_name?>
                        </option>
                        <?php
                    }
                   ?>
                  </select>
                </div>
				<?php 
				 }else{
					 ?>
				 <div class="form-group">
				<label><?php echo _("User Group")?> </label> <small class="red" style="font-size:14px">*</small>
				  <select class="form-control" id="user_groups" ng-model="user_groups" name="user_groups" required>
					<option value="1" ng-init="user_groups='1'">
					<?php echo _("Administrator")?>
					</option>
                  </select>
				  </div>
					 <?php
				 }
				 ?>
                
				<?php 
				 if($get_user[0]->username!='admin'){
				?>
				   <div class="form-group">
				   <label><?php echo _("Status")?></label> <small class="red" style="font-size:14px">*</small>
					  <select class="form-control" id="user_status" ng-model="user_status" name="user_status" required>
					  <option value="">--<?php echo _("Choose")?>--</option>
					   <option value="1" ng-init="user_status='<?php echo $get_user[0]->status;?>'"><?php echo _("Active")?></option>
					   <option value="0" ng-init="user_status='<?php echo $get_user[0]->status;?>'"><?php echo _("Inactive")?></option>
					  </select>
					  <small><?php echo _("Inactive users are restricted to access the system.")?></small><br>
					  <small class="red">* <?php echo _("Mandatory field(s).")?></small>
					</div>
				<?php 
				 }else{
					 ?>
					  <div class="form-group">
						<label><?php echo _("Status")?></label> <small class="red" style="font-size:14px">*</small>
					  <select class="form-control" id="user_status" ng-model="user_status" name="user_status" required>
					   <option value="1" ng-init="user_status='1'"><?php echo _("Active")?></option>
					  </select>
					  <small class="red">* <?php echo _("Mandatory field(s).")?></small>
					  </div>
					 <?php
				 }
				 ?>   
                
               <div class="form-group">
                    <button class="btn btn-primary pull-left m-t-n-xs" type="button" ng-click='UpdateUser()'>
					<i class="fa fa-pencil"></i> <?php echo _("Update")?>
                    </button>
                </div>
				<div id="rsDiv"></div>
				<br>
			  </form>
			  </div>
            </div>
            </div>
			<div class="col-md-4">
			 <div class="box padding_20 text-center">
			 <div class="row">
			 <div class="col-md-6">
                <?php 
					if($get_user[0]->profile_thumbnail!=''){
					?>
					<img src="<?php echo base_url().$get_user[0]->profile_thumbnail?>" alt="Thumbnail" width="100" height="100" class="img-circle">

					<?php 
					}
					else
					{
					
					?>
					<img src="<?php echo base_url().'theme4.0/admin/images/avatar5.png'?>" alt="Thumbnail" width="100" height="100" class="img-circle">
					<?php
					
					}
					?>	
				</div>
				<div class="col-md-6">
				 <?php 
					echo "Change <b>".$get_user[0]->first_name.' '.$get_user[0]->last_name."</b> 's Profile Picture";
				   ?>
			   
                 <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modalProfileThumb" title="<?php echo _('Change Picture') ?>">
				 <i class="fa fa-caret-right"></i> <?php echo _("Click here")?>
				 </a> 				
				 
				</div>
				</div>
              </div>					
		      <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                  <div class="widget-user">
                    <h3><i class="fa fa-user"></i> <?php echo _("Total Users")?></h3>
                  </div><!-- /.widget-user-image -->
                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><?php echo _("Active")?>
					<span class="pull-right badge bg-blue">
						<?php 
						 if($total_users!=0){
						  if(count($total_users)>10){
							  echo count($total_users);
						  }else{
							  echo "0".count($total_users);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                    <li><a href="#"><?php echo _("Inactive")?> <span class="pull-right badge bg-aqua">
					<?php 
					     if($inactive_users!=0){
						  if(count($inactive_users)>10){
							  echo count($inactive_users);
						  }else{
							  echo "0".count($inactive_users);
						 }}
						 else{
							 echo "0";
						 }
						?>
					</span></a>
					</li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->
			  
				<ul>
				 <strong><?php echo _("Suggestions")?></strong>
				 <li><?php echo _("Please fill all the mandatory fields.")?></li>
				 <li><?php echo _("Telephone is either mobile number(recommended) or, land line number along with country code(e.g. +673xxxxxx).")?></li>
				</ul>
                        
              </div>
				
			</div>
			 <?php 
			 }else{
				  echo '<div class="box col-sm-12 text-center">
						   <h2 class="text-danger"><i class="fa  fa-exclamation-circle"></i> <?php echo _("Unauthorized request")?></h2>
						   ' . _("Sorry we dont allow unauthorized actions") . '<br><br>
						</div>';
			 }
			 ?>
			</div>
			
            </div>
           
        </section>
		
<?php $this->load->view('admin/users/modals/profile_thumbnail');?>		
<?php $this->load->view('admin/users/modals/change_password');?>		
<?php $this->load->view('theme/footer.php');?>
		
