<?php $this->load->view('theme/header.php');?>
<?php $this->load->view('theme/sidebar.php');?>
 

<script type="application/javascript" src="<?php echo base_url();?>js/caller_number_requests.js"></script>

<section class="content-header">
  <h1>
	<?php echo _("Caller Number Requests")?>
            
  </h1>
  <ol class="breadcrumb">
	<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-dashboard"></i> <?php echo _("Dashboard")?></a></li>
	<li class="active"><?php echo _("Caller Number Requests")?></li>
  </ol>
</section>

<section class="content">
<div class="box ">
 <div class="box-header">
  <div class="row">
   <div class="col-md-6">
   <a href="<?php echo base_url()?>export/export_all_cn" class="btn btn-sm btn-default explink"  title="<?php echo _('Export To Excel') ?>">
	<i class="fa fa-file-excel-o"></i>
	</a>
	<a class="btn btn-sm btn-default explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
	<i class="fa fa-print"></i>
	</a>

   </div>
   <div class="col-md-6 text-right">
		 <a href="<?php echo base_url()?>caller_numbers/ci_requests" class="btn btn-sm btn-primary"  title="<?php echo _('CallerID Requests') ?>">
		 <i class="fa fa-phone"></i> <?php echo _("CallerID Requests")?>
		</a>
   </div>
</div> 
</div>
<div class="box-body">
  <table class="table table-bordered table-striped" id="dataTables-cnList">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Request By")?> </th>
		<th class="col-sm-1"><?php echo _("Request Country")?></th>
		<th class="col-sm-1"><?php echo _("Number")?></th>
		<th class="col-sm-1"><?php echo _("Price")?></th>
		<th class="col-sm-1"><?php echo _("Period")?></th>
		<th class="col-sm-1"><?php echo _("Payment Status")?></th>
		<th class="col-sm-1"><?php echo _("CN Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
			
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$cn_info=$this->caller_number_model->get_cn_requests();
		
		$gp = 1 
		?>
		<?php if ($cn_info!=0): foreach ($cn_info as $cn) : ?>

		<tr>
		<td>
		<?php 
		echo $cn->tid;
		
		if($cn->twilio_sender_id!=''){
			switch($cn->sender_id_status){
				
				case 1:
			    echo "<span title='CallerID Request Approved' class='text-success'>
				<i class='fa fa-hand-stop-o'></i> <i class='fa fa-check'></i>
				</span>";	
				break;
				
				case 2:
			    echo "<span title='CallerID Request Pending' class='text-warning'>
				<i class='fa fa-hand-stop-o'></i> <i class='fa  fa-hourglass-2'></i>
				</span>";	
				break;
				
				case 0:
			    echo "<span title='CallerID Request Rejected' class='text-danger'>
				<i class='fa fa-hand-stop-o'></i> <i class='fa  fa-times'></i>
				</span>";	
				break;
				
				case 3:
			    echo "<span title='CallerID Suspended' class='text-danger'>
				<i class='fa fa-hand-stop-o'></i> <i class='fa  fa-caret-down'></i>
				</span>";	
				break;
			}
			
		}
		?>
		</td>
		
		<td>
		<?php 
		if($cn->ac_thumbnail!=''){
		?>
		<img src="<?php echo base_url().$cn->ac_thumbnail?>" alt="Thumbnail" width="50" height="50" class="img-circle">
		
		<?php 
		}else{
			if($cn->ac_gender!=1){
				?>
				<img src="<?php echo base_url().'themeAdmin/dist/img/avatar2.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">
		
				<?php
			}else{
				?>
				<img src="<?php echo base_url().'themeAdmin/dist/img/avatar5.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">
		
				<?php
			}
		}
		?>
		<?php echo "<br><small>".$cn->ac_first_name.' '.$cn->ac_last_name."</small>"?>
		</td>
		
		<td>
		<?php echo $cn->country_name?>
		<br><small class="text-success"><b><?php echo _("Purchased On")?>: </b><br><?php echo date('d-M-Y H:i:s',strtotime($cn->purchased_on));?></small>
		</td>
		
		<td>
		   <?php 
		   if($cn->twilio_origin_number!=''){
		     echo $cn->twilio_origin_number.'<br>';
			 echo '<small><b class="text-danger">' . _('Expired On') . ':</b><br> '.date('d-M-Y H:i:s',strtotime($cn->expired_on))."</small>";
		   } else{
		     ?>
			 <a href="javascript:void(0)" onclick="javascript:assign_caller_number('<?php echo $cn->tid?>','<?php echo $cn->ac_first_name.' '.$cn->ac_last_name?>','<?php echo $cn->country_name?>')"><i class="fa fa-plus-circle"></i> <?php echo _("Assign Number")?></a>
			 <?php
		   }
		   ?> 
			 
		</td>
		<td>
		 <?php 
		   echo $cn->cn_currency.' '.$cn->price;
		 ?>
		</td>
		<td>
		 <?php 
		   echo $cn->subscription_period.'<small> ' . _('Month(s)') . '</small>';
		  ?>
		</td>
		
		<td>
		<?php 
		  if($cn->payment_status=='1'){
		   echo '<span class="label label-success"><i class="fa fa-check"></i> ' . _("Received") . '</span>';
		  }else{
		   echo '<span class="label label-warning"><i class="fa fa-times"></i> ' . _("Not yet") . '</span>';
		  }
		  
		?>
		</td>
		
		<td>
		<?php 
		
		  switch($cn->cn_status){
			  case 1:
			  echo '<span class="label label-primary">' . _('Active') . '</span>';
			  break;
			  
			  case 2:
			  echo '<span class="label label-warning">' . _('Waiting') . '</span>';
			  break;
			  
			  case 0:
			  echo '<span class="label label-danger"><i class="fa fa-times"></i> ' . _('Rejected') . '</span>';
			  break;
			  
			  case 3:
			  echo '<span class="label label-danger"><i class="fa fa-times"></i> ' . _('Suspended') . '</span>';
			  break;
			  
			 
		  }
		  
		?>
		</td>
		
		<td>
		    <?php 
			  if($cn->twilio_origin_number!=''){
			?>
				<a href="javascript:void(0)" title="<?php echo _('Edit Caller Number') ?>"  class="btn btn-info btn-xs" onclick="javascript:assign_caller_number('<?php echo $cn->tid?>','<?php echo $cn->ac_first_name.' '.$cn->ac_last_name?>','<?php echo $cn->country_name?>')">
				 <i class="fa fa-pencil"></i> 
				</a> 
			
				<?php 
				
				 if($cn->cn_status!=3){
				?>
				<a href="javascript:void(0)" title="<?php echo _('Suspend') ?>"   class="btn btn-danger btn-xs" onclick="javascript:suspend_caller_number('<?php echo $cn->tid?>','<?php echo $cn->twilio_origin_number?>')">
				 <i class="fa fa-plug"></i> 
				</a>
				<?php 
				 }
			  }
			 ?>
		</td>
		
		</tr>
		<?php
		$gp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody>
		</table>
	</div>
  </div>	
	<?php 
	 $this->load->view('includes/modals/assign_caller_number');
	 $this->load->view('includes/modals/suspend_caller_number');
	?>
		
</section>
		
		
<?php $this->load->view('theme/footer.php');?>
<script>
    $(document).ready(function() {
		$('#dataTables-cnList').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		
    });
</script>
  


		
