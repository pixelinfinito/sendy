 <?php 
  $cn_pending_requests=$this->caller_number_model->get_cn_pending_requests();
  $profile_image= $this->session->userdata['login_in']['profile_image'];
 ?>
 
 <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url().$profile_image?>" class="img-circle" alt="Profile Picture">
                  
            </div>
            <div class="pull-left info">
              <p><?php echo _("ID: #").ucfirst($this->session->userdata['login_in']['user_id']);?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> <?php echo _("Online")?></a>
            </div>
          </div>
          <!-- search form -->
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
           
            <li class="active treeview">
              <a href="<?php echo base_url()?>dashboard">
                <i class="fa fa-dashboard"></i> <span><?php echo _("Dashboard")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              
            </li>
			
			<li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i> <span><?php echo _("Users")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>users/add"><i class="fa fa-plus"></i> <?php echo _("New User")?></a></li>
                <li><a href="<?php echo base_url()?>users"><i class="fa fa-align-left"></i> <?php echo _("All Users")?></a></li>
		      </ul>
            </li>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span><?php echo _("User Groups")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>user_groups/add"><i class="fa fa-plus"></i> <?php echo _("New Group")?></a></li>
                <li><a href="<?php echo base_url()?>user_groups"><i class="fa fa-list-ul"></i> <?php echo _("All Groups")?></a></li>
				
              </ul>
            </li>
			<li class="treeview">
              <a href="#">
                <i class="fa  fa-suitcase"></i> <span><?php echo _("Members")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>members/add"><i class="fa fa-plus"></i> <?php echo _("Add Member")?></a></li>
                <li><a href="<?php echo base_url()?>members/all"><i class="fa  fa-align-left"></i> <?php echo _("All Members")?></a></li>
				<li><a href="<?php echo base_url()?>members/blacklist"><i class="fa fa-ban"></i> <?php echo _("Blacklist/Whitelist")?></a></li>
				
              </ul>
            </li>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-send-o"></i> <span><?php echo _("Campaigns")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>campaigns/all"><i class="fa fa-list-ul"></i> <?php echo _("All Campaigns")?></a></li>
                <li><a href="<?php echo base_url()?>campaigns/scheduled"><i class="fa fa-calendar"></i> <?php echo _("Scheduled Campaigns")?></a></li>
				<li><a href="<?php echo base_url()?>campaigns/by_member"><i class="fa  fa-suitcase"></i> <?php echo _("By Member")?></a></li>
		      </ul>
            </li>
			
			
			<li class="treeview">
              <a href="#">
                <i class="fa fa-dollar"></i> <span><?php echo _("Payments")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>payments/all_payments"><i class="fa fa-money"></i> <?php echo _("All Payments")?></a></li>
                <li><a href="<?php echo base_url()?>payments/payment_by_member"><i class="fa fa-suitcase"></i> <?php echo _("Check by Member")?></a></li>
		      </ul>
            </li>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-google-wallet"></i> <span><?php echo _("Wallet Money")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>wallet/all_transactions"><i class="fa fa-history"></i> <?php echo _("All Transactions")?></a></li>
                <li><a href="<?php echo base_url()?>wallet/transaction_by_member"><i class="fa fa-suitcase"></i> <?php echo _("Transaction By Member")?></a></li>
				<li><a href="<?php echo base_url()?>wallet/deposit_funds"><i class="fa fa-dollar"></i> <?php echo _("Refund/Deposit Funds")?></a></li>
				<li><a href="<?php echo base_url()?>wallet/rollback_funds"><i class="fa fa-reply-all"></i> <?php echo _("Rollback Funds")?></a></li>
				<li><a href="<?php echo base_url()?>wallet/free_credit_requests"><i class="fa  fa-hand-paper-o"></i> <?php echo _("Free Credit Requests")?></a></li>
		      </ul>
            </li>
			
			<li class="treeview">
              <a href="#">
                <i class="fa fa-phone"></i> <span><?php echo _("Caller Numbers")?></span>
				<i class="fa fa-angle-left pull-right"></i>
				<?php 
				if($cn_pending_requests!=0){
					?>
					 <span class="badge pull-right bg-aqua" title="<?php echo _('Pending Caller Number Requests') ?>">
					 <?php echo _("new").$cn_pending_requests?>
					 </span>
					<?php
					
				}else{
					?>
					<span class="badge pull-right bg-green" title="<?php echo _('Pending Caller Number Requests') ?>">
					 <?php echo _("new 0")?>
					 </span>
					<?php
				} 
				?>
				
              </a>
			   <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>caller_numbers/cn_requests"><i class="fa  fa-sort-numeric-asc"></i> <?php echo _("Caller Number Requests")?></a></li>
				<li><a href="<?php echo base_url()?>caller_numbers/ci_requests"><i class="fa fa-hand-stop-o"></i> <?php echo _("CallerID Requests")?></a> </li>
			   </ul>
            </li>
			
			
			<li class="treeview">
              <a href="#">
                <i class="fa  fa-history"></i> <span><?php echo _("SMS History")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>sms/history"><i class="fa fa-envelope-o"></i> <?php echo _("All History")?></a></li>
                <li><a href="<?php echo base_url()?>sms/by_campaign"><i class="fa fa-send-o"></i> <?php echo _("By Campaign")?></a></li>
			  </ul>
            </li>
			
			
			<li class="treeview">
              <a href="#">
                <i class="fa fa-star-o"></i> <span><?php echo _("Promotions/Offers")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>promotions/email_campaign"><i class="fa fa-envelope"></i> <?php echo _("Compose Email")?></a></li>
                <li><a href="<?php echo base_url()?>promotions/sms_campaign"><i class="fa fa-mobile"></i> <?php echo _("Compose SMS")?></a></li>
				<li><a href="<?php echo base_url()?>promotions/all"><i class="fa fa-list"></i> <?php echo _("All Promotions")?></a></li>
			  </ul>
            </li>
			
			<li class="treeview">
              <a href="#">
                <i class="fa  fa-commenting-o"></i> <span><?php echo _("Notifications")?></span>  <i class="fa fa-angle-left pull-right"></i>
              </a>
			   <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>notifications/send"><i class="fa fa-share"></i> <?php echo _("Send Notification")?></a></li>
				<li><a href="<?php echo base_url()?>notifications/all"><i class="fa fa-bell"></i> <?php echo _("All Notifications")?></a> </li>
			   </ul>
            </li>
			
			<li class="treeview">
              <a href="<?php echo base_url()?>enquiry/all">
                <i class="fa  fa-question-circle"></i> <span><?php echo _("Enquiries")?></span>
              </a>
            </li>
			
			<li class="treeview">
              <a href="<?php echo base_url()?>visitors">
                <i class="fa  fa-map-marker"></i> <span><?php echo _("Visitors")?></span>
              </a>
            </li>
			
			<li class="treeview">
              <a href="#">
                <i class="fa  fa-user-secret"></i> <span><?php echo _("Master Account")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
			   <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>master_account/check_cn"><i class="fa fa-search"></i> <?php echo _("Caller Number Lookup")?></a></li>
				<li><a href="<?php echo base_url()?>master_account/service_utility"><i class="fa  fa-line-chart"></i> <?php echo _("Service Utility")?></a> </li>
			   </ul>
            </li>
			
			<li class="treeview">
              <a href="#">
                <i class="fa  fa-globe"></i> <span><?php echo _("CMS")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
			   <ul class="treeview-menu">
				
				<li><a href="<?php echo base_url()?>cms/about_us"><i class="fa fa-building-o"></i> <?php echo _("About Us")?></a> </li>
				<li><a href="<?php echo base_url()?>cms/careers"><i class="fa fa-bullhorn"></i> <?php echo _("Careers")?></a> </li>
			    <li><a href="<?php echo base_url()?>cms/terms_conditions"><i class="fa  fa-star"></i> <?php echo _("Terms & Conditions")?></a></li>
				<li><a href="<?php echo base_url()?>cms/privacy_policy"><i class="fa fa-expeditedssl"></i> <?php echo _("Privacy Policy")?></a> </li>
				<li><a href="<?php echo base_url()?>cms/anti_spam_policy"><i class="fa fa-mars"></i> <?php echo _("Antispam Policy")?></a> </li>
				<li><a href="<?php echo base_url()?>cms/articles"><i class="fa fa-list"></i> <?php echo _("Article List")?></a> </li>
				
			   </ul>
            </li>
			
			
			<li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span><?php echo _("Settings")?></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url()?>settings/app"><i class="fa fa-asterisk"></i> <?php echo _("App")?></a></li>
				<li><a href="<?php echo base_url()?>settings/site"><i class="fa fa-globe"></i> <?php echo _("Site")?></a></li>
                <li><a href="<?php echo base_url()?>settings/payment"><i class="fa  fa-university"></i><?php echo _("Payment Gateway")?></a></li>
				<li><a href="<?php echo base_url()?>settings/sms"><i class="fa  fa-bolt"></i> <?php echo _("SMS/Call Gateway")?></a></li>
				<li><a href="<?php echo base_url()?>settings/caller_numbers"><i class="fa fa-phone"></i> <?php echo _("Caller Numbers")?></a></li>
				<li><a href="<?php echo base_url()?>settings/sms_prices"><i class="fa fa-dollar"></i> <?php echo _("SMS Prices")?></a></li>
				<li><a href="<?php echo base_url()?>settings/account_classes"><i class="fa fa-circle-o"></i> <?php echo _("Account Classes")?></a></li>
				<li><a href="<?php echo base_url()?>settings/country"><i class="fa  fa-location-arrow"></i> <?php echo _("Country")?></a></li>
				<li><a href="<?php echo base_url()?>settings/currency"><i class="fa  fa-balance-scale"></i> <?php echo _("Currency")?></a></li>
				
				
				
			  </ul>
            </li>
			
           <li><a href="<?php echo base_url()?>logout"><i class="fa fa-unlock-alt"></i> <span><?php echo _("Logout")?></span></a></li>
           
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">