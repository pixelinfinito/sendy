</div><!-- /.content-wrapper -->
<?php 
$intlib=$this->internal_settings->local_settings();
?>
  <footer class="main-footer">
	<div class="pull-right hidden-xs">
	  <b><?php echo _("Version")?></b> 2.3.0
	</div>
	&copy; <?php echo $intlib[0]->brand_copyrights;?>
  </footer>
  <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->


<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url()?>theme4.0/admin/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url()?>theme4.0/admin/js/fastclick.min.js"></script>

<script src="<?php echo base_url()?>theme4.0/admin/js/app.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url()?>theme4.0/admin/js/jquery.sparkline.min.js"></script>

<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url()?>theme4.0/admin/js/jquery.slimscroll.min.js"></script>

<script src="<?php echo base_url(); ?>theme4.0/admin/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme4.0/admin/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme4.0/admin/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>theme4.0/admin/js/demo.js"></script>
  </body>
</html>
