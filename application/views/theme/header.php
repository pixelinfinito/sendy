<?php
 if(!isset($this->session->userdata['login_in'])){
	  header("location:".base_url()."login");
	  exit(0);
      
 }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>OneTextGlobal | Dashboard</title>
	<link rel="shortcut icon" href="<?php echo base_url()?>theme4.0/admin/images/ico/favicon.png">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/js/jquery-jvectormap-1.2.2.css">
	<link href="<?php echo base_url(); ?>theme4.0/admin/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/admin.css">
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/customStyle.css">
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/skins.css">
	
    
	<script src="<?php echo base_url();?>theme4.0/admin/js/jquery-2.1.0.min.js"></script>
	<script src= "<?php echo base_url();?>js/angular.min.1.2.26.js"></script>
	<script src="<?php echo base_url();?>js/pages/ui-bootstrap-tpls-0.10.0.min.js"></script>
	<script src="<?php echo base_url();?>theme4.0/admin/js/jquery-jvectormap-2.0.3.min.js"></script>
	<script src="<?php echo base_url();?>theme4.0/admin/js/jquery-jvectormap-world-mill-en.js"></script>
	
	<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/bootstrapValidator.css"/>
    <script type="text/javascript" src="<?php echo base_url()?>theme4.0/admin/js/bootstrapValidator.js"></script>
	
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
   <div class="wrapper">
      <header class="main-header">
        <a href="index2.html" class="logo">
          <span class="fa fa-newspaper-o"></span>
          <span><?php echo _("DASHBOARD")?></span>
        </a>

          <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only"><?php echo _("Toggle navigation")?></span>
          </a>
		   		  
		  <div class="pull-left">
		   <ul class="nav navbar-nav">
		    <li class="dropdown messages-menu">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		  <?php echo ucfirst($this->session->userdata['login_in']['current_timezone']);?> 
		   <i class="fa fa-caret-down"></i>
		  </a>
		   <ul class="dropdown-menu">
				<li class="header">
					<span class="fa fa-clock-o"></span>
					<?php echo "<strong>" . _('Your Timezone is') . ":</strong> " .ucfirst($this->session->userdata['login_in']['current_timezone']);?>
				</li>
				<li>
				<ul class="menu">
					<li>
					<a href="#">
					<div class="pull-left">
					<img src="<?php echo base_url()?>theme4.0/admin/images/clock.png" class="img-circle" alt="Clock">
					</div>
					<h4>
					<?php echo _("DateTime")?><small><i class="fa fa-clock-o"></i> <?php echo ucfirst($this->session->userdata['login_in']['current_timezone']);?></small>
					</h4>
					<p> <?php echo date('d-M-Y H:i:s');?> </p>
					</a>
					</li>
				</ul>
				</li>
		   </ul>
		  </li>
		 </ul>
		</div>
		  <div class="pull-left">
		   <ul class="nav navbar-nav">
		    <li class="dropdown messages-menu">
		     <a href="<?php echo base_url()?>home" target="_blank"> <span class="fa fa-globe"></span>  <?php echo _("Visit Site")?></a>
		    <li>
			</ul>
		  </div>
		  <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
		    <ul class="nav navbar-nav">
			  
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
					<a href="#" class="dropdown-toggle " data-toggle="dropdown">
					<i class="fa fa-google-wallet"></i>
					<?php 
						$fCreditRequests=$this->wallet_model->check_freecredit_requests();
					?>
					<?php 
					if($fCreditRequests!=0){
					?>
						<span class="label label-success"><?php echo count($fCreditRequests);?></span>
					<?php 
					}
					?>
					</a>
					<ul class="dropdown-menu">
					<?php 
					if($fCreditRequests!=0){
					?>
					<li class="header">
						<?php echo "You got <b>".count($fCreditRequests)."</b> free credit requests";?>
					</li>
					<li>
					<!-- inner menu: contains the actual data -->
					<ul class="menu">
						<?php 
						foreach($fCreditRequests as $member){
						?>
						<li>
						<a href="<?php echo base_url()?>wallet/free_credit_requests">
						<div class="pull-left">
						<?php 
						if($member->ac_thumbnail!=''){
						?>
						<img src="<?php echo base_url().$member->ac_thumbnail?>" alt="Thumbnail" width="50" height="50" class="img-circle">

						<?php 
						}else{
						if($member->ac_gender!=1){
						?>
						<img src="<?php echo base_url().'theme4.0/admin/images/avatar2.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">

						<?php
						}else{
						?>
						<img src="<?php echo base_url().'theme4.0/admin/images/avatar5.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">

						<?php
						}
						}
						?>

						</div>
						<h4>
						<?php echo $member->member_name;?>
						</h4>
						<small>
						<i class="fa fa-clock-o"></i> <?php echo _("Request on")?><?php echo date('d-M-Y H:i:s',strtotime($member->request_datetime));?>
						</small>
						<p>
						<?php 
						if($member->request_comments!=''){
						echo substr($member->request_comments,0,60)."....";
						}else{
						echo _("No comments found.");
						}
						?>
						</p>
						</a>
						</li>
						<?php 
						}
						?>

					</ul>
					</li>
					<?php 
					}
					?>
					<li class="footer"><a href="<?php echo base_url()?>wallet/free_credit_requests"><?php echo _("View All")?></a></li>
					</ul>
              </li>
              
              <li class="dropdown messages-menu">
			  <?php 
			  $inactive_members=$this->account_model->list_all_inactive_members();
			  if($inactive_members!=0){
				  $im_count=count($inactive_members);
			  }else{
				  $im_count=0;
			  }
			  ?>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-laptop"></i>
				<span class="label label-warning"><?php echo $im_count;?></span>
				</a>
				
				<ul class="dropdown-menu">
					<li class="header"><?php echo "You have <b>".$im_count."</b> pending members";?></li>
					<li>
					<ul class="menu">
						<?php 
						foreach($inactive_members as $member){
						?>
						<li>
						<a href="<?php echo base_url()?>members/all">
						<div class="pull-left">
						<?php 
						if($member->ac_thumbnail!=''){
						?>
						<img src="<?php echo base_url().$member->ac_thumbnail?>" alt="Thumbnail" width="50" height="50" class="img-circle">

						<?php 
						}else{
						if($member->ac_gender!=1){
						?>
						<img src="<?php echo base_url().'theme4.0/admin/images/avatar2.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">

						<?php
						}else{
						?>
						<img src="<?php echo base_url().'theme4.0/admin/images/avatar5.png'?>" alt="Thumbnail" width="50" height="50" class="img-circle">

						<?php
						}
						}
						?>

						</div>
						<h4>
						<?php echo $member->ac_first_name.' '.$member->ac_last_name;?>
						</h4>
						<small>
						<i class="fa fa-globe"></i> <?php echo "Country : <b>".$member->ac_country."</b>";?>
						</small>
						<p>

						</p>
						</a>
						</li>
						<?php 
						}
						?>
					</ul>
					</li>
					<li class="footer"><a href="<?php echo base_url()?>members/all"><?php echo _("View all")?></a></li>
				</ul>
				</li>
             
             
              <li class="dropdown user user-menu">
			   <?php 
			   $profile_image= $this->session->userdata['login_in']['profile_image'];
			   ?>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				  <img src="<?php echo base_url().$profile_image?>" class="user-image" alt="Profile Picture">
                  <span class="hidden-xs"><?php echo ucfirst($this->session->userdata['login_in']['username']);?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
				  <img src="<?php echo base_url().$profile_image?>" class="img-circle" alt="Profile Picture">
                   
                    <p>
                      <?php 
					    echo ucfirst($this->session->userdata['login_in']['username']);
                        echo "<small>" . _('Member Since ') . "<b>" . date('d-M-Y',strtotime($this->session->userdata['login_in']['date_of_join']))."</b></small>";
					   ?>
					  
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-12 text-center">
                      <a href="#"><?php echo _("Timezone :").$this->session->userdata['login_in']['current_timezone'];?></a>
                    </div>
                   
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url()?>users/edit?id=<?php echo $this->session->userdata['login_in']['user_id']?>" class="btn btn-default btn-flat">
					  <i class="fa fa-pencil"></i> <?php echo _("Update Profile")?></a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url()?>logout" class="btn btn-default btn-flat">
					  <i class="fa fa-unlock-alt"></i> <?php echo _("Logout")?></a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gear"></i></a>
              </li>
            </ul>
          </div>
       </nav>
 </header>