<?php
$handle = fopen($file, "r");

?>
<script src="<?php echo base_url()?>js/jquery-2.1.1.js"></script>
<link href="<?php echo base_url()?>theme4.0/client/css/customStyle.css" rel="stylesheet">
<link href="<?php echo base_url()?>theme4.0/client/css/style.css" rel="stylesheet">
<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>theme4.0/client/css/light-bootstrap-dashboard-classic.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>theme4.0/client/css/pe-icon-7-stroke.css" rel="stylesheet" />

<div class='main-container'>
	<div class='container text-center'>
	   <div class="row">
		   <div class='col-md-3'>&nbsp;</div>
		   <div class='col-md-3 text-right'>
		   <img src="<?php echo base_url()?>theme4.0/client/images/logo_black.png"> 
		   </div>
		   <div class='col-md-3 text-left'><h4 class="text-muted"><?php echo _("Import Contacts")?></h4></div>
		   <div class='col-md-3'>&nbsp;</div>
	   </div>
	   
	   <div class="row">
		   <div class='col-md-3'>&nbsp;</div>
		   <div class='col-md-6 text-left'>
		   <div class="inner-box" style="padding-top:0px;margin-top:10px;">
		  
		     <?php
			 $cnt_array=array();
			
			while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
			{
				//$contact_name=$filesop[0];
				//$mobile_number="+".$filesop[1];
				//$email_id=$filesop[2];
				$row_contact=$filesop[0].'__'.$filesop[1].'__'.$filesop[2].'__'.$filesop[3];
				array_push($cnt_array,$row_contact);
			}
			
			echo "<div class='row bg-dark'>";
			echo "<div class='col-md-6'><h4 class='text-success'> " . _('Total Contacts') . " ".count($cnt_array)."</h4></div>";
			echo "<div class='col-md-6 text-right'><br>";
			?>
			<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('cts')?>"><i class='pe-7s-angle-left'></i><?php echo _("Back to contacts")?></a>
			<?php
			echo "</div>";
			echo "</div>";
			$i=1;
			for($c=0;$c<count($cnt_array);$c++){
				$exp=explode('__',$cnt_array[$c]);
				$contact_name=$exp[0];
				$calling_code=$exp[1];
				$mobile_number=$exp[2];
				$email_id=$exp[3];
			 ?>
			 <div id="cprocess<?php echo $c;?>"></div>
			 <script>

				 $(document).ready(function() {
				
				var cid= '<?php echo $c?>'; 
				var re = new RegExp(/^.*\//);
				var rCPath= re.exec(window.location.href);
				
				   $('#cprocess'+cid).html('<i class="fa fa-spinner fa-spin"></i> <?php echo _('importing ') . $calling_code.'-'.$mobile_number;?><br>');
					$.ajax({
					type: "POST",
					url: rCPath+"import_contact_process",
					data: {contact_name:'<?php echo $contact_name;?>',calling_code:'<?php echo $calling_code;?>',mobile_number:'<?php echo $mobile_number;?>',
					email_id:'<?php echo $email_id;?>',group:'<?php echo $group;?>',country:'<?php echo $country;?>'}
					})
					.done(function(msg) {
						//alert(msg)
						switch (msg) {
							case 'success':
							$('#cprocess'+cid).html('<div class="text-success"><span class="pe-7s-check"></span> <?php echo _("At row") . " [".$i."]"?> <b><?php echo $calling_code.'-'.$mobile_number;?></b> <?php echo _("imported success")?></div><hr>');
							break;
							case 'failed':
							$('#cprocess'+cid).html('<div class="text-danger"><span class="pe-7s-info"></span> <?php echo _("At row") . " [".$i."]"?> <b><?php echo $calling_code.'-'.$mobile_number;?></b> <?php echo _("imported error")?></div><hr>');
							break;
							case 'duplicated':
							$('#cprocess'+cid).html('<div class="text-warning"><span class="pe-7s-close-circle"></span> <?php echo _("At row") . " [".$i."]"?> <b><?php echo $calling_code.'-'.$mobile_number;?></b> <?php echo _("contact duplicated")?></div><hr>');
							break;
							case 'invalid':
							$('#cprocess'+cid).html('<div class="text-danger"><span class="pe-7s-close-circle"></span> <?php echo _("At row") . " [".$i."]"?> <b><?php echo $calling_code.'-'.$mobile_number;?></b> <?php echo _("contact is invalid please check country code")?></div><hr>');
							break;
							
						}
					
				});
			});
			
			 </script>
			 <?php
			 $i++;
			 
			}
			 ?>
		   </div>
		   </div>
		   <div class='col-md-3'>&nbsp;</div>
	   </div>
	   
	</div>
</div>
<?php
		