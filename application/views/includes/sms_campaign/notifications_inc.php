<style>
.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}
</style>

<div class="row">
   <div class="col-md-6">
   <h4><i class="fa fa-bell-o"></i>&nbsp; <?php echo _("Notifications")?></h4>
   </div>
   <div class="col-md-6 text-right">
		<a href="<?php echo base_url()?>site/export/export_notifications" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			 <i class="fa fa-file-excel-o"></i>
		</a>
		<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
		  <i class="fa fa-print"></i>
		</a>
			
   </div>
</div>  <hr>
  <table class="table table-bordered table-striped" id="dataTables-Notify">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("Id")?></th>
		<th class="col-sm-1"><?php echo _("Notification Type")?></th>
		<th class="col-sm-1"><?php echo _("Title")?></th>
		<th class="col-sm-1"><?php echo _("Notification From")?></th>
		<th class="col-sm-1"><?php echo _("Attachment")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$user_id = $this->session->userdata['site_login']['user_id'];
		$notify_info=$result=$this->notification_model->list_notifications_bymember($user_id);
		$sp = 1 
		?>
		<?php if ($notify_info!=0): foreach ($notify_info as $notify) : ?>

		<tr>
		<td>
		<?php echo $notify->notify_id?>
		</td>
		<td><?php echo ucfirst($notify->notify_type);?>
		</td>
		<td>
		 
		  <?php 
		   if($notify->read_status!=0){
			   ?>
			   <a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->notify_id;?>)">
			   <u><?php echo $notify->notify_title?></u>
			   </a>
			   <?php
		   }else{
			   ?>
			   <a href="javascript:void(0)" onclick="javascript:loadNotifyBody(<?php echo $notify->notify_id;?>)">
			   <u><b><?php echo $notify->notify_title?></b></u>
			   </a>
			   <?php
		   }
		  ?>
		
		
		</td>
		<td>
		  <?php 
		  echo ucfirst($notify->notify_from)."<br>";
		  echo "<small class='text-muted'> <b>" . _('Sent On') . "</b> ".date('d-M-Y h:i:s',strtotime($notify->notify_timestamp))."</small>";
		  
		  
		  ?>  
		  
		</td>
		<td>
		  <?php 
		  if($notify->attachment_path!=''){
		  ?>
		  <a href="<?php echo base_url().$notify->attachment_path ?>">
		   <h5><i class="fa fa-paperclip"></i></h5>
		  </a>  
		  <?php 
		  }else{
			  
			  echo "-";
		  }
		  ?>
		</td>
		<td>
		<a href="javascript:void(0)" title="<?php echo _('View Message') ?>" onclick="javascript:loadNotifyBody(<?php echo $notify->notify_id;?>)">
		  <i class="fa fa-envelope-o"></i>
		 </a> <span class="custom_title12">|</span>
		 
		 <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delNotify('<?php echo $notify->notify_id?>')" class="text-danger">
		  <i class="glyphicon glyphicon-trash"></i>
		 </a> 
		</td>
	
		</tr>
		<?php
		$sp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
</table> <!-- / Table -->
<?php $this->load->view('site/modals/campaign/sms/delete_member_notification');?>
<?php $this->load->view('site/modals/notification_body');?>