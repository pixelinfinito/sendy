<style>.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}</style>
<div class="row">
<div class="col-md-6">
 <h4><i class="fa fa-user-plus"></i>&nbsp; <?php echo _("All Contacts")?></h4>
</div>
 <div class="col-md-6 text-right">
			<a href="<?php echo base_url()?>site/export/export_contacts" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			 <i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			  <i class="fa fa-print"></i>
			</a>
			<a href="javascript:void(0)" onclick="javascript:triggerImportModal()" class="btn btn-sm explink"  title="<?php echo _('Import Contacts') ?>">
			<i class="fa fa-download"></i>
			</a>
			<a href="javascript:void(0)" id="link_copy_all" class="btn btn-sm explink"  title="<?php echo _('Copy all selected contacts') ?>">
			<span id="copy_all_tag"><i class="fa fa-copy"></i></span>
			</a>
			<a href="javascript:void(0)" id="link_move_all" class="btn btn-sm explink"  title="<?php echo _('Move all selected contacts') ?>">
			<span id="move_all_tag"><i class="fa fa-exchange"></i></span>
			</a>
			<a href="javascript:void(0)" id="link_delete_all" class="btn btn-sm explink"  title="<?php echo _('Delete selected contacts') ?>">
			<span id="delete_all_contacts" class="text-danger"><i class="fa fa-trash"></i></span>
			</a>
			 <a href="javascript:void(0)" onclick="javascript:triggerSMSContact()" class="btn btn-sm explink"  title="<?php echo _('Add New Contact') ?>">
			 <i class="fa fa-user-plus"></i> <?php echo _("New Contact")?>
			</a>
  
	 </div>
</div><hr>
		<table class="table table-bordered table-striped" id="dataTables-contacts">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-xs-1">
          <input type="checkbox" name="check_all_contacts" id="check_all_contacts">
		</th>
		<th class="col-sm-1"><?php echo _("Name")?> </th>
		<th class="col-sm-1"><?php echo _("Country")?> </th>
		<th class="col-sm-1"><?php echo _("MobileNo")?> </th>
		<th class="col-sm-1"><?php echo _("Campaign Group")?> </th>
		<th class="col-sm-1"><?php echo _("Action")?></th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$userId=$this->session->userdata['site_login']['user_id'];
		$all_contacts_info=$this->sms_model->get_contacts_byuser($userId);
		$key = 1 

		?>
		<?php if ($all_contacts_info!=0): foreach ($all_contacts_info as $contact) : ?>

		<tr>
		<td>
		<input type="checkbox" class="check_contact" name="check_contact" value="<?php echo $contact->c_id.'_'.$contact->contact_mobile;?>">
		<?php //echo $contact->c_id ?>
		</td>
		<td><?php echo $contact->contact_name ?></td>
		<td><?php echo $contact->country_name ?></td>
		<td><?php echo $contact->contact_mobile?></td>
		<td><?php echo $contact->group_name?></td>
		<td>

		<a href="javascript:void(0)" onclick="javascript:triggerCCSingle('<?php echo $contact->c_id?>')" title="<?php echo _('Copy') ?>" class="text-primary">
		<span><i class="fa fa-copy"></i></span>
		</a><span class="custom_title12">|</span>

		<a href="javascript:void(0)" onclick="triggerMvSingle('<?php echo $contact->c_id?>')" title="<?php echo _('Move') ?>" class="text-primary">
		<span><i class="fa fa-share"></i></span>
		</a> <span class="custom_title12">|</span>


		<a href="javascript:void(0)" title="<?php echo _('Edit') ?>" class="text-warning" onclick="triggerEditContact('<?php echo $contact->c_id?>','<?php echo $contact->contact_group?>','<?php echo $contact->country_code?>','<?php echo $contact->calling_code?>')">
		<span><i class="fa fa-pencil"></i></span>
		</a> <span class="custom_title12">|</span>

		<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delSMSContact('<?php echo $contact->c_id?>')" class="text-danger">
		<i class='glyphicon glyphicon-trash'></i> 
		</a>

		</td>
		</tr>
		<?php
		$key++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
					
		
<?php 
$this->load->view('site/modals/campaign/sms/edit_sms_contact');
$this->load->view('site/modals/campaign/sms/copy_single_contact');
$this->load->view('site/modals/campaign/sms/move_single_contact');
$this->load->view('site/modals/campaign/sms/delete_contact');
$this->load->view('site/campaign/add_sms_contact');
$this->load->view('site/campaign/import_contacts');
$this->load->view('site/modals/campaign/sms/copy_contacts_alert');
$this->load->view('site/modals/campaign/sms/move_contacts_alert');
$this->load->view('site/modals/campaign/sms/delete_contacts_alert');

?>

<br><br>

