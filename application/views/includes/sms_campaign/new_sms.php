<?php 
	$user_id=$this->session->userdata['site_login']['user_id'];
    $callerNumbers=$this->sms_model->get_caller_numbers($user_id);
	
	$appSettings= $this->app_settings_model->get_primary_settings();
	$appCountry=$appSettings[0]->app_country;
	$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
	@$getTimezones= $this->countries_model->list_country_timezone();
	
	$smsBackSettings=$this->app_settings_model->get_sms_settings();
	if($smsBackSettings[0]->campaign_minval!=0 && $smsBackSettings[0]->campaign_maxval!=0){
		$randMinval=$smsBackSettings[0]->campaign_minval;
		$randMaxval=$smsBackSettings[0]->campaign_maxval;
	}else{
		$randMinval=1000;
		$randMaxval=10000;
	}
	
	$campaign_code=rand($randMinval,$randMaxval);
	
	$walletInfo= $this->account_model->get_account_wallet($user_id);
	if(@$walletInfo[0]->balance_amount!=''){
		$accountCredit=$walletInfo[0]->balance_amount;
	}else{
		$accountCredit=0;
	}

?>
	
          
<div class="row">
<div class="col-md-4">
  <h3><?php echo _("New Text Message")?></h3>
</div>

<div class="col-md-4">
<i class="fa fa-bookmark-o"></i> <?php echo _("Campaign ID ")?>: <?php echo $campaign_code;?>
</div>


<div class="col-md-4 text-muted">
 <i class="fa fa-calculator"></i> <?php echo _("Projected Price")?> <h2 class="text-danger"> <span id="proacc_price">00.000</span> <?php echo $currencyInfo[0]->currency_name?> </h2>
</div>
</div>

<form  ng-controller="campaignSMSFormCtrl" role="form" name="campaignSMSForm" id="campaignSMSForm">
<div class="row">
<div class="col-md-8">
  <div class="form-group">
    <label><?php echo _("To")?> </label>
	<textarea class="form-control" id="campaign_to_list" name="campaign_to_list" placeholder="<?php echo _('e.g. +6737777777,+6738888888,+918989898989') ?>" required></textarea>
	<small class="text-muted"><?php echo _("Multiple contact numbers are seperated by comma")?>
	<a href="javascript:void(0)" onclick="javascript:triggerNumberInfo()"><u><?php echo _("learn more")?></u></a></small>
	<input type="hidden" class="form-control" name="hidAssignGrpToken" id="hidAssignGrpToken" />
	<input type="hidden" class="form-control" name="hidCampCode" id="hidCampCode"  value="<?php echo $campaign_code?>"/>
	<input type="hidden" class="form-control" name="hidProPrice" id="hidProPrice" />
	<input type="hidden" class="form-control" name="hidACredit" id="hidACredit"  value="<?php echo $accountCredit?>"/>
	<input type="hidden"   class="form-control" name="hidCampaignMode" id="hidCampaignMode" />
  </div>  
</div>

<div class="col-md-4">
  <label>&nbsp;</label>
  <ul class="">
   <li><a href="javascript:void(0)" onclick="javascript:fetchContacttoFromModal()"><i class="fa fa-user-plus"></i> <?php echo _("Select Contacts")?></a> <span class="text-muted">(<?php echo _("or")?>)</span></li>
   <li><a href="javascript:void(0)" onclick="javascript:fetchGrouptoFromModal()"><i class="fa fa-users"></i> <?php echo _("Select Groups")?></a></li>
   
   <li id="clearToContacts" style="display:none"><a href="javascript:void(0)" onclick="javascript:clearToContacts()">
   <span class="text-danger"><i class="fa fa-eraser"></i> <?php echo _("Clear Selection")?></a></span></li>
  </ul>
</div>
</div>

<div class="row">
<div class="col-md-8">
  <div class="form-group">
    <label><?php echo _("From")?> </label>
	<select class="form-control" id="campaign_from" id="campaign_from" required>
	  <option value="1"><?php echo _("Default Sender Settings")?> </option>
	   <?php 
	    if($callerNumbers!=0){
		date_default_timezone_set($appSettings[0]->app_timezone);	
	    foreach($callerNumbers as $number){
		
		$expires_in = strtotime($number->expired_on) - strtotime($number->purchased_on);
		$today=date('Y-m-d H:i:s');
		if(strtotime($number->expired_on) > strtotime($today))
		{
			$number_expires= floor($expires_in/3600/24);
			if($number_expires>0){
				switch($number->status){
				
				case 1:	
				//// cross check callerid status 
				if($number->sender_id_status!=1){
					$sender_id='';
				}else{
					$sender_id=$number->twilio_sender_id;
				}
				?>
				<option value="<?php echo $number->twilio_origin_number;?>">
				<?php echo _("Caller Number") . ": ".$number->twilio_origin_number. " &nbsp;[" . _("Sender ID") . ": ".$sender_id."]";?>
				</option>
				<?php
				break;
				
				}
			}
		}
		
		}}
	   ?>
	</select>
	<small class="text-muted"><?php echo _("If you use default sender settings,from number will be")?>
	 <?php echo $smsBackSettings[0]->twilio_origin_number;?>
	  <a href="javascript:void(0)" onclick="javascript:triggerApplySenderId()"><u><?php echo _("learn more")?></u></a>
	</small>
  </div>  
</div>

<div class="col-md-4">
<label>&nbsp;</label>
  <ul class="">
   <li><a href="javascript:void(0)" onclick="javascript:triggerApplySenderId()"><i class="fa fa-gear"></i> <?php echo _("How to Apply SenderID?")?></a></li>
   <li class="text-muted"><input type="checkbox" id="use_sid" name="use_sid" value="1"> <span class="text-muted"><?php echo _("Use My SenderID")?> <small>(<?php echo _("Instead of My Caller Number")?>)</small> </span></li>
  </ul>
</div>
</div>

<div class="row">
<div class="col-md-8">
  <div class="form-group">
    <label><?php echo _("Message")?> </label>
	<textarea class="form-control" rows="6" id="campaign_text" name="campaign_text" required></textarea>
	<small class="text-muted" id="campaign_text_alerter"></small>
  </div>  
 
</div>

<div class="col-md-4">
<label>&nbsp;</label>
  <ul class="">
   <li><a href="javascript:void(0)" onclick="javascript:triggerAddTitle()"><i class="fa fa-tag"></i> <?php echo _("Title Template")?></a></li>
    <li><a href="javascript:void(0)" onclick="javascript:toggleSchedule()"><i class="fa fa-calendar"></i> <?php echo _("Schedule Message")?></a></li>
   
  </ul>
</div>
</div>
	
	
<div class="row" id="schedule_message" style="display:none"> 
<div class="col-md-4">
    <label><?php echo _("Schedule Datetime")?> </label>
	<input type="text" name="cmps_datetime" id="schedule_input" class="form-control" value="" placeholder="<?php echo _('e.g 2016-10-25 12:09:00') ?>">
	<br/>
</div>

<div class="col-md-4">
	<label><?php echo _("Timezone")?></label>
	<?php 
	
	  if(count($getTimezones)>0){
		  ?>
		  <select class="form-control"  id="camp_timezone" name="camp_timezone" >
		  <option value="">--<?php echo _("Select Timezone")?>--</option>
		   <?php 
			for($i=0;$i<count($getTimezones);$i++){
				?>
				<option value="<?php echo $getTimezones[$i]->zone_name;?>" <?php if($getTimezones[$i]->zone_name==@$appSettings[0]->app_timezone){echo 'selected';}?>><?php echo $getTimezones[$i]->zone_name;?></option>
				<?php
			}
		   ?>
		  </select>
		  <?php
	  }
	?>
	
</div>
</div>

<div class="row">
<div class="col-md-4">
  <div class="form-group">
    <button type="button" class="btn btn-primary " ng-click='addCampaignSMS()'>
	<i class="fa  fa-envelope"></i> <?php echo _("Submit")?>
	</button>
	<small class="text-muted">
	(<?php echo _("or")?>) <a href="javascript:void(0)" onclick="javascript:toggleSchedule()"><i class="fa fa-calendar"></i> <?php echo _("Schedule Later")?></a>
	</small>
  </div>  
</div>
<div class="col-md-4">
  <?php 
  $smsMServiceInfo= $this->account_model->get_sms_service($user_id);
  $sms_service_status=@$smsMServiceInfo[0]->trigger_status;
   if($sms_service_status==1){
	    echo "<span class='text-success'><i class='fa fa-paper-plane-o'></i> " . _('SMS services are running..') . "</span>";
   }else{
	   echo "<span class='text-danger'><i class='fa fa-exclamation-triangle'></i> " . _('Start your SMS services') . "</span>";
   }
   ?>
</div>
<div class="col-md-4">
  <div id="dyCampaignProgress">
				 
  </div>
</div>
</div>

</form>
<br><br>
<?php $this->load->view('site/modals/campaign/sms/contact_alert');?>
<?php $this->load->view('site/modals/campaign/sms/howto_apply_senderid');?>
<?php $this->load->view('site/modals/campaign/sms/tonumbers_atcampaign');?>

<?php $this->load->view('site/campaign/fetch_title');?>
<?php $this->load->view('site/campaign/fetch_contact');?>
<?php $this->load->view('site/campaign/fetch_group');?>
<?php $this->load->view('site/campaign/credit_alert');?>
<?php $this->load->view('site/campaign/add_sms_contact');?>
<?php $this->load->view('site/campaign/import_contacts');?>