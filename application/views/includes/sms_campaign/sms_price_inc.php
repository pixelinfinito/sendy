<style>
.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}
</style>

<div class="row">
   <div class="col-md-6">
   <h4><i class="fa fa-dollar"></i>&nbsp; <?php echo _("SMS Pricing")?></h4>
   </div>
   <div class="col-md-6 text-right">
		<a href="<?php echo base_url()?>site/export/export_pricing" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			 <i class="fa fa-file-excel-o"></i>
		</a>
		<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
		  <i class="fa fa-print"></i>
		</a>
			
   </div>
</div>  <hr>
  <table class="table table-bordered table-striped" id="dataTables-SmsPricing">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1">#</th>
		<th class="col-sm-1"><?php echo _("Country Code")?> </th>
		<th class="col-sm-1"><?php echo _("Country Name")?></th>
		<th class="col-sm-1"><?php echo _("Price")?></th>
		<th class="col-sm-1"><?php echo _("Currency")?></th>
		<th class="col-sm-1"><?php echo _("Description")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$price_info=$result=$this->app_settings_model->get_sms_prices();
		$sp = 1 
		?>
		<?php if ($price_info!=0): foreach ($price_info as $price) : ?>

		<tr>
		<td>
		<?php echo $price->p_id?>
		</td>
		<td><?php echo $price->country_code?>
		</td>
		<td><?php echo $price->country_name ?></td>
		<td>
		  <?php echo $price->price ?>  
			 
		</td>
		<td>
		  <?php echo $price->currency_code ?>  
			 
		</td>
	
		<td>
		 <?php echo $price->price_desc ?>
		</td>
		
		</tr>
		<?php
		$sp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
</table> <!-- / Table -->
					
<br><br>
