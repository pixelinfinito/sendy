<style>.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}</style>
<?php 
$appSettings= $this->app_settings_model->get_app_settings();

?>

<div class="row">
   <div class="col-md-4">
	<h4><i class="fa  fa-phone"></i> &nbsp; <?php echo _("Caller Numbers")?></h4>
   </div>
   <div class="col-md-8 text-right">
        <a href="<?php echo base_url()?>site/export/export_caller_numbers" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			<i class="fa fa-hand-stop-o"></i> <?php echo _("What is SenderID?")?>
		</a>
		<a href="<?php echo base_url()?>site/export/export_caller_numbers" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			<i class="fa fa-phone"></i> <?php echo _("What is Caller Number?")?>
		</a>
		
		<a href="<?php echo base_url()?>site/export/export_caller_numbers" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			<i class="fa fa-file-excel-o"></i> 
		</a>
		<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			<i class="fa fa-print"></i> 
		</a>
   </div>
</div>  <hr>
  <table class="table table-bordered table-striped" id="dataTables-callerNumbers">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("Country")?> </th>
		<th class="col-sm-1"><?php echo _("Number")?></th>
		<th class="col-sm-1"><?php echo _("Sender ID")?> </th>
		<th class="col-sm-1"><?php echo _("Lease Period")?></th>
		<th class="col-sm-1"><?php echo _("Price")?></th>
		<th class="col-sm-1"><?php echo _("Purchase On")?></th>
		<th class="col-sm-1"><?php echo _("Expires On")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		
		<tbody>
		<?php 
		$user_id=$this->session->userdata['site_login']['user_id'];
		$caller_numbers=$this->sms_model->get_caller_numbers($user_id);
		$cnp = 1 
		?>
		<?php if ($caller_numbers!=0): foreach ($caller_numbers as $cn) : ?>

		<tr>
		<td>
		  <strong></strong> <?php echo $cn->country?>
		</td>
		<td>
		<?php 
		
		 if($cn->twilio_origin_number!=''){
			
			   switch($cn->status){
			   case 2:
			   echo '<small>'.$cn->twilio_origin_number.'</small><br>';
			   echo "<span class='label label-warning'> <i class='fa fa-hourglass-1'></i> " . _("In Process") . "</span>";
			   break;
			   
			   case 1:
			   echo '<span class="text-success">'.$cn->twilio_origin_number.'</span>';
			   break;
			   
			   case 0:
			   echo '<span class="text-danger">'.$cn->twilio_origin_number.'</span><br>';
			   echo "<span class='label label-danger'><i class='fa  fa-caret-down'></i> " . _("Rejected") . "</span>";
			   break;
			   
			   case 3:
			   echo '<span class="text-danger">'.$cn->twilio_origin_number.'</span><br>';
			   echo "<span class='label label-danger'> <i class='fa fa-caret-down'></i> " . _("Suspended") . "</span>";
			   break;	
			   
			   default:
			   echo "<span class='label label-primary'> <i class='fa fa-meh-o'></i> " . _('Unknown') . "</span>";
			   }			   
		
		 }
		 ?>
		</td>
		
			 
		<td>
		<?php 
		
		   if($cn->twilio_sender_id!=''){
			    
			   switch($cn->sender_id_status){
			   case 2:
			   echo '<small>'.$cn->twilio_sender_id.'</small><br>';
			   echo "<span class='label label-warning'> <i class='fa fa-hourglass-1'></i> " . _('In Process') . "</span>";
			   break;
			   
			   case 1:
			   echo '<span class="text-success"><i class="fa fa-check"></i>'.$cn->twilio_sender_id.'</span>';
			   break;
			   
			   case 0:
			   echo '<span class="text-danger">'.$cn->twilio_sender_id.'</span><br>';
			   echo "<span class='label label-danger'><i class='fa  fa-caret-down'></i> " . _('Rejected') . "</span><br>";
			   ?>
			   <a href="javascript:void(0)" onclick="javascript:cn_remarks('<?php echo $cn->sender_id_remarks?>')">
			   <small><i class="fa fa-question-circle"></i> <?php echo _("Reason?")?></small>
			   </a>
			   <br>
			   <a href="javascript:void(0)" onclick="triggerModalsenderId('<?php echo $cn->tid?>')">
				 <small><i class="fa fa-hand-stop-o"></i> <?php echo _("Request Again")?></small>
			   </a>
			   <?php
			   break;
			   
			   case 3:
			   echo '<span class="text-danger">'.$cn->twilio_sender_id.'</span><br>';
			   echo "<span class='label label-danger'> <i class='fa fa-caret-down'></i> " . _('Suspended') . "</span><br>";
			   ?>
			   <a href="javascript:void(0)" onclick="javascript:cn_remarks('<?php echo $cn->sender_id_remarks?>')">
			   <small><i class="fa fa-question-circle"></i> <?php echo _("Reason?")?></small>
			   </a>
			   <br>
			   <a href="javascript:void(0)" onclick="triggerModalsenderId('<?php echo $cn->tid?>')">
				 <small><i class="fa fa-hand-stop-o"></i> <?php echo _("Apply Again")?></small>
			   </a>
			   <?php
			   break;	
			   
			   default:
			   echo "<span class='label label-primary'> <i class='fa fa-meh-o'></i> " . _('Unknown') . "</span>";
			   }			   
			   
               
		   } else{
			   ?>
			    <div  class="ad_free_btn expwi50" >
					<a href="javascript:void(0)" onclick="triggerModalsenderId('<?php echo $cn->tid?>')">
					<span class="expclr"><i class="fa fa-hand-stop-o"></i> <?php echo _("Apply")?></span>
					</a>
				</div>
			   <?php
			   
		   }
		?>
		</td>
   
        <td>
		<?php echo $cn->subscription_period.' <small> ' . _('Month(s)') . '</small>'?>
		</td>
		
		 <td>
		<?php echo $cn->cn_currency.' '.$cn->price?>
		</td>
		
		 <td>
		<?php echo date('d-M-Y H:i:s',strtotime($cn->purchased_on))?>
		</td>
		
		<td>
		<?php
		date_default_timezone_set($appSettings[0]->app_timezone);
		$expires_in = strtotime($cn->expired_on) - strtotime($cn->purchased_on);
		$today=date('Y-m-d H:i:s');
		if(strtotime($cn->expired_on) > strtotime($today))
		{
			$number_expires= floor($expires_in/3600/24);
			echo "
			<span class='text-success'>
			<i class='fa fa-clock-o'></i>   
			<strong>". $number_expires." </strong> <small>" . _('Day(s)') . "</small>
			</span>";
		}
		else
		{
			echo "<span class='text-danger'>
					<i class='fa fa-exclamation-triangle'></i> " . _('Expired
				  ') . "</span><br>";
				  ?>
				  <a href="javascript:alert('Sorry this feature is currently disabled.')"><u><small><?php echo _("Extend ?")?></small></u></a>
				  <?php
		}
		?>
	
		<?php //echo date('d-M-Y H:i:s',strtotime($cn->expired_on))?>
		</td>
		
		
		<td>
		 <?php 
		
		   switch($cn->status){
			   case 2:
			   echo "<span class='label label-warning'> <i class='fa fa-hourglass-1'></i> " . _('In Process') . "</span>";
			   break;
			   
			   case 1:
			   echo "<span class='label label-success'> <i class='fa fa-caret-up'></i> " . _('Active') . "</span>";
			   break;
			   
			   case 0:
			   echo "<span class='label label-danger'><i class='fa  fa-caret-down'></i> " . _('Rejected') . "</span><br>";
			   ?>
			   <a href="javascript:void(0)" onclick="javascript:cn_remarks('<?php echo $cn->sender_id_remarks?>')">
			   <small><i class="fa fa-question-circle"></i> <?php echo _("Reason?")?></small>
			   </a>
			   <?php
			   break;
			   
			   case 3:
			   echo "<span class='label label-danger'> <i class='fa fa-caret-down'></i> " . _('Suspended') . "</span><br>";
			   ?>
			    <a href="javascript:void(0)" onclick="javascript:cn_remarks('<?php echo $cn->sender_id_remarks?>')">
			   <small><i class="fa fa-question-circle"></i> <?php echo _("Reason?")?></small>
			   </a>
			   <?php
			   break;
			   
			   default:
			   echo "<span class='label label-primary'> <i class='fa fa-meh-o'></i> " . _('Unknown') . "</span>";
			   
		   }
		  ?>
		</td>
		<td>
			
		   <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delCallerNumber('<?php echo $cn->tid?>')" class="text-danger">
				<i class='glyphicon glyphicon-trash'></i> 
			</a>
		
		</td>

		</tr>
		<?php
		$cnp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="9" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
		
			
		<br><br>


<?php 
  
  $this->load->view('site/modals/campaign/sms/delete_caller_number');
  $this->load->view('site/modals/campaign/sms/caller_number_remarks');
  $this->load->view('site/campaign/sender_id');

?>


