<?php 
if(isset($_POST['channel_year'])){
	$year=$this->input->post('channel_year');
}else{
	$year=date('Y');
}
$user_id=$this->session->userdata['site_login']['user_id'];
$campaign_summary=$this->sms_model->get_campaign_overview($user_id);
$unique_countries=$this->sms_model->get_campaign_unique_countries($user_id,$year);
$campaign_yearby=$this->sms_model->get_campaign_graph_yearby($user_id,$year);

$months_arr=array("01"=>'Jan',"02"=>'Feb',"03"=>'Mar',"04"=>'Apr',
"05"=>'May',"06"=>'Jun',"07"=>'Jul',"08"=>'Aug',"09"=>'Sep',"10"=>'Oct',"11"=>'Nov',"12"=>'Dec');
$months_arr2=array(1,2,3,4,5,6,7,8,9,10,11,12);
$i=0;
 
 
?>

			
<link href="<?php echo base_url()?>charts/css/float.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="<?php echo base_url()?>charts/js/canvasjs.min.js"></script>
<script type="text/javascript">
     window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		theme: "theme3",
		title:{
			text: ""
		},
		data: [
		{
			type: "pie",
			showInLegend: true,
			toolTipContent: "{y} - #percent %",
			yValueFormatString: "#.#,,. In%",
			legendText: "{indexLabel}",
			dataPoints: [
			 <?php 
			  foreach($unique_countries as $country){
			   $smscount=$this->sms_model->get_smscountry_graphcount($country->country_code,$user_id,$year); 
			   echo "{  y:".$smscount.",indexLabel:'".$country->country_name.'('.$smscount.')'."'},";
			 }
			?>
			]
			
		}
		]
	});
	chart.render();
	///// line chart
	var chart2 = new CanvasJS.Chart("chartContainer1", {
		theme: "theme2",
		title: {
			text: ""
		},
		animationEnabled: true,
		axisX: {
			valueFormatString: "MMM",
			interval: 1,
			intervalType: "month"

		},
		axisY: {
			includeZero: false

		},
		data: [{
			type: "line",
			
			dataPoints: [
			<?php 
			
				foreach($campaign_yearby as $campaign){
				$empty=0;
				$exp_smscount=explode('-',$campaign->do_sent);
				$month=$exp_smscount[1];
				$smscount=$this->sms_model->get_sms_graphcount($month,$year,$user_id); 
				echo '{  x: new Date('.$year.','.($month-1).',1'.'),y:'.count($smscount).'},';
				
				$i++;
				}
			
			
			?>
			
			
			]
		}
		]
	});
			
	chart2.render();
}
	

	
</script>
	
<div class="row">
	<div class="col-md-6">
	<h4>
		<i class="fa fa-pie-chart"></i>&nbsp; <?php echo _("Reports")?>
	</h4>
	</div>
	</div>
<hr>

<?php 

$success_count =$this->sms_model->get_success_count_byuser($user_id);
$failed_count=$this->sms_model->get_unsuccess_count_byuser($user_id);

$paymentInfo= $this->payment_model->get_member_payments($user_id);
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
$tPayment=array();
if($paymentInfo!='0'){
	foreach(@$paymentInfo as $payment){
		array_push($tPayment,$payment->c2o_total_amount);
	}
}
		
?>  
	<div class="row">
	<div class="">
	<!--/// campaigns block ////-->
	<div class="col-md-3">
		<div class="graybg margin-bottom-0">
		<b class="text-muted"><i class="fa fa-send-o"></i>  <?php echo _("All Campaigns")?></b>
		</div>
		<div class="padding-10 border_all">
			<div class="row">
				<div class="col-md-7">
					<?php 

					if($campaign_summary!=0){
					if(count($campaign_summary)<10){
					$allCamps="0".count($campaign_summary);
					}else{
					$allCamps=count($campaign_summary);
					}
					echo "<h4 class='no-padding text-warning font-bold'>".$allCamps."</h4>";
					}else{
					echo "<h4 class='no-padding text-danger font-bold'>(0)</h4>";
					}
					?>
				</div>
				<div class="col-md-5">
					<br>
					<div class="round-border-10">
						<a href="<?php echo base_url()?>site/export/export_campaigns" title="<?php echo _('Export') ?>">
						<small><i class="fa fa-arrow-up"></i><?php echo _("Export")?></small>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/// messages sent block ////-->
	<div class="col-md-3">
		<div class="graybg margin-bottom-0">
		<b class="text-muted"><i class="fa fa-check-circle"></i> <?php echo _("Total Messages Sent")?></b>
		</div>
		<div class="padding-10 border_all">
			<div class="row">
				<div class="col-md-7">
					<?php 

					if($success_count!=0){
					if($success_count<10){
					$total_success_count="0".$success_count;
					}else{
					$total_success_count=$success_count;
					}
					echo "<h4 class='no-padding text-success font-bold'>".$total_success_count."</h4>";
					}else{
					echo "<h4 class='no-padding text-danger font-bold'>(0)</h4>";
					}
					?>
				</div>
				<div class="col-md-5">
					<br>
					<div class="round-border-10">
					<a href="<?php echo base_url()?>site/export/export_success_history" title="<?php echo _('Export') ?>">
					<small><i class="fa fa-arrow-up"></i><?php echo _("Export")?></small>
					</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/// messages failed block ////-->
	<div class="col-md-3">
		<div class="graybg margin-bottom-0">
		<b class="text-muted"><i class="fa fa-times-circle"></i> <?php echo _("Total  Messages Failed")?> </b>
		</div>
		<div class="padding-10 border_all">
			<div class="row">
				<div class="col-md-7">
					<?php 

					if($failed_count!=0){
					if($failed_count<10){
					$total_failed_count="0".$failed_count;
					}else{
					$total_failed_count=$failed_count;
					}
					echo "<h4 class='no-padding text-danger font-bold'>".$total_failed_count."</h4>";
					}else{
					echo "<h4 class='no-padding text-danger font-bold'>(0)</h4>";
					}
					?>
				</div>
				<div class="col-md-5">
					<br>
					<div class="round-border-10">
					<a href="<?php echo base_url()?>site/export/export_failed_history" title="<?php echo _('Export') ?>">
					<small><i class="fa fa-arrow-up"></i><?php echo _("Export")?></small>
					</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/// payments block ////-->
	<div class="col-md-3">
		<div class="graybg margin-bottom-0">
		<b class="text-muted"><i class="fa fa-money"></i> <?php echo _("Total Payments")?></b>
		<?php echo "<small class='text-muted'><em>(".@$currencyInfo[0]->currency_name.")</em></small>";?>
		</div>
		<div class="padding-10 border_all">
			<div class="row">
			<div class="col-md-7">
				<?php 
				if(@$paymentInfo[0]->acc_item_number!=''){
				?>
				<h4 class="text-primary font-bold">
				<?php echo @$currencyInfo[0]->character_symbol.''.array_sum(@$tPayment);?>
				</h4>
				<?php
				}else{
				?>
				<h4 class="text-danger font-bold">
				<?php echo @$currencyInfo[0]->character_symbol."(0)";?>
				</h4>
				<?php
				}
				?>
			</div>
			<div class="col-md-5">
				<br>
				<div class="round-border-10">
				<a href="<?php echo base_url()?>site/payment/history" title="<?php echo _('Export') ?>">
				<small><i class="fa fa-list"></i><?php echo _("Details")?></small>
				</a>
				</div>
			</div>
			</div>
			</div>
		</div>
		</div> 
	</div>
<br><br>
<hr>
<div class="row">
<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<?php 
 
 $unique_years=$this->sms_model->get_campaign_unique_years($user_id);
 
?>
<?php echo _("Message Report for Year ")?>: <?php echo "<b>".$year."</b>"?>
</div>
<div class="col-md-6">
<form action="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('rpt')?>" method="post">
 <select class="form-control" onchange='this.form.submit()' name="channel_year" id="channel_year">
  <option value="">--<?php echo _("Year")?>--</option>
  <?php
  foreach($unique_years as $campaign){
	?>
	<option value="<?php echo  $campaign->parse_year?>"><?php echo  $campaign->parse_year?></option>
	<?php
	 
 }
  ?>
 </select>
 </form>
</div>
</div>
	<div class="demo-container">
	      <?php 
		  
		  if($campaign_yearby!=0){
		   ?>
		   
		   <div id="chartContainer1" style="width: 100%; height: 300px;display: inline-block;"></div>
		  <?php 
			}else{
				echo "<div class='text-center'><h4 class='text-muted'>
				<i class='fa fa-meh-o'></i> <em>" . _('no data to display') . "</em></h4></div>";
			}
			
		  ?>		  
	</div>
	
</div>


<!-- pie chart -->
<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<?php 
 $unique_years=$this->sms_model->get_campaign_unique_years($user_id);
 
?>
(%) by Country Year of : <?php echo "<b>".$year."</b>"?>
</div>
<div class="col-md-6">
 <form action="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('rpt')?>" method="post">
 <select class="form-control" onchange='this.form.submit()' name="channel_year" id="channel_year">
  <option value="">--<?php echo _("Year")?>--</option>
  <?php
  foreach($unique_years as $campaign){
	?>
	<option value="<?php echo  $campaign->parse_year?>"><?php echo  $campaign->parse_year?></option>
	<?php
	 
 }
  ?>
  
 </select>
</div>
</div>
	<div class="demo-container">
	    <?php 
		  if($unique_countries!=0){
		   ?>
		   
		  <div id="chartContainer" style="height:300px; width: 100%;"></div>
		 <?php 
			}else{
				echo "<div class='text-center'><h4 class='text-muted'>
				<i class='fa fa-meh-o'></i> <em>" . _('no data to display') . "</em></h4></div>";
			}
			
		  ?>	
	</div>
	
</div>
</div>