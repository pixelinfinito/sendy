<style>
.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}
</style>

        <?php 
		$appSettings= $this->app_settings_model->get_primary_settings();
		$appCountry=$appSettings[0]->app_country;
		$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
		$user_id=$this->session->userdata['site_login']['user_id'];
		
		if(isset($_POST['hist_bydate'])){
			 $fromDate=$this->input->post('hist_fromdate');
			 $toDate=$this->input->post('hist_todate');
			 $history_info=$this->sms_model->get_campaign_history_bydates($user_id,$fromDate,$toDate);
			 $reCount="(".count($history_info)." records filtered )";
		}else{
			$history_info=$this->sms_model->get_campaign_history($user_id);
		}
		
		
		?>
	
<div class="row">
   <div class="col-md-6">
   <h4><i class="fa fa-history"></i> <?php echo _("History")?> <small><?php echo @$reCount;?></small></h4>
   </div>
   <div class="col-md-6 text-right">
       <?php echo _("Period")?>: <b><?php echo _("All")?></b>
        <a href="javascript:void(0)" class="btn btn-sm explink"  title="<?php echo _('Search History by Dates') ?>" onclick="javascript:triggerSearchHistoryDates()">
			 <i class="fa fa-search"></i> <?php echo _("Filter by Dates")?>
		</a>
		<a href="<?php echo base_url()?>site/export/export_history" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			 <i class="fa fa-file-excel-o"></i>
		</a>
		<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
		  <i class="fa fa-print"></i>
		</a>
	
   </div>
</div>  <hr>
  <table class="table table-bordered table-striped" id="dataTables-SmsHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("C.Code")?></th>
		<th class="col-sm-1"><?php echo _("From")?></th>
		<th class="col-sm-1"><?php echo _("To")?></th>
		<th class="col-sm-2"><?php echo _("Message Body")?></th>
		<th class="col-sm-1"><?php echo _("Unit Cost")?></th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Actions")?></th>
		
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$h = 1 
		?>
		<?php if ($history_info!=0): foreach ($history_info as $history) : ?>

		<tr>
		<td>
		 <?php echo $history->campaign_id?>
		</td>
		<td>
		 <?php echo "#".$history->campaign_code?>
		</td>
		<td>
		  <?php echo $history->from_number?>
		</td>
		<td>
		  <?php echo $history->to_number.'<br>'.'<small class="text-muted">'.$history->group_name.'</small>' ?>
		 </td>
		<td>
		  <?php echo $history->message ?>
		  <?php 
		   if($history->is_scheduled==1){
			   echo '<hr><small><i class="fa fa-calendar"></i> <b> ' . _('Scheduled') . ' : </b><br>'.date('d-M-Y H:i:s',strtotime($history->scheduled_datetime)).'</small>';
		   }
		  ?>
			 
		</td>
		<td>
		  <?php echo "<small class='text-muted'>".$currencyInfo[0]->currency_name."</small> ".$history->unit_cost ?>  
		 
		</td>
	
		<td>
		   
		   <?php 
		    switch($history->deliver_status){
				case '3':
				echo '<span class="label label-primary"><i class="fa  fa-hourglass-end"></i> ' . _('Queued') . '</span>';
				break;
				
				case '2':
				echo '<span class="label label-warning"><i class="fa  fa-hourglass-half"></i> ' . _('Pending') . '</span>';
				echo '<br><small>'.date('d-M-Y H:i:s',strtotime($history->do_submit)).'</small>';
				break;
				
				case '1':
				echo '<span class="label label-success"><i class="fa  fa-check"></i> ' . _('Delivered') . '</span>';
				echo '<br><small>'.date('d-M-Y H:i:s',strtotime($history->do_sent)).'</small>';
				break;
				
				case '0':
				echo '<span class="label label-danger">	<i class="fa fa-exclamation-triangle"></i> ' . _('Failed ') . '</span>';
				echo '<br><small>'.date('d-M-Y H:i:s',strtotime($history->do_sent)).'</small>';
				?>
				<a href="javascript:void(0)" onclick="failRemarkslnk('<?php echo $history->campaign_id?>')"><small><?php echo _("Remarks")?></small></a>
				<?php
				break;
			}
		   ?>
		   
		</td>
		<td>
		 <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delSmsHistory('<?php echo $history->campaign_id?>','<?php echo $history->to_number?>')" class="text-danger">
			<i class='glyphicon glyphicon-trash'></i> 
			</a>
		</td>

		</tr>
		<?php
		$h++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="8" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
</table> <!-- / Table -->
					
<br><br>


<?php 
$this->load->view('site/modals/campaign/sms/delete_sms_history');
$this->load->view('site/modals/campaign/sms/search_history_bydate');
$this->load->view('site/campaign/sms_remarks');
?>
