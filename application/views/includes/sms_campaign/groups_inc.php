<style>
.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}
</style>

  <div class="row">
   <div class="col-md-6">
   <h4><i class="fa fa-users"></i>&nbsp; <?php echo _("Contact Groups")?></h4>
   </div>
   <div class="col-md-6 text-right">
			<a href="javascript:void(0)" onclick="javascript:triggerImportModal()" class="btn btn-sm explink"  title="<?php echo _('Import Contacts') ?>">
			<i class="fa fa-download"></i>
			</a>
			
			<a href="<?php echo base_url()?>site/export/export_groups" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			 <i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			  <i class="fa fa-print"></i>
			</a>
			
			<a href="javascript:void(0)" onclick="javascript:triggerSMSContact()" class="btn btn-sm explink"  title="<?php echo _('Add New Contact') ?>">
			 <i class="fa fa-user-plus"></i> <?php echo _("New Contact")?>
			</a>
			
			 <a href="javascript:void(0)" onclick="javascript:triggerSMSGroup()" class="btn btn-sm explink"  title="<?php echo _('Add New Group') ?>">
			 <i class="fa fa-plus-circle"></i> <?php echo _("New Group")?>
			</a>
   </div>
</div>  <hr>
  <table class="table table-bordered table-striped" id="dataTables-campaignGroups">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?> </th>
		<th class="col-sm-1"><?php echo _("Name")?> </th>
		<th class="col-sm-1"><?php echo _("Type")?> </th>
		<th class="col-sm-1"><?php echo _("Total Contacts")?></th>
		<th class="col-sm-1"><?php echo _("Group Created On")?> </th>
		<th class="col-sm-1"><?php echo _("Action")?></th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$user_id=$this->session->userdata['site_login']['user_id'];
		$groups_info=$result=$this->sms_model->get_groups_byuser($user_id);;
		$gp = 1 
		?>
		<?php if ($groups_info!=0): foreach ($groups_info as $group) : ?>

		<tr>
		<td>
		<?php echo $group->cgroup_id?>
		</td>
		<td><?php echo $group->group_name?>
		</td>
		<td><?php echo $group->group_type ?></td>
		<td >
		    <?php 
			  $cg_contacts=$this->sms_model->get_group_contacts($group->cgroup_id,$user_id);
			  
			 if($cg_contacts!=0){
			
				if(count($cg_contacts)<10){
					echo " <a href='#'>(0".count($cg_contacts).")</a> ";
				}else{
					echo " <a href='#'>(".count($cg_contacts).")</a> ";
				}
			}
			?>
			 
			 
		</td>
		<td>
		<?php 
		 echo date('d-M-Y H:i:s',strtotime($group->updated_on)); 
		?>
		</td>
		<td>
		<a href="javascript:void(0)" title="<?php echo _('Edit') ?>" class="text-warning" onclick="triggerEditCgGroup('<?php echo $group->cgroup_id?>')">
		<span><i class="fa fa-pencil"></i></span>
		</a> <span class="custom_title12">|</span>

        <?php 
		 if($group->group_name!='Default'){
		?>
			<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delCgGroup('<?php echo $group->cgroup_id?>')" class="text-danger">
			<i class='glyphicon glyphicon-trash'></i> 
			</a>
		<?php 
		 }else{
			 echo "<span title='Restricted' class='text-muted'><i class='glyphicon glyphicon-trash'></i></span> ";
		 }
		?>
		
		</td>

		</tr>
		<?php
		$gp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="6" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
					
		<?php 
		 $this->load->view('site/modals/campaign/sms/delete_group');
		?>
			
		<br><br>


<?php 
  $this->load->view('site/modals/campaign/sms/edit_sms_group');
  $this->load->view('site/campaign/add_sms_group');
  $this->load->view('site/campaign/add_sms_contact');
  $this->load->view('site/campaign/import_contacts');

 
?>


