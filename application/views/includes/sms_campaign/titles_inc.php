<style>.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}</style>
<div >
<div class="row">
   <div class="col-md-6">
   <h4><i class="fa fa-tags"></i>&nbsp; <?php echo _("Title Template")?></h4>
   </div>
   <div class="col-md-6 text-right">
    <a href="<?php echo base_url()?>site/export/export_template" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			 <i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			  <i class="fa fa-print"></i>
			</a>
			
		     <a href="javascript:void(0)" onclick="javascript:triggerSMSTitle()" class="btn btn-sm explink"  title="<?php echo _('Add Title') ?>">
			 <i class="fa fa-plus-circle"></i> <?php echo _("New Title")?>
			</a>
			
   </div>
</div>  <hr>
  <table class="table table-bordered table-striped" id="dataTables-templateTitles">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?> </th>
		<th class="col-sm-1"><?php echo _("Template Code")?> </th>
		<th class="col-sm-1"><?php echo _("Title")?> </th>
		<th class="col-sm-1"><?php echo _("Status")?></th>
		<th class="col-sm-1"><?php echo _("Action")?></th>
		</tr>
		</thead><!-- / Table head -->
		
		<tbody>
		<?php 
		$user_id=$this->session->userdata['site_login']['user_id'];
		$ttitles_info=$this->sms_model->get_template_titles($user_id);
		$lp = 1 
		?>
		<?php if ($ttitles_info!=0): foreach ($ttitles_info as $title) : ?>

		<tr>
		<td>
		<?php echo $title->template_id?>
		</td>
		<td>
		<?php echo $title->template_code?>
		</td>
		<td>
		<?php echo $title->template_title ?>
		</td>

		<td>
		 <?php 
		
		   switch($title->status){
			   case '1':
			   echo "<span class='label label-success'> <i class='fa fa-caret-up'></i> " . _('Active') . "</span>";
			   break;
			   
			   case '0':
			   echo "<span class='label label-primary'><i class='fa  fa-caret-down'></i> " . _('InActive') . "</span>";
			   break;
		   }
		  ?>
		</td>
		<td>
			<a href="javascript:void(0)" title="<?php echo _('Edit') ?>" class="text-warning" onclick="triggerEditTTitle('<?php echo $title->template_code?>')">
			<span><i class="fa fa-pencil"></i></span>
			</a> <span class="custom_title12">|</span>

		   <a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delTemplateTitle('<?php echo $title->template_code?>')" class="text-danger">
				<i class='glyphicon glyphicon-trash'></i> 
			</a>
		
		</td>

		</tr>
		<?php
		$lp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="5" class="text-center">
		<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
		
			
		<br><br>
</div>

<?php 
  $this->load->view('site/modals/campaign/sms/edit_template_title');
  $this->load->view('site/campaign/add_template_title');
  $this->load->view('site/modals/campaign/sms/delete_template_title');

?>




