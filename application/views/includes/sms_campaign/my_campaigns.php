<style>.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}</style>
<div >
<div class="row">
<div class="col-md-6">
  <h4><i class="fa fa-send-o"></i>&nbsp; <?php echo _("All Campaigns")?></h4>
</div>
<div class="col-md-6 text-right">
		<a href="<?php echo base_url()?>site/export/export_campaigns" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			<i class="fa fa-file-excel-o"></i> 
		</a>
		<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			<i class="fa fa-print"></i> 
		</a>
</div>
</div>
<hr>
<table class="table table-bordered table-striped" id="dataTables-sCampaigns">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1 "><?php echo _("ID")?> </th>
		<th class="col-sm-1 "><?php echo _("Campaign Code")?> </th>
		<th class="col-sm-1 "><?php echo _("Message")?> </th>
		<th class="col-sm-1 "><?php echo _("Sent On")?></th>
		<th class="col-sm-1 "><?php echo _("Summary")?> </th>
		<th class="col-sm-1 "><?php echo _("Type")?> </th>
		<th class="col-sm-1 "><?php echo _("Status")?> </th>
		<th class="col-sm-1 "><?php echo _("Action")?></th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$user_id=$this->session->userdata['site_login']['user_id'];
		$campaigns_info=$this->sms_model->get_campaign_overview($user_id);
		$cmp = 1 
		?>
		<?php if ($campaigns_info!=0): foreach ($campaigns_info as $campaigns) : ?>

		<tr>
		<td>
		<?php echo $campaigns->campaign_id?>
		</td>
		<td><?php echo "#".$campaigns->campaign_code?>
		<br>
		<small class="text-muted"><b><?php echo _("Submit On")?></b><br>
		 <?php echo date('d-M-Y H:i:s',strtotime($campaigns->do_submit)); ?>
		</small>
		
		</td>
		<td><?php echo $campaigns->message ?></td>
		
		<td>
		 <?php 
		  if($campaigns->do_sent!='0000-00-00 00:00:00'){
			 echo date('d-M-Y H:i:s',strtotime($campaigns->do_sent));
		  }else{
			  echo "<span class='label label-default'><i class='fa fa-hourglass-half'></i> " . _("Waiting..") . "</span>"; 
		  }
		 
		 ?>
		</td>
		<td width="25%">
		    <?php 
			  $success_count=$this->sms_model->get_success_count($campaigns->campaign_code,$user_id);
			  $participated_count=$this->sms_model->get_campaign_contacts_count($campaigns->campaign_code,$user_id);
			  $failed_count=$this->sms_model->get_unsuccess_count($campaigns->campaign_code,$user_id);
			?>
			  
			  <div class="row text-left">
			  <div class="col-md-4">
			    <div class="ad_free_btn"><?php echo _("Total")?>  <?php echo "(".$participated_count.")";?></div>
			  </div>
			  <div class="col-md-2">
			   &nbsp;
			  </div>
			  <div class="col-md-6">
			     <?php 
				  if($success_count!=0){
					  $failed=$participated_count-$success_count;
				  }else{
					  if($failed_count!=0){
						  $failed=$failed_count;
					  }else{
						  $failed="0";
					  }
				  }
				  ?>
				  
				  <?php echo "<span class='text-success'><i class='fa fa-check-circle'></i> success (".$success_count.") </span><br>";?>
				  <?php echo "<span class='text-danger'><i class='fa fa-exclamation-circle'></i> failed (".$failed.")</span>";?>
			  </span>
			  </div>
			  </div>
			
			 
		</td>
		<td>
		 <?php 
		   switch($campaigns->is_scheduled){
			   case '0':
			   echo "<span class='text-primary'><i class='fa fa-leaf'></i> " . _("regular") . "</span>";
			   break;
			   
			   case '1':
			   echo "<span class='text-warning'><i class='fa fa-clock-o'></i> " . _("scheduled") . "</span>";
			   break;
		   }
		  ?>
		</td>
		<td>
		<?php 
		   switch($campaigns->status){
			   case '1':
			   echo "<span class='label label-success'><i class='fa fa-check'></i> " . _("Completed") . "</span>";
			   break;
			   
			   case '0':
			   echo "<span class='label label-warning'><i class='fa fa-hourglass-half'></i> " . _("Waiting") . "</span>";
			   break;
		   }
		  ?>
		</td>
		<td>
			<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delCampaigns('<?php echo $campaigns->campaign_code?>')" class="text-danger">
			<i class='glyphicon glyphicon-trash'></i> 
			</a>
		</td>

		</tr>
		<?php
		$cmp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="8" class="text-center">
		   <h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
					
		<?php 
		$this->load->view('site/modals/campaign/sms/delete_campaign');
		
		?>
			
		<br><br>
</div>
