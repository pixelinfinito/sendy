<style>.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}</style>
<div >

 <div class="row">
   <div class="col-md-6">
   <h4><i class="fa  fa-calendar-plus-o"></i>&nbsp; <?php echo _("Scheduled Campaigns")?></h4>
   </div>
   <div class="col-md-6 text-right">
			<a href="<?php echo base_url()?>site/export/export_schedules" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			 <i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			  <i class="fa fa-print"></i>
			</a>
			
			<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>" class="btn btn-sm explink">
			  <i class="fa fa-envelope"></i> <?php echo _("Start Campaign")?>
			</a>
			
   </div>
</div>  <hr>
  <table class="table table-bordered table-striped" id="dataTables-cSchedules">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?> </th>
		<th class="col-sm-1"><?php echo _("Campaign Code")?> </th>
		<th class="col-sm-1"><?php echo _("Message")?> </th>
		<th class="col-sm-1"><?php echo _("Submit Datetime")?> </th>
		<th class="col-sm-1"><?php echo _("Scheduled Datetime")?> </th>
		<th class="col-sm-1"><?php echo _("Summary")?> </th>
		<th class="col-sm-1"><?php echo _("Status")?> </th>
		<th class="col-sm-1"><?php echo _("Actions")?> </th>

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		$user_id=$this->session->userdata['site_login']['user_id'];
		$schedules_info=$this->sms_model->get_campaign_schedules($user_id);
		$key = 1 
		?>
		<?php if ($schedules_info!=0): foreach ($schedules_info as $schedule) : ?>

		<tr>
		<td>
		<?php echo $schedule->campaign_id?>
		</td>
		
		<td>
		<?php echo $schedule->campaign_code?>
		</td>
		
		<td>
		<?php echo $schedule->message ?>
		</td>
		
		<td>
		 <?php echo date('d-M-Y H:i:s',strtotime($schedule->do_submit)); ?>
		</td>
		
		<td>
		<?php 
		   echo date('d-M-Y H:i:s',strtotime($schedule->scheduled_datetime)); 
		?>
		</td>
		
		<td width="25%">
		    <?php 
			  $success_count=$this->sms_model->get_success_count($schedule->campaign_code,$user_id);
			  $participated_count=$this->sms_model->get_campaign_contacts_count($schedule->campaign_code,$user_id);
			?>
			  
			  <div class="row text-left">
			  <div class="col-md-4">
			    <div class="ad_free_btn"><?php echo _("Total")?>  <?php echo "(".$participated_count.")";?></div>
			  </div>
			  <div class="col-md-2">
			   &nbsp;
			  </div>
			  <div class="col-md-6">
			     <?php 
				  if($success_count!=0){
					  $failed=$participated_count-$success_count;
				  }else{
					  $failed="0";
				  }
				  
				  ?>
				  <?php echo "<span class='text-success'><i class='fa fa-check-circle'></i> success (".$success_count.")</span><br>";?>
				  <?php echo "<span class='text-danger'><i class='fa fa-exclamation-circle'></i> failed (".$failed.")</span>";?>
			  </span>
			  </div>
			  </div>
			
			 
		</td>
		
		<td>
		  <?php 
		   switch($schedule->status){
			   case '1':
			   echo "<span class='label label-success'><i class='fa fa-check'></i> " . _("Completed") . "</span>";
			   break;
			   
			   case '0':
			   echo "<span class='label label-warning'><i class='fa fa-hourglass-half'></i> " . _("Waiting") . "</span>";
			   break;
		   }
		  ?>
		</td>
		
		<td>
		<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delSchedules('<?php echo $schedule->campaign_code?>')" class="text-danger">
		<i class='glyphicon glyphicon-trash'></i> 
		</a>
		
		</td>

		</tr>
		<?php
		$key++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="8" class="text-center">
		  <h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody><!-- / Table body -->
		</table> <!-- / Table -->
					
		<?php 
		$this->load->view('site/modals/campaign/sms/delete_schedule');
		?>
			
		<br><br>
</div>
