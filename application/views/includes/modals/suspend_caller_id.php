<div class="modal fade" id="suspendCiModal" tabindex="-1" role="dialog" 
     aria-labelledby="suspendCiModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Confirm Alert")?>
                </h4>
            </div>
			<div class="modal-body">
			   <h1><?php echo _("Are you sure ?")?></h1>
			    <?php echo _("You want to suspend")?>  [ <strong id="dlx_ci"> </strong> ] <br><br>
			   
			   <b class="text-danger"><?php echo _("Say Some Remarks")?></b>
			   <textarea class="form-control" id="suspend_remarks" name="suspend_remarks" rows="5"><?php echo _("N/A")?></textarea><br>
			   <div id="ciSuspendStatus"></div>
			 </div>
           
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  <div class="pull-left"> </div>
				</div>
				<div class="col-md-6">
				<button type="button" data-dismiss="modal" class="btn btn-danger" id="delete"><?php echo _("Suspend")?></button>
				<button type="button" data-dismiss="modal" class="btn"><?php echo _("Cancel")?></button>
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>