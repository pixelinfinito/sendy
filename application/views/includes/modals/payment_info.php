
	<!-- Modal -->
<div class="modal fade" id="modalpayment" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Payment Information")?>
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
               
			   <span id="dyLPayment">
			   </span>
             
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <small><?php echo _("Please choose the service option and proceed")?></small>
            </div>
        </div>
    </div>
</div>		