<div class="modal fade" id="approveCiModal" tabindex="-1" role="dialog" 
     aria-labelledby="approveCiModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Confirm Alert")?>
                </h4>
            </div>
			<div class="modal-body">
			  
			   <div class="row">
			   <div class="col-md-6">
			     <h1><i class="fa fa-phone"></i></h1>
				 <b><?php echo _("CallerID Availability")?></b><br>
				 
				 <span id="ci_availability"></span>
			   </div>
			   
			   <div class="col-md-6">
			   <h1><?php echo _("Are you sure ?")?></h1>
			   <?php echo _("You want approve")?>[<strong><span  id="apx_ci"></span> </strong>] <br><br>
			   <div id="ciApproveStatus"></div>
			   </div>
			   
			   </div>
			   
			 </div>
           
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  <div class="pull-left"> </div>
				</div>
				<div class="col-md-6">
				<button type="button" data-dismiss="modal" class="btn btn-primary" id="approve"><?php echo _("Approve")?></button>
				<button type="button" data-dismiss="modal" class="btn"><?php echo _("Cancel")?></button>
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>