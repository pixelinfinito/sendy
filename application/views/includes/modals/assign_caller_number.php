
	<!-- Modal -->
<div class="modal fade" id="assignCNModal" tabindex="-1" role="dialog" 
     aria-labelledby="assignCNLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="assignCNLabel">
                    <?php echo _("Assign Caller Number")?>
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
               
			   <div class="row">
			    <div class="col-md-12">
				 <div class="col-md-4 text-center">
				    <h1>
					<i class="fa fa-user"></i>
					</h1>
				   <small class="text-muted"><?php echo _("Assigning to")?></small>
				   <h4><span id="cnx_name"></span></h4>
				 </div>
				 <div class="col-md-8">
				 
				 <form class="form-horizontal"  role="form" name="assignCNForm" id="assignCNForm">
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Number")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				 <input type="text" class="form-control"  name="member_cn" id="member_cn" placeholder="<?php echo _('E.g. +673xxxxxx') ?>" required="required">
				 <input type="hidden" name="cnx_tid" id="cnx_tid" class="form-control"/>
				 </div>
				 </div>
				 </div>
				 </div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Country")?></label></div>
				 <div class="col-md-8">
				    <span id="cnx_country"></span>
				 </div>
				 </div>
				 </div>
				</div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				  <button class="btn btn-border btn-success" type="button" onclick='javascript:assignCnfCallerNumber()'>
				   <i class="fa fa-plus-circle"></i> <?php echo _("Assign")?>
			     </button>
				   
				 </div>
				 <div class="col-md-6" id="cnProgress">
				 
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				    
				 </div>
				</div>
			   </div>
             
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <small class="red pull-left">* <?php echo _("Mandatory fields")?></small>
            </div>
        </div>
    </div>
</div>		