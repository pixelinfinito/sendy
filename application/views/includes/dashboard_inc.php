<?php 
$appSettings= $this->app_settings_model->get_primary_settings();
$currency_name=$appSettings[0]->app_currency;


$all_members=$this->account_model->list_all_members();
$all_inactive_members=$this->account_model->list_all_inactive_members();
$visitors=$this->countries_model->list_visitors();
$visitors_count=$this->countries_model->list_visitors_count();

$total_payment_transactions=$this->payment_model->total_member_payments();
$total_failed_payments=$this->payment_model->total_failed_payments();

$total_pending_campaign_count =$this->sms_model->total_pending_campaign_count();
$total_campaign_count =$this->sms_model->total_campaign_count();

$total_successful_smscount=$this->sms_model->total_successful_smscount();
$total_unsuccessful_smscount=$this->sms_model->total_unsuccessful_smscount();

$recent_campaigns_atadmin=$this->sms_model->recent_campaigns_atadmin();
$list_latest_members=$this->account_model->list_latest_members();
?>
			   
 
 <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo _("Dashboard")?>
            <small><?php echo _("Version")?> 2.0</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo _("Home")?></a></li>
            <li class="active"><?php echo _("Dashboard")?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
         
          <div class="row">
           
          </div><!-- /.row -->

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
              <!-- MAP & BOX PANE -->
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo _('Latest Visitors');?></h3> <small class="text-muted"> (<?php echo _("In Last 2 weeks around worldwide")?>)</small>
                  <div class="box-tools pull-right">
				    <a href="#">
					<?php
					 if($visitors_count!=0){
						 echo _("Total Visitors") . " (".$visitors_count.")";
					 }else{
						 echo _("Total Visitors") . " (0)";
					 }
					?>
					</a>&nbsp;&nbsp;
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-12">
					<div id="world-map-markers" style="width: 100%; height: 400px"></div>
					
					<?php 
					 
					 if($visitors!=0){
					?>
					   <script>
						$(function(){
						  $('#world-map-markers').vectorMap({
							map: 'world_mill_en',
							scaleColors: ['#C8EEFF', '#0071A4'],
							normalizeFunction: 'polynomial',
							hoverOpacity: 0.7,
							hoverColor: false,
							markerStyle: {
							  initial: {
								fill: '#F8E23B',
								stroke: '#383f47'
							  }
							},
						backgroundColor: '#eeeeee',
						series: {
						  regions: [{
							values: {
								<?php 
								 foreach($visitors as $country){
								  echo $country->country_code.":"."'#208daa'".",";
								 }
								?>
							}
						  }]
						},

						markers: [
								<?php 
								 foreach($visitors as $country){
								  echo "{latLng: [".$country->geo_latitude.",".$country->geo_longitude."], name:"."'".$country->remote_city."'"."},";
								 }
								?>
								
						 
						]
					  });
					});
						</script>
						<?php 
					 }else{
						 ?>
						 <script>
						$(function(){
						  $('#world-map-markers').vectorMap({
							map: 'world_mill_en',
							scaleColors: ['#C8EEFF', '#0071A4'],
							normalizeFunction: 'polynomial',
							hoverOpacity: 0.7,
							hoverColor: false,
							markerStyle: {
							  initial: {
								fill: '#F8E23B',
								stroke: '#383f47'
							  }
							},
						backgroundColor: '#eeeeee',
						
					  });
					});
						</script>
						 <?php
					 }
						?>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              

              <!-- TABLE: LATEST ORDERS -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo _("Latest Campaigns")?></h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th><?php echo _("C.Code")?></th>
                          <th><?php echo _("Campaign By")?></th>
						  <th><?php echo _("Country")?></th>
                          <th><?php echo _("Status")?></th>
                          <th><?php echo _("Campaign Summary")?></th>
                        </tr>
                      </thead>
					  <?php 
					  if($recent_campaigns_atadmin!=0){
					  ?>
                      <tbody>
					   <?php 
					    foreach($recent_campaigns_atadmin as $campaign){
							?>
						<tr>
                          <td><a href="#"><?php echo "#".$campaign->campaign_code?></a></td>
                          <td><?php echo $campaign->ac_first_name.' '.$campaign->ac_last_name?></td>
						  <td><a href="#"><?php echo $campaign->country_name?></a></td>
                          <td>
						  <?php 
						   switch($campaign->cmp_status){
							   case 1:
							   echo "<span class='label label-success'><i class='fa fa-check'></i> " . _("Completed") . "</span>";
							   break;
							   
							   case 0:
							   echo "<span class='label label-warning'><i class='fa fa-hourglass-half'></i> Pending</span>";
							   break;
						   }
						  ?>
						  </td>
                          <td>
							<?php 
							$success_count=$this->sms_model->get_success_count($campaign->campaign_code,$campaign->user_id);
							$participated_count=$this->sms_model->get_campaign_contacts_count($campaign->campaign_code,$campaign->user_id);
							$failed_count=$this->sms_model->get_unsuccess_count($campaign->campaign_code,$campaign->user_id);
							?>
							 
							  <div class="col-md-4">
								<div class="ad_free_btn"><?php echo _("Total")?>  <?php echo "(".$participated_count.")";?></div>
							  </div>
							 <div class="col-md-1">&nbsp;</div>
							  <div class="col-md-3">
								 <?php 
								  
								  if($success_count!=0){
									  $failed=$participated_count-$success_count;
								  }else{
									  if($failed_count!=0){
										  $failed=$failed_count;
									  }else{
										  $failed="0";
									  }
								  }
								  echo "<span class='text-success'><i class='fa fa-check-circle'></i>  ".$success_count." </span><br>";
								  ?>
								 
							  </span>
							  </div>
							  
							   <div class="col-md-3">
								  <?php echo "<span class='text-danger'><i class='fa fa-exclamation-circle'></i>  ".$failed." </span>";?>
							  </span>
							  </div>
							 
						  </td>
					    </tr>
							<?php
							
						}
					   ?>
                      </tbody>
					  <?php 
					  }else{
						  ?>
					   <tbody>
                        <tr>
						 <td colspan="5"> <i class="fa fa-info-circle"></i> <?php echo _("No campaigns founds")?></td>
						</tr>
						</tbody>
						  <?php
					  }
					  ?>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                  <a href="<?php echo base_url()?>campaigns/all" class="btn btn-sm btn-info btn-flat pull-left"><?php echo _("View All")?></a>
                  <span class="pull-right">
				  <small><b><?php echo _("Legend ")?>:</b>
				  <span class="text-success"><i class="fa fa-check-circle"></i> </span> = <?php echo _("Success")?><span class="text-danger"><i class="fa fa-exclamation-circle"></i> </span>= <?php echo _("Failed")?></small>
				  </span>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->

            <div class="col-md-4">
              <!-- Info Boxes Style 2 -->
              <div class="info-box bg-yellow">
			   
                <span class="info-box-icon"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text"><?php echo _("All Members")?></span>
                  <span class="info-box-number">
				  <?php 
				   if($all_members!=0){
					   echo count($all_members);
				   }else{
					   echo _("None");
				   }
				  ?>
				  </span>
                  <div class="progress">
                    <div class="progress-bar" style="width:0%"></div>
                  </div>
                  <span class="progress-description">
                    <?php 
					 if($all_inactive_members!=0){
					   echo _("Inactive members").count($all_inactive_members);
				   }else{
					   echo _("Inactive members - none");
				   }
					?>
					
					
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-social-usd-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text"><?php echo _("Total Payments")?></span>
                  <span class="info-box-number">
				  <?php 
				  if($total_payment_transactions!=0){
					echo $currency_name.' '.$total_payment_transactions[0]->c2o_total_amount;
				  }else{
					  echo $currency_name." 00.00";
				  }
				  ?>
				  </span>
                  <div class="progress">
                    <div class="progress-bar" style="width:0%"></div>
                  </div>
                  <span class="progress-description">
				  <?php 
				  if($total_failed_payments!=0){
					echo _("Failed payments").$currency_name.' '.$total_failed_payments[0]->c2o_total_amount;
				  }else{
					  echo $currency_name." 00.00";
				  }
				  ?>
                   
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="ion ion-ios-paperplane-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text"><?php echo _("Total Campaigns")?></span>
                  <span class="info-box-number">
				  <?php 
				   if($total_campaign_count!=0){
					   echo $total_campaign_count ;
				   }else{
					   echo "0";
				   }
				  ?>
				  </span>
                  <div class="progress">
                    <div class="progress-bar" style="width:0%"></div>
                  </div>
                  <span class="progress-description">
                    <?php 
					 if($total_pending_campaign_count!=0){
						 echo _("Pending campaigns").$total_pending_campaign_count;
					 }else{
						 echo _("Pending campaigns - none");
					 }
					?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion ion-ios-email-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text"><?php echo _("Total Messages")?></span>
                  <span class="info-box-number">
				  <?php 
				   if($total_successful_smscount!=0){
					   echo $total_successful_smscount;
				   }else{
					   echo "0";
				   }
				  ?>
				  </span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 0%"></div>
                  </div>
                  <span class="progress-description">
                   <?php 
				   if($total_unsuccessful_smscount!=0){
					   echo _("Failed messages").$total_unsuccessful_smscount;
				   }else{
					   echo _("Failed messages - none");
				   }
				  ?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->


              <!-- MEMBER LIST -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo _("Latest Members")?></h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
				    <?php 
					 if($list_latest_members!=0){
					 foreach($list_latest_members as $member){
					?>
                    <li class="item">
                      <div class="product-img">
					   <?php 
						 if($member->ac_thumbnail!=''){
						?>
							<img src="<?php echo base_url().$member->ac_thumbnail?>" alt="Thumbnail">
						<?php 
						 }else{
							if($member->ac_gender!=1){
							?>
							<img src="<?php echo base_url().'theme4.0/admin/images/avatar2.png'?>" alt="Thumbnail">

							<?php
							}else{
							?>
							<img src="<?php echo base_url().'theme4.0/admin/images/avatar5.png'?>" alt="Thumbnail">

							<?php
							}
						 }
						 ?>
                      </div>
                      <div class="product-info">
                        <a href="<?php echo base_url()?>members/details?id=<?php echo base64_encode($member->user_id)?>" class="product-title" title="<?php echo _('More Details') ?>">
						 <?php echo $member->ac_first_name.' '.$member->ac_last_name?>
						 <?php 
						  if($member->is_validated==1){
						   ?>
						   <span class="label label-success pull-right">
						    <?php echo _("activated")?>
						  </span>
							<?php						   
						  }else{
							  ?>
						  <span class="label label-warning pull-right">
						    <?php echo _("pending")?>
						  </span>
							  <?php
						  }
						 ?>
						</a>
                        <span class="product-description">
                          <?php 
						   echo _("Joined on").date('d-M-Y H:i:s',strtotime($member->ac_join_date));
						  ?>
                        </span>
                      </div>
                    </li>
					<?php 
					 }
					 }else{
					 ?>
					  <li class="item"><i class="fa fa-info-circle"></i> <?php echo _("No members found")?></li>
					 <?php
					 }
					 ?>
					 
                   
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="<?php echo base_url()?>members/all" class="uppercase"><?php echo _("View All")?></a>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->