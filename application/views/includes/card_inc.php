<?php 
 @$rsCardDetails =$this->card_model->listCardDetails();
 
 
 if(@$rsCardDetails[0]->card_number!='' && @$rsCardDetails[0]->card_holder_name!='')
 {
		 
	 $cardNumber =$rsCardDetails[0]->card_number;
	 $card_holder_name =$rsCardDetails[0]->card_holder_name;
	 $card_exp_month =$rsCardDetails[0]->card_expire_month;
	 $card_expire_year =$rsCardDetails[0]->card_expire_year;
	 $card_cvv =$rsCardDetails[0]->card_cvv;
		 
?>
	<div class="form-group">
	 <small> <?php echo _("Card Number")?></small>
	 <input type="text" class="form-control" id="payCardNumber" name="payCardNumber" placeholder="<?php echo _('E.g. 54558988XXXXXXXX') ?>" value="<?php echo $cardNumber;?>" readonly>
	 </div>
	 
	 <div class="form-group">
	 <small>
	  <?php echo _("Cardholder Name")?>
	  </small>
	  <input type="text" class="form-control" id="payCardholderName" name="payCardholderName" placeholder="<?php echo _('E.g SAIDA D') ?>" value="<?php echo $card_holder_name;?>" readonly>
	  </div>
	  
	  <div class="form-group">
		<small><?php echo _("Expired Month")?></small>
		<select id="payCardExpMonth" name="payCardExpMonth" class="form-control" readonly>
		<option value="">
		 -- <?php echo _("Month")?> --
		</option>
		<?php 
		$payMonths= array(1,2,3,4,5,6,7,8,9,10,11,12);
		 for($pM=0;$pM<count($payMonths);$pM++){
			 ?>
			 <option value="<?php echo $payMonths[$pM];?>" <?php if($card_exp_month==$payMonths[$pM]){echo _("selected");}?> ><?php echo $payMonths[$pM];?> </option>
			 <?php
		 }
		?>
		</select>
	  </div>
	  
	   <div class="form-group">
		<small><?php echo _("Expired Year")?></small>
		  <select id="payCardExpYear" name="payCardExpYear" class="form-control" readonly>
			<option value="">
			 -- <?php echo _("Year")?> --
			</option>
			<?php 
			$payYears= array(2016,2017,2018,2019,2020,2021,2022,2023,2025,2026,2027,2028,2029,2030);
			 for($pY=0;$pY<count($payYears);$pY++){
				 ?>
				 <option value="<?php echo $payYears[$pY];?>" <?php if($card_expire_year==$payYears[$pY]){echo _("selected");}?>><?php echo $payYears[$pY];?> </option>
				 <?php
			 }
			?>
		  </select>
	  </div>
	  
	  <div class="form-group">
	 <small> <?php echo _("CVV")?></small>
	 <input type="text" class="form-control" id="payCardCvv" name="payCardCvv" placeholder="" value="<?php echo $card_cvv;?>" readonly>
	    <a href="#"><small><?php echo _("What is cvv ?")?></small></a> |   <a href="#"><small><?php echo _("Update Card")?></small></a>
	 </div>
	 
<?php
 }
 else{
		 
		 ?>
	<div class="form-group">
	 <small> <?php echo _("Card Number")?></small>
	 <input type="text" class="form-control" id="payCardNumber" name="payCardNumber" placeholder="<?php echo _('E.g. 54558988XXXXXXXX') ?>" >
	 </div>
	 
	 <div class="form-group">
	 <small>
	  <?php echo _("Cardholder Name")?>
	  </small>
	  <input type="text" class="form-control" id="payCardholderName" name="payCardholderName" placeholder="<?php echo _('E.g. SAIDA D') ?>" >
	  </div>
	  
	  <div class="form-group">
		<small><?php echo _("Expired Month")?></small>
		<select id="payCardExpMonth" name="payCardExpMonth" class="form-control" >
		<option value="">
		 -- <?php echo _("Month")?> --
		</option>
		<?php 
		$payMonths= array(1,2,3,4,5,6,7,8,9,10,11,12);
		 for($pM=0;$pM<count($payMonths);$pM++){
			 ?>
			 <option value="<?php echo $payMonths[$pM];?>" ><?php echo $payMonths[$pM];?> </option>
			 <?php
		 }
		?>
		</select>
	  </div>
	  
	   <div class="form-group">
		<small><?php echo _("Expired Year")?></small>
		  <select id="payCardExpYear" name="payCardExpYear" class="form-control" >
			<option value="">
			 -- <?php echo _("Year")?> --
			</option>
			<?php 
			$payYears= array(2016,2017,2018,2019,2020,2021,2022,2023,2025,2026,2027,2028,2029,2030);
			 for($pY=0;$pY<count($payYears);$pY++){
				 ?>
				 <option value="<?php echo $payYears[$pY];?>"><?php echo $payYears[$pY];?> </option>
				 <?php
			 }
			?>
		  </select>
	  </div>
	  
	  <div class="form-group">
	 <small> <?php echo _("CVV")?></small>
	 <input type="text" class="form-control" id="payCardCvv" name="payCardCvv" placeholder="" >
	    <a href="#"><small><?php echo _("What is cvv ?")?></small></a>
	 </div>
	 
		 <?php
 }
?>