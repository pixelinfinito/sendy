<?php $this->load->view('themeFront/header.php');?>
<?php //$this->load->view('themeFront/banner_intro.php');?>
<script type="application/javascript" src="<?php echo base_url();?>js/site_login.js"></script>
<div class="main-container">
    <div class="container">
       
	   <div class="row" ng-app="">
	   <div class="col-md-5 page-content">
            
		<div class="row">
		<div class="col-sm-12">
		<div class="alert alert-info">
			<h3 class="text-dark"><?php echo _("Welcome to campaign platform")?></h3>
			<p> 
			<?php echo _("You can access our Web Messaging Platform from any computer with internet connectivity and start sending messages with your OneTextGlobal account straight away.
			To get started please enter your login details.
			")?></p><br><hr>
		
		  <h3 class="text-dark"><?php echo _("Don't have account")?></h3>
		  <p> <?php echo _("Signup here for free account and enjoy the services")?></p><br>
		  <a href="<?php echo base_url()?>site/register" class="btn btn-danger">
		  <i class="fa fa-check"></i> <?php echo _("Sign Up !")?></a>
		  <br><br><br><br>
		</div>
		</div>
		</div>
		
	   </div>
	   
	    <div class="col-md-7 reg-sidebar">
          <div class="reg-sidebar-inner text-center">
		     <div class="promo-text-box"> 
              <h3 class="text-success"> <?php echo _("Demo Account")?></h3>
			  <form role="form" class="form-horizontal" ng-controller="SiteAuthController" id="loginForm" name="loginForm">
            	<div class="form-group text-left">
                      <label class="col-md-4 control-label" ><?php echo _("Username")?><small class="text-danger">*</small></label>
                      <div class="col-md-6">
                          <input  name="login_username" id="login_username" placeholder="<?php echo _('E.g. abc@yourmail.com') ?>" ng-model="login_username" class="form-control input-md" required="required" type="email">
                      </div>
					  <span id="validEmail"></span>
                 </div>
				 <div class="form-group text-left">
                      <label class="col-md-4 control-label" ><?php echo _("Password")?><small class="text-danger">*</small></label>
                      <div class="col-md-6">
                        <input type="password" ng-model="login_password" class="form-control" placeholder="**********" name="login_password" id="login_password" required="required">
                      </div>
                 </div>
				 <div class="form-group">
				  <label  class="col-md-4 control-label"></label>
				  <div class="col-md-6">
					
					<div style="clear:both"></div>
					 <button type="submit" class="btn btn-primary btn-block btn-flat btn-border" ng-click='SignIn()'>
					 <i class="fa  fa-unlock-alt"></i> <?php echo _("Sign In")?></button>
					</div>
				</div>
				<div class="form-group text-left">
					<label  class="col-md-4 control-label"></label>	
					<div class="col-md-6">				  
						<div id="vPrompt" style="display:none">
						<span class="text-danger"><?php echo "<i class='fa fa-exclamation-triangle'></i> Username & Password cannot be empty..!"?> </span>
						</div>	
					</div>
				</div>
				
				 <div class="form-group text-left">
				  <label  class="col-md-4 control-label"></label>
				    <div class="col-md-6">
					<div style="clear:both"></div>
					 <?php echo _("Username : demo@gmail.com")?><br>
					 <?php echo _("Password : demo123")?></div>
				</div>
	          </form>
            </div>
	      </div>
        </div>
		
	   </div>
	 </div>
  
<br><hr>	
<?php $this->load->view('themeFront/footer_login');?>



