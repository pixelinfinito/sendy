

<script src="<?php echo base_url()?>theme4.0/client/js/jquery-2.1.1.js"></script> 
<script src= "<?php echo base_url();?>theme4.0/client/js/angular.min.1.2.26.js"></script>
<script src="<?php echo base_url()?>theme4.0/client/js/ui-bootstrap-tpls-0.10.0.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/client/css/bootstrapValidator.css"/>
<script type="text/javascript" src="<?php echo base_url()?>theme4.0/client/js/bootstrapValidator.js"></script>
<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap-submenu.min.css" rel="stylesheet">

<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url()?>theme4.0/client/css/animate.min.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>theme4.0/client/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>theme4.0/client/css/demo.css" rel="stylesheet" />

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url()?>theme4.0/client/css/pe-icon-7-stroke.css" rel="stylesheet" />
	
<script src="<?php echo base_url()?>theme4.0/client/js/bootstrap.min.js"></script> 

<script type="application/javascript" src="<?php echo base_url();?>js/site_login.js"></script>
<div class="main-container">
    <div class="container">
       <br><br>
	   <div class="row" ng-app="">
	   <div class="col-md-3">&nbsp;</div>
	    <div class="col-md-6 alert-default">
		  <div class="row">
		  <div class="col-md-6">
		  <img src="<?php echo base_url()?>theme4.0/client/images/logo_black.png" width="120" height="50">
		  </div>
		  <div class="col-md-6">
		  <h3 class="text-info"> <?php echo _("Forgot Password")?></h3>
		  </div>
		  </div>
		  <hr>
          <div class="reg-sidebar-inner text-center">
		     <div class="promo-text-box"> 
              <form role="form" class="form-horizontal" ng-controller="ForgotController" id="forgotForm" name="forgotForm">
            	<div class="form-group">
                      <label class="col-md-3 control-label  text-left"><?php echo _("Email ID")?><small class="text-danger">*</small></label>
                      <div class="col-md-8  text-left">
                          <input  name="login_email" id="login_email" placeholder="<?php echo _('E.g. abc@domain.com') ?>" ng-model="login_email" class="form-control input-md" required="required" type="email">
                      </div>
					  <span id="validEmail"></span>
                 </div>
				
				 <div class="form-group">
				  <label  class="col-md-3 control-label"></label>
				  <div class="col-md-8">
					
					<div style="clear:both"></div>
					 <button type="submit" class="btn btn-primary btn-block btn-flat btn-border" ng-click='SubmitIn()'>
					 <i class="fa  fa-key"></i> <?php echo _("Submit")?></button>
					</div>
				</div>
				<div class="form-group text-left">
					<label  class="col-md-3 control-label"></label>	
					<div class="col-md-8">				  
						<div id="vPrompt" style="display:none">
						<span class="text-danger"><?php echo "<i class='fa fa-exclamation-triangle'></i>Email ID cannot be empty..!"?> </span>
						</div>	
					</div>
				</div>
				
				 <div class="form-group text-left">
				  <label  class="col-md-3 control-label"></label>
				    <div class="col-md-8">
					<div style="clear:both"></div>
					  <a href="<?php echo base_url()?>site/login" class="text-muted"><i class="fa fa-laptop"></i> <?php echo _("Account Login")?></a>
					</div>
				</div>
	          </form>
            </div>
	      </div>
        </div>
		<div class="col-md-3">&nbsp;</div>
	   </div>
	 </div>

