<?php $this->load->view('themeFront/header_logged.php');?>
<style>
.explink{border:1px solid #ddd;height:35px;}.expheight{height:35px;margin-left:2px}
</style>
<script type="text/javascript">
 function printInvoice(pid){
    var URL = "<?php echo base_url()?>site/export/payment_invoice?iv="+pid;
    var W = window.open(URL);   
    W.window.print(); 
 }
</script>

<link href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />   
<script type="application/javascript" src="<?php echo base_url();?>js/user_payments.js"></script>

<?php
$remoteCountry=$this->config->item('remote_country_code');
$remoteCity=$this->config->item('remote_geo_city');
if($remoteCity!=''){
	$geoCity=$remoteCity;
	$geolatitude=$this->config->item('remote_geo_latitude');
	$geolongitude=$this->config->item('remote_geo_longitude');
}else{
	$geoCity='';
	$geolatitude='';
	$geolongitude='';
}
$currencyInfo= $this->countries_model->get_currencies_cCode($remoteCountry);
$user_id = $this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);
$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);
 
$paymentSettings=$this->app_settings_model->get_payment_settings();
$settingsInfo= $this->app_settings_model->get_site_settings();
?>

<div class="main-container">
<div class="container">
     <div class="row">
      <div class="col-md-6">
      <ol class="breadcrumb pull-left">
        <li><a href="<?php echo base_url()?>site/home"><i class="icon-home fa"></i></a></li>
        <li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
		<li><a href="<?php echo base_url()?>site/wallet/add"><?php echo _("Payment")?></a></li>
		<li><a href="<?php echo base_url()?>site/payment/history"><?php echo _("History")?></a></li>
        
      </ol>
     </div>
	 <div class="col-md-6 text-right">
	  <?php 
	   /// custom library
	   $intlib=$this->internal_settings->local_settings();
	   echo '<i class="fa fa-question-circle"></i> '.$intlib[0]->help_text;
	  ?>
	 </div>
	</div>
</div>
<hr>
   <div class="row">
	<div class="container top-shadow">
	<div class="col-md-6 text-left"> 
		<?php 
		$paymentInfo= $this->payment_model->get_member_payments($user_id);
		$appSettings= $this->app_settings_model->get_primary_settings();
		$appCountry=$appSettings[0]->app_country;
		$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
		$tPayment=array();
		if($paymentInfo!='0'){
			foreach(@$paymentInfo as $payment){
				array_push($tPayment,$payment->c2o_total_amount);
			}
		}
		echo "<b>" . _('Total Transaction amount') . "</b><br>";
		if(@$paymentInfo[0]->acc_item_number!=''){
		   ?>
		   <h4 class="text-success">
		   <?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.array_sum(@$tPayment);?>
		   </h4>
		   <?php
		}else{
			?>
			<h4 class="text-danger">
			<?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name." (0)";?>
			</h4>
		   <?php
		}
		?>
	</div>
	<div class="col-md-6 text-right">
	<div class="row">
	  <div class="col-md-4">
	 <div class="hdata">
	  <div class="mcol-left">
		<i class="fa  fa-dollar"></i>
	  </div>
	  <div class="mcol-right">
		<a href="<?php echo base_url()?>site/wallet/add"><?php echo _("Add Wallet Funds")?></a>
	  </div>
	 </div>
	 </div>
	
	<div class="col-md-4">
	 <div class="hdata">
	  <div class="mcol-left">
		<i class="fa  fa-credit-card"></i>
	  </div>
	  <div class="mcol-right">
		<a href="<?php echo base_url()?>site/payment/card"><?php echo _("Manage Credit/Debit Card")?></a>
	  </div>
	 </div>
	 </div>
	 
	 <div class="col-md-4">
	 <div class="hdata">
	  <div class="mcol-left">
		<i class="fa  fa-history"></i>
	  </div>
	  <div class="mcol-right">
		<a href="<?php echo base_url()?>site/wallet/history"><?php echo _("Wallet History")?></a>
	  </div>
	 </div>
	 </div>
	 </div>
	</div>
	</div>
</div>

  <div class="container">
    <div class="row">
	   <div class="col-md-6">
	   <h4><i class="fa fa-money"></i>&nbsp; <?php echo _("Payment History")?></h4>
	   </div>
	   <div class="col-md-6 text-right">
			<a href="<?php echo base_url()?>site/export/export_payment_history" class="btn btn-sm explink"  title="<?php echo _('Export To Excel') ?>">
			 <i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-sm explink" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			  <i class="fa fa-print"></i>
			</a>
	   </div>
	</div> 
	<table class="table table-bordered table-striped" id="dataTables-paymentHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Ref.No")?></th>
		<th class="col-sm-1"><?php echo _("Amount")?></th>
		<th class="col-sm-1"><?php echo _("Transaction ID")?></th>
		<th class="col-sm-1"><?php echo _("Response Code")?></th>
		<th class="col-sm-1"><?php echo _("Payment Datetime")?></th>
		<th class="col-sm-1"><?php echo _("Payment Status")?></th>
		<th class="col-sm-1"><?php echo _("Invoice")?></th>
		<!--<th class="col-sm-1"><?php echo _("Actions")?></th>-->

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$gp = 1 
		?>
		<?php if ($paymentInfo!=0): foreach ($paymentInfo as $payment) : ?>

		<tr>
		<td>
		<?php echo $payment->acc_payment_id?>
		</td>
		
		<td>
		<?php echo $payment->acc_item_number?>
		</td>
		<td>
		<?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.$payment->c2o_total_amount?> 
		</td>
		
		<td>
		  <?php echo $payment->c2o_transaction_id ?>
		</td>
		<td>
		  <?php echo $payment->c2o_response_code ?>
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($payment->payment_datetime)); 
		?>
		</td>
		
		<td>
		     <?php 
			  switch($payment->payment_status){
				  case '1':
				  ?>
				  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i><?php echo _("Accepted")?></div>
				  <?php
				  break;
				  
				  case '0':
				  ?>
				  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i> <?php echo _("Failed")?></div>
				  <?php
				  break;
			  }
			 ?>
             
         </td>
		 <td>
		     <?php 
			  switch($payment->payment_status){
				  case '1':
				  ?>
				  <a href="<?php echo base_url()?>site/export/payment_invoice?iv=<?php echo base64_encode($payment->acc_payment_id)?>" title="<?php echo _('Download Invoice') ?>"> <i class="fa fa-download"></i></a>
				  <a href="javascript:void(0)" onclick="javascript:printInvoice('<?php echo base64_encode($payment->acc_payment_id)?>')" title="<?php echo _('Print Invoice') ?>"> <i class="fa fa-print"></i></a>
				  <?php
				  break;
				  
				  case '0':
				  echo '-';
				  break;
			  }
			 ?>
             
         </td>
		 
			
		<!--<td>
			<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" onclick="delAccPayment('<?php echo $payment->acc_payment_id?>')" class="text-danger">
			<i class='glyphicon glyphicon-trash'></i> 
			</a>
		</td>-->

		</tr>
	<?php
	$gp++;
	endforeach;
	?>
	<?php else : ?>
	<td colspan="6" class="text-center">
	<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
	</td>
	<?php endif; ?>
	</tbody><!-- / Table body -->
	</table> <!-- / Table -->
				
	<?php 
	// $this->load->view('site/modals/campaign/sms/delete_group');
	?>
		
	<br><br>
	<script src="<?php echo base_url(); ?>assets/js/dataTables/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/dataTables/dataTables.tableTools.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/dataTables/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script>
    $(document).ready(function() {
		$('#dataTables-paymentHistory').DataTable( {
			"order": [[ 0, "desc" ]]
		});
		
    });
    </script>	
</div>
</div>

<?php $this->load->view('themeFront/footer_classic.php');?>



