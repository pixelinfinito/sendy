<?php $this->load->view('themeFront/header_logged.php');
//$this->load->view('themeFront/banner_intro.php');
?>
<script type="application/javascript" src="<?php echo base_url();?>js/card_information/card.js"></script>

<?php

$remoteCountry=$this->config->item('remote_country_code');
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
$rsCountry = $this->countries_model->list_countries();

$user_id = $this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);
$paymentSettings=$this->app_settings_model->get_payment_settings();
$settingsInfo= $this->app_settings_model->get_site_settings();
$walletInfo= $this->account_model->get_account_wallet($user_id);

$card_info=$this->payment_model->get_card_information($user_id);
if($card_info!=0){
	
	 $mcard_name=$card_info[0]->card_holder_name;
	 $mcard_number=$card_info[0]->card_number;
	 $mcard_cvv=$card_info[0]->card_cvv;
	 $mcard_month=$card_info[0]->card_expire_month;
	 $mcard_year=$card_info[0]->card_expire_year;
	 $mcard_add1=$card_info[0]->billing_street1;
	 $mcard_add2=$card_info[0]->billing_street2;
	 $mcard_city=$card_info[0]->billing_city;
	 $mcard_country=$card_info[0]->billing_country;
	 $mcard_postal=$card_info[0]->billing_postalcode;
	 

} else{
	
	 $mcard_name='';
	 $mcard_number='';
	 $mcard_cvv='';
	 $mcard_month='';
	 $mcard_year='';
	 $mcard_add1='';
	 $mcard_add2='';
	 $mcard_city='';
	 $mcard_country='';
	 $mcard_postal='';
	 
}
 
?>
<div class="main-container">
    <div class="container">
	 <div class="row">
      <div class="col-md-6">
      <ol class="breadcrumb pull-left">
        <li><a href="<?php echo base_url()?>site/home"><i class="icon-home fa"></i></a></li>
        <li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
		<li><a href="<?php echo base_url()?>site/wallet/add"><?php echo _("Payment")?></a></li>
        <li><a href="<?php echo base_url()?>site/payment/card"><?php echo _("Card Information")?></a></li>
      </ol>
     </div>
	 <div class="col-md-6 text-right">
	  <?php 
	   /// custom library
	   $intlib=$this->internal_settings->local_settings();
	   echo '<i class="fa fa-question-circle"></i> '.$intlib[0]->help_text;
	  ?>
	 </div>
	</div>
	   <div class="row" ng-app="">
	   
	   <div class="col-md-4 reg-sidebar">
		<?php $this->load->view('themeFront/member_sidebar_horizental');?>
          <div class="reg-sidebar-inner ">
		    
			<div class="panel sidebar-panel"> 
			   <div class="panel-heading uppercase text-left">
					<span class="fa  fa-dollar"></span> <?php echo _("Available Credit")?></div>
			   
			   <div class="panel-body text-left">
               <div class="row">
              <div class="col-md-3 text-center">
			    <br><h1><i class="fa fa-google-wallet"></i></h1>
			  </div>
			  <div class="col-md-9 text-left">
			    <?php 
				echo "<small>".@$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.'</small><br>';
				if(@$walletInfo[0]->balance_amount!=''){
				?>
					<h1>
					<?php echo @$walletInfo[0]->balance_amount;?>
					</h1>
				<?php
				}else{
				?>
					<h1>
					<?php echo "0.00";?>
					</h1>
				<?php
				}
				?>
				<?php echo _("Check your")?><a href="<?php echo base_url()?>site/wallet/history"><?php echo _("transaction history")?></a>
			  </div>
              </div>
				<br>
			   </div>
		    </div>
			
			<div class="col-md-12 btn btn-success">
			<a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>"> 
			 <i class="fa fa-send-o"></i> <?php echo _("Start Campaign")?></a>
			</div>   
		  </div>
        </div>
		
	   <div class="col-md-8 panel">
          <div class="">
		   <div class="panel-success2-heading">
           <h4 class="panel-title">
		   <b><i class="fa fa-credit-card"></i>&nbsp; <?php echo _("Update Card Information")?></b>
		   </h4>
		   </div>
		<div class="inner-box">
           <br>
		   <form id="card_form" name="card_form" method="post" class="form-horizontal" ng-controller="McardController">
			<div class="form-group">
                <label class="col-md-4 control-label"><?php echo _("Card Holder Name")?><small class="red">*</small></label>
		         <div class="col-md-6">
                <input id="c_holder_name" name="c_holder_name" type="text" class="form-control" placeholder="<?php echo _('e.g. FNAME LNAME') ?>" value="<?php echo $mcard_name?>" required />
				</div>
            </div>
			
			<div class="form-group">
                <label class="col-md-4 control-label" ><?php echo _("Card Number")?><small class="red">*</small></label>
		         <div class="col-md-6">
                <input id="c_number" name="c_number" type="number" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100"  size="20" autocomplete="off" required class="form-control" placeholder="<?php echo _('e.g. 4XXX9XXX2XXX9999') ?>" value="<?php echo $mcard_number?>" required />
				<small class="text-muted">(<?php echo _("Any Credit/Debit Card")?>)</small>
				</div>
            </div>
            <div class="form-group">
			 <label class="col-md-4 control-label" ><?php echo _("Expiration Date (MM/YYYY")?>)<small class="red">*</small></label>
                <div class="col-md-6">
				<div class="col-md-6 no-padding">
				  <select class="form-control" id="c_month" name="c_month" required>
				   <?php 
				    for($i=1;$i<=12;$i++){
						if($i<10){$v='0'.$i;}else{$v=$i;}
						?>
						<option value="<?php echo $v;?>" <?php if($v==$mcard_month){echo _("selected");}?>>
						<?php echo $v;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				</div>
				
				<div class="col-md-6 no-padding">
				 <select class="form-control" id="c_year" name="c_year" required>
				   <?php 
				    for($j=2016;$j<=2030;$j++){
						
						?>
						<option value="<?php echo $j;?>" <?php if($j==$mcard_year){echo _("selected");}?>>
						<?php echo $j;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				</div>
				
				</div>
            </div>
			
            <div class="form-group">
                <label class="col-md-4 control-label" ><?php echo _("CVV Code")?><small class="red">*</small></label>
                <div class="col-md-6">
                <input id="c_cvv" name="c_cvv" size="4" type="number" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100"  autocomplete="off" required class="form-control" placeholder="<?php echo _('e.g. 123') ?>" value="<?php echo $mcard_cvv?>"  />
				<a href="#" data-toggle="modal" data-target="#modalCvv"><small><?php echo _("What is CVV ?")?></small></a>
				</div>
            </div>
			
			
			<div class="form-group">
                <h4 class="col-md-4 control-label text-info"><?php echo _("Billing Address")?></h4>
		         <div class="col-md-6 text-muted">
                  <?php echo _("Please enter the proper billing address otherwise your card may chance to decline.")?></div>
            </div>
			
			<div class="form-group">
                <label class="col-md-4 control-label"><?php echo _("Address 1")?><small class="red">*</small></label>
		         <div class="col-md-6">
                <input id="c_address1" name="c_address1" type="text" class="form-control" placeholder="<?php echo _('e.g. #10, robert street') ?>" required value="<?php echo $mcard_add1?>" />
				</div>
            </div>
			
			<div class="form-group">
                <label class="col-md-4 control-label"><?php echo _("Address 2")?></label>
		         <div class="col-md-6">
                <input id="c_address2" name="c_address2" type="text" class="form-control" placeholder="<?php echo _('e.g. ngo colony, ca ') ?>" required value="<?php echo $mcard_add2?>"/>
				</div>
            </div>
			
			<div class="form-group">
                <label class="col-md-4 control-label"><?php echo _("City")?><small class="red">*</small></label>
		         <div class="col-md-6">
                <input id="c_city" name="c_city" type="text" class="form-control" placeholder="<?php echo _('e.g. london') ?>" required value="<?php echo $mcard_city?>" />
				</div>
            </div>
			
			<div class="form-group">
			 <label class="col-md-4 control-label"><?php echo _("Country")?><small class="red">*</small></label>
			  <div class="col-md-6">
			 <?php $remoteCountry=$this->config->item('remote_country_code');?>
			   <select class="form-control"  id="c_country" name="c_country" >
			   <option value="">--<?php echo _("Select option")?>--</option>
				 <?php 
				 
				  if(count($rsCountry)>0){
					  for($rC=0;$rC<count($rsCountry);$rC++){
					  ?>
					  <option value="<?php echo $rsCountry[$rC]->country_code;?>" <?php if($rsCountry[$rC]->country_code==$mcard_country){echo 'selected';}?>><?php echo $rsCountry[$rC]->country_name;?></option>
					  <?php
					  }
				  }
				 ?>
			   </select>
			   </div>
			</div>
			
			<div class="form-group">
                <label class="col-md-4 control-label"><?php echo _("Postal Code")?><small class="red">*</small></label>
		         <div class="col-md-6">
                <input id="c_postal_code" name="c_postal_code" type="text" class="form-control" placeholder="<?php echo _('e.g. BBxxxxxx') ?>" required value="<?php echo $mcard_postal?>"/>
				</div>
            </div>
			
			 
			 <div  class="form-group">
                <label class="col-md-4 control-label" ></label>
				<div class="col-md-6">
				  <?php 
				  if($user_id!='1000578dd5df29bdd'){
				  ?>
				   <button type="button" class="btn btn-primary btn-border" ng-click="cardIn()">
				    <i class="fa fa-pencil"></i> <?php echo _("Update")?></button>
				   
				   <a href="<?php echo base_url()?>site/wallet/add" class="btn btn-success btn-border">
				    <i class="fa fa-money"></i> <?php echo _("Add Funds")?></a>
				  <?php 
				  }else{
					  echo "<h5 class='text-danger'>" . _('Demo Account (Restricted Feature)') . "</h5>";
				  }
				  ?>
				  
			    </div>
			 </div>
			 
			  <div  class="form-group">
                <label class="col-md-4 control-label">&nbsp;</label>
				<div class="col-md-6">
                  <div id="rsDiv"></div>
				  
			    </div>
			 </div>
			</form>
	      </div>
	    </div>
	   </div>
	  </div>
	 </div>
 </div>
 
 
  <!-- /////////////// CVV Alert ///////////////-->
 
 <!-- Modal -->
<div class="modal fade" id="modalCvv" tabindex="-1" role="dialog" 
     aria-labelledby="myCvvLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myCvvLabel">
                   <?php echo _("What is CVV")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
               <img src="<?php echo base_url()?>theme4.0/client/images/payment/cvv.jpg" alt="CVV">
		    </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <small><?php echo _("Please proceed with valid card")?></small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('site/modals/campaign/sms/upgrade_account');?>     
<?php $this->load->view('themeFront/footer.php');?>



