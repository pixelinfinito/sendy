<?php $this->load->view('themeFront/header.php');
$account_classes= $this->account_model->list_account_classes();
$countries= $this->countries_model->list_countries();

?>
<script type="application/javascript" src="<?php echo base_url();?>js/site_register.js"></script>
<div class="main-container">
    <div class="container" ng-app="">
       
	   <div class="row">
	   <div class="col-md-7 page-content">
          <div class="reg-sidebar-inner text-center">
           
          <div class="promo-text-box" >
            <h3 class="text-success"> <?php echo _("It's quick & free to get started")?></h3><br>
            <form class="form-horizontal" ng-controller="SiteRegisterController" role="form" name="siteRegisterForm" id="siteRegisterForm" > 
                  <fieldset>
                    <div class="form-group text-left">
                      <label class="col-md-4 control-label" ><?php echo _("Account Type")?><small class="text-danger">*</small></label>
                      <div class="col-md-6 text-left">
                        <select class="form-control"  name="ac_class" id="ac_class">
						 <option value="">--<?php echo _("Select option")?>--</option>
						 <?php
						  for($ac=0;$ac<count($account_classes);$ac++){
							  ?>
							  <option value="<?php echo $account_classes[$ac]->acc_id;?>"><?php echo $account_classes[$ac]->acc_type;?></option>
							  <?php
						  }
						 ?>
						</select>
					  </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group text-left">
                      <label class="col-md-4 control-label" ><?php echo _("First Name")?><small class="text-danger">*</small></label>
                      <div class="col-md-6">
                        <input  name="ac_first_name" id="ac_first_name" placeholder="<?php echo _('First Name') ?>" class="form-control input-md" required type="text">
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group text-left">
                      <label class="col-md-4 control-label" ><?php echo _("Last Name")?><small class="text-danger">*</small></label>
                      <div class="col-md-6">
                        <input  name="ac_last_name" id="ac_last_name" placeholder="<?php echo _('Last Name') ?>" class="form-control input-md" required type="text">
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group text-left">
                      <label class="col-md-4 control-label" ><?php echo _("Mobile Number")?><small class="text-danger">*</small></label>
                      <div class="col-md-6">
                        <input  name="ac_phone" id="ac_phone" placeholder="<?php echo _('e.g. +673xxxxxx') ?>" class="form-control input-md" type="text" required>
                       
                      </div>
                    </div>
                    
					 <div class="form-group text-left">
                      <label class="col-md-4 control-label" ><?php echo _("Country")?><small class="text-danger">*</small></label>
                      <div class="col-md-6">
					  <?php $remoteCountry=$this->config->item('remote_country_code');?>
                        <select class="form-control"  name="ac_country" id="ac_country">
						 <option value="">--<?php echo _("Select Country")?>--</option>
						 <?php
						  for($c=0;$c<count($countries);$c++){
							  ?>
							  <option value="<?php echo $countries[$c]->country_code;?>" <?php if($countries[$c]->country_code==@$remoteCountry){echo 'selected';}?>>
							  <?php echo $countries[$c]->country_name;?>
							  </option>
							  <?php
						  }
						 ?>
						</select>
						
                      </div>
                    </div>
					
                    <!-- Multiple Radios -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" ><?php echo _("Gender")?><span>&nbsp;</span> </label>
                      <div class="col-md-6 text-left">
                            <input name="ac_gender" id="ac_gender1" value="1" checked="checked" type="radio">
                            <?php echo _("Male")?>&nbsp;&nbsp;
                            <input name="ac_gender" id="ac_gender2" value="2" type="radio">
                            <?php echo _("Female")?></div>
                    
                    </div>
                    
                    <!-- Textarea -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textarea"><?php echo _("About Person/Company")?><span>&nbsp;</span></label>
                      <div class="col-md-6">
                        <textarea class="form-control" id="ac_about" name="ac_about" placeholder="<?php echo _('Share some comments about you (or) your company') ?>"></textarea>
                      </div>
                    </div>
                    <div class="form-group text-left">
                      <label for="inputEmail3" class="col-md-4 control-label"><?php echo _("Email")?><small class="text-danger">*</small></label>
                      <div class="col-md-6">
                        <input type="email" class="form-control" id="ac_email" name="ac_email" placeholder="<?php echo _('e.g. name@domain.com') ?>">
                      </div>
                    </div>
                    <div class="form-group text-left">
                      <label for="inputPassword3" class="col-md-4 control-label"><?php echo _("Password")?><small class="text-danger">*</small></label>
                      <div class="col-md-6">
                        <input type="password" class="form-control" name="ac_password" id="ac_password" placeholder="*******" required>
                        
                      </div>
                    </div>
                    <div class="form-group text-left">
                      <label  class="col-md-4 control-label"></label>
                      <div class="col-md-8">
                        <div class="termbox mb10">
                          <label class="checkbox-inline" for="checkboxes-1">
                            <input name="reg_terms" id="reg_terms" value="1" type="checkbox" required>
                            <?php echo _("I have read and agree to the")?><a href="terms-conditions.html"><?php echo _("Terms & Conditions")?></a> </label>
                        </div>
                       </div>
                    </div>
					<div class="form-group text-left">
					 <label  class="col-md-4 control-label"></label>
					<div class="col-md-8">
					  <button class="btn btn-primary" type="button" ng-click='RegisterUser()'>
					  <i class="fa fa-check"></i> <?php echo _("Register")?></button>
					    <div id="rsDiv"></div>
					 </div>
					</div>
                  </fieldset>
                </form>
          </div>
         <br><br>
       
	   </div>
	   </div>
	   
	    <div class="col-md-5 reg-sidebar">
         <div class="row">
		<div class="col-sm-12">
		<div class="alert alert-success">
			<h3 class="text-dark"><?php echo _("Get a free account")?></h3>
			<p> 
			 <?php echo _("One idea may changes your business style, one campaign may touch customer's feels. Why you waiting for get a free account today and enjoy
			 our services, we here help your business.
			")?></p><br><hr>
		
		  <h3 class="text-dark"><?php echo _("Already have a account")?></h3>
		  <p> <?php echo _("Please validate your credentials here.")?></p><br>
		  <a href="<?php echo base_url()?>site/login" class="btn btn-danger">
		  <i class="fa  fa-unlock-alt"></i> <?php echo _("Sign In")?></a>
		  <br><br><br><br>
		</div>
		
		</div>
		</div>
		
        </div>
		
	   </div>
	 </div>
  
<br><hr>
<?php $this->load->view('themeFront/footer_login.php');?>



