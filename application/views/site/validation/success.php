<?php $this->load->view('themeFront/header.php');?>
<?php //$this->load->view('themeFront/banner_intro.php');?>

<div class="main-container">
    <div class="container">
       
	   <div class="row">
	   <div class="col-md-12 page-content">
          <div class="inner-box category-content">
            <h2 class="title-2"> <i class="fa fa-exchange"></i> <?php echo _("Activation")?></h2>
            <div class="row">
              <div class="col-sm-12">
          
            <div class="panel-body">
              
                <h1>
				  <span class="fa fa-check-circle"></span> 
				 <?php echo _("Account activation success,")?><a href="<?php echo base_url()?>site/login"><?php echo _("click here")?></a> <?php echo _("to login.")?></h1>
				 
				
                 
            </div>
            <div class="panel-footer">
                 
                 <?php echo _("Thank you for validating your account.")?></div>
			<br><br>
          
        </div>
	   </div>
	   </div>
	   </div>
	   
	   
		
	   </div>
	 </div>
  </div>

<?php $this->load->view('themeFront/footer.php');?>



