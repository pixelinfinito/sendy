<?php $this->load->view('themeFront/header_logged.php');?>
<script type="application/javascript" src="<?php echo base_url();?>js/profile.js"></script>
<?php 
$user_id=$this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);
$rsCountry = $this->countries_model->list_countries();
$remoteCountry=$this->config->item('remote_country_code');
$currencyInfo= $this->countries_model->get_currencies_cCode($remoteCountry);

$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);
$settingsInfo= $this->app_settings_model->get_site_settings();

?>
<div class="main-container">
    <div class="container">
      <div class="row">
	  <div class="col-md-12">
	  <div class="col-md-6">
	  <ol class="breadcrumb pull-left">
		<li><a href="<?php echo base_url()?>site/home"><i class="icon-home fa"></i></a></li>
		<li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
		<li><a href="<?php echo base_url()?>site/profile"><?php echo _("Profile")?></a></li>
		<li><a href="<?php echo base_url()?>site/profile"><?php echo _("Update")?></a></li>
	  </ol>
	 </div>
	 <div class="col-md-6 text-right">
	 <?php 
	   /// custom library
	   $intlib=$this->internal_settings->local_settings();
	   echo '<i class="fa fa-question-circle"></i> '.$intlib[0]->help_text;
	  ?>
	  
	 </div>
	 </div>
	
	 <div class="col-md-12">
	
	     <!--///////// Col 3 /////////-->
	     <?php $this->load->view('themeFront/profile_sidebar.php');?>
		 
		 <div class="col-sm-8">
        	<div id="accordion" class="panel-group">
              <div class="panel panel-info2">
                <div class="panel-info2-heading">
				  <div class="row">
				  <div class="col-sm-6">
                  <h4 class="panel-title"> <a href="#collapseB1"  data-toggle="collapse"> <b><?php echo _("Profile Information")?></b> </a> </h4>
				  </div>
				  <div class="col-sm-6 text-right">
				   <i class="fa fa-calendar-check-o"></i> 
				   <?php 
				    $lastLogin=@$this->session->userdata['site_login']['last_login'];
				     echo _("Your last login :");
				   
					  if($lastLogin!='0000-00-00 00:00:00'){
						echo date('d-M-Y H:i:s',strtotime($lastLogin));
					  }else{
						  echo "<span class='text-muted'> " . _('data not available') . "</span>";
					  }
				   ?>
				  </div>
				  </div>
                </div>
                <div class="panel-collapse collapse in" id="collapseB1">
                  <div class="panel-info2-body">
					<?php 
					@$opt=base64_decode($this->input->get('token'));
					switch(@$opt){
					case 'profile_success':
					echo "<br><br><div class='text-center text-success'><span class='fa fa-check-circle'></span> " . _('Profile information updated successfully.') . "</div>";
					break;

					case 'profile_failed':
					echo "<br><br><div class='text-center text-danger'><span class='fa fa-times-circle'></span> " . _('Failed in update profile information.') . "</div>";
					break;
					
					case 'password_success':
					echo "<br><br><div class='text-center text-success'><span class='fa fa-check-circle'></span> " . _('Password updated successfully.') . "</div>";
					break;
					
					case 'password_failed':
					echo "<br><br><div class='text-center text-danger'><span class='fa fa-times-circle'></span> " . _('Failed in update password,please try again.') . "</div>";
					break;
					
					case 'preferences_success':
					echo "<br><br><div class='text-center text-success'><span class='fa fa-check-circle'></span> " . _('Preferences updated successfully.') . "</div>";
					break;
					
					case 'preferences_failed':
					echo "<br><br><div class='text-center text-danger'><span class='fa fa-times-circle'></span> " . _('Failed in update preferences,please try again.') . "</div>";
					break;
					}
					?>
					<br><br>
                    <form class="form-horizontal" role="form" action="<?php echo base_url()?>site/profile/update" method="post" ng-controller="profileBasicInfoCtrl" id="profileBasicInfoForm" name="profileBasicInfoForm">
					 
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("First Name")?><small class="red">*</small></label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="first_name" id="first_name" placeholder="<?php echo _('Jhon') ?>" value="<?php echo @$userInfo[0]->ac_first_name;?>" required="required">
                        </div>
                      </div>
					
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Last name")?><small class="red">*</small></label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="last_name" id="last_name" placeholder="<?php echo _('Doe') ?>" value="<?php echo @$userInfo[0]->ac_last_name;?>" required="required">
                        </div>
                      </div>
					  <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Gender")?><small class="red">*</small></label>
                        <div class="col-sm-8">
                          <select class="form-control" id="gender" name="gender" required="required">
						   <option value="1" <?php if(@$userInfo[0]->ac_gender=='1'){echo _("checked");}?>>
						    <?php echo _("Male")?></option>
						   <option value="0" <?php if(@$userInfo[0]->ac_gender=='0'){echo _("checked");}?>>
						    <?php echo _("Female")?></option>
						   <option value="2" <?php if(@$userInfo[0]->ac_gender=='2'){echo _("checked");}?>>
						    <?php echo _("Other")?></option>
						   
						  </select>
                        </div>
                      </div>
					  
					  
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Email ID")?><small class="red">*</small></label>
                        <div class="col-sm-8">
                          <input type="email" class="form-control" name="email_id" id="email_id" placeholder="<?php echo _('jhon.deo@example.com') ?>" value="<?php echo @$userInfo[0]->ac_email;?>" readonly required="required">
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Address Line1")?><small class="red">*</small></label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control"  name="address1" id="address1" placeholder="<?php echo _('e.g. #34,robert street') ?>" value="<?php echo @$userInfo[0]->ac_address1;?>" required="required">
                        </div>
                      </div>
					   <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Address Line2")?></label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control"  name="address2" id="address2" placeholder="<?php echo _('e.g. ngo colony') ?>" value="<?php echo @$userInfo[0]->ac_address2;?>">
                        </div>
                      </div>
					   <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("City")?><small class="red">*</small></label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control"  name="city" id="city" placeholder="<?php echo _('e.g london') ?>" value="<?php echo @$userInfo[0]->ac_city;?>" required="required">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="Phone" class="col-sm-3 control-label"><?php echo _("Mobile Number")?><small class="red">*</small></label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="phone" name="phone" placeholder="+44xxxxxx" value="<?php echo @$userInfo[0]->ac_phone;?>" required="required">
						 <?php 
						 if($userInfo[0]->mobile_validate!='1'){
						?>
						<a href="#" data-toggle="modal" data-target="#MobileValidateModal"><i class="fa fa-mobile"></i> <?php echo _("Verify Mobile")?></a>
						<?php
						 }else{
							 echo "<span class='text-success'><i class='fa fa-check-circle'></i> " . _('Mobile Verified') . "</span>";
						 }
						?>
                        </div>
                      </div>
					  
					  <div class="form-group">
					 <label class="col-md-3 control-label"><?php echo _("Country")?><small class="red">*</small></label>
					  <div class="col-md-8">
					 <?php $remoteCountry=$this->config->item('remote_country_code');?>
					   <select class="form-control"  id="ad_country" name="ad_country" required="required">
					   <option value="">--<?php echo _("Select option")?>--</option>
						 <?php 
						 
						  if(count($rsCountry)>0){
							  for($rC=0;$rC<count($rsCountry);$rC++){
							  ?>
							  <option value="<?php echo $rsCountry[$rC]->country_code;?>" <?php if($rsCountry[$rC]->country_code==$userInfo[0]->ac_country){echo 'selected';}?>><?php echo $rsCountry[$rC]->country_name;?></option>
							  <?php
							  }
						  }
						 ?>
					   </select>
					   </div>
					</div>
					
			
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Postcode")?></label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="postalcode" id="postalcode" placeholder="1217" value="<?php echo @$userInfo[0]->ac_postalcode;?>">
                        </div>
                      </div>
                      
                     
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9"> </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
						<?php 
							if($user_id!='1000578dd5df29bdd'){
						?>
                          <button type="submit" class="btn btn-primary btn-border" ng-click="basicInfoBtnIn()">
						  <i class="fa fa-pencil"></i> <?php echo _("Update")?></button>
						 <?php 
							} else{
								echo "<h5 class='text-danger'>" . _('Demo Account (Restricted Feature)') . "</h5>";
							}
						?>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="panel panel-pink">
                <div class="panel-pink-heading">
                  <h4 class="panel-title"> <a href="#collapseB2"  data-toggle="collapse"> <b><?php echo _("Password Settings")?></b> </a> </h4>
                </div>
                <div class="panel-collapse collapse" id="collapseB2">
                  <div class="panel-pink-body">
				   <?php 
				    if($user_id!='1000578dd5df29bdd'){
				   ?>
                    <form class="form-horizontal" role="form" id="profilePasswordForm" name="profilePasswordForm" method="post" action="<?php echo base_url()?>site/profile/update_password" ng-controller="profilePasswordCtrl">
                      <div class="form-group">
                        <div class="col-sm-12 text-center text-primary">
						   <br><br>
                           <i class="fa fa-info-circle"></i> <?php echo _("Please update your password in a while, to keep your account more strong.")?></div>
                      </div>
					   <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Old Password")?></label>
                        <div class="col-sm-8">
                          <input type="password" class="form-control"  required id="old_password" name="old_password" placeholder="********">
                        </div>
                      </div>
					  
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("New Password")?></label>
                        <div class="col-sm-8">
                          <input type="password" class="form-control"  required id="new_password" name="new_password" placeholder="********">
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Confirm Password")?></label>
                        <div class="col-sm-8">
                          <input type="password" class="form-control"  required id="retype_password" name="retype_password" placeholder="********">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-success btn-border" ng-click="passwordBtnIn()"> <i class="fa fa-key"></i> <?php echo _("Update")?></button>
                        </div>
                      </div>
                    </form>
					<?php 
					}else{
						echo "<h5 class='text-danger'>" . _('Demo Account (Restricted Feature)') . "</h5>";
					}
					?>
                  </div>
                </div>
              </div>
              <div class="panel panel-success2">
                <div class="panel-success2-heading">
                  <h4 class="panel-title"> <a href="#collapseB3"  data-toggle="collapse"> <b><?php echo _("Preferences")?></b> </a> </h4>
                </div>
                <div class="panel-collapse collapse" id="collapseB3">
                  <div class="panel-success2-body">
				   <div class="row">
                    <div class="form-group">
                      <div class="col-sm-12">
                       
					    <?php 
				    if($user_id!='1000578dd5df29bdd'){
				   ?>
                    <form class="form-horizontal" role="form" id="preferencesForm" name="preferencesForm" method="post" action="<?php echo base_url()?>site/profile/update_preferences" ng-controller="preferencesCtrl">
                      
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"> <?php echo _("Newsletters")?></label>
                        <div class="col-sm-8">
						  <?php 
						   if($userInfo[0]->news_letters!=0){
						  ?>
                               <input type="checkbox" id="check_newsletters" name="check_newsletters" checked> &nbsp;<?php echo _("I want to receive email newsletter.")?><?php
						   } else{
							   ?>
							   <input type="checkbox" id="check_newsletters" name="check_newsletters"> &nbsp;<?php echo _("I want to receive email newsletter.")?><?php
						   }
						   ?>
						  <input type="hidden" id="news_letters" name="news_letters" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo _("Credit Threshold")?></label>
                        <div class="col-sm-8">
                           <input type="number" value="<?php echo @$userInfo[0]->payment_threshold;?>" id="payment_threshold" name="payment_threshold" min="5" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" placeholder="<?php echo _('e.g. 10') ?>">
						   <small class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("We push alerts when your credit equal/lessthan this amount")?></small>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                          <button type="submit" class="btn btn-success btn-border" ng-click="preferencesBtnIn()"> <i class="fa fa-pencil"></i> <?php echo _("Update")?></button>
                        </div>
                      </div>
                    </form>
					<?php 
					}else{
						echo "<h5 class='text-danger'>" . _('Demo Account (Restricted Feature)') . "</h5>";
					}
					?>
					
                      </div>
					  </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--/.row-box End--> 
         
        </div>
        <!--/.page-content--> 
	   </div>
      </div>
  </div>
  </div>
 
 
<?php $this->load->view('site/modals/campaign/sms/upgrade_account');?>
<?php $this->load->view('site/modals/profile_thumbnail');?>
<?php $this->load->view('site/modals/mobile_validate');?>
<?php $this->load->view('themeFront/footer.php');?>



