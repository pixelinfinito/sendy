<style>.contact-acredit-bg{background:#e6ffff;padding:15px;}</style>
<div class="modal fade" id="modalFetchTitleFrom" tabindex="-1" role="dialog" aria-labelledby="smsFetchTItleLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Template Titles")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				<div class="row">
				<div class="col-md-12">
				<div class="col-md-7">
               <?php 
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allTitles= $this->sms_model->get_template_titles($user_id);
				$appSettings= $this->app_settings_model->get_primary_settings();
				$appCountry=$appSettings[0]->app_country;
				$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
				$walletInfo= $this->account_model->get_account_wallet($user_id);
				if(@$walletInfo[0]->balance_amount!=''){
					$accountCredit=$walletInfo[0]->balance_amount;
				}else{
					$accountCredit=0;
				}
				?>
				
				<h4 class="text-muted"> <?php echo _("Choose title")?><i class="fa fa-level-down"></i></h4>
				
				<table class="table table-bordered table-striped" id="dataTables-tTemplate">
				<thead ><!-- Table head -->
				<tr>
				<th class="col-sm-1">
				#
				</th>
				<!--<th class="col-sm-1"><?php echo _("ID")?></th>-->
				<th class="col-sm-1"><?php echo _("Template Code")?></th>
				<th class="col-sm-2"><?php echo _("Title")?></th>
				</tr>
				</thead><!-- / Table head -->
				<tbody>
				<?php 
				
				$key = 1; 

				?>
				<?php if ($allTitles!=0): foreach ($allTitles as $title) : ?>

				<tr>
				<td>
				<input type="checkbox" name="achk_title" class="achk_title"  value="<?php echo $title->template_id.'___'.$title->template_title?>">
				</td>
				<!--<td><?php echo $title->template_id?></td>-->
				<td><?php echo $title->template_code ?></td>
				<td><?php echo $title->template_title ?></td>
				</tr>
				<?php
				$key++;
				endforeach;
				?>
				<?php else : ?>
				<td colspan="6" class="text-center">
				<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
				</td>
				<?php endif; ?>
				</tbody><!-- / Table body -->
				</table> <!-- / Table -->
				
				</div>
				 <div class="col-md-5">
				  
				  <div class="alert alert-info">
					 <i class="fa  fa-google-wallet"></i><?php echo _("Avaialble Balance ")?>:<?php
					 if($accountCredit>0){
						echo "<b>".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.$accountCredit."</b>";
					  }else{
						  echo "<b>".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name." 00.00</b>";
					  }
					 ?>
				   </div>
				 
				   <div class="well well-sm text-center">
				    <h1 class="text-primary"><i class="fa fa-info-circle"></i></h1>
					<span class="text-muted"><?php echo _("Select (one) title  as a message body.")?></span><br>
		            <?php echo _("Selection allowed")?><b><?php echo _("maximum 1 title")?></b>.
				  </div>
				  
					<button class="btn btn-success btn-border" onclick="javascript:assignTitle()" >
					  <i class="fa fa-tag"></i> <?php echo _("Assign Title & Close")?></button>
					<br><br>
				</div>
				</div>
				</div>
			
		<!-- Modal Footer -->
		<div class="modal-footer">
			<div class="text-left"> <small class="text-muted"><?php echo _("For any assistance please contact our support team.")?></small> </div>
		</div>
	   </div>
</div>
</div>
</div>