<style>.contact-acredit-bg{background:#e6ffff;padding:15px;}</style>
<div class="modal fade" id="modalFetchContacttoFrom" tabindex="-1" role="dialog" aria-labelledby="smsFetchContactLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Contacts")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				<div class="row">
				<div class="col-md-12">
				<div class="col-md-7">
               <?php 
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				$appSettings= $this->app_settings_model->get_primary_settings();
				$appCountry=$appSettings[0]->app_country;
				$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
				$walletInfo= $this->account_model->get_account_wallet($user_id);
				if(@$walletInfo[0]->balance_amount!=''){
					$accountCredit=$walletInfo[0]->balance_amount;
				}else{
					$accountCredit=0;
				}
				?>
				
			<h4 class="text-muted"> <?php echo _("Select Contacts")?><i class="fa fa-level-down"></i></h4>
			  <table class="table table-bordered table-striped" id="dataTables-fContacts">
				<thead ><!-- Table head -->
				<tr>
				<th class="col-sm-1"><input type="checkbox" id="achk_contact_all" class="achk_contact_all" name="achk_contact_all"></th>
				<!--<th class="col-sm-1"><?php echo _("ID")?></th>-->
				<th class="col-sm-1"><?php echo _("Name")?></th>
				<th class="col-sm-1"><?php echo _("Mobile Number")?></th>
				<th class="col-sm-1"><?php echo _("Country")?></th>
				</tr>
				</thead><!-- / Table head -->
				<tbody>
				<?php 
				
				$all_contacts_info=$this->sms_model->get_contacts_byuser($user_id);
				$key = 1; 

				?>
				<?php if ($all_contacts_info!=0): foreach ($all_contacts_info as $contact) : ?>

				<tr>
				<td>
				<input type="checkbox" name="achk_contact" class="achk_contact"  value="<?php echo $contact->c_id.'_'.$contact->contact_mobile;?>">
				</td>
				<!--<td><?php echo $contact->c_id?></td>-->
				<td><?php echo $contact->contact_name ?></td>
				<td><?php echo $contact->contact_mobile ?></td>
				<td><?php echo $contact->country_name ?></td>
				</tr>
				<?php
				$key++;
				endforeach;
				?>
				<?php else : ?>
				<td colspan="6" class="text-center">
				  <h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
				  
				   <a href="javascript:void(0)" onclick="javascript:triggerSMSContact()" class="btn btn-sm explink"  title="<?php echo _('Add New Contact') ?>">
						<i class="fa fa-user-plus"></i> <?php echo _("Add Contacts")?></a>
					<a href="javascript:void(0)" onclick="javascript:triggerImportModal()" class="btn btn-sm explink"  title="<?php echo _('Import Contacts') ?>">
						<i class="fa fa-download"></i> <?php echo _("Import Contacts")?></a>
				</td>
				<?php endif; ?>
				</tbody><!-- / Table body -->
				</table> <!-- / Table -->
				
				</div>
				 <div class="col-md-5">
				  <div class="alert alert-info">
					 <i class="fa  fa-google-wallet"></i><?php echo _("Avaialble Balance ")?>:<?php
					 if($accountCredit>0){
						echo "<b>".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.$accountCredit."</b>";
					  }else{
						  echo "<b>".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name." 00.00</b>";
					  }
					 ?>
				   </div>
				 
				    <div class="well well-sm text-center">
				    <h1 class="text-primary"><i class="fa fa-info-circle"></i></h1>
				    <span class="text-muted"><?php echo _("Select single/multiple contacts where you wish to proeed for campaign.")?></span><br><br>
		            <?php echo _("Selection allowed <b>maximum 10 contacts</b>. If you wish to campaign more than 10 contacts you have to choose group campaign.")?></div>
					
					<button class="btn btn-success btn-border" onclick="javascript:assignContact()" >
					  <i class="fa fa-check"></i> <?php echo _("Select Contacts & Close")?></button>
					<br><br>
				</div>
				</div>
				</div>
			
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div class="text-left"> <small class="text-muted"><?php echo _("Select minimum one contact.")?></small> </div>
			</div>
           </div>
</div>
</div>
</div>