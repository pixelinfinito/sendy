<style>.contact-acredit-bg{background:#e6ffff;padding:15px;}</style>
<div class="modal fade" id="modalFetchGrouptoFrom" tabindex="-1" role="dialog" aria-labelledby="smsFetchGroupLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Group Contacts")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				<div class="row">
				<div class="col-md-12">
				<div class="col-md-7">
               <?php 
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				$appSettings= $this->app_settings_model->get_primary_settings();
				$appCountry=$appSettings[0]->app_country;
				$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
				$walletInfo= $this->account_model->get_account_wallet($user_id);
				if(@$walletInfo[0]->balance_amount!=''){
					$accountCredit=$walletInfo[0]->balance_amount;
				}else{
					$accountCredit=0;
				}
				?>
				
				<h4 class="text-muted"> <?php echo _("Choose group(s)")?><i class="fa fa-level-down"></i></h4>
				
			 <table class="table table-bordered table-striped" id="dataTables-fGroups">
				<thead ><!-- Table head -->
				<tr>
				<th class="col-sm-1">#</th>
				<!--<th class="col-sm-1"><?php echo _("ID")?></th>-->
				<th class="col-sm-1"><?php echo _("Group Name")?></th>
				<th class="col-sm-1"><?php echo _("Total Contacts")?></th>
				</tr>
				</thead><!-- / Table head -->
				<tbody>
				<?php 
				$key = 1; 

				?>
				<?php if ($allGroups!=0): foreach ($allGroups as $group) : ?>
				<?php 
				$allContacts=$this->sms_model->get_group_contacts($group->cgroup_id,$user_id);
				?>
				<tr>
				<td><input type="checkbox"  class="chk" value="<?php echo $group->cgroup_id.'_'.$group->group_name;?>"></td> 
				<!--<td><?php echo $group->cgroup_id?></td>-->
				<td><?php echo $group->group_name ?></td>
				<td class="text-primary">
				<?php 
					if($allContacts!=0){
						echo "(".count($allContacts).")";
					}else{
						echo "(0)";
					}
				?>
				</td>
				</tr>
				<?php
				$key++;
				endforeach;
				?>
				<?php else : ?>
				<td colspan="6" class="text-center">
				<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
				</td>
				<?php endif; ?>
				</tbody><!-- / Table body -->
				</table> <!-- / Table -->
			
				</div>
				
				 <div class="col-md-5">
				  <div class="alert alert-info">
					 <i class="fa  fa-google-wallet"></i><?php echo _("Avaialble Balance ")?>:<?php
					 if($accountCredit>0){
						echo "<b>".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.$accountCredit."</b>";
					  }else{
						  echo "<b>".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name." 00.00</b>";
					  }
					 ?>
				   </div>
				   
				   <div class="well well-sm text-center">
				    <h1 class="text-primary"><i class="fa fa-info-circle"></i></h1>
					<span class="text-muted"><?php echo _("Select single/multiple groups where you wish to proeed for campaign.")?></span><br><br>
				    <?php echo _("Selection allowed")?><b><?php echo _("maximum 50 groups")?></b>( <?php echo _("no limit in contacts ). 
					If you wish to campaign more than 50 groups please contact our support team.
				  ")?></div>
				  
				   <button name="assign_btn" id="assign_btn"  class="btn btn-success btn-border">
						<i class="fa  fa-check"></i> <?php echo _("Select Groups & Close")?></button>
				   <br><br>
				</div>
				</div>
				</div>
				
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div class="text-left"> <small class="text-muted"><?php echo _("Select minimum 1 group.")?></small> </div>
			</div>
           </div>
</div>
</div>
</div>