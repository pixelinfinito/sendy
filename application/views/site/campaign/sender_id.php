<div class="modal fade" id="modalSenderIdReq" tabindex="-1" role="dialog" aria-labelledby="smsSIdLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="smsSIdLabel">
                    <?php echo _("Request SenderID")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
			<form ng-controller="SenderIdReqCtrl" class="form-horizontal" method="post" enctype="multipart/form-data"  role="form" name="sIdReqForm" id="sIdReqForm">
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Number")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				   <input type="hidden"  class="form-control" id="cn_tid" name="cn_tid" required="required">
				   <input type="hidden"  class="form-control" id="cn_status" name="cn_status" required="required">
				   <input type="text"  class="form-control" id="sender_number" name="sender_number" required="required" readonly>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("SenderId Name")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				   <input type="text" placeholder="<?php echo _('E.g. GLOBALSMS') ?>" class="form-control" id="sender_id_name" name="sender_id_name" required="required" onkeyup="javascript:check_callerid_available(this.value)">
				   <small class="text-muted"><em><b><?php echo _("Note")?>:</b> <?php echo _("Maximum 11 Characters allowed")?></em></small>
				 </div>
				 </div>
				 </div>
				</div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label>&nbsp;</label></div>
				  
				 <div class="col-md-3 ">
				 <button class="btn btn-border btn-success" type="button" ng-click="senderIdReqBtn()">
				 <i class="fa fa fa-hand-stop-o"></i> <?php echo _("Request")?></button>
				 </div>
				 
				 <div class="col-md-5" id="dySIdReqProgress">
				  
				 </div>
				 </div>
				 </div>
				</div>
				</form>
				
            <!-- Modal Footer -->
            <div class="modal-footer">
			
                <div id="cnmdx_process" class="pull-left"></div>
				<small class="red">* <?php echo _("Mandatory Fields")?></small>
            </div>
       
    </div>
</div>
</div>
</div>