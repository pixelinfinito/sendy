<div class="modal fade" id="modalImportContacts" tabindex="-1" role="dialog" aria-labelledby="smsImportContactLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Import Contacts")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				<?php 
				
				
				$allCountries= $this->countries_model->list_countries();
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				$availableCountries=$this->sms_model->list_smsprice_aivailable_countries();
				$remoteCountry=$this->config->item('remote_country_code');
				?>
				
				<form action="<?php echo base_url()?>site/sms/import_contacts" class="form-horizontal" method="post" enctype="multipart/form-data"  role="form" name="sImportContactForm" id="sImportContactForm">
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Choose Group")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				  <select class="form-control" id="import_group" name="import_group" required>
				   <option value="">--<?php echo _("Select")?>--</option>
				    <?php 
				    foreach($allGroups as $group){
						?>
						<option value="<?php echo $group->cgroup_id;?>"><?php echo ucfirst(strtolower($group->group_name));?></option>
						<?php
					}
				   ?>
				  </select>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Available Countries")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				    <select class="form-control" id="import_country" name="import_country" required="required">
				   <option value="">--<?php echo _("Select Country")?>--</option>
				   <?php 
				    foreach($availableCountries as $country){
						?>
						<option value="<?php echo $country->calling_code.'__'.$country->country_code;?>" <?php if($country->country_code==$remoteCountry){echo 'selected';}?>>
							<?php echo $country->country_name.'('.$country->calling_code.')'?>
						</option>
						<?php
					}
				   ?>
				  </select>
				  
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group" >
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label>Upload [.CSV]</label> <small class="red">*</small></div>
				 <div class="col-md-8">
				 <input type="file" type="file" name="file"  class="form-control" id="file"  required>
				 </div>
				 </div>
				 </div>
				 </div>
				
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				 <button type="submit" class="btn btn-border btn-success" type="button"  >
				 <i class="fa fa-exchange"></i> <?php echo _("Import")?></button>
				 
                				 
				   
				 </div>
				 <div class="col-md-6" id="dyASMSProgress">
				  <a href="<?php echo base_url()?>documents/formats/import_contacts.csv"><i class="fa fa-download"></i> <?php echo _("Download Sample Format")?></a>
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				
         
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div id="" class="pull-left">
				<strong><?php echo _("Note")?>:</strong> <?php echo _("File upload allows only \".csv\" format.")?></div>
				<small class="red">* <?php echo _("Mandatory Fields")?></small>
            </div>
       
    </div>
</div>
</div>
</div>