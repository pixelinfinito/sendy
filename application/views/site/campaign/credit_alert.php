<div class="modal fade" id="modalCreditAlert" tabindex="-1" role="dialog" aria-labelledby="creditAlertLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="creditAlertLabel">
                    <?php echo _("Credit Alert")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				<div class="row">
				<div class="col-md-12">
				   
				 <div class="col-md-3 text-center">
				 <br>
				   <h1 class="text-danger"><i class="fa fa fa-frown-o"></i></h1>
				 
				</div>
				
				 <div class="col-md-8 text-left">
				   <h1 class="text-danger"><?php echo _("Insufficient Credit")?></h1>
				   <div id="">
				      <?php echo _("Sorry we can't submit your campaign, since you don't have enough credit.")?><br><br>
					  
					  <a href="<?php echo base_url()?>site/wallet/add" class="btn btn-success btn-sm"> <i class="fa fa-dollar"></i> <?php echo _("Add Funds")?></a>
				   </div>
				   <br><br>
				 </div>
				 
				</div>
				</div>
				
			
			
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div class="text-left"> <small class="text-muted"><?php echo _("Make sure you have enough credit before you proceed to campaign")?></small> </div>
			</div>
       
           </div>
</div>
</div>
</div>