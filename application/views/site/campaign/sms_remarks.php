<div class="modal fade" id="modalSMSRemarks" tabindex="-1" role="dialog" aria-labelledby="smsSMSRemarks" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="smsSMSRemarks">
                   <?php echo _("System Response")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
			 
			 <div class="row">
			    <div class="col-md-3 text-center">
				 <br>
				   <h1 class="text-danger"><i class="fa fa-exclamation-triangle"></i></h1>
				</div>
				
				 <div class="col-md-8 text-left">
				   <h1 class="text-danger"><?php echo _("SMS Remarks")?></h1>
				   <div id="rsRemarks">
				   </div>
				   <br><br>
				 </div>
			 </div>			 
            
				
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div class="text-left">
				<small>
				<i class="fa fa-info-circle"></i> 
				<?php echo _("Please check")?><b><?php echo _("from number")?></b> & <b><?php echo _("sender id")?></b> <?php echo _("before you proceed to campaign")?></small>
				</div>
            </div>
       
           </div>
</div>
</div>
</div>