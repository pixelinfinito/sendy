<div class="modal fade" id="modalAddSMSGroup" tabindex="-1" role="dialog" aria-labelledby="smsGroupLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="smsGroupLabel">
                    <?php echo _("Add Group")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				
             <?php 
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				
				?>
				
				<form class="form-horizontal" ng-controller="addSMSGroupFrmCtrl" role="form" name="addSMSGroupForm" id="addSMSGroupForm">
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Name")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				 <input type="text" class="form-control" name="group_name" id="group_name" required>
				 </div>
				 </div>
				 </div>
				 </div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Type")?></label></div>
				 <div class="col-md-8">
				   <select class="form-control" id="group_type" name="group_type">
				     <option value="public"><?php echo _("Public")?></option>
					 <option value="private"><?php echo _("Private")?></option>
				   </select>
				 </div>
				 </div>
				 </div>
				</div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				  <button class="btn btn-border btn-success" type="button" ng-click='addSMSGroupBtn();' >
				   <i class="fa fa-plus-circle"></i> <?php echo _("Add")?></button>
				   
				 </div>
				 <div class="col-md-6" id="dyAGrpProgress">
				 
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div id="" class="pull-left"></div> <small class="red">* <?php echo _("Mandatory Fields")?></small>
            </div>
       
           </div>
</div>
</div>
</div>