<?php $this->load->view('themeFront/header_logged.php');
//$this->load->view('themeFront/banner_intro.php');
?>
   
<script type="application/javascript" src="<?php echo base_url();?>js/user_wallet.js"></script>

<?php

$remoteCountry=$this->config->item('remote_country_code');
$remoteCity=$this->config->item('remote_geo_city');
if($remoteCity!=''){
	$geoCity=$remoteCity;
	$geolatitude=$this->config->item('remote_geo_latitude');
	$geolongitude=$this->config->item('remote_geo_longitude');
}else{
	$geoCity='';
	$geolatitude='';
	$geolongitude='';
}
$currencyInfo= $this->countries_model->get_currencies_cCode($remoteCountry);

$user_id = $this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);
$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);
 

$paymentSettings=$this->app_settings_model->get_payment_settings();
$settingsInfo= $this->app_settings_model->get_site_settings();



?>

<div class="main-container">
    <div class="container" >
       <?php //$this->load->view('includes/search_inc.php');?>
	   <div class="">
	   <div class="row">
	       
		    <div class="col-md-6 text-left"> 
			    <?php 
				$walletInfo= $this->account_model->get_account_wallet($user_id);
				$appSettings= $this->app_settings_model->get_primary_settings();
				$appCountry=$appSettings[0]->app_country;
				$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
				
				echo "&nbsp;&nbsp;&nbsp;Current available balance<br>";
				if(@$walletInfo[0]->balance_amount!=''){
				   ?>
				  <h1 class="text-primary">
				   <?php echo "&nbsp;&nbsp;&nbsp;".@$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.@$walletInfo[0]->balance_amount;?>
				   </h1>
				   <?php
				}else{
					?>
					<h1 class="text-danger">
					<?php echo "&nbsp;&nbsp;&nbsp;".@$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name." (0)";?>
					</h1>
					<?php
				}
				?>
				
		    </div>
			<div class="col-md-6">
			<div class="row">
			  <div class="col-md-4">
			 <div class="hdata">
			  <div class="mcol-left">
			    <i class="fa  fa-dollar"></i>
			  </div>
			  <div class="mcol-right">
			    <a href=""><?php echo _("Add Wallet Funds")?></a>
			  </div>
			 </div>
			 </div>
			
			<div class="col-md-4">
			 <div class="hdata">
			  <div class="mcol-left">
			    <i class="fa  fa-credit-card"></i>
			  </div>
			  <div class="mcol-right">
			    <a href=""><?php echo _("Manage Credit/Debit Card")?></a>
			  </div>
			 </div>
			 </div>
			 
			 <div class="col-md-4">
			 <div class="hdata">
			  <div class="mcol-left">
			    <i class="fa  fa-history"></i>
			  </div>
			  <div class="mcol-right">
			    <a href="<?php echo base_url()?>site/payment/history"><?php echo _("Payment History")?></a>
			  </div>
			 </div>
			 </div>
			 </div>
			</div>
         
		 </div>
	   </div>
	   <div class="row" >
	     
	   <div class="col-md-12 page-content">
          <div class="inner-box category-content">
		  
		   <h3 class="title-2">
		   <i class="fa fa fa-google-wallet"></i> <?php echo _("Wallet History")?></h3>
		   
		   
		  <div class="row">
           <?php 
		   $rsAdModes= $this->ads_model->list_adModes();
		   $rsAdFeatures= $this->ads_model->list_adFeatures();
		   $rsIcon= $this->ads_model->list_adIcon();
		   $rsCategory = $this->categories_model->list_categories();
		   $rsCountry = $this->countries_model->list_countries();
		   $rsCurrency= $this->app_settings_model->get_currencies();
		  
		   $siteSettings=$this->app_settings_model->get_site_settings();
		   $walletInfo = $this->account_model->get_account_wallet($user_id);
		  
		   ?>
		    <div class="panel-body">
			<div ng-app="WalletApp">
           <div ng-controller="WalletLogCrtl">
          <div class="box">
             <div class="box-header">
            <div class="row">
            <div class="col-md-2">
            <select ng-model="entryLimit" class="form-control">
            <option>5</option>
            <option>10</option>
            <option>20</option>
            <option>50</option>
            <option>100</option>
            </select>
			</div>
			
			<div class="btn-group pull-left" style="padding-left:5px">
			<a href="/users/export_users_to_excel" class="btn btn-sm" style="border:1px solid #ddd;height:35px" title="<?php echo _('Export To Excel') ?>">
			 <img src="<?php echo base_url()?>images/icons/xls.png" border="0">
			</a>
			<button class="btn btn-sm" style="border:1px solid #ddd;height:35px" onclick="javascript:window.print()" title="<?php echo _('Print') ?>">
			<img src="<?php echo base_url()?>images/icons/print_16.png">
			</button>
			</div>
			
            <div class="col-md-3 pull-right">
            <input type="text" ng-model="search" ng-change="filter()" placeholder="<?php echo _('Filter') ?>" class="form-control" />
			</div>
            <div class="col-md-3 pull-right">
             <h5>Filtered {{ filtered.length }} of {{ totalItems}} total Logs</h5>
            </div>
            </div>
           </div>
		   <br>
		   
		    <div class="box-body">
			<div class="row">
			
            <div class="col-md-12" ng-show="filteredItems > 0">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
			<th>
			 <input type="checkbox" name="adIdsAll" id="adIdsAll" value="">
			</th>
            
            <th><?php echo _("Ad/Listing Reference")?>&nbsp;</th>
            <th><?php echo _("Payment")?></th>
            <th><?php echo _("Date of Payment")?></th>
			<th><?php echo _("Payment/Invoice Reference")?></th>
			<th><?php echo _("Remarks")?></th>
			<th><?php echo _("Status")?></th>
			<th><?php echo _("Invoice")?></th>
			<th><?php echo _("Actions")?></th>
            
            </thead>
			
            <tbody>
            <tr ng-repeat="data in filtered = (list | filter:search | orderBy : predicate :reverse) | startFrom:(currentPage-1)*entryLimit | limitTo:entryLimit">
			<td width="1%"><input type="checkbox"  value="{{data.payment_id}}"></td>
            
            <td>
              <a href="<?php echo base_url()?>site/ads/details?rfc={{data.ad_reference}}">
			  <u>{{data.ad_reference}}</u>
			  </a>
            </td>
			<td>
            <?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name;?> {{data.ad_payment}}
            </td>
			<td>
            {{data.date_of_payment}}
            </td>
			<td>
            {{data.payment_reference}}
            </td>
			<td>
            {{data.payment_remarks}}
            </td>
			<td ng-if="data.payment_status=1">
             <div class="ad_cash_paid_btn"> <i class="fa fa-check"></i> <?php echo _("Paid")?></div>
            </td>
			
			<td ng-if="data.payment_status=0">
             <div class="ad_unpublish_btn"> <i class="fa fa-times"></i> <?php echo _("Failed")?></div>
            </td>
			
			<td ng-if="data.payment_status=1">
             <a href="#" title="<?php echo _('Download Invoice') ?>"> <i class="fa fa-download"></i> INVW9889</a>
            </td>
			<td ng-if="data.payment_status=0">
			-
			</td>
			
			<td>
			<a href="javascript:void(0)" title="<?php echo _('Delete') ?>" id="delFavAd{{data.payment_id}}" ng-click="triggerDisp(data.payment_id)" class='btn btn-xs btn-danger'>
            <i class='glyphicon glyphicon-trash'></i> 
            </td>
            </tr>
            </tbody>
            </table>
			
            </div>
            <div class="col-md-12" ng-show="filteredItems == 0">
            <div class="col-md-12">
            <h4><?php echo _("No Logs found.")?></h4>
            </div>
            </div>
            <div class="col-md-12" ng-show="filteredItems > 0">    
            <div pagination="" page="currentPage" on-select-page="setPage(page)" boundary-links="true" total-items="filteredItems" items-per-page="entryLimit" class="pagination-small" previous-text="&laquo;" next-text="&raquo;"></div>
            
            </div>
			
            </div>
			</div>
			</div>
			
            </div>
            </div>
			  
			  
			</div> <!--/// end panel body -->
		</div>
		</div>
		</div>
	  
        </div>
		
	  
		
	  </div>
	 </div>
  


<?php $this->load->view('site/modals/adfeature_alert');?>
<?php $this->load->view('site/modals/adbasic_alert');?>
<?php $this->load->view('themeFront/footer_classic.php');?>



