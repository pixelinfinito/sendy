<div class="modal fade" id="modalAddContact" tabindex="-1" role="dialog" aria-labelledby="smsContactLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Add Contact")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				<?php 
				$allCountries= $this->countries_model->list_countries();
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				$callCodes=$this->sms_model->list_smsprice_aivailable_countries();
				$remoteCountry=$this->config->item('remote_country_code');
				?>
				
				<form class="form-horizontal" ng-controller="addSMSFormCtrl" role="form" name="addSMSContactForm" id="addSMSContactForm">
				<div class="form-group" >
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Full Name")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				 <input type="text" class="form-control" name="contact_name" id="contact_name"  required>
				 </div>
				 </div>
				 </div>
				 </div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Mobile Number")?></label> <small class="red">*</small></div>
				 <div class="col-md-4">
				    <select class="form-control" id="caller_code" name="caller_code" required="required" onchange="trCountry(this.value)">
				   <option value="">--<?php echo _("Calling Code")?>--</option>
				   <?php 
				    foreach($callCodes as $country){
						?>
						<option value="<?php echo $country->calling_code.'__'.$country->country_code;?>" <?php if($country->country_code==$remoteCountry){echo 'selected';}?>>
							<?php echo $country->country_name.'('.$country->calling_code.')'?>
						</option>
						<?php
					}
				   ?>
				  </select>
				 </div>
				 <div class="col-md-4">
				 <input type="number" class="form-control" min="0" step="1" data-bind="value:contact_number" name="contact_number" id="contact_number" placeholder="<?php echo _('E.g. 885xxxx') ?>" required>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Email ID")?></label></div>
				 <div class="col-md-8">
				 <input type="email" class="form-control" name="contact_email" id="contact_email" placeholder="<?php echo _('E.g. dname@domain.com') ?>">
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Contact Group")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				  <select class="form-control" id="contact_group" name="contact_group" required>
				   <option value="">--<?php echo _("Select")?>--</option>
				    <?php 
				    foreach($allGroups as $group){
						?>
						<option value="<?php echo $group->cgroup_id;?>"><?php echo ucfirst(strtolower($group->group_name));?></option>
						<?php
					}
				   ?>
				  </select>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Country")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				 <!--<input type="text" class="form-control" id="contact_country" name="contact_country" required="required" />-->
				
				  <select class="form-control" id="contact_country" name="contact_country" required="required" disabled>
				   <option value="">--<?php echo _("Select")?>--</option>
				   <?php 
				    foreach($callCodes as $country){
						?>
						<option value="<?php echo $country->country_code;?>" <?php if($country->country_code==$remoteCountry){echo 'selected';}?>>
							<?php echo $country->country_name;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				 <button class="btn btn-border btn-success" type="button" ng-click='addSMSContactBtn()' >
				 <i class="fa fa-plus-circle"></i> <?php echo _("Add")?></button>
				  
				   
				 </div>
				 <div class="col-md-6" id="dyASMSProgress">
				 
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				
         
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div id="" class="pull-left">
				<strong><?php echo _("Note")?>:</strong> <?php echo _("Mobile number should not enter multiple times.")?></div>
				<small class="red">* <?php echo _("Mandatory Fields")?></small>
            </div>
       
    </div>
</div>
</div>
</div>