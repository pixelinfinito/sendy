<div class="modal fade" id="modalAddSMSTitle" tabindex="-1" role="dialog" aria-labelledby="smsTempTitleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" >
                    <?php echo _("Add Template")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				
               <?php 
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				
				$smsBackSettings=$this->app_settings_model->get_sms_settings();
				if($smsBackSettings[0]->campaign_minval!=0 && $smsBackSettings[0]->campaign_maxval!=0){
					$randMinval=$smsBackSettings[0]->campaign_minval;
					$randMaxval=$smsBackSettings[0]->campaign_maxval;
				}else{
					$randMinval=1000;
					$randMaxval=10000;
				}
				
				$template_code="OCTT-".rand($randMinval,$randMaxval);
				?>
				
				<form class="form-horizontal" ng-controller="addSMSTitleFrmCtrl" role="form" name="addSMSTitleForm" id="addSMSTitleForm">
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Template Code")?></label></div>
				 <div class="col-md-8">
				 <?php echo $template_code;?>
				 <input type="hidden" class="form-control" name="template_code" id="template_code" value="<?php echo $template_code;?>">
				 </div>
				 </div>
				 </div>
				 </div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Title")?></label> <span class="red">*</span></div>
				 <div class="col-md-8">
				     <textarea class="form-control" id="template_title" name="template_title"></textarea>
				 </div>
				 </div>
				 </div>
				</div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				  <button class="btn btn-border btn-success" type="button" ng-click='addSMSTitleBtn();' >
				   <i class="fa fa-tag"></i> <?php echo _("Add")?></button>
				   
				 </div>
				 <div class="col-md-6" id="dyATTProgress">
				 
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div id="" class="pull-left"></div> <small class="red">* <?php echo _("Mandatory Fields")?></small>
            </div>
       
           </div>
</div>
</div>
</div>