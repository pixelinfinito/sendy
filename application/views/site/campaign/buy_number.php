<div class="modal fade" id="modalBuyNumber" tabindex="-1" role="dialog" aria-labelledby="buyNumberLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Buy A Number")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body" >
				
             <?php 
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				$walletInfo= $this->account_model->get_account_wallet($user_id);
				$appSettings= $this->app_settings_model->get_primary_settings();
				$appCountry=$appSettings[0]->app_country;
				$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
				$allCountries= $this->app_settings_model->get_merge_caller_numbers();
				
				$callerNumbertypes= $this->app_settings_model->caller_number_classes();
				$settingsInfo= $this->app_settings_model->get_site_settings();
				$wallet_min_val=@$settingsInfo[0]->wallet_min_val;
				$wallet_max_val=@$settingsInfo[0]->wallet_max_val;
				$wallet_string=@$settingsInfo[0]->wallet_string;
				$walletCode=$wallet_string.rand($wallet_min_val,$wallet_max_val);
					 if($walletCode!=''){
						 $wItemCode=$walletCode;
					 }else{
						 $wItemCode='OCWL'.rand(600,6000);
					 }
					 
			     ?>
				
				<form class="form-horizontal" ng-controller="buyNumberFrmCtrl" role="form" name="buyCNumberForm" id="buyCNumberForm">
				<div class="row">
				<div class="col-md-12">
				<div class="col-md-7">
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Caller Number Type")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				  <select class="form-control" id="cn_type" name="cn_type" required="required">
				   <option value="">--<?php echo _("Select")?>--</option>
				   <?php 
				    foreach($callerNumbertypes as $class){
						?>
						<option value="<?php echo $class->cl_id;?>"><?php echo $class->class_name;?></option>
						<?php
					}
				   ?>
				  </select>
				 </div>
				 </div>
				 </div>
				</div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Country")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				  <select class="form-control" id="cn_country" name="cn_country" required="required" onchange="javascript:displayCNPrice(this.value)">
				   <option value="">--<?php echo _("Select")?>--</option>
				   <?php 
				    foreach($allCountries as $country){
						?>
						<option value="<?php echo $country->cn_country.'-'.$country->cn_price.'-'.$country->cn_period.'-'.$currencyInfo[0]->currency_name;?>">
						<?php echo $country->country_name;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				  <input type="hidden" class="form-control" id="hidCNdyPrice" name="hidCNdyPrice">
				  <small>[<?php echo _("Avaialble countries")?>]</small>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Lease Period")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				  <select class="form-control" id="cn_lease_period" name="cn_lease_period" onchange="javascript:totalCNPrice(this.value)" required="required">
				   <option value="">--<?php echo _("Select")?>--</option>
				   <?php 
				    for($l=1;$l<24;$l++){
						?>
						<option value="<?php echo $l;?>">
						 <?php echo $l." Months";?>
						</option>
						<?php
					}
				   ?>
				  </select>
				  <small>[<?php echo _("Avaialble countries")?>]</small>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Total")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				   <input type="text" class="form-control" name="total_cn_price" id="total_cn_price" readonly > 
				   <?php 
				    if(@$walletInfo[0]->balance_amount>0){
						  $walletBalance=$walletInfo[0]->balance_amount;
					  }else{
						  $walletBalance=0;
					  }
				  
					
				   ?>
				   <input type="hidden" class="form-control" id="hidWalletBalance" name="hidWalletBalance" value="<?php echo $walletBalance; ?>">
				   <input type="hidden" class="form-control" id="hidWalletReference" name="hidWalletReference" value="<?php echo $wItemCode; ?>">
				 </div>
				 </div>
				 </div>
				</div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				  <button class="btn btn-border btn-success" type="button" ng-click='buyCNumberBtn();' >
				   <i class="fa fa-shopping-cart"></i> <?php echo _("Proceed to Buy")?></button>
				   
				 </div>
				 <div class="col-md-6" id="dyCNProgress">
				 
				 </div>
				 </div>
				 </div>
				</div>
			    </div>
				
				<div class="col-md-5">
				 <i class="fa  fa-bookmark-o"></i> <strong><?php echo _("Payment Reference ")?>:</strong> <?php echo $wItemCode; ?><hr>
				 <div class="alert alert-info">
				 <i class="fa  fa-google-wallet"></i> <?php echo _("Avaialble Balance ")?>:<?php
				 if(@$walletInfo[0]->balance_amount>0){
					echo "<b>".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.$walletInfo[0]->balance_amount."</b>";
				  }else{
					  echo "<b>".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name." 00.00</b>";
				  }
				 ?>
				 </div>
				 
				 <div class="well well-sm text-center" id="cn_country_numbers">
				   <h1 class="text-primary"><i class="fa fa-info-circle"></i></h1>
				    <b><?php echo _("Note")?>:</b>  <?php echo _("Caller number will be assigned upon your country selection & number might not be same as your expected.
					A dynamic number will be assinged based on avaialability.
					
				 ")?></div>
				 
				 <div>
				  <h4>
				   <?php echo _("Number Pricing ")?>:<span id="cn_projected_price" class="text-primary"><small>--<?php echo _("select country")?>--</small></span>
				  <small id="cn_period_content"></small> 
				  </h4>
				 
				 </div>
				</div>
				</div>
				</div>
				</form>
				
            
            <!-- Modal Footer -->
            <div class="modal-footer">
             <div class="pull-left">
			   <small class="red">* <?php echo _("Mandatory Fields")?></small>
			 </div> 
		    </div>
          </div>
</div>
</div>
</div>