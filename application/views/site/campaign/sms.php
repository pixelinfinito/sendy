<?php $this->load->view('themeFront/header_logged.php');?>
   
<style>.margin-lft15{margin-left:15px;}.hour{padding:10px;cursor:pointer}.today,.minute{padding:10px;cursor:pointer}.day{margin:10px;cursor:pointer}
.next,.prev{cursor:pointer}.day:hover{background:#e4e4e4}.table-condensed td{padding:10px;}</style>

<link href="<?php echo base_url(); ?>theme4.0/client/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/jq_datetime/jquery.simple-dtpicker.js"></script>
<link type="text/css" href="<?php echo base_url()?>assets/jq_datetime/jquery.simple-dtpicker.css" rel="stylesheet" />

<script type="application/javascript" src="<?php echo base_url();?>js/sms_campaign/sms.js"></script>
<script type="application/javascript" src="<?php echo base_url();?>js/sms_campaign/reports.js"></script>
<script type="text/javascript">
$(function() {
    $('*[name=cmps_datetime]').appendDtpicker({
		"autodateOnStart": false
	});
});
	
</script>  
<?php

$remoteCountry=$this->config->item('remote_country_code');
$remoteCity=$this->config->item('remote_geo_city');
if($remoteCity!=''){
	$geoCity=$remoteCity;
	$geolatitude=$this->config->item('remote_geo_latitude');
	$geolongitude=$this->config->item('remote_geo_longitude');
}else{
	$geoCity='';
	$geolatitude='';
	$geolongitude='';
}
$currencyInfo= $this->countries_model->get_currencies_cCode($remoteCountry);
$user_id = $this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);
$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);
 
$paymentSettings=$this->app_settings_model->get_payment_settings();
$settingsInfo= $this->app_settings_model->get_site_settings();
?>

<div class="main-container no-padding">
<div class="row top10_padding">
    <div class="container top-shadow">
	<div class="col-md-6 text-left no-padding"> 
	<ul class="breadcrumb pull-left">
			<li>
			<a href="<?php echo base_url()?>site/dashboard">
			<i class="icon-home fa"></i></a>
			</li>
			<li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
			<li><a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('mc')?>"><?php echo _("Campaigns")?></a></li>
			<li><a href="#"><?php echo _("Start Campaign")?></a></li>
	</ul> 
	</div>
	
	<div class="col-md-6 text-right no-padding "> 
		<?php 
		   $smsMServiceInfo= $this->account_model->get_sms_service($user_id);
		   $sms_service_status=@$smsMServiceInfo[0]->trigger_status;
		   if($sms_service_status==1){
			   ?>
			<button id="stop_caller"  class="btn btn-warning"  onclick="javascript:stopSmsService()">
			<i class="fa  fa-stop"></i> <?php echo _("Stop SMS Services..")?></button>
			   <?php
		   }
			else{
				?>
		    <button id="service_caller"  class="btn btn-default"  onclick="javascript:startSmsService()">
			<i class="fa  fa-rocket"></i> <?php echo _("Start SMS Services")?></button>
			<?php
		   }
			?>
			
		    <button class="btn btn-primary"  onclick="javascript:triggerBuyNumberModal()">
			<i class="fa fa-shopping-cart"></i> <?php echo _("Buy a Number")?></button>
			
	  </div>
	</div>
</div>

    <div class="container">
      
	     <?php 
			  if($userInfo[0]->is_blocked!=1)
			  {
			?>
		   
		  <div class="row">
           <?php 
		   $rsCountry = $this->countries_model->list_countries();
		   $rsCurrency= $this->app_settings_model->get_currencies();
		  
		   $siteSettings=$this->app_settings_model->get_site_settings();
		   $walletInfo = $this->account_model->get_account_wallet($user_id);
		   ?>
		    <div class="col-md-2"> 
			   <?php 
			    $this->load->view('themeFront/campaign_sidebar');
			   ?>
			</div>
			<div class="col-md-10">
				<!-- Tab panes -->
				<div class="tab-content" ng-app="SMSApp">
				  
				   <?php 
				    $tkn=base64_decode($this->input->get('tkn'));
					switch($tkn){
						
						case 'nc':
						$this->load->view('includes/sms_campaign/new_sms');
						break;
						
						case 'mc':
						$this->load->view('includes/sms_campaign/my_campaigns');
						break;
						
						case 'cts':
						$this->load->view('includes/sms_campaign/contacts_inc');
						break;
						
						case 'grp':
						$this->load->view('includes/sms_campaign/groups_inc');
						break;
						
						case 'tt':
						$this->load->view('includes/sms_campaign/titles_inc');
						break;
						
						case 'sch':
						$this->load->view('includes/sms_campaign/schedules_inc');
						break;
						
						case 'ntf':
						$this->load->view('includes/sms_campaign/notifications_inc');
						break;
						
						case 'cn':
						$this->load->view('includes/sms_campaign/caller_numbers_inc');
						break;
						
						case 'sp':
						$this->load->view('includes/sms_campaign/sms_price_inc');
						break;
						
						case 'rpt':
						$this->load->view('includes/sms_campaign/reports_inc');
						break;
						
						case 'hist':
						$this->load->view('includes/sms_campaign/sms_history_inc');
						break;
						
						default:
						$this->load->view('includes/sms_campaign/new_sms');
					}
				   ?>
				  
				  <?php $this->load->view('site/campaign/buy_number');?>
				</div>
			</div>
			
		</div>
		 <?php 
		  } else{
			  ?>
			<div class="row">  
			<div class="col-md-12"> 
		    <ol class="breadcrumb pull-left">
			<li><a href="#"><i class="icon-home fa"></i></a></li>
			<li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
			<li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Campaign")?></a></li>
			<li><?php echo _("Start Campaign")?></li>
		   </ol> 
		   </div>
		   </div>
		   
			<div class="col-md-6 alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<h4><i class="icon fa fa-ban"></i> <?php echo _("System Alert !")?></h4>
			 <?php echo _("Your campaign service turned to blacklisted, please contact support team for more details.")?><br>
             <b>---<?php echo _("Remarks-")?>--</b><br>
             <?php echo $userInfo[0]->block_remarks;?>			 
		    </div>
			
			<div class="col-md-6">
			  <h4><?php echo _("SMS Campaign")?></h4>
			  <?php echo _("Services are temporarly blocked, since your account is blacklisted. You can't access this services until 
			  your account turn to whitelist.")?><br><br>
			  <a href="#" class="btn btn-danger"> <i class="fa  fa-support"></i> <?php echo _("Support")?></a>
			</div>
			  <?php
		  }
		?>
		
		</div>
	    
</div>
  
<script src="<?php echo base_url(); ?>theme4.0/client/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme4.0/client/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme4.0/client/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
		$('#dataTables-contacts').DataTable({
			"order": [[ 1, "asc" ]],
			"aoColumnDefs" : [{
            'bSortable' : false,
            'aTargets' : [ 0,5 ]}],
			"columns": [
			{ "width": "1%" },
			null,
			null,
			null,
			null,
			null
			]
		});
		
		$('#dataTables-sCampaigns').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		$('#dataTables-campaignGroups').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		$('#dataTables-cSchedules').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		$('#dataTables-templateTitles').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		$('#dataTables-callerNumbers').DataTable({
			"order": [[ 0, "desc" ]]
		});
		
		$('#dataTables-SmsPricing').DataTable();
		
		$('#dataTables-SmsHistory').DataTable( {
			"order": [[ 0, "desc" ]]
		});
		$('#dataTables-fContacts').DataTable( {
			"order": [[ 1, "asc" ]],
			"aoColumnDefs" : [{
            'bSortable' : false,
            'aTargets' : [ 0]}]
			
		});
		$('#dataTables-fGroups').DataTable( {
			"order": [[ 1, "asc" ]]
		});
		$('#dataTables-tTemplate').DataTable( {
			"order": [[ 1, "asc" ]]
		});
		
		$('#dataTables-Notify').DataTable( {
			"order": [[ 0, "desc" ]]
		});
		
		
		
		
		
    });
</script>


<?php $this->load->view('themeFront/footer_classic.php');?>

