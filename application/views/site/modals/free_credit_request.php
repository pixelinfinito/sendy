<div class="modal fade" id="creditRequestModal" tabindex="-1" role="dialog" 
     aria-labelledby="cReqModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Free Credit Request")?></h4>
            </div>
			<div class="modal-body">
			    <?php 
				 $member_name=$this->session->userdata['site_login']['ac_first_name']." ".$this->session->userdata['site_login']['ac_last_name'];
				?>
				
				<form class="form-horizontal"  role="form" name="fCreditReqForm" id="fCreditReqForm">
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Name")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				    <input type="text" class="form-control" name="member_name" id="member_name" value="<?php echo $member_name ?>" required>
				 </div>
				 </div>
				 </div>
				 </div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Comments")?></label></div>
				 <div class="col-md-8">
				    <textarea class="form-control" id="request_comments" name="request_comments"></textarea>
				 </div>
				 </div>
				 </div>
				</div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				  <button class="btn btn-border btn-success" type="button" onclick='javascript:fCreditReqCtrl()' >
				   <i class="fa fa-check"></i> <?php echo _("Submit")?></button>
				   
				 </div>
				 <div class="col-md-6" id="rsDiv">
				 
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				
				
			 </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  <div class="pull-left" id="txt_id">&nbsp; </div>
				</div>
				<div class="col-md-6">
				  &nbsp;
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>