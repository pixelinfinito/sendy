<div class="modal fade" id="searchHistByDateAlert" tabindex="-1" role="dialog" 
     aria-labelledby="searchHistByDateLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Filter History")?><small class="text-info">(<?php echo _("by Date Range")?>)</small>
                </h4>
            </div>
			<div class="modal-body">

			 <script>
			  $(function(){
				 $('*[name=hist_fromdate]').appendDtpicker({
					"autodateOnStart": false
				});
				$('*[name=hist_todate]').appendDtpicker({
					"autodateOnStart": false
				});
			  });
			 </script>
			
				   <form action="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('hist')?>" method="post">
					<div class="row">
					<div class="col-md-12">
			     	  <div class="col-md-5"> 
					  <label><?php echo _("From")?></label>
					  <input type="text" name="hist_fromdate" id="hist_fromdate" class="form-control" placeholder="<?php echo _('e.g. 01-01-2016') ?>">
					  </div>
					  <div class="col-md-5">
					   <label><?php echo _("To")?></label>
					  <input type="text" name="hist_todate" id="hist_todate" class="form-control" placeholder="<?php echo _('e.g. 10-02-2016') ?>">
					  </div>
					  <div class="col-md-1">
					   <label>&nbsp;</label>
					  <button type="submit" name="hist_bydate" id="hist_bydate"  class="btn btn-success">
						<i class="fa fa-search"></i> 
					  </button>
					  </div> 
					  </div>
			        </div>
					  
				   </form>
				   <br><br>
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			    <div class="">
				<div class="col-md-6 text-left">
				 <small><em>* <?php echo _("Enter date range to generate history")?></em></small>
				</div>
				<div class="col-md-6">
				  &nbsp;
				</div>
				</div>
			</div>
        </div>
    </div>
</div>