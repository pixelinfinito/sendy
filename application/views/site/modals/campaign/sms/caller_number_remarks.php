<div class="modal fade" id="cnRemarksModal" tabindex="-1" role="dialog" 
     aria-labelledby="cnRemarksLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Reason")?></h4>
            </div>
			<div class="modal-body">
			  <div class="row">
			   <div class="col-md-6">
			   <h1 class="text-danger" ><?php echo _("Remarks")?></h1>
			   <textarea id="cn_remarks" class="form-control" rows="5"></textarea> 
			   </div>
			   
			   <div class="col-md-6">
			     <b><?php echo _("The remarks shared by system administrator.")?><br></b>
			     <?php echo _("Please contact our support team for further assistance.")?></div>
			   </div>
			
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			    <div class="row">
				<div class="col-md-8">
				 &nbsp;
				</div>
				<div class="col-md-4">
				 &nbsp;
				</div>
				</div>
			</div>
        </div>
    </div>
</div>