<div class="modal fade" id="modalApplySenderId" tabindex="-1" role="dialog" 
     aria-labelledby="ApplySenderIdLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("How to Apply SenderID")?></h4>
            </div>
			<div class="modal-body">
			  <div class="row">
			   <div class="col-md-7">
			        
			       <h4 class="text-success"><?php echo _("Proceed this Steps")?></h4>
				    <table class="table table-bordered">
					
					<tr>
					<td>1</td>
					<td>
				    <a href="javascript:void(0)" onclick="javascript:triggerBuyNumberModal()"><?php echo _("Buy a Caller Number")?></a> </li>
					</td>
					</tr>
					
					<tr>
					<td>2</td>
					<td>
					<?php echo _("Goto \"Caller Numbers\" tab and check the purchased caller number staus")?></td>
					</tr>
					
					<tr>
					<td>3</td>
					<td>
					<?php echo _("If caller number is approved (active) then you ready to \"Apply\" for SenderID")?></td>
					</tr>
					
					<tr>
					<td>4</td>
					<td>
					<?php echo _("Click on \"Apply\" button")?><a href="javascript:void(0)">
					<span class="expclr"><i class="fa fa-hand-stop-o"></i>
						<?php echo _("Apply")?></span>
					</a>, <?php echo _("a modal dialog will appear to enter your SenderID details (e.g. HELLOSMS)")?></td>
					</tr>
					
					<tr>
					<td>5</td>
					<td>
				    <?php echo _("Check the SenderID availability and click on")?><a href="#" class="btn btn-border btn-xs btn-success"><i class="fa fa-hand-stop-o"></i> <?php echo _("Request")?></a> <?php echo _("button")?></td>
					</tr>
					
					<tr>
					<td>6</td>
					<td>
					<?php echo _("Upon your request SenderID will be approved within certain timeperiod")?></td>
					</tr>
					
					<tr>
					<td>7</td>
					<td>
					<?php echo _("Check \"Caller Number\" tab again to check requested \"SenderID\" status")?></td>
					</tr>
					
					<tr>
					<td>8</td>
					<td>
					<?php echo _("Once your SenderID turns to green color (i.e approved) then you ready to use")?></td>
					</tr>
					
					<tr>
					<td>9</td>
					<td>
					<?php echo _("Goto \"New Campaign\" and select your SenderID in \"From\" select box")?></td>
					</tr>
					
					<tr>
					<td>10</td>
					<td>
					<?php echo _("Don't forget to check \"<i class=\"fa fa-check-square-o\"></i> Use My SenderID\" option (if you want campaign using SenderID")?>)</td>
					</tr>
					
				   </table>
				  
			   </div>
			   <div class="col-md-5">
			     <?php 
				 $defaultSettings=$this->app_settings_model->get_sms_settings();
				 $appSettings= $this->app_settings_model->get_primary_settings();
				 ?>
				 
				 <div class="text-muted"><i class="fa fa-phone"></i> <?php echo _("Default sender number is")?></div>
				 <h2 class="text-muted"><?php echo $defaultSettings[0]->twilio_origin_number;?></h2>
				 
				 <div class="well text-muted">
				 <b class='text-danger'><?php echo _("Note1 ")?>:</b>
					 <?php echo sprintf(
					 	 _("If you process campaign using \"default sender settings\" all your recipients can see this number 
					 	 (or) %f, if you want to appear your \"company name\" (or) \"preferred country number\"
				         instead of %s, please follow the steps."),
						 "<span class='text-primary'>".$defaultSettings[0]->twilio_sender_id."</span>",
						 "<span class='text-primary'>".$defaultSettings[0]->twilio_origin_number."</span>"
					 )?>

				 </div>
				 
				 <div class="well text-muted">
				 <b class='text-danger'><?php echo _("Note2 ")?>:</b>
				 
					<?php echo sprintf(
						_("To apply SenderID you suppose to %s (from listed countries),since every SenderID linked with particular
							caller number. The caller number is nothing but a virtual private number (not a physical number), and those are generated
							by our core partners (telecom)."
						),
						'<a href="javascript:void(0)" onclick="javascript:triggerBuyNumberModal()"> ' . _('buy a caller number') . '</a>')?>
				 </div>
				 
				 <div class="text-muted"><i class="fa fa-support"></i> <?php echo _("For any assistance please contact our support team at")?><?php echo "<u class='text-primary'>".$appSettings[0]->default_support_email."</u>";?> <?php echo _("or call us at")?><?php echo "<b class='text-success'>".$appSettings[0]->default_support_numbers."</b>";?>
				 </div>
			  
			   </div>
			   </div>
			
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			    <div class="row">
				<div class="col-md-8">
				 &nbsp;
				</div>
				<div class="col-md-4">
				 &nbsp;
				</div>
				</div>
			</div>
        </div>
    </div>
</div>