<div class="modal fade" id="modalEditContact" tabindex="-1" role="dialog" aria-labelledby="smsEditContactLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="smsEditContactLabel" >
                    <?php echo _("Edit Contact")?> (<span id="ecnx_title"></span>)
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				<?php 
				$allCountries= $this->countries_model->list_countries();
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				$callCodes=$this->sms_model->list_smsprice_aivailable_countries();
				$remoteCountry=$this->config->item('remote_country_code');
				?>
				<form  name="editSMSContactForm" id="editSMSContactForm" ng-controller="editSMSContactFrm">
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Full Name")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				   <input type="hidden" class="form-control" name="ecnx_cid" id="ecnx_cid">
				   <input type="text" class="form-control" id="ecnx_name" name="ecnx_name" required>
				 </div>
				 </div>
				 </div>
				 </div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Mobile Number")?></label> <small class="red">*</small></div>
				 <div class="col-md-4">
				    <select class="form-control" id="ecnx_caller_code" name="ecnx_caller_code" required="required" onchange="etrCountry(this.value)">
				   <option value="">--<?php echo _("Calling Code")?>--</option>
				   <?php 
				    foreach($callCodes as $country){
						?>
						<option value="<?php echo $country->calling_code.'__'.$country->country_code;?>" <?php if($country->country_code==$remoteCountry){echo 'selected';}?>>
							<?php echo $country->country_name.'('.$country->calling_code.')'?>
						</option>
						<?php
					}
				   ?>
				  </select>
				 </div>
				 <div class="col-md-4">
				 <input type="text" class="form-control"  name="ecnx_mobile" id="ecnx_mobile" placeholder="<?php echo _('E.g. 885xxxx') ?>" required>
				 </div>
				 
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Email ID")?></label></div>
				 <div class="col-md-8">
				 <input type="email" class="form-control" name="ecnx_email" id="ecnx_email"  placeholder="<?php echo _('E.g. dname@domain.com') ?>">
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Contact Group")?></label> <small class="red">*</small></div>
				 <div class="col-md-8"> 
				 
				  <select class="form-control" id="ecnx_contact_group" name="ecnx_contact_group" required>
				   <option value="">--<?php echo _("Select")?>--</option>
				    <?php 
					foreach($allGroups as $group){
						
						?>
						<option value="<?php echo $group->cgroup_id;?>">
						<?php echo ucfirst(strtolower($group->group_name));?>
						</option>
						<?php
					}
				   ?>
				  </select>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Country")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				  <select class="form-control" id="ecnx_country" name="ecnx_country" required="required" disabled>
				   <option value="">--<?php echo _("Select")?>--</option>
				   <?php 
				    foreach($callCodes as $country){
						?>
						<option value="<?php echo $country->country_code;?>" <?php if($country->country_code==$remoteCountry){echo 'selected';}?>>
							<?php echo $country->country_name;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				 </div>
				 </div>
				 </div>
				</div>
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				
				 
				 <input type="button" class="btn btn-border btn-success" value="Update" ng-click="updateSMSContact()">
				 </div>
				 <div class="col-md-6" id="dyUSMSProgress">
				 
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				
         
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div id="ecnx_error" class="pull-left">
				<strong><?php echo _("Note")?>:</strong> <?php echo _("Mobile number should not enter multiple times.")?></div>
				<small class="red">* <?php echo _("Mandatory Fields")?></small>
            </div>
       
    </div>
</div>
</div>
</div>