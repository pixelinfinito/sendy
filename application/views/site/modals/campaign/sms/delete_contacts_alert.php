<div class="modal fade" id="delContactsAlert" tabindex="-1" role="dialog" 
     aria-labelledby="delContactsLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Confirm Alert")?></h4>
            </div>
			<div class="modal-body">
			  <div class="row">
			   <div class="col-md-6">
			     <h1 class="text-danger"><?php echo _("Are you sure ?")?></h1>
				 <div><?php echo _("You want to delete selected contacts")?></div> <br><br>
			   </form>
			   
			   </div>
			   <div class="col-md-6">
			     <b> <?php echo _("Total Selected Contacts")?></b>
				 <h1 id="del_contacts"></h1>
			   </div>
			   </div>
			
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			    <div class="row">
				<div class="col-md-7">
				 <div id="del_status" class="pull-left"></div>
				</div>
				<div class="col-md-5">
				<button type="button" data-dismiss="modal" class="btn btn-primary" id="del_confirm"><?php echo _("Yes")?></button>
				<button type="button" data-dismiss="modal" class="btn" id="del_cancel"><?php echo _("Cancel")?></button>
				</div>
				</div>
			</div>
        </div>
    </div>
</div>