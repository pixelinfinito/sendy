<div class="modal fade" id="mvContactsAlert" tabindex="-1" role="dialog" 
     aria-labelledby="mvContactsLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Move Contacts")?></h4>
            </div>
			<div class="modal-body">
			  <div class="row">
			   <div class="col-md-6">
			   <?php 
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $allGroups= $this->sms_model->get_groups_byuser($user_id);
			   ?>
			   <form  role="form" name="mvContactsForm" id="mvContactsForm">
			   <div class="form-group">
			   <div class="row">
			     <div class="col-md-3"><?php echo _("Move To")?><small class="red">*</small></div>
				 <div class="col-md-9">
				  <select class="form-control" id="mvto_group" name="mvto_group"  required="required"> 
				   <option value="">--<?php echo _("Select")?>--</option>
				   
					 <?php 
				    foreach($allGroups as $group){
						?>
						<option value="<?php echo $group->cgroup_id;?>"><?php echo ucfirst(strtolower($group->group_name));?></option>
						<?php
					}
				   ?>
				  </select>
				  <small class="text-muted"><?php echo _("Move to which group, please select destination group")?></small>
				 </div>
			   </div>
			   </div>
			   </form>
			   
			   </div>
			   <div class="col-md-6">
			     <b> <?php echo _("Total Selected Contacts")?></b>
				 <h1 id="mv_contacts"></h1>
			   </div>
			   </div>
			
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			    <div class="row">
				<div class="col-md-7">
				 <div id="mv_status" class="pull-left"></div>
				</div>
				<div class="col-md-5">
				<button type="button" data-dismiss="modal" class="btn btn-primary" id="mv_confirm"><?php echo _("Yes")?></button>
				<button type="button" data-dismiss="modal" class="btn" id="mv_cancel"><?php echo _("Cancel")?></button>
				</div>
				</div>
			</div>
        </div>
    </div>
</div>