<div class="modal fade" id="delMNotifyModal" tabindex="-1" role="dialog" 
     aria-labelledby="delMNotifyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Confirm Alert")?></h4>
            </div>
			<div class="modal-body">
			   <h1><?php echo _("Are you sure ?")?></h1>
			   <?php echo _("You want to delete this notification")?><br><br>
			    
			   <textarea id="dlx_notify" class="form-control"> </textarea><br>
			   <div id="callDelstatus"></div>
			 </div>
           
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			   <div class="row">
				<div class="col-md-6 text-left">
				  &nbsp;
				</div>
				<div class="col-md-6">
				<button type="button" data-dismiss="modal" class="btn btn-primary" id="delete"><?php echo _("Delete")?></button>
				<button type="button" data-dismiss="modal" class="btn"><?php echo _("Cancel")?></button>
				</div>
				</div>	
		    </div>
        </div>
    </div>
</div>