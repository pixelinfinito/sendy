<div class="modal fade" id="modalEditSMSGroup" tabindex="-1" role="dialog" aria-labelledby="smsEditGroupLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="smsEditGroupLabel">
                    <?php echo _("Edit Group")?> (<span id="ecnx_cggroup"></span>)
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				<?php 
					$allCountries= $this->countries_model->list_countries();
					$user_id = $this->session->userdata['site_login']['user_id'];
					$allGroups= $this->sms_model->get_groups_byuser($user_id);
				?>
				<form  name="editCgGroupForm" id="editCgGroupForm" ng-controller="editCgGroupFrm">
				<div class="form-group" >
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Group Name")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				   <input type="hidden" class="form-control" name="ecnx_groupid" id="ecnx_groupid">
				   <input type="text" class="form-control" id="ecnx_cg_groupname" name="ecnx_cg_groupname" required>
				 </div>
				 </div>
				 </div>
				 </div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				
				 
				 <input type="button" class="btn btn-border btn-success" value="Update" ng-click="updateSMSGroup()">
				 </div>
				 <div class="col-md-6" id="dyUSMSGroupProgress">
				 
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				
         
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div id="ecnx_cgprocess" class="pull-left">
				 
				</div> 
				<small class="red">* <?php echo _("Mandatory Fields")?></small>
            </div>
       
    </div>
</div>
</div>
</div>