<div class="modal fade" id="modalEditTempleteTitle" tabindex="-1" role="dialog" aria-labelledby="smsEditTTileLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="smsEditTTileLabel">
                    <?php echo _("Edit Title")?> (<span id="ecnx_tt_hcode"></span>)
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
				
				<form  name="editTemplateTitleForm" id="editTemplateTitleForm" ng-controller="editTemplateTitleFrm">
				<div class="form-group" >
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Template Code")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				   <input type="text" class="form-control" name="ecnx_tt_code" id="ecnx_tt_code" readonly>
				   
				 </div>
				 </div>
				 </div>
				 </div>
				 
				 <div class="form-group" >
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3 text-right"><label><?php echo _("Title")?></label> <small class="red">*</small></div>
				 <div class="col-md-8">
				   <textarea class="form-control" id="ecnx_tt_title" name="ecnx_tt_title" required></textarea>
				  
				 </div>
				 </div>
				 </div>
				 </div>
				
				
				<div class="form-group">
				 <div class="row">
				 <div class="col-md-12">
				 <div class="col-md-3"><label></label></div>
				 <div class="col-md-3">
				
				 
				 <input type="button" class="btn btn-border btn-success" value="Update" ng-click="updateTemplateTitle()">
				 </div>
				 <div class="col-md-6" id="dyEditTTProgress">
				 
				 </div>
				 </div>
				 </div>
				</div>
			
				</form>
				
         
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <div id="ecnx_tt_process" class="pull-left">
				 
				</div> 
				<small class="red">* <?php echo _("Mandatory Fields")?></small>
            </div>
       
    </div>
</div>
</div>
</div>