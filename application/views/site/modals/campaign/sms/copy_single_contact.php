<div class="modal fade" id="ccSingleModal" tabindex="-1" role="dialog" 
     aria-labelledby="ccSingleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="ccSingleLabel">
                  <?php echo _("Copy")?><span id="md_title"></span>
                </h4>
            </div>
			<div class="modal-body">
			   <?php 
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $allGroups= $this->sms_model->get_groups_byuser($user_id);
			   ?>
			   <form  ng-controller="copySingleCtrlFrm" role="form" name="ccSingleCtrlForm" id="ccSingleCtrlForm">
				<input type="hidden" id="mxc_id" name="mxc_id">
				<input type="hidden"  id="mxc_mobile" name="mxc_mobile">
				<input type="hidden"  id="mxc_name" name="mxc_name">
				<input type="hidden" id="mxc_email" name="mxc_email">
				<input type="hidden" id="mxc_ccode" name="mxc_ccode">
				
			   <div class="form-group" >
			   <div class="row">
			     <div class="col-md-3"><?php echo _("Current Group")?></div>
				 <div class="col-md-9"><strong id="md_group"></strong></div>
			   </div>
			   </div>
			   
			   <div class="form-group" >
			   <div class="row">
			     <div class="col-md-3"><?php echo _("Copy To")?><small class="red">*</small></div>
				 <div class="col-md-9">
				  <select class="form-control" id="to_group" name="cpto_group" ng-model="cpto_group" required="required" > 
				   <option value="">--<?php echo _("Select")?>--</option>
				   
					 <?php 
				    foreach($allGroups as $group){
						?>
						<option value="<?php echo $group->cgroup_id;?>"><?php echo ucfirst(strtolower($group->group_name));?></option>
						<?php
					}
				   ?>
				  </select>
				 </div>
			   </div>
			   </div>
			  
			   <div class="form-group">
				 <div class="row">
				 <div class="col-md-3"><label>&nbsp;</label></div>
				 <div class="col-md-3">
				 <button class="btn btn-border btn-success" type="button" ng-click='copyCCSingleBtn()' >
				 <i class="fa fa-copy"></i> <?php echo _("Copy")?></button>
				 </div>
				 <div class="col-md-6" id="ccStatus">
				
				 </div>
				 </div>
				</div>
				
			   </form>
			 </div>
           
            
            <!-- Modal Footer -->
            <div class="modal-footer">
				<small>*<?php echo _("Mandatory fields")?></small>
			</div>
        </div>
    </div>
</div>