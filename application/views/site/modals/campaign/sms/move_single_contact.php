<div class="modal fade" id="mvSingleModal" tabindex="-1" role="dialog" 
     aria-labelledby="mvSingleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="mvSingleLabel">
                  <?php echo _("Move")?><span id="mv_md_title"></span>
                </h4>
            </div>
			<div class="modal-body">
			    
				<?php 
				$user_id = $this->session->userdata['site_login']['user_id'];
				$allGroups= $this->sms_model->get_groups_byuser($user_id);
				//$dy_contact_info=$this->sms_model->get_single_contact_bycid($contact_id,$user_id);
				?>
				<form  ng-controller="mvSingleCtrlFrm" role="form" name="mvSingleCtrlForm" id="mvSingleCtrlForm">
					<input type="hidden" id="mv_mxc_id" name="mv_mxc_id">
					<input type="hidden"  id="mv_mxc_mobile" name="mv_mxc_mobile">
					<input type="hidden"  id="mv_mxc_name" name="mv_mxc_name">
					<input type="hidden" id="mv_mxc_email" name="mv_mxc_email">
					<input type="hidden" id="mv_mxc_ccode" name="mv_mxc_ccode">

					<div class="form-group" >
					<div class="row">
					<div class="col-md-3"><?php echo _("Current Group")?></div>
					<div class="col-md-9"><strong id="mv_md_group"></strong></div>
					</div>
					</div>

					<div class="form-group" >
					<div class="row">
					<div class="col-md-3"><?php echo _("Move To")?><small class="red">*</small></div>
					<div class="col-md-9">
					<select class="form-control" id="to_group" name="to_group" ng-model="to_group" required="required" > 
					<option value="">--<?php echo _("Select")?>--</option>

					<?php 
					foreach($allGroups as $group){
					?>
					<option value="<?php echo $group->cgroup_id;?>"><?php echo ucfirst(strtolower($group->group_name));?></option>
					<?php
					}
					?>
					</select>
					</div>
					</div>
					</div>

					<div class="form-group">
					<div class="row">
					<div class="col-md-3"><label>&nbsp;</label></div>
					<div class="col-md-3">
					<button class="btn btn-border btn-success" type="button" ng-click='mvSingleCBtn()' >
					<i class="fa fa-exchange"></i> <?php echo _("Move")?></button>
					</div>
					<div class="col-md-6" id="mvStatus">
					</div>
					</div>
					</div>
				</form>		   
			   
			 </div>
           
            <!-- Modal Footer -->
            <div class="modal-footer">
				<small>*<?php echo _("Mandatory fields")?></small>
			</div>
        </div>
    </div>
</div>