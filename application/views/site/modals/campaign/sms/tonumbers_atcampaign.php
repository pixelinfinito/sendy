<div class="modal fade" id="modalNumberInfo" tabindex="-1" role="dialog" 
     aria-labelledby="NumberInfoLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("To Numbers")?></h4>
            </div>
			<div class="modal-body">
			 <?php $appSettings= $this->app_settings_model->get_primary_settings();?>
			  <div class="row">
			   <div class="col-md-6">
			     <h4 class="text-primary"><?php echo _("Suggestions to enter numbers")?></h4>
				  <?php echo _("Every camapgin is required recipient's list (i.e receiver numbers) here you can enter multiple contacts numbers
				  seperated by (,) comma , but how many number you type manually?
				  ")?><br><br>
				  
				  <?php echo _("To avoid this we introduced contact list/ groups concepts, i.e you can select contacts from \"contact list\"
				  (or) you can select contacts from groups, its very easier way to proceed campaign.
				  ")?><br><br>
				  
				  <b><?php echo _("What is a group? Why you need to use group?")?></b><br>
				  <?php echo _("Creating group is upto you, when you have more contacts (e.g 10000) with different countries numbers its difficult 
                  you to find contact. So split your contacts followed by groups (e.g India Group, Singapore Group) and select these 
                  groups when you compose a new text message.				  
				  
			   ")?></div>
			   
			   <div class="col-md-6">
			    
				 <h4><?php echo _("Group Limit")?></h4>
				 <div class="well text-muted">
				  <?php echo _("If you wish to campaign using group (contacts) you may select upto 50 groups, each group you can have 
				  unlimited numbers.
				 ")?></div>
				<div class="text-muted"><i class="fa fa-support"></i> <?php echo _("For any assistance please contact our support team at")?><?php echo "<u class='text-primary'>".$appSettings[0]->default_support_email."</u>";?> <?php echo _("or call us at")?><?php echo "<b class='text-success'>".$appSettings[0]->default_support_numbers."</b>";?>
				 </div>
				 
			   </div>
			   </div>
			
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			    <div class="row">
				<div class="col-md-8">
				 &nbsp;
				</div>
				<div class="col-md-4">
				 &nbsp;
				</div>
				</div>
			</div>
        </div>
    </div>
</div>