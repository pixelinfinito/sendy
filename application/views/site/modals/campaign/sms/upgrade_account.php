
 <?php 
 $user_id=$this->session->userdata['site_login']['user_id'];
 $userInfo= $this->account_model->list_current_user($user_id);
 $account_classes= $this->account_model->list_account_classes_notById($userInfo[0]->ac_class);
 $settingsInfo= $this->app_settings_model->get_site_settings();
 $remoteCountry=$this->config->item('remote_country_code');
 $currencyInfo= $this->countries_model->get_currencies_cCode($remoteCountry);


 ?>
 <!-- Modal -->
<div class="modal fade" id="modalConfirmUpgrade" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    <?php echo _("Upgrade To")?></h4>
            </div>
            
			<!-- Modal Body -->
			<div class="modal-body">
				<form role="form" action="<?php echo base_url()?>site/upgrade" method="get">
				<div class="row">
				<div class="col-sm-12">
				<div class="form-group">
				<label><?php echo _("Choose Service")?></label>
				<select class="form-control" id="sToken" name="sToken">

				<?php 
				for($tp=0;$tp<count($account_classes);$tp++){
				?>
				<option value="<?php echo base64_encode($account_classes[$tp]->acc_cost.'-'.$account_classes[$tp]->item_number.'-'.$account_classes[$tp]->acc_type);?>">
				<?php echo _("Item") . " [".$account_classes[$tp]->item_number."] ".$account_classes[$tp]->acc_type." ". _("Services"). "  [".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.' '.$account_classes[$tp]->acc_cost."] ";?>
				</option>
				<?php
				}  
				?>
				</select>
				</div>
				</div>
				</div>

				<label><?php echo _("Payment Method")?></label>
				<div class="row">
				<div class="col-sm-12">

				<?php 
				$p_card   =@$settingsInfo[0]->allow_card;
				$p_wallet =@$settingsInfo[0]->allow_wallet;
				$p_pp     =@$settingsInfo[0]->allow_paypal;

				if($p_card!='0'){
				?>
				<div class="col-md-5" title="<?php echo _('Credit/Debit Card') ?>">
				<input type="radio" name="pay_methods" id="pay_methods" value="card">
				<img src="<?php echo base_url()?>theme4.0/client/images/payment/cards.jpg" alt="Credit/Debit Cards"> 
				</div>

				<?php
				}
				if($p_wallet!='0'){
				?>
				<div class="col-md-2" title="<?php echo _('My Wallet') ?>">
				<input type="radio" name="pay_methods" id="pay_methods" value="wallet">
				<img src="<?php echo base_url()?>theme4.0/client/images/payment/wallet.png" > 
				</div>

				<?php
				}
				if($p_pp!='0'){
				?>
				<div class="col-md-2" title="<?php echo _('Paypal') ?>">
				<input type="radio" name="pay_methods" id="pay_methods" value="pp">
				<img src="<?php echo base_url()?>theme4.0/client/images/payment/paypal.jpg" alt="Paypal">
				</div>

				<?php
				}
				?>
				</div>
				</div>
				<br>
				<div class="row">
				<div class="col-sm-4">
				<div class="form-group">
				<button type="submit" class="btn btn-primary"><i class="fa fa-share"></i> <?php echo _("Proceed")?></button>
				</div>
				</div>
				</div>
				</form>

			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <small><?php echo _("Please choose the service option and proceed")?></small>
            </div>
        </div>
    </div>
</div>