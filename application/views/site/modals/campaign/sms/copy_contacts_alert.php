<div class="modal fade" id="cpContactsAlert" tabindex="-1" role="dialog" 
     aria-labelledby="cpContactsLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Copy Contacts")?></h4>
            </div>
			<div class="modal-body">
			  <div class="row">
			   <div class="col-md-7">
			   <?php 
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $allGroups= $this->sms_model->get_groups_byuser($user_id);
			   ?>
			   <form  role="form" name="cpContactsForm" id="cpContactsForm">
			   <div class="form-group">
			   <div class="row">
			     <div class="col-md-3"><?php echo _("Copy To")?><small class="red">*</small></div>
				 <div class="col-md-9">
				  <select class="form-control" id="cpto_group" name="cpto_group"  required="required"> 
				   <option value="">--<?php echo _("Select")?>--</option>
				   
					 <?php 
				    foreach($allGroups as $group){
						?>
						<option value="<?php echo $group->cgroup_id;?>"><?php echo ucfirst(strtolower($group->group_name));?></option>
						<?php
					}
				   ?>
				  </select>
				  <small class="text-muted"><?php echo _("Copy to which group, please select destination group")?></small>
				 </div>
			   </div>
			   </div>
			   </form>
			   
			   </div>
			   <div class="col-md-5">
			     <b> <?php echo _("Total Selected Contacts")?></b>
				 <h1 id="tt_contacts"></h1>
			   </div>
			   </div>
			
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			    <div class="row">
				<div class="col-md-7">
				 <div id="cp_status" class="pull-left"></div>
				</div>
				<div class="col-md-5">
				<button type="button" data-dismiss="modal" class="btn btn-primary" id="cp_confirm"><?php echo _("Confirm")?></button>
				<button type="button" data-dismiss="modal" class="btn" id="mv_cancel"><?php echo _("Cancel")?></button>
				</div>
				</div>
			</div>
        </div>
    </div>
</div>