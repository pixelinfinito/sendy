<div class="modal fade" id="cnfTranferAlert" tabindex="-1" role="dialog" 
     aria-labelledby="cnfTransferLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title">
                  <?php echo _("Confirm Alert")?></h4>
            </div>
			<div class="modal-body">
			  <div class="row">
			   <div class="col-md-6">
			   <h1 class="text-primary"><?php echo _("Are you sure ?")?></h1>
			    <?php echo _("You want to transfer funds.")?><br><br>
			   </div>
			   
			   <div class="col-md-6">
			       <b><?php echo _("Amount you willing to transfer")?></b>
				   <h4 class="text-success" id="amt_tag"></h4>
			   </div>
			   </div>
			
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
			    <div class="row">
				<div class="col-md-8">
				  &nbsp;
				</div>
				<div class="col-md-4">
				<button type="button" data-dismiss="modal" class="btn btn-primary" id="cnf_confirm"><?php echo _("Yes")?></button>
				<button type="button" data-dismiss="modal" class="btn" id="cnf_cancel"><?php echo _("Cancel")?></button>
				</div>
				</div>
			</div>
        </div>
    </div>
</div>