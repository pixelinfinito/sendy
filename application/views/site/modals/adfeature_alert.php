<div class="modal fade" id="AdFeatureAlertModal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                   <?php echo _("Wallet Alert")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
             <div id="dyWalletDiv">
			 
			 </div>
			</div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <small>* <?php echo _("Please check your wallet balance before proceed.")?></small>
            </div>
        </div>
    </div>
</div>