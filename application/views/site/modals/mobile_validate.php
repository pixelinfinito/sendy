<?php 
$smsSettings=$this->app_settings_model->get_sms_settings();

if(@$smsSettings[0]->password_resend_interval!=''){
	$waitingTime=$smsSettings[0]->password_resend_interval;	
}else{
	$waitingTime='60';
}
?>
<script type="text/javascript">
	// set minutes
	var mins = 1;
	 
	// calculate the seconds (don't change this! unless time progresses at a different speed for you...)
	var secs = mins * <?php echo $waitingTime ?>;
	function countdown() {
	setTimeout('Decrement()',1000);
	}
	function Decrement() {
	if (document.getElementById) {
	minutes = document.getElementById("minutes");
	seconds = document.getElementById("seconds");
	// if less than a minute remaining
	if (seconds < 59) {
	seconds.value = secs;
	} else {
	minutes.value = getminutes();
	seconds.value = getseconds();
	}
	secs--;
	setTimeout('Decrement()',1000);
	}
	}
	function getminutes() {
	// minutes is seconds divided by 60, rounded down
	mins = Math.floor(secs / 60);
		if(mins==0&&secs==0)
		{
			document.getElementById("dispRCode").style.display="block"; 
			document.getElementById("timer").style.display="none"; 
			return false;
		}
		else{
			return mins;
		}
	}
	function getseconds() {
	// take mins remaining (as seconds) away from total seconds remaining
	return secs-Math.round(mins *60);
	}
	
	function resendValidateForm(){
	   
	
		$("#reloadFormDiv").html('<i class="fa fa-spinner fa-spin"></i> <small><?php echo _("Please wait..")?></small>');
		$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>site/sms/resend_mobile_validate",
		data: { token: 'resend'}
		})
		.done(function( msg ) {
			
		 $('#reloadFormDiv').html(msg);
		
		});
		
 }
 function triggerTwofactorCode(){
		
		var method=$('input:radio[name=method]:checked').val();
        var mobile=$('#tel_number').val();
		$("#reloadFormDiv").html('<i class="fa fa-spinner fa-spin"></i> <small><?php echo _("Please wait..")?></small>');
		$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>site/sms/generate_twofactor_token",
		data: { method: method,recipient:mobile}
		})
		.done(function( msg ) {
			
		 $('#reloadFormDiv').html(msg);
		
		});
		
 }
 function stageTriggerTwofactorCode(){
	 
	    var method=$('input:radio[name=method]:checked').val();
        var mobile=$('#tel_number').val();
		alert(mobile);
		$("#reloadFormDiv").html('<i class="fa fa-spinner fa-spin"></i> <small><?php echo _("Please wait..")?></small>');
		$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>site/sms/generate_twofactor_token",
		data: { method: method,recipient:mobile}
		})
		.done(function( msg ) {
			alert(msg);
		 $('#reloadFormDiv').html(msg);
		
		});
 }
 function triggerCodeValidate(){
		
		var token=$('#parseAuthkey').val();
       
        if(token==''){alert('Please enter the password');return false;}
		
		$("#reloadFormDiv").html('<i class="fa fa-spinner fa-spin"></i> <small><?php echo _("Please wait..")?></small>');
		$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>site/sms/validate_twofactor_token",
		data: { token: token}
		})
		.done(function( msg ) {
			
		 $('#reloadFormDiv').html(msg);
		
		});
		
 }
</script>
<div class="modal fade" id="MobileValidateModal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                   <?php echo _("Validate Mobile")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body" id="reloadFormDiv">
			 <?php 
		    $user_id=$this->session->userdata['site_login']['user_id'];
		    $userInfo=$this->account_model->list_current_user($user_id);
			
			$username=$userInfo[0]->ac_first_name.' '.$userInfo[0]->ac_last_name;
			$mobile_parse=$userInfo[0]->ac_phone;
			$mobile_1=substr($mobile_parse,0,3);

			if($mobile_1=='673'){
				$mobile='+'.$mobile_parse;
			}
			else{
				$mobile='+673'.$mobile_parse;
			}

			
	        $validStatus=$this->twilio_model->get_mobactive_status($user_id);
			
			if($validStatus=='0')
			{
			  
				$validStatus2=$this->twilio_model->get_mobinactive_status($user_id);
				@$parseRowId=$validStatus2[0]->id ;
				if(@$validStatus2[0]->user_id ==''){
					$authStatus='NotRecordedYet';
				}
				else{
					$authStatus='NotVerified';
				}
			  
			}
			else
			{
			   
			    $authCode=$validStatus[0]->verification_code;
				$authStatus='Verified';
				$parseRowId=$validStatus[0]->id;
                $actStatus=$validStatus[0]->status;    				
			  
			}
  

			if($authStatus=='NotVerified'){
            /*if(isset($_SESSION['password'])){*/
          
			
            ?>
			<form id="passAuthKey" name="passAuthKey"  method="post">
				<div class="row">
				<div class="col-md-12">
				<div class="row">
				<div class="col-md-6  text-center">
					<div class="well"><h1> <i class="fa fa-user"></i></h1> 
					<?php  echo "<p><b>Hi " . $username."</b>"; ?>
					</div>
					<?php 
					echo " Please ensure that your mobile device is available in proper singals.</p>";
					?>
				</div>


				<div class="col-md-6">
				<div class="form-group">
					<div>
					<label> <?php echo _("Enter Authentication Password")?></label>
					<input type="text" name="parseAuthkey" id="parseAuthkey" placeholder="<?php echo _('e.g b405xxxxxx') ?>" class="form-control" required> 
					</div>
				</div>
				
				<div class="form-group">
					<label> <?php echo _("Resend Password Waiting Time")?></label>
					<div id="timer">
					<div class="col-md-6 no-padding">
					<input id="minutes" type="text" style="width:100px" class="form-control"> <?php echo _("Minutes")?></div>
					<div class="col-md-6 no-padding">
					<input id="seconds" type="text" style="width:100px" class="form-control"> <?php echo _("Seconds.")?></div>
					</div>
					<span  id="dispRCode" style="display:none">
					<a href="javascript:void(0)" onclick="javascript:resendValidateForm()">
					<i class="fa fa-key"></i> <?php echo _("Resend Password")?></a>
					</span>
					<script>
					countdown();
					</script>
				</div>
				<br>
				<div class="form-group">
					<div>
					<button type="button" name="submitAuthKey" id="submitAuthKey"  class="btn btn-warning btn-border" onclick="javascript:triggerCodeValidate()">
					<i class="fa fa-check"></i> <?php echo _("Validate")?></button>
					</div>
				</div>

				</div>
				</div>
				</div>
				</div>
			</form>
			
            <?php	
             }
            			 
			// }
			else if($authStatus=='Verified')
			{
				echo "<h4 class='text-success'><i class='fa fa-check'></i>&nbsp; " . _('You mobile is already verified.') . "</h4>";
			} 
			else 
			{
			?>
           
		
			<form id="generateTwoFA"  method="POST" class="center">
				<div class="row">
				<div class="col-md-12">
				<div class="row">
				<div class="col-md-6  text-center">
					<div class="well"><h1> <i class="fa fa-user"></i></h1> 
					<?php  echo "<p><strong>Hi " . $username; ?>
					</div>
					<?php 
					echo " </strong>" . _('Please choose your option to deliver password and click on receive.') . "</p>";
					?>
				</div>

				<div class="col-md-6">
				<div class="form-group">
					<label><?php echo _("Mobile Owner")?></label>
					<input type="text" name="userName" id="userName" required="required" value="<?php echo $username;?>" class="form-control">
				</div>
				<div class="form-group">
					<label><?php echo _("Mobile Number")?></label>
					<input type="tel" name="tel_number" id="tel_number" required="required" class="form-control" value="<?php echo $mobile;?>">
				</div>

				<div class="form-group">
					<label><?php echo _("Deliver Password Via")?></label>
					<div>
					<input type="radio" name="method" value="sms" checked>&nbsp; <?php echo _("SMS")?>&nbsp;&nbsp;
					<input type="radio" name="method" value="voice">&nbsp; <?php echo _("Voice Call")?></div>
				</div>

				<div class="form-group">
					<button type="button" onclick="javascript:stageTriggerTwofactorCode()" class="btn btn-success btn-border">
					<i class="fa fa-share"></i> <?php echo _("Receive")?></button>
				</div>
				</div>
				</div>
				</div>
			</form>
		</div>
       <?php
	    }
	   ?>
		
		</div>
		<!-- Modal Footer -->
		<div class="modal-footer">
			<div class="text-left">
			<small class='text-muted'>
			<?php 
			   $intlib=$this->internal_settings->local_settings();
			   echo '<i class="fa fa-question-circle"></i> '.$intlib[0]->help_text.' <b>(' . _("or") . ')</b> ' . _('call us at') . ' '.$intlib[0]->default_support_numbers;
			  ?>
			</small>
			</div>
		</div>
       
 </div>
    </div>
</div>