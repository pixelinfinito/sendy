<?php $this->load->view('themeFront/header_logged.php');
//$this->load->view('themeFront/banner_intro.php');


$remoteCountry=$this->config->item('remote_country_code');
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);

$user_id = $this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);
$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);
 

$paymentSettings=$this->app_settings_model->get_payment_settings();
$settingsInfo= $this->app_settings_model->get_site_settings();


$walletInfo= $this->account_model->get_account_wallet($user_id);
 
 $wallet_min_val=@$settingsInfo[0]->wallet_min_val;
 $wallet_max_val=@$settingsInfo[0]->wallet_max_val;
 $wallet_string=@$settingsInfo[0]->wallet_string;
 
 $walletCode=$wallet_string.rand($wallet_min_val,$wallet_max_val);
 if($walletCode!=''){
	 $wItemCode=$walletCode;
 }else{
	 $wItemCode='OCWL'.rand(500,5000);
 }
 
///// checking existing card 
$card_info=$this->wallet_model->get_card_information($user_id);
if($card_info!=0){
	
	 $mcard_name=$card_info[0]->card_holder_name;
	 $mcard_number=$card_info[0]->card_number;
	 $mcard_cvv=$card_info[0]->card_cvv;
	 $mcard_month=$card_info[0]->card_expire_month;
	 $mcard_year=$card_info[0]->card_expire_year;
	 $mcard_add1=$card_info[0]->billing_street1;
	 $mcard_add2=$card_info[0]->billing_street2;
	 $mcard_city=$card_info[0]->billing_city;
	 $mcard_country=$card_info[0]->billing_country;
	 $mcard_postal=$card_info[0]->billing_postalcode;
	 

} else{
	
	 $mcard_name='';
	 $mcard_number='';
	 $mcard_cvv='';
	 $mcard_month='';
	 $mcard_year='';
	 $mcard_add1='';
	 $mcard_add2='';
	 $mcard_city='';
	 $mcard_country='';
	 $mcard_postal='';
	 
}

?>
<style>
.nopadding {
   padding: 0 !important;
   margin: 0 !important;
}
.lpadding1{padding-left:2px;}
.bottompad10{border-bottom:1px dashed #CCC;margin-bottom:15px}
</style>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>


 $(function(){
	 var flag = false;
	 
     $('#ba_link').click(function(){
		
		 if( flag == false){
		  $("#captured_billing_address").slideToggle();
		   $(this).html('<i class="fa fa-institution"></i> <?php echo _("Billing Address")?><i class="fa fa-caret-up"></i>');
		   flag = true;
		}
		else{
		   $("#captured_billing_address").slideToggle();
		   $(this).html('<i class="fa fa-institution"></i> <?php echo _("Billing Address")?><i class="fa fa-caret-down"></i>');
		   flag = false;
		}
	
		
	 });

	 $('#card_payment').slideDown();
	$('input').on('change', function() {
       var pMethod=$('input[name=pay_methods]:checked', '').val();
	   
	   if(pMethod=='pp'){
		   
		   $('#pp_payment').slideDown();
		   $('#card_payment').slideUp();
	   }
	   else if(pMethod=='card'){
		   
		   $('#card_payment').slideDown();
		   $('#pp_payment').slideUp();
	   }
    });
	
	
	
 });
 

</script> 
<div class="main-container">
    <div class="container">
	  <div class="row">
      <div class="col-md-6">
      <ol class="breadcrumb pull-left">
        <li><a href="<?php echo base_url()?>site/home"><i class="icon-home fa"></i></a></li>
        <li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
		<li><a href="<?php echo base_url()?>site/wallet/history"><?php echo _("Wallet")?></a></li>
        <li><a href="<?php echo base_url()?>site/wallet/add"><?php echo _("Add Funds")?></a></li>
      </ol>
     </div>
	 <div class="col-md-6 text-right">
	   <?php 
	   /// custom library
	   $intlib=$this->internal_settings->local_settings();
	   echo '<i class="fa fa-question-circle"></i> '.$intlib[0]->help_text;
	  ?>
	 </div>
	</div>
	 
	   <div class="row" ng-app="">
	   <div class="col-md-8">
          <div class="card-content">
           <h4 class="title-2"><i class="fa fa-dollar"></i> <?php echo _("Add Funds")?></h4>
			 <div class="row">
             
            <div class="">
			<div class="row">
			
				<div class="col-md-3 text-right">
				   <b><?php echo _("Payment Method")?></b>
				</div>
		   <?php 
				$p_card   =@$settingsInfo[0]->allow_card;
				$p_pp     =@$settingsInfo[0]->allow_paypal;
				
				if(@$p_card!='0'){
					?>
					
					<div class="col-md-6 text-left" title="<?php echo _('Credit/Debit Card') ?>">
					<input type="radio" name="pay_methods" id="pay_methods" value="card" checked>
					<img src="<?php echo base_url()?>theme4.0/client/images/payment/cards.jpg" alt="Credit/Debit Cards"> 
					</div>
					
					<?php
				}
				
				if(@$p_pp!='0'){
					?>
					<div class="col-md-3 text-left" title="<?php echo _('Paypal') ?>">
					<input type="radio" name="pay_methods" id="pay_methods" value="pp">
					<img src="<?php echo base_url()?>theme4.0/client/images/payment/paypal.jpg" alt="Paypal">
					</div>
					
					<?php
				}
			   ?>
			
			</div>   
			
		<?php 
		//Set useful variables for paypal form
		$paypal_url = $paymentSettings[0]->pp_payment_url; //Test PayPal API URL
		$paypal_id = $paymentSettings[0]->pp_email; //Business Email
		?>	
       <div id="pp_payment" style="display:none">	
	     <hr>
	    <div class="row">
		<div class="col-md-3">
		 &nbsp;
		</div>
		
		<div class="col-md-6">
		<div class="text-center text-muted">
		<h4 class="text-danger"><i class="fa fa-meh-o"></i> <?php echo _("Coming soon..")?></h4>
		 <?php echo _("Sorry for the inconvience this feature is currently offline, we are expanding region ranges at this moment.")?></div>
		</div>
		
		<div class="col-md-3">
		 &nbsp;
		</div>
		</div>
	   <!--<br>	   
    	<form action="<?php echo $paypal_url; ?>" method="post" role="form" class="form-horizontal">
		<div class="form-group required">
		  <label class="col-md-4 control-label" ><?php echo _("Wallet Code")?><sup>*</sup></label>
		  <div class="col-md-6">
			  <input  name="item_code" id="item_code" placeholder="<?php echo _('E.g. OCWL99002') ?>" class="form-control input-md" required="" type="text" value="<?php echo @$wItemCode;?>" readonly>
			  <small>(<?php echo _("System automated reference")?>)</small>
		  </div>
		</div>
		
		<div class="form-group required">
		  <label class="col-md-4 control-label" ><?php echo _("Amount")?><sup>*</sup></label>
		  <div class="col-md-6">
			  <input  name="amount" id="amount" placeholder="<?php echo _('E.g. 50') ?>" class="form-control input-md" required="" type="text" >
			  <small>(<?php echo _("How much you would like to topup")?>)</small>
		  </div>
		  
		</div>
				 
        <!-- Identify your business so that you can collect the payments. -->
        <!--<input type="hidden" name="business" value="<?php echo $paypal_id;?>">
        <!-- Specify a Buy Now button. -->
        <!--<input type="hidden" name="cmd" value="_xclick">
        <!-- Specify details about the item that buyers will purchase. -->
		<!--<input type="hidden" name="item_name" value="My Wallet">
        <input type="hidden" name="item_number" value="<?php echo @$wItemCode; ?>">
        <input type="hidden" name="currency_code" value="<?php echo @$currencyInfo[0]->currency_name;?>">
		<input type="hidden" name="payment_mode" id="payment_mode" value="pp">
		
		<!-- Specify URLs -->
        <!--<input type='hidden' name='cancel_return' value='<?php echo $paymentSettings[0]->pp_wallet_cancel_url;?>'>
        <input type='hidden' name='return' value='<?php echo $paymentSettings[0]->pp_wallet_return_url;?>'>
		 <div  class="form-group">
		<label class="col-md-4 control-label" ></label>
		<div class="col-md-6">
		<small>
		<input type="checkbox"> <?php echo _("By completing my purchase, I agree to the")?><a href="#"><?php echo _("Payment Terms, Terms of Use,")?></a> <?php echo _("and the")?><a href="#">
		<?php echo _("Privacy Statement.")?></a>
		</small>
		</div>
		 </div>
        <!--<!-- Display the payment button. -->
        <!--<div class="form-group form-group required">
	    <label class="col-md-4 control-label" ></label>
		<div class="col-md-6">
		<input type="submit" name="pay" id="pay" value="Submit Payment" class="btn btn-primary btn-border btn-flat">
        </div>
		</div>
    
		</form> -->
		
        <div>
        </div>
    </div>
		
		<div class="form-group required">
		  <div class="col-md-12">
		  <div id="card_payment" style="display:none">
		    <hr>
			<form id="myCCForm" action="<?php echo base_url()?>site/wallet/process" method="post" class="form-horizontal" >
			<br>
			<?php
			 if($mcard_number==''){
				 ?>
				<div class='col-md-12 text-center'>
				<b class='text-danger'>
				<i class='fa fa-exclamation-triangle'></i> <?php echo _("Please update your card information")?></b><a href="<?php echo base_url()?>site/payment/card"><?php echo _("by click here")?></a>
				</div>
				<br><br>
				<?php
			 }
			?>
			<div class="form-group">
                <label class="col-md-4 control-label" ><?php echo _("Amount")?><small class="red">*</small></label>
		        <div class="col-md-6">
                <input id="amount" name="amount" type="number"  min="5" data-number-to-fixed="2" data-number-stepfactor="100" size="20" autocomplete="off" required class="form-control" placeholder="<?php echo _('e.g. 50') ?>"/>
				<small class="text-muted">(<?php echo _("Amount you would like to topup, at minimum")?><?php echo $currencyInfo[0]->currency_name." 5"?>)</small>
				</div>
            </div>
			
			<?php 
			 if(@$currencyInfo[0]->currency_name!='')
			 {
				 $parseCurrency=$currencyInfo[0]->currency_name;
				
			}
		     else{
				 $parseCurrency='USD';
		     }
			?>
            <input id="token" name="token" type="hidden" value="">
			<input type="hidden" name="c2o_wallet_order" id="c2o_wallet_order" value="<?php echo @$wItemCode; ?>">
			<input type="hidden" name="c2o_currency_code" id="c2o_currency_code" value="<?php echo $parseCurrency;?>">
			<input type="hidden" name="wallet_tamt" id="wallet_tamt" value="<?php echo @$walletInfo[0]->total_amount;?>">
		    <input type="hidden" name="wallet_bamt" id="wallet_bamt" value="<?php echo @$walletInfo[0]->balance_amount;?>">
			<input type="hidden" name="payment_mode" id="payment_mode" value="card">
		    
			<div class="form-group">
                <label class="col-md-4 control-label"><?php echo _("Card Holder Name")?><small class="red">*</small></label>
		         <div class="col-md-6">
                <input id="c_holder_name" name="c_holder_name" type="text" class="form-control" placeholder="<?php echo _('e.g. FNAME LNAME') ?>" readonly value="<?php echo $mcard_name;?>" required />
				</div>
            </div>
			<div class="form-group">
                <label class="col-md-4 control-label" ><?php echo _("Card Number")?><small class="red">*</small></label>
		         <div class="col-md-6">
                <input id="ccNo" type="number" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" readonly size="20" value="<?php echo $mcard_number;?>" autocomplete="off" required class="form-control" placeholder="<?php echo _('e.g. 4XXX9XXX2XXX9999') ?>" required />
				</div>
            </div>
            <div class="form-group">
			 <label class="col-md-4 control-label" ><?php echo _("Expiration Date (MM/YYYY")?>)<small class="red">*</small></label>
                <div class="col-md-6">
				
				<div class="col-md-6 nopadding">
				  <select class="form-control" id="expMonth" disabled required>
				   <?php 
				    for($i=1;$i<=12;$i++){
						if($i<10){$v='0'.$i;}else{$v=$i;}
						?>
						<option value="<?php echo $v;?>" <?php if($v==$mcard_month){echo _("selected");}?>>
						<?php echo $v;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				</div>
				
				<div class="col-md-6 nopadding">
				 <select class="form-control" id="expYear" disabled required>
				   <?php 
				    for($j=2016;$j<=2030;$j++){
						
						?>
						<option value="<?php echo $j;?>" <?php if($j==$mcard_year){echo _("selected");}?>>
						<?php echo $j;?>
						</option>
						<?php
					}
				   ?>
				  </select>
				</div>
				</div>
            </div>
			
            <div  class="form-group">
                <label class="col-md-4 control-label"><?php echo _("CVV Code")?><small class="red">*</small></label>
                <div class="col-md-6">
                <input id="cvv" size="4" type="number" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" value="<?php echo $mcard_cvv;?>" autocomplete="off" required class="form-control" placeholder="<?php echo _('e.g. 123') ?>" />
				<a href="#" data-toggle="modal" data-target="#modalCvv" class="text-muted"><small><?php echo _("What is CVV ?")?></small></a>
				</div>
            </div>
			
			<div  class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-6">
                  <a href="javascript:void(0)" id="ba_link">
				  <i class="fa fa-institution"></i> <?php echo _("Billing Address")?><i class="fa fa-caret-down"></i>
				  </a>
				  <div id="captured_billing_address" style="display:none">
				    <?php 
					 if($mcard_add1!=''){
						 echo $mcard_add1."<br>";
						 echo $mcard_add2."<br>";
						 echo $mcard_city."<br>";
						 echo $mcard_country."<br>";
						 echo $mcard_postal."<br>";
					 }else{
						 echo "<b class='text-danger'>
						 <i class='fa fa-meh-o'></i> " . _("Please update billing address.") . "</b>";
					 }
					?>
				  </div>
				</div>
            </div>
			
			 <div  class="form-group">
                <label class="col-md-4 control-label" ></label>
				<div class="col-md-6">
                <small>
				<input type="checkbox" required> <?php echo _("By completing my purchase, I agree to the")?><a href="#"><?php echo _("Payment Terms, Terms of Use,")?></a> <?php echo _("and the")?><a href="#">
				<?php echo _("Privacy Statement.")?></a>
				</small>
                </div>
			 </div>
			 
			 <div  class="form-group">
                <label class="col-md-4 control-label"></label>
				<div class="col-md-3">
				
				 <?php 
				  if($user_id!='1000578dd5df29bdd'){
				  ?>
                <button type="submit" class="btn btn-success btn-border" id="cpBtn" name="cpBtn">
				 <i class="fa fa-share"></i> <?php echo _("Process Payment")?></button>
				  <?php 
				  }else{
					  echo "<h5 class='text-danger'>" . _('Demo Account (Restricted Feature)') . "</h5>";
				  }
				  ?>
			    </div>
				<div class="col-md-4 text-left">
				<br>
				<a href="<?php echo base_url()?>site/payment/card" class="text-muted">
				 <small><i class="fa fa-credit-card"></i> <?php echo _("Update Card")?></small>
				</a>
				</div>
			 </div>
			 <?php 
			 
			  if(@$paymentSettings[0]->tco_is_sandbox=='0'){
				  $sellerId=$paymentSettings[0]->tco_seller_id;
				  $publishKey=$paymentSettings[0]->tco_publish_key;
			      $payType='production';
			  }else{
				  $sellerId=$paymentSettings[0]->tco_seller_id;
				  $publishKey=$paymentSettings[0]->tco_publish_key;
			      $payType='sandbox';
			  }
			  
			 ?>
        </form>

        <script type="text/javascript" src="<?php echo base_url().'js/2co/2co.min.js'?>"></script>
        <script src="<?php echo base_url().'js/2co/jQuery.min.js'?>"></script>

        <script>
            // Called when token created successfully.
            var successCallback = function(data) {
                var myForm = document.getElementById('myCCForm');
                // Set the token as the value for the token input
                myForm.token.value = data.response.token.token;
                // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
                myForm.submit();
            };
            // Called when token creation fails.
            var errorCallback = function(data) {
                if (data.errorCode === 200) {
                    tokenRequest();
                } else {
                    alert(data.errorMsg);
                }
            };
            var tokenRequest = function() {
                // Setup token request arguments
                var args = {
                    sellerId: "<?php echo $sellerId;?>",
                    publishableKey: "<?php echo $publishKey;?>",
                    ccNo: $("#ccNo").val(),
                    cvv: $("#cvv").val(),
                    expMonth: $("#expMonth").val(),
                    expYear: $("#expYear").val()
                };
                // Make the token request
                TCO.requestToken(successCallback, errorCallback, args);
            };
            $(function() {
                // Pull in the public encryption key for our environment
                TCO.loadPubKey('<?php echo $payType;?>');
                $("#myCCForm").submit(function(e) {
					// Call our token request function
					//tokenRequest();
					// Prevent form from submitting
					//return false;
					$('#cnfPaymentAlert').modal('show');
					return false;
					
                });
				$('#cnf_confirm').click(function() {
							// Call our token request function
							tokenRequest();
							// Prevent form from submitting
							return false;
				});	
            });
        </script>
		  </div>
		</div>
		</div>
		  
	   </div>
	   </div>
	   </div>
	    </div>
	    <div class="col-md-4 reg-sidebar">
          <div class="reg-sidebar-inner">
		    
			  <div class="panel sidebar-panel"> 
			   <div class="panel-heading uppercase text-left">
					<span class="fa  fa-dollar"></span> <?php echo _("Available Credit")?></div>
			   
			   <div class="panel-body text-left">
               <div class="row">
              <div class="col-md-3 text-center">
			    <br><h1><i class="fa fa-google-wallet"></i></h1>
			  </div>
			  <div class="col-md-9 text-left">
			    <?php 
				echo "<small>".@$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.'</small><br>';
				if(@$walletInfo[0]->balance_amount!=''){
				?>
					<h1>
					<?php echo @$walletInfo[0]->balance_amount;?>
					</h1>
				<?php
				}else{
				?>
					<h1>
					<?php echo "0.00";?>
					</h1>
				<?php
				}
				?>
				<?php echo _("Check your")?><a href="<?php echo base_url()?>site/wallet/history"><?php echo _("transaction history")?></a>
			  </div>
              </div>
				<br>
			   </div>
		    </div>
			
            <div class="panel sidebar-panel"> 
			   <div class="panel-heading uppercase text-left">
					<span class="fa  fa-exchange"></span> <?php echo _("Recent Transaction")?></div>
			   <div class="panel-body text-left">
               <div class="row">
              <div class="col-md-3 text-center">
			    <br>
				<small><?php echo _("USD")?></small>
			    <h1>10</h1>
			  </div>
			  <div class="col-md-9 text-left">
			    <br>
			   <small> <b><?php echo _("Reference : WCWL4955")?></b></small><br>
			      <span class="text-primary"><?php echo _("Purchase of Caller number purchase")?></span> <br>
				 <small class="text-muted">2016-06-06 09:41:26 </small>
			   
			  </div>
              </div>
				<br>
			   </div>
		    </div>
			
			<?php
			 if($mcard_number==''){
				 ?>
				<div class='col-md-12 text-muted'>
				 <h4><i class='fa fa-exclamation-triangle'></i>&nbsp;  <?php echo _("Card Information")?></h4>
				 <?php echo _("Your card information is missing please update your card to make transactions")?><a href="<?php echo base_url()?>site/payment/card"><?php echo _("click here")?></a>
				</div>
				<br><br>
				<?php
			 }else{
				 ?>
				 <div class='col-md-12'>
				 <h4 class="text-primary"><i class='fa fa-check'></i>&nbsp;  <?php echo _("Card Information")?></h4>
				  <?php echo _("If you prefer to use other card, please update instead of present card")?><a href="<?php echo base_url()?>site/payment/card"><?php echo _("click here")?></a>
				</div>
				<br><br>
				 <?php
			 }
			?>
 
          </div>
        </div>
		
	  </div>
	 </div>
 </div>
 
<!-- /////////////// CVV Alert ///////////////-->
 
 <!-- Modal -->
<div class="modal fade" id="modalCvv" tabindex="-1" role="dialog" 
     aria-labelledby="myCvvLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only"><?php echo _("Close")?></span>
                </button>
                <h4 class="modal-title" id="myCvvLabel">
                   <?php echo _("What is CVV")?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
               <img src="<?php echo base_url()?>theme4.0/client/images/payment/cvv.jpg" alt="CVV">
		    </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <small><?php echo _("Please proceed with valid card")?></small>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('site/modals/campaign/sms/payment_confirm_alert'); ?>
<?php $this->load->view('themeFront/footer.php');?>



