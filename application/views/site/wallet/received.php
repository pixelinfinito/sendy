<?php $this->load->view('themeFront/header_logged.php');

$remoteCountry=$this->config->item('remote_country_code');
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);

$user_id = $this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);
$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);

$paymentSettings=$this->app_settings_model->get_payment_settings();
$walletInfo= $this->account_model->get_account_wallet($user_id);
$payments_received= $this->wallet_model->payments_received_bymember($user_id);
?>

<script src="<?php echo base_url()?>js/wallet/transfer.js"></script>
<link href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="main-container">
<div class="container">
	  <div class="row">
      <div class="col-md-6">
      <ol class="breadcrumb pull-left">
        <li><a href="<?php echo base_url()?>site/home"><i class="icon-home fa"></i></a></li>
        <li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
		<li><a href="<?php echo base_url()?>site/wallet/history"><?php echo _("Wallet")?></a></li>
        <li><a href="<?php echo base_url()?>site/wallet/received"><?php echo _("Received")?></a></li>
      </ol>
     </div>
	 <div class="col-md-6 text-right">
	    <?php 
				
			if($walletInfo[0]->balance_amount!=''){
				$accAmount=$walletInfo[0]->balance_amount;
				echo "Avaiailable balance <b>".$currencyInfo[0]->currency_name.' '.$accAmount."</b>, ";
				
			}else{
				$accAmount='0';
				echo "Avaiailable balance <b> ".$currencyInfo[0]->currency_name." 0.00</b>, ";
			
			}
			?>
			<?php echo _("Check")?><a href="<?php echo base_url()?>site/wallet/history"><?php echo _("transactions history")?></a>
	 </div>
	</div>
</div><hr>

<div class="container">
	   <div class="row">
	   <div class="col-md-4">
	   <div class="col-md-11  graybg">
	   <h4><?php echo _("Transfer Funds")?><br><small> (<?php echo _("by using this form")?>)</small></h4>
          <form id="transForm" name="transForm" class="no-padding">
		   <input type="hidden" value="<?php echo $accAmount?>" id="accAmount" name="accAmount">
		   <input type="hidden" value="<?php echo $currencyInfo[0]->currency_name?>" id="currencyTag" name="currencyTag">
		   
		   <div class="form-group">
                <div class="col-md-12">
				 <label><?php echo _("Amount")?><small class="red">*</small></label>
                <input id="amount" name="amount" type="number"  min="5" data-number-to-fixed="2" data-number-stepfactor="100" size="20" autocomplete="off" required class="form-control" placeholder="<?php echo _('e.g. 50') ?>"/>
				<small class="text-muted">(<?php echo _("How much would you like to transfer")?>)</small><br><br>
				</div>
            </div>
			
			<div class="form-group">
                <div class="col-md-12">
				 <label><?php echo _("Receiver Email")?><small class="red">*</small></label>
                <input id="recipient_email" name="recipient_email" type="email"  required class="form-control" placeholder="<?php echo _('e.g. abc@abc.com') ?>"/>
				<small class="text-muted">(<?php echo _("To whom you want to transfer")?>)</small><br><br>
				</div>
            </div>
			
			<div class="form-group">
			<div class="col-md-12">
			 <button type="button" name="transfer_btn" id="transfer_btn" class="btn btn-success"> 
			 <i class="fa fa-share"></i> <?php echo _("Transfer")?></button><br><br>
			 </div>
			</div>
		  </form>
		  
	   </div>
	   </div>
	    <div class="col-md-8">
       <h4><?php echo _("Received Funds")?></h4> <span id="rsTransfer"></span><br>
		   
	<table class="table table-bordered table-striped" id="dataTables-paymentHistory">
		<thead ><!-- Table head -->
		<tr>
		<th class="col-sm-1"><?php echo _("ID")?></th>
		<th class="col-sm-1"><?php echo _("Ref.No")?></th>
		<th class="col-sm-1"><?php echo _("Amount")?></th>
		<th class="col-sm-1"><?php echo _("Transaction ID")?></th>
		<th class="col-sm-1"><?php echo _("Response Code")?></th>
		<th class="col-sm-1"><?php echo _("Payment Datetime")?></th>
		<th class="col-sm-1"><?php echo _("Remarks")?></th>
		<th class="col-sm-1"><?php echo _("Invoice")?></th>
		<!--<th class="col-sm-1"><?php echo _("Actions")?></th>-->

		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		$gp = 1 
		?>
		<?php if ($payments_received!=0): foreach ($payments_received as $payment) : ?>

		<tr>
		<td>
		<?php echo $payment->acc_payment_id?>
		</td>
		
		<td>
		<?php echo $payment->acc_item_number?>
		</td>
		<td>
		<?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.$payment->c2o_total_amount?> 
		</td>
		
		<td>
		  <?php echo $payment->c2o_transaction_id ?>
		</td>
		<td>
		  <?php echo $payment->c2o_response_code ?>
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($payment->payment_datetime)); 
		?>
		</td>
		
		<td>
		     <?php 
			  echo $payment->c2o_response_msg;
			 ?>
             
         </td>
		 <td>
		     <?php 
			  switch($payment->payment_status){
				  case '1':
				  ?>
				  <a href="<?php echo base_url()?>site/export/payment_invoice?iv=<?php echo base64_encode($payment->acc_payment_id)?>" title="<?php echo _('Download Invoice') ?>"> <i class="fa fa-download"></i></a>
				  <a href="javascript:void(0)" onclick="javascript:printInvoice('<?php echo base64_encode($payment->acc_payment_id)?>')" title="<?php echo _('Print Invoice') ?>"> <i class="fa fa-print"></i></a>
				  <?php
				  break;
				  
				  case '0':
				  echo '-';
				  break;
			  }
			 ?>
             
         </td>
		</tr>
	<?php
	$gp++;
	endforeach;
	?>
	<?php else : ?>
	<td colspan="6" class="text-center">
	<h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
	</td>
	<?php endif; ?>
	</tbody><!-- / Table body -->
	</table> <!-- / Table -->
			
	</div>
	</div>
</div>
</div>

<script src="<?php echo base_url(); ?>assets/js/dataTables/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/dataTables/dataTables.tableTools.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/dataTables/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script>
    $(document).ready(function() {
		$('#dataTables-paymentHistory').DataTable( {
			"order": [[ 0, "desc" ]]
		});
		
    });
 </script>	 

<?php $this->load->view('site/modals/campaign/sms/transfer_confirm_alert');?>
<?php $this->load->view('themeFront/footer.php');?>



