<?php $this->load->view('themeFront/header_logged.php');?>
<!--<script type="application/javascript" src="<?php echo base_url();?>js/site_ads.js"></script>-->
<script type="application/javascript" src="<?php echo base_url();?>js/user_wallet.js"></script>

<?php 
$user_id=$this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);

$remoteCountry=$this->config->item('remote_country_code');
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);

$campaigns_info=$this->sms_model->get_recent_campaigns($user_id);
$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);
$settingsInfo= $this->app_settings_model->get_site_settings();
$transactionInfo=$this->wallet_model->recent_transactions($user_id);

@$walletInfo= $this->account_model->get_account_wallet($user_id);

?>
<div class="main-container no_margin no-padding">
    <?php $this->load->view('themeFront/dashboard_sidebar');?>
    <div class="container">
	
	<div class="row">
	 <div class="col-md-6">
      <ol class="breadcrumb pull-left">
        <li><a href="<?php echo base_url()?>site"><i class="icon-home fa"></i></a></li>
		<li><a href="<?php echo base_url()?>site/profile"><?php echo _("Account")?></a></li>
        <li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
       
      </ol>
      </div>
	  
	  <div class="col-md-6 text-right">
	    <i class="fa fa-calendar-check-o"></i> 
		   <?php 
		   $lastLogin=@$this->session->userdata['site_login']['last_login'];
		   echo _("Your last login :");
	   
		   if($lastLogin!='0000-00-00 00:00:00'){
			echo date('d-M-Y H:i:s',strtotime($lastLogin));
	   	   }else{
			  echo "<span class='text-muted'> " . _('data not available') . "</span>";
		   }
		   ?>
	  </div>
    </div>
	  
       <div class="row">
	    <div class="col-sm-8">
		 <?php 
			  if($userInfo[0]->is_blocked==1)
			  {
		 ?>
		  <div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<h4><i class="icon fa fa-ban"></i> <?php echo _("System Alert !")?></h4>
			 <?php echo _("Your campaign service turned to blacklisted, please contact support team for more details.")?><br>
             <b>---<?php echo _("Remarks-")?>--</b><br>
             <?php echo $userInfo[0]->block_remarks;?>			 
		  </div>
	     <?php 
			  }
		 ?>
	   
	     <?php 
		   if(@$walletInfo[0]->balance_amount>=0){
			   
			/// checking early free credit requests   
		    $check_early_request=$this->wallet_model->check_freecredit_request_byid($user_id); 
			if($check_early_request==0){
			   ?>
			<div class="inner-box">
			  <a href="javascript:void(0)">
			  <img src="<?php echo base_url()?>images/request_credit.jpg" alt="Request Free Credit"  onclick="javascript:fCreditRequestForm()"/>
			  </a>
			</div>
			   <?php
			}
		   }
		 ?>
		 <div class="card">
		  <div class="bg-green">
		    <div class="row">
		   <div class="col-md-10">
		     <i class="fa  fa-envelope-o"></i>  <b><?php echo _("RECENT CAMPAIGNS")?></b>
		   </div>
		   <div class="col-md-2 text-right">
		    <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('mc')?>"><i class="fa fa-list"></i> <?php echo _("View All")?></a>
		   </div>
		    </div>
		  </div>
		 
		 <!--///// rencent campaigns ////---->
		<?php if($campaigns_info>0){?>
            	 
	    <table class="table table-striped" >
		<thead ><!-- Table head -->
		<tr>
		
		<th class="col-sm-1"><?php echo _("C.Code")?></th>
		<th class="col-sm-1"><?php echo _("Submit On")?></th>
		<th class="col-sm-1"><?php echo _("Sent On")?></th>
		<th class="col-sm-2"><?php echo _("Campaign Summary")?></th>
		<th class="col-sm-1"><?php echo _("Type")?></th>
		</tr>
		</thead><!-- / Table head -->
		<tbody>
		<?php 
		
		
		$cmp = 1 
		?>
		<?php if ($campaigns_info!=0): foreach ($campaigns_info as $campaigns) : ?>

		<tr>
		<td><?php echo "#".$campaigns->campaign_code?></td>
		<td>
		 <?php echo date('d-M-Y H:i:s',strtotime($campaigns->do_submit)); ?>
		</td>
		
		<td>
		<?php 
		  if($campaigns->do_sent!='0000-00-00 00:00:00'){
			 echo date('d-M-Y H:i:s',strtotime($campaigns->do_sent));
		  }else{
			  echo "<span class='label label-default'><i class='fa fa-hourglass-half'></i> " . _("Waiting..") . "</span>";
		  }
		 
		 ?>
		 
		</td>
		<td width="20%">
		    <?php 
			  $success_count=$this->sms_model->get_success_count($campaigns->campaign_code,$user_id);
			  $participated_count=$this->sms_model->get_campaign_contacts_count($campaigns->campaign_code,$user_id);
			?>
			  
		  <div class="row text-left">
		  <div class="col-md-4">
			<div class="ad_free_btn"><span class="text-primary"><?php echo _("Total")?><?php echo "(".$participated_count.")";?></span></div>
		  </div>
		  <div class="col-md-2">
		   &nbsp;
		  </div>
		  <div class="col-md-6">
			 <?php 
			  if($success_count!=0){
				  $failed=$participated_count-$success_count;
			  }else{
				  $failed="0";
			  }
			  
			  ?>
			  <?php echo "<span class='text-success'><i class='fa fa-check-circle'></i>success (".$success_count.")</span><br>";?>
			  <?php echo "<span class='text-danger'><i class='fa fa-exclamation-circle'></i>failed (".$failed.") </span>";?> 
		  </div>
		  </div>
		</td>
		<td>
		 <?php 
		   switch($campaigns->is_scheduled){
			   case '0':
			   echo "<span class='text-primary'><i class='fa fa-leaf'></i> " . _("regular") . "</span>";
			   break;
			   
			   case '1':
			   echo "<span class='text-warning'><i class='fa fa-clock-o'></i> " . _("scheduled") . "</span>";
			   break;
		   }
		  ?>
		</td>
		</tr>
		<?php
		$cmp++;
		endforeach;
		?>
		<?php else : ?>
		<td colspan="4" class="text-center">
		   <h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
		</td>
		<?php endif; ?>
		</tbody>
		</table>
		<?php 
		 } else{
			 ?>
			 <div class="row">
			 <div class="col-md-12 text-center"> 
			 <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>">
				<img src="<?php echo base_url()?>images/no_campaign.jpg" alt="No listing found" />
			 </a>
			 </div>
			 </div>
			 <?php
		 }
		?>
		
	     <!--///// end campaigns ////-->
         </div>
		  
		  <!--/////////////// recent transaction ///////////-->
		
          <div class="card">
           <div class="bg-blue">
            <div class="row">
		   <div class="col-md-10">
		   <i class="fa fa-google-wallet"></i> <b><?php echo _("RECENT TRANSACTIONS")?></b>
		   </div>
		   <div class="col-md-2 text-right">
		    <div class=""> <a href="<?php echo base_url()?>site/wallet/history"><i class="fa fa-list"></i> <?php echo _("View All")?></a> </div>
		   </div>
		   </div>
           </div>
		  
			
			<?php if($transactionInfo>0){?>
            
            <table class="table table-striped ">
            <thead>
			<th><?php echo _("Ref.No")?>&nbsp;</th>
            <th><?php echo _("Payment")?></th>
            <th><?php echo _("Pay Date")?></th>
			<th><?php echo _("Remarks")?></th>
			<th><?php echo _("Status")?></th>
			<th><?php echo _("Invoice")?></th>
			</thead>
			
            <tbody>
			
				<?php 
				$tns = 1; 
				if ($transactionInfo!=0): foreach ($transactionInfo as $tns) : ?>

				<tr>
				<td><?php echo "#".$tns->ad_reference?></td>
				<td>
				 <?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.$tns->ad_payment ?>
				</td>
				
				<td>
				 <?php echo date('d-M-Y H:i:s',strtotime($tns->date_of_payment)); ?>
				</td>
				<td>
				<?php echo $tns->payment_remarks?>	
					
				</td>
				<td>
				  <?php 
				   switch($tns->payment_status){
					   case '1':
					   echo '<div class="label label-success"> <i class="fa fa-check-circle"></i>' . _('PAID') . '</div>';
					   break;
					   
					   case '0':
					   echo '<div class="label label-danger"> <i class="fa fa-times"></i> ' . _('Failed') . '</div>';
					   break;
				   }
				  ?>
				</td>
				
				<td>
				  <?php 
				   switch($tns->payment_status){
					   case '1':
					   ?>
					   <a href="<?php echo base_url()?>site/export/invoice?iv=<?php echo base64_encode($tns->payment_id)?>" title="<?php echo _('Download Invoice') ?>"> <i class="fa fa-download"></i> </a>
					   <?php
					   break;
					   
					   case '0':
					   echo '-';
					   break;
				   }
				  ?>
				</td>
				
				</tr>
				<?php
				$tns++;
				endforeach;
				?>
				<?php else : ?>
				<td colspan="4" class="text-center">
				  <h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("There is no data to display")?></h4>
				</td>
				<?php endif; ?>
            </tbody>
            </table>
			
			<?php 
			}
			else
			{ 
		       ?>
			   <div class="row">
			   <div class="col-md-12 text-center" >    
			   <h4 class="text-muted"><i class="fa fa-info-circle"></i> <?php echo _("No transactions found")?></h4>
			   </div>
			   </div>
			   <?php
			} ?>
		
		  </div>
		</div>
		
		<div class="col-md-4">
		<?php $this->load->view('themeFront/member_sidebar_horizental');?>
		
		 <div class="panel sidebar-panel">
              <div class="panel-heading uppercase"><i class="fa fa-google-wallet"></i> <?php echo _("Wallet Balance")?></div>
              <div class="panel-content">
			  <div class="panel-default text-left">
			   <div class="row">
			    <div class="col-md-6">
                <div class="text-left">
                 <?php 
				echo "<span class='text-primary'>" . _('Available balance ') . "</span><br>";
				if(@$walletInfo[0]->balance_amount!=''){
				   ?>
				   <h1>
				   <?php echo @$walletInfo[0]->balance_amount;?> <small><?php echo @$currencyInfo[0]->currency_name;?></small>
				   </h1>
				   
				   <?php
				}else{
					?>
					<h1>
					<?php echo "00.00";?> <small><?php echo @$currencyInfo[0]->currency_name;?></small>
					</h1>
					 
					<?php
				}
				?>
                </div>
				</div>
				
				<div class="col-md-6 text-center">
				   
				   <?php 
				    if(@$walletInfo[0]->balance_amount<10){
					 ?>
						<?php echo _("Your credit is getting low please topup here")?><br><br>
					<?php					 
					}else{
						?>
						<?php echo _("Topup your wallet funds here")?><br><br>
						<?php
					}
				   ?>
				   
				   <a href="<?php echo base_url()?>site/wallet/add" class="btn btn-sm btn-border btn-primary">
				   <i class="fa fa-money"></i> <?php echo _("Add Funds")?></a>
				</div>
				</div>
				<br><br>
				</div>
				
              </div>
         </div>
			
		 <div class="panel sidebar-panel">
		  <div class="panel-heading uppercase"><i class="fa fa-mobile"></i> <?php echo _("Campaign Summary")?></div>
		  <div class="panel-content">
			<div class="panel-default text-left">
			   <div class="row">
				 <div class="col-md-6">
				 <div class="w50per">
				  <div class="w50per_lft">
				   <h1 class="text-warning"><i class="fa  fa-send-o "></i></h1>
					
				  </div>
				  <div class="w50per_rgt text-left">
					  <?php 
						$campaign_summary=$this->sms_model->get_campaign_overview($user_id);
						
						if($campaign_summary!=0){
							if(count($campaign_summary)<10){
								$allCamps="0".count($campaign_summary);
							}else{
								$allCamps=count($campaign_summary);
							}
							echo "<h1 class='no-padding text-warning'>".$allCamps."</h1>
							<small>" . _('Campaigns') . "</small>";
						}else{
							echo "<h1 class='no-padding text-danger'>(0)</h1>
							<small>" . _('Campaigns') . "</small>";
						}
					 ?>
				  </div>
				 </div>
				 </div>
				 <div class="col-md-6">
				 <div class="w50per">
				 
				   <div class="w50per_lft">
				   <h1 class="text-primary"><i class="fa  fa-user"></i></h1>
					
				  </div>
				  <div class="w50per_rgt text-left ">
					  <?php 
						$contacts_summary=$this->sms_model->get_contacts_byuser($user_id);
						
						if($contacts_summary!=0){
							if(count($contacts_summary)<10){
								$allContacts="0".count($contacts_summary);
							}else{
								$allContacts=count($contacts_summary);
							}
							echo "<h1 class='no-padding text-primary'>".$allContacts."</h1>
							<small>" . _('Contacts') . "</small>";
						}else{
							echo "<h1 class='no-padding text-primary'>(0)</h1>
							<small>" . _('Contacts') . "</small>";
						}
					 ?>
				  </div>
				  
				 </div>
				 </div>
			   </div>
			  <br><br>
			  <i class="pe-7s-repeat"></i> 
			  <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('hist')?>">
			  <?php echo _("Check campaign transaction history")?></a>
			  <br><br>
			</div>
		  </div>
		</div>
		<?php 
		if($userInfo[0]->mobile_validate!='1'){
		?>

		<span class="red"><?php echo _("Your mobile number is not verified")?></span><br><br>
		<a href="<?php echo base_url()?>site/profile" class="btn btn-danger">
			<i class="fa fa-mobile"></i> <?php echo _("Click here to verify")?></a>
		<br><br>
		<?php
		}else{
			echo "<h4 class='text-success'><i class='fa fa-check-circle'></i> " . _("Mobile Verified") . "</h4><br><br>";
		}
		?>
		<br><br>
	</div>
   </div>
  </div>
</div>
  
 

<?php $this->load->view('site/modals/campaign/sms/upgrade_account');?>  
<?php //$this->load->view('site/modals/mobile_validate');?>
<?php $this->load->view('site/modals/free_credit_request');?>
<?php $this->load->view('site/modals/profile_thumbnail');?>
<?php $this->load->view('themeFront/footer');?>



