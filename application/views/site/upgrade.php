<?php $this->load->view('themeFront/header_logged.php');
//$this->load->view('themeFront/banner_intro.php');


$remoteCountry=$this->config->item('remote_country_code');
$appSettings= $this->app_settings_model->get_primary_settings();
$appCountry=$appSettings[0]->app_country;
$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);


$user_id = $this->session->userdata['site_login']['user_id'];
$userInfo= $this->account_model->list_current_user($user_id);
$account_classes= $this->account_model->list_account_classes_notById(@$userInfo[0]->ac_class);
 
$encodedToken=$this->input->get('sToken'); 
$parseToken=base64_decode($this->input->get('sToken'));
$exeToken=explode("-",$parseToken);
$pay_methods=$this->input->get('pay_methods');
$paymentSettings=$this->app_settings_model->get_payment_settings();

$walletInfo = $this->account_model->get_account_wallet($user_id);

?>
<div class="main-container">
    <div class="container">
     <div class="row">
      <div class="col-md-6">
      <ol class="breadcrumb pull-left">
        <li><a href="<?php echo base_url()?>site/home"><i class="icon-home fa"></i></a></li>
        <li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Dashboard")?></a></li>
		<li><a href="<?php echo base_url()?>site/profile"><?php echo _("My Account")?></a></li>
        <li><a href="<?php echo base_url()?>site/dashboard"><?php echo _("Upgrade")?></a></li>
      </ol>
     </div>
	 <div class="col-md-6 text-right">
	  <?php 
	   /// custom library
	   $intlib=$this->internal_settings->local_settings();
	   echo '<i class="fa fa-question-circle"></i> '.$intlib[0]->help_text;
	  ?>
	 </div>
	</div>
	
	   <div class="row" ng-app="">
	   <div class="col-md-8">
          <div class="card-content">
           	<h4>
			 <i class="fa fa-star-o"></i> <?php echo sprintf(_("Upgrade Account [to %s]"), @$exeToken[2]);?>
			 <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#modalConfirmUpgrade">
			  <small> <?php echo _("Change Payment Method")?></small>
			 </a>
			
			</h4>
			
			<hr>
		 <div class="row">
          <div class="col-sm-12">
          <div class="">
        <?php 
		
		switch($pay_methods)
		{
			
		case 'pp':	
		//Set useful variables for paypal form
		$paypal_url = $paymentSettings[0]->pp_payment_url; //Test PayPal API URL
		$paypal_id = $paymentSettings[0]->pp_email; //Business Email
		?>		
    	<form action="<?php echo $paypal_url; ?>" method="post" role="form" class="form-horizontal">
		<div class="form-group required">
		  <label class="col-md-4 control-label" ><?php echo _("Item Code")?><sup>*</sup></label>
		  <div class="col-md-6">
			  <input  name="item_code" id="item_code" placeholder="<?php echo _('E.g. SVCSSS02') ?>" class="form-control input-md" required="" type="text" value="<?php echo @$exeToken[1];?>" readonly>
		  </div>
		  <span id="validEmail"></span>
		</div>
		
		<div class="form-group required">
		  <label class="col-md-4 control-label" ><?php echo _("Upgrading To")?><sup>*</sup></label>
		  <div class="col-md-6">
			  <input  name="upgrade_to" id="upgrade_to" placeholder="<?php echo _('E.g. Professional services') ?>" class="form-control input-md" required="" type="text" value="<?php echo @$exeToken[2];?>" readonly>
		  </div>
		  <span id="validEmail"></span>
		</div>
		
		<div class="form-group required">
		  <label class="col-md-4 control-label" ><?php echo _("Price")?><sup>*</sup></label>
		  <div class="col-md-6">
			  <input  name="service_price" id="service_price" placeholder="<?php echo _('E.g. 50') ?>" class="form-control input-md" required="" type="text" 
			  value="<?php echo $currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.@$exeToken[0];?>" readonly>
		  </div>
		  <span id="validEmail"></span>
		</div>
				 
        <!-- Identify your business so that you can collect the payments. -->
        <input type="hidden" name="business" value="<?php echo $paypal_id;?>">
        <!-- Specify a Buy Now button. -->
        <input type="hidden" name="cmd" value="_xclick">
        <!-- Specify details about the item that buyers will purchase. -->
        <input type="hidden" name="item_name" value="<?php echo @$exeToken[1]; ?>">
        <input type="hidden" name="item_number" value="<?php echo @$exeToken[1]; ?>">
        <input type="hidden" name="amount" value="<?php echo $exeToken[0]; ?>">
        <input type="hidden" name="currency_code" value="USD">
        
        <!-- Specify URLs -->
        <input type='hidden' name='cancel_return' value='<?php echo $paymentSettings[0]->pp_cancel_url;?>'>
        <input type='hidden' name='return' value='<?php echo $paymentSettings[0]->pp_return_url;?>'>
        
        <!-- Display the payment button. -->
       <div class="form-group form-group required">
	    <label class="col-md-4 control-label" ></label>
		<div class="col-md-6">
		<button type="submit" name="pay" id="pay" class="btn btn-primary btn-block btn-flat">
		 <i class="fa fa-share"></i> <?php echo _("Proceed to Pay")?></button>
        </div>
		</div>
    
    </form>  
	
	   <?php
	     break;
		 
		 case 'wallet':
		
		 if(@$walletInfo[0]->balance_amount>@$exeToken[0]){
			 ?>
			 <div class="form-horizontal"> 
			  <label class="col-md-4 control-label" ></label>
			 <?php echo "<h3 style='color:#999'>Your wallet balance : ".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.@$walletInfo[0]->balance_amount."</h3>";?>
			 </div>
			 <form action="<?php echo base_url();?>site/upgrade/paybywallet" method="post" role="form" class="form-horizontal">
			<div class="form-group required">
			  <label class="col-md-4 control-label" ><?php echo _("Item Code")?><sup>*</sup></label>
			  <div class="col-md-6">
				  <input  name="item_code" id="item_code" placeholder="<?php echo _('E.g. SVCSSS02') ?>" class="form-control input-md" required="" type="text" value="<?php echo @$exeToken[1];?>" readonly>
			  </div>
			</div>
			
			<div class="form-group required">
			  <label class="col-md-4 control-label" ><?php echo _("Amount to Paid")?></label>
			  <div class="col-md-6">
				  <input  name="amount_to_pay" id="amount_to_pay" placeholder="<?php echo _('E.g. 50') ?>" class="form-control input-md" 
				  required="" type="text" value="<?php echo $currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.' '.@$exeToken[0];?>" readonly>
			  </div>
			</div>
			
			<div class="form-group required">
			  <label class="col-md-4 control-label"></label>
			  <div class="col-md-6">
				  <input  name="wallet_amount" id="wallet_amount" placeholder="<?php echo _('E.g. 200') ?>" class="form-control input-md"
				  required="" type="hidden" value="<?php echo @$walletInfo[0]->balance_amount;?>" readonly>
			  </div>
			</div>
			
			<div class="form-group required">
			  <label class="col-md-4 control-label"></label>
			  <div class="col-md-6">
				  <input type="submit" value="Proceed to Pay" class="btn btn-primary">
			  </div>
			</div>
		
		    </form>
			 <?php
		 }else{
			 ?>
			 <div class="form-horizontal">
			  <h4 class="text-warning"><span class="fa fa-exclamation-triangle"></span> &nbsp;<?php echo _("Sorry your wallet has insufficient funds")?></h4>
			   <?php echo _("Please topup your funds to proceed")?><a href="<?php echo base_url()?>site/wallet/add"><?php echo _("Click here")?></a>
			 </div>
			 <?php
		 }
		 
		 break;
		 
		 case 'card':
		 echo "<h4 class='text-danger'><i class='fa fa-meh-o'></i> " . _("Sorry for the inconvience") . " </h4> 
		 This feature is not available at this moment.";
		 
		 break;
		 
		 case '':
		 echo "<h4 class='text-danger'><i class='fa fa-info'></i> " . _('Missing payment methods') . "</h4>";
		 ?>
			<a  href="javascript:void(0)" data-toggle="modal" data-target="#modalConfirmUpgrade" class="btn btn-default">
			  <?php echo _("Select Payment Method")?></a>
		 <?php
		 break;
		 
	    }
	   ?>
         </div>
           
        </div>
	   </div>
	   </div>
	   </div>
	   
	    <div class="col-md-4 reg-sidebar">
          
		  <div class="panel sidebar-panel"> 
			   <div class="panel-heading uppercase text-left">
					<i class="fa fa-shopping-cart"></i> <?php echo _("For Item Code :").@$exeToken[1];?>
			   </div>
			   
			   <div class="panel-body text-left">
                <div class="row">
              <div class="col-md-3 text-center">
			    <br>
				<?php
				  switch($pay_methods){
					  
					  case 'pp':
					  ?>
					   <img src="<?php echo base_url()?>theme4.0/client/images/payment/333/paypal.png" > 
					  <?php
					  break;
					  
					  case 'card':
					  ?>
					  <img src="<?php echo base_url()?>theme4.0/client/images/payment/333/visa.png" > 
					  <small>...</small>
					  <?php
					  break;
					  
					  case 'wallet':
					  ?>
					  <img src="<?php echo base_url()?>theme4.0/client/images/payment/wallet.png" > 
					  <?php
					  break;
				  }
				 ?>
			  </div>
			  <div class="col-md-9 text-left">
			    <b><?php echo _("Amount to be paid")?></b><br>
				<h1><?php echo $currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.' '.@$exeToken[0];?></h1>
			  </div>
              </div>
				<br>
			   </div>
		    </div>
			
			
		  <div class="panel sidebar-panel"> 
			   <div class="panel-heading uppercase text-left">
					<i class="fa fa-paper-plane-o"></i>  <?php echo _("SMS Campaign")?></div>
			   
			   <div class="panel-body text-left">
                 
				<div class="row">
              <div class="col-md-3 text-center">
			    <br>
				<h1> <i class="fa fa-info-circle"></i> </h1>
			  </div>
			  <div class="col-md-9 text-left">
			    <br>
			   <span><?php echo _("Improve your business by using")?></span><br><br>
			   <a href="<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>" class="btn btn-primary btn-sm"> <?php echo _("Start Campaign")?></a>
			   
			  </div>
              </div>
				<br>
			   </div>
		    </div>
			
			<p>
			   <?php echo _("The amount you will be paying for above services. Please")?><a href="#"><?php echo _("click here")?></a> <?php echo _("for service advantages.")?></p>
			
        </div>
	   </div>
	 </div>
   </div>  
<?php $this->load->view('site/modals/campaign/sms/upgrade_account');?>  
<?php $this->load->view('themeFront/footer.php');?>



