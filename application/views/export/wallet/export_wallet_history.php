<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_wallet_history.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$user_id=$this->session->userdata['site_login']['user_id'];
		$walletInfo= $this->wallet_model->ad_payments_bywallet($user_id);
		$appSettings= $this->app_settings_model->get_primary_settings();
		$appCountry=$appSettings[0]->app_country;
		$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
		
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?></b></td>
		<td><b><?php echo _("Ref.No")?> </b></td>
		<td><b><?php echo _("Payment")?> </b></td>
		<td><b><?php echo _("Date Of Payment")?> </b></td>
		<td><b><?php echo _("Payment/Invoice Ref.No")?></b></td>
		<td><b><?php echo _("Remarks")?></b> </td>
		<td><b><?php echo _("Status")?></b></td>
		</tr>
		<?php
		$i=0;
		foreach($walletInfo as $wallet){
	    ?>
		<tr>
		<td>
		<?php echo ++$i?>
		</td>
		<td>
		<?php echo $wallet->ad_reference?>
		</td>
		<td>
		<?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.$wallet->ad_payment?> 
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($wallet->date_of_payment)); 
		?>
		</td>
		<td>
		   <?php echo $wallet->payment_reference ?>
		</td>
		<td>
		<?php 
		  echo $wallet->payment_remarks
		?>
		</td>
		<td>
		 <?php 
		  switch($wallet->payment_status){
			  case '1':
			  ?>
			  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i><?php echo _("PAID")?></div>
			  <?php
			  break;
			  
			  case '0':
			  ?>
			  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i> <?php echo _("Failed")?></div>
			  <?php
			  break;
		  }
		 ?>
         </td>
		</tr>
		<?php } ?>
</table>

