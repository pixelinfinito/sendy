<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_pricing.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$user_id=$this->session->userdata['site_login']['user_id'];
		$price_info=$result=$this->app_settings_model->get_sms_prices();
		
		?>
		<table border='1'>
		<tr>
		<td><strong><?php echo _("Country Code")?></strong></td>
		<td><strong><?php echo _("Country Name")?></strong></td>
		<td><strong><?php echo _("Price")?></strong></td>
		<td><strong><?php echo _("Currency")?></strong></td>
		<td><strong><?php echo _("Description")?></strong></td>
		</tr>
		<?php
		foreach($price_info as $data){
	    ?>
		<tr>
		<td><?php echo $data->country_code; ?></td>
		<td><?php echo $data->country_name; ?></td>
		<td><?php echo $data->price; ?></td>
		<td><?php echo $data->currency_code; ?></td>
		<td><?php echo $data->price_desc; ?></td>
		
		</tr>
		<?php } ?>
</table>

