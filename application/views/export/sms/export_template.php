<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_templates.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$user_id=$this->session->userdata['site_login']['user_id'];
		$ttitles_info=$this->sms_model->get_template_titles($user_id);
		
		?>
		<table border='1'>
		<tr>
		<td><strong><?php echo _("Template Code")?></strong></td>
		<td><strong><?php echo _("Title")?></strong></td>
		<td><strong><?php echo _("Status")?></strong></td>
		</tr>
		<?php
		foreach($ttitles_info as $data){
	    ?>
		<tr>
		<td><?php echo $data->template_code; ?></td>
		<td><?php echo $data->template_title; ?></td>
		<td>
		  <?php 
		
		   switch($data->status){
			   case '1':
			   echo _("active");
			   break;
			   
			   case '0':
			   echo _("inactive");
			   break;
		   }
		  ?>
		</td>
		</tr>
		<?php } ?>
</table>

