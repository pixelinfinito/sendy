<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_success_history.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$user_id=$this->session->userdata['site_login']['user_id'];
		$history_info=$this->sms_model->get_campaign_success_history($user_id);
		$appSettings= $this->app_settings_model->get_primary_settings();
		$appCountry=$appSettings[0]->app_country;
		$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
		
		?>
		<table border='1'>
		<tr>
		<td><strong><?php echo _("Campaign Code")?></strong></td>
		<td><strong><?php echo _("From")?></strong></td>
		<td><strong><?php echo _("To")?></strong></td>
		<td><strong><?php echo _("SMS Group")?></strong></td>
		<td><Strong><?php echo _("Message Body")?></strong></td>
		<td><strong><?php echo _("Unit Cost")?></strong></td>
		<td><strong><?php echo _("Status")?></strong></td>
		</tr>
		<?php
		foreach($history_info as $data){
	    ?>
		<tr>
		<td><?php echo $data->campaign_code; ?></td>
		<td><?php echo $data->from_number; ?></td>
		<td><?php echo $data->to_number; ?></td>
		<td><?php echo $data->group_name; ?></td>
		<td><?php echo $data->message; ?></td>
		<td><?php echo $currencyInfo[0]->currency_name.' '.$data->unit_cost; ?></td>
		<td>
		 <?php 
		    switch($data->deliver_status){
				case '3':
				echo 'Queued';
				break;
				
				case '2':
				echo 'Pending';
				break;
				
				case '1':
				echo 'Delivered';
				echo ' / on '.$data->do_sent;
				break;
				
				case '0':
				echo 'Failed';
				break;
			}
		   ?>
		</td>
		</tr>
		<?php } ?>
</table>

