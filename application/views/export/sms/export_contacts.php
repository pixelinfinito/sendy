<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_contacts.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$user_id=$this->session->userdata['site_login']['user_id'];
		$contacts_info=$this->sms_model->get_contacts_byuser($user_id);
		
		?>
		<table border='1'>
		<tr>
		<td><strong><?php echo _("Name")?></strong></td>
		<td><strong><?php echo _("Country")?></strong></td>
		<td><strong><?php echo _("Contact Number")?></strong></td>
		<td><strong><?php echo _("Group Name")?></strong></td>
		
		</tr>
		<?php
		foreach($contacts_info as $data){
	    ?>
		<tr>
		<td><?php echo $data->contact_name; ?></td>
		<td><?php echo $data->country_name; ?></td>
		<td><?php echo $data->contact_mobile; ?></td>
		<td><?php echo $data->group_name; ?></td>
		
		</tr>
		<?php } ?>
</table>

