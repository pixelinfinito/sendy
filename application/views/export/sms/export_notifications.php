<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_notifications.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$user_id=$this->session->userdata['site_login']['user_id'];
		$notify_info=$this->notification_model->list_notifications_bymember($user_id);
		
		?>
		<table border='1'>
		<tr>
		<td><strong><?php echo _("S.No")?></strong></td>
		<td><strong><?php echo _("Notification Type")?></strong></td>
		<td><strong><?php echo _("Title")?></strong></td>
		<td><strong><?php echo _("Message")?></strong></td>
		</tr>
		<?php
		$i=0;
		foreach($notify_info as $data){
	    ?>
		<tr>
		<td><?php echo ++$i?></td>
		<td><?php echo ucfirst($data->notify_type); ?></td>
		<td><?php echo $data->notify_title; ?></td>
		<td>
		<?php echo $data->notify_body; ?>
		</td>
		</tr>
		<?php 
		
		} 
		?>
</table>

