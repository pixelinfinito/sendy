
<?php
		
		$intlib=$this->internal_settings->local_settings();
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$payment_id=base64_decode($this->input->get('iv'));
		$payment_transaction=$this->payment_model->payment_invoice_byid($user_id,$payment_id);
		$appSettings= $this->app_settings_model->get_app_settings();
		$currency=$appSettings[0]->app_currency;
		
		$member_name=$payment_transaction[0]->ac_first_name.' '.$payment_transaction[0]->ac_last_name;
		$member_city=$payment_transaction[0]->ac_city;
		$member_add1=$payment_transaction[0]->ac_address1;
		$member_add2=$payment_transaction[0]->ac_address2;
		$member_email=$payment_transaction[0]->ac_email;
		$member_phone=$payment_transaction[0]->ac_phone;
		
		$order_reference=$payment_transaction[0]->acc_item_number;
		$payment_reference=$payment_transaction[0]->c2o_transaction_id;
		$date_of_payment=$payment_transaction[0]->payment_datetime;
		
		$payment_headline="Topup wallet funds order reference ".$order_reference;
		$payment_desc='Wallet funds transaction order '.$payment_transaction[0]->c2o_order_number;;
		$total_amount=$payment_transaction[0]->c2o_total_amount;
		$response_code=$payment_transaction[0]->c2o_response_code;
		if($response_code!='APPROVED'){
			$payment_status='Invalid Token';
		}else{
			$payment_status='Accepted';	
		}
		
		/*
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=invoice_".$payment_reference.".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		*/
	
 $html='<table width="100%">
	<tr>
	  <td colspan="2" class="bg_blue">
	  <table width="100%" >
	  <tr>
	  <td> <img src="'.base_url().'theme4.0/client/images/logo.png" alt="Logo" width="150" height="70"/> </td>
	  <td class="pull_right"> <h3>' . _('Invoice') . ' </h3> <h5>'.$payment_reference.'</h5></td>
	  </tr>
	  </table>
	  </td>
	 </tr>
	 
	  <tr>
	  <td colspan="2">
	   &nbsp;
	  </td>
	 </tr>
		 
	 <tr>
	 <td width="50%">
	 <table width="100%" class="border_all">
		   <tr>
		    <td class="border_bottom">
			<strong>' . _('Customer Information') . '</strong>
			</td>
			</tr>
			
			<tr>
			<td>'.$member_name.'</td>
			</tr>
			
			<tr>
			<td>'.$member_city.'</td>
			</tr>
			
			<tr>
			<td>'.$member_add1.'</td>
			</tr>
			
			<tr>
			<td>'.$member_add2.'</td>
			</tr>
			
			<tr>
			<td>'.$member_email.'</td>
			</tr>
			
			<tr>
			<td>'.$member_phone.'</td>
			</tr>
		  
		  </table>
	 </td>
	 
	  <td width="50%">
	  <table width="100%">
	     <tr>
			<td>' . _('Transaction ID') . ' </td> <td>:</td> <td>'.$payment_reference.'</td>
		 </tr>
			
		<tr>
			<td>' . _('Order Reference') . '</td> <td>:</td> <td>'.$order_reference.'</td>
		</tr>
		
		<tr>
			<td>' . _('Date Of Payment') . '</td> <td>:</td> <td>'.date('d-M-Y H:i:s',strtotime($date_of_payment)).'</td>
		</tr>
		
		<tr>
			<td>' . _('Reference') . '</td> <td>:</td> <td>'.$payment_headline.'</td>
		</tr>
		
	  </table>
	 </td>
	 </tr>
	 
	  <tr>
	 <td colspan="2">
	  &nbsp;
	 </td>
	 </tr>
	 
	 <tr>
	 <td colspan="2" class="border_bottom">
	   <table width="100%">
		<tr>
		 <td class="cap_letters"><b>' . _('Bill Description') . '</b></td>
		 <td class="cap_letters"><b>' . _('Payment Response') . '</b> </td>
		 <td class="cap_letters"><b>' . _('Payment Status') . '</b></td>
		 <td class="cap_letters"><b>' . _('Total') . '</b></td>
		</tr>
		
		 <tr>
		 <td>'.$payment_desc.'</td>
		 <td>'.$response_code.'</td>
		 <td>'.$payment_status.'</td>
		 <td>'.$currency.' '.$total_amount.'</td>
		</tr>
		
	   </table>
	 </td>
	 </tr>
	 
	 <tr>
	 <td colspan="2" class="border_bottom">
	 <table width="100%">
	  <tr>
	  <td colspan="3">
	   &nbsp;
	  </td>
	  <td>
	   &nbsp;
	  </td>
	  <td style="text-align:right">
	   <h4> ' . _('Grand Total') . ' : '.$currency.' '.$total_amount.'</h4>
	  </td>
	  </tr>
	  </table>
	  </td>
	 </tr>
</table>

<table>
<tr>
<td colspan="4">
		******* ' . _('Thank you') . ' '.$intlib[0]->app_default_name.' *******
</td>
</tr>
</table>';


$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list
// LOAD a stylesheet
$stylesheet = file_get_contents(APPPATH.'third_party/mpdf/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->WriteHTML($html,2);
$mpdf->Output('Invoice_'.$payment_reference.'.pdf','I');
exit;

?>

