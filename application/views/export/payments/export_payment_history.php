<?php
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=export_payment_history.xls");
		header("Pragma: no-cache");
		header("Expires: 0");
		$user_id=$this->session->userdata['site_login']['user_id'];
		$paymentInfo= $this->payment_model->get_member_payments($user_id);
		$appSettings= $this->app_settings_model->get_primary_settings();
		$appCountry=$appSettings[0]->app_country;
		$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
		
		?>
		<table border='1'>
		<tr>
		<td><b><?php echo _("S.No")?> </b></th>
		<td><b><?php echo _("Ref.No")?> </b></th>
		<td><b><?php echo _("Amount")?> </b></th>
		<td><b><?php echo _("Transaction ID")?> </b></th>
		<td><b><?php echo _("Response Code")?> </b></th>
		<td><b><?php echo _("Payment Datetime")?> </b></th>
		<td><b><?php echo _("Payment Status")?> </b></th>
		</tr>
		
		<?php
		$i=0;
		foreach($paymentInfo as $payment){
	    ?>
		
		<tr>
		<td>
		<?php echo ++$i?>
		</td>
		
		<td>
		<?php echo $payment->acc_item_number?>
		</td>
		<td>
		<?php echo @$currencyInfo[0]->character_symbol.@$currencyInfo[0]->currency_name.' '.$payment->c2o_total_amount?> 
		</td>
		
		<td>
		  <?php echo $payment->c2o_transaction_id ?>
		</td>
		<td>
		  <?php echo $payment->c2o_response_code ?>
		</td>
		<td>
		<?php 
		echo date('d-M-Y H:i:s',strtotime($payment->payment_datetime)); 
		?>
		</td>
		<td>
		 <?php 
		  switch($payment->payment_status){
			  case '1':
			  ?>
			  <div class="ad_cash_paid_btn"> <i class="fa fa-check-circle"></i><?php echo _("Accepted")?></div>
			  <?php
			  break;
			  
			  case '0':
			  ?>
			  <div class="ad_unpublish_btn"> <i class="fa fa-times"></i> <?php echo _("Failed")?></div>
			  <?php
			  break;
		  }
		 ?>
         </td>
		</tr>
		<?php } 
		?>
</table>

