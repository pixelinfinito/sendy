<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Payment_Model extends CI_Model
{
  
	public function update_card($c_holder_name,$c_number,$c_month,$c_year,$c_cvv,$c_address1,$c_address2,
	$c_city,$c_country,$c_postal_code,$userId){
			
      	try{
			
			  $data = array(
			  'user_id' => $userId ,
			  'card_number' => $c_number,
			  'card_holder_name' => $c_holder_name ,
			  'card_cvv' => $c_cvv ,
			  'card_expire_month' => $c_month ,
			  'card_expire_year' => $c_year ,
			  'billing_street1' => $c_address1 ,
			  'billing_street2' => $c_address2 ,
			  'billing_city' => $c_city,
			  'billing_country' => $c_country,
			  'billing_postalcode' => $c_postal_code 			   			  
			  
			);
			
			$data2 = array(
			  'card_number' => $c_number,
			  'card_holder_name' => $c_holder_name ,
			  'card_cvv' => $c_cvv ,
			  'card_expire_month' => $c_month ,
			  'card_expire_year' => $c_year ,
			  'billing_street1' => $c_address1 ,
			  'billing_street2' => $c_address2 ,
			  'billing_city' => $c_city,
			  'billing_country' => $c_country,
			  'billing_postalcode' => $c_postal_code 			   			  
			  
			);


			$this->db->select('*');
			$this->db->from('oc_card_master');
			$this->db->where('card_number',$c_number);
			$this->db->where('card_cvv',$c_cvv);
			$this->db->where('card_expire_month',$c_month);
			$this->db->where('card_expire_year',$c_year);
			$this->db->where('billing_street1',$c_address1);
			$this->db->where('user_id',$userId);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			    return 2;
				
			}
			else{
				$this->db->select('*');
				$this->db->from('oc_card_master');
				$this->db->where('user_id',$userId);
				$user=$this->db->get();
				
				$user_check=$user->result();
				if($user_check[0]->user_id!=''){
					$this->db->where('user_id',$userId);
					$query2=$this->db->update('oc_card_master', $data2); 
					if($query2){
						return true;
					}else{
						return 0;
					}
				}else{
					$query3=$this->db->insert('oc_card_master', $data); 
					if($query3){
						return true;
					}else{
						return 0;
					}
				}
				
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	
	}
	
	
	public function get_card_information($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_card_master');
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	
	public function get_account_wallet($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_account_wallet');
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_member_payments($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_account_payments');
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	
	public function all_member_payments(){
		$this -> db -> select('*');
		$this -> db -> from('oc_account_payments');
		$this->db->join('oc_ad_accounts','oc_ad_accounts.user_id=oc_account_payments.user_id');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	
	public function total_member_payments(){
		$this -> db -> select('acc_currency_code');
		$this -> db -> select_sum('c2o_total_amount');
		$this -> db -> from('oc_account_payments');
		$this->db->where_in('payment_status',1);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	public function total_failed_payments(){
		$this -> db -> select('acc_currency_code');
		$this -> db -> select_sum('c2o_total_amount');
		$this -> db -> from('oc_account_payments');
		$this->db->where_in('payment_status',0);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	
	public function payment_invoice_byid($user_id,$acc_payment_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_account_payments');
		$this->db->join('oc_ad_accounts','oc_ad_accounts.user_id=oc_account_payments.user_id');
		$this->db->where_in('oc_account_payments.user_id',$user_id);
		$this->db->where_in('oc_account_payments.acc_payment_id',$acc_payment_id);
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	
	public function list_users(){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_users');
		$this -> db -> where('status ', $status_id); 
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	public function list_account_classes(){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_account_class');
		$this -> db -> where('status ', $status_id); 
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	
	public function list_account_classes_notById($ac_class){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_account_class');
		$this -> db -> where('status ', $status_id); 
		$this -> db -> where('acc_id <> ', $ac_class);
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	public function list_current_user($user_id){
		
		$where=array('status'=>'1','user_id'=>$user_id);
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this -> db -> where($where); 
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	
	
	public function update_user_profile($group_id,$user_id,$first_name,$last_name,$email_id,$mobile_number){
		$data=array('user_group'=>$group_id,
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'email_id'=>$email_id,
		'mobile_number'=>$mobile_number,
		);
		
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}
		
	}
	public function update_stage_user($user_id,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status){
		$data=array('user_group'=>$user_groups,
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'user_email'=>$email_id,
		'user_mobile'=>$telephone,
		'user_phone'=>$telephone2,
		'status'=>$user_status,
		);
		
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	
} 
?>