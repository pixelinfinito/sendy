<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Account_Model extends CI_Model
{
	function authenticate($username,$password)
	{
		
		$where = array ('ac_email'=>$username,'ac_password'=>$password,'is_validated'=>'1','status'=>'1');
		
        $this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this -> db -> where($where); 
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1)
		{
			//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();
			
			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
			    $lastLogin=date("Y-m-d H:i:s");
				
				
			}else{
				date_default_timezone_set("Asia/Brunei");
				$lastLogin=date("Y-m-d H:i:s");
			}
			
			
			$data=array('last_login'=>$lastLogin);
			$this->db->where('ac_email',$username);
			$this->db->where('ac_password',$password);
			$this->db->update('oc_ad_accounts',$data);
			
			return $query->result();
		}
		else
		{
			return 0;
		}

	}
	
	
	public function add_account($ac_email,$ac_class,$ac_password,$ac_first_name,$ac_last_name,$ac_phone,
	$ac_gender,$ac_about,$ac_country,$created_by,$ac_active_status){
	
		$this->db->select('*');
		$this->db->from('oc_ad_accounts');
		$this->db->where('ac_email',$ac_email);
		$query=$this->db->get();
		
		if($query->num_rows()>0){
		  return 2;	
		  echo _("Duplication occured, please refresh your page.");
		  
		 
		}
		else{
			//** Getting email validation codes from site settings **//
			$this->db->select('*');
			$this->db->from('oc_site_settings');
			$query_site=$this->db->get();
			
			if($query_site->num_rows()>0){
				$rCodeRow=$query_site->result();
				$sitAdLimit=$rCodeRow[0]->individual_ad_limit;
				$randstrCode= $rCodeRow[0]->vcode_string;
				$randCode=$randstrCode.rand($rCodeRow[0]->vcode_min_val,$rCodeRow[0]->vcode_max_val);
				$genUserId=uniqid(rand(1000,$rCodeRow[0]->user_keygen));
			
				
			}else{
				$randCode="OC".rand(1000,100000);
				$sitAdLimit=10;
				$genUserId=uniqid(rand(1000,10000));
			
			}
			
			//** Gettings timezone based on country code **//
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$ac_country);
			$query_timezone=$this->db->get();
			
			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
			    $doj=date("Y-m-d H:i:s");
				
				
			}else{
				date_default_timezone_set("Asia/Brunei");
				$doj=date("Y-m-d H:i:s");
			}
			
			if($ac_active_status!=1){
				$ac_status=0;
				$is_validated=0;
			}else{
				$ac_status=1;
				$is_validated=1;
			}
			$data = array(
			      'user_id' => $genUserId,
				  'ac_email' => $ac_email ,
				  'ac_password' =>  MD5($ac_password),
				  'ac_first_name' => $ac_first_name ,
				  'ac_last_name' => $ac_last_name ,
				  'ac_phone' => $ac_phone ,
				  'ac_gender' => $ac_gender ,
				  'ac_about' => $ac_about,
				  'ac_class' => $ac_class,
				  'ac_country' => $ac_country ,
				  'validate_code' => $randCode ,
				  'ad_limit' => $sitAdLimit ,
				  'created_by'=>$created_by,
				  'is_paid' => '0',
				  'is_validated' => $is_validated,
				  'ac_join_date' => $doj ,
				  'status' => $ac_status
			);

			$res=$this->db->insert('oc_ad_accounts', $data); 
			if($res){
				
				$data2 = array(
			      'user_id' => $genUserId,
				  'group_name' => 'Default' ,
				  'group_type' => 'public',
				  'status' => '1'
				);
				/// adding default sms campaign group ///////
               $res=$this->db->insert('oc_campaign_groups', $data2); 	  
				
				//////////////////// Delivering email activation link /////////////////

				$this->db->select('*');
				$this->db->from('oc_app_settings');
				$app_query=$this->db->get();
				$resApp=$app_query->result();
		
		        if(@$resApp[0]->default_from_mail!=''){
					$from_emails=$resApp[0]->default_from_mail;
				}else{
					$from_emails = 'contact@onetextglobal.com';
				}
				
				 if(@$resApp[0]->app_default_name!=''){
					$app_name=$resApp[0]->app_default_name;
				}else{
					$app_name = 'OTGApp';
				}
				
				
		
		        $full_name       =  $ac_first_name.''.$ac_last_name;
				$notify_subject  =  sprintf(_("Account Activation from %s"), $app_name);
				$notify_from     =  $from_emails;
				$notify_body     =  sprintf(_("Dear %s"), $full_name) .", <br><br> " . _('Greetings from') . " ".$app_name." ..!<br><br>";
				$notify_body.= _("Your account activation link has follows, please validate your email by clicking the link.") . " <br><br>";
				$notify_body.= _("Activation URL") . base_url()."site/register/activation?token=".base64_encode($randCode)."<br><br>";
				
				$notify_body.= _("Thank you") . ".<br><br>" . _("Best regards") . ",<br><strong>".$app_name."</strong>";
				$notify_to  = $ac_email;
				$deliver_status  = '0';
				
				
				//Sending SMTP Mail
				// Loads the email library
				$this->load->library('email');
				// FCPATH refers to the CodeIgniter install directory
				// Specifying a file to be attached with the email
				//$file = FCPATH . 'license.txt';
				// Defines the email details
				$this->email->from($notify_from, $app_name);
				$this->email->to($notify_to);
				//$this->email->cc('another@example.com');
				//$this->email->bcc('one-another@example.com');
				$this->email->subject($notify_subject);
				$this->email->message($notify_body);
				//$this->email->attach($file);
				// The email->send() statement will return a true or false
				// If true, the email will be sent
				if($ac_active_status!=1){
					if ($this->email->send()) {
					   
					  $data = array(
					  'mail_to' => $ac_email ,
					  'mail_from' => $from_emails,
					  'mail_message' => $notify_body ,
					  'mail_subject' => $notify_subject ,
					  'member_name' => $full_name ,
					  'mail_status' => '1' 
					 
					);

						$res=$this->db->insert('oc_mail_activations', $data); 
						
						/////////// Make activation link up /////////////
						
						$data2=array('activation_link'=>'1');
						$this->db->where('ac_email',$ac_email);
						$query_activate=$this->db->update('oc_ad_accounts',$data2);
				
					}else{
						
					  $data = array(
					  'mail_to' => $ac_email ,
					  'mail_from' => $from_emails,
					  'mail_message' => $notify_body ,
					  'mail_subject' => $notify_subject ,
					  'member_name' => $full_name ,
					  'mail_error'=>_('Something went wrong'),
					  'mail_status' => '0' 
					 
					);

						$res=$this->db->insert('oc_mail_activations', $data); 
					}
				}
				////////////////////////// End Activation /////////////////////////////			  
				return true;	
			}else{
				return false;
			}
		}
	}
	
	public function activate_account($validate_code){
		
		
		$this->db->select('*');
		$this->db->from('oc_ad_accounts');
		$this->db->where('validate_code',$validate_code);
		$app_query=$this->db->get();
		$resApp=$app_query->result();

		if(@$resApp[0]->validate_code!=''){
			
			//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();
			
			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
			    $dateActivate=date("Y-m-d H:i:s");
				
				
			}else{
				date_default_timezone_set("Asia/Brunei");
				$dateActivate=date("Y-m-d H:i:s");
			}
			
			
			$data=array('is_validated'=>'1','validated_on'=>$dateActivate,'status'=>'1');
			$this->db->where('validate_code',$validate_code);
			$query_activate=$this->db->update('oc_ad_accounts',$data);
			
			if($query_activate)
			{
				return true;
			}else{
				return false;
			} 
		
			
		}else{
			return false;
		}
				
				
		
	}
	
	public function update_pp_payment($item_number,$txn_id,$payment_gross,$currency_code,$payment_status){
		try{
			$user_id=$this->session->userdata['site_login']['user_id'];
			$data = array(
			  'user_id' => $user_id ,
			  'acc_item_number' => $item_number,
			  'acc_txn_id' => $txn_id ,
			  'acc_payment_gross' => $payment_gross ,
			  'acc_currency_code' => $currency_code ,
			  'payment_status' => $payment_status 
			  
			);
						
						
			$this->db->select('*');
			$this->db->from('oc_account_payments');
			$this->db->where('user_id',$user_id);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			    /** getting site settings **/
			    $this->db->select('*');
				$this->db->from('oc_site_settings');
				$query_settings=$this->db->get();
				$settingInfo=$query_settings->result();
				
				$adLimit= $settingInfo[0]->business_ad_limit;
				$imgUploads= $settingInfo[0]->business_image_uploads;
				$accExpires = date('Y-m-d H:i:s');
				$data2 = array(
						  'is_paid' => '1',
						  'ad_limit' => $adLimit ,
						  'pay_acc_expire_datetime' => $accExpires,
						 
						);
				/** update to paid account **/	
				$this->db->where('user_id',$user_id);					
			    $updateAccount=	$this->db->update('oc_ad_accounts',$data2);
				
				
				$res=$this->db->update('oc_account_payments', $data); 
			   // $this->db->where('app_id');
               $res=$this->db->update('oc_account_payments', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				/** getting site settings **/
			    $this->db->select('*');
				$this->db->from('oc_site_settings');
				$query_settings=$this->db->get();
				$settingInfo=$query_settings->result();
				
				$adLimit= $settingInfo[0]->business_ad_limit;
				$imgUploads= $settingInfo[0]->business_image_uploads;
				$accExpires = date('Y-m-d H:i:s');
				$data2 = array(
						  'is_paid' => '1',
						  'ad_limit' => $adLimit ,
						  'pay_acc_expire_datetime' => $accExpires,
						 
						);
				/** update to paid account **/	
				$this->db->where('user_id',$user_id);					
			    $updateAccount=	$this->db->update('oc_ad_accounts',$data2);
				
				$res=$this->db->insert('oc_account_payments', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_profile($user_id,$first_name,$last_name,$gender,$address1,$address2,$city,$phone,$postalcode){
	  try{
			
			$data = array(
			'user_id' => $user_id ,
			'ac_first_name' => $first_name,
			'ac_last_name' => $last_name ,
			'ac_phone' => $phone ,
			'ac_address1' => $address1 ,
			'ac_address2' => $address2, 
			'ac_city' => $city,
			'ac_postalcode' => $postalcode ,
			'ac_gender' => $gender 

			);
			
			$this->db->where('user_id',$user_id);					
			$updateAccount=	$this->db->update('oc_ad_accounts',$data);
			
		    if($updateAccount){return true;} else{return false;}
			
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function backend_update_account($user_id,$ac_class,$ac_first_name,$ac_last_name,$ac_phone,
		$ac_gender,$ac_about,$ac_country){
	  try{
			
			$data = array(
			'ac_first_name' => $ac_first_name,
			'ac_last_name' => $ac_last_name ,
			'ac_phone' => $ac_phone ,
			'ac_about' => $ac_about ,
			'ac_country' => $ac_country, 
			'ac_gender' => $ac_gender 

			);
			
			$this->db->where('user_id',$user_id);					
			$updateAccount=	$this->db->update('oc_ad_accounts',$data);
			
		    if($updateAccount){return true;} else{return false;}
			
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	
	public function update_thumbnail($user_id,$thumb_path){
	   try{
			
			$data = array(
			'ac_thumbnail' => $thumb_path
			);
			
			$this->db->where('user_id',$user_id);					
			$updateAccount=	$this->db->update('oc_ad_accounts',$data);
			
		    if($updateAccount){return true;} else{return false;}
			
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	public function update_password($user_id,$old_password,$new_password){
	   try{
			
			$data = array(
				'ac_password' => md5($new_password)
			);
			
			$this->db->where('user_id',$user_id);					
			$this->db->where('ac_password',md5($old_password));					
			$updateAccount=	$this->db->update('oc_ad_accounts',$data);
			
		    if($updateAccount)
			{
				return true;
			}
			else{
				return false;
			}
			
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_preferences($user_id,$news_letters,$payment_threshold){
	   try{
			
			$data = array(
				'news_letters' => $news_letters,
				'payment_threshold' => $payment_threshold,
			);
			
			$this->db->where('user_id',$user_id);					
			$updateAccount=	$this->db->update('oc_ad_accounts',$data);
			
		    if($updateAccount)
			{
				return true;
			}
			else{
				return false;
			}
			
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	public function inactive_member($user_id){
			$data = array(
				'is_validated' => 0,
				'status' => 0,
			);
			
			$this->db->where('user_id',$user_id);					
			$inactivate=	$this->db->update('oc_ad_accounts',$data);
			
		    if($inactivate)
			{
				return true;
			}
			else{
				return 0;
			}
	}
	public function active_member($user_id){
			$data = array(
				'is_validated' => 1,
				'status' => 1,
			);
			
			$this->db->where('user_id',$user_id);					
			$inactivate=	$this->db->update('oc_ad_accounts',$data);
			
		    if($inactivate)
			{
				return true;
			}
			else{
				return 0;
			}
	}
	
	public function blacklist_whitelist_member($user_id,$block_status,$remarks,$notify_mail,$user_email){
		try{
			
			$data = array(
			'is_blocked' => $block_status,
			'block_remarks' => $remarks
			);
			
			$this->db->where('user_id',$user_id);					
			$updateAccount=	$this->db->update('oc_ad_accounts',$data);
			
		    if($updateAccount)
			{
				if($notify_mail==1){
					
					$this->db->select('*');
					$this->db->from('oc_app_settings');
					$app_query=$this->db->get();
					$resApp=$app_query->result();
			
					if(@$resApp[0]->default_from_mail!=''){
						$from_emails=$resApp[0]->default_from_mail;
					}else{
						$from_emails = 'contact@onetextglobal.com';
					}
					
					 if(@$resApp[0]->app_default_name!=''){
						$app_name=$resApp[0]->app_default_name;
					}else{
						$app_name = 'OTGApp';
					}
				
				
		              
					$full_name       =  $app_name." \'s Member";
					$notify_from     =  $from_emails;
					
					if($block_status==1){
						$notify_subject  =  $app_name. _("Your account turned to blacklist");
						$notify_body     =  sprintf(_("Dear %s"), $full_name) . ", <br><br> " . sprintf(_("Your campaign account with %s has been blacklisted."), $app_name). " <br>";
						$notify_body.= sprintf(_("Please contact %s 's support team for more details."), $app_name) . " <br>";
						$notify_body.= "<strong>---- " . _('Remarks') . " ----</strong><br>".$remarks."<br><br>";
						$notify_body.= _("Thank you") . ".<br><br>" . _("Best regards") . ",<br><strong>".$app_name."</strong>";
						
					}else{
						
						$notify_subject  =  $app_name. _("Congrats your account turns to whitelist");
						$notify_body     =  sprintf(_("Dear %s"), $full_name) . ", <br><br> " . sprintf(_("Your campaign account with %s has turned to whitelist."), $app_name) . "<br>";
						$notify_body.= sprintf(_("You may use campaigning services now, please contact %s 's support team for more details."), $app_name) . "<br>";
						$notify_body.= "<strong>---- " . _('Remarks') . " ----</strong><br>".$remarks."<br><br>";
						$notify_body.=_("Thank you") . ".<br><br>" . _("Best regards") . ",<br><strong>".$app_name."</strong>";
					}
					
					$notify_to  = $user_email;
					
				
					//Sending SMTP Mail
					// Loads the email library
					$this->load->library('email');
					// FCPATH refers to the CodeIgniter install directory
					// Specifying a file to be attached with the email
					//$file = FCPATH . 'license.txt';
					// Defines the email details
					$this->email->from($notify_from, $app_name);
					$this->email->to($notify_to);
					//$this->email->cc('another@example.com');
					//$this->email->bcc('one-another@example.com');
					$this->email->subject($notify_subject);
					$this->email->message($notify_body);
					//$this->email->attach($file);
					// The email->send() statement will return a true or false
					// If true, the email will be sent
					if ($this->email->send()) {
							$data = array(
							'block_notifymail' => '1'
							);

							$this->db->where('user_id',$user_id);					
							$blockNotify=	$this->db->update('oc_ad_accounts',$data);
							if($blockNotify){
								return true;
							}else{
								return 0;
							}
					}else{
						return 2;
					}
				}else{
					return true;
				}
			} 
			else
			{
				return 0;
			}
			
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function get_profile_thumbnail($user_id){
		$this -> db -> select('ac_thumbnail,ac_gender');
		$this -> db -> from('oc_ad_accounts');
		$this->db->where('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function member_details_bymail($email){
		$this -> db -> select('ac_first_name,ac_last_name,ac_country,user_id,ac_email');
		$this -> db -> from('oc_ad_accounts');
		$this->db->where('ac_email',$email);
		$this->db->limit(1);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function list_all_members(){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this -> db -> join('oc_countries','oc_countries.country_code=oc_ad_accounts.ac_country');
		$this -> db -> group_by('oc_ad_accounts.id');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function list_member_credit_threshold(){
		$this -> db -> select('oc_ad_accounts.ac_first_name,oc_ad_accounts.ac_last_name,
		oc_ad_accounts.ac_phone,oc_ad_accounts.user_id,oc_ad_accounts.mobile_validate,oc_ad_accounts.payment_threshold');
		$this -> db -> from('oc_ad_accounts');
		$this -> db -> join('oc_countries','oc_countries.country_code=oc_ad_accounts.ac_country');
		$this->db->where('status',1);
		$this->db->where('payment_threshold <> ','');
		$this -> db -> group_by('oc_ad_accounts.id');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_member_details($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this->db->where('user_id',$user_id);
		$this -> db -> join('oc_countries','oc_countries.country_code=oc_ad_accounts.ac_country');
		$this -> db -> group_by('oc_ad_accounts.id');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function list_inactive_members(){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this->db->where('status',0);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function list_latest_members(){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this->db->order_by('id','desc');
		$this->db->limit(5);
		$query = $this -> db -> get();
		
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function list_all_inactive_members(){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this->db->where('status',0);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_sms_service($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_triggers');
		$this->db->where('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_sms_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_back_settings');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	public function get_account_wallet($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_account_wallet');
		$this->db->where('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	public function get_wallet_transaction_byuser($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_payments');
		$this->db->where('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	
	public function all_wallet_transactions(){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_payments');
		$this->db->join('oc_ad_accounts','oc_ad_accounts.user_id=oc_ad_payments.user_id');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	public function grandtotal_wallet_transactions(){
		$this -> db -> select_sum('ad_payment');
		$this -> db -> from('oc_ad_payments');
		$this->db->where_in('payment_status',1);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	
	public function add_user_notification($notify_subject,$notify_from,$notify_body,$notify_to,$deliver_status){
	$data = array(
			  'notify_subject' => $notify_subject ,
			  'notify_from' =>  $notify_from,
			  'notify_message' => $notify_body ,
			  'notify_to' => $notify_to ,
			  'deliver_status' => $deliver_status ,
			  
			);
	
	$this->db->insert('oc_user_notifications', $data); 
	return true;
	}
  
  public function add_user_password_notification($notify_subject,$notify_from,$notify_body,$notify_to,$deliver_status){
	$data = array(
			  'notify_subject' => $notify_subject ,
			  'notify_from' =>  $notify_from,
			  'notify_message' => $notify_body ,
			  'notify_to' => $notify_to ,
			  'deliver_status' => $deliver_status ,
			  
			);
	
	$this->db->insert('oc_user_password_notifications', $data); 
	return true;
	}
	
	public function list_users(){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_users');
		$this -> db -> where('status ', $status_id); 
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	public function list_account_classes(){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_account_class');
		$this -> db -> where('status ', $status_id); 
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	
	public function list_account_classes_notById($ac_class){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_account_class');
		$this -> db -> where('status ', $status_id); 
		$this -> db -> where('acc_id <> ', $ac_class);
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	public function list_current_user($user_id){
		
		$where=array('status'=>'1','user_id'=>$user_id);
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this -> db -> where($where); 
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	public function list_backend_user($user_id){
		
		$where=array('status'=>'1','user_id'=>$user_id);
		$this -> db -> select('*');
		$this -> db -> from('oc_users');
		$this -> db -> where($where); 
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	public function get_mobactive_status($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_mobile_validation');
		$this->db->where('user_id',$user_id);
		$this->db->where('status','1');
		$this->db->limit(1);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_mobinactive_status($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_mobile_validation');
		$this->db->where('user_id',$user_id);
		$this->db->where('status','0');
		$this->db->limit(1);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function load_mobile_token($userID,$userMobile,$userPass,$userStatus) {
		$data = array(
			  'user_id' => $userID ,
			  'mobile_number' =>  $userMobile,
			  'verification_code' => $userPass ,
			  'status' => $userStatus ,
			  
			);
	
		$ins=$this->db->insert('oc_mobile_validation', $data); 
		if($ins){
		return true;
		}
		
	}
	
	
	public function update_user_profile($group_id,$user_id,$first_name,$last_name,$email_id,$mobile_number){
		$data=array('user_group'=>$group_id,
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'email_id'=>$email_id,
		'mobile_number'=>$mobile_number,
		);
		
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}
		
	}
	public function update_stage_user($user_id,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status){
		$data=array('user_group'=>$user_groups,
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'user_email'=>$email_id,
		'user_mobile'=>$telephone,
		'user_phone'=>$telephone2,
		'status'=>$user_status,
		);
		
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
	public function update_user_password($user_id,$new_password){
		
	    $data=array('password'=>$new_password);
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}
	}
	
	public function get_user_group($group_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_user_groups');
		$this -> db -> where('group_id = ' . "'" . $group_id . "'"); 
		$query = $this -> db -> get();
		
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	
	
	public function delete_user($user_id){
		
		$data = array('status'=>'0');
		$this -> db -> where('user_id', $user_id); 
		$query=$this -> db -> update('oc_users',$data);
		
		if($query){
			return true;
		 }
	}
	
	public function delete_member($user_id,$id){
		
		$this -> db -> where('id', $id); 
		$this -> db -> where('user_id', $user_id); 
		$query=$this -> db -> delete('oc_ad_accounts');
		
		if($query){
			
			//// delete dependent campaigns 
			$this -> db -> where('user_id', $user_id); 
		    $this -> db -> delete('oc_sms_member_campaign');
			
			//// delete memeber sms settings 
			$this -> db -> where('user_id', $user_id); 
		    $this -> db -> delete('oc_sms_member_settings');
			
			//// delete wallet history
			$this -> db -> where('user_id', $user_id); 
		    $this -> db -> delete('oc_account_wallet');
			
			//// delete payment history
			$this -> db -> where('user_id', $user_id); 
		    $this -> db -> delete('oc_account_payments');
			
			//// delete contacts
			$this -> db -> where('user_id', $user_id); 
		    $this -> db -> delete('oc_campaign_contacts');
			
			//// delete groups
			$this -> db -> where('user_id', $user_id); 
		    $this -> db -> delete('oc_campaign_groups');
			
			//// delete card info
			$this -> db -> where('user_id', $user_id); 
		    $this -> db -> delete('oc_card_master');
			
			//// delete notifications
			$this -> db -> where('user_id', $user_id); 
		    $this -> db -> delete('oc_notifications');
			
			return true;
			
		 }else{
			 return 0;
		 }
	}
	
	
	
	
	
} 
?>