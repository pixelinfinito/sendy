<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Twilio_Model extends CI_Model
{
	
	
	public function get_mobactive_status($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_mobile_validation');
		$this->db->where('user_id',$user_id);
		$this->db->where('status','1');
		$this->db->limit(1);
		
		//$this->db->order_by('id','desc');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_mobinactive_status($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_mobile_validation');
		$this->db->where('user_id',$user_id);
		$this->db->where('status','0');
		$this->db->limit(1);
		$this->db->order_by('id','desc');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function load_mobile_token($userID,$userMobile,$userPass,$userStatus) {
		$data = array(
			  'user_id' => $userID ,
			  'mobile_number' =>  $userMobile,
			  'verification_code' => $userPass ,
			  'status' => $userStatus ,
			  
			);
	
		$query=$this->db->insert('oc_mobile_validation', $data); 
		if($query){
			return true;
		}else
		{
			return 0;
		}
		
	}
	public function update_mobile_token($userId,$parseAuthkey){
		
		
		    $data = array(
			'status' => '1'

			);
			 
		     $this->db->where('user_id',$userId);
		     $this->db->where('verification_code',$parseAuthkey);
			 
			$query=$this->db->update('oc_mobile_validation', $data); 
			if($query){
				 $data2 = array(
						'mobile_validate' => '1',
						'mobile_validate_code' => $parseAuthkey

				);
				$this->db->where('user_id',$userId);
				$query2=$this->db->update('oc_ad_accounts', $data2); 
				if($query2){
					return true;
				}else
				{
					return 0;
				}
			}
			
	}
	
	
	
} 
?>