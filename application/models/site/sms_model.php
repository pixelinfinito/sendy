<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Sms_Model extends CI_Model
{
	
	
	public function add_sms_contact($userId,$full_name,$mobile,$email,$country,$group){
		try{
			
			//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$addedOn=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$addedOn=date("Y-m-d H:i:s");
			}
			
			
			$data = array(
			'user_id' => $userId ,
			'contact_name' => $full_name,
			'contact_mobile' => $mobile ,
			'contact_email' => $email ,
			'contact_group 	' => $group ,
			'country_code' => $country ,
			'updated_on' => $addedOn ,
			'status' => '1');
						
						
			$this->db->select('*');
			$this->db->from('oc_campaign_contacts');
			$this->db->where('contact_mobile',$mobile);
			$this->db->where('contact_group',$group);
			$this->db->where('user_id',$userId);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   return 0;
			}
			else{
				
				$query=$this->db->insert('oc_campaign_contacts', $data); 
				if($query)
				{
					
					return true;
				} 
				else
				{
					return false;
				}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_sms_contact($user_id,$full_name,$mobile,$email,$country,$group,$contact_id){
		try{
			
			//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$addedOn=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$addedOn=date("Y-m-d H:i:s");
			}
			
			
			$data = array(
			'contact_name' => $full_name,
			'contact_mobile' => $mobile ,
			'contact_email' => $email ,
			'contact_group 	' => $group ,
			'country_code' => $country ,
			'updated_on' => $addedOn ,
			'status' => '1' );
						
			$this->db->where('c_id',$contact_id);	
			//$this->db->where('user_id',$user_id);				
			$query2=$this->db->update('oc_campaign_contacts',$data);
			
			if($query2){
			   return true;
			}
			else{
				
				return false;
				
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_smsgroup($user_id,$group_id,$group_name){
		try{
			
			$data = array(
			'group_name' => $group_name,
			'status' => '1' );
						
			$this->db->where('cgroup_id',$group_id);	
			$this->db->where('user_id',$user_id);				
			$query=$this->db->update('oc_campaign_groups',$data);
			
			if($query){
			   return true;
			}
			else{
				
				return 0;
				
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_ttitle($user_id,$t_code,$t_title){
		try{
			
			$data = array(
			'template_title' => $t_title,
			'status' => '1' );
						
			$this->db->where('template_code',$t_code);	
			$this->db->where('user_id',$user_id);				
			$query=$this->db->update('oc_template_titles',$data);
			
			if($query){
			   return true;
			}
			else{
				
				return 0;
				
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	public function start_sms_service($userId,$token){
		   
		   //** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$timeStamp=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$timeStamp=date("Y-m-d H:i:s");
			}
			
			switch($token)
			{
				
				case 'start':
					$data1 = array(
					  'user_id' => $userId ,
					  'trigger_start_on' => $timeStamp,
					  'trigger_status' => '1' 
					);
					$this->db->select('*');
					$this->db->from('oc_sms_triggers');
					$this->db->where('user_id',$userId);
					$query=$this->db->get();

					if($query->num_rows()>0){
						$data2= array(
						  'trigger_start_on' => $timeStamp,
						  'trigger_status' => '1' 
						);
						$this->db->where('user_id',$userId);
						$query=$this->db->update('oc_sms_triggers', $data2); 
					   if($query){return true;}else{return false;}
					}
					else{

						$query=$this->db->insert('oc_sms_triggers', $data1); 
						if($query){return true;}else{return false;}

					}
				 break;
				 
				 case 'stop':
					$data2= array(
						  'trigger_stop_on' => $timeStamp,
						  'trigger_status' => '0' 
						);
						$this->db->where('user_id',$userId);
						$query=$this->db->update('oc_sms_triggers', $data2); 
					  if($query){return true;}else{return false;}
				 break;
			}		
						
			
	}
	
	public function copy_single_contact($group_id,$user_id,$contact_id,$c_mobile,$c_name,$c_email,$country_code){
			$this->db->select('*');
			$this->db->from('oc_campaign_contacts');
			$this->db->where('user_id',$user_id);
			$this->db->where('contact_mobile',$c_mobile);
			$this->db->where('contact_group',$group_id);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
				return 0;
			}else{
				$data=array('contact_group'=>$group_id,
				'contact_name'=>$c_name,
				'contact_mobile'=>$c_mobile,
				'contact_email'=>$c_email,
				'country_code'=>$country_code,
				'user_id'=>$user_id,
				'status'=>'1'
				);
				$query=$this->db->insert('oc_campaign_contacts',$data);
				if($query){
					return true;
				}
				
			}
	}
	
	public function copy_selected_contacts($contact_ids,$user_id,$c_group){
		  
		    $contactArr=array();
		    $exp_cids=explode(',',$contact_ids);
			for($i=0;$i<count($exp_cids);$i++){
				array_push($contactArr,$exp_cids[$i]);
			}
			
			$dupArr=array();
			$successArr=array();
			
			$this->db->select('*');
			$this->db->from('oc_campaign_contacts');
			$this->db->where_in('user_id',$user_id);
			$this->db->where_in('c_id',$contactArr);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
				
				$c=0;
				// cross check with group if contact already existed in same group just ignore
				foreach($query->result() as $row){
				
                  $this->db->select('*');
			      $this->db->from('oc_campaign_contacts');
			      $this->db->where('contact_group',$c_group);
				  $this->db->where('contact_mobile',$row->contact_mobile);
			      $cross_query=$this->db->get();				
				  if($cross_query->num_rows()>0){
						//return 0;
						array_push($dupArr,$row->contact_mobile);
				  }else{
					 
						$data=array('contact_group'=>$c_group,
						'contact_name'=>$row->contact_name,
						'contact_mobile'=>$row->contact_mobile,
						'contact_email'=>$row->contact_email,
						'country_code'=>$row->country_code,
						'user_id'=>$user_id,
						'status'=>'1'
						);
						$ins_query=$this->db->insert('oc_campaign_contacts',$data);
						if($ins_query){
							array_push($successArr,$row->contact_mobile);
						}
				  }	
				  $c++;
			    }
				return $successArr;
				
			}else{
				return 0;
				
			}
	}
	
	public function move_selected_contacts($contact_ids,$user_id,$c_group){
		 
		    $contactArr=array();
		    $exp_cids=explode(',',$contact_ids);
			for($i=0;$i<count($exp_cids);$i++){
				array_push($contactArr,$exp_cids[$i]);
			}
			
			$dupArr=array();
			$successArr=array();
			
			$this->db->select('*');
			$this->db->from('oc_campaign_contacts');
			$this->db->where_in('user_id',$user_id);
			$this->db->where_in('c_id',$contactArr);
			$query=$this->db->get();
			
			
			if($query->num_rows()>0){
				
				$c=0;
				// cross check with group if contact already existed in same group just ignore
				foreach($query->result() as $row){
				
                  $this->db->select('*');
			      $this->db->from('oc_campaign_contacts');
			      $this->db->where('contact_group',$c_group);
				  $this->db->where('contact_mobile',$row->contact_mobile);
				  $this->db->where('user_id',$user_id);
			      $cross_query=$this->db->get();				
				  if($cross_query->num_rows()>0){
						//return 0;
						array_push($dupArr,$row->contact_mobile);
				  }else{
					 
						$data=array('contact_group'=>$c_group);
						$this->db->where('c_id',$row->c_id);
						$this->db->where('contact_mobile',$row->contact_mobile);
						$this->db->where('user_id',$user_id);
						$up_query=$this->db->update('oc_campaign_contacts',$data);
						
						if($up_query){
							array_push($successArr,$row->contact_mobile);
						}
						
				
				  }	
				  $c++;
			    }
				return $successArr;
				
			}else{
				return 0;
				
			}
	}
	public function delete_selected_contacts($contact_ids,$user_id){
		$contactArr=array();
		$exp_cids=explode(',',$contact_ids);
		for($i=0;$i<count($exp_cids);$i++){
		array_push($contactArr,$exp_cids[$i]);
		}

		
		$successArr=array();
		$this->db->select('*');
		$this->db->from('oc_campaign_contacts');
		$this->db->where_in('user_id',$user_id);
		$this->db->where_in('c_id',$contactArr);
		$query=$this->db->get();
		
		
		if($query->num_rows()>0){
			
			$c=0;
			foreach($query->result() as $row){
			$this->db->where('c_id',$row->c_id);
			$this->db->where('user_id',$user_id);
			$del_query=$this->db->delete('oc_campaign_contacts');
			
			$c++;
			}
			return true;
		}else{
			return 0;
		}
	}
	
	public function move_single_contact($group_id,$user_id,$contact_id,$c_mobile){
			$this->db->select('*');
			$this->db->from('oc_campaign_contacts');
			$this->db->where('user_id',$user_id);
			$this->db->where('contact_mobile',$c_mobile);
			$this->db->where('contact_group',$group_id);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
				return 0;
			}else{
				$data=array('contact_group'=>$group_id);
				$this->db->where('c_id',$contact_id);
				$this->db->where('contact_mobile',$c_mobile);
				$this->db->where('user_id',$user_id);
				$query=$this->db->update('oc_campaign_contacts',$data);
				if($query){
					return true;
				}
				
			}
	}
	public function add_template_title($template_code,$template_title,$user_id){
		  $this->db->select('*');
			$this->db->from('oc_template_titles');
			$this->db->where('user_id',$user_id);
			$this->db->where('template_code',$template_code);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
				return 0;
			}else{
				$data=array('template_code'=>$template_code,
				'template_title'=>$template_title,
				'user_id'=>$user_id,
				'status'=>'1'
				);
				$query=$this->db->insert('oc_template_titles',$data);
				if($query){
					return true;
				}
				
			}
	}
	public function update_sms_campaign_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status){
	
			//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$timeStamp=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$timeStamp=date("Y-m-d H:i:s");
			}
			
			if($deliver_status!=3){
					 $data = array(
					  'message_id' => $message_id ,
					  'deliver_status' => $deliver_status,
					  'remarks' => $remarks,
					  'do_submit' => $timeStamp,
					  'do_sent' => $timeStamp,
					  'status' => '1'
					  );
			}else{
					$data = array(
					  'message_id' => $message_id ,
					  'deliver_status' => $deliver_status,
					  'remarks' => $remarks,
					  'do_submit' => $timeStamp,
					  'status' => '1'
					  );
			}
			
			$this->db->where('campaign_id',$campaign_id);
			$this->db->where('user_id',$user_id);
			$query=$this->db->update('oc_sms_member_campaign', $data); 
			if($query){return true;}else{return false;}
	}
	public function update_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit){
		//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$timeStamp=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$timeStamp=date("Y-m-d H:i:s");
			}
			$data = array(
					  'deliver_status' => $deliver_status,
					  'remarks' => $remarks,
					  'do_sent' => $timeStamp,
					  'status' => '1'
					  );
					  
			$this->db->where('campaign_id',$campaign_id);
			$this->db->where('user_id',$user_id);
			$query=$this->db->update('oc_sms_member_campaign', $data); 
			if($query){
				
				// deducting wallet credit
				$cost_update= floatval($accountCredit) - floatval($unit_cost);
				$data2 = array(
					  'balance_amount' => $cost_update,
					  );
				$this->db->where('user_id',$user_id);
				$query2=$this->db->update('oc_account_wallet', $data2); 
			    if($query2){
					return true;
				}
			}
			else{
				return false;
			}
	}
	
	public function get_queued_sms($user_id){
		$this -> db -> select('*');
		$this->db->where_in('user_id',$user_id);
		$this->db->where_in('deliver_status','3'); // pulling only queued sms
		$this -> db -> from('oc_sms_member_campaign');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_sms_remarks($campaign_id,$user_id){
		$this -> db -> select('*');
		$this->db->where('user_id',$user_id);
		$this->db->where('campaign_id',$campaign_id);
		$this -> db -> from('oc_sms_member_campaign');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_sms_priceby_country($country_code){
		$this -> db -> select('*');
		$this->db->where('country_code',$country_code);
		$this -> db -> from('oc_sms_prices');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
		
	}
	public function list_smsprice_aivailable_countries(){
		$this -> db -> select('*');
		$this -> db -> from('oc_countries');
		$this->db->join('oc_sms_prices','oc_sms_prices.country_code=oc_countries.country_code');
		$this->db->join('oc_country_callcodes','oc_country_callcodes.country_code=oc_countries.country_code');
		$this->db->group_by('oc_countries.country_code');
		$this->db->order_by('oc_countries.country_code');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	public function cross_check_callingcode($calling_code){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_prices');
		$this->db->join('oc_country_callcodes','oc_country_callcodes.country_code=oc_sms_prices.country_code');
		$this->db->where('oc_country_callcodes.calling_code',$calling_code);
		$this->db->limit(1);
		$query=$this->db->get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}else{
			return 0;
		}
	}
	public function get_projected_price($group_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_contacts');
		$this->db->join('oc_sms_prices','oc_sms_prices.country_code=oc_campaign_contacts.country_code');
		$this->db->where_in('oc_campaign_contacts.contact_group',$group_id);
		$this->db->group_by('oc_campaign_contacts.c_id');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_projected_price_bycontact($contact_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_contacts');
		$this->db->join('oc_sms_prices','oc_sms_prices.country_code=oc_campaign_contacts.country_code');
		$this->db->where_in('oc_campaign_contacts.c_id',$contact_id);
		$this->db->group_by('oc_campaign_contacts.c_id');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_groups_byuser($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_groups');
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	public function get_single_contact_bycid($contact_id,$user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_contacts');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_campaign_contacts.contact_group');
		//$this->db->where('user_id',$user_id);
		$this->db->where('c_id',$contact_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_contacts_byuser($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_contacts');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_campaign_contacts.contact_group');
		$this->db->join('oc_countries','oc_countries.country_code=oc_campaign_contacts.country_code');
		$this->db->join('oc_country_callcodes','oc_country_callcodes.country_code=oc_campaign_contacts.country_code');
		
		$this->db->where_in('oc_campaign_contacts.user_id',$user_id);
		$this->db->group_by('oc_campaign_contacts.c_id');
		$this->db->order_by('oc_campaign_contacts.c_id','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_caller_numbers($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_settings');
		$this->db->where_in('user_id',$user_id);
		//$this->db->where_in('status','1');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_campaign_history($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
		$this->db->where_in('oc_sms_member_campaign.user_id',$user_id);
		//$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_campaign_success_history($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
		$this->db->where_in('oc_sms_member_campaign.user_id',$user_id);
		$this->db->where('oc_sms_member_campaign.status', 1);
		$this->db->where('oc_sms_member_campaign.deliver_status', 1);
		
		$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_campaign_failed_history($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
		$this->db->where_in('oc_sms_member_campaign.user_id',$user_id);
		$this->db->where('oc_sms_member_campaign.status', 1);
		$this->db->where('oc_sms_member_campaign.deliver_status', 0);
		$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_campaign_history_bydates($user_id,$fromDate,$toDate){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
		$this->db->where_in('oc_sms_member_campaign.user_id',$user_id);
		$this->db->where('do_submit > ',$fromDate);
		$this->db->where('do_submit < ',$toDate);
		$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	
	public function get_campaign_history_byargs($user_id,$campaign_code){
		
		if($user_id!=''){
			$this -> db -> select('*');
			$this -> db -> from('oc_sms_member_campaign');
			$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
			$this->db->where_in('oc_sms_member_campaign.user_id',$user_id);
			$this->db->where_in('oc_sms_member_campaign.campaign_code',$campaign_code);
			//$this->db->group_by('oc_sms_member_campaign.campaign_code');
			$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		}else{
			$this -> db -> select('*');
			$this -> db -> from('oc_sms_member_campaign');
			$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
			$this->db->where_in('oc_sms_member_campaign.campaign_code',$campaign_code);
			//$this->db->group_by('oc_sms_member_campaign.campaign_code');
			$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		}
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	
	public function get_all_campaigns_history(){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
		//$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_recent_campaigns($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
		$this->db->where_in('oc_sms_member_campaign.user_id',$user_id);
		$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		$this->db->limit(5);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function recent_campaigns_atadmin(){
		$this -> db -> select('oc_sms_member_campaign.*,oc_sms_member_campaign.status cmp_status,oc_campaign_groups.*,
		oc_ad_accounts.*,oc_countries.*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->join('oc_campaign_groups','oc_campaign_groups.cgroup_id=oc_sms_member_campaign.sms_group');
		$this->db->join('oc_ad_accounts','oc_ad_accounts.user_id=oc_sms_member_campaign.user_id');
		$this->db->join('oc_countries','oc_countries.country_code=oc_ad_accounts.ac_country');
		$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$this->db->order_by('oc_sms_member_campaign.campaign_id','desc');
		$this->db->limit(8);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_campaign_overview($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->where_in('oc_sms_member_campaign.user_id',$user_id);
		$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$this->db->order_by('oc_sms_member_campaign.campaign_code','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_all_campaigns(){
		$this -> db -> select('oc_sms_member_campaign.*,oc_sms_member_campaign.status cmp_status,oc_ad_accounts.*');
		$this -> db -> from('oc_sms_member_campaign');
		$this -> db -> join('oc_ad_accounts','oc_ad_accounts.user_id=oc_sms_member_campaign.user_id');
		$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$this->db->order_by('oc_sms_member_campaign.campaign_code','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_all_scheduled_campaigns(){
		$this -> db -> select('oc_sms_member_campaign.*,oc_sms_member_campaign.status cmp_status,oc_ad_accounts.*');
		$this -> db -> from('oc_sms_member_campaign');
		$this -> db -> join('oc_ad_accounts','oc_ad_accounts.user_id=oc_sms_member_campaign.user_id');
		$this->db->where_in('oc_sms_member_campaign.is_scheduled','1');
		$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$this->db->order_by('oc_sms_member_campaign.campaign_code','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function total_campaign_count(){
		$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$query = $this -> db -> get('oc_sms_member_campaign');
		
		if($query->num_rows()>0){
		
			return count($query->result());
		}else{return 0;}
	}
	public function total_pending_campaign_count(){
		$this->db->where('status', 0);
		$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$query = $this -> db -> get('oc_sms_member_campaign');
		
		if($query->num_rows()>0){
		
			return count($query->result());
		}else{return 0;}
	}
	
	public function total_successful_smscount(){
		
		$this->db->where('status', '1');
		$this->db->where('deliver_status', '1');
		$query = $this -> db -> get('oc_sms_member_campaign');
		if($query->num_rows()>0){
		
			return count($query->result());
		}else{return 0;}

	}
	public function total_unsuccessful_smscount(){
		
		$this->db->where('status', '1');
		$this->db->where('deliver_status', '0');
		$query = $this -> db -> get('oc_sms_member_campaign');
		if($query->num_rows()>0){
		
			return count($query->result());
		}else{return 0;}

	}
	
	public function get_success_count($campaigns_code,$user_id){
		
		$this->db->where('campaign_code', $campaigns_code);
		$this->db->where('user_id', $user_id);
		$this->db->where('status', '1');
		$this->db->where('deliver_status', '1');
		$this->db->from('oc_sms_member_campaign');
		$count=$this->db->count_all_results();
		if($count>0){
			return $count;
		}else{
			return 0;
		}
	}
	
	
	public function get_unsuccess_count($campaigns_code,$user_id){
		
		$this->db->where('campaign_code', $campaigns_code);
		$this->db->where('user_id', $user_id);
		$this->db->where('status', '1');
		$this->db->where('deliver_status', 0);
		$this->db->from('oc_sms_member_campaign');
		$count=$this->db->count_all_results();
		if($count>0){
			return $count;
		}else{
			return 0;
		}

	}
	
	public function get_campaign_unique_years($user_id){
	    
		$this->db->select('YEAR(do_submit) as parse_year ,user_id');
        $this->db->where('user_id', $user_id);
		$this->db->group_by('YEAR(do_submit)');
		$this->db->from('oc_sms_member_campaign');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
		
	}
	public function get_campaign_unique_countries($user_id,$year){
	    
		$this->db->select('t1.campaign_code,t1.user_id,t1.do_sent,t1.country_code,t2.*');
		$this->db->from('oc_sms_member_campaign t1');
		$this->db->join('oc_countries t2','t2.country_code=t1.country_code','left');
		$this->db->where('user_id', $user_id);
		$this->db->where("YEAR(t1.do_sent)",$year);
		$this->db->group_by('t1.country_code');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
		
	}
	public function get_smscountry_graphcount($country_code,$user_id,$year){
		$this->db->select('COUNT(campaign_id) as smscount ,do_submit,do_sent,country_code,user_id');
        $this->db->where('user_id', $user_id);
	    $this->db->where('country_code', $country_code);
		$this->db->where("YEAR(do_sent)",$year);
		//$this->db->group_by('country_code');
		$this->db->from('oc_sms_member_campaign');
		$count=$this->db->count_all_results();
		if($count>0){
			return $count;
		}else{
			return 0;
		}

		
	}
	
	public function get_campaign_graph_yearby($user_id,$year){
	    
		$this->db->select('*');
        $this->db->where('user_id', $user_id);
		$this->db->where("YEAR(do_sent)",$year);
		$this->db->where('status', 1);
		$this->db->group_by('campaign_code');
		$this->db->from('oc_sms_member_campaign');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
		
	}
	
	public function get_sms_graphcount($month,$year,$user_id){
		
		$this->db->select('*');
		$this->db->where("YEAR(do_sent)",$year);
		$this->db->where("MONTH(do_sent)",$month);
		$this->db->where('user_id', $user_id);
		$this->db->where('status', 1);
		
		$query = $this -> db -> get('oc_sms_member_campaign');
		//echo $this->db->last_query();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}

	}
	
	public function get_success_count_byuser($user_id){
		
		$this->db->where('user_id', $user_id);
		$this->db->where('status', '1');
		$this->db->where('deliver_status', '1');
		$this->db->from('oc_sms_member_campaign');
		$count=$this->db->count_all_results();
		if($count>0){
			return $count;
		}else{
			return 0;
		}
	}
	
	public function get_unsuccess_count_byuser($user_id){
		
		$this->db->where('user_id', $user_id);
		$this->db->where('status', '1');
		$this->db->where('deliver_status', 0);
		$this->db->from('oc_sms_member_campaign');
		$count=$this->db->count_all_results();
		if($count>0){
			return $count;
		}else{
			return 0;
		}

	}
	
	
	public function get_campaign_contacts_count($campaigns_code,$user_id){
		
		$this->db->where('campaign_code', $campaigns_code);
		$this->db->where('user_id', $user_id);
		$this->db->from('oc_sms_member_campaign');
		$count=$this->db->count_all_results();
		if($count>0){
			return $count;
		}else{
			return 0;
		}

	}
	public function get_campaign_headline($campaigns_code,$user_id){
		$this->db->select('*');
		$this->db->from('oc_sms_member_campaign');
		$this->db->where('campaign_code', $campaigns_code);
		$this->db->where('user_id', $user_id);
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}

	}
	public function get_campaign_schedules($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->where_in('oc_sms_member_campaign.user_id',$user_id);
		$this->db->where_in('oc_sms_member_campaign.is_scheduled','1');
		$this->db->group_by('oc_sms_member_campaign.campaign_code');
		$this->db->order_by('oc_sms_member_campaign.campaign_code','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_template_titles($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_template_titles');
		$this->db->where_in('user_id',$user_id);
		$this->db->order_by('template_id','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function get_campaign_waiting($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->where_in('user_id',$user_id);
		$this->db->where_in('deliver_status','2');
		$this->db->where_in('is_scheduled','0'); // avoiding scheduled sms
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function buy_caller_number($cn_type,$cnp_reference,$cn_price,$cn_period,$cn_remarks,$payment_method,$cn_country,$cn_currency,$walletBalance,$user_id){
		/** Checking  duplications */
		$this->db->select('*');
		$this->db->from('oc_ad_payments');
		$this->db->where('payment_reference',$cnp_reference);
		$query=$this->db->get();
		
		if($query->num_rows()>0){
		  return 0;
		  echo _("Duplication occured, please refresh your page.");
		}
		else{
			
			
			$remoteCountry=$this->config->item('remote_country_code');
			//** Gettings timezone based on country code **//
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();
			
			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				$timezone=$rTimeRow[0]->zone_name;
			   
				
			}else{
				$timezone="Asia/Brunei";
			}
			date_default_timezone_set($timezone);
			$post_date = date('Y-m-d H:i:s');
			$expired_days = $cn_period * 30;
			$exp_date = date('Y-m-d H:i:s',strtotime($post_date) + (24*3600*$expired_days)); 
			

			$data = array(
			  'payment_reference' => $cnp_reference ,
			  'user_id' => $user_id ,
			  'ad_payment' => $cn_price,
			  'payment_method' => $payment_method,
			  'date_of_payment' => $post_date,
			  'payment_remarks'=> $cn_remarks,
			  'payment_status' => '1' 
			  
			);
			
			$data2 = array(
			  'user_id' => $user_id ,
			  'country' => $cn_country,
			  'cn_type'=> $cn_type,
			  'price' => $cn_price,
			  'cn_currency'=> $cn_currency,
			  'subscription_period' => $cn_period,
			  'purchased_on' => $post_date,
			  'expired_on' => $exp_date,
			  'payment_status' => '1',
			  'status' => '2' 			  
			  
			);
			
			
			
            /// Payment with wallet money
			$sql=$this->db->insert('oc_ad_payments', $data); 
			if($sql){
				    
					/// Deducting balance on AD Payment
					
					$accWalletBalance=floatval($walletBalance)-floatval($cn_price);
					$data_wallet = array('balance_amount' => $accWalletBalance);
					$this->db->where('user_id',$user_id);
					$query=$this->db->update('oc_account_wallet', $data_wallet); 
					if($query){
						
						/// Buying number
						$buyQuery=$this->db->insert('oc_sms_member_settings', $data2); 
						if($buyQuery){
							return true;
						}
					}else{
						return 0;
					}
			
				  
		
			}else{
					return 0;
			}
		}
	}
	
	public function campaign_wallet_invoice($ad_reference,$ad_payment,$payment_method,$payment_reference,
			 $payment_remarks,$payment_status,$user_id,$campign_code){
		/** Checking  duplications */
		$this->db->select('*');
		$this->db->from('oc_ad_payments');
		$this->db->where('payment_reference',$payment_reference);
		$query=$this->db->get();
		
		if($query->num_rows()>0){
		  return 0;
		  echo _("Duplication occured, please refresh your page.");
		}
		else{
			
			
			$remoteCountry=$this->config->item('remote_country_code');
			//** Gettings timezone based on country code **//
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();
			
			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				$timezone=$rTimeRow[0]->zone_name;
			   
				
			}else{
				$timezone="Asia/Brunei";
			}
			date_default_timezone_set($timezone);
			$paymentDatetime = date('Y-m-d H:i:s');
			
			

			$data = array(
			  'ad_reference' => $ad_reference ,
			  'payment_reference' => $payment_reference ,
			  'user_id' => $user_id ,
			  'ad_payment' => $ad_payment,
			  'payment_method' => $payment_method,
			  'date_of_payment' => $paymentDatetime,
			  'campaign_code'=>$campign_code,
			  'payment_remarks'=> $payment_remarks,
			  'payment_status' => $payment_status
			  
			);
			
			/// Payment with wallet money
			$sql=$this->db->insert('oc_ad_payments', $data); 
			if($sql){
				   return true;
			
			}else{
					return 0;
			}
		}
	}
	
	public function process_sender_id($userId,$sender_number,$sender_name,$tid){
			$data = array('sender_id_status' => '2','twilio_sender_id' => $sender_name);
			$this->db->where('tid',$tid);
			$query=$this->db->update('oc_sms_member_settings', $data); 
			if($query){
				return true;
			}else{
				return 0;
			}
	}
	public function check_caller_id($ci_key){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_settings');
		$this->db->where_in('twilio_sender_id',$ci_key);
		$query = $this -> db -> get();
		
		if($query->num_rows()>0){
			/// i.e matched - not available
			return 0;
		}
		else
		{
			/// i.e not matched - available
			return true;
		}
	}
	
	public function check_sms_contact($country,$user_id,$group,$mobile_number){
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_contacts');
		$this->db->where_in('user_id',$user_id);
		$this->db->where_in('country_code',$country);
		$this->db->where_in('contact_group',$group);
		$this->db->where_in('contact_mobile',$mobile_number);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function import_sms_contact($userId,$full_name,$mobile,$email,$country,$group){
		try{
			
			//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$addedOn=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$addedOn=date("Y-m-d H:i:s");
			}
			
			
			  $data = array(
						  'user_id' => $userId ,
						  'contact_name' => $full_name,
						  'contact_mobile' => $mobile ,
						  'contact_email' => $email ,
						  'contact_group 	' => $group ,
						  'country_code' => $country ,
						  'updated_on' => $addedOn ,
						  'status' => '1' );
						
				$query=$this->db->insert('oc_campaign_contacts', $data); 
				if($query)
				{
					
						return true;
				} 
				else
				{
					return 0;
				}
			
		
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	
	public function ad_contact_group($user_id,$group_name,$group_type){
		
		// Checking duplicate groups
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_groups');
		$this->db->where_in('group_name',$group_name);
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		  return 0;
		}else{
			
			$data=array('user_id'=>$user_id,'group_name'=>strtoupper($group_name),'group_type'=>$group_type,'status'=>'1');
			$query=$this->db->insert('oc_campaign_groups',$data);
			if($query){
				return true;
			}
		}
		
		
	}
	public function prepare_campaign($camp_code,$user_id,$sms_group,$from_number,$to_number,$message,$sms_price,
	$fScheduledTime,$isScheduled,$fCampTimeZone,$country_code){
		   //** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$addedOn=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$addedOn=date("Y-m-d H:i:s");
			}
			
			// Checking duplicate contact
			$this -> db -> select('*');
			$this -> db -> from('oc_sms_member_campaign');
			$this->db->where_in('sms_group',$sms_group);
			$this->db->where_in('to_number',$to_number);
			$this->db->where_in('deliver_status','2');
			$this->db->where_in('message',$message);
			$this->db->where_in('status','0');
			$this->db->where_in('user_id',$user_id);
			
			$query = $this -> db -> get();
			if($query->num_rows()>0){
			  return 0;
			}else{
			
			$data=array('user_id'=>$user_id,
			'sms_group'=> $sms_group,
			'campaign_code'=>$camp_code,
			'from_number'=>$from_number,
			'to_number'=>$to_number,
			'country_code'=>$country_code,
			'message'=>$message,
			'unit_cost'=>$sms_price,
			'do_submit'=>$addedOn,
			'deliver_status'=>'2',
			'scheduled_datetime'=>date("Y-m-d H:i:s",strtotime($fScheduledTime)),
			'is_scheduled'=>$isScheduled,
			'schedule_timezone'=>$fCampTimeZone,
			'status'=>'0'
			);
			$query=$this->db->insert('oc_sms_member_campaign',$data);
			if($query){
				return true;
			}else{
				return 0;
			}
		}
			
	}
	public function get_group_contacts($group_id,$user_id){
		
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_contacts');
		$this->db->where_in('contact_group',$group_id);
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_group_contacts_bycid($contact_id,$user_id){
		
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_contacts');
		$this->db->where_in('c_id',$contact_id);
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_group_contacts_bycnumber($contact_number,$user_id){
		
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_contacts');
		$this->db->where_in('contact_mobile',$contact_number);
		$this->db->where_in('user_id',$user_id);
		$this->db->group_by('contact_mobile');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_group_name($group_id,$user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_campaign_groups');
		$this->db->where('cgroup_id',$group_id);
		$this->db->where('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}else{return 0;}
	}
	
	public function get_ttitle_headline($t_code,$user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_template_titles');
		$this->db->where('template_code',$t_code);
		$this->db->where('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}else{return 0;}
	}
	public function get_caller_number_headline($tid,$user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_settings');
		$this->db->where('tid',$tid);
		$this->db->where('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}else{return 0;}
	}
	
	public function delete_sms_group($user_id,$group_id){
		    $where=array('user_id'=>$user_id,'cgroup_id'=>$group_id);

			$this->db->where($where);
			$query=$this->db->delete('oc_campaign_groups');
			if($query){
				return true;
			}else{
				return 0;
			}
	}
	public function delete_history_contact($user_id,$campaign_id){
		    $where=array('user_id'=>$user_id,'campaign_id'=>$campaign_id);

			$this->db->where($where);
			$query=$this->db->delete('oc_sms_member_campaign');
			if($query){
				return true;
			}else{
				return 0;
			}
	}
	
	public function delete_ttitle($user_id,$tt_code){
		    $where=array('user_id'=>$user_id,'template_code'=>$tt_code);

			$this->db->where($where);
			$query=$this->db->delete('oc_template_titles');
			if($query){
				return true;
			}else{
				return 0;
			}
	}
	
	public function delete_sms_contact($user_id,$contact_id){
		$where=array('user_id'=>$user_id,'c_id'=>$contact_id);

			$this->db->where($where);
			$query=$this->db->delete('oc_campaign_contacts');
			if($query){
				return true;
			}else{
				return false;
			}
	}
	public function delete_campaign($user_id,$campaign_code){
		$where=array('user_id'=>$user_id,'campaign_code'=>$campaign_code);

			$this->db->where($where);
			$query=$this->db->delete('oc_sms_member_campaign');
			if($query){
				return true;
			}else{
				return false;
			}
	}
	
	public function delete_caller_number($user_id,$cn_tid){
		   $where=array('user_id'=>$user_id,'tid'=>$cn_tid);

			$this->db->where($where);
			$query=$this->db->delete('oc_sms_member_settings');
			if($query){
				return true;
			}else{
				return false;
			}
	}
	
	
	
} 
?>