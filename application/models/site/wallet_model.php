<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Wallet_Model extends CI_Model
{
	
	public function add_funds_ByC2o_payment($itemNumber,$c2TransactionId,$currencyCode,$c2OrderNumber,$c2TotalAmount,
	$c2ResponseCode,$c2ResponsMsg,$paymentStatus,$userId,$walletTotalBalance,$walletAvailBalance,$payment_mode){
		try{
			
			$data = array(
			  'user_id' => $userId ,
			  'acc_item_number' => $itemNumber,
			  'c2o_transaction_id' => $c2TransactionId ,
			  'c2o_total_amount' => $c2TotalAmount ,
			  'acc_currency_code' => $currencyCode ,
			  'c2o_order_number' => $c2OrderNumber ,
			  'c2o_response_code' => $c2ResponseCode ,
			  'c2o_response_msg' => $c2ResponsMsg ,
			  'payment_status' => $paymentStatus,
			  'payment_mode' => $payment_mode,
			  
			  
			);
						
			$this->db->select('*');
			$this->db->from('oc_account_payments');
			$this->db->where('acc_item_number',$itemNumber);
			$this->db->where('user_id',$userId);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   return 0;
			}
			else{
				
				$res=$this->db->insert('oc_account_payments', $data); 
				if($res)
				{
					//** Gettings timezone based on country code **//
					$remoteCountry=$this->config->item('remote_country_code');
					$this->db->select('*');
					$this->db->from('oc_timezones');
					$this->db->where('country_code',$remoteCountry);
					$query_timezone=$this->db->get();

					if($query_timezone->num_rows()>0){
						$rTimeRow=$query_timezone->result();
						date_default_timezone_set($rTimeRow[0]->zone_name);
						$doTrans=date("Y-m-d H:i:s");


					}else{
						date_default_timezone_set("Asia/Brunei");
						$doTrans=date("Y-m-d H:i:s");
					}
			
					$accumTotalBal= floatval($walletTotalBalance) + floatval($c2TotalAmount);
					$accumAvailBal= floatval($walletAvailBalance) + floatval($c2TotalAmount);
					
					$data2 = array(
					'user_id' => $userId ,
					'total_amount' => $accumTotalBal,
					'balance_amount' => $accumAvailBal ,
					'debit_datetime' => $doTrans ,
					'status'=>1

					);
					$data3 = array(
					'total_amount' => $accumTotalBal,
					'balance_amount' => $accumAvailBal ,
					'debit_datetime' => $doTrans ,
					'status'=>1

					);
						
					$this->db->select('*');
					$this->db->from('oc_account_wallet');
					$this->db->where('user_id',$userId);
					$checkUser=$this->db->get();
					
				    if($checkUser->num_rows()>0){	
						$this->db->where('user_id',$userId);					
						$updateAccount=	$this->db->update('oc_account_wallet',$data3);	
						if($updateAccount){
							return true;
						}else{
							return 0;
						}
					}else{
											
						$insAccount=	$this->db->insert('oc_account_wallet',$data2);	
						if($insAccount){
							return true;
						}else{
							return 0;
						}
					}
				} 
				else
				{
					return false;
				}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	
	///// failure transactions
	public function failure_funds_ByC2o_payment($itemNumber,$c2TransactionId,$currencyCode,$c2OrderNumber,$c2TotalAmount,
	$c2ResponseCode,$c2ResponsMsg,$paymentStatus,$userId,$payment_mode){
		try{
			
			$data = array(
			  'user_id' => $userId ,
			  'acc_item_number' => $itemNumber,
			  'c2o_transaction_id' => $c2TransactionId ,
			  'c2o_total_amount' => $c2TotalAmount ,
			  'acc_currency_code' => $currencyCode ,
			  'c2o_order_number' => $c2OrderNumber ,
			  'c2o_response_code' => $c2ResponseCode ,
			  'c2o_response_msg' => $c2ResponsMsg ,
			  'payment_status' => $paymentStatus,
			  'payment_mode' => $payment_mode,
			  
			  
			);
						
			$this->db->select('*');
			$this->db->from('oc_account_payments');
			$this->db->where('acc_item_number',$itemNumber);
			$this->db->where('user_id',$userId);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   return 0;
			}
			else{
				
				$res=$this->db->insert('oc_account_payments', $data); 
				if($res)
				{
					return true;
				} 
				else
				{
					return 0;
				}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function deduct_creditfrom_member($txn_reference,$tns_payment,$payment_method,$payment_reference,
		 $payment_remarks,$payment_status,$user_id,$accBalance){
		/** Checking  duplications */
		$this->db->select('*');
		$this->db->from('oc_ad_payments');
		$this->db->where('payment_reference',$payment_reference);
		$query=$this->db->get();
		
		if($query->num_rows()>0){
		  return 0;
		  echo _("Duplication occured, please refresh your page.");
		}
		else{
			
			
			$remoteCountry=$this->config->item('remote_country_code');
			//** Gettings timezone based on country code **//
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();
			
			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				$timezone=$rTimeRow[0]->zone_name;
			   
				
			}else{
				$timezone="Asia/Brunei";
			}
			date_default_timezone_set($timezone);
			$paymentDatetime = date('Y-m-d H:i:s');
			
			

			$data = array(
			  'ad_reference' => $txn_reference ,
			  'payment_reference' => $payment_reference ,
			  'user_id' => $user_id ,
			  'ad_payment' => $tns_payment,
			  'payment_method' => $payment_method,
			  'date_of_payment' => $paymentDatetime,
			  'payment_remarks'=> $payment_remarks,
			  'payment_status' => $payment_status
			  
			);
			
			
			$sql=$this->db->insert('oc_ad_payments', $data); 
			if($sql){
				
				// deducting wallet credit
				$amount_update= floatval($accBalance) - floatval($tns_payment);
				$data2 = array(
					  'balance_amount' => $amount_update,
					  );
				$this->db->where('user_id',$user_id);
				$query2=$this->db->update('oc_account_wallet', $data2); 
			    if($query2){
					return true;
				}
				
			
			}else{
					return 0;
			}
		}
	}
	
	
	public function free_credit_request($userId,$member_name,$request_comments){
		
		try{
			
		    $data = array(
			  'user_id' => $userId ,
			  'member_name' => $member_name,
			  'request_comments' => $request_comments ,
			  'status' => 2,
			  
			);
						
			$this->db->select('*');
			$this->db->from('oc_credit_requests');
			$this->db->where('user_id',$userId);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   return 2;
			}
			else{
				
				$query2=$this->db->insert('oc_credit_requests', $data); 
				if($query2)
				{
					return true;
				} 
				else
				{
					return 0;
				}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	public function check_freecredit_requests(){
		$this -> db -> select('*');
		$this -> db -> from('oc_credit_requests');
		$this -> db -> join('oc_ad_accounts','oc_ad_accounts.user_id=oc_credit_requests.user_id');
		$this->db->where('oc_credit_requests.status',2);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	public function check_freecredit_request_byid($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_credit_requests');
		$this->db->where('oc_credit_requests.user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	public function check_all_freecredit_requests(){
		$this -> db -> select('oc_ad_accounts.*,oc_credit_requests.*,oc_credit_requests.status as arrpove_status');
		$this -> db -> from('oc_credit_requests');
		$this -> db -> join('oc_ad_accounts','oc_ad_accounts.user_id=oc_credit_requests.user_id');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
	
	public function update_freecredits_status($approved_amount,$user_id){
		
		$data = array(
		'approved_funds' => $approved_amount,
		'status' => 1 
		);
					
	    $this->db->where('user_id',$user_id);					
		$query=	$this->db->update('oc_credit_requests',$data);	
		if($query){
			return true;
		}else{
			return 0;
		}	
	}
	
	public function update_credit_threshold($user_id,$credit_threshold_count){
		
		//** Gettings timezone based on country code **//
		$remoteCountry=$this->config->item('remote_country_code');
		$this->db->select('*');
		$this->db->from('oc_timezones');
		$this->db->where('country_code',$remoteCountry);
		$query_timezone=$this->db->get();

		if($query_timezone->num_rows()>0){
			$rTimeRow=$query_timezone->result();
			date_default_timezone_set($rTimeRow[0]->zone_name);
			$ctTimestamp=date("Y-m-d H:i:s");


		}else{
			date_default_timezone_set("Asia/Brunei");
			$ctTimestamp=date("Y-m-d H:i:s");
		}
		$ct_count=$credit_threshold_count+1;
					
		$data = array(
		'credit_threshold_count' =>$ct_count ,
		'credit_threshold_timestamp' => $ctTimestamp
		);
					
	    $this->db->where('user_id',$user_id);					
		$query=	$this->db->update('oc_account_wallet',$data);	
		if($query){
			return true;
		}else{
			return 0;
		}	
	}
	
	public function rollback_system_payments($transaction_id,$wallet_token,$userId,$remarks,$notify_mail,
	$walletTotalBalance,$walletAvailBalance,$c2TotalAmount){
	  try{
			
		    /// checking system transaction mode ( i.e it able to delete only system transactions )	
			$exp_system_trans=explode('-',$wallet_token);
			
			if($exp_system_trans[0]=="SYS"){
				/// checking transaction info existed or not			
				$this->db->select('*');
				$this->db->from('oc_account_payments');
				$this->db->where('acc_item_number',$wallet_token);
				$this->db->where('c2o_transaction_id',$transaction_id);
				$this->db->where('c2o_total_amount',$c2TotalAmount);
				$this->db->where('user_id',$userId);
				$query=$this->db->get();
				
				if($query->num_rows()>0){
					
					//// delete payment record
					$where = array(
					'acc_item_number' => $wallet_token,
					'c2o_transaction_id' => $transaction_id ,
					'user_id'=>$userId
					);
					
					$this->db->where($where);
					$query=$this->db->delete('oc_account_payments');
						
					if($query){
						   
							/// deducting from wallet
							$accumTotalBal= floatval($walletTotalBalance) - floatval($c2TotalAmount);
							$accumAvailBal= floatval($walletAvailBalance) - floatval($c2TotalAmount);
							
							$data3 = array(
							'total_amount' => $accumTotalBal,
							'balance_amount' => $accumAvailBal ,
							'status'=>1
							);
								
							$this->db->where('user_id',$userId);					
							$updateAccount=	$this->db->update('oc_account_wallet',$data3);	
							if($updateAccount){
								return true;
							}else{
								return 0;
							}
							
					}else{
						return 0;
					}
				}
				else{
					return 2;
				}
			}else{
				return 3;
			}
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	  
	}
	
	public function get_card_information($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_card_master');
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function update_pp_payment($item_number,$txn_id,$payment_gross,$currency_code,$payment_status){
		try{
			$user_id=$this->session->userdata['site_login']['user_id'];
				$data = array(
				'user_id' => $user_id ,
				'acc_item_number' => $item_number,
				'acc_txn_id' => $txn_id ,
				'acc_payment_gross' => $payment_gross ,
				'acc_currency_code' => $currency_code ,
				'payment_status' => $payment_status 

			);
						
			$this->db->select('*');
			$this->db->from('oc_account_payments');
			$this->db->where('user_id',$user_id);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			    /** getting site settings **/
			    $this->db->select('*');
				$this->db->from('oc_site_settings');
				$query_settings=$this->db->get();
				$settingInfo=$query_settings->result();
				
				$adLimit= $settingInfo[0]->business_ad_limit;
				$imgUploads= $settingInfo[0]->business_image_uploads;
				$accExpires = date('Y-m-d H:i:s');
				$data2 = array(
				'is_paid' => '1',
				'ad_limit' => $adLimit ,
				'pay_acc_expire_datetime' => $accExpires,

				);
				/** update to paid account **/	
				$this->db->where('user_id',$user_id);					
			    $updateAccount=	$this->db->update('oc_ad_accounts',$data2);
				
				
				$res=$this->db->update('oc_account_payments', $data); 
			   // $this->db->where('app_id');
               $res=$this->db->update('oc_account_payments', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				/** getting site settings **/
			    $this->db->select('*');
				$this->db->from('oc_site_settings');
				$query_settings=$this->db->get();
				$settingInfo=$query_settings->result();
				
				$adLimit= $settingInfo[0]->business_ad_limit;
				$imgUploads= $settingInfo[0]->business_image_uploads;
				$accExpires = date('Y-m-d H:i:s');
				$data2 = array(
				'is_paid' => '1',
				'ad_limit' => $adLimit ,
				'pay_acc_expire_datetime' => $accExpires,

				);
				/** update to paid account **/	
				$this->db->where('user_id',$user_id);					
			    $updateAccount=	$this->db->update('oc_ad_accounts',$data2);
				
				$res=$this->db->insert('oc_account_payments', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function get_account_wallet($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_account_wallet');
		$this->db->where_in('user_id',$user_id);
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function ad_payments_bywallet($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_payments');
		$this->db->where_in('user_id',$user_id);
		$this->db->order_by('payment_id','desc');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	public function member_transfers($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_payments');
		$this->db->where_in('user_id',$user_id);
		$this->db->like('ad_reference ','MOTF');
		$this->db->order_by('payment_id','desc');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	public function payments_received_bymember($user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_account_payments');
		$this->db->where_in('user_id',$user_id);
		$this->db->where_in('c2o_response_code','RECEIVED');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function wallet_invoice_byid($user_id,$payment_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_payments');
		$this->db->join('oc_ad_accounts','oc_ad_accounts.user_id=oc_ad_payments.user_id');
		$this->db->where_in('oc_ad_payments.user_id',$user_id);
		$this->db->where_in('oc_ad_payments.payment_id',$payment_id);
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	public function recent_transactions($userId){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_payments');
		$this->db->where_in('user_id',$userId);
		$this->db->limit(5);
		$this->db->order_by('payment_id','desc');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	
	
	public function list_users(){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_users');
		$this -> db -> where('status ', $status_id); 
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	public function list_account_classes(){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_account_class');
		$this -> db -> where('status ', $status_id); 
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	
	public function list_account_classes_notById($ac_class){
		$status_id= '1';
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_account_class');
		$this -> db -> where('status ', $status_id); 
		$this -> db -> where('acc_id <> ', $ac_class);
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	public function list_current_user($user_id){
		
		$where=array('status'=>'1','user_id'=>$user_id);
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this -> db -> where($where); 
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	
	
	public function update_user_profile($group_id,$user_id,$first_name,$last_name,$email_id,$mobile_number){
		$data=array('user_group'=>$group_id,
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'email_id'=>$email_id,
		'mobile_number'=>$mobile_number,
		);
		
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}
		
	}
	public function update_stage_user($user_id,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status){
		$data=array('user_group'=>$user_groups,
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'user_email'=>$email_id,
		'user_mobile'=>$telephone,
		'user_phone'=>$telephone2,
		'status'=>$user_status,
		);
		
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	
} 
?>