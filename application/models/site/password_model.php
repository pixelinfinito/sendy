<?php
Class Password_Model extends CI_Model
{
	function check($ac_email)
	{
		
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_accounts');
		$this -> db -> where('ac_email',$ac_email); 
		$this -> db -> where('status',1); 
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}

	}
	
	public function update_new_password($user_id,$new_password){
		
		$data=array('ac_password'=>$new_password);
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_ad_accounts',$data);
		if($query){
			return true;
		}else{
			return 0;
		}
	}
	
	
	
	
} 
?>