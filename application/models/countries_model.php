<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Countries_Model extends CI_Model
{
	
	public function load_city($queryString,$geoCountry){
		$this->db->select('*');
		$this->db->from('oc_cities');
		$this->db->where_in('country_code',$geoCountry);
		$this->db->like('city',$queryString,'after');
		$query=$this->db->get();
		
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			
			return $data;
		}else{
			return '0';
		}
	}
	
	
	public function list_countries(){
		$query=$this->db->get('oc_countries');
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	
	public function list_visitors(){
			
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_visits');
		$this->db->join('oc_countries','oc_countries.country_code=oc_ad_visits.remote_country');
		$this->db->where('oc_ad_visits.visit_on BETWEEN DATE_SUB(NOW(), INTERVAL 15 DAY) AND NOW()');
		
		//$this->db->group_by('oc_ad_visits.remote_country');
		$this->db->order_by('oc_ad_visits.visit_id','desc');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	public function list_visitors_count(){
		
		$query=$this->db->get('oc_ad_visits');
		if($query->num_rows()>0){
			return count($query->result());
		}else{
			return '0';
		}
	}
	
	
	
	public function list_country_timezone(){
		
		
		$this -> db -> select('*');
		$this -> db -> from('oc_timezones');
		$this->db->join('oc_countries','oc_countries.country_code=oc_timezones.country_code');
		$this->db->order_by('oc_timezones.zone_name');
		$this->db->group_by('oc_countries.country_code');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	
	public function list_country_timezone_byCode($country_code){
		
		
		$this -> db -> select('*');
		$this -> db -> from('oc_timezones');
		$this->db->join('oc_countries','oc_countries.country_code=oc_timezones.country_code');
		$this->db->where('oc_countries.country_code',$country_code);
		$this->db->group_by('oc_countries.country_code');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	public function get_currencies_cCode($country_code){
		
		$this -> db -> select('*');
		$this -> db -> from('oc_currencies');
		//$this->db->join('oc_countries','oc_countries.country_code=oc_currencies.country_code');
		if($country_code!=''){
			$this->db->where('oc_currencies.country_code',$country_code);
		}else{
			$this->db->where('oc_currencies.country_code','US');
		}
		$query=$this->db->get();
			
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	
	public function get_sms_price_bycc($country_code){
		$this -> db -> select('*');
		$this->db->where('country_code',$country_code);
		$this -> db -> from('oc_sms_prices');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
		
	}
	
	
} 
?>