<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
 
Class User_Groups_Model extends CI_Model
{
	function add_group($group_name, $group_desc)
	{
        $data = array(
				  'group_name' => $group_name ,
				  'group_desc' => $group_desc,
				  'status' => '1' ,
				);

		$sql=$this->db->insert('oc_user_groups', $data); 
		if($sql){
			return true;
		}

	}
	
	public function delete_group($group_id){
		    $this->db->where('group_id', $group_id);
		    $query=$this->db->delete('oc_user_groups'); 
            if($query){
			return true;
		    }
    }
	
	
	
	public function update_group($group_id,$group_name,$group_desc){
			$data = array(
				  'group_name' => $group_name ,
				  'group_desc' => $group_desc,
				  
				);
			$this->db->where('group_id', $group_id);
			$query=$this->db->update('oc_user_groups', $data); 
			if($query){
				return true;
			}
	}
	
	public function list_groups(){
		$query=$this->db->get('oc_user_groups');
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	public function list_inactive_groups(){
		$this->db->where('status',0);
		$query=$this->db->get('oc_user_groups');
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function get_single_group($group_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_user_groups');
		$this -> db -> where('group_id = ' . "'" . $group_id . "'"); 
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	
	
} 
?>