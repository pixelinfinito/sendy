<?php
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */

Class Caller_Number_Model extends CI_Model
{
	
	public function get_cn_requests(){
		
		$this -> db -> select('t1.*,t1.status as cn_status,t3.*,t2.*');
		$this -> db -> from('oc_sms_member_settings t1');
		$this->db->join('oc_countries t2','t2.country_code=t1.country');
		$this->db->join('oc_ad_accounts t3','t3.user_id=t1.user_id');
		$this->db->group_by('t1.tid');
		$this->db->order_by('t1.tid','asc');
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	public function get_cn_pending_requests(){
		$this->db->where('status',2);
		$query=$this->db->get('oc_sms_member_settings');
		if($query->num_rows()>0){
			return count($query->result());
		}else{
			return 0;
		}
	}
	
	public function get_ci_requests(){
		
		$this -> db -> select('t1.*,t1.status as cn_status,t3.*,t2.*');
		$this -> db -> from('oc_sms_member_settings t1');
		$this->db->join('oc_countries t2','t2.country_code=t1.country');
		$this->db->join('oc_ad_accounts t3','t3.user_id=t1.user_id');
		$this->db->where('t1.sender_id_status !=','0');
		
		$this->db->group_by('t1.tid');
		$this->db->order_by('t1.tid','asc');
		$query=$this->db->get();
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	public function update_member_cn($cnId,$cnNumber){
		$data=array('twilio_origin_number'=>$cnNumber,'status'=>'1');
		$this->db->where('tid',$cnId);
		$query=$this->db->update('oc_sms_member_settings',$data);
		
		if($query){
			return true;
		}else{
			
			return 0;
		}
	}
	public function get_member_details($tid){
	  	$this -> db -> select('t1.*,t2.user_id,t2.ac_phone,t2.ac_first_name,t2.ac_last_name');
		$this -> db -> from('oc_sms_member_settings t1');
		$this->db->join('oc_ad_accounts t2','t2.user_id=t1.user_id','left');
		$this->db->where('t2.mobile_validate',1);
		$this->db->where('t1.tid',$tid);
		$this->db->group_by('t1.tid');
		
		$query=$this->db->get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}else{
			return 0;
		}
	}
	
	public function suspend_caller_number($cnId,$cnNumber,$remarks){
	
 	    $data=array('status'=>'3','sender_id_remarks'=>$remarks);
		$this->db->where('tid',$cnId);
		$this->db->where('twilio_origin_number',$cnNumber);
		$query=$this->db->update('oc_sms_member_settings',$data);
		
		if($query){
			return true;
		}else{
			
			return 0;
		}
	}
	public function suspend_caller_id($cnId,$ciTag,$remarks){
		
		$data=array('sender_id_status'=>'3','sender_id_remarks'=>$remarks);
		$this->db->where('tid',$cnId);
		$this->db->where('twilio_sender_id',$ciTag);
		$query=$this->db->update('oc_sms_member_settings',$data);
		
		if($query){
			return true;
		}else{
			
			return 0;
		}
	}
	
	public function check_caller_id($ci_key,$user_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_settings');
		$this->db->where_in('twilio_sender_id',$ci_key);
		$this->db->where_not_in('user_id',$user_id);
		$query = $this -> db -> get();
		
		if($query->num_rows()>0){
			/// i.e matched - not available
			return 0;
		}
		else
		{
			/// i.e not matched - available
			return true;
		}
	}
	public function cn_local_check($cn){
		$this -> db -> select('twilio_origin_number');
		$this -> db -> from('oc_sms_member_settings');
		$this->db->where('twilio_origin_number',$cn);
		$query = $this -> db -> get();
		
		if($query->num_rows()>0){
			/// i.e matched - not available
			return 1;
		}
		else
		{
			/// i.e not matched - available
			return 0;
		}
	}
	
	
	public function approve_caller_id($tid){
		$data=array('sender_id_status'=>'1','sender_id_remarks'=>'approved');
		$this->db->where('tid',$tid);
		$query=$this->db->update('oc_sms_member_settings',$data);
		
		if($query){
			return true;
		}else{
			
			return 0;
		}
	}
	
	
	
} 
?>