<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class User_Model extends CI_Model
{
	function authenticate($username, $password)
	{
		$where = array ('username'=>$username,'password'=>$password,'status'=>'1');
		
        $this -> db -> select('*');
		$this -> db -> from('oc_users');
		$this -> db -> where($where); 
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}

	}
	
	
	public function add_user($user_name, $password,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status){
		
		$doj=date("Y-m-d H:i:s");
		$data = array(
			  'username' => $user_name ,
			  'password' =>  MD5($password),
			  'first_name' => $first_name ,
			  'last_name' => $last_name ,
			  'user_email' => $email_id ,
			  'user_mobile' => $telephone ,
			  'user_phone' => $telephone2 ,
			  'user_group' => $user_groups,
			  'date_of_join' => $doj ,
			  'status' => $user_status ,
		);

		$res=$this->db->insert('oc_users', $data); 
		if($res){
		  return TRUE;	
		}else{
			return FALSE;
		}
		
	}
	
	
  
  public function add_user_password_notification($notify_subject,$notify_from,$notify_body,$notify_to,$deliver_status){
	$data = array(
			  'notify_subject' => $notify_subject ,
			  'notify_from' =>  $notify_from,
			  'notify_message' => $notify_body ,
			  'notify_to' => $notify_to ,
			  'deliver_status' => $deliver_status ,
			  
			);
	
	$this->db->insert('oc_user_password_notifications', $data); 
	return true;
	}
	
	public function list_users(){
		
		$this -> db -> select('oc_users.*,oc_user_groups.group_id,oc_user_groups.group_name');
		$this -> db -> from('oc_users');
		$this -> db -> join('oc_user_groups','oc_user_groups.group_id=oc_users.user_group');
		
		$this->db->order_by('oc_users.user_id','desc');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	public function list_inactive_users(){
		
		$this -> db -> select('oc_users.*,oc_user_groups.group_id,oc_user_groups.group_name');
		$this -> db -> from('oc_users');
		$this -> db -> join('oc_user_groups','oc_user_groups.group_id=oc_users.user_group');
		$this->db->where('oc_users.status',0);
		
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function list_current_user($user_id){
		
		$where=array('user_id'=>$user_id);
		$this -> db -> select('*');
		$this -> db -> from('oc_users');
		$this -> db -> where($where); 
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	
	public function update_stage_user($user_id,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status){
		$data=array('user_group'=>$user_groups,
		'first_name'=>$first_name,
		'last_name'=>$last_name,
		'user_email'=>$email_id,
		'user_mobile'=>$telephone,
		'user_phone'=>$telephone2,
		'status'=>$user_status,
		);
		
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
	
	public function update_thumbnail($user_id,$thumb_path){
	   try{
			
			$data = array(
			'profile_thumbnail' => $thumb_path
			);
			
			$this->db->where('user_id',$user_id);					
			$query=	$this->db->update('oc_users',$data);
			
		    if($query){
				return true;
			} 
			else{
				return 0;
			}
			
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_user_password($user_id,$new_password){
		
	    $data=array('password'=>$new_password);
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}else{
			return 0;
		}
	}
	
	public function get_user_group($group_id){
		$this -> db -> select('*');
		$this -> db -> from('oc_user_groups');
		$this -> db -> where('group_id = ' . "'" . $group_id . "'"); 
		$query = $this -> db -> get();
		
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	
	
	public function delete_user($user_id){
		
		$this -> db -> where('user_id', $user_id); 
		$query=$this -> db -> delete('oc_users');
		
		if($query){
			return true;
		 }
	}
	
	
} 
?>