<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Promotions_Model extends CI_Model
{
	 function compose_promotion($promotion_code,$user_id,$promotion_title,
	 $message,$attachment,$from_name,$from_mail,$to_name,$mail_to,$promotion_type,$status)
	 {
		
		
			 $data = array(
			  'promotion_code' => $promotion_code ,
			  'user_id' => $user_id ,
			  'promotion_title' => $promotion_title,
			  'promotion_body' => $message,
			  'attachment_path' => $attachment,
			  'mail_from' => $from_mail,
			  'from_name' => $from_name,
			  'to_name' => $to_name,
			  'mail_to' => $mail_to,
			  'promotion_type' => $promotion_type,
			  'status' => $status,
			  
			);
			
			$this->db->select('*');
			$this->db->from('oc_promotions');
			$this->db->where('mail_to',$mail_to);
			$this->db->where('promotion_code',$promotion_code);
			$query=$this->db->get();
			if($query->num_rows()>0){
			   echo "<b>" . _("Duplication") . " </b>, " . _("Mail already composed to") . " ".$mail_to."<br>";
			   return 0;
			}
			
			else{
				$sql=$this->db->insert('oc_promotions', $data); 
				if($sql){
					return 1;
				}else{
					return 0;
				}
			}
		
	}
	
	 function compose_sms_promotion($promotion_code,$user_id,
	$message,$sender_from,$to_phone,$to_name,$promotion_type,$status,$notify_appname)
	 {
		
			 $data = array(
			  'promotion_code' => $promotion_code ,
			  'user_id' => $user_id ,
			  'promotion_body' => $message,
			  'mail_from' => $sender_from,
			  'from_name' => $notify_appname,
			  'to_name' => $to_name,
			  'mail_to' => $to_phone,
			  'promotion_type' => $promotion_type,
			  'status' => $status,
			  
			);
			
			$this->db->select('*');
			$this->db->from('oc_promotions');
			$this->db->where('mail_to',$to_phone);
			$this->db->where('promotion_code',$promotion_code);
			$query=$this->db->get();
			if($query->num_rows()>0){
			   echo "<b>" . _("Duplication") . " </b>, " . _("SMS already composed to") . " ".$to_phone."<br>";
			   return 0;
			}
			
			else{
				$sql=$this->db->insert('oc_promotions', $data); 
				if($sql){
					return 1;
				}else{
					return 0;
				}
			}
		
	}
	
	
	public function list_promotion_count(){
		
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$this->db->group_by('promotion_code');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function load_promotion_message($promotion_id){
		
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$this->db->where('promotion_id',$promotion_id);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function list_promotions(){
		
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function list_promotions_bysms(){
		
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$this->db->where('promotion_type',1);
		$this->db->where('status',2);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function list_promotions_byemail(){
		
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$this->db->where('promotion_type',2);
		$this->db->where('status',2);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function list_delivered_promotions(){
		
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$this->db->where('status',1);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	public function list_failed_promotions(){
		
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$this->db->where('status',0);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function list_waiting_promotions(){
		
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$this->db->where('status',2);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function update_promotion_status($promotion_id,$status){
		    $data= array('status'=>$status);
		    $this->db->where('promotion_id', $promotion_id);
		    $query=$this->db->update('oc_promotions',$data); 
			
			if($query){
				return true;
		    }else{
				return 0;
			}
    }
	
	public function delete_promotion($promotion_id){
		    $this->db->where('promotion_id', $promotion_id);
		    $query=$this->db->delete('oc_promotions'); 
			
			if($query){
				return true;
		    }else{
				return 0;
			}
	}
	
	public function delete_selected_promotions($promotion_ids){
		$notifyArr=array();
		$exp_cids=explode(',',$promotion_ids);
		for($i=0;$i<count($exp_cids);$i++){
		array_push($notifyArr,$exp_cids[$i]);
		}

		
		$successArr=array();
		$this->db->select('*');
		$this->db->from('oc_promotions');
		$this->db->where_in('promotion_id',$notifyArr);
		$query=$this->db->get();
		
		
		if($query->num_rows()>0){
			
			$c=0;
			foreach($query->result() as $row){
			$this->db->where('promotion_id',$row->promotion_id);
			$del_query=$this->db->delete('oc_promotions');
			
			$c++;
			}
			return true;
		}else{
			return 0;
		}
	}
	
	
} 
?>