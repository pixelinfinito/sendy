<?php
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
Class Card_Model extends CI_Model
{
	function add_card($adfPaymentMethod,$payCardNumber,$payCardholderName,$payCardExpMonth,$payCardExpYear,$payCardCvv,$cardCity,$cardStreet1,$cardCountry,$cardPostalCode)
	{
		/** Checking  duplications */
		$user_id=$this->session->userdata['login_in']['user_id'];

		$this->db->select('*');
		$this->db->from('oc_card_master');
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		
		if($query->num_rows()>0){
		  return 0;
		  echo _("Duplication occured, please refresh your page.");
		}
		else{
			date_default_timezone_set('Asia/Brunei');
			$post_date = date('Y-m-d H:i:s');
			$add_days = $bidValidTo;
			$exp_date = date('Y-m-d H:i:s',strtotime($post_date) + (24*3600*$add_days)); 
			$user_id=$this->session->userdata['login_in']['user_id'];

			$data = array(
			  'card_number' => $payCardNumber ,
			  'card_holder_name' => $payCardholderName ,
			  'card_cvv' => $payCardCvv,
			  'card_expire_month' => $payCardExpMonth,
			  'card_expire_year' => $payCardExpYear,
			  'card_method' => $adfPaymentMethod,
			  'billing_street1' => $cardStreet1,
			  'billing_street2' => '',
			  'billing_city' => $cardCity,
			  'billing_country' => $cardCountry,
			  'billing_postalcode' => $cardPostalCode,
			  'user_id'=> $user_id
			  
			);
			//print_r($data);

			$sql=$this->db->insert('oc_card_master', $data); 
			if($sql){
				return 1;
			}else{
				return 0;
			}
		}
	}
	
	public function listCardDetails(){
		$user_id=$this->session->userdata['login_in']['user_id'];
		$this->db->select('*');
		$this->db->from('oc_card_master');
		$this->db->where('user_id',$user_id);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	
	
	public function listCardMethods(){
		$query=$this->db->get('oc_payment_methods');
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	
	
	
} 
?>