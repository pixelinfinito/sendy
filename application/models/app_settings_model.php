<?php
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class App_Settings_Model extends CI_Model
{
	
	
	public function add_ftp_settings($ftp_host,$ftp_username,$ftp_password,$ftp_port,$api_type){
		$data = array(
				  'api_username' => $ftp_username ,
				  'api_password' =>  base64_encode($ftp_password),
				  'api_url' => $ftp_host ,
				  'api_port' => $ftp_port ,
				  'api_type' => $api_type ,
				  'status' => '1' ,
				  
				);

		$this->db->insert('oc_api_settings', $data); 
		return true;
	}
	
	public function add_app_settings($ftp_host,$ftp_username,$ftp_password,$ftp_port,$api_type){
		$data = array(
				  'api_username' => $ftp_username ,
				  'api_password' =>  base64_encode($ftp_password),
				  'api_url' => $ftp_host ,
				  'api_port' => $ftp_port ,
				  'api_type' => $api_type ,
				  'status' => '1' ,
				  
				);

		$this->db->insert('oc_api_settings', $data); 
		return true;
	}
	
	public function add_email_settings($email_host,$email_username,$email_password,$email_port,$api_type){
		$data = array(
				  'api_username' => $email_username ,
				  'api_password' =>  base64_encode($email_password),
				  'api_url' => $email_host ,
				  'api_port' => $email_port ,
				  'api_type' => $api_type ,
				  'status' => '1' ,
				  
				);

		$this->db->insert('oc_api_settings', $data); 
		return true;
	}
	public function get_app_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_app_settings');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function caller_number_classes(){
		$this -> db -> select('*');
		$this -> db -> from('oc_caller_number_class');
		$this->db->where('status','1');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_merge_caller_numbers(){
		$this -> db -> select('*');
		$this -> db -> from('oc_caller_numbers');
		$this->db->join('oc_caller_number_class','oc_caller_number_class.cl_id=oc_caller_numbers.cn_type');
		$this->db->join('oc_countries','oc_countries.country_code=oc_caller_numbers.cn_country');
		$this -> db ->group_by('oc_caller_numbers.cn_id');
		$this -> db ->order_by('oc_countries.country_name','asc');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_sms_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_back_settings');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_sms_prices(){
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_prices');
		$this->db->join('oc_countries','oc_countries.country_code=oc_sms_prices.country_code');
		$this->db->group_by('oc_sms_prices.p_id');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_sms_price_byid($pid){
		$this -> db -> select('*');
		$this->db->where('p_id',$pid);
		$this -> db -> from('oc_sms_prices');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
		
	}
	public function get_caller_number_byid($cnId){
		$this -> db -> select('*');
		$this->db->where('cn_id',$cnId);
		$this -> db -> from('oc_caller_numbers');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_caller_numbers(){
		$this -> db -> select('*');
		$this -> db -> from('oc_caller_numbers');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_sms_member_settings($userId){
		$this -> db -> select('*');
		$this -> db -> where('user_id',$userId);
		$this -> db -> from('oc_sms_member_settings');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_member_campaign_settings($userId,$from_number){
		$this -> db -> select('*');
		$this -> db -> where('user_id',$userId);
		$this -> db -> where('twilio_origin_number',$from_number);
		$this -> db -> from('oc_sms_member_settings');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	public function get_payment_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_payment_settings');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_account_classes(){
		
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_account_class');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_currency_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_currencies');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	public function get_currency_settings_byid($cId){
		$this -> db -> select('*');
		$this -> db -> from('oc_currencies');
		$this->db->where('currency_id',$cId);
		$query = $this -> db -> get();
		
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	public function get_site_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_site_settings');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return '0';}
	}
	
	public function get_currencies(){
		$this -> db -> select('*');
		$this -> db -> from('oc_currencies');
		$this->db->join('oc_app_settings','oc_app_settings.app_country=oc_currencies.country_code');
		$query=$this->db->get();
			
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return '0';
		}
	}
	
	public function get_primary_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_app_settings');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function get_user_groups(){
		$this -> db -> select('*');
		$this -> db -> from('oc_user_groups');
		$query = $this -> db -> get();
		
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	public function get_acclass_settings($tId){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_account_class');
		$this->db->where('acc_id',$tId);
		$query = $this -> db -> get();
		
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}else{
			return 0;
		}
	}
	
	public function get_ad_features(){
		
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_features');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function get_ad_features_byId($tId){
		$this -> db -> select('*');
		$this -> db -> from('oc_ad_features');
		$this->db->where('feature_id',$tId);
		$query = $this -> db -> get();
		
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}else{
			return 0;
		}
	}
	
	
	
	
	public function update_ftp_settings($ftp_host,$ftp_username,$ftp_password,$ftp_port,$api_id){
		$data = array(
				  'api_username' => $ftp_username ,
				  'api_password' =>  base64_encode($ftp_password),
				  'api_url' => $ftp_host ,
				  'api_port' => $ftp_port ,
				  'status' => '1' ,
				  
				);

        $this->db->where('api_id', $api_id); 
		$this->db->update('oc_api_settings', $data); 
		return true;
	}
	public function update_sms_settings($sc_acc_id,$sc_auth_token,$sc_origin_number,$sc_sender_id,$sc_resend_time,$campaign_minval,$campaign_maxval,
	$service_recall,$credit_threshold_recall,$cmp_alerts,$cn_alerts,$sid_alerts,$pay_alerts,$sid_approval_alerts,$enable_alerts,$blacklist_alerts){
		try{
			
			$data = array(
			  'twilio_accid' => $sc_acc_id ,
			  'twilio_authtoken' => $sc_auth_token,
			  'twilio_origin_number' => $sc_origin_number ,
			  'twilio_sender_id' => $sc_sender_id ,
			  'password_resend_interval' => $sc_resend_time,
			  'campaign_minval' => $campaign_minval ,
			  'campaign_maxval' => $campaign_maxval ,
			  'service_recall_interval' => $service_recall,
			  'credit_threshold_interval' => $credit_threshold_recall ,
			  'cmp_alert_token' => $cmp_alerts,
			  'cn_alert_token' => $cn_alerts ,
			  'sid_alert_token' => $sid_alerts ,
			  'pay_alert_token' => $pay_alerts,
			  'sid_approve_alert_token' => $sid_approval_alerts,
			  'enable_alerts' => $enable_alerts,
			  'blacklist_alerts' => $blacklist_alerts,
			 
			);
						
			$this->db->select('*');
			$this->db->from('oc_sms_back_settings');
			//$this->db->where('1');
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			  // $this->db->where('app_id');
               $res=$this->db->update('oc_sms_back_settings', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				$res=$this->db->insert('oc_sms_back_settings', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_app_settings($app_timezone,$app_country,$app_currency,$smtp_host,$smtp_username,$smtp_password,
	$smtp_port,$app_from_mail,$app_default_name,$app_mobile_number,$app_support_number,$app_support_email,$app_upgrade_option,
	  $app_helptext,$app_branded_by,$app_copyrights_text,$fb_link,$twitter_link,$youtube_link){
		try{
			
			$data = array(
			  'app_country' => $app_country ,
			  'app_timezone' => $app_timezone,
			  'app_currency' => $app_currency ,
			  'default_from_mail' => $app_from_mail ,
			  'default_mobile_number' => $app_mobile_number ,
			  'app_default_name' => $app_default_name,
			  'default_support_email' => $app_support_email ,
			  'default_support_numbers' => $app_support_number ,
			  'smtp_host' => $smtp_host ,
			  'smtp_user' => $smtp_username ,
			  'smtp_password' => $smtp_password ,
			  'smtp_port' => $smtp_port ,
			  'upgrade_option' => $app_upgrade_option ,
			  'help_text' => $app_helptext ,
			  'branded_by' => $app_branded_by ,
			  'brand_copyrights' => $app_copyrights_text ,
			  'fb_social_link' => $fb_link ,
			  'youtube_social_link' => $twitter_link ,
			  'twitter_social_link' => $youtube_link ,
			  
			);
						
			$this->db->select('*');
			$this->db->from('oc_app_settings');
			//$this->db->where('1');
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			  // $this->db->where('app_id');
               $res=$this->db->update('oc_app_settings', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				$res=$this->db->insert('oc_app_settings', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
	}
	public function update_acclass_settings($ac_itemcode,$ac_name,$ac_cost,$ac_status,$ac_desc){
		try{
			
			$data = array(
			'item_number' => $ac_itemcode,
			'acc_type' => $ac_name,
			'acc_desc' => $ac_desc ,
			'acc_cost' => $ac_cost ,
			'status' => $ac_status ,

			);
			
			$data2 = array(
			'acc_type' => $ac_name,
			'acc_desc' => $ac_desc ,
			'acc_cost' => $ac_cost ,
			'status' => $ac_status ,

			);

			$this->db->select('*');
			$this->db->from('oc_ad_account_class');
			$this->db->where('item_number',$ac_itemcode);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			   $this->db->where('item_number',$ac_itemcode);
               $res=$this->db->update('oc_ad_account_class', $data2); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				$res=$this->db->insert('oc_ad_account_class', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_ad_features($af_code,$af_name,$price_tag,$af_mapping,$af_status,$af_desc){
		try{
			
			$this->db->select('*');
			$this->db->from('oc_ad_features');
			$this->db->where('featured_code',$af_code);
			$query=$this->db->get();
			 
				
			$data = array(
			'featured_code' => $af_code,
			'feature_name' => $af_name,
			'price_tag' => $price_tag ,
			'feature_desc' => $af_desc ,
			'status' => $af_status ,
             
			);
			
			
			if($query->num_rows()>0){
			   
			            $chkVal=$query->result();
						
						$data1 = array(
						 $af_mapping =>'1',
						'featured_code' => $af_code,
						'feature_name' => $af_name,
						'price_tag' => $price_tag ,
						'feature_desc' => $af_desc ,
						'status' => $af_status,
						);
						
						
						$array1=array('f1'=>$chkVal[0]->f1,'f2'=>$chkVal[0]->f2,'f3'=>$chkVal[0]->f3,'f4'=>$chkVal[0]->f4);
						$array2 = array($af_mapping => "1");
						$result = array_diff_assoc($array1,$array2);

						foreach($result as $key=>$val){
							///// Appending not matched values in format of associative array to existing $data1 /////
							$data1[$key] = '0';
							
						}
					   
					   
					   $this->db->where('featured_code',$af_code);
					   $res=$this->db->update('oc_ad_features', $data1); 
					   if($res)
					   {
						   return true;
					   } 
						   
					   else
					   {
						   return false;
					   }
				
			}
			else{
				
				/*$res=$this->db->insert('oc_ad_features', $data); 
				if($res){return true;} else{return false;}
			    */
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	public function update_currency_settings($currency_id,$c_country,$cr_name,$cr_symbol,$cr_desc){
	
	   try{
			
			$data = array(
			
			'currency_name' => $c_country,
			'country_code' => $cr_name ,
			'character_symbol' => $cr_symbol ,
			'currency_desc' => $cr_desc ,

			);
			
			
			$this->db->select('*');
			$this->db->from('oc_currencies');
			$this->db->where('currency_id',$currency_id);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			   $this->db->where('currency_id',$currency_id);
               $res=$this->db->update('oc_currencies', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				$res=$this->db->insert('oc_currencies', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	
	}
	public function update_sms_prices($price_id,$sp_country,$sp_currency_code,$sp_price,$sp_desc){
		 try{
			
			$data = array(
			
			'country_code' => $sp_country,
			'currency_code' => $sp_currency_code ,
			'price' => $sp_price ,
			'price_desc' => $sp_desc ,

			);
			
			
			$this->db->select('*');
			$this->db->from('oc_sms_prices');
			$this->db->where('p_id',$price_id);
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			   $this->db->where('p_id',$price_id);
               $res=$this->db->update('oc_sms_prices', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				$res=$this->db->insert('oc_sms_prices', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	public function update_caller_number($cn_id,$cn_country,$cn_currency_code,$cn_price,$cn_type,$cn_desc,$cn_period){
		 try{
			
			$data = array(
			'cn_country' => $cn_country,
			'cn_currency' => $cn_currency_code ,
			'cn_price' => $cn_price ,
			'cn_type' => $cn_type ,
			'cn_period' => $cn_period ,
			'cn_desc' => $cn_desc ,
			'status' => '1',

			);
			
			$this->db->select('*');
			$this->db->from('oc_caller_numbers');
			$this->db->where('cn_id',$cn_id);
			$query=$this->db->get();
			
			
			if($query->num_rows()>0){
			   
			   $this->db->where('cn_id',$cn_id);
               $res=$this->db->update('oc_caller_numbers', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				$this->db->insert('oc_caller_numbers', $data); 
				if($query){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function update_payment_settings($pp_email,$pp_return_url,$pp_cancel_url,$pp_notify_url,
	$pp_payment_url,$pp_wallet_email,$pp_wallet_return_url,$pp_wallet_cancel_url,$pp_wallet_notify_url,$co_seller_id,
	  $co_base_url,$co_sandbox_url,$co_private_key,$co_publish_key,$co_username,$co_password,$is_sandbox){
		
		try{
			
			$data = array(
						  'pp_email' => $pp_email ,
						  'pp_return_url' => $pp_return_url,
						  'pp_cancel_url' => $pp_cancel_url ,
						  'pp_notify_url' => $pp_notify_url ,
						  'pp_payment_url' => $pp_payment_url,
						  'pp_wallet_email' => $pp_wallet_email ,
						  'pp_wallet_return_url' => $pp_wallet_return_url,
						  'pp_wallet_cancel_url' => $pp_wallet_cancel_url,
						  'pp_wallet_notify_url' => $pp_wallet_notify_url,
						  
						  'tco_sandbox_url' => $co_sandbox_url ,
						  'tco_base_url' => $co_base_url ,
						  'tco_seller_id' => $co_seller_id,
						  'tco_private_key' => $co_private_key ,
						  'tco_publish_key' => $co_publish_key,
						  'tco_is_sandbox' => $is_sandbox,
						  'tco_username' => $co_username,
						  'tco_password' => $co_password
						  
						);
						
			$this->db->select('*');
			$this->db->from('oc_payment_settings');
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			  // $this->db->where('app_id');
               $res=$this->db->update('oc_payment_settings', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				$res=$this->db->insert('oc_payment_settings', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
	}
	public function update_site_settings($allow_card,$allow_paypal,$bid_expire_days,$image_uploads,$sms_panel,
	$email_panel,$payad_expire_days,$vcode_min_val,$vcode_max_val,$vcode_string,$individual_ad_limit,$business_ad_limit,
	$site_business_image_uploads,$site_wallet,$wallet_min_val,$wallet_max_val,$wallet_string,$user_keygen,$bid_expired_time){
		try{
			
			$data = array(
						  'allow_card' => $allow_card ,
						  'allow_paypal' => $allow_paypal,
						  'allow_wallet' => $site_wallet,
						  'bid_expire_days' => $bid_expire_days ,
						  'image_uploads' => $image_uploads ,
						  'sms_panel' => $sms_panel ,
						  'email_panel' => $email_panel ,
						  'payad_expire_days' => $payad_expire_days,
						  'vcode_min_val' => $vcode_min_val ,
						  'vcode_max_val' => $vcode_max_val ,
						  'vcode_string' => $vcode_string,
						  'wallet_min_val' => $wallet_min_val ,
						  'wallet_max_val' => $wallet_max_val ,
						  'wallet_string' => $wallet_string,
						  'business_image_uploads' => $site_business_image_uploads ,
						  'individual_ad_limit' => $individual_ad_limit ,
						  'business_ad_limit' => $business_ad_limit,
						  'user_keygen' => $user_keygen,
						  'bid_refresh_time' => $bid_expired_time
						  
						);
						
			$this->db->select('*');
			$this->db->from('oc_site_settings');
			//$this->db->where('1');
			$query=$this->db->get();
			
			if($query->num_rows()>0){
			   
			  // $this->db->where('app_id');
               $res=$this->db->update('oc_site_settings', $data); 
			   if($res){return true;} else{return false;}
			}
			else{
				
				$res=$this->db->insert('oc_site_settings', $data); 
				if($res){return true;} else{return false;}
			
			}
			
		}
		catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	public function delete_accclass($acc_id){
		    
			$this->db->where('acc_id', $acc_id);
		    $query=$this->db->delete('oc_ad_account_class'); 
            if($query){
				return true;
			}else{
				return false;
			}
	}
	public function delete_currency_code($currency_id){
		    
			$this->db->where('currency_id', $currency_id);
		    $query=$this->db->delete('oc_currencies'); 
            if($query){
				return true;
			}else{
				return false;
			}
	}
	public function delete_sms_prices($p_id){
		   $this->db->where('p_id', $p_id);
		    $query=$this->db->delete('oc_sms_prices'); 
            if($query){
				return true;
			}else{
				return false;
			}
	}
	public function delete_caller_number($cn_id){
		  $this->db->where('cn_id', $cn_id);
		    $query=$this->db->delete('oc_caller_numbers'); 
            if($query){
				return true;
			}else{
				return false;
			}
	}
	
	public function update_email_settings($email_host,$email_username,$email_password,$email_port,$api_id){
		$data = array(
				  'api_username' => $email_username ,
				  'api_password' =>  base64_encode($email_password),
				  'api_url' => $email_host ,
				  'api_port' => $email_port ,
				  'status' => '1' ,
				  
				);

        $this->db->where('api_id', $api_id); 
		$this->db->update('oc_api_settings', $data); 
		return true;
	}
	
	public function get_ftp_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_api_settings');
		$this -> db -> where('api_type = ' . "'ftp'"); 
		$query = $this -> db -> get();
		
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	public function get_email_settings(){
		$this -> db -> select('*');
		$this -> db -> from('oc_api_settings');
		$this -> db -> where('api_type = ' . "'email'"); 
		$query = $this -> db -> get();
		
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
	}
	
	
	
} 
?>