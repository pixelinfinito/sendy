<?php
Class Password_Model extends CI_Model
{
	function check($username,$email_id)
	{
		
		$this -> db -> select('*');
		$this -> db -> from('oc_users');
		$this -> db -> where('user_email',$email_id); 
		$this -> db -> where('username',$username); 
		$this -> db -> where('status',1); 
		$this -> db -> limit(1);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}

	}
	
	public function update_new_password($user_id,$new_password){
		
		$data=array('password'=>$new_password);
		$this->db->where('user_id',$user_id);
		$query=$this->db->update('oc_users',$data);
		if($query){
			return true;
		}else{
			return 0;
		}
	}
	
	
	
	
} 
?>