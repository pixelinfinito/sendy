<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Sms_Model extends CI_Model
{
	
	public function get_cli_timezone_schedules(){
		
		// getting all schedule timezones by country e.g. BN US IN
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->where_in('is_scheduled',1);
		$this->db->where_in('status',0);
		$this->db->where_in('deliver_status',2);
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function get_cli_campaign_schedules($timezone){
		
		if($timezone!=''){
			
			date_default_timezone_set($timezone);
			$dt=date("Y-m-d H:i:s");


		}else{
			date_default_timezone_set("Asia/Brunei");
			$dt=date("Y-m-d H:i:s");
		}
		
		//echo $dt= Date('Y-m-d H:i:s');
		$this -> db -> select('*');
		$this -> db -> from('oc_sms_member_campaign');
		$this->db->where_in('is_scheduled',1);
		$this->db->where('scheduled_datetime <= ',$dt);
		$this->db->where_in('status',0);
		$this->db->where_in('deliver_status',2);
		//$this->db->group_by('campaign_code');
		//$this->db->order_by('campaign_id','desc');
		
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	public function update_cli_campaign_status($campaign_id,$remarks,$message_id,$deliver_status){
	
			//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$timeStamp=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$timeStamp=date("Y-m-d H:i:s");
			}
			
			if($deliver_status!=3){
					 $data = array(
					  'message_id' => $message_id ,
					  'deliver_status' => $deliver_status,
					  'remarks' => $remarks,
					  'do_submit' => $timeStamp,
					  'do_sent' => $timeStamp,
					  'status' => '1'
					  );
			}else{
					$data = array(
					  'message_id' => $message_id ,
					  'deliver_status' => $deliver_status,
					  'remarks' => $remarks,
					  'do_submit' => $timeStamp,
					  'status' => '1'
					  );
			}
			
			$this->db->where('campaign_id',$campaign_id);
			$query=$this->db->update('oc_sms_member_campaign', $data); 
			if($query){return true;}else{return false;}
	}
	public function get_cli_queued_sms(){
		$this -> db -> select('*');
		$this->db->where_in('deliver_status',3); // pulling only queued sms
		$this->db->where_in('status',1);
		$this -> db -> from('oc_sms_member_campaign');
		$query = $this -> db -> get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{return 0;}
	}
	
	public function update_cli_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit){
		//** Gettings timezone based on country code **//
			$remoteCountry=$this->config->item('remote_country_code');
			$this->db->select('*');
			$this->db->from('oc_timezones');
			$this->db->where('country_code',$remoteCountry);
			$query_timezone=$this->db->get();

			if($query_timezone->num_rows()>0){
				$rTimeRow=$query_timezone->result();
				date_default_timezone_set($rTimeRow[0]->zone_name);
				$timeStamp=date("Y-m-d H:i:s");


			}else{
				date_default_timezone_set("Asia/Brunei");
				$timeStamp=date("Y-m-d H:i:s");
			}
			$data = array(
					  'deliver_status' => $deliver_status,
					  'remarks' => $remarks,
					  'do_sent' => $timeStamp,
					  'status' => '1'
					  );
					  
			$this->db->where('campaign_id',$campaign_id);
			$this->db->where('user_id',$user_id);
			$query=$this->db->update('oc_sms_member_campaign', $data); 
			if($query){
				
				// deducting wallet credit
				$cost_update= floatval($accountCredit) - floatval($unit_cost);
				$data2 = array(
					  'balance_amount' => $cost_update,
					  );
				$this->db->where('user_id',$user_id);
				$query2=$this->db->update('oc_account_wallet', $data2); 
			    if($query2){
					return true;
				}
			}
			else{
				return false;
			}
	}
	
} 
?>