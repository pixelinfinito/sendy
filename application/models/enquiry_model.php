<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Enquiry_Model extends CI_Model
{
	function send_enquiry($enq_country,$enq_name,$enq_contact,$enq_email,$enq_type,$enq_comments)
	{
		
			$remoteCountry=$this->config->item('remote_country_code');
			$remoteCity=$this->config->item('remote_geo_city');
			if($remoteCity!=''){
			$geoCity=$remoteCity;
			$geolatitude=$this->config->item('remote_geo_latitude');
			$geolongitude=$this->config->item('remote_geo_longitude');
			}else{
			$geoCity='';
			$geolatitude='';
			$geolongitude='';
			}
			if($this->config->item('remote_country_code')!=''){
				$countryCode=$this->config->item('remote_country_code');
			}else{
			  $countryCode=$this->session->userdata['site_login']['ac_country'];
			}
		  
		   $remote_address="Ip Address : ".$_SERVER['REMOTE_ADDR'].", City : {$remoteCity}, Country : {$remoteCountry} 
		   ,geoLatitude : {$geolatitude}, geoLongitude {$geolongitude}";

			$data = array(
			  'enq_name' => $enq_name ,
			  'enq_type' => $enq_type ,
			  'enq_country' => $enq_country,
			  'enq_email' => $enq_email,
			  'enq_contact' => $enq_contact,
			  'enq_comments' => $enq_comments,
			  'remote_address' => $remote_address,
			  
			);
			

			$sql=$this->db->insert('oc_guest_enquiry', $data); 
			if($sql){
				return 1;
			}else{
				return 0;
			}
		
	}
	
	public function get_enquiries(){
		
		$this->db->select('*');
		$this->db->from('oc_guest_enquiry');
		$this->db->order_by('enq_id','desc');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function delete_enquiry($enquiry_id){
		    $this->db->where('enq_id', $enquiry_id);
		    $query=$this->db->delete('oc_guest_enquiry'); 
			
			if($query){
				return true;
		    }else{
				return 0;
			}
	}
	
	
} 
?>