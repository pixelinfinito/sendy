<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Notification_Model extends CI_Model
{
	 function save_general_notification($notify_type,$user_id,$notify_title,
	 $notify_body,$attachment,$notify_from_name,$member_name,$status)
	 {
		

			$data = array(
			  'notify_type' => $notify_type ,
			  'user_id' => $user_id ,
			  'notify_title' => $notify_title,
			  'notify_body' => $notify_body,
			  'attachment_path' => $attachment,
			  'notify_from' => $notify_from_name,
			  'notify_to' => $member_name,
			  'status' => $status,
			  
			);
			
			$sql=$this->db->insert('oc_notifications', $data); 
			if($sql){
				return 1;
			}else{
				return 0;
			}
		
	}
	
	public function list_notifications(){
		
		$this->db->select('*');
		$this->db->from('oc_notifications');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	public function list_delivered_notifications(){
		
		$this->db->select('*');
		$this->db->from('oc_notifications');
		$this->db->where('status',1);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	public function list_failed_notifications(){
		
		$this->db->select('*');
		$this->db->from('oc_notifications');
		$this->db->where('status',0);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function list_notifications_bymember($user_id){
		$this->db->select('*');
		$this->db->from('oc_notifications');
		$this->db->where('user_id',$user_id);
		$this->db->where('status',1);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function load_notification_byid($notify_id){
		$this->db->select('*');
		$this->db->from('oc_notifications');
		$this->db->where('notify_id',$notify_id);
		$this->db->where('status',1);
		$query=$this->db->get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			$data_arr = array('read_status' => 1);
			$this->db->where('notify_id',$notify_id);
			$this->db->update('oc_notifications', $data_arr); 
			
			return $data;
		}else{
			return 0;
		}
	}
	
	public function load_notification_byadmin($notify_id){
		$this->db->select('*');
		$this->db->from('oc_notifications');
		$this->db->where('notify_id',$notify_id);
		$this->db->where('status',1);
		$query=$this->db->get();
		if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
		}else{
			return 0;
		}
	}
	
	public function delete_notification($notify_id){
		    $this->db->where('notify_id', $notify_id);
		    $query=$this->db->delete('oc_notifications'); 
			
			if($query){
				return true;
		    }else{
				return 0;
			}
    }
	
	public function delete_selected_notifications($notify_ids){
		$notifyArr=array();
		$exp_cids=explode(',',$notify_ids);
		for($i=0;$i<count($exp_cids);$i++){
		array_push($notifyArr,$exp_cids[$i]);
		}

		
		$successArr=array();
		$this->db->select('*');
		$this->db->from('oc_notifications');
		$this->db->where_in('notify_id',$notifyArr);
		$query=$this->db->get();
		
		
		if($query->num_rows()>0){
			
			$c=0;
			foreach($query->result() as $row){
			$this->db->where('notify_id',$row->notify_id);
			$del_query=$this->db->delete('oc_notifications');
			
			$c++;
			}
			return true;
		}else{
			return 0;
		}
	}
	
	public function delete_notification_bymember($notify_id){
		    $data= array('status'=>2);
		    $this->db->where('notify_id', $notify_id);
		    $query=$this->db->update('oc_notifications',$data); 
			
			if($query){
				return true;
		    }else{
				return 0;
			}
    }
	
	
} 
?>