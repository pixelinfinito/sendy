<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Visitors_Model extends CI_Model
{
	function trigger_geo_visit($adRef,$remoteAddress,$remoteCity,$remoteCountry,$geoLatitude,$geoLongitude)
	{
			$data = array(
			  'ad_reference' => $adRef ,
			  'remote_country' => $remoteCountry ,
			  'remote_city' => $remoteCity,
			  'remote_address' => $remoteAddress,
			  'geo_latitude' => $geoLatitude,
			  'geo_longitude' => $geoLongitude,
			 
			);
			
			
			$sql=$this->db->insert('oc_ad_visits', $data); 
			if($sql){
				return 1;
			}else{
				return 0;
			}
		
	}
	function load_geo_visits($adRef)
	{
			$this->db->select('*');
			$this->db->from('oc_ad_visits');
			$this->db->where('ad_reference',$adRef);
			$query=$this->db->get();
			if($query->num_rows()>0){
			foreach($query->result() as $row){
				$data[]=$row;
				
			}
			return $data;
			}else{
				return 0;
			}
		
	}
	
	function list_all_visitors()
	{
		
		$this->db->select('*');
		$this->db->from('oc_ad_visits');
		$this->db->order_by('visit_id','desc');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
		

	}
	function list_all_visitors_byparams($country,$date){
		
		if($date==''){
			$this->db->select('*');
			$this->db->from('oc_ad_visits');
			$this->db->where('remote_country',$country);
			$this->db->order_by('visit_id','desc');
		}else{
			$this->db->select('*');
			$this->db->from('oc_ad_visits');
			$this->db->where('remote_country',$country);
			$this->db->where('DATE(visit_on)',date('Y-m-d',strtotime($date)));
			$this->db->order_by('visit_id','desc');
		
		}
		
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
} 
?>