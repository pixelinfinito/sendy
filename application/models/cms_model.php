<?php

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
Class Cms_Model extends CI_Model
{
	 function save_article($article_type,$article_code,$article_postedby,$article_headline,$article_body)
	 {
		
		$this->db->select('*');
		$this->db->from('oc_articles');
		$this->db->where('article_type',$article_type);
		$query=$this->db->get();
		
		//// if exists update or insert new article
		if($query->num_rows()>0){
			
			 $data1 = array(
			  'article_title' => $article_headline ,
			  'article_code' => $article_code ,
			  'article_type' => $article_type,
			  'article_body' => $article_body,
			  'posted_by' => $article_postedby,
			  'status' =>'1',
			  
			);
			$this->db->where('article_type',$article_type);
			$query2=$this->db->update('oc_articles', $data1); 
			if($query2){
				return 1;
			}else{
				return 0;
			}
		}
		
			 $data2 = array(
			  'article_title' => $article_headline ,
			  'article_code' => $article_code ,
			  'article_type' => $article_type,
			  'article_body' => $article_body,
			  'posted_by' => $article_postedby,
			  'status' =>'1',
			  
			);
			
			$query3=$this->db->insert('oc_articles', $data2); 
			if($query3){
				return 1;
			}else{
				return 0;
			}
		
	}
	
	public function list_articles(){
		
		$this->db->select('*');
		$this->db->from('oc_articles');
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
	public function list_articles_bytype($type){
		
		$this->db->select('*');
		$this->db->from('oc_articles');
		$this->db->where('article_type',$type);
		$query=$this->db->get();
		if($query->num_rows()>0){
		foreach($query->result() as $row){
			$data[]=$row;
			
		}
		return $data;
		}else{
			return 0;
		}
	}
	
} 
?>