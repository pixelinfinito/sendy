<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 

class Sms extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		
	}
	
	public function index()
	{
		$this->load->view('admin/sms/history');
	}
	
	public function history()
	{
		$this->load->view('admin/sms/history');
	}
	public function by_campaign()
	{
		$this->load->view('admin/sms/history_bycampaign');
	}
	
	public function delete_history_contact(){
		$user_id = $this->input->post('uid');
		$campaign_id=mysql_real_escape_string($this->input->post('c_id'));
		
		$rset=$this->sms_model->delete_history_contact($user_id,$campaign_id);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	
	public function load_sms_remarks(){
	           $campaign_id=$this->input->post('cId');
			   $user_id = $this->input->post('uId');
			   $data=$this->sms_model->get_sms_remarks($campaign_id,$user_id);
			  
			   if($data!=0){
			   
			      echo $data[0]->remarks;
			   }else{
				   echo 'failed';
			   }
	}
	
	
}
