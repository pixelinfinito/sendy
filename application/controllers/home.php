<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Home extends CI_Controller {
  
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('countries_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('visitors_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		
	}
	
	public function index()
	{
		
		$this->load->view('front/main');
	}
	public function check_price(){
		$country_code= $this->input->post('ccode');
		$qty= $this->input->post('qty');
		if($country_code!=''){
			$appSettings= $this->app_settings_model->get_primary_settings();
			$appCountry=$appSettings[0]->app_country;
			$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
	        $price_info=$this->countries_model->get_sms_price_bycc($country_code);
			if($price_info!=0){
				if($qty!=''){
					$price='<b style="font-size:30px">'.$currencyInfo[0]->currency_name.' '.$qty*$price_info[0]->price."</b><small> <small>for (".$qty.") sms</small>";
				}else{
					$price=' <b style="font-size:30px">'.$currencyInfo[0]->currency_name.' '.$price_info[0]->price."</b> <small>".$price_info[0]->price_desc."</small>";
				}
			echo "<span class='text-center text-success'>
			      <i class='icon-info'></i> " . _('Pricing Information') . "  
			      ".$price."</span>";
			}else{
				echo "<span class='text-center'><b class='text-danger'>
				<i class='icon-remove'></i> " . _('Sorry pricing not listed.') . "</b></span>";
			}
		}else{
		    echo "<h2>" . _("Invalid token") . "</h2>";
		}
	}
	
  
	
}
