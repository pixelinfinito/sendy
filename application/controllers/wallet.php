<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Wallet extends CI_Controller {
  
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		
		
	}
	
	public function index()
	{
		$this->load->view('admin/wallet/all_transactions');
	}
	public function all_transactions()
	{
		$this->load->view('admin/wallet/all_transactions');
	}
	
	public function transaction_by_member(){
		$this->load->view('admin/wallet/transaction_bymember');
	}
	public function deposit_funds(){
		$this->load->view('admin/wallet/deposit_funds');
	}
	public function rollback_funds(){
		$this->load->view('admin/wallet/rollback_funds');
	}
	public function free_credit_requests(){
		$this->load->view('admin/wallet/free_credit_requests');
	}
	public function member_wallet_credit(){
		$user_id=$this->input->post('uid');
		$wallet_info=$this->wallet_model->get_account_wallet($user_id);
		if($wallet_info!=0){
			$total_amount=$wallet_info[0]->total_amount;
			$balance_amount=$wallet_info[0]->balance_amount;
			echo $total_amount.'__'.$balance_amount;
		}else{
			echo _("failed");
		}
	}
	public function process_deposit(){
		$data 			= json_decode(file_get_contents("php://input"));
		
		$c2TransactionId    = mysql_real_escape_string($data->transaction_id);
		$currencyCode       = mysql_real_escape_string($data->currency);
		$c2TotalAmount      = mysql_real_escape_string($data->deposit_amt);
		$itemNumber         = mysql_real_escape_string($data->wallet_token);
		$walletTotalBalance = mysql_real_escape_string($data->wallet_tamt);
		$walletAvailBalance = mysql_real_escape_string($data->wallet_bamt);
		$userId             = mysql_real_escape_string($data->member_id);
		$remarks            = mysql_real_escape_string($data->payment_remarks);
		
		$c2ResponseCode="System Deposit";
		$c2ResponsMsg="Funds revert/deposit successfully made by system<br>--" . _('Remarks') . "--<br>" . $remarks;
		$c2OrderNumber="SYSTNSORD-".rand(1000,20000);
		
		$paymentStatus='1';
		$payment_mode='System';
		
		$resMsg=$this->wallet_model->add_funds_ByC2o_payment($itemNumber,$c2TransactionId,$currencyCode,$c2OrderNumber,$c2TotalAmount,
	    $c2ResponseCode,$c2ResponsMsg,$paymentStatus,$userId,$walletTotalBalance,$walletAvailBalance,$payment_mode);
		
		if($resMsg!=0){
			echo _("success");
		}else{
			echo _("failed");
		}
	
	}
	public function process_free_credits(){
		$data 			= json_decode(file_get_contents("php://input"));
		
		$c2TransactionId    = mysql_real_escape_string($data->transaction_id);
		$currencyCode       = mysql_real_escape_string($data->currency);
		$c2TotalAmount      = mysql_real_escape_string($data->deposit_amt);
		$itemNumber         = mysql_real_escape_string($data->wallet_token);
		$walletTotalBalance = mysql_real_escape_string($data->wallet_tamt);
		$walletAvailBalance = mysql_real_escape_string($data->wallet_bamt);
		$userId             = mysql_real_escape_string($data->member_id);
		$remarks            = mysql_real_escape_string($data->payment_remarks);
		
		$c2ResponseCode="System Deposit";
		$c2ResponsMsg="Funds revert/deposit successfully made by system<br>--" . _('Remarks') . "--<br>" . $remarks;
		$c2OrderNumber="SYSTNSORD-".rand(1000,20000);
		
		$paymentStatus='1';
		$payment_mode='System';
		
		$resMsg=$this->wallet_model->add_funds_ByC2o_payment($itemNumber,$c2TransactionId,$currencyCode,$c2OrderNumber,$c2TotalAmount,
	    $c2ResponseCode,$c2ResponsMsg,$paymentStatus,$userId,$walletTotalBalance,$walletAvailBalance,$payment_mode);
		
		if($resMsg!=0){
			//// update request status
			
			$status=$this->wallet_model->update_freecredits_status($c2TotalAmount,$userId);
			if($status){
				echo _("success");
			}else{
				echo _("statusfailed");
			}
		}else{
			echo _("failed");
		}
	}
	public function process_rollback(){
		$data 			    = json_decode(file_get_contents("php://input"));
		$transaction_id     = mysql_real_escape_string($data->transaction_id);
		$wallet_token       = mysql_real_escape_string($data->wallet_token);
		$userId             = mysql_real_escape_string($data->member_id);
		$remarks            = mysql_real_escape_string($data->payment_remarks);
		$notify_mail        = mysql_real_escape_string($data->notify_mail);
		$walletTotalBalance = mysql_real_escape_string($data->wallet_tamt);
		$walletAvailBalance = mysql_real_escape_string($data->wallet_bamt);
		$c2TotalAmount      = mysql_real_escape_string($data->deposit_amt);
		
		$resMsg=$this->wallet_model->rollback_system_payments($transaction_id,$wallet_token,$userId,$remarks,$notify_mail,
		$walletTotalBalance,$walletAvailBalance,$c2TotalAmount);
		
		switch($resMsg){
			case 1:
			echo _("success");
			break;
			
			case 0:
			echo _("failed");
			break;
			
			case 2:
			echo _("notfound");
			break;
			
			case 3:
			echo _("notsys");
			break;
		}
		
	}
	
}
