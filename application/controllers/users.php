<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Users extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('user_groups_model','',TRUE);
		$this->load->model('user_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		
	}
	   
	public function index()
	{
		$this->load->view('admin/users/users');
	}
	public function all()
	{
		$this->load->view('admin/users/users');
	}
	
	public function delete_user(){
		
		$user_id = mysql_real_escape_string($this->input->post('user_id'));
		$result = $this->user_model->delete_user($user_id);
		if($result){
			echo _("success");
				
				
		}else{
			   echo _("failed");
		}
	}
	
	
	public function add()
	{
		$this->load->view('admin/users/add_user');
	}
	public function edit(){
		
		$this->load->view('edit/edit_user');
	}
	
	public function add_process()
	{
		$data 			= json_decode(file_get_contents("php://input"));
		$user_name 		= mysql_real_escape_string($data->user_name);
		$password 		= mysql_real_escape_string($data->password);
		$first_name 	= mysql_real_escape_string($data->first_name);
		$last_name 		= mysql_real_escape_string($data->last_name);
		$email_id 		= mysql_real_escape_string($data->email_id);
		$telephone 		= mysql_real_escape_string($data->telephone);
		$telephone2 	= mysql_real_escape_string($data->telephone2);
		$user_groups 	= mysql_real_escape_string($data->user_groups);
		$user_status 	= mysql_real_escape_string($data->user_status);
		
		$result = $this->user_model->add_user($user_name, $password,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status);
		
		if($result){
			
			   echo _("success");
				
		}else{
			    echo _("failed");
				
		}
	}
	public function update_process(){
		$data 			= json_decode(file_get_contents("php://input"));
		$user_id 		= mysql_real_escape_string($data->user_id);
		$first_name 	= mysql_real_escape_string($data->first_name);
		$last_name 		= mysql_real_escape_string($data->last_name);
		$email_id 		= mysql_real_escape_string($data->email_id);
		$telephone 		= mysql_real_escape_string($data->telephone);
		$telephone2 	= mysql_real_escape_string($data->telephone2);
		$user_groups 	= mysql_real_escape_string($data->user_groups);
		$user_status 	= mysql_real_escape_string($data->user_status);
		
		$result = $this->user_model->update_stage_user($user_id,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status);
		
		if($result){
			
			    echo _("success");
		}else{
				
				echo _("failed");
		}
	}
	public function change_password(){
		
		$user_id=$this->session->userdata['login_in']['user_id'];
		$old_password = mysql_real_escape_string($this->input->post('old_password'));
		$new_password = mysql_real_escape_string($this->input->post('new_password'));
		
		if($old_password!=$new_password){
        		
			$result=$this->user_model->update_user_password($user_id,md5($new_password));
			if($result)
			{
				$token=base64_encode("success");
			}else{
				$token=base64_encode("failed");
			}
			redirect(base_url().'users/edit?id='.$user_id.'&token='.$token,true);
		}else{
			$token=base64_encode("match");
			redirect(base_url().'users/edit?id='.$user_id.'&token='.$token,true);
		}
	}
	
	public function thumbnail(){
	   $user_id = mysql_real_escape_string($this->input->post('user_id'));
	   
	   if(isset($_FILES['files'])){
		  
		  
				if(!file_exists('uploads/profiles/'.$user_id)){
					////////// Create a folder on userid ////////////
					 mkdir('uploads/profiles/'.$user_id,0777,true);
				}else{
					////////// Use existing user folder ////////////
					$local_dir='uploads/profiles/'.$user_id;
				}
				
				$file_name = $_FILES['files']['name'];
				$file_size =$_FILES['files']['size'];
				$file_tmp =$_FILES['files']['tmp_name'];
				$file_type=$_FILES['files']['type'];	
				if($file_size > 2097152){
					$errors[]='File size must be less than 2 MB';
				}		
				
				$local_dir='uploads/profiles/'.$user_id;
				$thumb_path=$local_dir."/".$file_name;
				$encFile=$file_name;
				
				if (file_exists($encFile)) {
					echo "<span class='red'>The file $encFile exists</span>";
					exit;
					
				} else 
				{
				   //echo _("The file $encFile does not exist");
				   move_uploaded_file($file_tmp,$local_dir."/".$encFile);
				   $resImages=$this->user_model->update_thumbnail($user_id,$thumb_path);
				
                   if($resImages){
					  
					   	$token=base64_encode("thumb_success");
					    redirect(base_url().'users/edit?id='.$user_id.'&token='.$token,true);
			
				   }else{
					   	$token=base64_encode("failed");
						redirect(base_url().'users/edit?id='.$user_id.'&token='.$token,true);
				   } 				
				
				}
				
				
		   }
	}
}

