<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Caller_Numbers extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
	}
	   
	public function index()
	{
		$this->load->view('caller_numbers/requests');
	}
	public function cn_requests(){
	
		$this->load->view('caller_numbers/requests');
	}
	public function ci_requests(){
	
		$this->load->view('caller_numbers/caller_id');
	}
	public function requests(){
		
		$result=$this->caller_number_model->get_cn_requests();
		$json = json_encode( $result );
		echo $json;
	}
	public function load_ci_requests(){
		
		$result=$this->caller_number_model->get_ci_requests();
		$json = json_encode( $result );
		echo $json;
	}
	public function approve_caller_id(){
		$tid = mysql_real_escape_string($this->input->post('cn_id'));
		$member_info = $this->caller_number_model->get_member_details($tid);
		$result = $this->caller_number_model->approve_caller_id($tid);
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appDefaultName     = $alertAppSettings[0]->app_default_name;
		
		//// preparing alert to member
		$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
		$account_sid        = $alertSMSSettings[0]->twilio_accid;
		$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
		$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
		$sid_approve_token   = $alertSMSSettings[0]->sid_approve_alert_token;
		$enable_alerts      = $alertSMSSettings[0]->enable_alerts;
		
		if($sid_approve_token!=''){
			$alertMasking       = $alertSMSSettings[0]->sid_approve_alert_token;
		}else{
			if($enable_alerts==1){
			    $alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
			}else{
				$alertMasking       = '';	
			}
			
		}
		
		
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
		
		
		if($result){
			echo 'success';
			
			if($member_info!=0){
				$alert_first_name =$member_info[0]->ac_first_name;
				$alert_last_name  =$member_info[0]->ac_last_name;
				$caller_id        =$member_info[0]->twilio_sender_id;
				$memberContact    =$member_info[0]->ac_phone; 
				
				$alertContent       = "Hello ".$alert_first_name." ".$alert_last_name.", your SenderID : ".$caller_id." has been approved, please logon to ".$appDefaultName." for more details."; 
				if($alertMasking!=''){
				//// trigger sms alert to member 
				$status=$client->account->messages->create(array( 
					'To' => $memberContact, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
				));
				}
		
			}
			
			
		}else{
			echo 'failed';
		}
	}
	public function assign_caller_number(){
		$cnId = mysql_real_escape_string($this->input->post('hidtId'));
		$cnNumber = mysql_real_escape_string($this->input->post('cn_number'));
		$result = $this->caller_number_model->update_member_cn($cnId,$cnNumber);
		$member_info = $this->caller_number_model->get_member_details($cnId);
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appDefaultName     = $alertAppSettings[0]->app_default_name;
		
		//// preparing alert to member
		$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
		$account_sid        = $alertSMSSettings[0]->twilio_accid;
		$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
		$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
		$sid_approve_token  = $alertSMSSettings[0]->sid_approve_alert_token;
		$enable_alerts      = $alertSMSSettings[0]->enable_alerts;
		
		if($sid_approve_token!=''){
			$alertMasking       = $alertSMSSettings[0]->sid_approve_alert_token;
		}else{
			if($enable_alerts==1){
			    $alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
			}else{
				$alertMasking       = '';	
			}
			
		}
		
		
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
		
		if($result){
			echo 'success';
			
			if($member_info!=0){
				$alert_first_name =$member_info[0]->ac_first_name;
				$alert_last_name  =$member_info[0]->ac_last_name;
				$caller_no        =$member_info[0]->twilio_origin_number;
				$memberContact    =$member_info[0]->ac_phone; 
				
				$alertContent       = "Hello ".$alert_first_name." ".$alert_last_name.", a new caller number : ".$caller_no." has been approved to you, please logon to ".$appDefaultName." for more details."; 
				if($alertMasking!=''){
				//// trigger sms alert to member 
				$status=$client->account->messages->create(array( 
					'To' => $memberContact, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
				));
				}
		
			}
			
		}else{
			echo 'failed';
		}
	}
	public function suspend_caller_number(){
		$cnId = mysql_real_escape_string($this->input->post('hidtId'));
		$cnNumber = mysql_real_escape_string($this->input->post('cn_number'));
		$remarks  = mysql_real_escape_string($this->input->post('remarks'));
		
		$result = $this->caller_number_model->suspend_caller_number($cnId,$cnNumber,$remarks);
		if($result){
			echo 'success';
		}else{
			echo 'failed';
		}
	}
	public function suspend_caller_id(){
		$cnId = mysql_real_escape_string($this->input->post('hidtId'));
		$ciTag = mysql_real_escape_string($this->input->post('cn_id'));
		$remarks  = mysql_real_escape_string($this->input->post('remarks'));
		
		$result = $this->caller_number_model->suspend_caller_id($cnId,$ciTag,$remarks);
		if($result){
			echo 'success';
		}else{
			echo 'failed';
		}
	}
	public function check_caller_id(){
		$ci_tag = mysql_real_escape_string($this->input->post('ci_tag'));
		$user_id = mysql_real_escape_string($this->input->post('uid'));
		
		$result = $this->caller_number_model->check_caller_id($ci_tag,$user_id);
		if($result){
			echo '<h4 class="text-success"><i class="fa fa-check"></i> ' . _('Available') . ' </h4>';
		}else{
			echo '<h4 class="text-danger"><i class="fa fa-times"></i> ' . _('Taken') . ' </h4>';
		}
	}
	
	
	
	public function export_users_to_excel(){
		$this->load->model('user_groups_model','',TRUE);
		$this->load->model('user_model','',TRUE);
		$this->load->view('export/export_pb_users');
	}
	
	public function delete_process(){
		$data = json_decode(file_get_contents("php://input"));
		$user_id = mysql_real_escape_string($data->user_id);
		$this->load->model('user_model','',TRUE);
		$result = $this->user_model->delete_user($user_id);
		if($result){
			
			    $arr = array('msg' => "success", 'error' => '');
				$jsn = json_encode($arr);
				print_r($jsn);
				
				
		}else{
			    $arr = array('msg' => "", 'error' => 'OOPS request failed ..!');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}
	}
	
	
	public function add()
	{
		$this->load->model('user_groups_model','',TRUE);
		$this->load->model('user_model','',TRUE);
		$this->load->view('add_user');
	}
	public function edit(){
		$this->load->model('user_groups_model','',TRUE);
		$this->load->model('user_model','',TRUE);
		$this->load->view('edit/edit_user');
	}
	
	public function add_process()
	{
		$data 			= json_decode(file_get_contents("php://input"));
		$user_name 		= mysql_real_escape_string($data->user_name);
		$password 		= mysql_real_escape_string($data->password);
		$first_name 	= mysql_real_escape_string($data->first_name);
		$last_name 		= mysql_real_escape_string($data->last_name);
		$email_id 		= mysql_real_escape_string($data->email_id);
		$telephone 		= mysql_real_escape_string($data->telephone);
		$telephone2 	= mysql_real_escape_string($data->telephone2);
		$user_groups 	= mysql_real_escape_string($data->user_groups);
		$user_status 	= mysql_real_escape_string($data->user_status);
		
		$this->load->model('user_model','',TRUE);
		$this->load->model('tools_model','',TRUE);
		$result = $this->user_model->add_user($user_name, $password,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status);
		
		if($result){
			
			   echo _("success");
				
		}else{
			    echo _("failed");
				
		}
	}
	public function update_process(){
		$data 			= json_decode(file_get_contents("php://input"));
		$user_id 		= mysql_real_escape_string($data->user_id);
		$first_name 	= mysql_real_escape_string($data->first_name);
		$last_name 		= mysql_real_escape_string($data->last_name);
		$email_id 		= mysql_real_escape_string($data->email_id);
		$telephone 		= mysql_real_escape_string($data->telephone);
		$telephone2 	= mysql_real_escape_string($data->telephone2);
		$user_groups 	= mysql_real_escape_string($data->user_groups);
		$user_status 	= mysql_real_escape_string($data->user_status);
		
		$this->load->model('user_model','',TRUE);
		
		$result = $this->user_model->update_stage_user($user_id,$first_name,$last_name,$email_id,$telephone,$telephone2,$user_groups,$user_status);
		
		if($result){
			
			    echo _("success");
		}else{
				
				echo _("failed");
		}
	}
}

