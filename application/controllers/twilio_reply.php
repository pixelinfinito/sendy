<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Twilio_Reply extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('app_settings_model','',TRUE);
	}
	   
	public function index()
	{
	
    $app_settings=$this->app_settings_model->get_app_settings();	
    header("content-type: text/xml");
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
     ?>
	<Response>
		<Message><?php echo _("Hello thank you for message, for any assistance please contact")?><?php echo $app_settings[0]->app_default_name;?> <?php echo _("support team at")?><?php echo $app_settings[0]->default_support_email;?> </Message>
	</Response>
	<?php
	}
	
}

