<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Company extends CI_Controller {
   
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('countries_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('cms_model','',TRUE);
		$this->load->model('visitors_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		
	}
	   
	public function about_us()
	{
		
		$this->load->view('front/about_us');
	}
	
	public function careers()
	{
		
		$this->load->view('front/careers');
	}
	
	
	
}
