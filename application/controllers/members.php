<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Members extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		
	}
	
	public function index()
	{
		$this->load->view('admin/members/all');
	}
	public function all()
	{
		$this->load->view('admin/members/all');
	}
	public function details()
	{
		$this->load->view('admin/members/member_details');
	}
	public function add(){
		
		$this->load->view('admin/members/add_member');
	}
	public function edit(){
		
		$this->load->view('admin/members/edit_member');
	}
	public function blacklist(){
		
		$this->load->view('admin/members/block_member');
	}
	
	public function delete_member(){
		$user_id	 			= mysql_real_escape_string($this->input->post('uid'));
		$id	 					= mysql_real_escape_string($this->input->post('id'));
		$result = $this->account_model->delete_member($user_id,$id);
		
		if($result){
			echo 'success';
		}else{
			echo 'failed';
		}
	}
	
	public function load_sms_remarks(){
		   $campaign_id=$this->input->post('cId');
		   $user_id = $this->input->post('uId');
		   $data=$this->sms_model->get_sms_remarks($campaign_id,$user_id);
		  
		   if($data!=0){
		   
			  echo $data[0]->remarks;
		   }else{
			   echo 'failed';
		   }
	}
	
	public function inactive_member(){
		$user_id	 			= mysql_real_escape_string($this->input->post('uid'));
		$id	 					= mysql_real_escape_string($this->input->post('id'));
		$notify_status	 		= mysql_real_escape_string($this->input->post('notify_status'));
		
		$result = $this->account_model->inactive_member($user_id);
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appDefaultName     = $alertAppSettings[0]->app_default_name;
		$member_info = $this->account_model->get_member_details($user_id);
		
		if($member_info!=0){
			$memberContact=$member_info[0]->ac_phone;
			$memberEmail=$member_info[0]->ac_email;
			$memberName =$member_info[0]->ac_first_name.' '.$member_info[0]->ac_last_name;
				
		}else{
			$memberContact='';
			$memberEmail='';
			$memberName='';
		}
		
		if($result){
			
			if($notify_status!=''){
				
				switch($notify_status){
					
					case 'sms':
					//// preparing alert to member
					$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
					$account_sid        = $alertSMSSettings[0]->twilio_accid;
					$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
					$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
					$app_sender_id      = $alertSMSSettings[0]->twilio_sender_id;
					
					if($app_sender_id!=''){
						$alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
					}else{
						$alertMasking       = 'StsAlerts';
					}
					include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
					$client = new Services_Twilio($account_sid, $auth_token); 
					
					$alertContent=sprintf(_("Hello %s, Your %s's account has been inactivated, contact our support team for more details."), $memberName, $appDefaultName);
					if($alertMasking!=''){
						//// trigger sms alert to member 
						$status=$client->account->messages->create(array( 
							'To' => $memberContact, 
							'From' => $alertMasking, 
							'Body' => $alertContent,   
						));
					}
					
					break;
					
					case 'email':
					
					if($alertAppSettings[0]->default_from_mail!=''){
						$from_emails=$alertAppSettings[0]->default_from_mail;
					}else{
						$from_emails = 'contact@saidapro.com';
					}
					
					 if($alertAppSettings[0]->app_default_name!=''){
						$app_name=$alertAppSettings[0]->app_default_name;
					}else{
						$app_name = 'OTGApp';
					}
				
				
		              
					$full_name       =  sprintf(_("%s\'s Member"), $app_name);
					$notify_from     =  $from_emails;
					
					$notify_subject  =  sprintf(_("Your %s has been inactivated"), $app_name);
					$notify_body     =  sprintf(_("Dear %s"), $memberName) . ", <br><br> " . sprintf(_("Your %s account has been inactivated."), $app_name) . "<br>";
					$notify_body.= sprintf(_("Please contact %s's support team for more details."), $app_name) . "<br>";
					$notify_body.= _("Thank you") . ".<br><br>" . _("Best regards") . ",<br><strong>".$app_name."</strong>";
					$notify_to  = $memberEmail;
					
					$this->load->library('email');
					$this->email->from($notify_from, $app_name);
					$this->email->to($notify_to);
					$this->email->subject($notify_subject);
					$this->email->message($notify_body);
					$this->email->send();
					
					break;
				}
			}
			
			
			
			?>
			<div id="inactive_placeholder<?php echo $id?>">
			  <span class="label label-warning" id="inactive_label<?php echo $id?>"><?php echo _("Inactive")?></span><br>
			  <a href="javascript:void(0)" onclick="javascript:active_member('<?php echo $user_id?>','<?php echo $id?>')" class="text-success" title="<?php echo _('Change to Active') ?>">
			  <small><u><?php echo _("Activate")?></u></small>
			  </a>
			 </div>
			<?php
		}
		
	}
	public function active_member(){
		$user_id	 		= mysql_real_escape_string($this->input->post('uid'));
		$id	 				= mysql_real_escape_string($this->input->post('id'));
		$notify_status	 	= mysql_real_escape_string($this->input->post('notify_status'));
		
		$result 			= $this->account_model->active_member($user_id);
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appDefaultName     = $alertAppSettings[0]->app_default_name;
		$member_info = $this->account_model->get_member_details($user_id);
		
		if($member_info!=0){
			$memberContact=$member_info[0]->ac_phone;
			$memberEmail=$member_info[0]->ac_email;
			$memberName =$member_info[0]->ac_first_name.' '.$member_info[0]->ac_last_name;
				
		}else{
			$memberContact='';
			$memberEmail='';
			$memberName='';
		}
		
		if($result){
			
			if($notify_status!=''){
				
				switch($notify_status){
					
					case 'sms':
					//// preparing alert to member
					$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
					$account_sid        = $alertSMSSettings[0]->twilio_accid;
					$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
					$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
					$app_sender_id      = $alertSMSSettings[0]->twilio_sender_id;
					
					if($app_sender_id!=''){
						$alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
					}else{
						$alertMasking       = 'StsAlerts';
					}
					include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
					$client = new Services_Twilio($account_sid, $auth_token); 
					
					$alertContent=sprintf(_("Congratulations your %s's account back to active, please logon to your account."),$appDefaultName);
					if($alertMasking!=''){
						//// trigger sms alert to member 
						$status=$client->account->messages->create(array( 
							'To' => $memberContact, 
							'From' => $alertMasking, 
							'Body' => $alertContent,   
						));
					}
					
					break;
					
					case 'email':
					
					if($alertAppSettings[0]->default_from_mail!=''){
						$from_emails=$alertAppSettings[0]->default_from_mail;
					}else{
						$from_emails = 'contact@onetextglobal.com';
					}
					
					 if($alertAppSettings[0]->app_default_name!=''){
						$app_name=$alertAppSettings[0]->app_default_name;
					}else{
						$app_name = 'OTGApp';
					}
				
				
		              
					$full_name       =  $app_name." \'s Member";
					$notify_from     =  $from_emails;
					
					$notify_subject  =  sprintf(_("Congratulations your %s is back to active"), $app_name);
					$notify_body     =  sprintf(_("Dear %s"), $memberName) . ", <br><br> " . sprintf(_("Your %s account is back to online, logon to your account.", $app_name)) . "<br>";
					$notify_body.= sprintf(_("For any assistance please contact %s's support team."), $app_name) . "<br>";
					$notify_body.= _("Thank you") . ".<br><br>" . _("Best regards") . ",<br><strong>".$app_name."</strong>";
					$notify_to  = $memberEmail;
					
					$this->load->library('email');
					$this->email->from($notify_from, $app_name);
					$this->email->to($notify_to);
					$this->email->subject($notify_subject);
					$this->email->message($notify_body);
					$this->email->send();
					
					break;
				}
			}
				
			
			?>
			<div id="active_placeholder<?php echo $id?>">
			  <span class="label label-success" id="active_label<?php echo $id?>"><?php echo _("Active")?></span><br>
			  <a href="javascript:void(0)" onclick="javascript:inactive_member('<?php echo $user_id?>','<?php echo $id?>')" class="text-danger" title="<?php echo _('Change to Inactive') ?>">
			  <small><u><?php echo _("Inactive")?></u></small>
			  </a>
			  </div>
			</div>
			<?php
		}
		
	}
	public function blackwhitelist_process(){
		$data 			= json_decode(file_get_contents("php://input"));
		$user	 		= mysql_real_escape_string($data->user_id);
		$block_status 	= mysql_real_escape_string($data->block_status);
		$remarks 	    = mysql_real_escape_string($data->remarks);
		$notify_mail 	= mysql_real_escape_string($data->notify_mail);
		
		$expuser=explode('__',$user);
		$user_id=$expuser[0];
		$user_email=$expuser[1];
		
		$member_info = $this->account_model->get_member_details($user_id);
		$result = $this->account_model->blacklist_whitelist_member($user_id,$block_status,$remarks,$notify_mail,$user_email);
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appDefaultName     = $alertAppSettings[0]->app_default_name;
		
		//// preparing alert to member
		$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
		$account_sid        = $alertSMSSettings[0]->twilio_accid;
		$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
		$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
		$blacklist_token    = $alertSMSSettings[0]->blacklist_alerts;
		$enable_alerts      = $alertSMSSettings[0]->enable_alerts;
		
		if($blacklist_token!=''){
			$alertMasking       = $alertSMSSettings[0]->blacklist_alerts;
		}else{
			if($enable_alerts==1){
			    $alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
			}else{
				$alertMasking       = '';	
			}
			
		}
		
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
		
		
		switch($result){
			case 1:
			echo _("success");
			
			if($member_info!=0){
				$alert_first_name =$member_info[0]->ac_first_name;
				$alert_last_name  =$member_info[0]->ac_last_name;
				$memberContact    =$member_info[0]->ac_phone; 
				
				if($block_status==1){
					$alertContent       = sprintf(
						_("Hello %s %s, Your account has been blocked please contact %s's support team for assistance."),
						$alert_first_name, $alert_last_name, $appDefaultName
					);
				}else{
					$alertContent       = sprintf(
						_("Congratulations %s %s, Your %s 's account is back to online please logon to your account."),
						$alert_first_name, $alert_last_name, $appDefaultName
					);
				}
				
				if($alertMasking!=''){
				//// trigger sms alert to member 
				$status=$client->account->messages->create(array( 
					'To' => $memberContact, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
				));
				}
		
			}
			
			break;
			
			case 0:
			echo _("failed");
			break;
			
			case 2:
			echo _("notify_failed");
			break;
		}
		
	}
	public function register_process(){
		
		
		$data 			    = json_decode(file_get_contents("php://input"));
		$ac_first_name 		= mysql_real_escape_string($data->ac_first_name);
		$ac_last_name 		= mysql_real_escape_string($data->ac_last_name);
		$ac_phone 	        = mysql_real_escape_string($data->ac_phone);
		$ac_country 		= mysql_real_escape_string($data->ac_country);
		$ac_class 		    = mysql_real_escape_string($data->ac_class);
		$ac_gender 		    = mysql_real_escape_string($data->ac_gender);
		$ac_about 	        = mysql_real_escape_string($data->ac_about);
		$ac_email 	        = mysql_real_escape_string($data->ac_email);
		$ac_password 	    = mysql_real_escape_string($data->ac_password);
		$created_by 	    = mysql_real_escape_string($data->created_by);
		$ac_active_status 	= mysql_real_escape_string($data->ac_active_status);
		
		
		$result = $this->account_model->add_account($ac_email,$ac_class,$ac_password,$ac_first_name,$ac_last_name,$ac_phone,
		$ac_gender,$ac_about,$ac_country,$created_by,$ac_active_status);
		
		switch($result){
			case 1:
			echo _("success");
			break;
			
			case 0:
			echo _("failed");
			break;
			
			case 2:
			echo _("duplicated");
			break;
		}
		
	}
	
	public function update_process(){
		
		
		$data 			    = json_decode(file_get_contents("php://input"));
		$user_id 		    = mysql_real_escape_string($data->user_id);
		$ac_first_name 		= mysql_real_escape_string($data->ac_first_name);
		$ac_last_name 		= mysql_real_escape_string($data->ac_last_name);
		$ac_phone 	        = mysql_real_escape_string($data->ac_phone);
		$ac_country 		= mysql_real_escape_string($data->ac_country);
		$ac_class 		    = mysql_real_escape_string($data->ac_class);
		$ac_gender 		    = mysql_real_escape_string($data->ac_gender);
		$ac_about 	        = mysql_real_escape_string($data->ac_about);
		
		$result = $this->account_model->backend_update_account($user_id,$ac_class,$ac_first_name,$ac_last_name,$ac_phone,
		$ac_gender,$ac_about,$ac_country);
		
		switch($result){
			case 1:
			echo _("success");
			break;
			
			case 0:
			echo _("failed");
			break;
			
		}
		
	}
	
	public function load_campaign_headline(){
	           $campaign_code=$this->input->post('c_code');
			   $user_id = $this->input->post('uid');
			   $data=$this->sms_model->get_campaign_headline($campaign_code,$user_id);
			  
			   if($data!=0){
			   $dxc_code=$data[0]->campaign_code;
			   $dxc_message=$data[0]->message;
			   echo $dxc_code.'__'.$dxc_message;
			   }else{
				   echo 'failed';
			   }
	}
	public function delete_campaign(){
		$user_id=$this->input->post('uid');
		$campaign_code=mysql_real_escape_string($this->input->post('campaign_code'));
		
		$rset=$this->sms_model->delete_campaign($user_id,$campaign_code);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
 
   public function caller_number_headline(){
	           $tId     =  $this->input->post('tId');
			   $user_id = $this->input->post('uId');
			   $data=$this->sms_model->get_caller_number_headline($tId,$user_id);
			  
			   if($data!=0){
			   $cn_tid=$data[0]->tid;
			   $cn_tno=$data[0]->twilio_origin_number;
			   $cn_status=$data[0]->status;
			   	
			   echo $cn_tid.'__'.$cn_tno.'__'.$cn_status;
			   }else{
				   echo 'failed';
			   }
	}
	public function delete_caller_number(){
		$user_id = $this->input->post('uId');
		$cn_tid=mysql_real_escape_string($this->input->post('cn_tid'));
		
		$rset=$this->sms_model->delete_caller_number($user_id,$cn_tid);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	
	public function load_modal_contact(){
	           $contact_id=$this->input->post('cId');
			   $user_id = $this->input->post('uId');
			   $data=$this->sms_model->get_single_contact_bycid($contact_id,$user_id);
			  
			   if($data!=0){
			   $mv_mxc_id=$data[0]->c_id;
			   $mv_mxc_mobile=$data[0]->contact_mobile;
			   $mv_mxc_name=$data[0]->contact_name;
			   $mv_mxc_email=$data[0]->contact_email;
			   $mv_mxc_ccode=$data[0]->country_code;
			   $mv_mxc_groupname=$data[0]->group_name;
			   
			   echo $mv_mxc_id.'__'.$mv_mxc_mobile.'__'.$mv_mxc_name.'__'.$mv_mxc_email.'__'.$mv_mxc_ccode.'__'.$mv_mxc_groupname;
			   }else{
				   echo 'failed';
			   }
	}
	public function delete_sms_contact(){
		$user_id = $this->input->post('uId');
		$contact_id=mysql_real_escape_string($this->input->post('contact_id'));
		
		$rset=$this->sms_model->delete_sms_contact($user_id,$contact_id);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	
	public function delete_history_contact(){
		$user_id = $this->input->post('uid');
		$campaign_id=mysql_real_escape_string($this->input->post('c_id'));
		
		$rset=$this->sms_model->delete_history_contact($user_id,$campaign_id);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	
}
