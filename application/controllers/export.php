<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Export extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/twilio_model','',TRUE);
		$this->load->model('notification_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('promotions_model','',TRUE);
		
	}
	
	public function export_contacts(){
		
		$this->load->view('admin/export/sms/export_contacts');
	}
	public function export_members(){
		
		$this->load->view('admin/export/members/export_members');
	}
	public function export_campaigns(){
		
		$this->load->view('admin/export/sms/export_campaigns');
	}
	public function export_all_campaigns(){
		
		$this->load->view('admin/export/campaigns/export_all_campaigns');
	}
	public function export_all_scheduled_campaigns(){
		
		$this->load->view('admin/export/campaigns/export_all_scheduled_campaigns');
	}
	public function export_campaigns_bymember(){
		$this->load->view('admin/export/campaigns/export_campaigns_bymember');
	}
	public function export_groups(){
		
		$this->load->view('admin/export/sms/export_groups');
	}
	
	public function export_template(){
		
		$this->load->view('admin/export/sms/export_template');
	}
	
	public function export_schedules(){
		
		$this->load->view('admin/export/sms/export_schedules');
	}
	public function export_enquiries(){
		
		$this->load->view('admin/export/enquiry/export_enquiries');
	}
	
	public function export_notifications(){
		
		$this->load->view('admin/export/sms/export_notifications');
	}
	
	public function export_all_notifications(){
		
		$this->load->view('admin/export/notifications/export_notifications');
	}
	public function export_promotions(){
		
		$this->load->view('admin/export/promotions/export_promotions');
	}
	
	public function export_caller_numbers(){
		
		$this->load->view('admin/export/sms/export_caller_numbers');
	}
	
	public function export_all_cn(){
		
		$this->load->view('admin/export/caller_numbers/export_all_cn');
	}
	
	public function export_all_ci(){
		
		$this->load->view('admin/export/caller_numbers/export_all_ci');
	}
	
	public function export_pricing(){
		$this->load->view('admin/export/sms/export_pricing');
	}
	public function export_history(){
		$this->load->view('admin/export/sms/export_history');
	}
	public function export_all_history(){
		$this->load->view('admin/export/sms/export_all_history');
	}
	public function export_history_bycampaign(){
		$this->load->view('admin/export/sms/export_history_bycampaign');
	}
	
	public function export_success_history(){
		$this->load->view('admin/export/sms/export_success_history');
	}
	
	public function export_failed_history(){
		$this->load->view('admin/export/sms/export_failed_history');
	}
	
	public function invoice(){
		$this->load->library('m_pdf');
		$this->load->view('admin/export/invoice/export_invoice');
		
	}
	public function payment_invoice(){
		$this->load->library('m_pdf');
		$this->load->view('admin/export/invoice/export_payment_invoice');
	}
	
	public function export_payment_history(){
		$this->load->view('admin/export/payments/export_payment_history');
	}
	public function export_all_payments(){
		$this->load->view('admin/export/payments/export_all_payments');
	}
	public function export_payments_bymember(){
		$this->load->view('admin/export/payments/export_payments_bymember');
	}
	public function export_all_wallet(){
		$this->load->view('admin/export/wallet/export_all_wallet');
	}
	
	public function export_wallet_history(){
		$this->load->view('admin/export/wallet/export_wallet_history');
	}
	
	public function export_wallet_bymember(){
		$this->load->view('admin/export/wallet/export_wallet_bymember');
	}
	
	
	
 
	
}
