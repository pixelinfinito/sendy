<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Notifications extends CI_Controller {
    
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('notification_model','',TRUE);
	}
	   
	public function index()
	{
		
		$this->load->view('admin/notifications/send');
	}
	public function send()
	{
		
		$this->load->view('admin/notifications/send');
	}
	
	public function all()
	{
		$this->load->view('admin/notifications/all');
	}
	
	public function member_wallet_credit(){
		$user_id=$this->input->post('uid');
		$wallet_info=$this->wallet_model->get_account_wallet($user_id);
		if($wallet_info!=0){
			$total_amount=$wallet_info[0]->total_amount;
			$balance_amount=$wallet_info[0]->balance_amount;
			echo $total_amount.'__'.$balance_amount;
		}else{
			echo _("failed");
		}
	}
	
	 public function process_general_notification(){
	   
	   		$notify_type		    = $this->input->post('notify_type');
			$notify_from_name    	= $this->input->post('notify_from');
			$notify_from_mail  		= $this->input->post('notify_from_mail');
			$notify_to  		    = $this->input->post('notify_to');
			$notify_headline        = $this->input->post('notify_headline');
			$notify_message         = $this->input->post('notify_message');
			$check_sendby_mail      = $this->input->post('notify_sendby_mail');
			
			$notify_body  = $notify_message; 
			$notify_title = $notify_headline;
			
			$intlib=$this->internal_settings->local_settings();
			$notify_from_email2= $intlib[0]->default_from_mail;
			$notify_appname= $intlib[0]->app_default_name;

			$name  			    = $notify_appname;
			$upload_folder      = 'attachments/replies/';
			$name_of_uploaded_file =  basename($_FILES['file']['name']);
			if($name_of_uploaded_file!='')
			{
				$attachment = $upload_folder . $name_of_uploaded_file;
				
				$tmp_path = $_FILES["file"]["tmp_name"];
				if(is_uploaded_file($tmp_path))
				{
					if(!copy($tmp_path,$attachment))
					{
						$errors .= '\n error while copying the uploaded file';
					}
				}
			}else{
				$attachment='';
			}
			?>
			<script src="<?php echo base_url()?>js/jquery-2.1.1.js"></script>
			<link href="<?php echo base_url()?>theme4.0/admin/css/bootstrap.min.css" rel="stylesheet">
			<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/admin.css">
		
			<div class="content">
			<div class="row">
			<div class='col-md-3'>&nbsp;</div>
			<div class='col-md-3 text-right'>
			<img src="<?php echo base_url()?>theme4.0/admin/images/logo_black.png"> 
			</div>
			<div class='col-md-3 text-left'><h3 class="text-muted">| Notifications</h3></div>
			<div class='col-md-3'>&nbsp;</div>
			</div>
			<br><br>
			
			<div class="row">
			<div class='col-md-3'>&nbsp;</div>
			<div class='col-md-6'>
			<div class="box padding_20">
			<div class="box-body">
			<?php			
 
			if($check_sendby_mail==1){
				
				
				if($notify_to=='all')
				{
					$to_mails_list =$this->account_model->list_all_members();
					$list_mail='';
					if($name_of_uploaded_file!=''){
						$this->email->attach($attachment);
					}
						
					foreach($to_mails_list as $member){
						//$list_mail.=','.$member->ac_email;
						$this->load->library('email');
						$this->email->from($notify_from_mail,$name);
						$this->email->to($member->ac_email);
						//$this->email->cc('another@example.com');
						$this->email->subject($notify_headline);
						//$this->email->cc($cc_email);
						//$this->email->bcc($bcc_email);
						$this->email->message($notify_message);
						$member_name=$member->ac_first_name.' '.$member->ac_last_name;
						
						if ($this->email->send()) 
						{
						
							$user_id=$member->user_id;
							$status = '1';
							$rset=$this->notification_model->save_general_notification($notify_type,$user_id,$notify_title,
							$notify_body,$attachment,$notify_from_name,$member_name,$status);
							if($rset){echo _("Mail sent to").$member->ac_email;}
						}
						else
						{
							
							$user_id=$member->user_id;
							$status = '0';
							$rset=$this->notification_model->save_general_notification($notify_type,$user_id,$notify_title,
							$notify_body,$attachment,$notify_from_name,$member_name,$status);
							if($rset){echo _("Unable to sending mail to").$member->ac_email;}
							
						}

						
				
					}// end foreach
					//$to_email = $list_mail;
					
				}else{
					$to_mails_list =$this->account_model->get_member_details($notify_to);
					$to_email = $to_mails_list[0]->ac_email;
					
					//$list_mail.=','.$member->ac_email;
					$this->load->library('email');
					$this->email->from($notify_from_mail,$name);
					$this->email->to($to_email);
					//$this->email->cc('another@example.com');
					$this->email->subject($notify_headline);
					//$this->email->cc($cc_email);
					//$this->email->bcc($bcc_email);
					$this->email->message($notify_message);
					
					if($name_of_uploaded_file!=''){
						$this->email->attach($attachment);
					}
					
						$member_name=$to_mails_list[0]->ac_first_name.' '.$to_mails_list[0]->ac_last_name;
						if ($this->email->send()) 
						{
						
							$user_id=$to_mails_list[0]->user_id;
							$status = '1';
							$rset=$this->notification_model->save_general_notification($notify_type,$user_id,$notify_title,
							$notify_body,$attachment,$notify_from_name,$member_name,$status);
							if($rset){echo _("Mail sent to").$to_email;}
						}
						else
						{
							
							$user_id=$to_mails_list[0]->user_id;
							$status = '0';
							$rset=$this->notification_model->save_general_notification($notify_type,$user_id,$notify_title,
							$notify_body,$attachment,$notify_from_name,$member_name,$status);
							if($rset){echo _("Unable to sending mail to").$to_email;}
						}
				}
				
			
			}else{
				
				//// sending plain text only
				if($notify_to=='all')
				{
					$to_mails_list =$this->account_model->list_all_members();
					foreach($to_mails_list as $member){
						$member_name=$to_mails_list[0]->ac_first_name.' '.$to_mails_list[0]->ac_last_name;
						$user_id=$member->user_id;
						$status=1;
						
						$rset=$this->notification_model->save_general_notification($notify_type,$user_id,$notify_title,
						$notify_body,$attachment,$notify_from_name,$member_name,$status);
						if($rset){echo _("Plaintext sent to").$member_name;}
					}
					
				}else{
					
						$to_mails_list =$this->account_model->get_member_details($notify_to);
						$member_name=$to_mails_list[0]->ac_first_name.' '.$to_mails_list[0]->ac_last_name;
						$user_id=$to_mails_list[0]->user_id;
						$status=1;
						
						$rset=$this->notification_model->save_general_notification($notify_type,$user_id,$notify_title,
						$notify_body,$attachment,$notify_from_name,$member_name,$status);
						if($rset){echo _("Plaintext sent to").$member_name;}
						
				}
			}
			?>
			 <hr>
				<span class="text-danger">
				<i class='fa fa-spinner fa-spin'></i> <b><?php echo _("Please wait")?> ..</b>
				</span><br>

				</div>
				</div>
				</div>
				<div class='col-md-3'>&nbsp;</div>
				</div>
			</div> 
			 
			 <script>
				$(document).ready(function () {
				// Handler for .ready() called.
				window.setTimeout(function () {
				location.href = "<?php echo base_url()?>notifications/all";
				}, 5000);
				});
			 </script>
			<?php
			
		   
   }
   public function email_progress(){
	   
	   $this->load->view('notification_progress');
   }
   
   public function load_notification(){
	           $notify_id=$this->input->post('notify_id');
			   $data=$this->notification_model->load_notification_byadmin($notify_id);
			  
			   if($data!=0){
				   echo $data[0]->notify_body;
			   }else{
				   echo 'failed';
			   }
	}
	
	 public function delete_notification(){
	           $notify_id=$this->input->post('notify_id');
			   $res=$this->notification_model->delete_notification($notify_id);
			  
			   if($res){
				   echo 'success';
			   }else{
				   echo 'failed';
			   }
	}
	public function delete_selected_notifications(){
		$notify_ids=mysql_real_escape_string($this->input->post('cIds'));
		$res=$this->notification_model->delete_selected_notifications($notify_ids);
		
		if($res!=0){
			echo 'success';
			
		}else{
			echo 'failed';
		}
	}
	
}
