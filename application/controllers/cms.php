<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Cms extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('cms_model','',TRUE);
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		
	}
	   
	public function index()
	{
		
		$this->load->view('admin/cms/articles');
	}
	public function articles()
	{
		
		$this->load->view('admin/cms/articles');
	}
	public function about_us()
	{
		$mode['token']= 'About Us';
		$mode['type'] = 'about_us';
		$this->load->view('admin/cms/post_article',$mode);
	}
	public function careers()
	{
		$mode['token']= 'Careers';
		$mode['type'] = 'careers';
		$this->load->view('admin/cms/post_article',$mode);
	}
	public function terms_conditions()
	{
		$mode['token']= 'Terms & Conditions';
		$mode['type'] = 'terms_conditions';
		$this->load->view('admin/cms/post_article',$mode);
	}
	
	public function privacy_policy()
	{
		$mode['token']= 'Privacy Policy';
		$mode['type'] = 'privacy_policy';
		$this->load->view('admin/cms/post_article',$mode);
	}
	
	public function anti_spam_policy()
	{
		$mode['token']= 'Antispam Policy';
		$mode['type'] = 'anti_spam_policy';
		$this->load->view('admin/cms/post_article',$mode);
	}
	
	
	 public function process_article(){
	   
	   		$article_type		    = $this->input->post('article_type');
			$article_code    	    = $this->input->post('article_code');
			$article_postedby  		= $this->input->post('article_postedby');
			$article_headline       = $this->input->post('article_headline');
			$article_body           = $this->input->post('article_body');
			
			
			$rset=$this->cms_model->save_article($article_type,$article_code,$article_postedby,
			$article_headline,$article_body);
			
			if($rset)
			{
				redirect(base_url().'cms/articles', 'refresh');
				
				
			}else{
				echo 'failed';
			}
   }
   
	
}
