<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Campaigns extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		
	}
	
	public function index()
	{
		$this->load->view('admin/campaigns/regular_campaigns');
	}
	public function all()
	{
		$this->load->view('admin/campaigns/regular_campaigns');
	}
	public function scheduled()
	{
		$this->load->view('admin/campaigns/scheduled_campaigns');
	}
	public function by_member()
	{
		$this->load->view('admin/campaigns/campaigns_bymember');
	}
	
	public function load_campaign_headline(){
	           $campaign_code=$this->input->post('c_code');
			   $user_id = $this->input->post('uId');
			   $data=$this->sms_model->get_campaign_headline($campaign_code,$user_id);
			  
			   if($data!=0){
			   $dxc_code=$data[0]->campaign_code;
			   $dxc_message=$data[0]->message;
			   echo $dxc_code.'__'.$dxc_message;
			   }else{
				   echo 'failed';
			   }
	}
	public function delete_campaign(){
		$user_id = $this->input->post('uId');
		$campaign_code=mysql_real_escape_string($this->input->post('campaign_code'));
		
		$rset=$this->sms_model->delete_campaign($user_id,$campaign_code);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	
	
}
