<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Settings extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('countries_model');
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
	}
	   
	public function index()
	{
		$this->load->view('admin/settings/404');
	}
	
	public function app()
	{
		
		$this->load->view('settings/app_settings');
	}
	public function site()
	{
		$this->load->view('settings/site_settings');
	}
	public function payment()
	{
		$this->load->view('settings/payment_settings');
	}
	public function sms()
	{
		$this->load->view('settings/sms');
	}
	public function account_classes()
	{
		$this->load->view('settings/account_classes');
	}
	public function sms_prices()
	{
		$this->load->view('settings/sms_prices');
	}
	public function caller_numbers()
	{
		$this->load->view('settings/caller_numbers');
	}
	public function ad_feature(){
		$this->load->view('settings/ad_featured');
	}
	public function currency()
	{
		$this->load->view('settings/currency');
	}
	public function load_account_classes(){
		
		$result=$this->app_settings_model->get_account_classes();
		$json = json_encode($result);
		echo $json;
	}
	public function load_ad_features(){
		$result=$this->app_settings_model->get_ad_features();
		$json = json_encode($result);
		echo $json;
	}
	public function load_currency(){
		
		$result=$this->app_settings_model->get_currency_settings();
		$json = json_encode($result);
		echo $json;
	}
	public function load_sms_prices(){
		$result=$this->app_settings_model->get_sms_prices();
		$json = json_encode($result);
		echo $json;
	}
	
	public function load_caller_numbers(){
		$result=$this->app_settings_model->get_caller_numbers();
		if($result!=0){
			$json = json_encode($result);
			echo $json;
		}
	}
	
	public function email_setup()
	{
		
		$this->load->view('email_settings');
	}
	public function group_setup()
	{
		
		$this->load->view('group_settings');
	}
	
	public function process_app_settings(){
	 
	 
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  $app_timezone  =  mysql_real_escape_string($data->app_timezone);
	  $app_country   =  mysql_real_escape_string($data->app_country);
	  $app_currency  =  mysql_real_escape_string($data->app_currency);
	  $smtp_host     =  mysql_real_escape_string($data->smtp_host);
	  $smtp_username =  mysql_real_escape_string($data->smtp_username);
	  $smtp_password =  mysql_real_escape_string($data->smtp_password);
	  $smtp_port     =  mysql_real_escape_string($data->smtp_port);
	  $app_from_mail      =  mysql_real_escape_string($data->app_from_mail);
	  $app_mobile_number  = mysql_real_escape_string($data->app_mobile_number);
	  $app_default_name   = mysql_real_escape_string($data->app_default_name);
	  $app_support_number = mysql_real_escape_string($data->app_support_number);
	  $app_support_email  = mysql_real_escape_string($data->app_support_email);
	  
	  $app_upgrade_option  = mysql_real_escape_string($data->app_upgrade_option);
	  $app_helptext        = mysql_real_escape_string($data->app_helptext);
	  $app_branded_by      = mysql_real_escape_string($data->app_branded_by);
	  $app_copyrights_text = mysql_real_escape_string($data->app_copyrights_text);
	  $fb_link             = mysql_real_escape_string($data->fb_link);
	  $twitter_link        = mysql_real_escape_string($data->twitter_link);
	  $youtube_link        = mysql_real_escape_string($data->youtube_link);
	 
	 
	  $result=$this->app_settings_model->update_app_settings($app_timezone,$app_country,$app_currency,$smtp_host,$smtp_username,$smtp_password,
	  $smtp_port,$app_from_mail,$app_default_name,$app_mobile_number,$app_support_number,$app_support_email,$app_upgrade_option,
	  $app_helptext,$app_branded_by,$app_copyrights_text,$fb_link,$twitter_link,$youtube_link);
	  if($result)
	  {
	    echo _("success");
	  }else{
		echo _("failed");
	  }
	  
	}
	public function process_sms_settings(){
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  $sc_acc_id         =  mysql_real_escape_string($data->sc_acc_id);
	  $sc_auth_token     =  mysql_real_escape_string($data->sc_auth_token);
	  $sc_origin_number  =  mysql_real_escape_string($data->sc_origin_number);
	  $sc_sender_id      =  mysql_real_escape_string($data->sc_sender_id);
	  $sc_resend_time    =  mysql_real_escape_string($data->sc_resend_time);
	  
	  $campaign_minval   =  mysql_real_escape_string($data->campaign_minval);
	  $campaign_maxval   =  mysql_real_escape_string($data->campaign_maxval);
	  $service_recall    =  mysql_real_escape_string($data->service_recall);
	  
	  $credit_threshold_recall   =  mysql_real_escape_string($data->credit_threshold_recall);
	  $cmp_alerts                =  mysql_real_escape_string($data->cmp_alerts);
	  $cn_alerts                 =  mysql_real_escape_string($data->cn_alerts);
	  $sid_alerts                =  mysql_real_escape_string($data->sid_alerts);
	  $pay_alerts                =  mysql_real_escape_string($data->pay_alerts);
	  $sid_approval_alerts       =  mysql_real_escape_string($data->sid_approval_alerts);
	  $enable_alerts             =  mysql_real_escape_string($data->enable_alerts);
	  $blacklist_alerts          =  mysql_real_escape_string($data->blacklist_alerts);
	  
	 
	  $result=$this->app_settings_model->update_sms_settings($sc_acc_id,$sc_auth_token,$sc_origin_number,$sc_sender_id,$sc_resend_time,$campaign_minval,
	  $campaign_maxval,$service_recall,$credit_threshold_recall,$cmp_alerts,$cn_alerts,$sid_alerts,$pay_alerts,$sid_approval_alerts,$enable_alerts,$blacklist_alerts);
	  if($result)
	  {
	    echo _("success");
	  }else{
		echo _("failed");
	  }
	}
	public function process_caller_number(){
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  $hidId        =  mysql_real_escape_string($data->hidId);
	  
	  $cn_country    =  mysql_real_escape_string($data->cn_country);
	  $cn_currency_code      =  mysql_real_escape_string($data->cn_currency_code);
	  $cn_price    =  mysql_real_escape_string($data->cn_price);
	  $cn_type      =  mysql_real_escape_string($data->cn_type);
	  $cn_desc      =  mysql_real_escape_string($data->cn_desc);
	  $cn_period   = mysql_real_escape_string($data->cn_period);
	  if($hidId!=''){$cn_id=$hidId;}else{$cn_id=0;}
	  
	  $result=$this->app_settings_model->update_caller_number($cn_id,$cn_country,$cn_currency_code,$cn_price,$cn_type,$cn_desc,$cn_period);
	  
	  
	  if($result)
	  {
	    echo _("success");
		
	  }else{
		echo _("failed");
	  }
	}
	public function process_acclass_settings(){
	 
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  $ac_itemcode  =  mysql_real_escape_string($data->ac_itemcode);
	  $ac_name   =  mysql_real_escape_string($data->ac_name);
	  $ac_cost  =  mysql_real_escape_string($data->ac_cost);
	  $ac_status     =  mysql_real_escape_string($data->ac_status);
	  $ac_desc =  mysql_real_escape_string($data->ac_desc);
	 
	 
	  $result=$this->app_settings_model->update_acclass_settings($ac_itemcode,$ac_name,$ac_cost,$ac_status,$ac_desc);
	  if($result)
	  {
	    echo _("success");
		//redirect(base_url().'settings/account_classes', 'refresh'); 
	  }else{
		echo _("failed");
	  }
	}
	
	public function process_ad_feature(){
	  
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  $af_code      =  mysql_real_escape_string($data->af_code);
	  $af_name      =  mysql_real_escape_string($data->af_name);
	  $price_tag    =  mysql_real_escape_string($data->price_tag);
	  $af_mapping   =  mysql_real_escape_string($data->af_mapping);
	  $af_status    =  mysql_real_escape_string($data->af_status);
	  $af_desc      =  mysql_real_escape_string($data->af_desc);
	  
	  
	  $result=$this->app_settings_model->update_ad_features($af_code,$af_name,$price_tag,$af_mapping,$af_status,$af_desc);
	  if($result)
	  {
	    echo _("success");
		//redirect(base_url().'settings/account_classes', 'refresh'); 
	  }else{
		echo _("failed");
	  }
	}
	
	public function process_currency_settings(){
	
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  $hidId        =  mysql_real_escape_string($data->hidId);
	  $c_country    =  mysql_real_escape_string($data->c_country);
	  $cr_name      =  mysql_real_escape_string($data->cr_name);
	  $cr_symbol    =  mysql_real_escape_string($data->cr_symbol);
	  $cr_desc      =  mysql_real_escape_string($data->cr_desc);
	 
	  if($hidId!=''){$currency_id=$hidId;}else{$currency_id=0;}
	  
	  $result=$this->app_settings_model->update_currency_settings($currency_id,$c_country,$cr_name,$cr_symbol,$cr_desc);
	  if($result)
	  {
	    echo _("success");
		//redirect(base_url().'settings/account_classes', 'refresh'); 
	  }else{
		echo _("failed");
	  }
	}
	public function process_sms_prices(){
	
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  $hidId        =  mysql_real_escape_string($data->hidId);
	  $sp_country    =  mysql_real_escape_string($data->sp_country);
	  $sp_currency_code      =  mysql_real_escape_string($data->sp_currency_code);
	  $sp_price    =  mysql_real_escape_string($data->sp_price);
	  $sp_desc      =  mysql_real_escape_string($data->sp_desc);
	 
	  if($hidId!=''){$price_id=$hidId;}else{$price_id=0;}
	  
	  $result=$this->app_settings_model->update_sms_prices($price_id,$sp_country,$sp_currency_code,$sp_price,$sp_desc);
	  if($result)
	  {
	    echo _("success");
		//redirect(base_url().'settings/account_classes', 'refresh'); 
	  }else{
		echo _("failed");
	  }
	}
	public function delete_acclass_process(){
		
		$acc_id = mysql_real_escape_string($this->input->post('acc_id'));
		$result = $this->app_settings_model->delete_accclass($acc_id);
		if($result){
			
			   echo _("success");
		}else{
			    echo _("failed");
		}
	}
	public function delete_currency_process(){
		
		$currency_id = mysql_real_escape_string($this->input->post('c_id'));
		$result = $this->app_settings_model->delete_currency_code($currency_id);
		if($result){
			
			   echo _("success");
		}else{
			    echo _("failed");
		}
	}
	public function delete_sms_prices(){
		
		$p_id = mysql_real_escape_string($this->input->post('p_id'));
		$result = $this->app_settings_model->delete_sms_prices($p_id);
		if($result){
			
			   echo _("success");
		}else{
			    echo _("failed");
		}
	}
	public function delete_caller_number(){
		
		$cn_id = mysql_real_escape_string($this->input->post('cn_id'));
		$result = $this->app_settings_model->delete_caller_number($cn_id);
		if($result){
			
			   echo _("success");
		}else{
			    echo _("failed");
		}
	}
	public function process_site_settings(){
	 
	
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  $allow_card        =  mysql_real_escape_string($data->allow_card);
	  $allow_paypal      =  mysql_real_escape_string($data->allow_paypal);
	  $bid_expire_days   =  mysql_real_escape_string($data->bid_expire_days);
	  $image_uploads     =  mysql_real_escape_string($data->image_uploads);
	  $sms_panel         =  mysql_real_escape_string($data->sms_panel);
	  $email_panel       =  mysql_real_escape_string($data->email_panel);
	  $payad_expire_days =  mysql_real_escape_string($data->payad_expire_days);
	  
	  $vcode_min_val    =  mysql_real_escape_string($data->vcode_min_val);
	  $vcode_max_val    =  mysql_real_escape_string($data->vcode_max_val);
	  $vcode_string     =  mysql_real_escape_string($data->vcode_string);
	  
	  $wallet_min_val    =  mysql_real_escape_string($data->wallet_min_val);
	  $wallet_max_val    =  mysql_real_escape_string($data->wallet_max_val);
	  $wallet_string     =  mysql_real_escape_string($data->wallet_string);
	  
	  $individual_ad_limit    =  mysql_real_escape_string($data->iAdLimit);
	  $business_ad_limit    =  mysql_real_escape_string($data->bAdLimit);
	  $site_business_image_uploads     =  mysql_real_escape_string($data->sbImageUploads);
	  $site_wallet     =  mysql_real_escape_string($data->site_wallet);
	  $user_keygen     =  mysql_real_escape_string($data->user_keygen);
	  $bid_expired_time     =  mysql_real_escape_string($data->bid_refresh_time);
	  
	  
	 
	  $result=$this->app_settings_model->update_site_settings($allow_card,$allow_paypal,$bid_expire_days,$image_uploads,$sms_panel,$email_panel,
	  $payad_expire_days,$vcode_min_val,$vcode_max_val,$vcode_string,$individual_ad_limit,$business_ad_limit,
	  $site_business_image_uploads,$site_wallet,$wallet_min_val,$wallet_max_val,$wallet_string,$user_keygen,$bid_expired_time);
	  if($result)
	  {
	    echo _("success");
	  }else{
		echo _("failed");
	  }
	  
	}
	
	public function process_payment_settings(){
	 
	 
	  $data         = json_decode(file_get_contents("php://input"));
	 
	  ///// paypal params
	  $pp_email      =  mysql_real_escape_string($data->pp_email);
	  $pp_return_url   =  mysql_real_escape_string($data->pp_return_url);
	  $pp_cancel_url  =  mysql_real_escape_string($data->pp_cancel_url);
	  $pp_notify_url     =  mysql_real_escape_string($data->pp_notify_url);
	  $pp_payment_url     =  mysql_real_escape_string($data->pp_payment_url);
	  
	  $pp_wallet_email  =  mysql_real_escape_string($data->pp_wallet_email);
	  $pp_wallet_return_url   =  mysql_real_escape_string($data->pp_wallet_return_url);
	  $pp_wallet_cancel_url  =  mysql_real_escape_string($data->pp_wallet_cancel_url);
	  $pp_wallet_notify_url     =  mysql_real_escape_string($data->pp_wallet_notify_url);
	  
	  //////2co params
	  $co_seller_id  =  mysql_real_escape_string($data->co_seller_id);
	  $co_base_url   =  mysql_real_escape_string($data->co_base_url);
	  $co_sandbox_url  =  mysql_real_escape_string($data->co_sandbox_url);
	  $co_private_key     =  mysql_real_escape_string($data->co_private_key);
	  
	  $co_publish_key  =  mysql_real_escape_string($data->co_publish_key);
	  $co_username   =  mysql_real_escape_string($data->co_username);
	  $co_password  =  mysql_real_escape_string($data->co_password);
	  $is_sandbox     =  mysql_real_escape_string($data->is_sandbox);
	  
	  $result=$this->app_settings_model->update_payment_settings($pp_email,$pp_return_url,$pp_cancel_url,
	  $pp_notify_url,$pp_payment_url,$pp_wallet_email,$pp_wallet_return_url,$pp_wallet_cancel_url,$pp_wallet_notify_url,$co_seller_id,
	  $co_base_url,$co_sandbox_url,$co_private_key,$co_publish_key,$co_username,$co_password,$is_sandbox);
	  if($result)
	  {
	    echo _("success");
	  }else{
		echo _("failed");
	  }
	  
	}
	
	public function ftp_process()
	{
		
		$data         = json_decode(file_get_contents("php://input"));
		$ftp_host     = mysql_real_escape_string($data->ftp_host);
		$ftp_username = mysql_real_escape_string($data->ftp_username);
		$ftp_password = mysql_real_escape_string($data->ftp_password);
		$ftp_port     = mysql_real_escape_string($data->ftp_port);
		$api_type     = "ftp";
		
		$result = $this->app_settings_model->add_ftp_settings($ftp_host, $ftp_username,$ftp_password,$ftp_port,$api_type);
		
		if($result){
			
			    $arr = array('msg' => "success", 'error' => '');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}else{
			    $arr = array('msg' => "", 'error' => 'OOPS request failed ..!');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}
	}
	
	public function email_process()
	{
		
		$data         = json_decode(file_get_contents("php://input"));
		$email_host     = mysql_real_escape_string($data->email_host);
		$email_username = mysql_real_escape_string($data->email_username);
		$email_password = mysql_real_escape_string($data->email_password);
		$email_port     = mysql_real_escape_string($data->email_port);
		$api_type     = "email";
		
		$result = $this->app_settings_model->add_ftp_settings($email_host, $email_username,$email_password,$email_port,$api_type);
		
		if($result){
			
			    $arr = array('msg' => "success", 'error' => '');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}else{
			    $arr = array('msg' => "", 'error' => 'OOPS request failed ..!');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}
	}
	
	public function ftp_update_process()
	{
		$data         = json_decode(file_get_contents("php://input"));
		$ftp_host     = mysql_real_escape_string($data->ftp_host);
		$ftp_username = mysql_real_escape_string($data->ftp_username);
		$ftp_password = mysql_real_escape_string($data->ftp_password);
		$ftp_port     = mysql_real_escape_string($data->ftp_port);
		$api_id       = mysql_real_escape_string($data->ftp_id);;
		
		$result = $this->app_settings_model->update_ftp_settings($ftp_host, $ftp_username,$ftp_password,$ftp_port,$api_id);
		
		if($result){
			
			    $arr = array('msg' => "success", 'error' => '');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}else{
			    $arr = array('msg' => "", 'error' => 'OOPS request failed ..!');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}
	}
	
	
	public function email_update_process()
	{
		$data         = json_decode(file_get_contents("php://input"));
		$email_host     = mysql_real_escape_string($data->email_host);
		$email_username = mysql_real_escape_string($data->email_username);
		$email_password = mysql_real_escape_string($data->email_password);
		$email_port     = mysql_real_escape_string($data->email_port);
		$api_id       = mysql_real_escape_string($data->email_setup_id);;
		
		$result = $this->app_settings_model->update_email_settings($email_host, $email_username,$email_password,$email_port,$api_id);
		
		if($result){
			
			    $arr = array('msg' => "success", 'error' => '');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}else{
			    $arr = array('msg' => "", 'error' => 'OOPS request failed ..!');
				$jsn = json_encode($arr);
				print_r($jsn);
				
		}
	}
}
