<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Export extends CI_Controller {
     
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/twilio_model','',TRUE);
		$this->load->model('notification_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		
	}
	
	public function export_contacts(){
		
		$this->load->view('export/sms/export_contacts');
	}
	public function export_campaigns(){
		
		$this->load->view('export/sms/export_campaigns');
	}
	public function export_groups(){
		
		$this->load->view('export/sms/export_groups');
	}
	
	public function export_template(){
		
		$this->load->view('export/sms/export_template');
	}
	
	public function export_schedules(){
		
		$this->load->view('export/sms/export_schedules');
	}
	
	public function export_notifications(){
		
		$this->load->view('export/sms/export_notifications');
	}
	
	public function export_caller_numbers(){
		
		$this->load->view('export/sms/export_caller_numbers');
	}
	public function export_pricing(){
		$this->load->view('export/sms/export_pricing');
	}
	public function export_history(){
		$this->load->view('export/sms/export_history');
	}
	
	public function export_success_history(){
		$this->load->view('export/sms/export_success_history');
	}
	
	public function export_failed_history(){
		$this->load->view('export/sms/export_failed_history');
	}
	
	public function invoice(){
		
		$this->load->library('m_pdf');
		$this->load->view('export/invoice/export_invoice');
		
		
	}
	public function payment_invoice(){
		$this->load->library('m_pdf');
		$this->load->view('export/invoice/export_payment_invoice');
	}
	
	public function export_payment_history(){
		$this->load->view('export/payments/export_payment_history');
	}
	
	public function export_wallet_history(){
		$this->load->view('export/wallet/export_wallet_history');
	}
	
	
}
