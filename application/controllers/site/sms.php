<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Sms extends CI_Controller {
     
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('notification_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('site/twilio_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
	}
	
	public function index()
	{
		
		/// not found
	}
	public function campaign(){
		
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->view('site/campaign/sms');
	}
	public function load_sms_prices(){
		
		$result=$this->app_settings_model->get_sms_prices();
		$json = json_encode($result);
		echo $json;
	}
	public function load_groups(){
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$result=$this->sms_model->get_groups_byuser($user_id);
		if($result!=0){
		$json = json_encode($result);
		echo $json;
		}
	}
	public function load_contacts(){
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$result=$this->sms_model->get_contacts_byuser($user_id);
		if($result!=0){
		$json = json_encode($result);
		echo $json;
		}
	}
	public function load_group_contacts(){
		$data = json_decode(file_get_contents("php://input"));
		$group_id=mysql_real_escape_string($data->group_id);
		$user_id=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->get_group_contacts($group_id,$user_id);
		
		if($rset!=''){
			
			if(count($rset)<10){
				echo " <a href='#'>(0".count($rset).")</a> ";
			}else{
				echo " <a href='#'>(".count($rset).")</a> ";
			}
		}
	}
	public function check_caller_id(){
		
		$ci_key=$this->input->post('key');
		$result=$this->sms_model->check_caller_id($ci_key);
		
		if($result){
			echo "<span class='text-success'><i class='fa fa-check'></i> <b>".$ci_key."</b> " . _('is available.') . "</span>";
			
		}else{
			echo "<span class='text-danger'><i class='fa fa-times'></i> <b>".$ci_key."</b> " . _('is not available.') . "</span>";
		}
	}
	public function process_cc_single(){
		$data = json_decode(file_get_contents("php://input"));
		$contact_id=mysql_real_escape_string($data->contact_id);
		$group_id=mysql_real_escape_string($data->to_group);
		$c_mobile=mysql_real_escape_string($data->c_mobile);
		$c_name=mysql_real_escape_string($data->c_name);
		$c_email=mysql_real_escape_string($data->c_email);
		$country_code=mysql_real_escape_string($data->country_code);
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->copy_single_contact($group_id,$user_id,$contact_id,$c_mobile,$c_name,$c_email,$country_code);
		
		if($rset!=''){
			echo 'success';
			
		}else{
			echo 'failed';
		}
	}
	
	public function process_cpselected_contacts(){
		$contact_ids=mysql_real_escape_string($this->input->post('cIds'));
		$c_group=mysql_real_escape_string($this->input->post('groupId'));
		$user_id=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->copy_selected_contacts($contact_ids,$user_id,$c_group);
		
		if($rset!=0){
			echo count($rset);
			
		}else{
			echo 'failed';
		}
	}
	public function process_mvselected_contacts(){
		$contact_ids=mysql_real_escape_string($this->input->post('cIds'));
		$c_group=mysql_real_escape_string($this->input->post('groupId'));
		$user_id=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->move_selected_contacts($contact_ids,$user_id,$c_group);
		
		if($rset!=0){
			echo count($rset);
			
		}else{
			echo 'failed';
		}
	}
	public function process_delselected_contacts(){
		$contact_ids=mysql_real_escape_string($this->input->post('cIds'));
		$user_id=$this->session->userdata['site_login']['user_id'];
		
		$rset=$this->sms_model->delete_selected_contacts($contact_ids,$user_id);
		
		if($rset!=0){
			echo 'success';
			
		}else{
			echo 'failed';
		}
	}
	public function process_mv_single(){
		$data = json_decode(file_get_contents("php://input"));
		$contact_id=mysql_real_escape_string($data->contact_id);
		$group_id=mysql_real_escape_string($data->to_group);
		$c_mobile=mysql_real_escape_string($data->c_mobile);
		$c_name=mysql_real_escape_string($data->c_name);
		$c_email=mysql_real_escape_string($data->c_email);
		$country_code=mysql_real_escape_string($data->country_code);
		
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->move_single_contact($group_id,$user_id,$contact_id,$c_mobile);
		
		if($rset!=''){
			echo 'success';
			
		}else{
			echo 'failed';
		}
	}
	public function add_template_title(){
		$data           = json_decode(file_get_contents("php://input"));
		$template_code  = mysql_real_escape_string($data->template_code);
		$template_title = mysql_real_escape_string($data->template_title);
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->add_template_title($template_code,$template_title,$user_id);
		
		if($rset!=''){
			echo 'success';
			
		}else{
			echo 'failed';
		}
	}
	public function load_group_name(){
		$data = json_decode(file_get_contents("php://input"));
		$group_id=mysql_real_escape_string($data->group_id);
		$user_id=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->get_group_name($group_id,$user_id);
		
		if(@$rset!=''){
			echo @$rset[0]->group_name;
		}else{
			echo _("Not Assigned");
		}
	}
	public function campaign_history_grpname(){
		$data = json_decode(file_get_contents("php://input"));
		$group_id=mysql_real_escape_string($data->group_id);
		$user_id=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->get_group_name($group_id,$user_id);
		
		if(@$rset!=''){
			echo @$rset[0]->group_name;
		}else{
			echo _("Not Assigned");
		}
	}
	public function load_caller_numbers(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$result=$this->sms_model->get_caller_numbers($user_id);
		if($result!=0){
		$json = json_encode($result);
		echo $json;
		}
	}
	public function load_campaign_history(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$result=$this->sms_model->get_campaign_history($user_id);
		if($result!=0){
			$json = json_encode($result);
			echo $json;
		}
	}
	public function load_my_campaigns(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$result=$this->sms_model->get_campaign_overview($user_id);
		if($result!=0){
			$json = json_encode($result);
			echo $json;
		}
	}
	public function load_my_schedules(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$result=$this->sms_model->get_campaign_schedules($user_id);
		if($result!=0){
			$json = json_encode($result);
			echo $json;
		}
	}
	public function load_template_titles(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$result=$this->sms_model->get_template_titles($user_id);
		if($result!=0){
			$json = json_encode($result);
			echo $json;
		}
	}
	public function get_projected_price(){
		$group_id=mysql_real_escape_string($this->input->post('gid'));
		$campaign_mode=mysql_real_escape_string($this->input->post('mode'));
		if($campaign_mode!=''){
		switch($campaign_mode){
		
		 case 'group':
				if($group_id!=''){
					$exp_grp=explode(',',substr($group_id,0,-1));
					//1_ggg
					$arr=array();
				    
					if($exp_grp[0]!=''){
						for($i=0;$i<count($exp_grp);$i++){
							//$exp_inner=explode('_',$exp_grp[$i]);
							//$grp_id=$exp_inner[0];
							$grp_id=$exp_grp[$i];
							
							$result=$this->sms_model->get_projected_price($grp_id);
							if($result!=''){
								for($j=0;$j<count($result);$j++){
									array_push($arr,$result[$j]->price);
								}
							}
							
						}
						
						echo array_sum($arr);
					
					}
				}else{
					echo "00.00";
				}
			break;
			
			case 'contact':
				if($group_id!=''){
					$exp_contacts=explode(',',$group_id);
					
					$arr=array();
					if($exp_contacts[0]!=''){
						for($i=0;$i<count($exp_contacts);$i++){
							$contact_id=$exp_contacts[$i];
							
							$result=$this->sms_model->get_projected_price_bycontact($contact_id);
							if($result!=''){
								for($j=0;$j<count($result);$j++){
									array_push($arr,$result[$j]->price);
								}
							}
							
						}
						echo array_sum($arr);
					
					}
				}else{
					echo "00.00";
				}
			break;
		}
		}else{
			echo "<span class='text-danger'>" . _('Campaign mode tracking error.') . "</span>";
		}
	}
	public function add_sms_contact(){
		$data = json_decode(file_get_contents("php://input"));
		$full_name=mysql_real_escape_string($data->full_name);
		$mobile=mysql_real_escape_string($data->mobile);
		$caller_code=mysql_real_escape_string($data->caller_code);
		$email=mysql_real_escape_string($data->email);
		$country=mysql_real_escape_string($data->country);
		$group=mysql_real_escape_string($data->group);
		$userId=$this->session->userdata['site_login']['user_id'];
		
		$concat_mobile=substr($caller_code,0);
		if($concat_mobile!='+'){
			$calling_code='+'.$caller_code;
		}else{
			$calling_code=$caller_code;
		}
		$mobile_number=$calling_code.$mobile;
		$check=$this->sms_model->check_sms_contact($country,$userId,$group,$mobile_number);
		
		if($check==0){
			$rset=$this->sms_model->add_sms_contact($userId,$full_name,$mobile_number,$email,$country,$group);
			if($rset){
				echo _("success");
			}else{
				echo _("failed");
			}
		}else{
			echo _("duplicated");
		}
	}
	public function edit_contact(){
		//$data = json_decode(file_get_contents("php://input"));
		$full_name=mysql_real_escape_string($this->input->post('full_name'));
		$calling_code=mysql_real_escape_string($this->input->post('calling_code'));
		$mobile=mysql_real_escape_string($this->input->post('mobile'));
		$email=mysql_real_escape_string($this->input->post('email'));
		$country=mysql_real_escape_string($this->input->post('country'));
		$group=mysql_real_escape_string($this->input->post('group'));
		$contact_id=mysql_real_escape_string($this->input->post('cid'));
		$user_id=$this->session->userdata['site_login']['user_id'];
		
		$concat_mobile=substr($calling_code,0);
		if($concat_mobile!='+'){
			$caller_code='+'.$calling_code;
		}else{
			$caller_code=$calling_code;
		}
		$mobile_number=$caller_code.$mobile;
		
		
		$rset=$this->sms_model->update_sms_contact($user_id,$full_name,$mobile_number,$email,$country,$group,$contact_id);
		if($rset){
			//redirect(base_url().'site/sms/campaign#contacts',true);
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function edit_smsgroup(){
		//$data = json_decode(file_get_contents("php://input"));
		
		$group_name=mysql_real_escape_string($this->input->post('cg_groupname'));
		$group_id=mysql_real_escape_string($this->input->post('cgrp_id'));
		$user_id=$this->session->userdata['site_login']['user_id'];
		
		
		$rset=$this->sms_model->update_smsgroup($user_id,$group_id,$group_name);
		if($rset){
			
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function edit_ttitle(){
		$t_title=mysql_real_escape_string($this->input->post('t_title'));
		$t_code=mysql_real_escape_string($this->input->post('t_code'));
		$user_id=$this->session->userdata['site_login']['user_id'];
		
		
		$rset=$this->sms_model->update_ttitle($user_id,$t_code,$t_title);
		if($rset){
			
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function process_sender_id(){
		$data = json_decode(file_get_contents("php://input"));
		$sender_number=mysql_real_escape_string($data->sender_number);
		$sender_name=mysql_real_escape_string($data->sender_name);
		$tid=mysql_real_escape_string($data->tid);
		$userId=$this->session->userdata['site_login']['user_id'];
		
		//// prepare sms alert to admin
		$alert_first_name   = $this->session->userdata['site_login']['ac_first_name'];
		$alert_last_name    = $this->session->userdata['site_login']['ac_last_name'];
		$alert_country      = $this->session->userdata['site_login']['ac_country'];
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appAlertMobile     = $alertAppSettings[0]->default_mobile_number;
		$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
		$account_sid        = $alertSMSSettings[0]->twilio_accid;
		$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
		$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
		$enable_alerts      = $alertSMSSettings[0]->enable_alerts;
		
		
		$sid_masking_token   = $alertSMSSettings[0]->sid_alert_token;
		if($sid_masking_token!=''){
			$alertMasking       = $alertSMSSettings[0]->sid_alert_token;
		}else{
			//$alertMasking       = "SIDAlerts";
			if($enable_alerts==1){
			    $alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
			}else{
				$alertMasking       = '';	
			}
			
		}
		
		$alertContent       = "Hello you received a SenderId request by: ".$alert_first_name." ".$alert_last_name.", Requested SId: ".$sender_name.", Country: ".$alert_country; 
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
		
		
		$rset=$this->sms_model->process_sender_id($userId,$sender_number,$sender_name,$tid);
		if($rset){
			echo _("success");
			if($alertMasking!=''){
				//// trigger sms alert to admin 
				$status=$client->account->messages->create(array( 
					'To' => $appAlertMobile, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
				));
			}
		}else{
			echo _("failed");
		}
	}
	public function call_sms_service(){
		
		$token=mysql_real_escape_string($this->input->post('token'));
		$userId=$this->session->userdata['site_login']['user_id'];
		$rset=$this->sms_model->start_sms_service($userId,$token);
		
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function trigger_sms_campaign(){
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$SmsSiteSettings=$this->app_settings_model->get_sms_member_settings($user_id);
		$appSettings=$this->app_settings_model->get_app_settings();
		$smsSettings=$this->app_settings_model->get_sms_settings();
		
		$account_sid = $smsSettings[0]->twilio_accid;
		//Put your Twilio Auth Key here:
		$auth_token = $smsSettings[0]->twilio_authtoken;
		// Put your Twilio Outgoing SMS enabled number here:
		
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
        $waitingList=$this->sms_model->get_campaign_waiting($user_id);
		
		
		$authMethod = 'sms';
		if($waitingList!=0){
		
        foreach($waitingList as $campaign) {
		$campaign_id= $campaign->campaign_id;	
		$content = $campaign->message;
        $toNumber = $campaign->to_number; 
		$outboundNumber = $campaign->from_number; 
		// If the delivery method is via SMS, make the API call to Twilio sending the message using the supplied credentials. 
		
			
					// Checking member purchased number/sender id
					/*if(@$SmsSiteSettings[0]->twilio_sender_id=='' && @$SmsSiteSettings[0]->twilio_origin_number!=''){
						$senderId=$SmsSiteSettings[0]->twilio_origin_number;
						
					}elseif(@$SmsSiteSettings[0]->twilio_sender_id!='' && @$SmsSiteSettings[0]->twilio_origin_number!=''){
						$senderId=$SmsSiteSettings[0]->twilio_sender_id;
						
					}elseif(@$SmsSiteSettings[0]->twilio_sender_id=='' && @$SmsSiteSettings[0]->twilio_origin_number==''){
						$senderId=$smsSettings[0]->twilio_sender_id;
						
					}
					
					// Propagate app default name as sender id if empty from sms settings
					if($senderId!=''){
						echo $outboundNumber=$senderId;
					}else{
						$outboundNumber = $appSettings[0]->app_default_name; 
					}*/
				
				//// checking number valid/invalid 				
				/*$number = $client->account->incoming_phone_numbers->create(array(
				"PhoneNumber" => @$outboundNumber 
				));
				echo $number->sid;
				echo $number->code;
				*/
				
				
				
				try 
		        {
					
					$status=$client->account->messages->create(array( 
						'To' => $toNumber, 
						'From' => $outboundNumber, 
						'Body' => $content,   
					));
				   
				   $fCall=$status->status;
				   $messageToken=$status->sid;
				   //$final_status=$client->account->messages->get($messageToken);
				   //echo $final_status->status;
				   
				   if($fCall!='queued'){
					   $remarks='message failed';
					   $deliver_status=0;
					   $message_id=$messageToken;
					   $upResponse=$this->sms_model->update_sms_campaign_status($user_id,$campaign_id,$remarks,$message_id);
				   }else{
					   $remarks='message in queued';
					   $deliver_status=3;
					   $message_id=$messageToken;
					   $upResponse=$this->sms_model->update_sms_campaign_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status);
				   }
				}catch (Services_Twilio_RestException $e){
					   echo $e->getMessage();
					   $remarks= $e->getMessage();
					   $deliver_status=0;
					   $message_id='';
					   $upResponse=$this->sms_model->update_sms_campaign_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status);
				}

        }  
		}else{
			echo _("Empty set");
		}
		
	}
	
	public function grab_smsfinal_status(){
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$cost_arr=array();
		$parse_campaign_code="";
		// getting wallet credit
		$walletInfo= $this->account_model->get_account_wallet($user_id);
		if(@$walletInfo[0]->balance_amount!=''){
			$accountCredit=$walletInfo[0]->balance_amount;
		}else{
			$accountCredit=0;
		}
	
		/// getting queued sms
		
		$smsSettings=$this->app_settings_model->get_sms_settings();
		$account_sid = $smsSettings[0]->twilio_accid;
		$auth_token = $smsSettings[0]->twilio_authtoken;
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		
		$client = new Services_Twilio($account_sid, $auth_token); 
		
        //echo $final_status->status;
		$queuedList=$this->sms_model->get_queued_sms($user_id);
		if($queuedList!=0){
		foreach($queuedList as $message){
			$message_id=$message->message_id;
			$campaign_id=$message->campaign_id;
			$unit_cost=$message->unit_cost;
			array_push($cost_arr,$unit_cost);
			$parse_campaign_code='Charges for campaign code :'.$message->campaign_code;
			$final_status=$client->account->messages->get($message_id);
			//echo $final_status->status;
			$finalCall=$final_status->status;
			//$attempt_n=$final_status->attempt_n;
			switch($finalCall){
				case 'failed':
				$deliver_status=0;
			    $remarks='message final status failed';
				$upResponse=$this->sms_model->update_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit);
				break;
				
				case 'delivered':
				$deliver_status=1;
			    $remarks='message has been sent';
				$upResponse=$this->sms_model->update_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit);
				break;
				
				case 'sent':
				$deliver_status=1;
			    $remarks='message has been sent';
				$upResponse=$this->sms_model->update_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit);
				break;
				
				default:
				$upResponse=$this->sms_model->update_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit);
			}
			
		}
		 if(array_sum($cost_arr)>0){
			/// generate wallet history invoice
			$totalPayment=array_sum($cost_arr);
			$settingsInfo= $this->app_settings_model->get_site_settings();
			$wallet_min_val=@$settingsInfo[0]->wallet_min_val;
			$wallet_max_val=@$settingsInfo[0]->wallet_max_val;
			
			 if($wallet_min_val!='0' && $wallet_max_val!='0'){
				 $camp_reference="OCWLSC".rand($wallet_min_val,$wallet_max_val);
			 }else{
				 $camp_reference='OCWLSC'.rand(600,6000);
			 }
			$ad_reference='OCSMSCMP';
			$ad_payment=$totalPayment;
			$payment_method='wallet';
			$payment_reference=$camp_reference;
			$payment_remarks=$parse_campaign_code;
			$campaign_code=$message->campaign_code;
			$payment_status='1';
			 
			 $invRset=$this->sms_model->campaign_wallet_invoice($ad_reference,$ad_payment,$payment_method,$payment_reference,
			 $payment_remarks,$payment_status,$user_id,$campaign_code);
			 if($invRset){
				 echo _("Wallet invoice generated.");
			 }
			 
			 
			 
		 }
		}else{
			echo _("Empty set in final call");
			/// Stop services if empty queued list 
			$token='stop';
		    $userId=$this->session->userdata['site_login']['user_id'];
		    $rset=$this->sms_model->start_sms_service($userId,$token);
			
		}
		
	}
	public function buy_caller_number(){
		$data = json_decode(file_get_contents("php://input"));
		$cn_type=mysql_real_escape_string($data->cn_type);
		$cn_country=mysql_real_escape_string($data->cn_country);
		$cn_period=mysql_real_escape_string($data->cn_lease_period);
		$country=mysql_real_escape_string($data->hid_cnprice);
		$cn_price=mysql_real_escape_string($data->total_cn_price);
		$walletBalance=mysql_real_escape_string($data->hid_wallet_credit);
		$cnp_reference=mysql_real_escape_string($data->hid_wallet_reference);
		$cn_remarks ='Caller number purchase';
		$payment_method='wallet';
		
		$expCountry=explode('-',$cn_country);
		$user_id=$this->session->userdata['site_login']['user_id'];
		
		//// prepare sms alert to admin
		$alert_first_name   = $this->session->userdata['site_login']['ac_first_name'];
		$alert_last_name    = $this->session->userdata['site_login']['ac_last_name'];
		$alert_country      = $this->session->userdata['site_login']['ac_country'];
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appAlertMobile     = $alertAppSettings[0]->default_mobile_number;
		$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
		$account_sid        = $alertSMSSettings[0]->twilio_accid;
		$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
		$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
		$enable_alerts      = $alertSMSSettings[0]->enable_alerts;
		
		$cn_masking_token   = $alertSMSSettings[0]->cn_alert_token;
		if($cn_masking_token!=''){
			$alertMasking       = $alertSMSSettings[0]->cn_alert_token;
		}else{
			//$alertMasking       = "CN Alerts";
			if($enable_alerts==1){
			    $alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
			}else{
				$alertMasking       = '';	
			}
		}
		
		$alertContent       = "Hello you received a caller number request by: ".$alert_first_name." ".$alert_last_name.", Country: ".$alert_country; 
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
		
		$rset=$this->sms_model->buy_caller_number($cn_type,$cnp_reference,$cn_price,$cn_period,$cn_remarks,$payment_method,$expCountry[0],$expCountry[3],$walletBalance,$user_id);
		if($rset){
			echo _("success");
			if($alertMasking!=''){
				//// trigger sms alert to admin 
				$status=$client->account->messages->create(array( 
					'To' => $appAlertMobile, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
				));
			}
		}else{
			echo _("failed");
		}
	}
	
	
	
	public function process_sms_campaign(){
		$data = json_decode(file_get_contents("php://input"));
		$user_id       = $this->session->userdata['site_login']['user_id'];
		
		$to_groups     = mysql_real_escape_string($data->to_tokens);  // for contacts, group assigned same variable
		$from_number   = mysql_real_escape_string($data->from_token);
		$message       = mysql_real_escape_string($data->message);
		$chksid        = mysql_real_escape_string($data->chksid);
		$camp_code     = mysql_real_escape_string($data->camp_code);
		$campaign_mode = mysql_real_escape_string($data->mode);
		$schedule_time = mysql_real_escape_string($data->schedule_time);
		$camp_timezone = mysql_real_escape_string($data->camp_timezone);
		$mcn_contacts  = mysql_real_escape_string($data->mcn_list);
		
		//// prepare sms alert to admin
		$alert_first_name   = $this->session->userdata['site_login']['ac_first_name'];
		$alert_last_name    = $this->session->userdata['site_login']['ac_last_name'];
		$alert_country      = $this->session->userdata['site_login']['ac_country'];
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appAlertMobile     = $alertAppSettings[0]->default_mobile_number;
		$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
		$account_sid        = $alertSMSSettings[0]->twilio_accid;
		$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
		$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
		$enable_alerts      = $alertSMSSettings[0]->enable_alerts;
		
		
		$cmp_masking_token   = $alertSMSSettings[0]->cmp_alert_token;
		if($cmp_masking_token!=''){
			$alertMasking       = $alertSMSSettings[0]->cmp_alert_token;
		}else{
			//$alertMasking       = "CampAlerts";
			if($enable_alerts==1){
			    $alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
			}else{
				$alertMasking       = '';	
			}
			
		}
		
		$alertContent       = "Hello you received new campaign by: ".$alert_first_name." ".$alert_last_name.", Campaign code: ".$camp_code.", Country: ".$alert_country; 
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
		
		
		if($schedule_time!=''){
			$fScheduledTime=$schedule_time;
			$fCampTimeZone=$camp_timezone;
			$isScheduled=1;
		}else{
			$fScheduledTime='0000-00-00 00:00:00';
			$fCampTimeZone='';
			$isScheduled=0;
		}
		
		
		$smsMemberSettings=$this->app_settings_model->get_member_campaign_settings($user_id,$from_number);
		if($from_number==1){
			$smsSettings=$this->app_settings_model->get_sms_settings();
			$fromNumber=$smsSettings[0]->twilio_origin_number;
		}else{
			if($chksid==1){
				//// checking sender id status
				if($smsMemberSettings[0]->twilio_sender_id!='' && $smsMemberSettings[0]->sender_id_status =='1')
				{
					$fromNumber=$smsMemberSettings[0]->twilio_sender_id;
					
				}else{
					echo 'failed';
					exit;
				}
			}else{
				///// checking number status
				if($smsMemberSettings[0]->twilio_origin_number!='' && $smsMemberSettings[0]->status =='1'){
					$fromNumber=$smsMemberSettings[0]->twilio_origin_number;
				}else{
					echo 'failed';
					exit;
				}
			}
			
		}
		
		if($campaign_mode!=''){
		
         switch($campaign_mode){
			
			case 'group':
			$split_groups= explode(',',substr($to_groups,0,-1));
			//// getting group contacts 
			for($g=0;$g<count($split_groups);$g++){
				
				$fullContacts= $this->sms_model->get_group_contacts($split_groups[$g],$user_id);
				//print_r($fullContacts);
				
				if($fullContacts!='0'){
					foreach($fullContacts as $contact)
					{
						$getSmsPrice = $this->sms_model->get_sms_priceby_country($contact->country_code);
						//// preparing campaign 
						$rset=$this->sms_model->prepare_campaign($camp_code,$user_id,$split_groups[$g],$fromNumber,$contact->contact_mobile,
						$message,$getSmsPrice[0]->price,$fScheduledTime,$isScheduled,$fCampTimeZone,$contact->country_code);
						
						if($rset){
							echo _("success");
						}else{
							echo _("failed @").$contact->contact_mobile;
						}
						
					}
				}
				
			}
			//// trigger sms alert to admin 
			if($alertMasking!=''){
				$status=$client->account->messages->create(array( 
					'To' => $appAlertMobile, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
				));
			}
			break;
			
			case 'contact':
			$split_contacts= explode(',',$to_groups);  // parsing here contact ids
			//print_r($split_groups);
			//// getting group contacts 
			for($g=0;$g<count($split_contacts);$g++){
				
				$fullContacts= $this->sms_model->get_group_contacts_bycid($split_contacts[$g],$user_id);
				
				//print_r($fullContacts);
				
				if($fullContacts!='0'){
					foreach($fullContacts as $contact)
					{
						$getSmsPrice = $this->sms_model->get_sms_priceby_country($contact->country_code);
						//// preparing campaign 
						$rset=$this->sms_model->prepare_campaign($camp_code,$user_id,$contact->contact_group,$fromNumber,$contact->contact_mobile,
						$message,$getSmsPrice[0]->price,$fScheduledTime,$isScheduled,$fCampTimeZone,$contact->country_code);
						
						if($rset){
							echo _("success");
						}else{
							echo _("failed @").$contact->contact_mobile;
						}
						
					}
				}
				
			}
			//// trigger sms alert to admin 
			if($alertMasking!=''){
				$status=$client->account->messages->create(array( 
					'To' => $appAlertMobile, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
				));
			}
			
			break;
			
			
		 }// end switch
		} // end if
		
		else{
			
			//echo '<span class="text-danger">' . _('Campaign mode change to manual.') . '</span>';
			// campaign with manual numbers
			
			$split_numbers= explode(',',$mcn_contacts); // parsing here contact numbers
			for($n=0;$n<count($split_numbers);$n++){
				
				$fullContacts= $this->sms_model->get_group_contacts_bycnumber($split_numbers[$n],$user_id);
				if($fullContacts!='0'){
					foreach($fullContacts as $contact)
					{
						$getSmsPrice = $this->sms_model->get_sms_priceby_country($contact->country_code);
						//// preparing campaign 
						$rset=$this->sms_model->prepare_campaign($camp_code,$user_id,$contact->contact_group,$fromNumber,$contact->contact_mobile,
						$message,$getSmsPrice[0]->price,$fScheduledTime,$isScheduled,$fCampTimeZone,$contact->country_code);
						
						if($rset){
							echo _("success");
						}else{
							echo _("failed @").$contact->contact_mobile;
						}
						
					}
				}
				
			}// end for
			//// trigger sms alert to admin 
			if($alertMasking!=''){
				$status=$client->account->messages->create(array( 
					'To' => $appAlertMobile, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
				));
			}
		}
		
	}
	public function add_sms_group(){
		$data = json_decode(file_get_contents("php://input"));
		$user_id=$this->session->userdata['site_login']['user_id'];
		$group_name=mysql_real_escape_string($data->group_name);
		$group_type=mysql_real_escape_string($data->group_type);
		$rset=$this->sms_model->ad_contact_group($user_id,$group_name,$group_type);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function delete_sms_group(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$group_id=mysql_real_escape_string($this->input->post('group_id'));
		
		$rset=$this->sms_model->delete_sms_group($user_id,$group_id);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function delete_history_contact(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$campaign_id=mysql_real_escape_string($this->input->post('c_id'));
		
		$rset=$this->sms_model->delete_history_contact($user_id,$campaign_id);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function delete_ttitle(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$tt_code=mysql_real_escape_string($this->input->post('tt_code'));
		
		$rset=$this->sms_model->delete_ttitle($user_id,$tt_code);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function delete_caller_number(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$cn_tid=mysql_real_escape_string($this->input->post('cn_tid'));
		
		$rset=$this->sms_model->delete_caller_number($user_id,$cn_tid);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function delete_sms_contact(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$contact_id=mysql_real_escape_string($this->input->post('contact_id'));
		
		$rset=$this->sms_model->delete_sms_contact($user_id,$contact_id);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function delete_campaign(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$campaign_code=mysql_real_escape_string($this->input->post('campaign_code'));
		
		$rset=$this->sms_model->delete_campaign($user_id,$campaign_code);
		if($rset){
			echo _("success");
		}else{
			echo _("failed");
		}
	}
	public function import_contacts(){
		
		if($this->input->post('import_country')!='' && $this->input->post('import_group')!='' ){
		$data['group']   = mysql_real_escape_string($this->input->post('import_group'));
		
		$exp_country     = explode('__',$this->input->post('import_country'));
		$data['country'] = $exp_country[1];
		$data['calling_code'] = $exp_country[0];
        $data['title']   = 'OneTextGlobal | Import Contacts';
		$data['file']    = $_FILES['file']['tmp_name'];
		$data['uer_id']  = $this->session->userdata['site_login']['user_id'];
		$this->load->view('import/sms/import_contacts',$data);
		}else{
			redirect(base_url().'site/sms/campaign?tkn='.base64_encode('cts'),true);
		}
	}
	
	public function import_contact_process(){
		
		$user_id=$this->session->userdata['site_login']['user_id'];
		$contact_name = mysql_real_escape_string($this->input->post('contact_name'));
		$calling_code = mysql_real_escape_string($this->input->post('calling_code'));
		$mobile_number = mysql_real_escape_string($this->input->post('mobile_number'));
		$email_id = mysql_real_escape_string($this->input->post('email_id'));
		$country = mysql_real_escape_string($this->input->post('country'));
		$group = mysql_real_escape_string($this->input->post('group'));
		
		/// cross check calling code 
		$calling_code_info=$this->sms_model->cross_check_callingcode($calling_code);
		
		if($calling_code_info==0){
			echo 'invalid';
		}
		else
		{
			$concat_mobile=substr($calling_code,0);
			if($concat_mobile!='+'){
				$mobile_calling_code='+'.$calling_code;
			}else{
				$mobile_calling_code=$calling_code;
			}
			
			
			$mobile=$mobile_calling_code.$mobile_number;
			$check=$this->sms_model->check_sms_contact($country,$user_id,$group,$mobile);
			
			if($check==0){
				$res=$this->sms_model->import_sms_contact($user_id,$contact_name,$mobile,$email_id,$country,$group);
				if($res){
					echo 'success';
				}else{
					echo 'failed';
				}
				
				
			}else{
				echo 'duplicated';
			}
		}
		
	}
	public function stage_mobile_validate(){
		    $user_id=$this->session->userdata['site_login']['user_id'];
		    $userInfo=$this->account_model->list_current_user($user_id);
			
			$username=$userInfo[0]->ac_first_name.' '.$userInfo[0]->ac_last_name;
			$mobile_parse=$userInfo[0]->ac_phone;
			
		   ?>
			<form id="passAuthKey" name="passAuthKey"  method="post">
				<div class="row">
				<div class="col-md-12">
				<div class="row">
				<div class="col-md-6  text-center">
					<div class="well"><h1> <i class="fa fa-user"></i></h1> 
					<?php  echo "<p><b>Hi " . $username."</b>"; ?>
					</div>
					<?php 
					echo " Please ensure that your mobile device is available in proper singals.</p>";
					?>
				</div>


				<div class="col-md-6">
				<div class="form-group">
					<div>
					<label> <?php echo _("Enter Authentication Password")?></label>
					<input type="text" name="parseAuthkey" id="parseAuthkey" placeholder="<?php echo _('e.g b405xxxxxx') ?>" class="form-control" required> 
					</div>
				</div>
				
				<br>
				<div class="form-group">
					<div>
					<button type="button" name="submitAuthKey" id="submitAuthKey"  class="btn btn-warning btn-border" onclick="javascript:triggerCodeValidate()">
					<i class="fa fa-check"></i> <?php echo _("Validate")?></button>
					</div>
				</div>

					</div>
					</div>
				</div>
				</div>
			</form>
		<?php
	}
	public function resend_mobile_validate(){
		//$this->load->model('site/account_model','',TRUE);
		$user_id=$this->session->userdata['site_login']['user_id'];
		$userInfo=$this->account_model->list_current_user($user_id);
		
		$username=$userInfo[0]->ac_first_name.' '.$userInfo[0]->ac_last_name;
		$mobile_parse=$userInfo[0]->ac_phone;
		$mobile_1=substr($mobile_parse,0,3);

		switch($mobile_1){
			case '673':
			$mobile='+'.$mobile_parse;
			break;
			
			case '+673':
			$mobile=$mobile_parse;
			break;
			
			default:
			$mobile=$mobile_parse;
			
		}
		
		?>
   
		<form id="generateTwoFA" action="" method="POST" class="center">
		 <div class="row">
		   <div class="col-md-12">
				<div class="col-md-6 text-center">
				<div class="well">
					<h1> <i class="fa fa-retweet"></i></h1>
					<strong><?php echo _("Resend Password")?></strong>
				</div>
				<?php 
				echo "<p>" . _('Please ensure that your mobile device is available in proper singals.') . "</p>";
				?>
				</div>
				<div class="col-md-6">
				<div class="form-group">
					<label><?php echo _("Mobile Owner")?></label>
					<div>
					<input type="text" name="userName" id="userName" required="required" value="<?php echo $username;?>" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label><?php echo _("Mobile Number")?></label>
					<div>
					<input type="tel" name="tel_number" id="tel_number" required="required" value="<?php echo $mobile;?>" class="form-control">
					<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
					</div>
				</div>

				<div class="form-group">
					<label><?php echo _("Deliver Password Via")?></label>
					<div>
					<input type="radio" name="method" value="sms" checked>&nbsp; <?php echo _("SMS")?>&nbsp;&nbsp;
					<input type="radio" name="method" value="voice">&nbsp; <?php echo _("Voice Call")?></div>
				</div>

				<div class="form-group">
				<input type="button" value="Receive" class="btn btn-success btn-border" onclick="javascript:triggerTwofactorCode()">
				</div>

				</div>
		   </div>
		</div>
		</form>
      <?php
	}
	
	public function generate_twofactor_token(){
		$user_id=$this->session->userdata['site_login']['user_id'];
		$SmsSiteSettings=$this->app_settings_model->get_sms_member_settings($user_id);
		$appSettings=$this->app_settings_model->get_app_settings();
		$smsSettings=$this->app_settings_model->get_sms_settings();
		
		
		$account_sid = $smsSettings[0]->twilio_accid;
		//Put your Twilio Auth Key here:
		$auth_token = $smsSettings[0]->twilio_authtoken;
		// Put your Twilio Outgoing SMS enabled number here:
		
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/twilio_model','',TRUE);
		
		$userInfo=$this->account_model->list_current_user($user_id);
		
		$userName = $userInfo[0]->ac_first_name.' '.$userInfo[0]->ac_last_name;
		$recipient=$this->input->post("recipient");
		
		if($recipient!=''){
		   $toNumber = $userInfo[0]->ac_phone;
		}else{
			$toNumber=$recipient;
		}
		
		$authMethod = $this->input->post("method");
		 
		//echo _("Mobile Number :").$toNumber;
		
		// Generate a new password for this two factor instance
		//$password = substr(md5(time().rand(0, 10^10)), 0, 10);
		$password = rand(1000,10000);

		// Assemble the SMS or voice message ready for Twilio to deliver

		$content = " Hi ". $userName . ", Your Mobile Authentication Password is: " . $password."  by ".$appSettings[0]->app_default_name;

		// If the delivery method is via SMS, make the API call to Twilio sending the message using the supplied credentials. 
		$client = new Services_Twilio($account_sid, $auth_token); 

		if ($authMethod == "sms") {
			
			
			// Checking member purchased number/sender id
			if(@$SmsSiteSettings[0]->twilio_sender_id=='' && @$SmsSiteSettings[0]->twilio_origin_number!=''){
				$senderId=$SmsSiteSettings[0]->twilio_origin_number;
				
			}elseif(@$SmsSiteSettings[0]->twilio_sender_id!='' && @$SmsSiteSettings[0]->twilio_origin_number!=''){
				$senderId=$SmsSiteSettings[0]->twilio_sender_id;
				
			}elseif(@$SmsSiteSettings[0]->twilio_sender_id=='' && @$SmsSiteSettings[0]->twilio_origin_number==''){
				$senderId=$smsSettings[0]->twilio_sender_id;
				
			}
			
			// Propagate app default name as sender id if empty from sms settings
			/*if($senderId!=''){
				$outboundNumber=$senderId;
			}else{
				$outboundNumber = $appSettings[0]->app_default_name; 
			}*/
			
			/// check sender if from backend sms settings 
			if($smsSettings[0]->twilio_sender_id!=''){
				$outboundNumber = $smsSettings[0]->twilio_sender_id; 
			}else{
				$outboundNumber = $smsSettings[0]->twilio_origin_number; 
			}
			
			
		$client->account->messages->create(array( 
			'To' => $toNumber, 
			'From' => $outboundNumber, 
			'Body' => $content,   
		));
		
		 
		}
		else // If we are not delivering the message via SMS it must be via voice
		{
			// Checking member purchased number/sender id
			if(@$SmsSiteSettings[0]->twilio_origin_number==''){
				$outboundNumber = $smsSettings[0]->twilio_origin_number; 	
			}else{
				$outboundNumber = $SmsSiteSettings[0]->twilio_origin_number;
			}
			$messageURL ="http://twimlets.com/message?Message=Hello+".str_replace(" ", "+", $userName)."+you+received+fifty+thousand+dollars+please+call+four+d+lottery+and+password+is+".$password;
			
			$client = new Services_Twilio($account_sid,$auth_token);
			$call = $client->account->calls->create(
			  $outboundNumber,
			  $toNumber, 
			  $messageURL
			);
		}

		// Store the username and password in the session - This is for demo purposes only!
		// When your done testing remove this to prevent the username and password being stored on the client machine. 

		$status='0';
		$userID=$user_id;
		$userMobile=$toNumber;
		$userPass=$password;
		$userStatus=$status;

		$res=$this->twilio_model->load_mobile_token($userID,$userMobile,$userPass,$userStatus); 
		if($res){
			echo "<h4 class='text-success'><i class='fa fa-check'></i> &nbsp; " . _('Your password delivered successfully.') . "</h4>";
			Sms::stage_mobile_validate();
			
		}else{
			echo "<strong><i class='fa fa-times'></i> " . _('Sorry your request failed. ') . "</strong>";
		}
			

	}
	public function validate_twofactor_token(){
		$parseAuthkey= mysql_real_escape_string($this->input->post('token'));
		$userId=$this->session->userdata['site_login']['user_id'];
		$this->load->model('site/twilio_model','',TRUE);
		$res=$this->twilio_model->update_mobile_token($userId,$parseAuthkey);
	
	
		if ($res)
		{
		   echo "<span class='text-success'><i class='fa fa-check-circle'></i> " . _('You mobile validated successfully.') . "</span>";
		}
		else{
			echo "<span class='text-danger'><i class='fa fa-exlamation'></i> " . _('Sorry something went wrong.') . "</span> ";
		}
	}
	
	public function load_modal_contact(){
	           $contact_id=$this->input->post('cId');
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $data=$this->sms_model->get_single_contact_bycid($contact_id,$user_id);
			  
			   if($data!=0){
			   $mv_mxc_id=$data[0]->c_id;
			   $mv_mxc_mobile=$data[0]->contact_mobile;
			   $mv_mxc_name=$data[0]->contact_name;
			   $mv_mxc_email=$data[0]->contact_email;
			   $mv_mxc_ccode=$data[0]->country_code;
			   $mv_mxc_groupname=$data[0]->group_name;
			   
			   echo $mv_mxc_id.'__'.$mv_mxc_mobile.'__'.$mv_mxc_name.'__'.$mv_mxc_email.'__'.$mv_mxc_ccode.'__'.$mv_mxc_groupname;
			   }else{
				   echo 'failed';
			   }
	}
	public function load_sms_remarks(){
	           $campaign_id=$this->input->post('cId');
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $data=$this->sms_model->get_sms_remarks($campaign_id,$user_id);
			  
			   if($data!=0){
			   
			      echo $data[0]->remarks;
			   }else{
				   echo 'failed';
			   }
	}
	
	public function load_campaign_headline(){
	           $campaign_code=$this->input->post('c_code');
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $data=$this->sms_model->get_campaign_headline($campaign_code,$user_id);
			  
			   if($data!=0){
			   $dxc_code=$data[0]->campaign_code;
			   $dxc_message=$data[0]->message;
			   echo $dxc_code.'__'.$dxc_message;
			   }else{
				   echo 'failed';
			   }
	}
	
	public function load_cgroup_headline(){
	           $group_id=$this->input->post('cg_id');
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $data=$this->sms_model->get_group_name($group_id,$user_id);
			  
			   if($data!=0){
			   $dxc_cgid=$data[0]->cgroup_id;
			   $dxc_cgname=$data[0]->group_name;
			   echo $dxc_cgid.'__'.$dxc_cgname;
			   }else{
				   echo 'failed';
			   }
	}
	
	public function load_ttitle_headline(){
	           $t_code=$this->input->post('t_code');
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $data=$this->sms_model->get_ttitle_headline($t_code,$user_id);
			  
			   if($data!=0){
			   $tcx_cgid=$data[0]->template_code;
			   $tcx_cgname=$data[0]->template_title;
			   echo $tcx_cgid.'__'.$tcx_cgname;
			   }else{
				   echo 'failed';
			   }
	}
	public function load_notification(){
	           $notify_id=$this->input->post('notify_id');
			   $data=$this->notification_model->load_notification_byid($notify_id);
			  
			   if($data!=0){
				   echo $data[0]->notify_body;
			   }else{
				   echo 'failed';
			   }
	}
	public function load_notification_headline(){
	           $notify_id=$this->input->post('notify_id');
			   $data=$this->notification_model->load_notification_byid($notify_id);
			  
			   if($data!=0){
				   echo $data[0]->notify_title;
			   }else{
				   echo 'failed';
			   }
	}
	public function delete_notification(){
	           $notify_id=$this->input->post('notify_id');
			   $rset=$this->notification_model->delete_notification_bymember($notify_id);
			  
			   if($rset){
				   echo 'success';
			   }else{
				   echo 'failed';
			   }
	}
	
	public function caller_number_headline(){
	           $tId=$this->input->post('tId');
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $data=$this->sms_model->get_caller_number_headline($tId,$user_id);
			  
			   if($data!=0){
			   $cn_tid=$data[0]->tid;
			   $cn_tno=$data[0]->twilio_origin_number;
			   $cn_status=$data[0]->status;
			   	
			   echo $cn_tid.'__'.$cn_tno.'__'.$cn_status;
			   }else{
				   echo 'failed';
			   }
	}
	
	
	
	
}
