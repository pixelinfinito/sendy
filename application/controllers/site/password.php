<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
 	
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Password extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/password_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
	}
	
	public function index()
	{
		$this->load->view('site/forgot_password');
	}
	public function forgot()
	{
		$this->load->view('site/forgot_password');
	}
	public function process(){
		$data = json_decode(file_get_contents("php://input"));
		$email_id = mysql_real_escape_string($data->email_id);
		$result=$this->password_model->check($email_id);
		
		if($result!=''){
			$rand=rand(12345,54232);
			$mix_password=$rand;
			$new_password=md5($mix_password);
			$user_id=$result[0]->user_id;
			
			$intlib=$this->internal_settings->local_settings();
			$from_emails    = $intlib[0]->default_from_mail;
			$notify_appname = $intlib[0]->app_default_name;
			
			$from_email		    = $from_emails;
			$to_email  		    = $result[0]->ac_email;
			$email_subject      = sprintf(_('%s\'s - New Password'), $notify_appname);
			$email_body         = _('Dear ') . $result[0]->ac_first_name.' '.$result[0]->ac_last_name.', <br><br>
			' . sprintf(_('Your new password is here, please logon to %s\'s account'), $notify_appname) .'  
			<br><br> <b>' . _('Updated Credentials') . ' :</b> <br>
			' . _('Username') . ' : '.$result[0]->ac_email.'<br>
			' . _('Password') . ' : '.$mix_password.'<br><br> ' . _('Thank you.') . '<br><br>
			' . _('Regards,') . '<br> ' . _('Team') . ' '.$notify_appname.' <br><br> ';
			$email_body.='------------------------------------------------------------------------------------------------<br>';
			$email_body.='<em>' . sprintf(_('Note : This is a system generated message.Please do not reply to this email.Please contact %s 
				 if you require further assistance.'),'<u>' . _('our support team') . '</u>') . '</em>';
			$name  			    = $notify_appname;
			
			//Sending SMTP Mail
			
			$this->load->library('email');
			$this->email->from($from_email, $name);
			$this->email->to($to_email);
			$this->email->subject($email_subject );
			$this->email->message($email_body);
			
			if ($this->email->send()) {
				$result=$this->password_model->update_new_password($user_id,$new_password);
				if($result){
					echo "<span class='text-success'><b><i class='fa fa-check-circle'></i> 
					" . _("Your request success, please check your email.") . "</b></span>";
					
					
				}else{
					echo _("failed");
					
				}
			}else{
				echo _("failed");
			}
		
			
		}else{
			echo _("failed");
		}
	}
	
	
	
}
