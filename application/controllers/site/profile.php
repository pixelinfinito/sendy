<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Profile extends CI_Controller {
     
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('site/twilio_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		
	}
	
	public function index()
	{
		
		$this->load->view('site/profile');
	}
	
	public function add()
	{
		$this->load->view('site/wallet/add');
	}
	
	
	public function process(){
		$this->load->model('site/account_model','',TRUE);
		  
	}
	public function update(){
			$first_name = $this->input->post('first_name'); 
			$last_name  = $this->input->post('last_name');
			$gender   = $this->input->post('gender');
			$address1   = $this->input->post('address1');
			$address2   = $this->input->post('address2');
			
			$city       = $this->input->post('city');
			$phone      = $this->input->post('phone');
			$postalcode = $this->input->post('postalcode');
			$user_id    = $this->session->userdata['site_login']['user_id'];
			
            $vRes=$this->account_model->update_profile($user_id,$first_name,$last_name,$gender,$address1,$address2,$city,$phone,$postalcode);
			if($vRes){
				redirect(base_url().'site/profile?token='.base64_encode('profile_success'));
			}else{
				redirect(base_url().'site/profile?token='.base64_encode('profile_failed'));
			}
			
	}
	public function update_password(){
		    $old_password      = mysql_real_escape_string($this->input->post('old_password')); 
			$new_password      = mysql_real_escape_string($this->input->post('new_password'));
			$retype_password   = mysql_real_escape_string($this->input->post('retype_password'));
			$user_id           = $this->session->userdata['site_login']['user_id'];
			
			$vRes=$this->account_model->update_password($user_id,$old_password,$new_password);
			if($vRes){
				redirect(base_url().'site/profile?token='.base64_encode('password_success'));
			}else{
				redirect(base_url().'site/profile?token='.base64_encode('password_failed'));
			}
	}
	public function update_preferences(){
		    $news_letters      = mysql_real_escape_string($this->input->post('news_letters')); 
			$payment_threshold = mysql_real_escape_string($this->input->post('payment_threshold'));
			$user_id           = $this->session->userdata['site_login']['user_id'];
			
			$vRes=$this->account_model->update_preferences($user_id,$news_letters,$payment_threshold);
			if($vRes){
				redirect(base_url().'site/profile?token='.base64_encode('preferences_success'));
			}else{
				redirect(base_url().'site/profile?token='.base64_encode('preferences_failed'));
			}
	}
	
	public function activation()
	{
		$this->load->model('categories_model','',TRUE);
		
		$validate_code 		= mysql_real_escape_string(base64_decode($this->input->get('token')));
		switch($validate_code){
			
			
			case $validate_code:
			if($validate_code!=''){
			  
			  $vRes=$this->account_model->activate_account($validate_code);
			  
			  if($vRes){
				 $this->load->view('site/validation/success');
			  }else{
				 
				 $this->load->view('site/validation/fail');
			  }
			  
			  
			}else{
				$this->load->view('site/validation/not_found');
			}
			break;
		}
		
	}
 
     public function thumbnail(){
	   $user_id=$this->session->userdata['site_login']['user_id'];	   
	   
	   if(isset($_FILES['files'])){
		  
		  
				if(!file_exists('uploads/profiles/'.$user_id)){
					////////// Create a folder on userid ////////////
					 mkdir('uploads/profiles/'.$user_id,0777,true);
				}else{
					////////// Use existing user folder ////////////
					$local_dir='uploads/profiles/'.$user_id;
				}
				
				$file_name = $_FILES['files']['name'];
				$file_size =$_FILES['files']['size'];
				$file_tmp =$_FILES['files']['tmp_name'];
				$file_type=$_FILES['files']['type'];	
				if($file_size > 2097152){
					$errors[]='File size must be less than 2 MB';
				}		
				
				$local_dir='uploads/profiles/'.$user_id;
				$thumb_path=$local_dir."/".$file_name;
				$encFile=$file_name;
				
				if (file_exists($encFile)) {
					echo "<span class='red'>The file $encFile exists</span>";
					exit;
					
				} else 
				{
				   //echo _("The file $encFile does not exist");
				   move_uploaded_file($file_tmp,$local_dir."/".$encFile);
				   $resImages=$this->account_model->update_thumbnail($user_id,$thumb_path);
				
                   if($resImages){
					  
					   redirect(base_url().'site/dashboard',true);
				   }else{
					   echo _("Sorry something went wrong.");
				   } 				
				
				}
				
				
		   }
	}
	
	
}
