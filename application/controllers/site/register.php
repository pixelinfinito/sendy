<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Register extends CI_Controller {
     
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
	}
	
	public function index()
	{
		
		$this->load->view('site/register');
	}
	
	public function register_process(){
		
		$data 			= json_decode(file_get_contents("php://input"));
		$ac_first_name 		= mysql_real_escape_string($data->ac_first_name);
		$ac_last_name 		= mysql_real_escape_string($data->ac_last_name);
		$ac_phone 	= mysql_real_escape_string($data->ac_phone);
		$ac_country 		= mysql_real_escape_string($data->ac_country);
		$ac_class 		= mysql_real_escape_string($data->ac_class);
		$ac_gender 		= mysql_real_escape_string($data->ac_gender);
		$ac_about 	= mysql_real_escape_string($data->ac_about);
		$ac_email 	= mysql_real_escape_string($data->ac_email);
		$ac_password 	= mysql_real_escape_string($data->ac_password);
		$created_by=0;
		$ac_active_status=0;
		
		$result = $this->account_model->add_account($ac_email,$ac_class,$ac_password,$ac_first_name,
		$ac_last_name,$ac_phone,$ac_gender,$ac_about,$ac_country,$created_by,$ac_active_status);
		
		switch($result){
			case 1:
			echo _("success");
			break;
			
			case 0:
			echo _("failed");
			break;
			
			case 2:
			echo _("duplicated");
			break;
		}
	}
	public function activation()
	{
		
		$validate_code 		= mysql_real_escape_string(base64_decode($this->input->get('token')));
		switch($validate_code){
			
			
			case $validate_code:
			if($validate_code!=''){
			  
			  $vRes=$this->account_model->activate_account($validate_code);
			  
			  if($vRes){
				 $this->load->view('site/validation/success');
			  }else{
				 
				 $this->load->view('site/validation/fail');
			  }
			  
			  
			}else{
				$this->load->view('site/validation/not_found');
			}
			break;
		}
		
	}
 
	
}
