<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Bye extends CI_Controller {
     
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
	}
	
	public function index(){
		
		$this->session->unset_userdata('site_login');
		$url=base_url()."site/login";
		header("Location:".$url);
		
   }
  
	
}
