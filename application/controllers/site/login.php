<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Login extends CI_Controller {
     
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',true);
		
	}
	
	public function index()
	{
		$this->load->view('site/login');
	}
	public function authenticate()
	{
		$data = json_decode(file_get_contents("php://input"));
		$username = mysql_real_escape_string($data->uname);
		$parse_password = mysql_real_escape_string($data->pswd);
       
		$password = md5($parse_password);
		$result = $this->account_model->authenticate($username,$password);
		
		
		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{
				$sess_array = array(
				'user_id' => $row->user_id,
				'ac_first_name' => $row->ac_first_name,
				'ac_last_name' => $row->ac_last_name,
				'ac_email' => $row->ac_email,
				'ac_mobile'=>$row->ac_phone,
				'ac_country' => $row->ac_country,
				'ad_limit'=>$row->ad_limit,
				'last_login'=>$row->last_login,
				
				);
				
				$this->session->set_userdata('site_login',$sess_array);
			}
			   
			echo _("success");
		}else{
			echo _("failed");
		}
		
   }
   
  
	
}
