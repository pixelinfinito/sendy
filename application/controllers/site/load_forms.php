<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Load_Forms extends CI_Controller {
   
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('site/twilio_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
	}
	
	public function load_mv_contact(){
	           $contact_id=$this->input->get('cid');
			   $user_id = $this->session->userdata['site_login']['user_id'];
			   $data['dy_contact_info']=$this->sms_model->get_single_contact_bycid($contact_id,$user_id);
			   $this->load->view('includes/modals/load_mv_form',$data);
	}
	
}
