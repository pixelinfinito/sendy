<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Payment extends CI_Controller {
  
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		
	}
	
	public function index()
	{
		$this->load->view('site/wallet/add');
	}
	public function history()
	{
		
		$this->load->view('site/payment/history');
	}
	public function load_history(){
		
		$userId=$this->session->userdata['site_login']['user_id'];
		$result=$this->payment_model->get_member_payments($userId);
		$json = json_encode($result);
		echo $json;
	}
	public function add()
	{
		$this->load->view('site/wallet/add');
		require_once(APPPATH.'third_party/2checkout/lib/Twocheckout.php');
	}
	
	public function card()
	{
		$this->load->view('site/payment/card');
		
	}
	public function process_card(){
		
		$data         = json_decode(file_get_contents("php://input"));
		
		$userId=$this->session->userdata['site_login']['user_id'];
		$c_holder_name     = mysql_real_escape_string($data->c_holder_name);
		$c_number = mysql_real_escape_string($data->c_number);
		$c_month = mysql_real_escape_string($data->c_month);
		$c_year     = mysql_real_escape_string($data->c_year);
		$c_cvv     = mysql_real_escape_string($data->c_cvv);
		
		$c_address1 = mysql_real_escape_string($data->c_address1);
		$c_address2 = mysql_real_escape_string($data->c_address2);
		$c_city     = mysql_real_escape_string($data->c_city);
		$c_country     = mysql_real_escape_string($data->c_country);
		$c_postal_code = mysql_real_escape_string($data->c_postal_code);
		
		
		$result = $this->payment_model->update_card($c_holder_name,$c_number,$c_month,$c_year,$c_cvv,$c_address1,$c_address2
		,$c_city,$c_country,$c_postal_code,$userId);
		
		if($result){
			
			switch($result){
				case 1:
				echo _("success");
				break;
				
				case 0:
				echo _("failed");
				break;
				
				case 2:
				echo _("duplicated");
				break;
			}
		}
	}
	
	public function process(){
		
		$tcoSettings=$this->app_settings_model->get_payment_settings();
		$userId=$this->session->userdata['site_login']['user_id'];
		$userInfo= $this->account_model->list_current_user($userId);
		$fullName=$userInfo[0]->ac_first_name.' '.$userInfo[0]->ac_last_name;
		require_once(APPPATH.'third_party/2checkout/lib/Twocheckout.php');
		
		if($tcoSettings[0]->tco_private_key!='' && $tcoSettings[0]->tco_seller_id!='')
		{
			$tcoPrivateKey=@$tcoSettings[0]->tco_private_key;
			$tcoSellerId=@$tcoSettings[0]->tco_seller_id;
			
		}else{
			$tcoPrivateKey='F0BC2305-A262-4F98-ADBB-8428D1CBACA2';
			$tcoSellerId='901316091';
		}
		
		if($tcoSettings[0]->tco_username!='' && $tcoSettings[0]->tco_password!='')
		{
			$tcoUsername=@$tcoSettings[0]->tco_username;
			$tcoPassword=@$tcoSettings[0]->tco_password;
			
		}else{
			$tcoUsername='sybase123';
			$tcoPassword='Systems@321#';
		}
		
		Twocheckout::username($tcoUsername); 
		Twocheckout::password($tcoPassword); 
		Twocheckout::privateKey($tcoPrivateKey); //Private Key
		Twocheckout::sellerId($tcoSellerId); // 2Checkout Account Number
		
		if($tcoSettings[0]->tco_is_sandbox==1){
		    Twocheckout::sandbox(true); // Set to false for production accounts.
		}else{
			Twocheckout::sandbox(false); // Set to false for production accounts.
		}
		?>
		<script src="<?php echo base_url()?>js/jquery-2.1.1.js"></script>
		<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url()?>theme4.0/client/css/style.css" rel="stylesheet">
	<?php
	
try {
    $charge = Twocheckout_Charge::auth(array(
        "merchantOrderId" => mysql_real_escape_string($this->input->post('c2o_wallet_order')),
        "token"      => mysql_real_escape_string($this->input->post('token')),
        "currency"   => 'USD',
        "total"      => mysql_real_escape_string($this->input->post('amount')),
        "billingAddr" => array(
            "name" => mysql_real_escape_string($fullName),
            "addrLine1" => mysql_real_escape_string($userInfo[0]->ac_address1),
            "city" => mysql_real_escape_string($userInfo[0]->ac_city),
            "state" => 'NOST',
            "zipCode" => mysql_real_escape_string($userInfo[0]->ac_postalcode),
            "country" => mysql_real_escape_string($userInfo[0]->ac_country),
            "email" => mysql_real_escape_string($userInfo[0]->ac_email),
            "phoneNumber" => mysql_real_escape_string($userInfo[0]->ac_phone),
        )
    ));
	
    if ($charge['response']['responseCode'] == 'APPROVED') {
		
		$c2ResponseCode=$charge['response']['responseCode'];
		$c2ResponsMsg=$charge['response']['responseMsg'];
		$c2OrderNumber=$charge['response']['orderNumber'];
		$c2TransactionId=$charge['response']['transactionId'];
		$currencyCode=$this->input->post('c2o_currency_code');
		$c2TotalAmount=$this->input->post('amount');
		$itemNumber=$this->input->post('c2o_wallet_order');
		$walletTotalBalance=$this->input->post('wallet_tamt');
		$walletAvailBalance=$this->input->post('wallet_bamt');
		$paymentStatus='1';
		
		$resMsg=$this->wallet_model->add_funds_ByC2o_payment($itemNumber,$c2TransactionId,$currencyCode,$c2OrderNumber,$c2TotalAmount,
	    $c2ResponseCode,$c2ResponsMsg,$paymentStatus,$userId,$walletTotalBalance,$walletAvailBalance);
	
        /*echo _("Thanks for your Order!");
        echo "<h3>" . _('Return Parameters:') . "</h3>";
        echo "<pre>";
        print_r($charge);
        echo "</pre>";
		*/
		
		echo "<div class=main-container'><div class='container'><div class='row'>
		<div class='col-md-12 inner-box category-content'>
		<h2 class='text-primary'>" . _("Thank you for the wallet funds.") . "</h2>
		<strong class='text-success'><i class='fa fa-check-circle'></i> " . sprintf(_("Your funds $%s added successfully."), $this->input->post('amount')) . " </strong>
		<br>
		<span class='text-warning'><i class='fa fa-spinner fa-spin'></i> " . _("Please wait while we redirecting to dashboard.") . "</span>
		<br><br>
		</div>";
		
		?>
		<script>
			$(document).ready(function () {
			// Handler for .ready() called.
			window.setTimeout(function () {
			location.href = "<?php echo base_url()?>site/dashboard";
			}, 5000);
			});
		</script>
		<?php
	
    }
} catch (Twocheckout_Error $e) {
	    echo "<div class=main-container'><div class='container'><div class='row'>
		<div class='col-md-12 inner-box category-content'>
		<h2 class='text-primary'>".$e->getMessage()."</h2>
		<strong class='text-danger'><i class='fa fa-times'></i>" . _("Prevented unauthorized activities") . "</strong>
		<br>
		<span class='text-muted'><i class='fa fa-spinner fa-spin'></i> " . _("Please wait while we redirecting to wallet.") . "</span>
		<br><br>
		</div>";
		?>
		<script>
			$(document).ready(function () {
			// Handler for .ready() called.
			window.setTimeout(function () {
			location.href = "<?php echo base_url()?>site/wallet/add";
			}, 8000);
			});
		</script>
		<?php
	
}
		  
	}
	public function load_user_wallet(){
		
		$token 		= mysql_real_escape_string($this->input->post('token'));
		$userId=$this->session->userdata['site_login']['user_id'];
		$remoteCountry=$this->config->item('remote_country_code');
        $currencyInfo= $this->countries_model->get_currencies_cCode($remoteCountry);
             
		if($token=='wallet'){
			$result = $this->wallet_model->get_account_wallet($userId);
			
			
			if($result>0){
			   //echo "<h2 class='custom_priceTag'><i class='fa fa-google-wallet'></i> Your wallet balance is (".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.' '.$result[0]->balance_amount.") </h2>";
			   echo '1';
			}else{
				echo "<h2 class='custom_priceTag'><i class='fa fa-google-wallet'></i> " . _('You wallet balance is empty.') . "</h2>";
				?>
				<?php echo _("Please")?><strong><?php echo _("add funds")?></strong> <?php echo _("to your wallet, otherwise your ad goes to regular category.")?><br><br>
				
				<a href="<?php echo base_url()?>site/wallet/add" class="btn btn-success"><strong><?php echo _("Click here.")?></strong></a>
				
				<?php
			}
			
		
		}else{
			echo "<h2>" . _('Suspected activities are not allowed.') . "</h2>";
		}
		
			
	}
	public function success(){
			$item_number = $this->input->get('item_number'); 
			$txn_id = $this->input->get('tx');
			$payment_gross = $this->input->get('amt');
			$currency_code = $this->input->get('cc');
			$payment_status = $this->input->get('st');
			
			$vRes=$this->account_model->update_pp_payment($item_number,$txn_id,$payment_gross,$currency_code,$payment_status);
			if($vRes){
				$this->load->view('site/payment/success?token='.base64_encode($payment_status));
			}else{
				$this->load->view('site/payment/failed');
			}
			
	}
	
	
	public function activation()
	{
		$validate_code 		= mysql_real_escape_string(base64_decode($this->input->get('token')));
		switch($validate_code){
			
			
			case $validate_code:
			if($validate_code!=''){
			  
			  $vRes=$this->account_model->activate_account($validate_code);
			  
			  if($vRes){
				 $this->load->view('site/validation/success');
			  }else{
				 
				 $this->load->view('site/validation/fail');
			  }
			  
			  
			}else{
				$this->load->view('site/validation/not_found');
			}
			break;
		}
		
	}
 
	
}
