<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Dashboard extends CI_Controller {
    
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('user_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		
	}
	   
	public function index(){
		
		$this->load->view('site/dashboard');
	}
	
	public function load_history(){
		
		$this->load->model('site/wallet_model','',TRUE);
		$userId=$this->session->userdata['site_login']['user_id'];
		$result=$this->wallet_model->recent_transactions($userId);
		$json = json_encode($result);
		echo $json;
	}
	
	

	public function free_credit_request(){
	    //$data = json_decode(file_get_contents("php://input"));
		$member_name = mysql_real_escape_string($this->input->post('member_name'));
		$request_comments = mysql_real_escape_string($this->input->post('request_comments'));
		$userId=$this->session->userdata['site_login']['user_id'];
		
		$this->load->model('site/wallet_model','',TRUE);
		$result = $this->wallet_model->free_credit_request($userId,$member_name,$request_comments);
		switch($result){
			case 1:
			echo _("success");
			break;
			
			case 0:
			echo _("failed");
			break;
			
			case 2:
			echo _("duplicated");
			break;
			
		}
	}
}

