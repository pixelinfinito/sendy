<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Wallet extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
	
	}
	
	public function index()
	{
		$this->load->view('site/wallet/add');
	}
	public function history()
	{
		
		$this->load->view('site/wallet/history');
	}
	public function load_history(){
		
		$userId=$this->session->userdata['site_login']['user_id'];
		$result=$this->wallet_model->ad_payments_bywallet($userId);
		$json = json_encode($result);
		echo $json;
	}
	public function add()
	{
		
		$this->load->view('site/wallet/add');
		require_once(APPPATH.'third_party/2checkout/lib/Twocheckout.php');
	}
	public function transfer()
	{
		
		$this->load->view('site/wallet/transfer');
		
	}
	public function received()
	{
		
		$this->load->view('site/wallet/received');
		
	}
	public function transfer_process()
	{
		$sender_userid=$this->session->userdata['site_login']['user_id'];
		$sender_first_name=$this->session->userdata['site_login']['ac_first_name'];
		$sender_last_name=$this->session->userdata['site_login']['ac_last_name'];
		
		$receiver_email=$this->input->post('recipient_email');
		$appSettings= $this->app_settings_model->get_primary_settings();
		$appCountry=$appSettings[0]->app_country;
		
		$currencyInfo= $this->countries_model->get_currencies_cCode($appCountry);
		$receiverInfo=$this->account_model->member_details_bymail($receiver_email);
		if($receiverInfo!=0){
			$walletInfo= $this->account_model->get_account_wallet($receiverInfo[0]->user_id);
			$senderWallet= $this->account_model->get_account_wallet($sender_userid);
			
			$settingsInfo= $this->app_settings_model->get_site_settings();
			$wallet_min_val=@$settingsInfo[0]->wallet_min_val;
			$wallet_max_val=@$settingsInfo[0]->wallet_max_val;
			$wallet_string=@$settingsInfo[0]->wallet_string;
			 
			$walletCode=$wallet_string.rand($wallet_min_val,$wallet_max_val);
			if($walletCode!=''){
				 $wItemCode="TNF-".$walletCode;
			}else{
				 $wItemCode='TNF-OCWL'.rand(500,5000);
			}
	 
			///// transfer to receiver
			$c2ResponseCode="RECEIVED";
			$c2ResponsMsg=_('Transfer funds by ') . $sender_first_name.' '.$sender_last_name;
			$c2OrderNumber='MOTF-'.rand(5000,50000);
			$c2TransactionId='MTFN-'.rand(1000,50000);
			
			$currencyCode=$currencyInfo[0]->currency_name;
			$c2TotalAmount=$this->input->post('amount');
			$itemNumber=$wItemCode;
			
			$walletTotalBalance=$walletInfo[0]->total_amount;
			$walletAvailBalance=$walletInfo[0]->balance_amount;
			$payment_mode='wallet';
			$paymentStatus='1';
			
			$resMsg=$this->wallet_model->add_funds_ByC2o_payment($itemNumber,$c2TransactionId,$currencyCode,$c2OrderNumber,$c2TotalAmount,
			$c2ResponseCode,$c2ResponsMsg,$paymentStatus,$receiverInfo[0]->user_id,$walletTotalBalance,$walletAvailBalance,$payment_mode);
			
			//// deducting from sender
			if($resMsg){
				$txn_reference=$c2OrderNumber;
				$tns_payment=$c2TotalAmount;
				$payment_method='wallet';
				$payment_reference=$c2TransactionId;
				$payment_remarks=_('Transfer funds to ').$receiverInfo[0]->ac_first_name.' '.$receiverInfo[0]->ac_last_name;
				$payment_status='1';
				$accBalance=$senderWallet[0]->balance_amount;
				
				$result=$this->wallet_model->deduct_creditfrom_member($txn_reference,$tns_payment,$payment_method,$payment_reference,
				 $payment_remarks,$payment_status,$sender_userid,$accBalance);
				if($result){
					echo _("success");
				}else{
					echo _("failed");
				}
			}
		}else{
			echo _("failed");
		}
	}
	
	
	public function process(){
		
		$tcoSettings=$this->app_settings_model->get_payment_settings();
		$userId=$this->session->userdata['site_login']['user_id'];
		$userInfo= $this->account_model->list_current_user($userId);
		$fullName=$userInfo[0]->ac_first_name.' '.$userInfo[0]->ac_last_name;
		
		//// getting billing address
		$card_info=$this->wallet_model->get_card_information($userId);
		if($card_info!=0){
			
			 $mcard_name=$card_info[0]->card_holder_name;
			 $mcard_add1=$card_info[0]->billing_street1;
			 $mcard_add2=$card_info[0]->billing_street2;
			 $mcard_city=$card_info[0]->billing_city;
			 $mcard_country=$card_info[0]->billing_country;
			 $mcard_postal=$card_info[0]->billing_postalcode;
			 

		} else{
			
			 $mcard_name=_('not found');
			 $mcard_add1=_('not found');
			 $mcard_add2=_('not found');
			 $mcard_city=_('not found');
			 $mcard_country=_('not found');
			 $mcard_postal=_('not found');
			 
		}
		
		require_once(APPPATH.'third_party/2checkout/lib/Twocheckout.php');
		if($tcoSettings[0]->tco_private_key!='' && $tcoSettings[0]->tco_seller_id!='')
		{
			$tcoPrivateKey=@$tcoSettings[0]->tco_private_key;
			$tcoSellerId=@$tcoSettings[0]->tco_seller_id;
			
		}else{
			$tcoPrivateKey='F0BC2305-A262-4F98-ADBB-8428D1CBACA2';
			$tcoSellerId='901316091';
		}
		
		if($tcoSettings[0]->tco_username!='' && $tcoSettings[0]->tco_password!='')
		{
			$tcoUsername=@$tcoSettings[0]->tco_username;
			$tcoPassword=@$tcoSettings[0]->tco_password;
			
		}else{
			$tcoUsername='sybase123';
			$tcoPassword='Systems@321#';
		}
		
		Twocheckout::username($tcoUsername); 
		Twocheckout::password($tcoPassword); 
		Twocheckout::privateKey($tcoPrivateKey); //Private Key
		Twocheckout::sellerId($tcoSellerId); // 2Checkout Account Number
		
		if($tcoSettings[0]->tco_is_sandbox==1){
		    Twocheckout::sandbox(true); // Set to false for production accounts.
		}else{
			Twocheckout::sandbox(false); // Set to false for production accounts.
		}
		?>
		<script src="<?php echo base_url()?>js/jquery-2.1.1.js"></script>
		<link href="<?php echo base_url()?>theme4.0/client/css/style.css" rel="stylesheet">
		<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url()?>theme4.0/client/css/light-bootstrap-dashboard-classic.css" rel="stylesheet"/>
		<link href="<?php echo base_url()?>theme4.0/client/css/bootstrap.vertical-tabs.min.css" rel="stylesheet"> 
		<link href="<?php echo base_url()?>theme4.0/client/css/pe-icon-7-stroke.css" rel="stylesheet" />
	<?php
	try {
	$charge = Twocheckout_Charge::auth(array(
        "merchantOrderId" => mysql_real_escape_string($this->input->post('c2o_wallet_order')),
        "token"      => mysql_real_escape_string($this->input->post('token')),
        "currency"   => 'USD',
        "total"      => mysql_real_escape_string($this->input->post('amount')),
        "billingAddr" => array(
            "name" => mysql_real_escape_string($mcard_name),
            "addrLine1" => mysql_real_escape_string($mcard_add1),
            "city" => mysql_real_escape_string($mcard_city),
            "state" => 'NO ST',
            "zipCode" => mysql_real_escape_string($mcard_postal),
            "country" => mysql_real_escape_string($mcard_country),
            "email" => mysql_real_escape_string($userInfo[0]->ac_email),
            "phoneNumber" => mysql_real_escape_string($userInfo[0]->ac_phone),
        )
    ));
	
    if ($charge['response']['responseCode'] == 'APPROVED') {
		
		///// getting success status
		$c2ResponseCode=$charge['response']['responseCode'];
		$c2ResponsMsg=$charge['response']['responseMsg'];
		$c2OrderNumber=$charge['response']['orderNumber'];
		$c2TransactionId=$charge['response']['transactionId'];
		$currencyCode=$this->input->post('c2o_currency_code');
		$c2TotalAmount=$this->input->post('amount');
		$itemNumber=$this->input->post('c2o_wallet_order');
		$walletTotalBalance=$this->input->post('wallet_tamt');
		$walletAvailBalance=$this->input->post('wallet_bamt');
		$payment_mode=$this->input->post('payment_mode');
		$paymentStatus='1';
		
		$resMsg=$this->wallet_model->add_funds_ByC2o_payment($itemNumber,$c2TransactionId,$currencyCode,$c2OrderNumber,$c2TotalAmount,
	    $c2ResponseCode,$c2ResponsMsg,$paymentStatus,$userId,$walletTotalBalance,$walletAvailBalance,$payment_mode);
	
	    //// prepare sms alert to admin
		$alert_first_name   = $this->session->userdata['site_login']['ac_first_name'];
		$alert_last_name    = $this->session->userdata['site_login']['ac_last_name'];
		$alert_country      = $this->session->userdata['site_login']['ac_country'];
		
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appAlertMobile     = $alertAppSettings[0]->default_mobile_number;
		$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
		$account_sid        = $alertSMSSettings[0]->twilio_accid;
		$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
		$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
		$enable_alerts      = $alertSMSSettings[0]->enable_alerts;
		
		$pay_masking_token   = $alertSMSSettings[0]->pay_alert_token;
		if($pay_masking_token!=''){
			$alertMasking       = $alertSMSSettings[0]->pay_alert_token;
		}else{
			//$alertMasking       = "PayAlerts";
			if($enable_alerts==1){
			    $alertMasking       = $alertSMSSettings[0]->twilio_sender_id;
			}else{
				$alertMasking       = '';	
			}
			
		}
		
		$alertContent       = _("Hello new payment made by:").$alert_first_name." ".$alert_last_name.", "._("Order number:").$itemNumber.", "._("Topup amount:").$currencyCode." ".$c2TotalAmount.", "._("Country:").$alert_country;
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
		
		if($alertMasking!=''){
			//// trigger sms alert to admin 
				$status=$client->account->messages->create(array( 
					'To' => $appAlertMobile, 
					'From' => $alertMasking, 
					'Body' => $alertContent,   
			));
		}
		
		?>
		<div class='main-container'>
		<div class='container text-center'>
		   <div class="row">
			   <div class='col-md-3'>&nbsp;</div>
			   <div class='col-md-3 text-right'>
			   <img src="<?php echo base_url()?>theme4.0/client/images/logo_black.png"> 
			   </div>
			   <div class='col-md-3 text-left'><h3 class="text-muted">| <?php echo _("Payment area");?></h3></div>
			   <div class='col-md-3'>&nbsp;</div>
		   </div>
		   <div class="row">
			<div class='col-md-3'>&nbsp;</div>
			<div class='col-md-6'>
			<div class="inner-box">
			<h3 class="text-success"><i class="pe-7s-check"></i> <?php echo _("Congratulations");?></h3>
				<?php echo sprintf(_("Your transaction done & wallet funds <b>%f</b>  added successfully."), $this->input->post('amount'));?>
			<br><br>
			<hr>
			<br>
			<span class="text-primary"><i class='fa fa-spinner fa-spin'></i> <?php echo _("Please wait while we redirecting to site ..");?> </span><br><br>
			
			</div>
			</div>
			<div class='col-md-3'>&nbsp;</div>
		  </div>
		</div>
		</div>
		
		<script>
			$(document).ready(function () {
			// Handler for .ready() called.
			window.setTimeout(function () {
			location.href = "<?php echo base_url()?>site/sms/campaign?tkn=<?php echo base64_encode('nc')?>";
			}, 5000);
			});
		</script>
		<?php
		}
		} 
		catch (Twocheckout_Error $e) {
			
			///// getting failure status
			$c2ResponseCode='failed';
			$c2ResponsMsg=$e->getMessage();
			$c2OrderNumber='failed';
			$c2TransactionId='failed';
			$currencyCode=$this->input->post('c2o_currency_code');
			$c2TotalAmount=$this->input->post('amount');
			$itemNumber=$this->input->post('c2o_wallet_order');
			$payment_mode=$this->input->post('payment_mode');
			$paymentStatus='0';

			$resMsg=$this->wallet_model->failure_funds_ByC2o_payment($itemNumber,$c2TransactionId,$currencyCode,$c2OrderNumber,$c2TotalAmount,
			$c2ResponseCode,$c2ResponsMsg,$paymentStatus,$userId,$payment_mode);
		?>
		<div class='main-container'>
		<div class='container text-center'>
		   <div class="row">
			   <div class='col-md-3'>&nbsp;</div>
			   <div class='col-md-3 text-right'>
			   <img src="<?php echo base_url()?>theme4.0/client/images/logo_black.png"> 
			   </div>
			   <div class='col-md-3 text-left'><h3 class="text-muted">| <?php echo _("Payment area");?></h3></div>
			   <div class='col-md-3'>&nbsp;</div>
		   </div>
		   
		   <div class="row">
			<div class='col-md-3'>&nbsp;</div>
			<div class='col-md-6'>
			<div class="inner-box">
			<h3 class="text-danger"><i class="pe-7s-close-circle"></i> <?php echo $e->getMessage()?></h3>
			<b><?php echo _("Unauthorized tokens are restricted");?></b>
			<br><br>
			<hr>
			<br>
			<span class="text-primary"><i class='fa fa-spinner fa-spin'></i> <?php echo _("Please wait while we redirecting to site ..");?></span><br><br>
			</div>
			</div>
			<div class='col-md-3'>&nbsp;</div>
		  </div>
		</div>
		</div>
		
		
		<script>
			$(document).ready(function () {
			// Handler for .ready() called.
			window.setTimeout(function () {
			location.href = "<?php echo base_url()?>site/wallet/add";
			}, 3000);
			});
		</script>
		<?php
	
}
		  
	}
	public function load_user_wallet(){
		
		$token 		= mysql_real_escape_string($this->input->post('token'));
		$userId=$this->session->userdata['site_login']['user_id'];
		$remoteCountry=$this->config->item('remote_country_code');
        $currencyInfo= $this->countries_model->get_currencies_cCode($remoteCountry);
             
		if($token=='wallet'){
			$result = $this->wallet_model->get_account_wallet($userId);
			
			
			if($result>0){
			   //echo "<h2 class='custom_priceTag'><i class='fa fa-google-wallet'></i> Your wallet balance is (".$currencyInfo[0]->character_symbol.$currencyInfo[0]->currency_name.' '.$result[0]->balance_amount.") </h2>";
			   echo '1';
			}else{
				echo "<h2 class='custom_priceTag'><i class='fa fa-google-wallet'></i>". _("You wallet balance is empty") ."</h2>";
				?>
				<?php echo sprintf(_("Please %s to your wallet, otherwise your ad goes to regular category."),'<strong>' . _('add funds') . '</strong>');?><br><br>
				<a href="<?php echo base_url()?>site/wallet/add" class="btn btn-success"><strong><?php echo _("Click here.");?></strong></a>
				<?php
			}
			
		}else{
			echo "<h2>" . _("Suspected activities are not allowed.") . "</h2>";
		}
		
			
	}
	public function success(){
			$item_number = $this->input->get('item_number'); 
			$txn_id = $this->input->get('tx');
			$payment_gross = $this->input->get('amt');
			$currency_code = $this->input->get('cc');
			$payment_status = $this->input->get('st');
			
            $vRes=$this->account_model->update_pp_payment($item_number,$txn_id,$payment_gross,$currency_code,$payment_status);
			if($vRes){
				$this->load->view('site/payment/success?token='.base64_encode($payment_status));
			}else{
				$this->load->view('site/payment/failed');
			}
			
	}
	
	
}
