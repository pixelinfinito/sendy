<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Upgrade extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		
	}
	
	public function index()
	{
		$this->load->view('site/upgrade');
	}
	
	
	public function process(){
		  
	}
	public function success(){
			$item_number = $this->input->get('item_number'); 
			$txn_id = $this->input->get('tx');
			$payment_gross = $this->input->get('amt');
			$currency_code = $this->input->get('cc');
			$payment_status = $this->input->get('st');
			
            $vRes=$this->account_model->update_pp_payment($item_number,$txn_id,$payment_gross,$currency_code,$payment_status);
			if($vRes){
				$this->load->view('site/payment/success?token='.base64_encode($payment_status));
			}else{
				$this->load->view('site/payment/failed');
			}
			
	}
	
	
	public function activation()
	{
		$validate_code 		= mysql_real_escape_string(base64_decode($this->input->get('token')));
		switch($validate_code){
			
			
			case $validate_code:
			if($validate_code!=''){
			  
			  $vRes=$this->account_model->activate_account($validate_code);
			  
			  if($vRes){
				 $this->load->view('site/validation/success');
			  }else{
				 
				 $this->load->view('site/validation/fail');
			  }
			  
			  
			}else{
				$this->load->view('site/validation/not_found');
			}
			break;
		}
		
	}
 
	
}
