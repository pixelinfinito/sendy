<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Promotions extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('promotions_model','',TRUE);
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		
	}
	   
	public function index()
	{
		
		$this->load->view('admin/promotions/all_promotions');
	}
	public function email_campaign()
	{
		
		$this->load->view('admin/promotions/email_campaign');
	}
	public function sms_campaign()
	{
		
		$this->load->view('admin/promotions/sms_campaign');
	}
	
	public function all()
	{
		$this->load->view('admin/promotions/all_promotions');
	}
	
	public function member_wallet_credit(){
		$user_id=$this->input->post('uid');
		$wallet_info=$this->wallet_model->get_account_wallet($user_id);
		if($wallet_info!=0){
			$total_amount=$wallet_info[0]->total_amount;
			$balance_amount=$wallet_info[0]->balance_amount;
			echo $total_amount.'__'.$balance_amount;
		}else{
			echo _("failed");
		}
	}
	
	 public function process_email_campaign(){
		    $errors                 = '';
	   		$promotion_type		    = $this->input->post('notify_type');
			$from_name    	        = $this->input->post('notify_from');
			$from_mail  		    = $this->input->post('notify_from_mail');
			$mail_to  		        = $this->input->post('notify_to');
			$promotion_title        = $this->input->post('notify_headline');
			$message                = $this->input->post('notify_message');
			$promotion_code         = $this->input->post('promotion_code');
			
			$intlib=$this->internal_settings->local_settings();
			$notify_from_email2= $intlib[0]->default_from_mail;
			$notify_appname= $intlib[0]->app_default_name;

			$name  			    = $notify_appname;
			$upload_folder      = 'attachments/replies/';
			$name_of_uploaded_file =  basename($_FILES['file']['name']);
			if($name_of_uploaded_file!='')
			{
				$attachment = $upload_folder . $name_of_uploaded_file;
				
				$tmp_path = $_FILES["file"]["tmp_name"];
				if(is_uploaded_file($tmp_path))
				{
					if(!copy($tmp_path,$attachment))
					{
						$errors .= '\n error while copying the uploaded file';
					}
				}
			}else{
				$attachment='';
			}
				
                ?>
				<script src="<?php echo base_url()?>js/jquery-2.1.1.js"></script>
				<link href="<?php echo base_url()?>theme4.0/admin/css/bootstrap.min.css" rel="stylesheet">
				<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/admin.css">
			
				<div class="content">
				<div class="row">
				<div class='col-md-3'>&nbsp;</div>
				<div class='col-md-3 text-right'>
				<img src="<?php echo base_url()?>theme4.0/admin/images/logo_black.png"> 
				</div>
				<div class='col-md-3 text-left'><h3 class="text-muted">| Promotions</h3></div>
				<div class='col-md-3'>&nbsp;</div>
				</div>
				<br><br>
				
				<div class="row">
				<div class='col-md-3'>&nbsp;</div>
				<div class='col-md-6'>
				<div class="box padding_20">
				<div class="box-body">
				
				<?php
				
				if($mail_to=='all')
				{
					$to_mails_list =$this->account_model->list_all_members();
					
					foreach($to_mails_list as $member){
					
						$to_name  = $member->ac_first_name.' '.$member->ac_last_name;
						$user_id  = $member->user_id;
						$to_email = $member->ac_email;
						$status   = '2';
						
						if($member->user_id!='' && $to_email!=''){
							$rset=$this->promotions_model->compose_promotion($promotion_code,$user_id,$promotion_title,
							$message,$attachment,$from_name,$from_mail,$to_name,$to_email,$promotion_type,$status);
							
							if($rset)
							{
								echo "Mail composed to <b>".$member->ac_email."</b><br>";
							}
						}
					
				
					}
					
					
				}else{
						$to_mails_list =$this->account_model->get_member_details($mail_to);
						$to_email = $to_mails_list[0]->ac_email;
						
						$to_name=$to_mails_list[0]->ac_first_name.' '.$to_mails_list[0]->ac_last_name;
						$user_id=$to_mails_list[0]->user_id;
						$status = '2';
						
						if($to_mails_list[0]->user_id!='' && $to_email!=''){
							$rset=$this->promotions_model->compose_promotion($promotion_code,$user_id,$promotion_title,
							$message,$attachment,$from_name,$from_mail,$to_name,$to_email,$promotion_type,$status);
							
							if($rset)
							{
								echo "Mail composed to <b>".$to_mails_list[0]->ac_email."</b> <br>";
							}
						}
					
				}
				?>
				 <hr>
					<span class="text-danger">
					<i class='fa fa-spinner fa-spin'></i> <b><?php echo _("Please wait")?> ..</b>
					</span><br>

					</div>
					</div>
					</div>
					<div class='col-md-3'>&nbsp;</div>
					</div>
				</div> 
			     
				 <script>
					$(document).ready(function () {
					// Handler for .ready() called.
					window.setTimeout(function () {
					location.href = "<?php echo base_url()?>promotions/email_campaign";
					}, 5000);
					});
				 </script>
				<?php
				
			
		   
   }
   
   public function process_sms_campaign(){
	   
	   		$promotion_type		    = $this->input->post('notify_type');
			$sender_from    	    = $this->input->post('notify_from');
			$to_recipients  		= $this->input->post('notify_to');
			$message                = $this->input->post('notify_message');
			$promotion_code         = $this->input->post('promotion_code');
			
			$intlib=$this->internal_settings->local_settings();
			$notify_from_email2= $intlib[0]->default_from_mail;
			$notify_appname= $intlib[0]->app_default_name;

				
                ?>
				<script src="<?php echo base_url()?>js/jquery-2.1.1.js"></script>
				<link href="<?php echo base_url()?>theme4.0/admin/css/bootstrap.min.css" rel="stylesheet">
				<link rel="stylesheet" href="<?php echo base_url()?>theme4.0/admin/css/admin.css">
			
				<div class="content">
				<div class="row">
				<div class='col-md-3'>&nbsp;</div>
				<div class='col-md-3 text-right'>
				<img src="<?php echo base_url()?>theme4.0/admin/images/logo_black.png"> 
				</div>
				<div class='col-md-3 text-left'><h3 class="text-muted">| Promotions</h3></div>
				<div class='col-md-3'>&nbsp;</div>
				</div>
				<br><br>
				
				<div class="row">
				<div class='col-md-3'>&nbsp;</div>
				<div class='col-md-6'>
				<div class="box padding_20">
				<div class="box-body">
				
				<?php
				
				if($to_recipients=='all')
				{
					$to_member_list =$this->account_model->list_all_members();
					
					foreach($to_member_list as $member){
					
						$to_name=$member->ac_first_name.' '.$member->ac_last_name;
						$user_id=$member->user_id;
						$to_phone = $member->ac_phone;
						$status = '2';
						
						if($member->user_id!='' && $member->ac_phone){
							$rset=$this->promotions_model->compose_sms_promotion($promotion_code,$user_id,
							$message,$sender_from,$to_phone,$to_name,$promotion_type,$status,$notify_appname);
							
							if($rset)
							{
								echo "SMS composed to <b>".$member->ac_phone."</b><br>";
							}
						}
				
					}
					
					
				}else{
						$to_member_list =$this->account_model->get_member_details($to_recipients);
						$to_phone = $to_member_list[0]->ac_phone;
						
						$to_name=$to_member_list[0]->ac_first_name.' '.$to_member_list[0]->ac_last_name;
						$user_id=$to_member_list[0]->user_id;
						$status = '2';
						
						if($to_member_list[0]->user_id!=''  && $to_phone!=''){
							$rset=$this->promotions_model->compose_sms_promotion($promotion_code,$user_id,
							$message,$sender_from,$to_phone,$to_name,$promotion_type,$status,$notify_appname);
							
							if($rset)
							{
								echo "SMS composed to <b>".$to_member_list[0]->ac_phone."</b> <br>";
							}
						}
				}
				?>
				 <hr>
					<span class="text-danger">
					<i class='fa fa-spinner fa-spin'></i> <b><?php echo _("Please wait")?> ..</b>
					</span><br>

					</div>
					</div>
					</div>
					<div class='col-md-3'>&nbsp;</div>
					</div>
				</div> 
			     
				 <script>
					$(document).ready(function () {
					// Handler for .ready() called.
					window.setTimeout(function () {
					location.href = "<?php echo base_url()?>promotions/sms_campaign";
					}, 5000);
					});
				 </script>
				<?php
				
			
		   
   }
   
    public function load_promotion_message(){
	           $promotion_id=$this->input->post('promotion_id');
			   $data=$this->promotions_model->load_promotion_message($promotion_id);
			  
			   if($data!=0){
				   echo $data[0]->promotion_body;
			   }else{
				   echo 'failed';
			   }
	}
	
	 public function delete_promotion(){
	           $promotion_id=$this->input->post('promotion_id');
			   $res=$this->promotions_model->delete_promotion($promotion_id);
			  
			   if($res){
				   echo 'success';
			   }else{
				   echo 'failed';
			   }
	}
	
	public function delete_selected_promotions(){
		$promotion_ids=mysql_real_escape_string($this->input->post('cIds'));
		$res=$this->promotions_model->delete_selected_promotions($promotion_ids);
		
		if($res!=0){
			echo 'success';
			
		}else{
			echo 'failed';
		}
	}
	
	
	
}

