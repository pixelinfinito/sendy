<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Login extends CI_Controller {
   
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('user_model','',TRUE);
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	public function authenticate()
	{
		$data = json_decode(file_get_contents("php://input"));
		$username = mysql_real_escape_string($data->uname);
		$password = mysql_real_escape_string($data->pswd);
        
		$username = $username;
		$password = md5($password);
		$result = $this->user_model->authenticate($username, $password);
		$appSettings=$this->app_settings_model->get_app_settings();
		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{
				$sess_array = array(
				'user_id' => $row->user_id,
				'username' => $row->username,
				'user_group' => $row->user_group,
				'user_email' => $row->user_email,
				'date_of_join' => $row->date_of_join,
				'current_timezone'=>$appSettings[0]->app_timezone,
				'profile_image'=>$row->profile_thumbnail
				
				);
				
				$this->session->set_userdata('login_in',$sess_array);
			}
			   
			echo _("success");
		}else{
			echo "";
		}
		
   }
   
  
	
}
