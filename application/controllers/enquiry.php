<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Enquiry extends CI_Controller {
   
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('countries_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('enquiry_model','',TRUE);
		$this->load->model('visitors_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		
	}
	   
	public function index()
	{
		$this->load->view('front/enquiry');
	}
	public function all()
	{
		$this->load->view('admin/enquiry/all');
	}
	public function send_enquiry(){
		
		$enq_country= mysql_real_escape_string($this->input->post('enq_country'));
		$enq_name= mysql_real_escape_string($this->input->post('enq_name'));
		$enq_contact= mysql_real_escape_string($this->input->post('enq_contact'));
		$enq_email= mysql_real_escape_string($this->input->post('enq_email'));
		$enq_type= mysql_real_escape_string($this->input->post('enq_type'));
		$enq_comments= mysql_real_escape_string($this->input->post('enq_comments'));
		
		
		//// prepare sms alert to admin
		$alertAppSettings   = $this->app_settings_model->get_primary_settings();
		$appAlertMobile     = $alertAppSettings[0]->default_mobile_number;
		$alertSMSSettings   = $this->app_settings_model->get_sms_settings();
		$account_sid        = $alertSMSSettings[0]->twilio_accid;
		$auth_token         = $alertSMSSettings[0]->twilio_authtoken;
		$outboundNumber     = $alertSMSSettings[0]->twilio_origin_number;
		$alertMasking       = "EnqAlerts";
		$alertContent       = "Hello you received a enquiry by: ".$enq_name.", Country: ".$enq_country." Please logon to ".$alertAppSettings[0]->app_default_name; 
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
		
		/// saving enquiry 
		
		$rSet=$this->enquiry_model->send_enquiry($enq_country,$enq_name,$enq_contact,$enq_email,$enq_type,$enq_comments);
		if($rSet!=0){
			echo _("success");
			//// trigger sms alert to admin 
			$status=$client->account->messages->create(array( 
				'To' => $appAlertMobile, 
				'From' => $alertMasking, 
				'Body' => $alertContent,   
			));
		
			
		}else{
			echo 'failed';
		}
		
	}
	
	 public function delete_enquiry(){
	           $enquiry_id=$this->input->post('enquiry_id');
			   $res=$this->enquiry_model->delete_enquiry($enquiry_id);
			  
			   if($res){
				   echo 'success';
			   }else{
				   echo 'failed';
			   }
	}
	
}
