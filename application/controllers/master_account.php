<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Master_Account extends CI_Controller {
  
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('countries_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		
		
	}
	
	public function index()
	{
		$this->load->view('admin/master_account/check_cn');
	}
	public function check_cn()
	{
		$this->load->view('admin/master_account/check_cn');
	}
	
	public function service_utility(){
		$this->load->view('admin/master_account/sms_usage');
	}
	
	
	
}
