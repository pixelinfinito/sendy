<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class User_Groups extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('caller_number_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		$this->load->model('user_groups_model','',TRUE);
		$this->load->model('site/account_model','',TRUE);
		$this->load->model('site/sms_model','',TRUE);
		$this->load->model('site/payment_model','',TRUE);
		$this->load->model('site/wallet_model','',TRUE);
		$this->load->model('app_settings_model','',TRUE);
		
	}
	   
	public function index()
	{
		
		$this->load->view('admin/user_groups/user_groups');
	}
	
	public function add()
	{
		$this->load->view('admin/user_groups/add_user_group');
	}
	public function edit(){
		
		$this->load->view('edit/edit_user_group');
	}
	public function delete_group(){
		
		$group_id = mysql_real_escape_string($this->input->post('group_id'));
		$result = $this->user_groups_model->delete_group($group_id);
		if($result){
			
			  echo _("success");
		}else{
			    
				echo _("failed");
		}
	}
	public function add_process()
	{
		$data = json_decode(file_get_contents("php://input"));
		$group_name = mysql_real_escape_string($data->ug_name);
		$group_desc = mysql_real_escape_string($data->ug_desc);
		$result = $this->user_groups_model->add_group($group_name, $group_desc);
		
		if($result){
			
			  echo _("success");
			
		}else{
			    echo _("failed");
				
		}
	}
	
	public function update_process()
	{
		
		$data = json_decode(file_get_contents("php://input"));
		$group_id   = mysql_real_escape_string($data->group_id);
		$group_name = mysql_real_escape_string($data->ug_name);
		$group_desc = mysql_real_escape_string($data->ug_desc);
		
		$result = $this->user_groups_model->update_group($group_id,$group_name,$group_desc);
		
		if($result){
			
			   echo _("success");
		}else{
			   echo _("failed");
		}
	}
}

