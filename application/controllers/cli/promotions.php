<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Promotions extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('app_settings_model','',true);
		$this->load->model('promotions_model','',true);
		$this->load->model('site/twilio_model','',true);
		$this->load->model('site/wallet_model','',true);
		$this->load->model('site/account_model','',true);
		$this->load->model('countries_model','',true);
		$this->load->model('cli/sms_model','',true);
	}
	
	public function index()
	{
		// no CI crotab 
		if (!$this->input->is_cli_request()){
			 Promotions::trigger_sms_campaign();
		} else {
			echo _("access is NOT from CLI").PHP_EOL;
		}
	}
	
	public function trigger_sms_campaign(){
		
		$appSettings=$this->app_settings_model->get_app_settings();
		$smsSettings=$this->app_settings_model->get_sms_settings();
		
		$account_sid = $smsSettings[0]->twilio_accid;
		$auth_token = $smsSettings[0]->twilio_authtoken;
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
        
		
		//// getting waiting list 
		$promotion_info=$this->promotions_model->list_promotions_bysms();
		if($promotion_info!=0){
		foreach($promotion_info as $alert){
			
			$member_info=$this->account_model->get_member_details($alert->user_id);
			$check_mobile_verify_status=$member_info[0]->mobile_validate;
			$user_id=$member_info[0]->user_id;
			$member_name=$member_info[0]->ac_first_name.' '.$member_info[0]->ac_last_name;
			
			
			/// if mobile verified
			if($check_mobile_verify_status!=0){
				
				   //// trigger alert to member
					$content = $alert->promotion_body;
					$toNumber = $alert->mail_to; 
					if($smsSettings[0]->twilio_sender_id!=''){
						$outboundNumber = $smsSettings[0]->twilio_sender_id; 
					}else{
						$outboundNumber = $smsSettings[0]->twilio_origin_number; 
					}
					
					try 
					{
						
						
						$status=$client->account->messages->create(array( 
						'To' => $toNumber, 
						'From' => $outboundNumber, 
						'Body' => $content,   
						));

						$fCall=$status->status;
						$messageToken=$status->sid;
						$status=1;
						$upResponse=$this->promotions_model->update_promotion_status($alert->promotion_id,$status);
						
						
					}catch (Services_Twilio_RestException $e){
						
						//echo $e->getMessage();
						//$remarks= $e->getMessage();
						$status=0;
						$upResponse=$this->promotions_model->update_promotion_status($alert->promotion_id,$status);
					}
					
				}else{
					    $status=0;
						$upResponse=$this->promotions_model->update_promotion_status($alert->promotion_id,$status);
				}
			}
		}else{
			echo _("oops empty set!");
		}
		
	}
	
	public function trigger_email_campaign(){
		
		$appSettings=$this->app_settings_model->get_app_settings();
		
		//// getting waiting list 
		$promotion_info=$this->promotions_model->list_promotions_byemail();
		if($promotion_info!=0){
		foreach($promotion_info as $alert){
			
			$member_info=$this->account_model->get_member_details($alert->user_id);
			$is_member_validated=$member_info[0]->is_validated;
			
			$user_id=$member_info[0]->user_id;
			$member_name=$member_info[0]->ac_first_name.' '.$member_info[0]->ac_last_name;
			
			
			/// if mobile verified
			if($is_member_validated!=0){
				
					   //// trigger alert to member
						$message = $alert->promotion_body;
						$toMail = $alert->mail_to; 
						$fromMail = $alert->mail_from; 
						$fromName = $alert->from_name; 
						$subject = $alert->promotion_title; 
						$attachment = $alert->attachment_path; 
					
						$this->load->library('email');
						$this->email->from($fromMail,$fromName);
						$this->email->to($toMail);
						$this->email->subject($subject);
						$this->email->message($message);
						if($attachment!=''){
						$this->email->attach($attachment);
						}
						
						if ($this->email->send()) 
						{
							$status=1;
							$upResponse=$this->promotions_model->update_promotion_status($alert->promotion_id,$status);
						}else{
							$status=0;
							$upResponse=$this->promotions_model->update_promotion_status($alert->promotion_id,$status);
						}
					
					
				}
			}
		}else{
			echo _("oops empty set!");
		}
		
	}
	
}
