<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Run extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->model('app_settings_model','',true);
		$this->load->model('site/twilio_model','',true);
		$this->load->model('site/account_model','',true);
		$this->load->model('countries_model','',true);
		$this->load->model('cli/sms_model','',true);
	}
	
	public function index()
	{
		// no CI crotab 
		if (!$this->input->is_cli_request()){
			 Run::trigger_sms_campaign();
		} else {
			echo _("access is NOT from CLI").PHP_EOL;
		}
	}
	
	public function trigger_sms_campaign(){
		
		$appSettings=$this->app_settings_model->get_app_settings();
		$smsSettings=$this->app_settings_model->get_sms_settings();
		
		
		$account_sid = $smsSettings[0]->twilio_accid;
		//Put your Twilio Auth Key here:
		$auth_token = $smsSettings[0]->twilio_authtoken;
		// Put your Twilio Outgoing SMS enabled number here:
		
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
        
		$scheduleTimezone=$this->sms_model->get_cli_timezone_schedules();
		
		if($scheduleTimezone!=0){
			//print_r($scheduleTimezone);
			foreach($scheduleTimezone as $timezone) {
				//echo $timezone->schedule_timezone;
				$waitingList=$this->sms_model->get_cli_campaign_schedules($timezone->schedule_timezone);
				//echo count($waitingList)."<br>";
				//print_r($waitingList);exit;
				$authMethod = 'sms';
				if($waitingList!=0){
					foreach($waitingList as $campaign) {
					$campaign_id= $campaign->campaign_id;	
					$content = $campaign->message;
					$toNumber = $campaign->to_number; 
					$outboundNumber = $campaign->from_number; 
						
						try 
						{
							
							$status=$client->account->messages->create(array( 
								'To' => $toNumber, 
								'From' => $outboundNumber, 
								'Body' => $content,   
							));
						   
						   $fCall=$status->status;
						   $messageToken=$status->sid;
						   //$final_status=$client->account->messages->get($messageToken);
						   //echo $final_status->status;
						   
						   if($fCall!='queued'){
							   $remarks='message failed';
							   $deliver_status=0;
							   $message_id=$messageToken;
							   $upResponse=$this->sms_model->update_cli_campaign_status($campaign_id,$remarks,$message_id);
						   }else{
							   $remarks='message in queued';
							   $deliver_status=3;
							   $message_id=$messageToken;
							   $upResponse=$this->sms_model->update_cli_campaign_status($campaign_id,$remarks,$message_id,$deliver_status);
						   }
						}catch (Services_Twilio_RestException $e){
							   //echo $e->getMessage();
							   $remarks= $e->getMessage();
							   $deliver_status=0;
							   $message_id='';
							   $upResponse=$this->sms_model->update_cli_campaign_status($campaign_id,$remarks,$message_id,$deliver_status);
						}

					}
					
					//// getting final status
					Run::grab_smsfinal_status();
					
				}else{
					echo _("No CLI Schedule found");
				}
			}
			
		}else{
			echo _("No CLI timezone found");
		}
		
	}
	
	public function grab_smsfinal_status(){
		
		$cost_arr=array();
		$parse_campaign_code="";
		
		/// getting queued sms
		$smsSettings=$this->app_settings_model->get_sms_settings();
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$account_sid = $smsSettings[0]->twilio_accid;
		//Put your Twilio Auth Key here:
		$auth_token = $smsSettings[0]->twilio_authtoken;
		
		// Put your Twilio Outgoing SMS enabled number here:
		$client = new Services_Twilio($account_sid, $auth_token); 
        //echo $final_status->status;
		$queuedList=$this->sms_model->get_cli_queued_sms();
		
		if($queuedList!=0){
		foreach($queuedList as $message){
			// getting wallet credit
			$user_id=$message->user_id;
			$walletInfo= $this->account_model->get_account_wallet($user_id);
			if(@$walletInfo[0]->balance_amount!=''){
				$accountCredit=$walletInfo[0]->balance_amount;
			}else{
				$accountCredit=0;
			}
			$message_id=$message->message_id;
			$campaign_id=$message->campaign_id;
			$unit_cost=$message->unit_cost;
			array_push($cost_arr,$unit_cost);
			$parse_campaign_code='Campaign code: '.$message->campaign_code.'<br>'.$message->message;
			$final_status=$client->account->messages->get($message_id);
			//echo $final_status->status;
			$finalCall=$final_status->status;
			//$attempt_n=$final_status->attempt_n;
			switch($finalCall){
				case 'failed':
				$deliver_status=0;
			    $remarks='message final status failed';
				$upResponse=$this->sms_model->update_cli_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit);
				break;
				
				case 'delivered':
				$deliver_status=1;
			    $remarks='message has been sent';
				$upResponse=$this->sms_model->update_cli_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit);
				break;
				
				case 'sent':
				$deliver_status=1;
			    $remarks='message has been sent';
				$upResponse=$this->sms_model->update_cli_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit);
				break;
				
				default:
				$upResponse=$this->sms_model->update_cli_smsfinal_status($user_id,$campaign_id,$remarks,$message_id,$deliver_status,$unit_cost,$accountCredit);
			}
			
		}
		 /*if(array_sum($cost_arr)>0){
			 /// generate wallet history invoice
			    $totalPayment=array_sum($cost_arr);
			    $settingsInfo= $this->app_settings_model->get_site_settings();
				$wallet_min_val=@$settingsInfo[0]->wallet_min_val;
				$wallet_max_val=@$settingsInfo[0]->wallet_max_val;
				
				 if($wallet_min_val!='0' && $wallet_max_val!='0'){
					 $camp_reference="OCWLSC".rand($wallet_min_val,$wallet_max_val);
				 }else{
					 $camp_reference='OCWLSC'.rand(600,6000);
				 }
			$ad_reference='OCSMSCMP';
			$ad_payment=$totalPayment;
			$payment_method='wallet';
			$payment_reference=$camp_reference;
			$payment_remarks='<strong>' . _('SMS Campaign') . '</strong><br> '.$parse_campaign_code;
			$payment_status='1';
			 
			 $invRset=$this->sms_model->campaign_wallet_invoice($ad_reference,$ad_payment,$payment_method,$payment_reference,
			 $payment_remarks,$payment_status,$user_id);
			 if($invRset){
				 echo _("Wallet invoice generated.");
			 }
			 
			 
		 }*/
		}else{
			echo _("Empty set in final call");
		}
		
	}
	
	
}
