<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('error_reporting', E_ALL);
/*
 *  Project  : Bulk SMS Campaign Software
 *	@author  : Mr.Saida D
 *  @support : support@onetextglobal.com
 *	date	 : 01 July, 2016
 *	http     : https://onetextglobal.com
 *  version: 1.0
 */
 
class Credit extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url'));
		
		$this->load->model('app_settings_model','',true);
		$this->load->model('site/twilio_model','',true);
		$this->load->model('site/wallet_model','',true);
		$this->load->model('site/account_model','',true);
		$this->load->model('countries_model','',true);
		$this->load->model('cli/sms_model','',true);
	}
	
	public function index()
	{
		// no CI crotab 
		if (!$this->input->is_cli_request()){
			 Credit::trigger_threshold_list();
		} else {
			echo _("access is NOT from CLI").PHP_EOL;
		}
	}
	
	public function trigger_threshold_list(){
		
		$appSettings=$this->app_settings_model->get_app_settings();
		$smsSettings=$this->app_settings_model->get_sms_settings();
		
		$account_sid = $smsSettings[0]->twilio_accid;
		$auth_token = $smsSettings[0]->twilio_authtoken;
		include APPPATH."third_party/twoFactorAuthentication/Services/Twilio.php";
		$client = new Services_Twilio($account_sid, $auth_token); 
        
		
		//// getting waiting list 
		$credit_alert_info=$this->account_model->list_member_credit_threshold();
		foreach($credit_alert_info as $alert){
			
			$check_mobile_verify_status=$alert->mobile_validate;
			$user_id=$alert->user_id;
			$threshold_limit=$alert->payment_threshold;
			$member_name=$alert->ac_first_name.' '.$alert->ac_last_name;
			
			/// if mobile verified
			if($check_mobile_verify_status!=0){
				$wallet_info= $this->account_model->get_account_wallet($user_id);
				$available_balance=$wallet_info[0]->balance_amount;
				$credit_threshold_count=$wallet_info[0]->credit_threshold_count;
				$credit_threshold_lasttimestamp=$wallet_info[0]->credit_threshold_timestamp;
				
				if($available_balance<=$threshold_limit){
					
					date_default_timezone_set($appSettings[0]->app_timezone);
					$today=date('Y-m-d H:i:s');
					//$difference = strtotime($credit_threshold_lasttimestamp) - strtotime($today);
		            
					//// first trigger alert 
					if($credit_threshold_lasttimestamp=="0000-00-00 00:00:00")
					{
						   //// trigger alert to member
							$content = 'Dear '.$member_name.',your credit getting low please add funds & current balance '.$appSettings[0]->app_currency.' '.$available_balance.' thank you '.$appSettings[0]->app_default_name;
							$toNumber = $alert->ac_phone; 
							if($smsSettings[0]->twilio_sender_id!=''){
								$outboundNumber = $smsSettings[0]->twilio_sender_id; 
							}else{
								$outboundNumber = $smsSettings[0]->twilio_origin_number; 
							}
							
							try 
							{

								$status=$client->account->messages->create(array( 
								'To' => $toNumber, 
								'From' => $outboundNumber, 
								'Body' => $content,   
								));

								$fCall=$status->status;
								$messageToken=$status->sid;
								$upResponse=$this->wallet_model->update_credit_threshold($user_id,$credit_threshold_count);
								//print_r($upResponse);
								
							}catch (Services_Twilio_RestException $e){
								
								//echo $e->getMessage();
								$remarks= $e->getMessage();
								$upResponse=$this->wallet_model->update_credit_threshold($user_id,$credit_threshold_count);
							}
					}
					else{
						//// next trigger
					
						$firstTime=strtotime($credit_threshold_lasttimestamp);
						$lastTime=strtotime($today);
						$difference=$lastTime-$firstTime;
						$years = abs(floor($difference / 31536000));
						$days = abs(floor(($difference-($years * 31536000))/86400));
						$hours = abs(floor(($difference-($years * 31536000)-($days * 86400))/3600));
						$mins = abs(floor(($difference-($years * 31536000)-($days * 86400)-($hours * 3600))/60));
						
						//// 30 days = 43200 in minutes
						$credit_threshold_interval=$smsSettings[0]->credit_threshold_interval;
						if($mins>$credit_threshold_interval){
							//// trigger alert to member
							$content = 'Dear '.$member_name.',your credit getting low please add funds & current balance '.$appSettings[0]->app_currency.' '.$available_balance.' thank you '.$appSettings[0]->app_default_name;
							$toNumber = $alert->ac_phone; 
							if($smsSettings[0]->twilio_sender_id!=''){
								$outboundNumber = $smsSettings[0]->twilio_sender_id; 
							}else{
								$outboundNumber = $smsSettings[0]->twilio_origin_number; 
							}
							
							try 
							{

								$status=$client->account->messages->create(array( 
								'To' => $toNumber, 
								'From' => $outboundNumber, 
								'Body' => $content,   
								));

								$fCall=$status->status;
								$messageToken=$status->sid;
								$upResponse=$this->wallet_model->update_credit_threshold($user_id,$credit_threshold_count);
								print_r($upResponse);
								
							}catch (Services_Twilio_RestException $e){
								
								//echo $e->getMessage();
								$remarks= $e->getMessage();
								$upResponse=$this->wallet_model->update_credit_threshold($user_id,$credit_threshold_count);
							}
						 }
					}
				}
			}
		}
		
		
	}
	
}
