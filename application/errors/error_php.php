<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4><?php echo _("A PHP Error was encountered")?></h4>

<p><?php echo _("Severity")?>:<?php echo $severity; ?></p>
<p><?php echo _("Message")?>:<?php echo $message; ?></p>
<p><?php echo _("Filename")?>:<?php echo $filepath; ?></p>
<p><?php echo _("Line Number")?>:<?php echo $line; ?></p>

</div>