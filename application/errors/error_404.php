<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo _("404 Page Not Found")?></title>

 <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="../../font-awesome/css/font-awesome.css" rel="stylesheet">-->

    <!-- Morris -->
    <link href="../../css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="../../css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="../../js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
   

    <link href="../../css/animate.css" rel="stylesheet">
    <link href="../../css/prettify.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">

   


</head>
<body class="gray-bg">
	
    <div class="middle-box text-center animated fadeInDown">
       
        <h1 class="font-bold">404</h1>
        <div class="error-desc">
       <?php //echo $heading; ?>
       <h3 class="font-bold"><?php echo $message; ?></h3>
       <small><?php echo _("Suspious activities are prevented , please don't attempt.")?></small>
       <br><small><?php echo _("Thank you for your co-operation.")?></small>
       
        </div>
    </div>

</body>
</html>